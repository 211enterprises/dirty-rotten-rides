function getScript(url,success) {
    var script = document.createElement('script');
    script.src = url;
    var head = document.getElementsByTagName('head')[0],
    done = false;
    // Attach handlers for all browsers
    script.onload = script.onreadystatechange = function() {
        if (!done && (!this.readyState
            || this.readyState == 'loaded'
            || this.readyState == 'complete')) {
            done = true;
            success();
            script.onload = script.onreadystatechange = null;
            head.removeChild(script);
        }
    };
    head.appendChild(script);
}
getScript('//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',function() {
    js = jQuery.noConflict();
    js(document).ready(function() {
        js('#form-dirtygirlsubmission').submit(function(event) {
            
					if(js('#jform_image_1').val() != ''){
						js('#jform_image_1_hidden').val(js('#jform_image_1').val());
					}
					
					if (js('#jform_image_1').val() == '' && js('#jform_image_1_hidden').val() == '' || js('#jform_image_2').val() == '' && js('#jform_image_2_hidden').val() == '' || js('#jform_image_3').val() == '' && js('#jform_image_3_hidden').val() == '' || js('#jform_image_4').val() == '' && js('#jform_image_4_hidden').val() == '' || js('#jform_image_5').val() == '' && js('#jform_image_5_hidden').val() == '') {
						alert('Please upload 5 images before submitting!');
						event.preventDefault();
					}
					
					if(js('#jform_image_2').val() != ''){
						js('#jform_image_2_hidden').val(js('#jform_image_2').val());
					}
					
					if(js('#jform_image_3').val() != ''){
						js('#jform_image_3_hidden').val(js('#jform_image_3').val());
					}
					
					if(js('#jform_image_4').val() != ''){
						js('#jform_image_4_hidden').val(js('#jform_image_4').val());
					}
					
					if(js('#jform_image_5').val() != ''){
						js('#jform_image_5_hidden').val(js('#jform_image_5').val());
					} 
					
					//Agree checkboxes
					js("input[type='checkbox']").each(function() {
						if(js(this).prop('checked') == false) {
							js(this).closest('li').css({
								color: 'red'
							});
							event.preventDefault();
						} else {
							js(this).closest('li').removeAttr('style');
						}
					});
				}); 
    });
});