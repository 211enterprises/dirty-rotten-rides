<?php 

$currentMonth = date('F');
$deadlineMonth = date('F',strtotime(date("F", strtotime($currentMonth)) . " +1 month"));
$nextVotingMonth = date('F',strtotime(date("F", strtotime($currentMonth)) . " +1 month"));
$lastDayOfMonth = date('t');
$lastDayOfNextMonth = date('t',strtotime('next month'));
	
?>
<p class="submissionRules">
	<span style="color: green;">Do's</span><br />
	Upload clean, clear photos<br />
	size should be at least 400 x 600 minimum<br />
	Ask photographer for permission before submitting<br />
	Please upload five completely different images. Similar images will not be accepted.<br /><br />
					
	<span style="color: red;">Don't</span><br />
	Don't upload a instagram photo that has been edited<br />
	No fuzzy or pixalated photos<br />
	No full nudity<br />
	Don't upload a tiny photo<br />
					
	By uploading your photos you give DRR rights to use them for any and all advertising, promotions and marketing.
	Photos that are accepted and good quality will be put up on the voting page. Members can vote once per hour.<br /><br />
	
	Dirty Rotten Rides is looking for a pinup each month to feature! We will look over all submissions and pick girls based on photo quality and answers in the form. Voting will be by members on the site. The 3 pinups with the most votes will need to submit a video at the end and one girl will be chosen by the DRR crew. That girl will be the pinup for the following month and receive a Dirty Girl jacket, and some DRR apparel. That girl may also be asked to attend events with us and might be in the 2015 calendar. We will also select from the Dirty girls for promotions, events, calendars, and posters.<br /><br />
				
	For any questions please email us at <a href="mailto: dirty@dirtyrottenrides.com" target="_blank">dirty@dirtyrottenrides.com</a>
</p>
        
<hr class="submissionRules" />
	
	<span class="submissionDates">
		Voting for <?php echo $nextVotingMonth; ?>'s pinup begins <?php echo $currentMonth; ?> 7th. It will end <?php echo $currentMonth; ?> 30th. Winner is announced <?php echo $nextVotingMonth; ?> 1st or 2nd. 
	</span>
	
<hr class="submissionRules" />

