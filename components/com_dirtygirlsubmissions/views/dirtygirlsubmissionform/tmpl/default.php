<?php
/**
 * @version     1.0.0
 * @package     com_dirtygirlsubmissions
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Allan Cutler <allan@211enterprises.com> - http://www.211enterprises.com
 */
// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');

//Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_dirtygirlsubmissions', JPATH_ADMINISTRATOR);

$doc = JFactory::getDocument();
$doc->addStyleSheet('/components/com_dirtygirlsubmissions/css/submissions.css');
$doc->addScript('/components/com_dirtygirlsubmissions/js/submission.js');
?>

<figure class="nextDirtyGirl">
	<img src="/templates/dirtygirls/images/nextDirtyGirl.gif" alt="" />
</figure>

<div class="dirtygirlsubmission-edit front-end-edit">
    <?php if (!empty($this->item->id)): ?>
        <h1>Edit <?php echo $this->item->id; ?></h1>
    <?php else: ?>
    		<h1>Dirty Girl of the Month Submission</h1>
				<h2>Want to win FREE dirty swag and prizes? Submit to become this months dirty girl! Only one<br/>submission per month is accepted.</h2>
        <?php include_once('submissionRules.php'); ?>
    <?php endif; ?>

    <form id="form-dirtygirlsubmission" action="<?php echo JRoute::_('index.php?option=com_dirtygirlsubmissions&task=dirtygirlsubmission.save'); ?>" method="post" class="form-validate" enctype="multipart/form-data">
        <ul class="submissionForm">
	        
	        <?php //Partial for form fields ?>
					<?php require_once('formFields.php'); ?>
					
					<li class="imageNotice" style="margin: 30px 0;">You're required to submit five images of yourself.</li>
					
					<?php //Partial for file fields ?>
					<?php require_once('fileFields.php'); ?>
					
					<?php //Partial for user agree fields ?>
					<?php require_once('agreeFields.php'); ?>
        </ul>

        <div>
            <button type="submit" class="validate"><span><?php echo JText::_('JSUBMIT'); ?></span></button>
            <?php echo JText::_('or'); ?>
            <a href="<?php echo JRoute::_('index.php?option=com_dirtygirlsubmissions&task=dirtygirlsubmission.cancel'); ?>" title="<?php echo JText::_('JCANCEL'); ?>"><?php echo JText::_('JCANCEL'); ?></a>

            <input type="hidden" name="option" value="com_dirtygirlsubmissions" />
            <input type="hidden" name="task" value="dirtygirlsubmissionform.save" />
            <?php echo JHtml::_('form.token'); ?>
        </div>
    </form>
</div>
