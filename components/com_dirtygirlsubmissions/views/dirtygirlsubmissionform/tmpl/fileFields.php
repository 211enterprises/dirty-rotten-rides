<li>
	<?php echo $this->form->getLabel('image_1'); ?>
	<?php echo $this->form->getInput('image_1'); ?>
</li>

<?php if (!empty($this->item->image_1)) : ?>
	<a href="<?php echo JRoute::_(JUri::base() . 'administrator' . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'com_dirtygirlsubmissions' . DIRECTORY_SEPARATOR . 'uploads/submissions' .DIRECTORY_SEPARATOR . $this->item->image_1, false);?>"><?php echo JText::_("COM_DIRTYGIRLSUBMISSIONS_VIEW_FILE"); ?>
	</a>
<?php endif; ?>

<input type="hidden" name="jform[image_1]" id="jform_image_1_hidden" value="<?php echo $this->item->image_1 ?>" />
<li>
	<?php echo $this->form->getLabel('image_2'); ?>
	<?php echo $this->form->getInput('image_2'); ?>
</li>

<?php if (!empty($this->item->image_2)) : ?>
	<a href="<?php echo JRoute::_(JUri::base() . 'administrator' . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'com_dirtygirlsubmissions' . DIRECTORY_SEPARATOR . 'uploads/submissions' .DIRECTORY_SEPARATOR . $this->item->image_2, false);?>"><?php echo JText::_("COM_DIRTYGIRLSUBMISSIONS_VIEW_FILE"); ?>
	</a>
<?php endif; ?>
<input type="hidden" name="jform[image_2]" id="jform_image_2_hidden" value="<?php echo $this->item->image_2 ?>" />

<li>
	<?php echo $this->form->getLabel('image_3'); ?>
	<?php echo $this->form->getInput('image_3'); ?>
</li>

<?php if (!empty($this->item->image_3)) : ?>
	<a href="<?php echo JRoute::_(JUri::base() . 'administrator' . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'com_dirtygirlsubmissions' . DIRECTORY_SEPARATOR . 'uploads/submissions' .DIRECTORY_SEPARATOR . $this->item->image_3, false);?>"><?php echo JText::_("COM_DIRTYGIRLSUBMISSIONS_VIEW_FILE"); ?>
	</a>
<?php endif; ?>
<input type="hidden" name="jform[image_3]" id="jform_image_3_hidden" value="<?php echo $this->item->image_3 ?>" />

<li>
	<?php echo $this->form->getLabel('image_4'); ?>
	<?php echo $this->form->getInput('image_4'); ?>
</li>

<?php if (!empty($this->item->image_4)) : ?>
		<a href="<?php echo JRoute::_(JUri::base() . 'administrator' . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'com_dirtygirlsubmissions' . DIRECTORY_SEPARATOR . 'uploads/submissions' .DIRECTORY_SEPARATOR . $this->item->image_4, false);?>"><?php echo JText::_("COM_DIRTYGIRLSUBMISSIONS_VIEW_FILE"); ?>
		</a>
<?php endif; ?>
<input type="hidden" name="jform[image_4]" id="jform_image_4_hidden" value="<?php echo $this->item->image_4 ?>" />

<li>
	<?php echo $this->form->getLabel('image_5'); ?>
	<?php echo $this->form->getInput('image_5'); ?>
</li>

<?php if (!empty($this->item->image_5)) : ?>
		<a href="<?php echo JRoute::_(JUri::base() . 'administrator' . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'com_dirtygirlsubmissions' . DIRECTORY_SEPARATOR . 'uploads/submissions' .DIRECTORY_SEPARATOR . $this->item->image_5, false);?>"><?php echo JText::_("COM_DIRTYGIRLSUBMISSIONS_VIEW_FILE"); ?>
		</a>
<?php endif; ?>
<input type="hidden" name="jform[image_5]" id="jform_image_5_hidden" value="<?php echo $this->item->image_5 ?>" />
