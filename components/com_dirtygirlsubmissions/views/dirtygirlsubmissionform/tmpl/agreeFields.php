<li style="padding: 20px 0 0 0;">
	<input type="checkbox" name="agreeRules" id="agreeRules" />
	I have read and understand the rules & policies provided at the top of this form.
</li>
<li>
	<input type="checkbox" name="agreeImageRes" id="agreeImageRes" />
	My photo is 300 resolution or higher (I understand that if this policy is not followed, my entry will be forfeited).
</li>
<li>
	<input type="checkbox" name="agreeRelease" id="agreeRelease" />
	I have a model and/or photographer's release for the photo that I have uploaded and am able to supply at any time if requested.
</li>
<li>
	<input type="checkbox" name="agreeLiability" id="agreeLiability" />
	I am releasing Dirty Rotten Rides from any and all liability. I take full responsibility for the photo and information that I have provided.
</li>
<li>
	<input type="checkbox" name="agree" id="agree" />
	<span style="color:red;">I agree</span> to submit a minimum of 5 high resolution photos of myself if I'm announced as the winner.
</li>