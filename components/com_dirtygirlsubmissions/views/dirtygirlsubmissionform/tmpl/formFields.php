<li style="display:none;">
	<?php echo $this->form->getLabel('id'); ?>
	<?php echo $this->form->getInput('id'); ?>
</li>
<li>
	<?php echo $this->form->getLabel('first_name'); ?>
	<?php echo $this->form->getInput('first_name'); ?>
</li>
<li>
	<?php echo $this->form->getLabel('last_name'); ?>
	<?php echo $this->form->getInput('last_name'); ?>
</li>
<li>
	<?php echo $this->form->getLabel('email_address'); ?>
	<?php echo $this->form->getInput('email_address'); ?>
</li>
<li>
	<?php echo $this->form->getLabel('phone'); ?>
	<?php echo $this->form->getInput('phone'); ?>
</li>
<li>
	<?php echo $this->form->getLabel('age'); ?>
	<?php echo $this->form->getInput('age'); ?>
</li>
<li>
	<?php echo $this->form->getLabel('where_are_you_from'); ?>
	<?php echo $this->form->getInput('where_are_you_from'); ?>
</li>
<li>
	<?php echo $this->form->getLabel('previous_pinup'); ?>
	<?php echo $this->form->getInput('previous_pinup'); ?>
</li>
<li>
	<?php echo $this->form->getLabel('favorite_car'); ?>
	<?php echo $this->form->getInput('favorite_car'); ?>
</li>
<li>
	<?php echo $this->form->getLabel('favorite_pinup'); ?>
	<?php echo $this->form->getInput('favorite_pinup'); ?>
</li>
<li>
	<?php echo $this->form->getLabel('special_talents'); ?>
	<?php echo $this->form->getInput('special_talents'); ?>
</li>
<li>
	<?php echo $this->form->getLabel('why_you'); ?>
	<?php echo $this->form->getInput('why_you'); ?>
</li>
<li>
	<?php echo $this->form->getLabel('biggest_turn_on'); ?>
	<?php echo $this->form->getInput('biggest_turn_on'); ?>
</li>
<li>
	<?php echo $this->form->getLabel('biggest_turn_off'); ?>
	<?php echo $this->form->getInput('biggest_turn_off'); ?>
</li>
<li>
	<?php echo $this->form->getLabel('favorite_quote'); ?>
	<?php echo $this->form->getInput('favorite_quote'); ?>
</li>
<li style="display:none;">
	<?php echo $this->form->getLabel('created_by'); ?>
	<?php echo $this->form->getInput('created_by'); ?>
</li>
<li style="display:none;">
	<?php echo $this->form->getLabel('created_at'); ?>
	<?php echo $this->form->getInput('created_at'); ?>
</li>