<?php
/**
 * @version     1.0.0
 * @package     com_dirtygirlsubmissions
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Allan Cutler <allan@211enterprises.com> - http://www.211enterprises.com
 */
// no direct access
defined('_JEXEC') or die;
?>
<script type="text/javascript">
    function deleteItem(item_id){
        if(confirm("<?php echo JText::_('COM_DIRTYGIRLSUBMISSIONS_DELETE_MESSAGE'); ?>")){
            document.getElementById('form-dirtygirlsubmission-delete-' + item_id).submit();
        }
    }
</script>

<div class="items">
    <ul class="items_list">
<?php $show = false; ?>
        <?php foreach ($this->items as $item) : ?>

            
				<?php
						$show = true;
						?>
							<li>
								<a href="<?php echo JRoute::_('index.php?option=com_dirtygirlsubmissions&view=dirtygirlsubmission&id=' . (int)$item->id); ?>"><?php echo $item->first_name; ?></a>
								<?php
									if(JFactory::getUser()->authorise('core.delete','com_dirtygirlsubmissions')):
									?>
										<a href="javascript:deleteItem(<?php echo $item->id; ?>);"><?php echo JText::_("COM_DIRTYGIRLSUBMISSIONS_DELETE_ITEM"); ?></a>
										<form id="form-dirtygirlsubmission-delete-<?php echo $item->id; ?>" style="display:inline" action="<?php echo JRoute::_('index.php?option=com_dirtygirlsubmissions&task=dirtygirlsubmission.remove'); ?>" method="post" class="form-validate" enctype="multipart/form-data">
											<input type="hidden" name="jform[id]" value="<?php echo $item->id; ?>" />
											<input type="hidden" name="jform[first_name]" value="<?php echo $item->first_name; ?>" />
											<input type="hidden" name="jform[last_name]" value="<?php echo $item->last_name; ?>" />
											<input type="hidden" name="jform[email_address]" value="<?php echo $item->email_address; ?>" />
											<input type="hidden" name="jform[phone]" value="<?php echo $item->phone; ?>" />
											<input type="hidden" name="jform[age]" value="<?php echo $item->age; ?>" />
											<input type="hidden" name="jform[where_are_you_from]" value="<?php echo $item->where_are_you_from; ?>" />
											<input type="hidden" name="jform[previous_pinup]" value="<?php echo $item->previous_pinup; ?>" />
											<input type="hidden" name="jform[favorite_car]" value="<?php echo $item->favorite_car; ?>" />
											<input type="hidden" name="jform[favorite_pinup]" value="<?php echo $item->favorite_pinup; ?>" />
											<input type="hidden" name="jform[special_talents]" value="<?php echo $item->special_talents; ?>" />
											<input type="hidden" name="jform[why_you]" value="<?php echo $item->why_you; ?>" />
											<input type="hidden" name="jform[biggest_turn_on]" value="<?php echo $item->biggest_turn_on; ?>" />
											<input type="hidden" name="jform[biggest_turn_off]" value="<?php echo $item->biggest_turn_off; ?>" />
											<input type="hidden" name="jform[favorite_quote]" value="<?php echo $item->favorite_quote; ?>" />
											<input type="hidden" name="jform[image_1]" value="<?php echo $item->image_1; ?>" />
											<input type="hidden" name="jform[image_2]" value="<?php echo $item->image_2; ?>" />
											<input type="hidden" name="jform[image_3]" value="<?php echo $item->image_3; ?>" />
											<input type="hidden" name="jform[image_4]" value="<?php echo $item->image_4; ?>" />
											<input type="hidden" name="jform[image_5]" value="<?php echo $item->image_5; ?>" />
											<input type="hidden" name="jform[created_by]" value="<?php echo $item->created_by; ?>" />
											<input type="hidden" name="jform[created_at]" value="<?php echo $item->created_at; ?>" />
											<input type="hidden" name="option" value="com_dirtygirlsubmissions" />
											<input type="hidden" name="task" value="dirtygirlsubmission.remove" />
											<?php echo JHtml::_('form.token'); ?>
										</form>
									<?php
									endif;
								?>
							</li>

<?php endforeach; ?>
        <?php
        if (!$show):
            echo JText::_('COM_DIRTYGIRLSUBMISSIONS_NO_ITEMS');
        endif;
        ?>
    </ul>
</div>
<?php if ($show): ?>
    <div class="pagination">
        <p class="counter">
            <?php echo $this->pagination->getPagesCounter(); ?>
        </p>
        <?php echo $this->pagination->getPagesLinks(); ?>
    </div>
<?php endif; ?>


									<?php if(JFactory::getUser()->authorise('core.create','com_dirtygirlsubmissions')): ?><a href="<?php echo JRoute::_('index.php?option=com_dirtygirlsubmissions&task=dirtygirlsubmission.edit&id=0'); ?>"><?php echo JText::_("COM_DIRTYGIRLSUBMISSIONS_ADD_ITEM"); ?></a>
	<?php endif; ?>