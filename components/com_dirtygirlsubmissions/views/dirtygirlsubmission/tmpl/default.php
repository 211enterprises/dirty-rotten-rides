<?php
/**
 * @version     1.0.0
 * @package     com_dirtygirlsubmissions
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Allan Cutler <allan@211enterprises.com> - http://www.211enterprises.com
 */
// no direct access
defined('_JEXEC') or die;

//Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_dirtygirlsubmissions', JPATH_ADMINISTRATOR);
$canEdit = JFactory::getUser()->authorise('core.edit', 'com_dirtygirlsubmissions');
if (!$canEdit && JFactory::getUser()->authorise('core.edit.own', 'com_dirtygirlsubmissions')) {
	$canEdit = JFactory::getUser()->id == $this->item->created_by;
}
?>
<?php if ($this->item) : ?>

    <div class="item_fields">

        <ul class="fields_list">

            			<li><?php echo JText::_('COM_DIRTYGIRLSUBMISSIONS_FORM_LBL_DIRTYGIRLSUBMISSION_ID'); ?>:
			<?php echo $this->item->id; ?></li>
			<li><?php echo JText::_('COM_DIRTYGIRLSUBMISSIONS_FORM_LBL_DIRTYGIRLSUBMISSION_FIRST_NAME'); ?>:
			<?php echo $this->item->first_name; ?></li>
			<li><?php echo JText::_('COM_DIRTYGIRLSUBMISSIONS_FORM_LBL_DIRTYGIRLSUBMISSION_LAST_NAME'); ?>:
			<?php echo $this->item->last_name; ?></li>
			<li><?php echo JText::_('COM_DIRTYGIRLSUBMISSIONS_FORM_LBL_DIRTYGIRLSUBMISSION_EMAIL_ADDRESS'); ?>:
			<?php echo $this->item->email_address; ?></li>
			<li><?php echo JText::_('COM_DIRTYGIRLSUBMISSIONS_FORM_LBL_DIRTYGIRLSUBMISSION_PHONE'); ?>:
			<?php echo $this->item->phone; ?></li>
			<li><?php echo JText::_('COM_DIRTYGIRLSUBMISSIONS_FORM_LBL_DIRTYGIRLSUBMISSION_AGE'); ?>:
			<?php echo $this->item->age; ?></li>
			<li><?php echo JText::_('COM_DIRTYGIRLSUBMISSIONS_FORM_LBL_DIRTYGIRLSUBMISSION_WHERE_ARE_YOU_FROM'); ?>:
			<?php echo $this->item->where_are_you_from; ?></li>
			<li><?php echo JText::_('COM_DIRTYGIRLSUBMISSIONS_FORM_LBL_DIRTYGIRLSUBMISSION_PREVIOUS_PINUP'); ?>:
			<?php echo $this->item->previous_pinup; ?></li>
			<li><?php echo JText::_('COM_DIRTYGIRLSUBMISSIONS_FORM_LBL_DIRTYGIRLSUBMISSION_FAVORITE_CAR'); ?>:
			<?php echo $this->item->favorite_car; ?></li>
			<li><?php echo JText::_('COM_DIRTYGIRLSUBMISSIONS_FORM_LBL_DIRTYGIRLSUBMISSION_FAVORITE_PINUP'); ?>:
			<?php echo $this->item->favorite_pinup; ?></li>
			<li><?php echo JText::_('COM_DIRTYGIRLSUBMISSIONS_FORM_LBL_DIRTYGIRLSUBMISSION_SPECIAL_TALENTS'); ?>:
			<?php echo $this->item->special_talents; ?></li>
			<li><?php echo JText::_('COM_DIRTYGIRLSUBMISSIONS_FORM_LBL_DIRTYGIRLSUBMISSION_WHY_YOU'); ?>:
			<?php echo $this->item->why_you; ?></li>
			<li><?php echo JText::_('COM_DIRTYGIRLSUBMISSIONS_FORM_LBL_DIRTYGIRLSUBMISSION_BIGGEST_TURN_ON'); ?>:
			<?php echo $this->item->biggest_turn_on; ?></li>
			<li><?php echo JText::_('COM_DIRTYGIRLSUBMISSIONS_FORM_LBL_DIRTYGIRLSUBMISSION_BIGGEST_TURN_OFF'); ?>:
			<?php echo $this->item->biggest_turn_off; ?></li>
			<li><?php echo JText::_('COM_DIRTYGIRLSUBMISSIONS_FORM_LBL_DIRTYGIRLSUBMISSION_FAVORITE_QUOTE'); ?>:
			<?php echo $this->item->favorite_quote; ?></li>
			<li><?php echo JText::_('COM_DIRTYGIRLSUBMISSIONS_FORM_LBL_DIRTYGIRLSUBMISSION_IMAGE_1'); ?>:

			<?php 
				$uploadPath = 'administrator' . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'com_dirtygirlsubmissions' . DIRECTORY_SEPARATOR . 'uploads/submissions' . DIRECTORY_SEPARATOR . $this->item->image_1;
			?>
			<a href="<?php echo JRoute::_(JUri::base() . $uploadPath, false); ?>" target="_blank"><?php echo $this->item->image_1; ?></a></li>			<li><?php echo JText::_('COM_DIRTYGIRLSUBMISSIONS_FORM_LBL_DIRTYGIRLSUBMISSION_IMAGE_2'); ?>:

			<?php 
				$uploadPath = 'administrator' . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'com_dirtygirlsubmissions' . DIRECTORY_SEPARATOR . 'uploads/submissions' . DIRECTORY_SEPARATOR . $this->item->image_2;
			?>
			<a href="<?php echo JRoute::_(JUri::base() . $uploadPath, false); ?>" target="_blank"><?php echo $this->item->image_2; ?></a></li>			<li><?php echo JText::_('COM_DIRTYGIRLSUBMISSIONS_FORM_LBL_DIRTYGIRLSUBMISSION_IMAGE_3'); ?>:

			<?php 
				$uploadPath = 'administrator' . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'com_dirtygirlsubmissions' . DIRECTORY_SEPARATOR . 'uploads/submissions' . DIRECTORY_SEPARATOR . $this->item->image_3;
			?>
			<a href="<?php echo JRoute::_(JUri::base() . $uploadPath, false); ?>" target="_blank"><?php echo $this->item->image_3; ?></a></li>			<li><?php echo JText::_('COM_DIRTYGIRLSUBMISSIONS_FORM_LBL_DIRTYGIRLSUBMISSION_IMAGE_4'); ?>:

			<?php 
				$uploadPath = 'administrator' . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'com_dirtygirlsubmissions' . DIRECTORY_SEPARATOR . 'uploads/submissions' . DIRECTORY_SEPARATOR . $this->item->image_4;
			?>
			<a href="<?php echo JRoute::_(JUri::base() . $uploadPath, false); ?>" target="_blank"><?php echo $this->item->image_4; ?></a></li>			<li><?php echo JText::_('COM_DIRTYGIRLSUBMISSIONS_FORM_LBL_DIRTYGIRLSUBMISSION_IMAGE_5'); ?>:

			<?php 
				$uploadPath = 'administrator' . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'com_dirtygirlsubmissions' . DIRECTORY_SEPARATOR . 'uploads/submissions' . DIRECTORY_SEPARATOR . $this->item->image_5;
			?>
			<a href="<?php echo JRoute::_(JUri::base() . $uploadPath, false); ?>" target="_blank"><?php echo $this->item->image_5; ?></a></li>			
			
			<li><?php echo JText::_('COM_DIRTYGIRLSUBMISSIONS_FORM_LBL_DIRTYGIRLSUBMISSION_CREATED_BY'); ?>:
			<?php echo $this->item->created_by; ?></li>
			<li><?php echo JText::_('COM_DIRTYGIRLSUBMISSIONS_FORM_LBL_DIRTYGIRLSUBMISSION_CREATED_AT'); ?>:
			<?php echo $this->item->created_at; ?></li>


        </ul>

    </div>
    <?php if($canEdit): ?>
		<a href="<?php echo JRoute::_('index.php?option=com_dirtygirlsubmissions&task=dirtygirlsubmission.edit&id='.$this->item->id); ?>"><?php echo JText::_("COM_DIRTYGIRLSUBMISSIONS_EDIT_ITEM"); ?></a>
	<?php endif; ?>
								<?php if(JFactory::getUser()->authorise('core.delete','com_dirtygirlsubmissions')):
								?>
									<a href="javascript:document.getElementById('form-dirtygirlsubmission-delete-<?php echo $this->item->id ?>').submit()"><?php echo JText::_("COM_DIRTYGIRLSUBMISSIONS_DELETE_ITEM"); ?></a>
									<form id="form-dirtygirlsubmission-delete-<?php echo $this->item->id; ?>" style="display:inline" action="<?php echo JRoute::_('index.php?option=com_dirtygirlsubmissions&task=dirtygirlsubmission.remove'); ?>" method="post" class="form-validate" enctype="multipart/form-data">
										<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
										<input type="hidden" name="jform[first_name]" value="<?php echo $this->item->first_name; ?>" />
										<input type="hidden" name="jform[last_name]" value="<?php echo $this->item->last_name; ?>" />
										<input type="hidden" name="jform[email_address]" value="<?php echo $this->item->email_address; ?>" />
										<input type="hidden" name="jform[phone]" value="<?php echo $this->item->phone; ?>" />
										<input type="hidden" name="jform[age]" value="<?php echo $this->item->age; ?>" />
										<input type="hidden" name="jform[where_are_you_from]" value="<?php echo $this->item->where_are_you_from; ?>" />
										<input type="hidden" name="jform[previous_pinup]" value="<?php echo $this->item->previous_pinup; ?>" />
										<input type="hidden" name="jform[favorite_car]" value="<?php echo $this->item->favorite_car; ?>" />
										<input type="hidden" name="jform[favorite_pinup]" value="<?php echo $this->item->favorite_pinup; ?>" />
										<input type="hidden" name="jform[special_talents]" value="<?php echo $this->item->special_talents; ?>" />
										<input type="hidden" name="jform[why_you]" value="<?php echo $this->item->why_you; ?>" />
										<input type="hidden" name="jform[biggest_turn_on]" value="<?php echo $this->item->biggest_turn_on; ?>" />
										<input type="hidden" name="jform[biggest_turn_off]" value="<?php echo $this->item->biggest_turn_off; ?>" />
										<input type="hidden" name="jform[favorite_quote]" value="<?php echo $this->item->favorite_quote; ?>" />
										<input type="hidden" name="jform[image_1]" value="<?php echo $this->item->image_1; ?>" />
										<input type="hidden" name="jform[image_2]" value="<?php echo $this->item->image_2; ?>" />
										<input type="hidden" name="jform[image_3]" value="<?php echo $this->item->image_3; ?>" />
										<input type="hidden" name="jform[image_4]" value="<?php echo $this->item->image_4; ?>" />
										<input type="hidden" name="jform[image_5]" value="<?php echo $this->item->image_5; ?>" />
										<input type="hidden" name="jform[created_by]" value="<?php echo $this->item->created_by; ?>" />
										<input type="hidden" name="jform[created_at]" value="<?php echo $this->item->created_at; ?>" />
										<input type="hidden" name="option" value="com_dirtygirlsubmissions" />
										<input type="hidden" name="task" value="dirtygirlsubmission.remove" />
										<?php echo JHtml::_('form.token'); ?>
									</form>
								<?php
								endif;
							?>
<?php
else:
    echo JText::_('COM_DIRTYGIRLSUBMISSIONS_ITEM_NOT_LOADED');
endif;
?>
