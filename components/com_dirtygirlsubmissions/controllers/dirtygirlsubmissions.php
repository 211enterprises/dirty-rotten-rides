<?php
/**
 * @version     1.0.0
 * @package     com_dirtygirlsubmissions
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Allan Cutler <allan@211enterprises.com> - http://www.211enterprises.com
 */

// No direct access.
defined('_JEXEC') or die;

require_once JPATH_COMPONENT.'/controller.php';

/**
 * Dirtygirlsubmissions list controller class.
 */
class DirtygirlsubmissionsControllerDirtygirlsubmissions extends DirtygirlsubmissionsController
{
	/**
	 * Proxy for getModel.
	 * @since	1.6
	 */
	public function &getModel($name = 'Dirtygirlsubmissions', $prefix = 'DirtygirlsubmissionsModel')
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));
		return $model;
	}
}