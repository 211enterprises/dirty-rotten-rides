<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/


class JTheFactoryUploads
{
	/**
	 * Returns an array with accepted image file extensions
	 * The array is hard coded in the sources. 
	 * TODO - Make the Array confgurable in the INI file 
	 *
	 * @return array
	 */
    function getImageType()
    {
        return array("jpg","jpeg","gif","png","bmp");
    }
    /**
     * Verifies if a file name is an allowed image type (file extension) 
     *
     * @param string $filename
     * @return boolean
     */
    function isImageType($filename)
    {
        jimport('joomla.filesystem.file');
        $ext=strtolower(JFile::getExt($filename));
        return in_array($ext,JTheFactoryUploads::getImageType());

    }
    /**
     * Manages Image Uploads.
     * 	
     *		If the uploaded image has a non allowed image extension - the upload failes
     *		If destination folder is not specified, then the Joomla standard images folder will be used
     * 		if $with_user_subfolders is true then the images are uploaded in a subfolder named after the userid of the current user
     *        - this option requres user to be logged in
     * 		if $with_rename is true then the uploaded file will have the name specified in $newname, but with the original file extension
     *      if $with_resize is true then the image will be resized according to $maxheight and $maxwidth
     * 			
     *
     * @param string $varname 	the variable name from the HTML form 
     * @param string $destination_folder destination folder for the uploaded images
     * @param boolean $with_user_subfolders
     * @param boolean $with_rename
     * @param string $newname
     * @param boolean $with_resize
     * @param int $maxwidth
     * @param int $maxheight 
     * @return boolean returns true if the upload was succesfull
     */
    function imageUpload($varname='image',$destination_folder='',$with_user_subfolders=false,$with_rename=false,$newname='',$with_resize=false,$maxwidth=150,$maxheight=150)
    {

        if (!isset($_FILES[$varname]['name'])){
			return false;
        }
        $source_file=$_FILES[$varname]['tmp_name'];
        $orig_file_name=$_FILES[$varname]['name'];
        if (!JTheFactoryUploads::isImageType($orig_file_name)){
			JError::raiseWarning(21, JText::_('ADS_UPLOADED_FILE')." $orig_file_name ".JText::_('ADS_ERR_NOT_IMAGE_FILE'));
			return false;
        }
        if (!$newname) $newname=$orig_file_name;

        jimport('joomla.filesystem.file');
        if ($with_rename){
            $newname=JFile::getName(DS.$newname).".".JFile::getExt($orig_file_name);
        }

        if (!$destination_folder) $destination_folder=JPATH_ROOT.DS.'images';

        if ($with_user_subfolders){
            $my=&JFactory::getUser();
            if (!$my->id) return false;
            $destination_folder.=DS.$my->id;
        }

        require_once(JPATH_COMPONENT_SITE.DS.'thefactory'.DS.'front.images.php');
        if ($with_resize){
	        $imgTrans = new JTheFactoryImages();
            $imgTrans->sourceFile =$source_file;
            $imgTrans->targetFile = $destination_folder.DS.$newname;

        	$imgTrans->maintainAspectRatio=true;
        	$imgTrans->resizeToWidth = $maxwidth;
            $imgTrans->resizeToHeight = $maxheight;
            $imgTrans->resizeIfGreater=true;

            if($imgTrans->resize()) return $imgTrans->targetFile;
        	else return false;

        }else{
            return move_uploaded_file($source_file,$destination_folder.DS.$newname);
        }
    }
    
	function fileUpload()
	{
		//TODO - JText::_('upload fisiere non imagini')
	}

}


?>