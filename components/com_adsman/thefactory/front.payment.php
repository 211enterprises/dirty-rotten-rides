<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/


class JTheFactoryPaymentController extends JObject
{

    var $_db = null;
    var $payment_type = null;
    var $payment_obj = null;
    var $_request_array = null;
    var $_state = null;

    function __construct(&$db, &$d)
    {

        $this->_db = $db;
        $this->_request_array = $d;

        /*@var $payments JTheFactoryPayment*/
        $JTheFactoryPaymentLibClass = new JTheFactoryPaymentLib();
        $payments = $JTheFactoryPaymentLibClass->getInstance();
        //$payments	=&JTheFactoryPaymentLib::getInstance();
        $payments->loadAvailabeItems();

        $this->payment_items = $payments->price_plugins;
    }

    function &getInstance($db = null, $d = null)
    {

        static $instance;

        if (!isset ($instance)) {
            $database = JFactory::getDbo();
            if (!$db)
                $db = $database;

            if (!$d)
                $d = $_REQUEST;

            $instance = new JTheFactoryPaymentController($db, $d);
        }

        return $instance;
    }

    function front_checkout()
    {
        $this->payment_type = $this->GetCurrentPaymentSystem();

        if ($this->payment_type) {
            $classname = $this->payment_type;
            require_once(JPATH_COMPONENT_SITE . "/plugins/payment/$classname.php");
            $this->payment_obj = new $classname ($this->_db);
            $this->payment_obj->checkout($this->_request_array);
        }
    }

    function front_return()
    {
        /*@var $app JTheFactoryApp*/
        $japp =& JTheFactoryApp::getInstance();
        if ($japp->getIniValue('use_smarty_templates')) {

            $smarty = AdsUtilities::SmartyLoaderHelper();
            $itemname = JRequest::getVar("itemname", null);
            $pricing = $this->payment_items["price_" . $itemname];
            $itemdescription = $pricing->getDescription();
            $invoice = JRequest::getVar("invoice", null);

            $item_payment_status = null;

            if ($invoice) {
                // The Order Number (not order_id !)
                $payment_status = JRequest::getVar("payment_status", null);

                if (!class_exists("JTheFactoryPaymentLog"))
                    require_once(dirname(__FILE__) . DS . ".." . DS . "plugins" . DS . "payment" . DS . "payment_log.php");

                $log = new JTheFactoryPaymentLog($this->_db);
                $log->set('_tbl_key', "invoice");
                $log->load($invoice);

                $status = $log->status;
                $ipn_ip = $log->ipn_ip;

                if ($ipn_ip == "Payment Waiting")
                    $item_payment_status = "pending";

                else {

                    // If pending (IPN not runned yet for misc reasons )
                    if (($payment_status == "Completed" || $payment_status == "Processed" || $payment_status == "In-Progress" || $payment_status == "Pending") && $log->status != "ok") {

                        if ($payment_status == "Pending" || $payment_status == "In-Progress")
                            $item_payment_status = "pending";
                        else
                            $item_payment_status = "ok";

                    } elseif ($log->status == "ok") {

                        $item_payment_status = "ok";
                    }
                }
            }

            $smarty->assign('item_payment_status', $item_payment_status);
            $smarty->assign('itemname', $itemname);
            $smarty->assign('itemamount', JRequest::getVar("itemamount", null));
            $smarty->assign('itemprice', JRequest::getVar("itemprice", null));
            $smarty->assign('itemdescription', $itemdescription);
            $smarty->assign('currency', JRequest::getVar("currency", null));
            $smarty->display('plugins/t_payment_return.tpl');
        }
    }

    function front_cancel()
    {
        /*@var $app JTheFactoryApp*/
        $japp = JTheFactoryApp::getInstance();
        if ($japp->getIniValue('use_smarty_templates')) {
            $smarty = AdsUtilities::SmartyLoaderHelper();

            $smarty->display('plugins/t_payment_cancel.tpl');
        }

    }

    function front_checktask($task, $d)
    {
        if ($task == 'purchase') {
            $act = JArrayHelper::getValue($d, 'act');
            if (!$act)
                $act = 'checkout';

            $method = 'front_' . $act;

            if (method_exists($this, $method))
                $this->$method();
            return true;
        }

        if ($task == "payment") {
            $act = JArrayHelper::getValue($d, 'act');

            if ($act == "return") {
                $this->front_return();
                return true;
            }
            if ($act == "cancel") {
                $this->front_cancel();
                return true;
            }

        }

        if (count($this->payment_items))
            foreach ($this->payment_items as $k => $obj) {

                if ($obj->enabled && $obj->checktask($task, $d)) {
                    return true;
                }
            }

        return false;
    }

    function processTemplate($task, $d, &$smarty)
    {
        if (count($this->payment_items))
            foreach ($this->payment_items as $k => $obj) {
                if ($obj->enabled && method_exists($obj, "processTemplate"))
                    $obj->processTemplate($task, $d, $smarty);
            }
    }

    function processCron($daily = null)
    {
        if (count($this->payment_items))
            foreach ($this->payment_items as $k => $obj) {
                if ($obj->enabled && method_exists($obj, "CronProcesssing"))
                    $obj->CronProcesssing($daily);
            }

    }

    function showChoosePayment(&$pay_arr)
    {
        foreach ($pay_arr as $pay) {
            $pay->thumbnail = $pay->getLogo();
        }

        $post_var = JRequest::get('POST');

        if (isset($post_var['itemamount_package'])) {
            $itemamount = $post_var['itemamount_package'];
        }
        /*@var $app JTheFactoryApp*/
        $japp = JTheFactoryApp::getInstance();
        if ($japp->getIniValue('use_smarty_templates')) {
            $smarty = AdsUtilities::SmartyLoaderHelper();

            $smarty->assign('object_id', JRequest::getVar("id", null));
            $smarty->assign('payment_systems', $pay_arr);
            $smarty->assign('itemname', JRequest::getVar("itemname", null));
            $smarty->assign('itemamount', isset($itemamount) ? $itemamount : JRequest::getVar("itemamount", null));
            $smarty->assign('itemprice', JRequest::getVar("itemprice", null));
            $smarty->assign('currency', JRequest::getVar("currency", null));
            $smarty->assign('return_url', JRequest::getVar("return_url", null));

            $smarty->assign('nr_payperimage', JRequest::getVar("nr_payperimage", null));

            $smarty->assign('var_name1', JRequest::getVar("var_name1", null));
            $smarty->assign('var_name2', JRequest::getVar("var_name2", null));
            $smarty->assign('var_name3', JRequest::getVar("var_name3", null));
            $smarty->assign('var_name4', JRequest::getVar("var_name4", null));

            $smarty->assign('var_value1', JRequest::getVar("var_value1", null));
            $smarty->assign('var_value2', JRequest::getVar("var_value2", null));
            $smarty->assign('var_value3', JRequest::getVar("var_value3", null));
            $smarty->assign('var_value4', JRequest::getVar("var_value4", null));

            $smarty->display('plugins/t_payment_choose_gateway.tpl');
        } else {
            //No Template
            //TODO: no template add
        }
    }

    function GetCurrentPaymentSystem()
    {
        $database = $this->_db;
        $pay_type = JArrayHelper::getValue($this->_request_array, 'paymenttype', '');
        if (!$pay_type) {
            $payments =& JTheFactoryPaymentLib::getInstance();

            $pay_arr = $payments->listEnabledGateways();

            if (count($pay_arr) > 1) {
                JSession::set('prev_request', serialize($this->_request_array));
                JTheFactoryPaymentController::showChoosePayment($pay_arr);
                return;
            } else {
                $pay_type = $pay_arr[0]->classname;
            }
        }

        if (JSession::has('prev_request')) {
            $prev = JSession::get('prev_request');
            $this->_request_array = unserialize($prev);
            $this->_request_array['paymenttype'] = $pay_type;
            JSession::clear('prev_request');
        }
        return $pay_type;
    }

}

?>