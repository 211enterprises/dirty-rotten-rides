<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

class JTheFactorySmarty extends Smarty
{
	function display($tpl_name){
		$task = JRequest::getCmd('task');
		$database = JFactory::getDbo();
		/*@var $payment JTheFactoryPaymentController*/
		$payment=&JTheFactoryPaymentController::getInstance($database,$_REQUEST);		
		$payment->processTemplate($task, $_REQUEST,$this);
		parent::display($tpl_name);
	}

}




?>