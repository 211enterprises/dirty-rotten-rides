<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/


class FactoryFieldsFront{
	
	var $_db 	= null;
	
	
	function &getInstance($db=null)
    {
        static $instances;

		if (!isset ($instances["FactoryFieldsFront"]))
		{
            $database  = FactoryLayer::getDB();
            if (!$db) $db=$database;

            $instances["FactoryFieldsFront"] = new FactoryFieldsFront($db);
        }
        return $instances["FactoryFieldsFront"];
    }
	
	function FactoryFieldsFront($db){
		$this->_db = $db;
	}
	
	function setSQLFilters(&$filter_array){
        
		global $thefactory;
		
		if (isset($thefactory["searchable_fields"])) {
			$l = $thefactory["searchable_fields"];
		} else {
			$this->_db->setQuery("SELECT * FROM #__".APP_CFIELDS_PREFIX."_fields WHERE search = 1 ");
			$thefactory["searchable_fields"] = $l =  $this->_db->loadObjectList();
		}
		
		if (count($l))		
			foreach($l as $key => $search_item){
				
				$filter = FactoryLayer::getRequest($_REQUEST,$search_item->db_name);
				if($filter){
					$search_item_classname = "FieldType_".$search_item->ftype;
					$search_item_obj = new $search_item_classname;
					$filter_array[] = $search_item_obj->getSQLFilter( $search_item , $filter );
							
				}
			}
	}
	
	function setFiltersToState(&$filters, &$reset){
        global $JAds_aliases, $thefactory;
        
		if(isset($thefactory["searchable_fields"])){
			$l = $thefactory["searchable_fields"];
		}else{
			$this->_db->setQuery("SELECT * FROM #__".APP_CFIELDS_PREFIX."_fields WHERE search = 1 ");
			$thefactory["searchable_fields"] = $l =  $this->_db->loadObjectList();
		}
		
		$app	= JFactory::getApplication();

		if (count($l))
			foreach($l as $key => $search_item){
				
				$filter = FactoryLayer::getRequest($_REQUEST,$search_item->db_name);
				if($reset==1){
		            $app->setUserState(APP_EXTENSION.".$search_item->db_name", '');
				}else if($filter){
		            $filters[$search_item->db_name] = JRequest::getVar("$search_item->db_name", $app->getUserStateFromRequest(APP_EXTENSION.".$search_item->db_name","$search_item->db_name"));
				}
			}
		
	}
	
	function setHTMLFilters(&$filters, &$sfilters){
        global $JAds_aliases,$thefactory;
        
		if(isset($thefactory["searchable_fields"])){
			$l = $thefactory["searchable_fields"];
		}else{
			$this->_db->setQuery("SELECT * FROM #__".APP_CFIELDS_PREFIX."_fields WHERE search = 1 ");
			$thefactory["searchable_fields"] = $l =  $this->_db->loadObjectList();
		}

		$field_table = new FactoryFields($this->_db);
		
		if (count($l))
			foreach($l as $key => $search_item){

				if( isset($filters[$search_item->db_name])){
					
					$field_table->set('_tbl_key', "db_name");
			       // $field_table->_tbl_key= "db_name";
			        $field_table->load($search_item->db_name);
			        
					$filter_val = $filters[$search_item->db_name];

			        if($search_item->has_options=="1"){
						$options = $field_table->getOptions();

						if(isset($options) && count($options)>0 ) {

                            if (is_array($filters[$search_item->db_name])) {

                                $allowed_options = array();
                                $filter_val_array = array();

                                foreach ($options as $id=>$field) {
                                    $allowed_options[] = trim($field->option_name);
                                }
                            /*
                             * array
                             *   0 => string 'red' (length=3)
                             *   1 => string ' white' (length=6)
                             *
                             */
                               foreach ($filters[$search_item->db_name] as $k=>$value) {
                                   if (in_array($value,$allowed_options)) {
                                        $filter_val_array[] = $value;
                                   }
                               }

                               $filter_val = implode(',',$filter_val_array);

                            } else {
                                $filter_val = $options[$filters[$search_item->db_name]]->option_name;
                            }
                        }
			        }
						
					$sfilters[$search_item->db_name] = $search_item->name." - ".$filter_val;
					
				}
			}
		
	}
	
	
	function getHTMLFields($section, $owner_id, $values=null,$isNewAd){
		
		$field_table = new FactoryFields($this->_db);
		//if(!$values)
			$values = $field_table->getValues($section, $owner_id);

        $session	= JFactory::getSession();
        $l = array();

        $category = (int)JRequest::getVar('custom_fields_category');
        if (!$category && $section == "ads") {
            $category = JRequest::getInt( "category", $session->get('custom_fields_category') );
        }

        $allowed_fields_sql = "";
		$allowed_fields = '';

        if ($category) {
		    $this->_db->setQuery("SELECT fid FROM #__".APP_CFIELDS_PREFIX."_fields_categories WHERE cid = '{$category}'");
		    $allowed_fields = $this->_db->loadResultArray();

            if (count($allowed_fields)) {
                $allowed_fields_sql = " AND id IN (".implode(",",$allowed_fields).")";
            }
        }

        if (count($allowed_fields)) {
		    $this->_db->setQuery("SELECT * FROM #__".APP_CFIELDS_PREFIX."_fields WHERE page = '$section' AND status = 1 $allowed_fields_sql order by ordering");
		    $l =  $this->_db->loadObjectList();
        }
		$outputZ = array();
		$o = 0;

		if (count($l) && ( is_array($allowed_fields) || $category==0 || $section=="user_profile" ) )
		
			foreach($l as $k => $plugin){

				$field_class="FieldType_".$plugin->ftype;
				$fido=(int)$plugin->id;
			
				if (is_array($allowed_fields) && !@in_array( $fido, $allowed_fields ) )
					continue;
					
				if (class_exists($field_class)) {
					$pl = new $field_class();
					$pl->compulsory = $plugin->compulsory;
					$pl->owner_id = $owner_id;
					$pl->_attributes = $plugin->attributes;
										
					if(isset($plugin->validate_type ))
						$pl->validate_type = $plugin->validate_type;
					
					$db_name = $plugin->db_name;
					$outputZ[$o]["compulsory"] = $plugin->compulsory;
					$outputZ[$o]["validate_type"] = $plugin->validate_type;
					$outputZ[$o]["field_name"] = $plugin->name;
					$outputZ[$o]["field_id"] = $plugin->db_name;
					$outputZ[$o]["row_id"] = $plugin->db_name;
					$outputZ[$o]["help"] = $plugin->help;
					
					$field_table->load($plugin->id);
					if($pl->has_options){
						$pl->_options = $field_table->getOptions();  
					}

                    if ( ($values && ($values->$db_name || empty($values->$db_name))) || ($section=="user_profile" && !$values) ) {
                        if ($values){
                            $pl->value  = stripslashes($values->$db_name);
                            $outputZ[$o]["value"] = $pl->value;
                        }
					    $outputZ[$o]["field_html"] = $pl->display($plugin->db_name,$plugin->field_id,$plugin->style_attr, $plugin->css_class);
                    }
                    else {
                        if (!$values && $section=="ads" && $isNewAd ){
                            $outputZ[$o]["field_html"] = $pl->display($plugin->db_name,$plugin->field_id,$plugin->style_attr, $plugin->css_class);
                        }
                    }

					$o++;
				}
			}

		return $outputZ;
	}

	function getSearchFields($section, $owner_id) {
		
		global $thefactory;
		$field_table = new FactoryFields($this->_db);

        $l = array();

		$section_filter = "";
		if( $section )
			$section_filter = " page = '$section' AND ";

        $session	= JFactory::getSession();
        $category = (int)JRequest::getVar('custom_fields_category');

        //
        if (!$category && $section == "ads") {
            $category = JRequest::getInt( "cat", $session->get('custom_fields_category') );
        }

        $allowed_fields_sql = "";
        $allowed_fields = null;

        if ($category) {
            $this->_db->setQuery("SELECT fid FROM #__".APP_CFIELDS_PREFIX."_fields_categories WHERE cid = '{$category}'");
            $allowed_fields = $this->_db->loadResultArray();

            if (count($allowed_fields)) {
                $allowed_fields_sql = " AND id IN (".implode(",",$allowed_fields).")";
            }
        }
        //

		if (isset($thefactory["searchable_fields"][$section])) {
			$l = $thefactory["searchable_fields"][$section];
		} else {
            if (count($allowed_fields)) {
			    $this->_db->setQuery("SELECT * FROM #__".APP_CFIELDS_PREFIX."_fields WHERE $section_filter search = 1 AND status = 1 $allowed_fields_sql order by ordering");
			    $thefactory["searchable_fields"][$section] = $l =  $this->_db->loadObjectList();
            }
		}

		$outputZ = array();
		$o = 0;
		if (count($l))		
			foreach($l as $k => $plugin){
				$field_class="FieldType_".$plugin->ftype;
				if(class_exists($field_class)){
					$pl = new $field_class;
					$db_name = $plugin->db_name;
					if($plugin->field_id!="")
						$tz_field_id = $plugin->field_id;
					else
						$tz_field_id = $plugin->db_name;

					$outputZ[$o]["row_id"] = $tz_field_id;
						
					$outputZ[$o]["field_name"] = $plugin->name;
					$field_table->load($plugin->id);
					if($pl->has_options){
						$pl->_none_option = 1;
						$pl->_options = $field_table->getOptions();
					}
					
					$outputZ[$o]["field_html"] = $pl->display_search($plugin->db_name , $tz_field_id ,$plugin->style_attr, $plugin->css_class);
					$o++; 
				}
			}
		
		
		return $outputZ;
	}

	/**
	 * @since 1.3.0
	 */
	function getFilters($section, $owner_id){
		
		$field_table = new FactoryFields($this->_db);
		
		//$values = $field_table->getValues($section, $owner_id);
		
		$section_filter = "";
		if( $section )
			$section_filter = " page = '$section' AND ";
		
		$this->_db->setQuery("SELECT * FROM #__".APP_CFIELDS_PREFIX."_fields WHERE $section_filter search = 1 AND status = 1 order by ordering");
		$l =  $this->_db->loadObjectList();
		
		$outputZ = array();
		$o = 0;
		if (count($l))		
			foreach($l as $k => $plugin){
				$field_class="FieldType_".$plugin->ftype;
				if(class_exists($field_class)){
					//$o = $plugin->name;
					$pl = new $field_class;
					$db_name = $plugin->db_name;
					$outputZ[$plugin->db_name]["field_name"] = $plugin->name;
					$field_table->load($plugin->id);
					if($pl->has_options){
						$pl->_none_option = 1;
						$pl->_options = $field_table->getOptions();
					}
					
					$pl->_db_params["db_name"] = $plugin->db_name;
					$pl->_db_params["own_table"] = $plugin->own_table;
					
					$outputZ[$plugin->db_name]["field_html"] = $pl->display_filter($plugin->db_name,$plugin->field_id,$plugin->style_attr, $plugin->css_class);
					$o++; 
				}
			}
		
		
		return $outputZ;
	}
	
	/**
	 * Get Custom fields with values of an specified owner ID
	 *  
	*/
	function getCustomFields($section, $owner_id,$values=null){
		
		
		$field_table = new FactoryFields($this->_db);
		// TO DO: cache results on page Static load
		$this->_db->setQuery("SELECT * FROM #__".APP_CFIELDS_PREFIX."_fields WHERE page = '$section' AND status = 1 order by ordering");
		$l =  $this->_db->loadObjectList();
		
		if( $l && !$values ){
			$values = $field_table->getValues($section, $owner_id);
		}
		
		$outputZ = array();
		$o = 0;
		
		if (count($l))	
			foreach($l as $k => $plugin){
				
				$field_class="FieldType_".$plugin->ftype;
				
				if(class_exists($field_class)){
					
					$pl = new $field_class;
					$db_name = $plugin->db_name;
					
					$outputZ[$o]["db_name"] = $db_name;
					$outputZ[$o]["name"] = $plugin->name;
					$field_table->load($plugin->id);
 
 					if ($values)
						$pl->value = $values->$db_name;
					
					if($pl->has_options){
						$pl->_options = $field_table->getOptions();
					}
					
					$outputZ[$o]["value"] = $pl->getValue($db_name);
					
					$o++; 
				}
			}

			return $outputZ;
	}


	/**
	 * Listing usefull
	 *
	 * @return list
	 */
	function getCustomFieldList($section=null){
		
		static $static_custom_fields;
		$db = JFactory::getDbo();
		
		$result = null;
		if(!$section){
			if(!$static_custom_fields["all"]){
					$db->setQuery("SELECT * FROM #__".APP_CFIELDS_PREFIX."_fields WHERE status = 1 order by ordering");
					$result = $static_custom_fields["all"] =  $db->loadObjectList();
			}else 
				return $static_custom_fields["all"];
		}else
			if(!isset($static_custom_fields[$section])){
				$db->setQuery("SELECT * FROM #__".APP_CFIELDS_PREFIX."_fields WHERE page = '$section' AND status = 1 order by ordering");
				$result = $static_custom_fields[$section] =  $db->loadObjectList();
				
			}else $result = $static_custom_fields[$section];
		
		return $result;
	}
	
	function appendCustomFields(&$obj,$section=null){
		$fields = FactoryFieldsFront::getCustomFieldList($section=null);

		if(count($fields)>0)
			foreach ($fields as $fk => $fv){
				$fname = $fv->db_name;
				$fid = $fname."_id";
				$obj->$fname = null;
				$obj->$fid = null;
			}
	}
	

	/**
	 * Listing usefull
	 *
	 * Gets list of fields and ads in the joins list each left join with the custom fields option table
	 * and ads to select list the field to select (the field it self or the option name of the field value id)
	 * 
	 *
	 * @param : &joinArray  array of joins
	 * @param : &selectArray  array of select to be fields
	 * @param : section - if for single item (ad/ user/ auction )
	 * @param : alias list table_name => alias
	 */
	function getCustomFieldsJoin( &$joinArray, &$selectArray, $section=null, $alias_helper=array() ){

		$fields = FactoryFieldsFront::getCustomFieldList($section);
		
		if(count($fields)>0) {
		  foreach ($fields as $fk => $fv) {
			
			$field_class="FieldType_".$fv->ftype;
			
			if (class_exists($field_class)) {
				
				// field type has options
				$Plug = new $field_class;
				
				if ( isset($alias_helper[$fv->own_table]) ) {
					
					$join_table = $alias_helper[$fv->own_table];
					
					if ( $Plug->has_options && $Plug->multiple!=1 ) {
						
						$joomfish_id = $fv->db_name."_id";
						$selectArray[] = "  fi_$fv->db_name.id AS $joomfish_id ";
						$selectArray[] = "  fi_$fv->db_name.option_name AS $fv->db_name ";
						$joinArray[] = "LEFT JOIN #__".APP_CFIELDS_PREFIX."_fields_options as fi_$fv->db_name ON $join_table.$fv->db_name = fi_$fv->db_name.id ";
						
					} else {
						
						$selectArray[] = "$join_table.$fv->db_name";
						
					}
				}

			}
		  }
		}
	}

	/**
	 * If joomfish:
	 * 
	 * Get's all options for optioned type custom fields
	 * 
	 * Joomfish Utility For Listing - if joomfish installed the query will get the translations of the options in the 
	 * current language
	 *
	 */
	function getCustomFieldsOptions(){
		$database = JFactory::getDbo();
		$database->setQuery(
		"
		SELECT 
		 opt.*, f.db_name 
		FROM #__".APP_CFIELDS_PREFIX."_fields_options as opt
		LEFT JOIN #__".APP_CFIELDS_PREFIX."_fields AS f ON f.id = opt.fid
		");
		$cur = $database->loadObjectList();
		$custom_fields_translations = array();
		foreach ($cur as $cfield){
			$custom_fields_translations[$cfield->db_name][$cfield->id]=$cfield->option_name;
		}
		
		return $custom_fields_translations;
	}
	
	/**
	 * Joomfish Translation Utility
	 *
	 * @param item $row
	 * @param translations of options
	 */
	function translateRowOptions( &$row, $custom_fields_translations ) {
		
		$database = JFactory::getDbo();
		static $thef_opt_field_list;
		
		$database->setQuery("SELECT * FROM #__".APP_CFIELDS_PREFIX."_fields WHERE status = 1 AND has_options = 1 order by ordering");
		$field_list =  $database->loadObjectList();
		foreach ($field_list as $field){
			$fieldname = $field->db_name;
			$field_id = $fieldname."_id";
			if(isset($row->$fieldname) && isset($row->$field_id) ){
				 if( isset($custom_fields_translations[$fieldname]) && isset($custom_fields_translations[$fieldname][$row->$field_id]) ){
					$row->$fieldname = $custom_fields_translations[$fieldname][$row->$field_id];
				}
			}
			elseif(isset($row->$fieldname) && isset($row->$fieldname) ){
				if( isset($custom_fields_translations[$fieldname]) && isset($custom_fields_translations[$fieldname][$row->$fieldname]) ){
					$row->$fieldname = $custom_fields_translations[$fieldname][$row->$fieldname];
				}
			}
		}
		
	}
	
}


?>