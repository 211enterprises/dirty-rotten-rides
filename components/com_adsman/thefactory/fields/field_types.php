<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/


class FactoryFieldTypes{
	
	var $type_name 		= null;

	var $class_name 	= null;
	
	var $has_options	= null;

	/**
	 * @since 1.2.0
	 */
	var $multiple		= 0;
	
	// > sql params
	var $sql_type 		= null;
	
	var $length 		= null;
	// < sql params

	var $value		 	= null;
	var $owner_id		= null;
	
	var $_attributes 	= null;
	var $_params	 	= null;
	var $_options	 	= null;
	
	var $_properties	= null;
	
	var $_action		="list";
	
	function FactoryFieldTypes(){
		
	}
	
	
	function getAttributes(){
		
		$config_file = JPATH_COMPONENT_SITE.DS."thefactory".DS."fields".DS."fields.config.ini";
		$fields_config = parse_ini_file( $config_file,true);
		if (isset($fields_config[$this->class_name."_attributes"]) && $fields_config[$this->class_name."_attributes"]!="") {
			return $fields_config[$this->class_name."_attributes"];
		}else return null;
	}
	
	
	function getParams(){
		
		$config_file = JPATH_COMPONENT_SITE.DS."thefactory".DS."fields".DS."fields.config.ini";
		$fields_config = parse_ini_file( $config_file,true);
		if (isset($fields_config[$this->class_name."_params"]) && $fields_config[$this->class_name."_params"]!="") {
			return $fields_config[$this->class_name."_params"];
		}else return null;
	}
	
	function renderFieldType($db_name){
		
		global $thefactory;
		
		if($db_name){
			/**
			 * Load the Params ONCE on page!!
			 **/
			if(isset($thefactory[$db_name])){
				$object = $thefactory[$db_name];
			}else{
				$db = JFactory::getDbo();
				$statement = "SELECT db_name,ftype,attributes,params FROM #__".APP_CFIELDS_PREFIX."_fields WHERE db_name = '{$db_name}';";
				$db->setQuery($statement);
				$thefactory[$db_name] = $object = $db->loadObject();
			}
			
			$attrs = explode(";",$object->attributes);
			$attrs_array = array();
			
			foreach ($attrs as $m){
				$mm = explode("=",$m);
				if(isset($mm[1]))
					$attrs_array[$mm[0]] = $mm[1];
			}
			$this->_attributes = $attrs_array;
			
			$params = explode(";",$object->params);
			$params_array = array();
			
			foreach ($params as $m){
				$mm = explode("=",$m);
				if(isset($mm[1]))
					$params_array[$mm[0]] = $mm[1];
			}
			$this->_params = $params_array;
		}
		
	}
	
	function getOptions(){
		// abstract
		
	}
	
	/**
	 *
	 * Returns a field type selected value ; if list type -> get value
	 * 
	 * OBS: prefill value and options properties
	 *  
	 **/
	function getValue(){
		
		if($this->has_options){
			if($this->value && isset($this->_options[$this->value]))
				return $this->_options[$this->value]->option_name;
		}
		else
			return $this->value;
	}
	
	function getAllTypes($OnlyOptions_types=false){
		
		$OnlyOptions_types = false;
		
		$config_file = JPATH_COMPONENT_SITE.DS."thefactory".DS."fields".DS."fields.config.ini";
		$fields_config = parse_ini_file( $config_file,true);
		$field_list = $fields_config["field_list"];
		$html_plugin_list = explode(",", $field_list);
		
		$html_plugins = array();
		$c = 0;
		
		
		if( $OnlyOptions_types ){
			foreach($html_plugin_list as $p => $pl){
				$field_class="FieldType_".$pl;
				$html_plugins[$c] = new $field_class;
				
				if(isset($fields_config[$pl."_attributes"]))
					$html_plugins[$c]->attributes = $fields_config[$pl."_attributes"];
				if(isset($fields_config[$pl."_params"]))
					$html_plugins[$c]->_params = $fields_config[$pl."_params"];
				
				if( $html_plugins[$c]->has_options )
					$c++;
				else
					unset($html_plugins[$c]);
			}
		}else{
			foreach($html_plugin_list as $p => $pl){
				$field_class="FieldType_".$pl;				
				$html_plugins[$c] = new $field_class;
				if(isset($fields_config[$pl."_attributes"]))
					$html_plugins[$c]->_attributes = $fields_config[$pl."_attributes"];
					
				if(isset($fields_config[$pl."_params"]))
					$html_plugins[$c]->_params = $fields_config[$pl."_params"];
				$c++;
			}
		}
		
		
		return $html_plugins;

	}
	
	function prepareClassName(&$classname){
		
		$css_classes = array();
		if ($classname!="") {
			$css_classes[] = $classname;
		}
		
		if(isset($this->compulsory) && $this->compulsory==1)
			$css_classes[] = "required";
			
		if(isset($this->validate_type ) && $this->validate_type !="")
			switch (strtolower($this->validate_type)){
				case "email":
					$css_classes[] = "validate-email";
					break;
				case "number":
					$css_classes[] = "validate-numeric";
					break;
				case "url":
					$css_classes[] = "validate-url";
					break;
				default:break;	
			}
		if(is_array($css_classes))	
			$classname = implode(" ", $css_classes);
		
		return $classname;
	}
	
	function prepareAttributesString(){
		$attrs = $this->_attributes;
		$attr_string = "";
		$attrs_array = explode(";", $attrs);
			
		if($attrs_array)
			foreach ($attrs_array as $attribute){
				$ks = explode("=",$attribute);
				if(isset($ks[1]))
					$attr_string .= $ks[0].'="'.$ks[1].'"';
				
			}
		return $attr_string;
	}
	
	
	function getVar($name){
		return FactoryLayer::getRequest( $_REQUEST, $name );
	}
	
	function display_search($name, $id, $css_attributes="", $css_class="", $javascript_attributes="" ){

		if( !$this->has_options ){
			
			return $this->display($name, $id, $css_attributes, $css_class, $javascript_attributes );
			
		}else{
			$opts = array();
			if(count($this->_options))
				if(isset($this->_none_option) && $this->_none_option==1)
					$opts[0] = " -- ";
				foreach($this->_options as $k => $i ){
					$opts[$i->id] = $i->option_name;
				}
			
			return FactoryHTML::selectGenericList( $opts, $name, $this->value, 'class="'.$css_class.'" id="'.$id.'" style="'.$css_attributes.'"' );
		}

	}
	
	function getTemplateHTML($name, $value){
		return $value;
	}
	
	function getSQLFilter( $search_item, $filter ){
		
        global $JAds_aliases;
		$db = JFactory::getDbo();
        
		if (isset($search_item->type_name) && in_array($search_item->type_name,array("radioBox","selectList")))
			$comparsion=" = '".$db->getEscaped($filter)."'";
		else {
		    if ( is_array($filter) ) {
                foreach ( $filter as $k=>$val ) {
                    $comparsion[] =" LIKE '%".$db->getEscaped($val)."%'";
                }
                $prefix = (isset($JAds_aliases[$search_item->own_table])) ? ($JAds_aliases[$search_item->own_table]."."): "";
                $comparsion_str =  implode(' OR '.$prefix.$search_item->db_name, $comparsion);

                return "( ".
                    ((isset($JAds_aliases[$search_item->own_table])) ? ($JAds_aliases[$search_item->own_table]."."):"")
                    .$search_item->db_name.$comparsion_str
                    ." )";
            }
			else
			    $comparsion=" LIKE '%".$db->getEscaped($filter)."%'";
        }


		return " ".
			((isset($JAds_aliases[$search_item->own_table]))?($JAds_aliases[$search_item->own_table]."."):"")
			.$search_item->db_name.$comparsion;
		
	}
	
	/**
	 * @since 1.3.0
	 */
	function display_filter($name, $id, $css_attributes="", $css_class="", $javascript_attributes="onchange='document.adsForm.submit();'" ){
		
		$val=FactoryLayer::getRequest($_REQUEST,$name);
		
		if($this->has_options){
			$opts = array();
			if(count($this->_options))
				if(isset($this->_none_option) && $this->_none_option==1)
					$opts[''] = " -- ";
				foreach($this->_options as $k => $i ){
					$opts[$i->id] = $i->option_name;
				}
			
			return FactoryHTML::selectGenericList( $opts, $name, $val, 'class="'.$css_class.'" id="'.$id.'" '.$javascript_attributes.' style="'.$css_attributes.'"' );
			
		}else{
			$database  = FactoryLayer::getDB();
	
			$owner_table = $this->_db_params["own_table"];
			$dbname = $this->_db_params["db_name"];
			
			$database->setQuery( "select id,{$dbname} as option_name from {$owner_table} group by option_name " );
			$vals = $database->loadObjectList();
	
			$opts = array();
			$opts[''] = " -- ";
			if(count($vals))
				foreach($vals as $k => $i ){
					$opts[$i->option_name] = $i->option_name;
				}
			
			return FactoryHTML::selectGenericList( $opts, $name, $val, 'class="'.$css_class.'" id="'.$id.'" style="'.$css_attributes.'" '.$javascript_attributes.' ' );
		}

	}
	
}


/// Field Plugins

class FieldType_inputBox extends FactoryFieldTypes{
	
	var $type_name 		= "TextBox";

	var $class_name 	= "inputBox";
	
	var $has_options	= 0;
	
	var $sql_type 		= "varchar";
	
	var $length 		= 200;
	
	var $_attributes 	= null;
	
	var $_options	 	= null;
	
	function display($name, $id, $css_attributes="", $css_class="", $javascript_attributes="" ){
		if(!$id)
			$id = $name;
		$attributes = $this->prepareAttributesString();
		$css_class = $this->prepareClassName($css_class);
		return "<input type=\"text\" name=\"$name\" id=\"$id\" $attributes class=\"$css_class\" style=\"$css_attributes\" $javascript_attributes value=\"".$this->value."\"  />";
	}
	
	
} 

class FieldType_hidden extends FactoryFieldTypes{
	
	var $type_name 		= "Hidden field";

	var $class_name 	= "hidden";
	
	var $has_options	= 0;
	
	var $sql_type 		= "varchar";
	
	var $length 		= 200;
	
	var $_attributes 	= null;
	
	var $_options	 	= null;
	
	function display($name, $id, $css_attributes="", $css_class="", $javascript_attributes="" ){
		if(!$id)
			$id = $name;
		$attributes = $this->prepareAttributesString();
		$css_class = $this->prepareClassName($css_class);
		return "<input type=\"hidden\" name=\"$name\" id=\"$id\" $attributes class=\"$css_class\" style=\"$css_attributes\" $javascript_attributes value=\"".$this->value."\"  />". $this->value ;
	}
	
	function display_search($name, $id, $css_attributes="", $css_class="", $javascript_attributes="" ){
		$attributes = $this->prepareAttributesString();
		$css_class = $this->prepareClassName($css_class);
		return "<input type=\"text\" name=\"$name\" id=\"$id\" $attributes class=\"$css_class\" style=\"$css_attributes\" $javascript_attributes value=\"".$this->value."\"  />";
	}
	
} 

class FieldType_textArea extends FactoryFieldTypes{
	
	var $type_name 		= "TextArea";

	var $class_name 	= "textArea";
	
	var $has_options	= 0;
	
	var $sql_type 		= "text";
	
	var $_attributes 	= null;
	var $_options	 	= null;
	
	function display($name, $id, $css_attributes="", $css_class="", $javascript_attributes="" ){
		if(!$id)
			$id = $name;
		$css_class = $this->prepareClassName($css_class);
		
		$value = str_replace("<br />", PHP_EOL, $this->value );
		
		return "<textarea name=\"$name\" id=\"$id\" class=\"$css_class\" style=\"$css_attributes\" $javascript_attributes >{$value}</textarea>";
	}
	
	function display_search($name, $id, $css_attributes="", $css_class="", $javascript_attributes="" ){
		return $this->display($name, $id, $css_attributes, $css_class, $javascript_attributes );
	}
	
} 

class FieldType_radioBox extends FactoryFieldTypes{
	
	var $type_name 		= "Radio";

	var $class_name 	= "radioBox";
	
	var $has_options	= 1;
	
	var $sql_type 		= "varchar";
	
	var $length 		= 200;
	
	var $_attributes 	= null;
	var $_options	 	= null;
	
	function display($name, $id, $css_attributes="", $css_class="", $javascript_attributes="" ){
		$attributes = $this->prepareAttributesString();
		$css_class = $this->prepareClassName($css_class);
		
		if(!$id)
			$id = $name;

		$ret = "";
			
		if(count($this->_options)){
			foreach($this->_options as $k => $option){
				$oid = $id;
				//$oid = $id.$k;
				$selected = "  ";
				if($option->id==$this->value)
					$selected = " checked=\"checked\" ";
				$ret.= "<input type=\"radio\" class=\"$css_class\" style=\"$css_attributes\" name=\"{$name}\" id=\"$oid\" $selected value=\"".$option->id."\" > ".$option->option_name." \r\n";
			}
		}
		$ret .= "";
		
		return $ret;
	}
	
	function display_search($name, $id, $css_attributes="", $css_class="", $javascript_attributes="" ){
		return $this->display($name, $id, $css_attributes, $css_class, $javascript_attributes );
	}
	
} 

class FieldType_selectList extends FactoryFieldTypes{
	
	var $type_name 	= "Select Box";

	var $class_name 	= "selectList";
	
	var $has_options	= 1;

	var $sql_type 		= "int";
	
	var $length 		= 11;
	
	var $_attributes 	= null;
	var $_options	 	= null;
	
	function display($name, $id, $css_attributes="", $css_class="inputbox", $javascript_attributes="" ){
		
		$this->renderFieldType($name);
		
		if(!$id)
			$id = $name;
		$css_class = $this->prepareClassName($css_class);
		
		$opts = array();
		if( isset($this->_params["FirstOption"]) && $this->_params["FirstOption"]!="")
			$opts[''] = JText::_($this->_params["FirstOption"]);
			
		if(count($this->_options))
			foreach($this->_options as $k => $i ){
				$opts[$i->id] = $i->option_name;
			}
		
		return FactoryHTML::selectGenericList( $opts, $name, $this->value, 'class="'.$css_class.'" id="'.$id.'" style="'.$css_attributes.'"' );
	}
	
	function display_search($name, $id, $css_attributes="", $css_class="", $javascript_attributes="" ){
		$opts = array();
		if(count($this->_options))
			if(isset($this->_none_option) && $this->_none_option==1)
				$opts[0] = " -- ";
			foreach($this->_options as $k => $i ){
				$opts[$i->id] = $i->option_name;
			}
		
		return FactoryHTML::selectGenericList( $opts, $name, $this->value, 'class="'.$css_class.'" id="'.$id.'" style="'.$css_attributes.'"' );
	}
	
}

class FieldType_editor extends FactoryFieldTypes{
	
	var $type_name 		= "Editor";

	var $class_name 	= "editor";
	
	var $has_options	= 0;
	
	var $sql_type 		= "text";
	
	var $_attributes 	= null;
	var $_options	 	= null;
	
	function display($name, $id, $css_attributes="", $css_class="inputbox", $javascript_attributes="" ){
		
		if(!$id)
			$id = $name;
		$css_class = $this->prepareClassName($css_class);
		/** @var $editor JEditor */
		$editor = JFactory::getEditor();
		
		$attrs = explode(";",$this->_attributes);
		$attrs_array = array();

		foreach ($attrs as $m){

            if ($m != '') {
                $mm = explode("=",$m);
                $attrs_array[$mm[0]] = $mm[1];
            }
		}
		
		if(isset($attrs_array["width"]))
			$width = $attrs_array["width"];
		else
			$width = '100%';
		//$width= $height= $col= $row=

		return $editor->display($name, $this->value, $width, 60, 120,60, defined("ads_opt_post_editor_buttons")?(bool)ads_opt_post_editor_buttons:false );
	}

	function display_search($name, $id, $css_attributes="", $css_class="", $javascript_attributes="" ){
		
		return "<textarea name=\"$name\" id=\"$id\" >$this->value</textarea>";
	}
	

	function getVar($name){
		

		return FactoryLayer::getRequest( $_POST, $name, '');
		
	}
}


/**
 * @since:Custom Fields Management 1.2.0
 *
 */
class FieldType_checkbox extends FactoryFieldTypes{
   
   var $type_name       = "CheckBox";

   var $class_name    = "checkBox";
   
   var $has_options   = 1;
	
   var $multiple	  = 1;
   
   var $sql_type       = "varchar";
   
   var $length       = 200;
   
   var $_attributes    = null;
   var $_options       = null;
   
  function display($name, $id, $css_attributes="", $css_class="", $javascript_attributes="" ){
   
	if(!$id)
		$id = $name;
      $this->value = explode(",", $this->value);
      $css_class = $this->prepareClassName($css_class);
      $ret = "";
      if(count($this->_options))
         foreach($this->_options as $k => $option){
            $selected = "  ";
		    $option_name = trim($option->option_name);
            if( in_array($option_name,$this->value) )
               $selected = " checked=\"checked\" ";
			$ret.= "<input type=\"checkbox\" name=\"{$name}[]\" id=\"$id\" $selected value=\"".($option_name)."\" > ".$option_name;
         }
      return $ret;
   }

   function getVar($name){
     
      return implode(",",FactoryLayer::getRequest( $_REQUEST, $name ));
     
   }

    function display_search($name, $id, $css_attributes="", $css_class="", $javascript_attributes="" ){
        return $this->display($name, $id, $css_attributes, $css_class, $javascript_attributes );
    }
   
   function getValue(){
         return $this->value;
    }
   
}

/**
 * @since:Custom Fields Management 1.2.0
 *
 */
class FieldType_selectmultiple extends FactoryFieldTypes{
   
   var $type_name       = "SelectMultiple";

   var $class_name    = "selectmultiple";
   
   var $has_options   = 1;
	
   var $multiple	  = 1;
   
   var $sql_type       = "varchar";
   
   var $length       = 200;
   
   var $_attributes    = null;
   var $_options       = null;
   
  function display($name, $id, $css_attributes="", $css_class="", $javascript_attributes="" ){
   
      $this->value = explode(",", $this->value);
      $css_class = $this->prepareClassName($css_class);
		if(!$id)
			$id = $name;
      
      $ret = "<select name=\"{$name}[]\" multiple id=\"$id\" >";
      if(count($this->_options))
         foreach($this->_options as $k => $option){
            $selected = "  ";
            if( in_array($option->option_name,$this->value) )
               $selected = " selected=\"selected\" ";
			$ret.= "<option $selected value=\"".($option->option_name)."\" > ".$option->option_name."</option>";
         }
         
	  $ret.= "</select>";
      return $ret;
   }

   function display_search($name, $id, $css_attributes="", $css_class="", $javascript_attributes="" ){
		return $this->display($name, $id, $css_attributes, $css_class, $javascript_attributes );
	}
   
   function getVar($name){
   	
   	$requested_var = FactoryLayer::getRequest( $_REQUEST, $name );

   	if ($requested_var != null)
      return implode(",", FactoryLayer::getRequest( $_REQUEST, $name ));
    
    else 
    	return null;   
      
   }
   
   function getValue(){
         return $this->value;
   }
   
   function getSQLFilter( $search_item, $filter ){
		
        global $JAds_aliases;
		$db = JFactory::getDbo();

		$field_table = new FactoryFields($db);
		
		$field_table->set('_tbl_key', "db_name");
        $field_table->load($search_item->db_name);
		$options = $field_table->getOptions();
        
		foreach ( $filter as $k=>$val ) {
			$comparsion[] =" LIKE '%".$db->getEscaped($val)."%'";
			//$comparsion[] =" LIKE '%".$db->getEscaped($options[$filter]->option_name)."%'";
		}
		
		$prefix = (isset($JAds_aliases[$search_item->own_table])) ? ($JAds_aliases[$search_item->own_table]."."): "";
				
		$comparsion_str =  implode(' OR '.$prefix.$search_item->db_name, $comparsion);

		return "( ".
			((isset($JAds_aliases[$search_item->own_table])) ? ($JAds_aliases[$search_item->own_table]."."):"")
			.$search_item->db_name.$comparsion_str
			." )";
		
	}
	
}

/**
 * @since:Custom Fields Management 1.2.0
 *
 */
class FieldType_image extends FactoryFieldTypes{
	
	var $type_name 		= "Image";

	var $class_name 	= "image";
	
    var $sql_type       = "varchar";
   
    var $length       = 200;
   
	var $has_options	= 0;
	
	var $_attributes 	= null;
	var $_options	 	= null;
	
	function display($name, $id, $css_attributes="", $css_class="", $javascript_attributes="" ){
		
		$this->renderFieldType($name);
		if(isset($this->_params["upload_path"]))
			$upload_path = $this->_params["upload_path"];
		else
			$upload_path = 'images/stories';
			
			
		$css_class = $this->prepareClassName($css_class);
		
		$value = $this->value;
		if(!$id)
			$id = $name;

		if( $value ){
			$ret = "<img src='".JURI::root()."$upload_path/$value' width='150' /> <input type=\"hidden\" name=\"{$name}\" value=\"{$value}\"  />";
			if($this->compulsory==0)
				$ret .= "<input type='checkbox' value='1' name='{$name}_delete' /> ".JText::_("ADS_REMOVE");
			else{
				$ret .= "<br /> ".JText::_("ADS_REPLACE_ATTACHMENT")."  <br />";
				$ret .= "<br /> <input type=\"file\" name=\"{$name}\" id=\"{$id}\" style=\"{$css_attributes}\" />";
			}
			
			return $ret;
		}
		else
			return " <input type=\"file\" name=\"{$name}\" id=\"{$id}\" style=\"{$css_attributes}\" {$javascript_attributes} />";
	}
	
	function display_search($name, $id, $css_attributes="", $css_class="", $javascript_attributes="" ){
		
		return null;

	}
	
	function getVar($name,$defValue=null){

		$file = $_FILES[$name];
        $fname = $file['name'];
        
		$this->renderFieldType($name);
		
		if( isset($this->_params["upload_path"]) )
			$upload_path = $this->_params["upload_path"];
		else
			$upload_path = 'images/stories/';
		
        $delete_file = JRequest::getVar("{$name}_delete", 0);
        $owner_id = $this->owner_id;
        
        if ( $delete_file && $defValue!="" ){

        	unlink(JPATH_ROOT.DS.$upload_path. "/$defValue");
			return "";

		}
        
        if ( is_uploaded_file(@$file['tmp_name']) ){
        	
			jimport('joomla.filesystem.file');
			$ext = strtolower( JFile::getExt( $fname ) );
	        
	        /*
	        if (!$add_obj->isAllowedImage($ext)) {
	            die(adsman_err_not_allowed_ext.$fname);continue;
	        }
	        */
	        
			$file_name = trim($name).$owner_id."-custom.$ext";
	        
	        require_once(JPATH_COMPONENT_SITE.DS.'thefactory'.DS.'front.images.php');
			$imgTrans = new JTheFactoryImages();
			
	        $file_name = JFile::makesafe( $file_name );

	        $path = JPATH_ROOT.DS.$upload_path . "/$file_name";
			if (!file_exists(JPATH_ROOT.DS.$upload_path))
	        	mkdir(JPATH_ROOT.DS.$upload_path,755);
	        	
			$res = move_uploaded_file($file['tmp_name'], $path);
	        /*
		        if ( ads_opt_resize_if_larger && filesize($file['tmp_name']) > ads_opt_max_picture_size * 1024 ){
					$res = $imgTrans->resize_to_filesize($file['tmp_name'], $path, ads_opt_max_picture_size * 1024);
		        }else{
					$res = move_uploaded_file($file['tmp_name'], $path);
		        }
	        */
			return $file_name;
       }else {
       	
			return null;
       }
       
       // return old value
        

	}
	
    
   	function getTemplateHTML($name,$value){
   		
   		
		$this->renderFieldType($name);
		
		if(isset($this->_params["upload_path"])){
			$upload_path = $this->_params["upload_path"];
		}
		else
			$upload_path = 'images/stories';
			
		$css_atribs = array();	
		$css_atribs[] = "border:none;";
		$css_atribs[] = "vertical-align:middle;";
		
		if($this->_action == "details" ){
			if(isset($this->_attributes["detail_width"])){
				$width = $this->_attributes["detail_width"];
				$css_atribs[] = "max-width:{$width}px !important;";
			}
				
			if(isset($this->_attributes["detail_height"])){
				$height = $this->_attributes["detail_height"];
				$css_atribs[] = "max-height:{$height}px !important;";
			}
			
		}else{
			
			if(isset($this->_attributes["list_width"])){
				$width = $this->_attributes["list_width"];
				$css_atribs[] = "max-width:{$width}px !important;";
			}
				
			if(isset($this->_attributes["list_height"])){
				$height = $this->_attributes["list_height"];
				$css_atribs[] = "max-height:{$height}px !important;";
			}
		}
		
		JHTML::_("behavior.modal");
			
		if($css_atribs)
			$css = "style='".implode(";",$css_atribs)."' ";
			
		if( $value ){
			$ret = "<a class='modal' href='".JURI::root()."{$upload_path}/{$value}'><img src='".JURI::root()."{$upload_path}/{$value}' {$css} /></a>";
			return $ret;
		}
		return "";
  }

    
} 

class FieldType_colorPicker extends FactoryFieldTypes{
	
	var $type_name 		= "ColorPicker";

	var $class_name 	= "colorPicker";
	
	var $has_options	= 0;
	
	var $sql_type 		= "varchar";
	
	var $length 		= 200;
	
	var $_attributes 	= null;
	
	var $_options	 	= null;
	
	function display($name, $id, $css_attributes="", $css_class="", $javascript_attributes="" ){
		
		$jdoc = & JFactory::getDocument();
		JHTML::_("behavior.mootools");
		$js = JURI::root()."/components/".APP_EXTENSION."/thefactory/fields/assets/mooRainbow.js";
		$jdoc->addScript( $js );
		
		$jdoc->addStyleSheet(JURI::root()."components/".APP_EXTENSION."/thefactory/fields/assets/mooRainbow.css");
		
		$js_declaration = "
			window.addEvent('domready', function() {
				var r = new MooRainbow('myRainbow', {
					'startColor': [58, 142, 246],
					'imgPath' : '".JURI::root()."/components/".APP_EXTENSION."/thefactory/fields/assets/images/',
					'onChange': function(color) {
						$('{$name}_id').value = color.hex;
					}
				});
			});		
		";
		$jdoc->addScriptDeclaration( $js_declaration );
		$css_declaration = "
		.moor-cursor{
			background-image: url(".JURI::root()."/components/".APP_EXTENSION."/thefactory/fields/assets/images/moor_cursor.gif) !important; 
		}
		.moor-arrows {
			background-image: url(".JURI::root()."/components/".APP_EXTENSION."/thefactory/fields/assets/images/moor_arrows.gif) !important;
		}
		";
		$jdoc->addStyleDeclaration( $css_declaration );
		
		$attributes = $this->prepareAttributesString();
		$css_class = $this->prepareClassName($css_class);
		
		if(!$id)
			$id = $name;
		
		return "<img id=\"myRainbow\" src=\"".JURI::root()."images/edit_f2.png\" alt=\"[r]\" width=\"16\" height=\"16\" />
		<input id=\"{$name}_id\" name=\"{$name}\" type=\"text\" size=\"13\" value=\"".$this->value."\" />
		";
	}
	
 	function getTemplateHTML($name,$value){
   	
		$this->renderFieldType($name);
		
		return "<span style=\"background:".$value."\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
 	}
 	
 	function display_search(){
 		$ser = null;
 		return $ser;

 	}
	
} 

class FieldType_calendar extends FactoryFieldTypes{
	
	var $type_name 		= "Calendar";

	var $class_name 	= "calendar";
	
	var $has_options	= 0;
	
	var $sql_type 		= "varchar";
	
	var $length 		= 16;
	
	var $_attributes 	= null;
	
	var $_options	 	= null;
	
	function display($name, $id, $css_attributes="", $css_class="", $javascript_attributes="" ){
		
		$this->renderFieldType($name);
		
		if(isset($this->_params["format"]) && $this->_params["format"] != "" )
		{
			$format_custom = $this->_params["format"];
			
			switch ($format_custom) {
				case 'Y-m-d':
					$format = '%Y-%m-%d';
					break;
                case 'm/d/Y':
                    $format = '%m/%d/%Y';
                    break;
                case 'd/m/Y':
                    $format = '%d/%m/%Y';
                    break;
                case 'd.m.Y':
                    $format = '%d.%m.%Y';
                    break;
				default:
					$format = '%Y-%m-%d';
					break;	
			}
			
			
		}
		else
			$format = "%Y-%m-%d";
			
		$attributes = "";
		$css_class = $this->prepareClassName($css_class);
		
		if(!$id)
			$id = $name;
		
		$attribs = 	" $attributes class=\"$css_class\" style=\"$css_attributes\" $javascript_attributes ";
		if (is_array($attribs)) {
			$attribs = JArrayHelper::toString( $attribs );
		}

		JHTML::_('behavior.calendar');

        $list_calendar   = JHTML::_('adsdate.calendar', htmlspecialchars($this->value, ENT_COMPAT, 'UTF-8'), $name);

        return  $list_calendar;

	}
	
	
} 

class FieldType_datetime extends FactoryFieldTypes{
	
	var $type_name 		= "DateTime";

	var $class_name 	= "datetime";
	
	var $has_options	= 0;
	
	var $sql_type 		= "varchar";
	
	var $length 		= 20;
	
	var $_attributes 	= null;
	
	var $_options	 	= null;
	
	function display($name, $id, $css_attributes="", $css_class="", $javascript_attributes="" ){
		
		$this->renderFieldType($name);
		
		if(isset($this->_params["format"]))
			$format = $this->_params["format"];
		else
			$format = "%Y-%m-%d";
			
		$attributes = "";
		$css_class = $this->prepareClassName($css_class);
		
		if(!$id)
			$id = $name;
		
		if($this->value)	{
			$hm_index = strlen($this->value)-5;
			$dtvalue	= substr($this->value,0,$hm_index);
			$hm = substr($this->value,$hm_index);
			$hm_list = explode(":",$hm);
			$h = $hm_list[0];
			$m = $hm_list[1];
		}else{
			$dtvalue = "";
			$h = "00";
			$m = "00";
		}
		
		$attribs = 	" $attributes class=\"$css_class\" style=\"$css_attributes\" $javascript_attributes ";
		if (is_array($attribs)) {
			$attribs = JArrayHelper::toString( $attribs );
		}
		
        $out   = JHTML::_('adsdate.calendar', htmlspecialchars($dtvalue, ENT_COMPAT, 'UTF-8'), $name);

		$out .= " <input type=\"text\" size=\"1\" name=\"{$name}_hours\" value=\"{$h}\" /> : <input type=\"text\" size=\"1\" name=\"{$name}_minutes\" value=\"{$m}\" />  ";
		return $out;
	}
	
	function getVar($name,$defValue=null){
		$dtime = FactoryLayer::getRequest( $_REQUEST, $name );
		$dhours = FactoryLayer::getRequest( $_REQUEST, $name."_hours" );
		$dminutes = FactoryLayer::getRequest( $_REQUEST, $name."_minutes" );
		return $dtime." ".$dhours.":".$dminutes;
		
	}
	
	
} 
