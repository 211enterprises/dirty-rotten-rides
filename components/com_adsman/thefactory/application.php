<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

require_once(JPATH_COMPONENT_ADMINISTRATOR .DS.'thefactory'. DS ."application" . DS . "application.class.php" );

class JTheFactoryApp extends JTheFactoryApplication
{
   
    function __construct($configfile=null,$runfront=null)
    {
    	parent::__construct(null,1);
		require_once( $this->app_path_admin.DS.'payments'.DS.'payments.lib.php');
        require_once(JPATH_COMPONENT_SITE.DS.'thefactory'.DS.'front.payment.php');
    }	
    
    function checktask($task)
    {

    	if( parent::checktask($task)!==false )
    		return true;
    	
		$methoded='front_'.$task;
    	
        if (method_exists($this,$methoded)){
			return $this->$methoded();
        }
        return false;
    }
    
    function front_Purchase()
    {
        $db = JFactory::getDbo();
        /*@var $payment JTheFactoryPaymentController*/
        $payment=&JTheFactoryPaymentController::getInstance($db,$_REQUEST);
    	$payment->front_checkout();
	    return true;
    }
    
    function front_Payment()
    {
        $db = JFactory::getDbo();
        /*@var $payment JTheFactoryPaymentController*/
        $payment=&JTheFactoryPaymentController::getInstance($db,$_REQUEST);
    	$payment->front_checkout();
	    return true;
    }
}
?>