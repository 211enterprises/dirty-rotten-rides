<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

if(!class_exists('JTheFactoryHelper')) {
class JTheFactoryHelper extends JObject
{

    /*static function &getConfig()
    {
        static $config;

        if ($config) return $config;

        $MyApp =& JTheFactoryApplication::getInstance();
        $configfile = $MyApp->getIniValue('option_file');
        $classname = ucfirst(APP_PREFIX) . "Config";
        require_once(JPATH_COMPONENT_SITE . DS . $configfile);

        $config = new $classname();

        return $config;
    }*/

    /**
     * Reads a Remote HTTP file using Curl or fopen
     * Retruns a string containg the URL content
     * or false if an error occured
     *
     * @param string $uri the url that needs to be read
     * @param int $timeout timeout for the connect operation
     * @return mixed returns the string read or false if failed
     */
    function remote_read_url($uri, $timeout = 30)
    {

        if (function_exists('curl_init')) {
            $handle = curl_init();

            curl_setopt($handle, CURLOPT_URL, $uri);
            curl_setopt($handle, CURLOPT_MAXREDIRS, 5);
            curl_setopt($handle, CURLOPT_AUTOREFERER, 1);
            @curl_setopt($handle, CURLOPT_FOLLOWLOCATION, 1); //not in safe mode
            curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 10);
            curl_setopt($handle, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($handle, CURLOPT_TIMEOUT, $timeout);

            $buffer = @curl_exec($handle);

            curl_close($handle);
            return $buffer;
        } else if (ini_get('allow_url_fopen')) {
            $fp = @fopen($uri, 'r');
            if (!$fp) return false;
            stream_set_timeout($fp, $timeout);
            $linea = '';
            while ($remote_read = fread($fp, 4096))
                $linea .= $remote_read;
            $info = stream_get_meta_data($fp);
            fclose($fp);
            if ($info['timed_out']) return false;
            return $linea;
        } else {
            return false;
        }
    }

    function str_clean(& $string)
    {

        $string = strtolower($string);

        $customToReplace = array();
        $customReplacements = array();
        $string = str_replace($customToReplace, $customReplacements, $string);

        $aToReplace = array(" ", "/", "&", "�", "�", "�", "!", "$", "%", "@", "?", "#", "(", ")", "+", "*", ":", ";", "'", "\"");
        $aReplacements = array("-", "-", "and", "");
        $string = str_replace($aToReplace, $aReplacements, $string);

        $string = preg_replace('/[^\x{032}-\x{07F}]/', '-', $string);

        return $string;
    }

    function formattedDate2ISO($date, $format)
    {
        if ($format == 'Y-m-d') {
            return $date;
        }

        if ($format=='Y-d-m'){
              preg_match("/([0-9]+)-([0-9]+)-([0-9]+)/",$date,$matches);
              return $matches[1]."-".$matches[3]."-".$matches[2];
          }
          if ($format=='m-d-Y') {
              preg_match("/([0-9]+)-([0-9]+)-([0-9]+)/",$date,$matches);
              return $matches[3]."-".$matches[1]."-".$matches[2];
          }
        if ($format == 'd-m-Y') {
            preg_match("/([0-9]+)-([0-9]+)-([0-9]+)/", $date, $matches);
            return $matches[3] . "-" . $matches[2] . "-" . $matches[1];
        }

        if (ads_opt_date_format == 'm/d/Y') {
            if (preg_match("/([0-9]+)\/([0-9]+)\/([0-9]+)/", $date, $matches))
                return $matches[3] . "-" . $matches[1] . "-" . $matches[2];
        }

        if (ads_opt_date_format == 'd/m/Y') {
            if (preg_match("/([0-9]+)\/([0-9]+)\/([0-9]+)/", $date, $matches))
                return $matches[3] . "-" . $matches[2] . "-" . $matches[1];
        }

        if (ads_opt_date_format == 'd.m.Y') {
            if (preg_match("/([0-9]+)\.([0-9]+)\.([0-9]+)/", $date, $matches))
                return $matches[3] . "-" . $matches[2] . "-" . $matches[1];
        }

        if ($format == 'D, F d Y') {
            $d = strtotime($date);
            return date("Y-m-d", $d);
        }

        return $date;

    }

    /**
     * Returns arrays of arrays with fields positions
     *  array=(
     *       position1=array('field1', 'field2'),
     *    position2=array('field1', 'field2')
     *     ..
     * )
     * Ads only
     *
     * @return array
     */
    function loadPositionedFields(&$add, $tpl = null, $owner_table = null)
    {

        global $option;

        $T_app =& JTheFactoryApp::getInstance(null, true);

        $PO = &PositionsLib::getInstance();
        return $PO->loadPositionedFields($add, $tpl, $owner_table);
    }

}
}
class JTheFactoryLangHelper extends JObject
{
    /**
     * Chyrilik Transliteration
     *
     * @param  $string
     * @return string
     *
     * @since 1.5.3
     */
    function transliterate($string)
    {

        $string = JTheFactoryLangHelper::transliterate_cyr($string);
        $string = JTheFactoryLangHelper::transliterate_co($string);

        return $string;
    }

    function transliterate_cyr(&$str)
    {

        $niddle = array("а", "б", "в", "г", "д", "ђ", "е", "ж", "з", "и", "�?", "к", "л", "љ", "м", "н",
            "њ", "о", "п", "р", "�?", "т", "ћ", "�?", "ф", "х", "ц", "ч", "џ", "�?",
            "�?", "Б", "В", "Г", "Д", "Ђ", "Е", "Ж", "З", "�?", "�?", "К", "Л", "Љ", "М", "�?",
            "Њ", "О", "П", "Р", "С", "Т", "Ћ", "У", "Ф", "Х", "Ц", "Ч", "�?", "Ш");

        $replace = array("a", "b", "v", "g", "d", "d", "e", "z", "z", "i", "j", "k", "l", "lj", "m", "n", "nj", "o", "p",
            "r", "s", "t", "c", "u", "f", "h", "c", "c", "dz", "s",
            "A", "B", "B", "G", "D", "D", "E", "Z", "Z", "I", "J", "K", "L", "LJ", "M", "N", "NJ", "O", "P",
            "R", "S", "T", "C", "U", "F", "H", "C", "C", "DZ", "S"
        );

        return str_replace($niddle, $replace, $str);
    }

    function transliterate_co($string)
    {

        // &
        $string = str_replace("&", "a", $string);

        // TMJ MOD
        $string = urldecode("$string");

        if (function_exists('iconv')) {

            /*
             1.6.x get Locale
             $lang =& JFactory::getLanguage();
             setlocale(LC_ALL, $lang->getLocale());
             */
            $string = iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', $string);
            $string = htmlentities($string);
        } else {
            $string = htmlentities(utf8_decode($string));
            $string = preg_replace(
                array('/&szlig;/', '/&(..)lig;/', '/&([aouAOU])uml;/', '/&(.)[^;]*;/'),
                array('ss', "$1", "$1" . 'e', "$1"),
                $string);
        }

        $cw = array('^a', '^i', '^I', '^A', 'quot;', '&', '$', '@', '%3C', '%3E', '?', '%', 'amp', 'gt;', 'lt;', '+', '*');
        $cw2 = array('a', 'i', 'I', 'A', "", "", "\$", "", "", "", "\?", "\%", "", "", "", " ", '\*');
        $string = str_replace($cw, $cw2, $string);

        return $string;
    }

}

?>