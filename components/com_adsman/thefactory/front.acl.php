<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

class JTheFactoryACL extends JObject
{
    var $public_tasks=array();
    var $anon_task=array();
    var $group_mapping=array();
    var $use_group_acl=true;

     function __construct($public_tasks,$anon_tasks,$group_mapping,$use_group_acl)
     {
     		$this->public_tasks=explode(',',$public_tasks);
     		$this->anon_task=explode(',',$anon_tasks);
     		$this->group_mapping=$group_mapping;
     		$this->use_group_acl=$use_group_acl;
     		     		
     }

     function check_acl($task)
     {
         $mainframe = JFactory::getApplication();
         $my =& JFactory::getUser();

         //check if this task is bypassed by component's permission settings
		 if($this->checkComponentPermissions($task)){
			return true;
         }
         else{
       		JError::raiseWarning(1,JText::_(answ_group_has_no_access));
       		if(!$my->id)
			   $mainframe->redirect(JRoute::_('index.php?option=com_user&view=login'));
   			else{
   				$mainframe->redirect(JURI::root());
   			}
         	return false;
         }
         //to do: #_opt_permission_$task_user=userid1,userid2
         //to do: #_opt_permission_$task_group=gid1,gid2

        if (!$this->checkPublicTask($task)){

            JError::raiseWarning(1,JText::_(answ_have_to_login));
            $mainframe->redirect(JURI::root());

	        return false;
        }

        if (!$this->checkProfile($task)){
            JError::raiseWarning(101,JText::_(answ_have_to_fill_profile));
            JTheFactoryUserProfile::redirectToProfile();
            return false;
        }
        if(!$this->checkGroupACL($task)){
            JError::raiseError(403,JText::_(answ_group_has_no_access));
	        return false;
        }
     }

	//first check if the task's permissions are overrided by component's settings
     function checkComponentPermissions($task){
     	
		$my =& JFactory::getUser();
		$acl =& JFactory::getACL();
		$Tapp =& JTheFactoryApp::getInstance();

		$permission_constant = $Tapp->getIniValue('prefix').'_opt_permission_'.strtolower($task);

		if(defined($permission_constant)){
			switch(constant($permission_constant)){
    			case '0':
    				return true;
   				break;
   				case '1':
   					//group child of Public Backend
   					if($acl->is_group_child_of((int)$my->gid,30)){
   						return true;
   					}
		 			//group child of Public Frontend
				   	else if($acl->is_group_child_of((int)$my->gid,29)){
   						return true;
   					}
   				break;
   				case '2':
   					//group child of Public Backend
   					if($acl->is_group_child_of((int)$my->gid,30)){
   						return true;
   					}
   				break;
   				default:
   					return false;
   				break;
    		}
    		return false;
    	}
    	return true;
     }
     
     /**
     *      Check if User has to be logged on for this task
     *
     * */
     function checkPublicTask($task)
     {
        $my=&JFactory::getUser();

        if (!$my->id && !in_array($task,$this->public_tasks)){
        	return false;
        }

        return true;
     }
    /**
     *  Check if user has to have a profile in order to perform this task
     *
     */
    function checkProfile($task)
    {
        /*@var $app JTheFactoryApp */
        $Tapp=&JTheFactoryApp::getInstance();
        if (!$Tapp->getIniValue('use_extended_profile')) return true;//Does not use
        if (in_array($task,$this->anon_task) || in_array($task,$this->public_tasks)) return true; //can be done without profile

        $user_fact=&JTheFactoryUserProfile::getInstance();
        if ($user_fact->hasProfile()) return true;

        return false;
    }
    function checkGroupACL($task)
    {
    	if (!$this->use_group_acl) return true;
        $map='all';
        if (isset($this->group_mapping[$task])) $map=$this->group_mapping[$task];

        if ($map=='all') return true;

        $user_fact=&JTheFactoryUserProfile::getInstance();
        $user_groups=$user_fact->getUserGroups();
        if(!count($user_groups)) return false;
        $map_groups=explode(',',$map);

        $intersect=array_intersect($map_groups,$user_groups);
        if (count($intersect)) return true;

        return false;

    }
}


?>