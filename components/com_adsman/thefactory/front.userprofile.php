<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/


class JTheFactoryUserProfile extends JObject
{
    var $tablename 		= null;
    var $field_userid	= null;
    var $cb_fields		= null;

    function __construct() {
    	
		$this->tablename = constant("APP_".APP_PREFIX."_PROFILE");
    	
    }

    function &getInstance() {
        static $instance;

        if ( !isset ($instance))
        {
            $instance = new JTheFactoryUserProfile();
        }
        return $instance;
    }
    
    function getUserProfile(&$user_obj, $profile_mode){
    	
		$tbl = constant("APP_".APP_PREFIX."_PROFILE");
    	$uid = (int)$user_obj->userid;
    	
    	
		if( !$profile_mode || $profile_mode=="ads" || $profile_mode=="component" ){
			$SelectCols = array();
			$JoinList = array();
					
			$SelectCols[]	= " 1 as has_profile";
			$Joom_User = & JTable::getInstance("user");
			$Joom_User->load($uid);
			
			$user_obj->username = @$Joom_User->username;
			$user_obj->name  = @$Joom_User->name;
			$user_obj->surname  = @$Joom_User->surname;
			
			$user_obj->country_id = null;
			
			$SelectCols[] = " ptable.country as country_id,co.name as `country` "; 
			$JoinList[] = " LEFT JOIN #__ads_country AS co ON ptable.country=co.id ";
			
			$user_obj->has_profile = 0;
			$user_obj->loadData( $uid, $SelectCols, $JoinList ,array($tbl => "ptable"));
			
		}else{
			
			$int_modes = array("cb"=>"cb","love"=>"lovefactory");
			$cmode = $int_modes[$profile_mode];
			$cname = $cmode."_integration";
			$cobj = new $cname;
			$cobj->getProfile($user_obj);
			
	    }

    }
    
    function getListJoin($mode,$exclusive=1,&$JoinList,&$SelectCols){
    	
		$tbl = constant("APP_".APP_PREFIX."_PROFILE");
		
    	
		if($mode=="" || $mode=="ads" || $mode=="component" ){
			
			$cb_fieldmap = JTheFactoryIntegration::integrationFields();
			foreach ($cb_fieldmap as $c ){
				$SelectCols[] = " prof.{$c}";
			}
			
	    	if($exclusive==1){
	        	$JoinList[] = " RIGHT JOIN `".$tbl."` AS `prof` ON `u`.`id` = `prof`.`userid` " ;
	    	}else{
	        	$JoinList[] = " LEFT JOIN `".$tbl."` AS `prof` ON `u`.`id` = `prof`.`userid` " ;
	    	}
	    	
	    	/*
				$SelectCols[] = " prof.country as country_id, co.name as `country` "; 
				$JoinList[] = " LEFT JOIN #__ads_country AS co ON prof.country=co.id ";
			*/
	    	
		}else{
			$int_modes = array("cb"=>"cb","love"=>"lovefactory");
			$cmode = $int_modes[$mode];
			$cname = $cmode."_integration";
			$cobj = new $cname;
			
			$cobj->getListJoins($JoinList,$SelectCols,$exclusive);
			
		}
    	
    	return $JoinList;
    }
    
    /**
     * Returns True or False if the Current User has an additional Profile
     *
     * @return boolean
     */
    function hasProfile($mode="")
    {
        $my = JFactory::getUser();

		if (!$my->id) return false;
		
		if($mode=="" || $mode=="ads" || $mode=="component" ){
			$tbl = constant("APP_".APP_PREFIX."_PROFILE");
	        $my=&JFactory::getUser();
			
	        $db = JFactory::getDbo();
	        $db->setQuery("select count(*) as no from `".$tbl."` where `userid`='{$my->id}'");
	        $r = $db->loadObject();
	        return (bool)$r->no;
            
		}elseif( $mode=="cb" && CB_DETECT ){
			return cb_integration::hasProfile();
		}elseif( $mode=="love" && LOVE_DETECT ){
			return lovefactory_integration::hasProfile();
		}
    }
    
    /**
     * Returns the Usergroups (array of groups) 
     *
     * @return array of groups
     */
    function getUserGroups()
    {
        $my = JFactory::getUser();
        if ($my->id) return false;

        $acl = JFactory::getACL();

        $user_groups=$acl->acl_get_groups('users',$my->id);
        if (!is_array($user_groups)) return false;
        return $user_groups;
    }
    
    function setRedirectToProfile($mode, $id = null )
    {
    	$Itemid = JRequest::getVar('Itemid', 0, 'REQUEST', 'integer');
		$url_dir = null;
		if($mode){
			switch ($mode){
				case "cb":{
					/*
					// get ItemId CB
				    require_once(JPATH_ADMINISTRATOR.DS."components".DS."com_comprofiler".DS."plugin.foundation.php");
					cbimport( 'cb.database' );
					$Iid = getCBprofileItemid();
					*/
					if($id)
						$url_dir = JURI::root()."index.php?option=com_comprofiler&user=$id&task=userprofile&Itemid=$Itemid";
					else 
						$url_dir = JURI::root()."index.php?option=com_comprofiler&Itemid=$Itemid";
					break;
				}
				case "love":{
					if($id)
						$url_dir = JURI::root()."index.php?option=com_lovefactory&id=$id&view=profile&Itemid=$Itemid";
					else	
						$url_dir = JURI::root()."index.php?option=com_lovefactory&view=profile&Itemid=$Itemid";
					break;
				}
				default:{
					
					break;
				}
			}
			
		}
		return $url_dir;

    }

}


?>