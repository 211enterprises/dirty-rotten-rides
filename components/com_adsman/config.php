<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

define('ADDSMAN_DEBUG', 0 );

define('ADS_COMPONENT_PATH', JPATH_ROOT.DS."components".DS."com_adsman");

define('ADDSMAN_TYPE_PUBLIC',0 );
define('ADDSMAN_TYPE_PRIVATE',1 );
define('ADDSMAN_TYPE_ALL',2 );

   //INITIAL define('ADDSMAN_IMAGE_PATH',JPATH_ROOT.DS."components".DS."com_adsman".DS."images".DS );
    define('ADDSMAN_IMAGE_PATH',JPATH_ROOT.DS.'media'.DS.'com_adsman'.DS.'images'.DS);
    // define('ADS_PICTURES_PATH',JPATH_ROOT.DS.'media'.DS.'com_adsman'.DS.'images'.DS);
    define('ADSMAN_PICTURES', JURI::root().'/media/com_adsman/images');

	
define('add_opt_allowed_ext','JPEG,JPG,GIF,PNG');

define('ADSMAN_PAYMENT_SUBJECT','Payment notification');
define('ADSMAN_PAYMENT_CONTENT','Dear %USERNAME%. <br /> New payment was made for %AMOUNT% %CURRENCY%. <br /><br /> The Ads Team');

define("ads_allowed_attachments", 'doc,txt,zip,rar,pdf' );



// backend use only
global $ads_opt_gallery_objects;
// if just created new gallery plugin named gl_demo add it in this array so you can select it in admin / settings 
$ads_opt_gallery_objects = array(
	"lytebox" => "JQuery Lytebox",
	"moo" => "MooTools",
	"simple_carousel" => "Simple Carousel"
);

?>