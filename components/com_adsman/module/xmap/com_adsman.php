<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.2.1
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined( '_JEXEC' ) or die( 'Restricted access.' );


class xmap_com_adsman{

	/*
	* This function is called before a menu item is printed. We use it to set the
	* proper uniqueid for the item and indicate whether the node is expandible or not
	*/
	function prepareMenuItem(&$node) {
		
		$link_query = parse_url( $node->link );
		parse_str( html_entity_decode($link_query['query']), $link_vars);
		
		$view = JArrayHelper::getValue($link_vars,'view','');
		$task = JArrayHelper::getValue($link_vars,'task','');
		
		if($view == "adsman"){
			
		if ( $task == 'details') {
			$id = intval(JArrayHelper::getValue($link_vars,'id',0));
			if ( $id ) {
				$node->uid = 'com_adsmani'.$id;
				$node->expandible = false;
			}
		} elseif ( $task == 'listadds') {
			
            $node->uid = 'com_adsmanlist';
            $node->expandible = true;
		}
			
		}
	}

	function getTree( &$xmap, &$parent, &$params) {
		
		$link_query = parse_url( $parent->link );
		parse_str( html_entity_decode($link_query['query']), $link_vars );
		$view = JArrayHelper::getValue($link_vars,'view',0);
		$task = JArrayHelper::getValue($link_vars,'task',"");
		if($task!="listadds")
			return;
		

		$menu =& JSite::getMenu();
		$menuparams = $menu->getParams($parent->id);

		$include_links = JArrayHelper::getValue( $params, 'include_links',1,'' );
		$include_links = ( $include_links == 1
				  || ( $include_links == 2 && $xmap->view == 'xml')
				  || ( $include_links == 3 && $xmap->view == 'html')
				  ||   $xmap->view == 'navigator');
		$params['include_links'] = $include_links;

		$priority = JArrayHelper::getValue($params,'cat_priority',$parent->priority,'');
		$changefreq = JArrayHelper::getValue($params,'cat_changefreq',$parent->changefreq,'');
		if ($priority  == '-1')
			$priority = $parent->priority;
		if ($changefreq  == '-1')
			$changefreq = $parent->changefreq;

		$params['cat_priority'] = $priority;
		$params['cat_changefreq'] = $changefreq;

		$priority = JArrayHelper::getValue($params,'link_priority',$parent->priority,'');
		$changefreq = JArrayHelper::getValue($params,'link_changefreq',$parent->changefreq,'');
		if ($priority  == '-1')
			$priority = $parent->priority;

		if ($changefreq  == '-1')
			$changefreq = $parent->changefreq;

		$params['link_priority'] = $priority;
		$params['link_changefreq'] = $changefreq;

		$params['limit'] = '';
		$limit = JArrayHelper::getValue($params,'max_links','','');

		if ( intval($limit) && $xmap->view != 'navigator' ) {
			$params['limit'] = ' LIMIT '.$limit;
		}

		xmap_com_adsman::getAdsTree($xmap, $parent, $params, $link_vars );

	}

	function getAdsTree ( &$xmap, &$parent, &$params, $link_vars) {

		$view = JArrayHelper::getValue($link_vars,'view',0);
		$task = JArrayHelper::getValue($link_vars,'task',"");
		
		$db = &JFactory::getDBO();

        if ($task=="listadds") {

        	// ads list
            $query = ' SELECT a.id,a.title'.
                     ' FROM #__ads a '.
                     ' ORDER by a.title ';

            $db->setQuery($query);
            $cats = $db->loadObjectList();
            $xmap->changeLevel(1);
            foreach($cats as $cat) {
                $node = new stdclass;
                $node->id   = $parent->id;
                $node->uid  = $parent->uid .'c'.$cat->id;
                $node->name = $cat->title;
                $node->link = 'index.php?option=com_adsman&view=adsman&task=details&id='.$cat->id;
                $node->priority   = $params['cat_priority'];
                $node->changefreq = $params['cat_changefreq'];
                $node->expandible = false;
                $xmap->printNode($node);
            }
            
            $xmap->changeLevel(-1);

        	// ads by users
            $query = ' SELECT u.id ,u.username '.
                     ' FROM #__users u ';

            $db->setQuery($query);
            $users = $db->loadObjectList();
            $xmap->changeLevel(1);
            foreach($users as $us) {
                $node = new stdclass;
                $node->id   = $parent->id;
                $node->uid  = $parent->uid .'c'.$us->id;
                $node->name = $us->username;
                $node->link = 'index.php?option=com_adsman&task=listadds&userid='.$us->id;
                $node->priority   = $params['cat_priority'];
                $node->changefreq = $params['cat_changefreq'];
                $node->expandible = false;
                $xmap->printNode($node);
            }
            
            $xmap->changeLevel(-1);

        	// ads by tags
            $query = ' SELECT tagname '.
                     ' FROM #__ads_tags GROUP BY tagname';

            $db->setQuery($query);
            $tags = $db->loadObjectList();
            $xmap->changeLevel(1);
            foreach($tags as $us) {
                $node = new stdclass;
                $node->id   = $parent->id;
                $node->uid  = $parent->uid .'c'.$us->tagname;
                $node->name = $us->tagname;
                $node->link = 'index.php?option=com_adsman&task=listadds&tag='.$us->tagname;
                $node->priority   = $params['cat_priority'];
                $node->changefreq = $params['cat_changefreq'];
                $node->expandible = false;
                $xmap->printNode($node);
            }
            
            $xmap->changeLevel(-1);

        	// ads by categories
            $query = ' SELECT catname , id  '.
                     ' FROM #__ads_categories WHERE status=1 GROUP BY catname';

            $db->setQuery($query);
            $tags = $db->loadObjectList();
            $xmap->changeLevel(1);
            foreach($tags as $us) {
                $node = new stdclass;
                $node->id   = $parent->id;
                $node->uid  = $parent->uid .'c'.$us->catname;
                $node->name = $us->catname;
                $node->link = 'index.php?option=com_adsman&task=listadds&cat='.$us->id;
                $node->priority   = $params['cat_priority'];
                $node->changefreq = $params['cat_changefreq'];
                $node->expandible = false;
                $xmap->printNode($node);
            }
            
            $xmap->changeLevel(-1);
            
        }
	}

}
