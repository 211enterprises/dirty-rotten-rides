<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory Factory 3.0.0
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

// Payitems
define("adsman_upgrade_listing", "Actualización del anuncio");
define("adsman_payment_featured_gold", "Destacado de oro");
define("adsman_payment_featured_silver", "plata destacados");
define("adsman_payment_featured_bronze", "Destacado de bronce");

define("adsman_paymentitem_desc_featured", "Destacado de compra");
define("adsman_payment_purchase", "la compra de pago");
define("adsman_buy_contact_head", "Comprar este contacto");
define("adsman_payment_contact", "Contacto");
define("adsman_payment_listing", "Página");

define("adsman_payment_canceled","Payment canceled");
define("adsman_payment_canceled_info","You canceled your payment.");
define("adsman_payment_thankyou", "Gracias por su pago!");

define("adsman_choose_category", "Elegir esta categoría para el anuncio");
define("adsman_fill_in_fields", "Por favor, rellene los campos necesarios");

?>