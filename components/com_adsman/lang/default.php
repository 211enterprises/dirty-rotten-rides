<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory Factory 3.0.0
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

//Payitems
define("adsman_upgrade_listing", "Upgrade listing");
define("adsman_payment_featured_gold", "Featured gold");
define("adsman_payment_featured_silver", "Featured silver");
define("adsman_payment_featured_bronze", "Featured bronze");
define("adsman_payment_featured_image", "Featured images");

define("adsman_paymentitem_desc_featured", "Featured purchase");
define("adsman_payment_purchase", "Payment purchase");
define("adsman_buy_contact_head", "Buy this contact!");
define("adsman_payment_contact","Contact");
define("adsman_payment_listing","Listing");

define("adsman_payment_canceled","Payment canceled");
define("adsman_payment_canceled_info","You canceled your payment.");
define("adsman_payment_thankyou", "Thankyou for your payment!");

define("adsman_choose_category", "Choose this category for ad");
define("adsman_fill_in_fields", "Please fill in the necessary fields ");

?>