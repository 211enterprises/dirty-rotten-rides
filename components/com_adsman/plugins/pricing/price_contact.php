<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
require_once(ADS_COMPONENT_PATH."/plugins/pricing/pricing_object.php");

class price_contact  extends generic_pricing
{
    function price_contact(&$db) {
		$this->classname		= 'price_contact';
    	$this->classdescription	= 'Module for buying contacts';
		parent::generic_pricing($db);
		$this->itemname = 'contact';
    }

    function getPrice($d)
    {
    	$p = JRequest::getVar('itemprice');
    	if (!$p)
    		$p = parent::getPrice($d);
    	return $p;
    }

	function checktask($task,$d) {
		
		$Itemid 	= JRequest::getVar('Itemid', 0, 'REQUEST', 'integer');
		$mainframe 	= JFactory::getApplication();
		$my			=& JFactory::getUser();
        $contact_id=JRequest::getInt('id');

		if ($task == 'buy_contact') {
			if (!$my->id ) {
	            JError::raiseWarning(1, JText::_("ADS_ERR_LOGIN_FIRST"));
				$mainframe->redirect(JURI::root().'/index.php?option=com_adsman&task=userprofile&view=adsman&id='.$contact_id.'&Itemid='.$Itemid);
	            return true;
			}

            $session	= JFactory::getSession();
            $session->clear("posted_pm");
			
			$d['return_url']	= JURI::root().'/index.php?option=com_adsman&view=adsman&task=set_contact&id='.$contact_id;
            $d["itemamount"] = $this->getPrice($d);
            $d["id"] = null;
            $d['total_price_ad'] = $this->getPrice($d);
            $d['featured'] = 'none';
            $d['itemname'] = $this->itemname;
            //$d['return_url'] = JArrayHelper::getValue($d,'return_url');
            //$d['needed_credits'] = $needed_credits;

            $this->info["payment_description"] = 'Buy contact';
            $session->set('posted_pm',$d);

			$this->ShowPurchaseDialog($d);
			return true;
        }
        
        // pay_return
		if ($task == 'set_contact') {
			$_amount 			= trim(stripslashes($_POST['mc_gross']));
            $_payment_status 	= trim(stripslashes($_POST['payment_status']));
            $msgC = ""; 
            
            switch  ($_payment_status) {
            	case "Completed":
                case "Processed":{
		            $msgC 		= JText::_("ADS_SUCCESS_CONTACT_PAYED"); 
                    $_status	= 'ok';
                    $_balance_update = ($_amount)/$this->price;
	                break;
                    }
                case "Failed":
                case "Denied":
                case "Canceled-Reversal":
                case "Expired":
                case "Voided":
                case "Reversed":
                case "Refunded":
                    $_status='error';
                break;
                default:
                case "In-Progress":
                case "Pending":
                	{
		            $msgC 		= JText::_("ADS_SUCCESS_CONTACT_PAYED");  
                    $_status	= JText::_("ADS_PENDING_CONTACT_PAYED");
                    $_balance_update = 0;
                	break;
                	}
            }
        	if ( $_status != "error" ) {
	        	$this->acceptOrder($my->id, $_balance_update, $this->getCurrency($d),'none');
				$mainframe->redirect(JURI::root().'/index.php?option=com_adsman&task=listadds&view=adsman&Itemid='.$Itemid,$msgC);
        	}
		}

        return false;

    }
    

    function processTemplate($task,$d, &$smarty){
    	
		$Itemid 	= JRequest::getVar('Itemid', 0, 'REQUEST', 'integer');
		$mainframe 	= JFactory::getApplication();
		$my			=& JFactory::getUser();

		//if ($smarty) {			
			if ($task == "userprofile") {				       
				
				require_once (JPATH_ADMINISTRATOR.DS.'components'.DS.'com_adsman'.DS.'tables'.DS.'addsman.php');
				$aid	= JRequest::getVar('aid','');
				$addObj = new JAdsAddsman($this->_db);
				$addObj->load($aid);
				
				//$contactId = JRequest::getVar('id','');
				$contactId = isset($smarty->_tpl_vars["user"]->userid ) ? $smarty->_tpl_vars["user"]->userid : $smarty->_tpl_vars["user"]->id;

                if ( $my->id==$contactId)
					return ; // my profile

				$uri = JURI::root().'/index.php?option=com_adsman&view=adsman&task='.$task.'&id='.$contactId.'&Itemid='.$Itemid."&contact=1";

				//if ($my->id!==$userid && !$this->verifyContact($my->id, $userid))
                if(!$my->id)
   				{ 		    				
   						$smarty->_tpl_vars["user"]->surname = "hidden";
	    				$smarty->_tpl_vars["user"]->phone = "hidden";
                        //$smarty->_tpl_vars["user"]->city = "hidden";
                        $smarty->_tpl_vars["user"]->state = "hidden";
                        $smarty->_tpl_vars["user"]->email = "hidden";
                        $smarty->_tpl_vars["user"]->googleX = "hidden";
                        $smarty->_tpl_vars["user"]->googleY = "hidden";
	    				$smarty->_tpl_vars["user"]->address = "hidden";
	    				$smarty->_tpl_vars["user"]->paypalemail = "hidden";
	    				$smarty->_tpl_vars["user"]->name = "hidden&nbsp;<a href='index.php?option=com_adsman&task=buy_contact&id=".$contactId."&ItemId=".$Itemid."'>".JText::_("ADS_PAY_BUY_CONTACT_HEADER")."</a>"
							. " (". JText::_("ADS_ERR_LOGIN_FIRST").")";
	    				return ;
  				 }

  				$credits = AdsUtilities::getUserCredits($my->id);
  				$contact_price = AdsUtilities::getItemPriceCredits('contact');
   				//$credits = $this->getGredits($my->id, $contactId);
				$contact_bought = $this->verifyContact($my->id, $contactId);

				if($contact_bought) return;

				if ( isset($credits)  && $credits >= $contact_price ){
					// if you have not bought the contact it and no request to consume contact link to consume contact
					if(!isset($_GET["contact"]) || $_GET["contact"]!="1"){
	    				$smarty->_tpl_vars["user"]->surname = "hidden";
	    				$smarty->_tpl_vars["user"]->phone = "hidden";
                        //$smarty->_tpl_vars["user"]->city = "hidden";
                        $smarty->_tpl_vars["user"]->state = "hidden";
                        $smarty->_tpl_vars["user"]->email = "hidden";
                        $smarty->_tpl_vars["user"]->googleX = "hidden";
                        $smarty->_tpl_vars["user"]->googleY = "hidden";
	    				$smarty->_tpl_vars["user"]->address = "hidden";
	    				$smarty->_tpl_vars["user"]->paypalemail = "hidden";
	   					$smarty->_tpl_vars["user"]->name = "<a href='".$uri."'>".JText::_("ADS_CONTACT_CONSUME")."</a>";
	   					
   					} elseif (isset($_GET["contact"]) && $_GET["contact"]=="1"){
   						// consuming a credit and view
   						$this->decreaseCredit($my->id, $contactId);
   					}
   				} else {
   					// no credits, buy if ($credits==0)
					
    				$smarty->_tpl_vars["user"]->surname = "hidden";
    				$smarty->_tpl_vars["user"]->phone = "hidden";
                    //$smarty->_tpl_vars["user"]->city = "hidden";
                    $smarty->_tpl_vars["user"]->state = "hidden";
                    $smarty->_tpl_vars["user"]->email = "hidden";
                    $smarty->_tpl_vars["user"]->googleX = "hidden";
                    $smarty->_tpl_vars["user"]->googleY = "hidden";
    				$smarty->_tpl_vars["user"]->address = "hidden";
    				$smarty->_tpl_vars["user"]->paypalemail = "hidden";
    				$smarty->_tpl_vars["user"]->name = "hidden&nbsp;<a href='index.php?option=com_adsman&view=adsman&task=buy_contact&id=".$contactId."&ItemId=".$Itemid."'>".JText::_("ADS_PAY_BUY_CONTACT_HEADER")."</a>";
    				   				
   				}
    		}
    	//}
    }

    function getDescription() {
        return "Pay for user Contact information ";
    }

    function verifyContact($userId, $contact_id) {
		$sql = "
			SELECT count(*) FROM
			#__ads_contacts_buy
			WHERE  userid='".$userId."' AND contact_id = '".$contact_id."'";

		$this->_db->setQuery( $sql );
		return $this->_db->loadResult();
    }

    function decreaseCredit($userId, $contact_id){
    	
    	$this->_db->setQuery("SELECT price FROM #__ads_pricing WHERE itemname='$this->itemname'");
 		$credits_needed = $this->_db->loadResult();
 		
    	$this->_db->setQuery("UPDATE #__ads_user_credits SET credits_no=credits_no - $credits_needed WHERE userid='$userId' ");
		$this->_db->query();
   	
		$this->_db->setQuery("INSERT INTO #__ads_contacts_buy SET userid='$userId' , contact_id = '$contact_id'");
		$this->_db->query();
	}

    function save_admin_config()
    {
        $storePaymentItem = parent::save_admin_config();
        $task = JRequest::getCmd("task");

        if ($storePaymentItem) {
            if ($task == 'paymentitemsconfigapply' ) {
                // Redirect to the detail page.
                FactoryLayer::redirect( "index.php?option=com_adsman&task=paymentitemsconfig&itemname=$this->itemname",JText::_("ADS_SETTINGS_SAVED") );
            }

            FactoryLayer::redirect("index.php?option=com_adsman&task=paymentitems", JText::_("ADS_SETTINGS_SAVED"));
        } else {
            if ($task == 'paymentitemsconfigapply'  ) {
                // Redirect to the detail page.
                FactoryLayer::redirect( "index.php?option=com_adsman&task=paymentitemsconfig&itemname=$this->itemname",JText::_("ADS_NOT_SAVED") );
            }

            FactoryLayer::redirect( "index.php?option=com_adsman&task=paymentitemsconfig&itemname=$this->itemname",JText::_("ADS_NOT_SAVED") );
        }


        FactoryLayer::redirect("index.php?option=com_adsman&task=paymentitems", JText::_("ADS_SETTINGS_SAVED"));
    }

}
?>