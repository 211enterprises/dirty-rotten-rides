<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/



// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
require_once(ADS_COMPONENT_PATH."/plugins/pricing/pricing_object.php");

class price_maincredit extends generic_pricing
{
	
	function price_maincredit(&$db) {
        $this->classname='price_maincredit';
        $this->classdescription='Buy credits to publish ad';
        
        parent::generic_pricing($db);
    }

    function getPrice($d)
    {
    	$p = JRequest::getVar('itemprice');
    	if(!$p)
    		$p = parent::getPrice($d);
    	return $p;
    }
    
    function getCurrency($d){
    	$db = JFactory::getDbo();
    	$p  = JRequest::getVar('currency');
    	
    	if(!$p) {
    		$db->setQuery("SELECT * FROM #__ads_pricing WHERE itemname='maincredit' ");
		    $row = $db->LoadObject();
	    	
		    $p = $row->currency;
    	}
    	return $p;
    	
    }
    
    
    function update_object($order,$credits_amount,$total_price_ad,$featured_ad){
    	
    	$db = JFactory::getDbo();
    	$my	= JFactory::getUser();
    	$user_id = $my->id;

    	if(is_object($order)){
    		$aid = $order->object_id;
	    	$user_id = $order->userid;
    	} else 
    		$aid = $order;
    		
    	// UPDATE OBJECT	
    	$db = JFactory::getDbo();
		$db->setQuery("UPDATE #__ads SET featured='$featured_ad' where id='$aid'");
		$db->query();
	
		$db->setQuery("UPDATE #__ads SET status='1' where id='$aid'");
		$db->query();

		// CONSUME CREDITS - all credits
		$db->setQuery("UPDATE #__ads_user_credits SET credits_no=credits_no - $total_price_ad WHERE userid='$user_id' ");
		$db->query();

    }
	
}

?>