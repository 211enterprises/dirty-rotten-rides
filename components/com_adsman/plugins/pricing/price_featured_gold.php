<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/



// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
require_once(ADS_COMPONENT_PATH."/plugins/pricing/pricing_object.php");

class price_featured_gold  extends generic_pricing
{
    var $featured_type = 'gold';
    
    function price_featured_gold(&$db) {
        $this->classname		= 'price_featured_gold';
        $this->classdescription	= 'Module for buying featured Ads (Gold)';
        parent::generic_pricing($db);
        $this->itemname = 'featured_gold';
    }
    
    // CONSUME CREDITS
    function consume_credits($nr_credits=1,$user_id){
    	
    	$nr_credits = (int)$nr_credits;
 		$this->_db->setQuery("UPDATE #__ads_user_credits SET credits_no=credits_no - $nr_credits WHERE userid='$user_id' ");
		$this->_db->query();
    }
   
    function checktask($task,$d) {
		$Itemid = JRequest::getVar('Itemid', 0, 'REQUEST', 'integer');
		$mainframe = JFactory::getApplication();

        $language = JFactory::getLanguage();
        $language->load('com_adsman');
        $lng = substr( $language->get("tag") , 0, 2);
        if (file_exists(ADS_COMPONENT_PATH."/plugins/payment/pay_paypal.$lng.php"))
            require_once(ADS_COMPONENT_PATH."/plugins/payment/pay_paypal.$lng.php");
        else
            require_once(ADS_COMPONENT_PATH."/plugins/payment/pay_paypal.en.php");
		//require_once(ADS_COMPONENT_PATH."/plugins/payment/pay_paypal.en.php");
	
		$my = JFactory::getUser();
		$db = JFactory::getDbo();

		$credits = $this->getCredits($my->id);
	
		$id = JRequest::getVar('id', null);
		$islogged = 0;

		if ( $id ) {

			$sql = "SELECT COUNT(*) FROM ".PAYTABLE_PAYLOG." WHERE object_id = '$id' AND itemname = '{$this->itemname}' AND ( status <> 'ok' OR ISNULL(status ) )  ";
			$db->setQuery($sql);
			$islogged = $db->loadResult();

		}
				
    	if ($task=='save' ){
			$Itemid = JRequest::getVar('Itemid', 0, 'REQUEST', 'integer');
			$mainframe = JFactory::getApplication();
			
			$id = JRequest::getVar('id','');
			
			if ($id && !AdsUtilities::isMyAdd($id)) { 
				JError::raiseWarning(1,JText::_("ALERTNOTAUTH"));
				return;
			}
        
			$credits = $this->getCredits($my->id);
			
			$posted_pm_values = JRequest::get('POST');	
	        $featured_ad = isset($posted_pm_values["featured"]) ? $posted_pm_values["featured"] : 'none';
        
	        if ($featured_ad != null && $featured_ad != 'none' && $featured_ad == 'gold') {
	        	
	        	$add = new JAdsAddsman($db);
			
				if($id) {  
					$add->load($id);
					$add_featured_status = 	$add->featured;
					
					if ($add_featured_status == 'gold') {
						JError::raiseNotice(1,JText::_("ADS_ALREADY_FEATURED_GOLD"));
						return; 					
					}
				}
            }
    	}
    }

    function save_admin_config()
    {
        $storePaymentItem = parent::save_admin_config();
        $task = JRequest::getCmd("task");

        if ($storePaymentItem) {
            if ($task == 'paymentitemsconfigapply' ) {
                // Redirect to the detail page.
                FactoryLayer::redirect( "index.php?option=com_adsman&task=paymentitemsconfig&itemname=$this->itemname",JText::_("ADS_SETTINGS_SAVED") );
            }

            FactoryLayer::redirect("index.php?option=com_adsman&task=paymentitems", JText::_("ADS_SETTINGS_SAVED"));
            //$mainframe->redirect( "index.php?option=com_adsman&task=listadds",JText::_("ADS_SETTINGS_SAVED") );
        } else {
            if ($task == 'paymentitemsconfigapply'  ) {
                // Redirect to the detail page.
                FactoryLayer::redirect( "index.php?option=com_adsman&task=paymentitemsconfig&itemname=$this->itemname",JText::_("ADS_NOT_SAVED") );
            }

            FactoryLayer::redirect( "index.php?option=com_adsman&task=paymentitemsconfig&itemname=$this->itemname",JText::_("ADS_NOT_SAVED") );
        }


        FactoryLayer::redirect("index.php?option=com_adsman&task=paymentitems", JText::_("ADS_SETTINGS_SAVED"));
    }

}

?>