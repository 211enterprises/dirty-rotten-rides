<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );


class generic_pricing
{
	
	var $classname	= 'override this';
    var $classdescription	= 'Generic Module for pricing';
    var $_db		= null;
    var $itemname	= null;
    var $price		= null;
    var $currency	= null;
    var $enabled	= null;
    var $ordering	= null;
    var $params		= null;
    var $param_obj	= null;
    
    function generic_pricing(&$db)
    {
        $jp_lang = JFactory::getLanguage();
        $jp_lang->load('com_adsman');

	    //$jp_lang->load( 'ads_pricing' );
		
    	$itemname = substr($this->classname,6);
    	
        $this->_db = $db;
        $this->_db->setQuery("SELECT * FROM #__ads_pricing WHERE itemname='$itemname'");
		
        $tmp = null;
        $tmp = $this->_db->LoadObject();
        
		$this->itemname 	= $tmp->itemname;
		$this->enabled 		= $tmp->enabled;
		$this->price 		= $tmp->price;
		$this->currency		= $tmp->currency;
		$this->ordering 	= $tmp->ordering;
		$this->params 		= ($tmp->params) ? $tmp->params : "";
		$this->loadParams($this->params);		
    }
    
    function getPrice($d)
    {
    	return $this->price;
    }
    
    function getCurrency($d)
    {
    	return $this->currency;
    }
    
    function getDescription()
    {
    	return $this->classdescription;
    }
    
    function initiateOrder()
    {
		$my = & JFactory::getUser();
        return md5(microtime())."_".$my->id;
    }
    
    /**
     * 
     * Since 1.4.1
     *
     * @param $log Since 1.4.1 before order_id wich was f actually user_id
     * @param $amount
     */

    function acceptOrder($log,$amount,$total_price_ad,$featured_ad)
    {
    	$this->_db->setQuery("SELECT credits_no FROM #__ads_user_credits WHERE userid='$log->userid' ");
    	$val = $this->_db->LoadResult();

        if ($val != null && $val >= 0 ){
       	
            $this->_db->setQuery("UPDATE #__ads_user_credits SET credits_no=credits_no+$amount WHERE userid='$log->userid' ");
        } 
        else 
        {
            $this->_db->setQuery("INSERT INTO #__ads_user_credits SET credits_no=$amount , userid='$log->userid' ");
        }
        
        $this->_db->query();

        if($log->object_id && $log->itemname != 'contact')
        	$this->update_object($log,$amount,$total_price_ad,$featured_ad);
    }
            
    
    function cancelOrder($order_id)
    {
        return;
    }
    
    function show_admin_config() {
    	?>

    <style type="text/css">
        .red_border { border: 2px solid red; }
    </style>

        <script type="text/javascript">
            Joomla.submitbutton = function(task)
            {
                if (task == 'savepaymentitemsconfig' || task == 'paymentitemsconfigapply' ) {

                    var is_valid = mainPriceValidate();

                    if (is_valid == false) {
                        document.getElementById('price').setAttribute("class", "red_border");
                        return false;
                    }
                    else {
                        Joomla.submitform(task, document.getElementById('mainForm'));
                        return true;
                    }
                }
            }


            function mainPriceValidate() {
               // If price listing is enabled, basic price must be > 0
                var has_mainprice = document.getElementById('price');

                if (typeof(has_mainprice) != "undefined" && has_mainprice != null) {
                    var mainprice = document.getElementById('price').value;
                }
                else {
                    var mainprice = 0;
                }

                if ( (mainprice == "" || mainprice == 0) && has_mainprice) {
                    alert('Please enter a price bigger than 0 for Payment Item Price ');
                    return false;
                }

                return true;

            };
        </script>


        <h2><?php echo JText::_("ADS_PAYMENT_ITEM")." ".$this->itemname;?></h2>

        <?php $checkArray = array('featured_gold','featured_silver','featured_bronze','contact');
            if ( in_array($this->itemname,$checkArray ) ) { ?>
            <form action="<?php echo JRoute::_('index.php'); ?>" method="post" name="adminForm" id="mainForm" onsubmit="return mainPriceValidate();">
        <?php } else { ?>
            <form action="<?php echo JRoute::_('index.php'); ?>" method="post" name="adminForm" id="mainForm">
        <?php } ?>
				<?php
					price_object_AdminHelper::showCommonSettingsTab($this->itemname);
				?>
            <input type="hidden" name="option" value="com_adsman"/>
            <input type="hidden" name="task" value="savepaymentitemsconfig"/>
            <input type="hidden" name="itemname" value="<?php echo $this->itemname;?>"/>
            </form>
		<?php
    }
    
    function save_admin_config()
    {
        $price = JRequest::getVar('price','');
        $currecy=JRequest::getVar('currency','');
        $itemname=JRequest::getVar('itemname','');
        
        $this->_db->setQuery("UPDATE  #__ads_pricing SET price='$price',currency='$currecy' WHERE itemname='$itemname'");
        $price_updated = $this->_db->query();

        //$this->saveParams($_REQUEST);

        if ($price_updated) {
            $paramsUpdated = $this->saveParams(JRequest::get('request'));
        }

        if ($paramsUpdated) return $paramsUpdated;

    }

    function loadParams($paramtext=null)
    {
    	if ($paramtext === null) {
			$this->_db->setQuery("SELECT params FROM  #__ads_pricing WHERE itemname='{$this->itemname}'");
        	$paramtext=$this->_db->LoadResult();
    	}
    	
    	$arr = explode("\n",$paramtext);
    	
    	if ( $arr && $arr[0] != "" ) {
    		
    		for ($i=0; $i<count($arr); $i++) {
    			
    			$v	= explode("=",$arr[$i]);
    			
    			if ( in_array($v[0],$this->_param_members) ) {
    				$member			= $v[0];
    				$this->$member	= $v[1];
    			}
    		}
    	}
    }
    
    function saveParams($d=null)
    {
        $paramtext	= '';
    	for ( $i=0; $i < count($this->_param_members); $i++ ) {
    		
    		$member	= $this->_param_members[$i];
    		if ($d === null) {
    			$value	= $this->$member;
    		} else {
    			$value	= $d[$member];
    		}
    		$paramtext .= $member.'='.$value."\n";
    	}
		$this->_db->setQuery("UPDATE #__ads_pricing SET params='{$paramtext}' WHERE itemname='{$this->itemname}'");
        return $this->_db->query();

    }
        
    function checktask($task,$d)
    {
    	// Global Feature Listing CheckTasking
        $mainframe = JFactory::getApplication();
        $language = JFactory::getLanguage();
        $language->load('com_adsman');
        $lng = substr( $language->get("tag") , 0, 2);
        if (file_exists(ADS_COMPONENT_PATH."/plugins/payment/pay_paypal.$lng.php"))
            require_once(ADS_COMPONENT_PATH."/plugins/payment/pay_paypal.$lng.php");
        else
            require_once(ADS_COMPONENT_PATH."/plugins/payment/pay_paypal.en.php");
		//require_once(ADS_COMPONENT_PATH."/plugins/payment/pay_paypal.en.php");
	
		$my = JFactory::getUser();
		$db = JFactory::getDbo();
       
        $price_listing_DBHelperClass = new price_listing_DBHelper();
        $isListingEnabled = $price_listing_DBHelperClass->getEnabledPayListing();
        //$isListingEnabled = price_listing_DBHelper::getEnabledPayListing();

        if ( $task == 'set_featured' && isset($d['featured']) && $d['featured'] != '' ) {
            //if ($task == 'set_featured' && $d['featured'] == $this->featured_type) {
        	
        	$aid = JArrayHelper::getValue($d,"id");

	        if (isset($_POST['invoice'])) {
		        // The Order Number (not order_id !)
		        $invoice =  trim(stripslashes($_POST['invoice']));
		        $payment_status = trim(stripslashes($_POST['payment_status']));
				
				if (!class_exists("JTheFactoryPaymentLog"))
					require_once(dirname(__FILE__).DS."..".DS."payment".DS."payment_log.php");
		        
				$log = new JTheFactoryPaymentLog($this->_db);
				$log->set('_tbl_key', "invoice");
		
				$log->load($invoice);
				
				// If pending (IPN not runned yet for misc reasons )
				if( ( $payment_status == "Completed" || $payment_status == "Processed" || $payment_status == "In-Progress" || $payment_status == "Pending" ) && $log->status!="ok"){
					FactoryLayer::redirect('index.php?option='.APP_EXTENSION.'&task=details&id='.$aid,"Payment Succesfully! Waiting for approval!");
					return true;
				}
				
				elseif( $log->status == "ok" ){
					$ad = new JAdsAddsman($db);
					$ad->load($aid);
					
					if( $ad->featured == str_replace("featured_","",$this->itemname ) )
						FactoryLayer::redirect('index.php?option='.APP_EXTENSION.'&task=details&id='.$aid,"Payment Succesfully! Set $ad->featured !");
					
				}
				
	        } else {
		        $featured = $d['featured'];

	        	$credits = $this->getCredits($my->id);
	        	$featured_price = AdsUtilities::getItemPriceCredits('featured_'.$featured);
	        	
	            if ( $credits >= $featured_price ){
	            	
	            	//$this->update_object($log,$featured_price,$featured_price);
					$this->update_object($aid,$featured_price,$featured_price,$featured);
	                FactoryLayer::redirect('index.php?option='.APP_EXTENSION.'&task=details&id='.$aid,"Ad set $featured ". ' <br />'.JText::_('ADS_EDIT_24HOURS'));
					return true;
					
	            } else {
	            	
	            	// TO DO : enhance ItemID
	                $d['return_url'] = JURI::root().'index.php?option='.APP_EXTENSION.'&task=set_featured&featured=featured&id='.$aid;
	                $d["id"] 		= $aid;
					$d['needed_credits'] = $featured_price - $credits;
					$d['total_price_ad'] = $featured_price;
					$d['featured_type'] = $d['featured'];
					$d['itemname'] = 'featured_'.$featured;

	                $this->ShowPurchaseDialog($d);
						
	                return true;
	            }
	        }
           
        }

        if ($task == 'buy_featured' &&  isset($d['featured']) && $d['featured'] != ''){
            $this->ShowPurchaseDialog($d);
            return true;
        }

        if ($task == 'buy_contact'){
            $this->ShowPurchaseDialog($d);
            return true;
        }
		
        if ($task=='payment' && !$isListingEnabled){
	        
        	// The Order Number (not order_id !)
	        $invoice =  trim(stripslashes($_POST['invoice']));
	        $payment_status = trim(stripslashes($_POST['payment_status']));
	        
	        if($invoice){
	        	require_once(dirname(__FILE__).DS."..".DS."payment".DS."payment_log.php");
				$log = new JTheFactoryPaymentLog($this->_db);
				$log->set('_tbl_key', "invoice");
		
				$log->load($invoice);
				
				// If pending (IPN not runned yet for misc reasons )
				if( ( $payment_status == "Completed" || $payment_status == "Processed" || $payment_status == "In-Progress" || $payment_status == "Pending" ) && $log->status!="ok"){
					if($aid = $log->object_id)
						$mainframe->redirect('index.php?option=com_adsman&task=details&id='.$aid,"Payment Succesfully! Waiting for approval!");
					else
						$mainframe->redirect('index.php?option=com_adsman',"Payment Succesfully! Waiting for approval!");
					return true;
				}else{
					if($aid = $log->object_id)
						$mainframe->redirect('index.php?option=com_adsman&task=details&id='.$aid,"Payment Succesfully!");
				}
	        }
        }

        return false;
    }

    function ShowPurchaseDialog($d) {
        $Itemid = JRequest::getVar('Itemid', 0, 'REQUEST', 'integer');

        $mainframe = JFactory::getApplication();
    	$my = JFactory::getUser();
		$smarty = AdsUtilities::SmartyLoaderHelper();

        $credits = generic_pricing::getCredits($my->id);
		$db = JFactory::getDbo();
        $session	= JFactory::getSession();

		$lists["packages"] = price_packages_DBHelper::getFrontPackages();
        $package_image_path = JPATH_ROOT.DS.'images'.DS.'ads'.DS.'packages'.DS;

		$checkout = defined("ads_opt_choose_checkout") ? ads_opt_choose_checkout : 'credits';
		$db->setQuery("SELECT * FROM #__ads_pricing WHERE itemname='maincredit' ");
		$row = $db->LoadObject();
	  
		$currency = $row->currency;
		$d['currency'] = $currency;

		// check if use packages
		if ($checkout == 'packages' && !empty($lists["packages"])) { //and packages enabled
		
			$smarty->assign("lists", $lists);
			$smarty->assign('object_id',JRequest::getVar("id",null));
			$smarty->assign('my',$my);
			$smarty->assign_by_ref('item_object',$this);
			$smarty->assign('itemamount',JArrayHelper::getValue($d,'itemamount'));
			$smarty->assign('credits',$credits);
				
			$smarty->assign('paymenttype','');
			$smarty->assign('return_url',JArrayHelper::getValue($d,'return_url'));
	        $smarty->assign('page_title', JText::_("Buy Credits Package ") ) ;
	        
	        $smarty->assign('price_maincredit',$row->price);	
			$smarty->assign('currency_maincredit',$row->currency);	

			if (isset ($d['total_price_ad'])) {
				$smarty->assign('total_price_ad',$d['total_price_ad']);
			}
			
			if (isset ($d['featured'])) {
				$smarty->assign('featured_ad',$d['featured']);
			}

	        $id = JRequest::getVar('id', null);
            $id_ad = JArrayHelper::getValue($d,'id'); //check if ad id

            $smarty->assign('package_img_path',$package_image_path);
                        
            $packages_purchase_pm['featured_ad'] = (isset ($d['featured']) ) ? $d['featured'] : 'none';
            $packages_purchase_pm['total_price_ad'] = (isset ($d['total_price_ad']) ) ? $d['total_price_ad'] : '';
            $packages_purchase_pm['needed_credits'] = (isset ($d['needed_credits']) ) ? $d['needed_credits'] : '';
            $packages_purchase_pm['ad_id'] = (isset($id) && ($id != '')) ? $id : $id_ad;

            $session->set('packages_purchase_pm',$packages_purchase_pm);
	    					       
	        $smarty->display('plugins/t_packages_purchase.tpl');
			
		} else {
			// if not packages enabled or not use packages display buy credits
			$db->setQuery("SELECT * FROM #__ads_pricing WHERE enabled=1");
			$rows=$db->LoadObjectList();
			
			$id = JArrayHelper::getValue($d,'id'); //check if ad id
 			$add = new JAdsAddsman($db);
 			
			if($id) {  
				$add->load($id);
			}
			$islogged = 0;

			if ($id && !AdsUtilities::isMyAdd($id) && ($d['task'] != 'buy_contact') ) {
				JError::raiseWarning(1,JText::_("ALERTNOTAUTH")); 
				return;
			}

			$smarty->assign('object_id',$id);
			$add = new JAdsAddsman($db);
			
			if (isset ($d['total_price_ad'])) {
				$smarty->assign('total_price_ad',$d['total_price_ad']);
			}
				
			if (isset($d['needed_credits'])) {
				$smarty->assign('itemamount',$d['needed_credits']);
			}
			
			if (isset($d['itemname'])) {
				$smarty->assign('itemname',$d['itemname']);
			}
			
			if (isset ($d['featured'])) {
				$smarty->assign('featured_ad',$d['featured']);
			}
			
			$this->info["payment_description"] = $add->title;
				
			$smarty->assign('pricing',$rows);
			$smarty->assign('my',$my);
			$smarty->assign('item_object',$this);
			
			$smarty->assign('credits',$credits);
			
			$smarty->assign('paymenttype','');
			$smarty->assign('currency',$d['currency']);
			$smarty->assign('return_url',JArrayHelper::getValue($d,'return_url'));
			$smarty->assign('page_title', "Buy credits" ) ;
			
			$smarty->assign('price_maincredit',$row->price);	
			$smarty->assign('currency_maincredit',$row->currency);

            //test direct pay
            //check if we want ot show purchase dialog
            $session->set("payment_listing_dialog", null);

            $session_vars = $session->get('posted_pm');
            $session->set('posted_pm',$d);

            $mainframe->redirect(JRoute::_('index.php?option=com_adsman&task=purchase&view=adsman&id='.$add->id.'&Itemid='. $Itemid));
            //$smarty->display('plugins/t_credits_purchase.tpl');
		}
    }
    
    function checkReturnStatus(){
    	// nothing
    }
    
    /**
     * This is the same for Featuring Listing
     * override function in pricing class for custom handling
     * 
     *
     * @param int/object $order (ad id or log object)
     */
    function update_object($order,$credits_amount,$total_price_ad,$featured_ad){

    	$my	= JFactory::getUser();
    	$user_id = $my->id;

    	if(is_object($order)){
    		$aid = $order->object_id;
	    	$user_id = $order->userid;
    	} else 
    		$aid = $order;
    			
    	// UPDATE OBJECT
    	$db = JFactory::getDbo();
		$db->setQuery("UPDATE #__ads SET featured='$featured_ad' where id='$aid'");
		$db->query();

        $this->_db->setQuery("UPDATE #__ads SET status='1' where id='$aid'");
        $this->_db->query();

		// CONSUME CREDITS - all credits
		$db->setQuery("UPDATE #__ads_user_credits SET credits_no=credits_no - $total_price_ad WHERE userid='$user_id' ");
		$db->query();
		
    }
    
    function consume_credits()
    {
    	// TO DO : single code responsible for credit decrease
    }
    
    function getCredits($user){
    	$db = JFactory::getDbo();
		$db->setQuery("SELECT credits_no FROM #__ads_user_credits WHERE userid='$user' ");
		$credits =  $db->LoadResult();
		
		return (int)$credits;
	}
}

class price_object_AdminHelper{

	function showCommonSettingsTab($itemname) {
	?>
		<fieldset>
			<legend><?php echo JText::_("ADS_PAYMENT_ITEM")." ".$this->itemname;?></legend>
	        <table class="adminlist" width="100%">
	        <tr>
	            <td class="paramlist key" width="120px"><?php echo JText::_("ADS_PLG_PAYMENT_ITEM_PRICE");?>: </td>
	            <td><input size="5" name="price" id="price" class="inputbox" value="<?php echo $this->price;?>">
	            
	            <?php if ($itemname == 'maincredit') {
	            	$currency = $this->currency;
	            	
	            	$this->_db->setQuery("SELECT * FROM ".constant("APP_CURRENCY_TABLE_".strtoupper(APP_PREFIX)));
					$currencies = $this->_db->loadObjectList();	
	                        		
            		if ($currencies) { ?>
	            		<select name="currency">
		            		<?php foreach ($currencies as $k => $c) { 
		            			if ($currency == $c->name) {
		            			?>
		            			<option selected="selected"><?php echo $c->name; ?></option>
		            			<?php } else { ?>
		            			<option><?php echo $c->name; ?></option>
		            		<?php }
		            		} ?>
		            	</select>
	            		<?php
            		} else { 
            			 
            			?>
            			<input type="text" name="currency" value="<?php echo $currency; ?>"  />
            			<?php
            		}
            		
	            } else { ?>
	            	 <input size="5" name="currency"  readonly="readonly" class="inputbox" value="<?php echo $this->currency;?>">
	            <?php } //disabled
	            ?></td>
	        </tr>
	       
	       <!-- <tr>
	            <td class="paramlist key" width="120px"><?php //echo JText::_("ADS_PLG_CURRENCY");?>: </td>
	            <td><input size="5" name="currency" class="inputbox" value="<?php //echo $this->currency;?>"></td>
	        </tr>-->
	        </table>
		</fieldset>
	<?php 
	}
	
	function showSpecialUserSettingsTab(){
	?>
	<fieldset>
		<legend><?php echo JText::_("ADS_REDUCED_PRICES_PAYMENT_ITEM")." ".$this->itemname;?></legend>
        <table class="adminlist" width="100%">
        <tr>
            <td class="paramlist key" width="120px"><?php echo JText::_("ADS_PRICE_FOR_POWERSELLERS_USERS");?>: </td>
            <td><input size="5" name="price_powerseller" class="inputbox" value="<?php echo $this->price_powerseller;?>"></td>
        </tr>
        <tr>
            <td class="paramlist key" width="120px"><?php echo JText::_("ADS_PRICE_FOR_VERIFIED_USERS");?>: </td>
            <td><input size="5" name="price_verified" class="inputbox" value="<?php echo $this->price_verified;?>"></td>
        </tr>
        </table>
	</fieldset>
	<?php
	}
	
}

?>