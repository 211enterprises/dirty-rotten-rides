<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/



// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

require_once(dirname(__FILE__).DS."pricing_object.php");

class price_packages extends generic_pricing
{
    function price_packages(&$db)
    {
        $this->classname='price_packages';
        $this->classdescription=JText::_("ADS_BUYING_PACKAGES");
        parent::generic_pricing($db);
		$this->itemname = "packages";
    }
    
   function getPrice($d)
    {
    	$price = JRequest::getVar('itemprice');
   	
    	if (!$price) {
    		if( ($d['item_number'] == 'packages') || ($d['itemname'] == 'packages')) {
	    	//if($d['itemname'] == 'packages') {
	    		
    			$package_id = isset($d['custom']) ? $d['custom'] : $d['var_value1'];
				$package = price_packages_DBHelper::getPackage((int)$package_id);
			
				return $package->price;
				
			} else  {
				return $price;
			}
    	
    	}
    	
    	//$p = parent::getPrice($d);
    	return $price;
    	
    }
    
    function getCurrency($d) {
    	
    	$currency = JRequest::getVar('currency');
    	
    	if (!$currency) {
    	
	    	if($d['item_number'] == 'packages') {
	    		$package_id = $d['custom'];
				//$selected_package = price_packages_DBHelper::getPackage($d['item_name']);
								
				$package = price_packages_DBHelper::getPackage((int)$package_id);
				return $package->currency;
	    	} else {
	    		return $currency;
	    	}
    	}
    	
    	return ($d["currency"]);
    }
    
    function getDescription(){
    	
    	$key = array_search("package_id",$_REQUEST);
    	$package_id = JRequest::getVar(str_replace("var_name","var_value",$key),null);
    	$package = price_packages_DBHelper::getPackage((int)$package_id);
    	return JText::_("{$package->name} Package");
    	
    }
    
    function checktask($task,$d){
   	
    	switch ($task) {
    		case 'buy_packages':{
	            $this->showPackagesFull($d);
		        return true;
    			break;
    		}
    		case 'buy_package':{
	            $this->ShowPurchaseDialog($d);
		        return true;
    			break;
    		}
    		case 'free_package':
    			$this->offerFreeCredits($d);
		        return true;
    			break;
    	}
    	
    }

    function update_object($order,$credits_amount,$total_price_ad,$featured_ad) {
    	//
    	$my= JFactory::getUser();
    	$user_id = $my->id;

    	if ( is_object($order) ){
    		$aid = $order->object_id;
	    	$user_id = $order->userid;
    	} else
    		$aid = $order;
        
    	// UPDATE OBJECT
    	$this->_db->setQuery("UPDATE #__ads SET featured='$featured_ad' where id='$aid'");
		$this->_db->query();

		$this->_db->setQuery("UPDATE #__ads SET status='1' where id='$aid'");
		$this->_db->query();

		// CONSUME CREDITS
		$this->_db->setQuery("UPDATE #__ads_user_credits SET credits_no=credits_no - $total_price_ad WHERE userid='$user_id' ");
		$this->_db->query();
    }

        
    function offerFreeCredits($d) {
		$mainframe = JFactory::getApplication();
		$db = JFactory::getDbo();
		$user = JFactory::getUser();

        //free package exists
		$db->setQuery("SELECT * FROM ".PAYTABLE_PACKAGES." WHERE price = 0 AND status = 1 LIMIT 0,1 ");
		$free_package  = $db->loadObject();

        //user had free package
        $user_free_package = price_packages_DBHelper::getUserFreePackage($user->id);
        if ($user_free_package != null)
            $had_free_package = $user_free_package->package_free;
        else
            $had_free_package = 0;

		if ($free_package && !$had_free_package) {
			$amount = (int)$free_package->credits;
		
			$db->setQuery("SELECT credits_no FROM #__ads_user_credits WHERE userid='$user->id' ");
			$val = $db->LoadResult();

	        if ($val != null && $val >= 0 ){
	            $db->setQuery("UPDATE #__ads_user_credits SET credits_no=credits_no+$amount, package_free=1 WHERE userid='$user->id' ");
	        } 
	        else {
	            $db->setQuery("INSERT INTO #__ads_user_credits SET credits_no=$amount , userid='$user->id', package_free=1 ");
	        }
	        
	        $db->query();
		
			$mainframe->redirect("index.php?option=com_adsman&task=paymentitemsconfig&itemname=packages","Free Credits offered");
		} else {
			$mainframe->redirect("index.php?option=com_adsman&task=paymentitemsconfig&itemname=packages","No Free Credits to offer");
		}
		
	}
    
	function ShowPurchaseDialog($d) {
        $my =  JFactory::getUser();
		$smarty = AdsUtilities::SmartyLoaderHelper();
				
		$selected_package = JRequest::getVar("package",null);

		$smarty->assign('object_id',JRequest::getVar("id",null));
		$smarty->assign('selected_type', $selected_package );
		$smarty->assign('my',$my);
		$smarty->assign_by_ref('item_object',$this);
	   // $smarty->assign('paymenttype','');
		$smarty->assign('return_url',JArrayHelper::getValue($d,'return_url'));
        $smarty->assign('page_title', JText::_("Buy package ".JRequest::getVar("package",null)) ) ;
        
    	$packages = price_packages_DBHelper::getPackage( $selected_package );
    	    	
    	$payOb = JTheFactoryPaymentLib::getInstance();
    	$payOb->loadAvailabeItems();
    	
    	$pricings = $payOb->price_plugins;
		$price_maincredit = $pricings['price_maincredit']->price;
		$currency_maincredit = $pricings['price_maincredit']->currency;
    	
		$opts = array();
		for ($j = 1;$j<=20; $j++) {
			$opts[] = JHTML::_('select.option', $j, $j);
		}
		$lists['package_quant'] = JHTML::_('select.genericlist',  $opts, 'itemamount_package', 'class="inputbox"  size="1" alt="itemamount_package" ',  'value', 'text',1);
			
			
		$pac = $packages;
		//$pac->pricing_list = $pricings;
		if (isset($packages)) {

            $session	= JFactory::getSession();
            $packages_purchase_pm = $session->get('packages_purchase_pm');
    
    		$smarty->assign('price_maincredit',$price_maincredit);	
    		$smarty->assign('currency_maincredit',$currency_maincredit);	
    		$smarty->assign('package_quant',$lists['package_quant']);
    
            $smarty->assign('total_price_ad',$packages_purchase_pm['total_price_ad']);
            $smarty->assign('featured_ad',$packages_purchase_pm['featured_ad']);
                            
    		$smarty->assign('pac',$packages);
    		$smarty->display('plugins/t_package_purchase.tpl');
		}
		else {
			echo '<span style="float: left;margin: 5px 5px 5px 0; color: BLUE; font-size: 14px;">'. JText::_("ADS_PACKAGE_NOT_FOUND") .' </span><br />';
		}		
		
	}
    
	function showPackagesFull($d) {
        $my= & JFactory::getUser();
		$smarty = AdsUtilities::SmartyLoaderHelper();
		
		$payOb = JTheFactoryPaymentLib::getInstance();
    	$payOb->loadAvailabeItems();
    	
    	$pricings = $payOb->price_plugins;
		$price_maincredit = $pricings['price_maincredit']->price;
		$currency_maincredit = $pricings['price_maincredit']->currency;
		
		$smarty->assign('object_id',JRequest::getVar("id",null));
		$smarty->assign('selected_type',JRequest::getVar("package",null));
		$smarty->assign('my',$my);
		$smarty->assign_by_ref('item_object',$this);
		$smarty->assign('return_url',JArrayHelper::getValue($d,'return_url'));
		$smarty->assign('page_title', JText::_("Buy package ".JRequest::getVar("package",null)) ) ;
 		
		$smarty->assign('price_maincredit',$price_maincredit);	
		$smarty->assign('currency_maincredit',$currency_maincredit);	
					
    	$packages = price_packages_DBHelper::getFrontPackages();
        $package_image_path = JPATH_ROOT.DS.'images'.DS.'ads'.DS.'packages'.DS;

		$smarty->assign('packages',$packages);
        $smarty->assign('package_img_path',$package_image_path);
        
		$smarty->display('plugins/t_packages.tpl');
    }

    function show_admin_config() {
    	
        $action = JRequest::getVar('action','');
        $package_id = JRequest::getInt('id','');
        
		switch ($action){
			case "toggle_publishing":
				price_packages_AdminHelper::toggle_publishing(); 
				break;
				
			case "delete":
				price_packages_AdminHelper::del_package(); 
				break;
			case "edit":
				price_packages_AdminHelper::show_config($package_id); 
				break;
			default:
				price_packages_AdminHelper::show_config();
				break; 
		}
    	
    }
    
    function show_free_credits() {
    	//$itemname	= JRequest::getVar('itemname','');
		$database = JFactory::getDbo();
						
		$database->setQuery("SELECT * FROM ".PAYTABLE_PACKAGES." WHERE price = 0 AND status = 1 LIMIT 0,1 ");
		$free_package  = $database->loadObject();
						
		if ($free_package) {
			$amount = (int)$free_package->credits;
			price_packages_AdminHelper::show_freecredits($amount);
		}  else { 
			echo '<span style="float: left;margin: 5px 5px 5px 0; color: green; font-size: 14px;">'. JText::_("ADS_PACKAGES_FREE_NOT_DEFINED") .' </span><br />';
			//echo JText::_("ADS_PACKAGES_FREE_ENABLED");
		}		
    }
 
    function save_admin_config()
    {
        $itemname	= JRequest::getVar('itemname','');
        $act		= JRequest::getVar('act','addpackage');
        $package_id = JRequest::getVar('package_id',0);

        $new_package_values = JRequest::get('POST');

        if ($new_package_values['name'] == '' && $new_package_values['credits'] == '' && $new_package_values['price'] == '') {
            return false;
        }

        $package_posts['name'] 		= JRequest::getVar('name','','post','string');
        $package_posts['price']		= JRequest::getVar('price',0,'post','int');
        $package_posts['currency']	= JRequest::getVar('currency','','post','word');
        $package_posts['credits'] 	= JRequest::getVar('credits',0,'post','int');

		//ads_packages store
    	JTable::addIncludePath(JPATH_SITE.DS.'administrator'.DS.'components'.DS.'com_adsman'.DS.'tables');
	    $package =& JTable::getInstance('Packages', 'Table');

        if ($package_id != 0)   
    	{
	    	$package->load($package_id,false);
	    	
	    	if (!$package->bind($package_posts))
		    {
		      $this->setError($this->_db->getErrorMsg());
		      return false;
		    }
	    	
	    	if (!$package->store())
		    {
		      $this->setError($this->_db->getErrorMsg());
		      return false;
		    }
    	}  else {
		    if (!$package->bind($package_posts))
		    {
		      $this->setError($this->_db->getErrorMsg());
		      return false;
		    }
	  	    
		    if (!$package->check())
		    {
		      $this->setError($this->_db->getErrorMsg());
		      return false;
		    }
		
		    if (empty($package->id)) {
			    if (!$package->store())
			    {
			      $this->setError($this->_db->getErrorMsg());
			      return false;
			    }
		    }
	    }

        //$this->saveParams($_REQUEST);
    }
    
}

class price_packages_AdminHelper extends price_object_AdminHelper {
	
	function show_config($package_id=null){
		$this->_db->setQuery("SELECT * FROM ".constant("APP_CURRENCY_TABLE_".strtoupper(APP_PREFIX)));
	
		$currencies = $this->_db->loadObjectList();
		$act = 'addpackage';
		
		
    	$packages = price_packages_DBHelper::getPackages();
    	
    	$payOb = JTheFactoryPaymentLib::getInstance();
    	$payOb->loadAvailabeItems();
    	
    	$pricings = $payOb->price_plugins;
    	unset($pricings["price_packages"]);
    	unset($pricings["price_comission"]);
    	    	
    	if ($package_id != null) {
    		$package = price_packages_DBHelper::getPackage((int)$package_id);
    		$act = 'editpackage';
    	} else {
    		$package_id = 0;
    	}
    	
		?>
<h2><?php echo JText::_("ADS_PACKAGES");?></h2>

<div>
<?php 
echo '<h3>1 Credit = '.JText::_("ADS_DEFAULT_PRICE").' '.$pricings['price_maincredit']->price.' '.$pricings['price_maincredit']->currency.'</h3>';
?>
</div>

<table class="adminlist">
	<tr>
		<td valign="top">
			
            <form action="<?php echo JRoute::_('index.php'); ?>" method="post" name="adminForm" id="adminForm">
            <fieldset>
            <legend><?php echo ($package_id != null) ? JText::_("ADS_EDIT_PACKAGE") : JText::_("ADS_ADD_PACKAGE");?></legend>
			<table class="adminlist" border=1 width="100%">
				<tr>
            		<td class="paramlist key" style="width:60px !important;"><?php echo JText::_("ADS_NAME");?></td>
            		<td colspan="2">
            		<?php $name = ($package_id != null) ? $package->name : ''; ?>
            		<input type="text" size="45" maxlength="255" name="name" value="<?php echo $name; ?>" /></td>
            	</tr>
            	<tr>
            		<td class="paramlist key" style="width:60px !important;"><?php echo JText::_("ADS_CREDITS");?></td>
            		<td colspan="2">
            		<?php $credits = ($package_id != null) ? $package->credits : ''; ?>
            		<input type="text" size="15" name="credits" value="<?php echo $credits; ?>" /></td>
            	</tr>
            	<tr>
            		<td class="paramlist key" style="width:60px !important;"><?php echo JText::_("ADS_PAYMENT_ITEM_PRICE");?></td>
            		<td style="width:70px !important;">
            		<?php $price = ($package_id != null) ? $package->price : ''; ?>
            		<?php $disabled = ($package_id != null && $package->price == 0) ? 'disabled' : ''; ?>
            		
            		<input type="text" name="price" size="15" value="<?php echo $price; ?>" <?php echo $disabled;?> /></td>
            	<!--</tr>-->
            	
            	<!--<tr>
            		<td class="paramlist key" style="width:80px !important;"><?php //echo JText::_("ADS_CURRENCY");?></td>-->
            		<td>
            		<?php 
            		$currency = ($package_id != null) ? $package->currency : '';
            		
            		if ($currencies) { ?>
	            		<select name="currency">
		            		<?php foreach ($currencies as $k => $c){ 
		            			if ($currency == $c->name) {
		            			?>
		            			<option selected="selected"><?php echo $c->name; ?></option>
		            			<?php } else { ?>
		            			<option><?php echo $c->name; ?></option>
		            		<?php }
		            		} ?>
		            	</select>
	            		<?php
            		} else { 
            			 
            			?>
            			<input type="text" name="currency" value="<?php echo $currency; ?>"  />
            			<?php
            		}
            		?>
            		</td>
            	</tr>
            </table>
            </fieldset>
            <input type="hidden" name="option" value="<?php echo APP_EXTENSION;?>"/>
            <input type="hidden" name="act" value="<?php echo $act; ?>"/>
            <input type="hidden" name="task" value="savepaymentitemsconfig"/>
            <input type="hidden" name="itemname" value="<?php echo $this->itemname;?>"/>
            <input type="hidden" name="package_id" value="<?php echo $package_id; ?>"/>
            </form>
            
            <div>
            	<fieldset>
					<legend>Assign free credits package to an user</legend>
					<span> <?php echo JText::_("Click"); ?>
						<?php $freecredits_link = JRoute::_('index.php?option=com_adsman&task=freecredits&amp;itemname=packages'); ?>
		            	<a href="<?php echo $freecredits_link; ?>"><?php echo JText::_("here"); ?></a>
					<?php echo JText::_("to select an user to Offer free credits package"); ?> </span>
					
            	</fieldset>
            </div>
            
           
			
		</td>
		<td width="75%" valign="top">
		
		<form action="index.php" method="post" name="adminList" id="adminList">
         <input type="hidden" name="option" value="<?php echo APP_EXTENSION;?>" />
         <input type="hidden" name="task" value="save_packages_order" /> 
         <input type="hidden" name="itemname" value="packages" />
            <fieldset>
            <legend><?php echo JText::_("ADS_PACKAGES_LIST");?></legend>
			<table class="adminlist">
				<thead>
				<tr>
					
					<th><?php echo JText::_("ADS_ENABLED");?></th>
					<th><?php echo JText::_("ADS_NAME");?></th>
					<th><?php echo JText::_("ADS_CREDITS");?></th>
					<!--<th><?php //echo JText::_("ADS_NORMAL_PRICE");?></th>-->
					<th><?php echo JText::_("ADS_PAYMENT_ITEM_PRICE");?></th>
					<th><?php echo JText::_("ADS_EDIT");?></th>
					<th><?php echo JText::_("Ordering").'&nbsp;<a href="javascript: void(0);" onclick="document.adminList.submit();">Save</a>'; ?></th>
					<th height="30"><?php echo JText::_("ADS_DELETE");?></th>
				</tr>
				</thead>
				<tbody>
				<?php 
			if (isset($packages)) 
				foreach ($packages as $p => $pac) {?>
				<tr>
                   
                    <td style="text-align: center !important; width: 4%;">
                    <?php
						if ( isset($pac->status) && $pac->status == 1 ) {
							$img = 'publish_g.png';
							$alt = JText::_( 'ADS_ENABLED' );
						} else {
							$img = 'publish_x.png';
							$alt = JText::_( 'ADS_NOT_ENABLED' );
						}
						
						$status ="<span class=\"editlinktip hasTip\" title=\"".( JText::_("ADS_ENABLED_INFORMATION").$alt )."\">";
					 	$status .="<a href=\"index.php?option=".APP_EXTENSION."&task=paymentitemsconfig&itemname=packages&action=toggle_publishing&cid[]=$pac->id\" >";
					 	$status .="<img src=\"".JURI::root()."administrator/components/com_adsman/img/".$img."\" width=\"16\" height=\"16\" border=\"0\" alt=\"".$alt."\" /></a></span>"; 
					 	
					 	echo $status;
					 ?>
                    </td>
					<td><span class="tooltip_left"><strong><?php echo $pac->name;?></strong></span>
                        <?php if ($pac->price == 0) { ?>
                     	<span class="tooltip_right"><?php  echo JText::_("ADS_FREE_PACKAGE"); echo "&nbsp;"; ?></span>
						<span class="tooltip_right"><?php echo JHTML::_('tooltip',JText::_("ADS_FREE_PACKAGE_HELP")); ?></span>

                     <?php } ?>
                        <br />
					<?php 
					/*if ( file_exists(JPATH_ROOT."/images/ads/packages/".strtolower($pac->name).".jpg") ) {	
						echo "<img src='".JURI::root()."/images/ads/packages/".strtolower($pac->name).".jpg"."' width='70' />";
					}else{ 
						echo JText::_("ADS_THUMBNAIL_NOT_CREATED"); echo JHTML::tooltip( JText::_("ADS_THUMBNAIL_PATH_IS").JPATH_ROOT."/images/ads/packages/".strtolower($pac->name).".jpg" );
					}	*/
					?>
					</td>
					<td style="width:8%; text-align: right;"> <?php echo $pac->credits; ?></td>
					<!--<td><strong><?php //foreach ($pac->unprice as $cur => $val) echo $cur.": ".$val.";";?></strong><br /><?php //echo $pac->unprice_detailed;?></td>-->
					<td style="width:10%; text-align: right;"><?php echo $pac->price;?> <?php echo $pac->currency;?>&nbsp;</td>
					<td style="width:6%; text-align: center; ">
						<a href="#" onclick="this.href='index.php?option=<?php echo APP_EXTENSION;?>&task=paymentitemsconfig&itemname=packages&action=edit&id=<?php echo $pac->id;?>';  ">
                    		<img src="<?php  echo F_ROOT_URI;?>administrator/components/com_adsman/img/addedit.png" style="width:12px;" class="ads_noborder" />
                    	</a>
                    </td>
                    <td style="width:5%; text-align: center !important;"><input name="order-<?php echo $pac->id;?>" value="<?php echo $pac->ordering;?>" size='1'> </td>
                     <td width="30">
                    	<a href="#" onclick="if(confirm('Are you sure?')=='1') this.href='index.php?option=<?php echo APP_EXTENSION;?>&task=paymentitemsconfig&itemname=packages&action=delete&id=<?php echo $pac->id;?>';  ">
                    		<img src="<?php  echo F_ROOT_URI;?>administrator/components/com_adsman/img/cancel_f2.png" style="width:12px;  text-align: center !important;" class="ads_noborder" />
                    	</a>
                    </td>
                    
				</tr>
				<?php } ?>
				</tbody>
			</table>
			</form>
            </fieldset>
		</td>
	</tr>
</table>
            <?php
		
	}
	
	function show_freecredits($amount) { ?>
		<div>
			<script type="text/javascript" src="<?php echo JURI::root();?>administrator/components/com_adsman/js/adsman.js"></script>
				<fieldset>
					<legend>Assign free credits package to user</legend>
					  <form action="<?php echo JRoute::_('index.php'); ?>" method="post" name="adminForm2" id="adminForm">	

						<?php 
						if ($amount > 0) {
							
							
						$users = array();
						$database = JFactory::getDbo();
			
						$query = 'SELECT id AS value, name AS text'
						. ' FROM #__users'
						. ' WHERE block = 0';
						
						$database->setQuery( $query );
						$users[] = JHTML::_('select.option',  '0', '- '. JText::_( 'Select User' ) .' -' );
						$users = array_merge( $users, $database->loadObjectList() );
			
						if ($users) { ?>
							<select name="user_id">
								<?php foreach ($users as $k => $user){ ?>
									<option value="<?php echo $user->value; ?>"><?php echo $user->text; ?></option>
								<?php } ?>
							</select>
						<?php 
						} else { ?>
							<input type="text" name="user_id" value="" />
						<?php } ?>
						<input type="hidden" name="free_credits_amount" value=<?php echo $amount;?> >
						<input type="button" class="button" onclick="javascript:Joomla.submitbutton('offerFreeCredits')" value="<?php echo JText::_("Offer free credits package"); ?>">
						<span style="float: left; vertical-align: bottom; margin: 5px 5px 5px 0;"><?php echo '(' . $amount .' '. JText::_("ADS_CREDITS") .')'; ?> </span>
						<input type="hidden" name="option" value="<?php echo APP_EXTENSION;?>"/>
						<input type="hidden" name="task" value="offerFreeCredits" />
					  </form>
						<?php 
						} else { 
							echo '<span style="float: left;">'. JText::_("ADS_PACKAGES_FREE_ENABLED") .' </span><br />';
							echo JText::_("ADS_PACKAGES_FREE_NOT_DEFINED");
						}	
						?>
				</fieldset>
			</div>
	<?php }

	function toggle_publishing(){
		
		$cid = JRequest::getVar("cid",array());
		if(!is_array($cid))
			FactoryLayer::redirect("index.php?option=".APP_EXTENSION."&task=paymentitemsconfig&itemname=packages");

		foreach ($cid as $c => $id){
			$this->_db->setQuery("UPDATE ".PAYTABLE_PACKAGES." SET status=1-status WHERE id = '{$id}'");
			$this->_db->query();
			
		}
		FactoryLayer::redirect("index.php?option=".APP_EXTENSION."&task=paymentitemsconfig&itemname=packages", JText::_("ADS_PACKAGES_TOGGLED"));
	}
	
	function del_package(){
		
        $id = JRequest::getInt('id','');
		$db = JFactory::getDbo();

		$db->setQuery("DELETE FROM ".PAYTABLE_PACKAGES." WHERE id = '{$id}'");
		$db->query();
		
		FactoryLayer::redirect("index.php?option=".APP_EXTENSION."&task=paymentitemsconfig&itemname=packages",JText::_("ADS_PACKAGES_REMOVED"));
	}
}

class price_packages_DBHelper{
	
	function getPackage($name){

		$db = JFactory::getDbo();
		if(is_int($name)){
			$db->setQuery("SELECT * FROM ".PAYTABLE_PACKAGES." WHERE id = '{$name}'  ORDER BY price+0 DESC ");
			$package  = $db->loadObject();
		} else {
			$db->setQuery("SELECT * FROM ".PAYTABLE_PACKAGES." WHERE name = '{$name}'  ORDER BY price+0 DESC ");
			$package  = $db->loadObject();
		}

		return  $package;
	}
	
	function getPackages(){
		$db = JFactory::getDbo();
		$db->setQuery("SELECT * FROM ".PAYTABLE_PACKAGES."  ORDER BY price+0 DESC ");
		$packages  = $db->loadObjectList();

		return  $packages;
	}
	
	function getFrontPackages(){

		$my = JFactory::getUser();
		$user_free_package = price_packages_DBHelper::getUserFreePackage($my->id);
		
		if ($user_free_package != null)
			$had_free_package = $user_free_package->package_free;
		else 
			$had_free_package = 0;
			
		$where = '';
		if ($had_free_package == 0) {
			//he can get free package once
			$where = 'price >= 0';
		} else {
			$where = 'price > 0';
		}
		
		$db = JFactory::getDbo();
		$db->setQuery("SELECT * FROM ".PAYTABLE_PACKAGES." WHERE status=1 AND $where ORDER BY ordering ASC ");
		$packages  = $db->loadObjectList();

		return  $packages;
	}
	
	function getUserFreePackage($userid) {
		
		$db = JFactory::getDbo();
		$db->setQuery("SELECT c.* FROM `#__ads_user_credits` c WHERE userid = $userid ");
		//$free_package  = $db->loadObjectList();
		$free_package  = $db->loadObject();
		
		return $free_package;
		
	}
	
}

?>