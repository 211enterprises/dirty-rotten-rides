<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

// no direct access
//defined( '_JEXEC' ) or die( 'Restricted access' );
//require_once(dirname(__FILE__).DS."pricing_object.php");
require_once(ADS_COMPONENT_PATH."/plugins/pricing/pricing_object.php");
require_once(dirname(__FILE__).DS."price_maincredit.php");
require_once(ADS_COMPONENT_PATH."/helpers/helper.php");
require_once(dirname(__FILE__).DS."price_pay_image.php");

class price_listing extends generic_pricing
{

	var $_param_members = array(
		'preferential_categories', 
		'default_price_categories', 
		'prices_categories', 
		'commisionable_categories',
		'time_valability', 
		'time_valability_prices'
		
	);
	
    function price_listing(&$db) {
        $this->classname = 'price_listing';
        $this->classdescription='Module for buying credits';
        parent::generic_pricing($db);
        $this->itemname = 'listing';
    }
    
    function getPrice($d){
    	
		//return $this->price;
    	
		if (isset($this->preferential_categories) && $this->preferential_categories == 1) {
			$cati = JRequest::getVar('category','');
			$id = JRequest::getVar('id','');
			
			if ( isset($cati) && $cati>0 )
				return price_listing_DBHelper::getPrice($cati);
				
			elseif ( isset($id) && $id>0 && $cati = price_listing_DBHelper::getCategory($id) )
				return price_listing_DBHelper::getPrice($cati);
				
			else	
				return $this->price;
				
		} else {
			return $this->price;
		}

    }

    function setRequestVar($var,$value)
    {
        $_REQUEST[$var]=$value;
        if (isset($_POST[$var])) $_POST[$var]=$value;
        if (isset($_GET[$var])) $_GET[$var]=$value;
    }
    
    function checktask($task,$d) {
        $Itemid = JRequest::getVar('Itemid', 0, 'REQUEST', 'integer');
        $mainframe = JFactory::getApplication();

        $language = JFactory::getLanguage();
        $language->load('com_adsman');
        $lng = substr( $language->get("tag") , 0, 2);
        if (file_exists(ADS_COMPONENT_PATH."/plugins/payment/pay_paypal.$lng.php"))
            require_once(ADS_COMPONENT_PATH."/plugins/payment/pay_paypal.$lng.php");
        else
            require_once(ADS_COMPONENT_PATH."/plugins/payment/pay_paypal.en.php");

        //require_once(ADS_COMPONENT_PATH."/plugins/payment/pay_paypal.en.php");

        $my = JFactory::getUser();
        $db = JFactory::getDbo();
        $session	= &JFactory::getSession();

        $credits = $this->getCredits($my->id);
        //$id = JArrayHelper::getValue($d,'id',null,'int');
        $id = JRequest::getVar('id', null);
        $islogged = 0;
        $was_payed = 0;
        $category = JRequest::getVar('category', null);

        if ( $id ) {
            $sql_pay = "SELECT COUNT(*) FROM ".PAYTABLE_PAYLOG." WHERE object_id = '$id' AND itemname = '{$this->itemname}' AND status='ok' ";
            $db->setQuery($sql_pay);
            $was_payed = $db->loadResult(); // payment done

            //$sql = "SELECT COUNT(*) FROM ".PAYTABLE_PAYLOG." WHERE object_id = '{$id}' AND itemname = '{$this->itemname}' AND (coalesce(status,'') <> 'ok')";
            $sql = "SELECT COUNT(*) FROM ".PAYTABLE_PAYLOG." WHERE object_id = '$id' AND itemname = '{$this->itemname}' AND ( `status` <> 'ok' OR ISNULL(`status` ) ) ";
            $db->setQuery($sql);
            $islogged = $db->loadResult(); //payment started

        }

        // A credit Price
        $this_price = $this->price;

        if ($task=='republish' || $task=="new" || ( $task=="edit" && !$id  ) ) {

            // display Price foe listing
            $credits_quant = price_listing_DBHelper::getQuant();

            // IF FREE, than,.. carry on
            if ($credits_quant==0 || $this_price==0){
                return false;
            }
            // if use packages or credit direct - link
            echo sprintf(ads_paypal_credit_situation, $credits);

            $link_packages = JRoute::_("index.php?option=com_adsman&task=buy_packages&Itemid=$Itemid");
            $get_credits =' <a href="'.$link_packages.'">'.JText::_("ADS_GET_CREDITS").'</a>';
            echo sprintf(ads_paypal_buy_credits, $get_credits);

            if ( isset($this->time_valability) && ($this->time_valability == 1) ) {

                echo ads_paypal_credit_valability;
                if( $credits == 0 )
                    echo sprintf(ads_paypal_valability_saved_tmp, $this->price, JText::_("ADS_CREDITS"));
            } else {
                // if not free print the price
                echo sprintf(ads_paypal_listing_is_not_free_default, $this_price);

                if( $credits <= 0 ) {
                    echo sprintf(ads_paypal_saved_tmp, $this_price, JText::_("ADS_CREDITS"));
                }
            }

            return false;

        } elseif ($task =="edit" && $id) {

                echo sprintf(ads_paypal_credit_situation, $credits);

                $link_packages = JRoute::_("index.php?option=com_adsman&task=buy_packages&Itemid=$Itemid");
                $get_credits =' <a href="'.$link_packages.'">'.JText::_("ADS_GET_CREDITS").'</a>';
                echo sprintf(ads_paypal_buy_credits, $get_credits);

            if ($islogged==1) {
                echo JText::_(ads_paypal_allready_processing);
                return false;
            }
        }
		
        if ( $task=="extend_add" ) {
        	
			$credits = $this->getCredits($my->id);
            //$credits_quant = price_listing_DBHelper::getQuant();

			if ($credits <= 0) {
				$this->ShowPurchaseDialog($d);
				return true;
				
            } elseif ($task=="extend_add") {
            	// CONSUME CREDITS
                $this->_db->setQuery("SELECT price FROM #__ads_pricing WHERE itemname = '".$this->itemname."' ");
				$credits_price = (int)$this->_db->LoadResult();
                $this->_db->setQuery("UPDATE #__ads_user_credits SET credits_no=credits_no - ".$credits_price." WHERE userid='$my->id' ");
                    //$this->_db->setQuery("UPDATE #__ads_user_credits SET credits_no=credits_no - 1 WHERE userid='".$my->id."' ");
    		    $this->_db->query();
            }
        }
        
        if ($task == 'save' ) {
            $video_url = JRequest::getVar('video-link', '');
            $delete_video = JRequest::getVar('delete_video', '');
            $credits_quant = price_listing_DBHelper::getQuant(); //1 listing
            $posted_pm_values = JRequest::get('POST');

            // IF FREE, than,.. carry on
            if ( (isset($credits_quant) && $credits_quant==0) || (isset($this_price) && $this_price==0) ) {
            //if ($credits_quant==0 || $this_price==0){
                return false;
            }

            if ($id && !AdsUtilities::isMyAdd($id)) {
                JError::raiseWarning(1,JText::_("ALERTNOTAUTH"));
                return;
            }

            $add = new JAdsAddsman($db);
            $orig_status_state = 0;

            // calculate total needed credits
            $prices = AdsUtilities::add_credits_calculate($id);

            $total_price_ad = $prices['total_price_ad'];
            $listing_price = $prices['listing_price'];
            $main_img_price = $prices['main_img_price'];
            $pictures_price = $prices['pictures_price'];
            $featured_price = $prices['featured_price'];
            $needed_credits = $prices['needed_credits'];
            $ad_images_price = 0;
            $ad_featured_price = 0;
            $edit_featured_price_ad = 0;
            $count_replace_images = 0;

            if($id) {
                $add->load($id);
                if (isset($posted_pm_values['featured'])) {
                        $orig_featured_state = $add->featured;
                        if (($orig_featured_state != $posted_pm_values['featured']) || ($add->status == 0))  {
                            $ad_featured_price = $featured_price;
                        }
                    }
                
                if ($add->status) { // if is a published ad
                    $orig_status_state = $add->status;
                }

                $orig_has_main_image = ($add->picture != null) ? 1 : 0; // had main img
                $orig_images_count = $add->getCountImages(); // initial no if img

            }

            //consume images credits if pay image enabled
            $isImageEnabled = price_pay_image_DBHelper::getEnabledPayImage();

            if ($isImageEnabled ) {
                $img_prices = price_pay_image_DBHelper::getPayImageAmount();

                $delete_pictures = isset($posted_pm_values['delete_pictures']) ? $posted_pm_values['delete_pictures'] : null;
                $count_replace_db_pictures = AdsUtilities::getPicturesReplace($id);

                $count_replace_images = (int)$delete_pictures + (int)$count_replace_db_pictures;

                $count_replace_main = AdsUtilities::getMainImgReplace($id);
                $delete_main_picture = isset($posted_pm_values['delete_main_picture']) ? $posted_pm_values['delete_main_picture'] : '';

                $nrfiles_images =  $prices['nrfiles_images']; // how many img were uploaded
                $is_main_pic = $prices['is_main_pic']; // if main img uploaded

                $orig_images_count = $add->getCountImages(); // initial no if img
                $orig_has_main_image = ($add->picture != null) ? 1 : 0; // had main img

                if ( ($orig_status_state == 1 && $count_replace_main == 1) || ($delete_main_picture == 1) ) {
                    $main_img_price = 0;
                } else {
                    if ( (isset($orig_has_main_image) && !$orig_has_main_image && $is_main_pic ) || ($orig_has_main_image && $add->status == 0)) {
                        $main_img_price = price_pay_image_DBHelper::getCreditImg(0);
                    }
                }

                if ( isset($orig_images_count) && $orig_images_count && $add->status == 0) {
          		    $total_images = $nrfiles_images + $orig_images_count;
                    $pictures_price = $img_prices[$total_images];
                } elseif ($nrfiles_images > 0 && $nrfiles_images > $count_replace_images) {
                    $total_images = $nrfiles_images - $count_replace_images;
                    
		            if ($orig_images_count && $add->status == 0) {
		                $total_images = $nrfiles_images + $orig_images_count;
		            }

                    $pictures_price = $img_prices[$total_images];
                } else {
                    // no pay, just replace payed images
                    $pictures_price = 0;
                }

                $ad_images_price = $main_img_price + $pictures_price;
            }

            $edit_featured_price_ad = $ad_images_price + $ad_featured_price;

            if ($id && $add->status) {
                $total_edit_price_ad = $edit_featured_price_ad;
            } else {
                $total_edit_price_ad = $edit_featured_price_ad + $credits_quant;
            }

			$total_price_ad = (!$add->id) ? $prices['total_price_ad'] : $total_edit_price_ad;

            // IF NO CREDITS unpublished!
            if( $credits < $total_price_ad ) {
                if (!$id)
                {
                    JError::raiseNotice(0,JText::_('Your new ad was saved as unpublished until you purchase Listing Credits!'));
                    // Prevent publishing unpublished Ads with no money
                    $add->status = 0;
                    $this->setRequestVar('status',0);
                } else {
                    $add->status = $orig_status_state;
                    if (!$add->status)
                        JError::raiseNotice(0,JText::_('Your ad was saved as unpublished until you purchase Listing Credits!'));
                    // Prevent publishing unpublished Ads with no money
                    $this->setRequestVar('status',$orig_status_state);
                }

                if (!$add->status)
                {
                    //remember to show him the purchase dialog
                    $session->set("payment_listing_dialog", 1);
                }
            }

            // save the Post
            $session->set("session.token", null);
            $session->set("token", null);
            $controller = new AdsController();
			$model = &$controller->getModel( 'Adsman','adsModel' );
            $model->bindAdd( $add );

            if ( isset($posted_pm_values["cat"]) && $posted_pm_values["cat"]!="" )
                $add->category = $posted_pm_values["cat"];
            elseif ( isset($posted_pm_values["category"]) && $posted_pm_values["category"]!="" )
                $add->category = $posted_pm_values["category"];

            $err = $model->saveAdd( $add );

            if( $err !== true ) {
                $session->set("tmp_ad", serialize($add) ,"tmpadserialized" );
                $mainframe->redirect("index.php?option=com_adsman&view=adsman&task=new&Itemid=$Itemid",$err); //no need to consume credits, there is an error
            }

            if ($add->id) {
                // after ad is saved, save video url too
                if (isset($video_url) && $video_url != '' && !$delete_video) {
                    $saved_url = $model->saveUrls($add->id,$video_url);

                    if( $saved_url !== true ) {
                        $session->set("tmp_ad", serialize($add) ,"tmpadserialized" );
                        $mainframe->redirect("index.php?option=com_adsman&view=adsman&task=new&Itemid=$Itemid",$err);
                    }
                }
            }

            // consume credits to publish
            if ($add->status && !$was_payed ) {
                if ( $credits >= $total_price_ad ) {
                    if ($id) { // and published

                        // charge only new added images
                        if ( ( $pictures_price != 0) || ($main_img_price != 0)  ) {

                            price_listing::consume_credits($ad_images_price, $add->userid);
                        }

                        // charge if added feature
                        if (isset($posted_pm_values['featured']) && $featured_price != 0) {
                            price_listing::consume_credits($ad_featured_price, $add->userid);
                            $this->_db->setQuery("UPDATE #__ads SET featured='".$posted_pm_values['featured']."' where id='$add->id'");
                            $this->_db->query();
                        }

                    } else { // new ad

                        //consume listing credits
                        price_listing::consume_credits($credits_quant, $add->userid);

                        //consume images credits if pay image enabled
                        $isImageEnabled = price_pay_image_DBHelper::getEnabledPayImage();

                        if ($isImageEnabled ) {
                            $ad_images_price = $main_img_price + $pictures_price;

                            if ( ( $pictures_price != 0) || ($main_img_price != 0)  ) {
                                price_listing::consume_credits($ad_images_price, $add->userid);
                            }
                        }

                        //consume featured credits if set featured
                        if (isset($posted_pm_values['featured']) && $featured_price != 0) {
                            price_listing::consume_credits($featured_price, $add->userid);
                        }
                    }

                    $mainframe->redirect( JURI::root()."index.php?option=com_adsman&view=adsman&Itemid=$Itemid&task=details&id=" .$add->id, JText::_("ADS_SAVED"). ' <br />'.JText::_('ADS_EDIT_24HOURS') );
                    return false;

                } else {
                    JError::raiseNotice(0,JText::_("You don't have enough credits to add features!"));
                    $mainframe->redirect( JURI::root()."index.php?option=com_adsman&view=adsman&Itemid=$Itemid&task=details&id=" .$add->id);
                    return false;
                }
            } else {
                // if not payed and status unpublished
                if (!$was_payed && $session->get("payment_listing_dialog")) {
                    //check if we want ot show purchase dialog
                    $session->set("payment_listing_dialog", null);
                    $d["category"] 	= $add->category;
                    $d["id"] 		= $add->id;
                    $d['needed_credits'] = $needed_credits;
                    $d['total_price_ad'] = $total_price_ad;
                    $d['itemname'] = $this->itemname;
                    $this->info["payment_description"] = $add->title;

                    $this->ShowPurchaseDialog($d);
                    return true;

                } else {  //was payed or $session->get("payment_listing_dialog") is null

                    // check if still in date interval
                    $config = JFactory::getConfig();
		            $offset = $config->get("offset");
                    if ( defined("ads_opt_component_offset") && ads_opt_component_offset!="" )
                        $offset = ads_opt_component_offset;

                    if ($offset != 'UTC') {
                        $corelate = 0;
                    } else {
                        $corelate = 1;
                    }

                    $ad_images_price = 0;

                    if ($isImageEnabled ) {
                        $orig_has_main_image = ($add->picture != null) ? 1 : 0; // had main img

                        if ( (!$orig_has_main_image && $is_main_pic ) || ($add->status == 0 && $is_main_pic) ) {
                            $main_img_price = price_pay_image_DBHelper::getCreditImg(0);
                        }

                        if ( ($nrfiles_images > 0 && $nrfiles_images > $count_replace_images) || ($nrfiles_images > 0 && $add->status == 0) ) {
                            $total_images = $nrfiles_images - $count_replace_images;
                            $pictures_price = $img_prices[$total_images];
                        }

                        $ad_images_price = $main_img_price + $pictures_price;
                    }

                    $edit_featured_price = 0;
                    if (isset($posted_pm_values['featured']) && $featured_price != 0) {
                        $add_featured_status = 	$add->featured;

                        if (isset($posted_pm_values['featured'])) {
                            $orig_featured_state = $add->featured;
                            if ($orig_featured_state != $posted_pm_values['featured'] || $add->status == 0) {
                                $edit_featured_price = $featured_price;
                            }
                        }
                    }

                    $listing_price = 0;
                    if ($add->status == 0) {
                        $listing_price = $credits_quant;
                    }

                    $total_price_ad = $ad_images_price + $edit_featured_price + $listing_price;

                    if ( $credits >= $total_price_ad ) {
                        //consume images credits if pay image enabled
                        if ($isImageEnabled ) {
                          if ( ( $pictures_price != 0) || ($main_img_price != 0)  ) {
                                price_pay_image::consume_credits($ad_images_price, $add->userid);
                          }
                        }
                        // check added feature - pay feature? is done in bronze, silver,... plugin
                        if (isset($posted_pm_values['featured']) && $featured_price != 0) {
                            $add_featured_status = 	$add->featured;

                            if ( $edit_featured_price != 0) {
                                price_listing::consume_credits($edit_featured_price, $add->userid);
                                $this->_db->setQuery("UPDATE #__ads SET featured='".$posted_pm_values['featured']."' where id='$add->id'");
                                $this->_db->query();
                            }
                        }

                        if ($add->status == 0) {
                            //consume listing credits
                            price_listing::consume_credits($credits_quant, $add->userid);
                        }

                        // UPDATE OBJECT
                        $this->_db->setQuery(" UPDATE #__ads SET status='1' where id='".$add->id."' ");
                        $this->_db->query();

                        $mainframe->redirect( JURI::root()."index.php?option=com_adsman&view=adsman&Itemid=$Itemid&task=details&id=" .$add->id, JText::_("ADS_SAVED") );
                        return false;
                    } else {
                        JError::raiseNotice(0,JText::_("You don't have enough credits to add features !"));
                        $mainframe->redirect( JURI::root()."index.php?option=com_adsman&view=adsman&Itemid=$Itemid&task=details&id=" .$add->id);
                        return false;
                    }
                }
			}
            return false;
        }
       
        if ($task=='buy_package') {	
            price_packages::ShowPurchaseDialog($d);
            return true;
        }
        
        if ($task=='buy_packages' ){
            price_packages::showPackagesFull($d);
            return true;
        }
        
        if ($task=='payment'){
	        
        	// The Order Number (not order_id !)
	        $invoice =  trim(stripslashes($_POST['invoice']));
	        $payment_status = trim(stripslashes($_POST['payment_status']));
	        
	        if($invoice){
	        	require_once(dirname(__FILE__).DS."..".DS."payment".DS."payment_log.php");
				$log = new JTheFactoryPaymentLog($this->_db);
				$log->set('_tbl_key', "invoice");
						
				$log->load($invoice);
				
				// If pending (IPN not runned yet for misc reasons )
				if( ( $payment_status == "Completed" || $payment_status == "Processed" || $payment_status == "In-Progress" || $payment_status == "Pending" ) && $log->status!="ok"){
					if($aid = $log->object_id)
						$mainframe->redirect('index.php?option=com_adsman&task=details&id='.$aid,"Payment Succesfully! Waiting for approval!");
					else
						$mainframe->redirect('index.php?option=com_adsman',"Payment Succesfully! Waiting for approval!");
					return true;
				}else{
					if($aid = $log->object_id)
						$mainframe->redirect('index.php?option=com_adsman&task=details&id='.$aid,"Payment Succesfully!");
				}
	        }
	        
        }
        if ($task=='importcsv' || $task=='bulkimport'){
            echo adsman_payment_listing_bulkimport;
            echo "<br>";
            return true;
        }
        return false;
    }
    
    
	// CONSUME CREDITS
    function consume_credits($nr_credits=1,$user_id){
    	
    	$nr_credits = (int)$nr_credits;
        $db = JFactory::getDbo();
 		$db->setQuery("UPDATE #__ads_user_credits SET credits_no=credits_no - ".$nr_credits." WHERE userid='$user_id' ");
		$db->query();
    }
    
    /**
     * This is the same for Featuring Listing
     * override function in pricing class for custom handling
     * 
     *
     * @param int/object $order (ad id or log object)
     */
    function update_object($order,$credits_amount,$total_price_ad,$featured_ad) {
    	// 
        $db = JFactory::getDbo();
        $my= JFactory::getUser();
    	$user_id = $my->id;

    	//$credits = 1;
    	if ( is_object($order) ){
    		$aid = $order->object_id;
	    	$user_id = $order->userid;
	    	//$credits = $order->amount / $this->price;
	    	
    	}else 
    		$aid = $order;

    	// UPDATE OBJECT	
    	$db->setQuery("UPDATE #__ads SET featured='$featured_ad' where id='$aid'");
		$db->query();
		
		$db->setQuery("UPDATE #__ads SET status='1' where id='$aid'");
		$db->query();

		// CONSUME CREDITS
		$db->setQuery("UPDATE #__ads_user_credits SET credits_no=credits_no - $total_price_ad WHERE userid='$user_id' ");
		$db->query();
    }

    function processTemplate($task,$d, &$smarty){
    	
		$Itemid = JRequest::getVar('Itemid', 0, 'REQUEST', 'integer');
		$mainframe = JFactory::getApplication();
		
		$id = JRequest::getVar('id', null);

        if($task=='republish' || $task=="new" || ( $task=="edit" && !$id  ) ){
				/**
				 * End Date Template processing
				 * 
				 */
				if( isset($this->time_valability) && $this->time_valability == 0 ) {
					$smarty->assign("time_limited", 0 );
					
					// get price listing
					$catid = (int)JRequest::getVar('category',0);
					// if edit ad - get category
					$listing_price = 0;

					if ( isset($this->preferential_categories) && $this->preferential_categories == 1) {

						//if ($this->default_price_categories && $this->prices_categories == 0) {
						//	$price = $this->default_price_categories;
						//} else {
							if ( isset($catid) && $catid > 0 ) {
								$listing_price = AdsUtilities::getCategoryPrice($catid,$this->default_price_categories,$this->preferential_categories,$this->commisionable_categories);
							}
							else	
								$listing_price = $this->price;
						//}
					}
					
					$smarty->assign("credits_category_ini", $listing_price);

				} else {

				  if ( isset($this->time_valability) && $this->time_valability == 1 ) {
					$params_arr = array();
					if($this->time_valability_prices  && trim( strip_tags($this->time_valability_prices) ) !=""  )
						$params_arr = explode("|", trim( strip_tags($this->time_valability_prices) ) );
						
					$valabs = array();
					$vprices = array();
					
					if (count($params_arr) > 0) {
						foreach ($params_arr as $p){
							if ($p) {
								$pi = explode(":",$p);
								$valabs[] = $pi[0];
								$vprices[$pi[0]] = $pi[1];
							}
						}
					}
					
					sort($valabs);
					
					$jDoc = JFactory::getDocument();
					
					$d = "document.getElementById('end_date').value = ''";
					$js_block = "";
					$js_block .= "
						var pricingCurrency = 'credits';
						var VList = new Array();
						var VPriceList = new Array();

						function showValabilityPrice(price){
							document.getElementById('pricing_info').innerHTML ='".JText::_("ADS_PRICE_LISTING_VALABILITY")."'+ price+ ' '+pricingCurrency;
							
						}
					";
					if (count($valabs) )
						foreach ($valabs as $k => $v){
						
						$rtime = Ads_Time::apendDays(Ads_Time::getNow(),$v);
						$rtime = date(ads_opt_date_format, $rtime);
						
						$js_block .= "
						\r\n
							VList[{$k}] = '{$rtime}';\r\n
							VPriceList[{$k}] = '".$vprices[$v]."';\r\n
						";
						}
												
						$jDoc->addScriptDeclaration($js_block);

						$smarty->assign("valabs_list", $valabs );
						$smarty->assign("valabs_prices", $vprices);
						$smarty->assign("valabs_default_val", $valabs[0]);
						$smarty->assign("valabs_currency", 'credits');
						$smarty->assign("time_limited", 1 );
					
				  }
				}
				
				//if pay image
				$isImageEnabled = price_pay_image_DBHelper::getEnabledPayImage();
				$img_prices = price_pay_image_DBHelper::getPayImageAmount();
				
				if ( $isImageEnabled ) {
					// display credits costs for selected option
					$jDocImg = & JFactory::getDocument();
					$js_blockImg = "";
					$js_blockImg .= "
						var pricingCurrency = 'credit(s)';
						var iList = new Array();
						var iPriceList = new Array();
						
						function showImagePrice(price) {
							document.getElementById('pricing_img_info').innerHTML ='".JText::_("ADS_PRICE_LISTING_IMAGES")."'+ price + ' '+pricingCurrency;
						}
					";
			
					if (count($img_prices) ) {
						
						//$first_index = key($img_prices); 
						//$first_index = 'none';
						
						$js_blockImg .= " 
						\r\n
							iList['none'] = 'none';\r\n
							iPriceList['none'] = '0';\r\n
							";
							
							
							foreach ($img_prices as $k => $v){
							
								$js_blockImg .= "
								\r\n
									iList[{$k}] = '{$k}';\r\n
									iPriceList[{$k}] = '".$v."';\r\n
								";
							}
						$smarty->assign("credits_row_ini", 0);
						
						$jDocImg->addScriptDeclaration($js_blockImg);
					}
				}
				
				$img_main_price = price_pay_image_DBHelper::getMainImageAmount(); //credits to pay for main img
				$smarty->assign("img_main_price", $img_main_price);
		}
    }	
    
/**
 * 
 * ADMINISTRATION TASKS
 * 
 *
 */
    
    function show_admin_config() {
        JHTML::_('behavior.formvalidation');
		$jdoc = JFactory::getDocument();
    	
		$categories_exist = 0;
		$this->_db->setQuery("SELECT count(*) as n FROM ".APP_CATEGORY_TABLE);
		$categories_exist = $this->_db->loadResult();
	
// Price Per Category Enabled
		$preferential_categories = ( isset($this->preferential_categories) ) ? $this->preferential_categories : 0;
		$prices_categories 		 = ( isset($this->prices_categories) ) ? $this->prices_categories : null;
		$time_valability 		 = ( isset($this->time_valability) ) ? $this->time_valability : null;
	
		if($categories_exist==0)
			$preferential_categories = 0;

		$commisionable_categories = null;
		$catoblist = array();
		if ( $preferential_categories ){
			$commisionable_categories = (isset($this->commisionable_categories))?$this->commisionable_categories:null;
			if($commisionable_categories){
				$carr = explode(",",$commisionable_categories);
				for($ii=0; $ii< count($carr); $ii++){
					$t = new stdClass();
					$t->value = $carr[$ii];
					$catoblist[$ii] = $t;
				}
				$ListingPricesCategories = price_listing_DBHelper::getCategoriesSelect("tmp_cat", $commisionable_categories);
			}
		}
		
		$categories_select = price_listing_DBHelper::getCategoriesHTML("commisionable_categories[]",$catoblist,array("multiple"=>1));
		$customListingPricesCategories = price_listing_DBHelper::getCategoriesPrices($commisionable_categories);
		$time_valability_mode = (int)($time_valability);
			
?>
	<script type="text/javascript">
		// GUI for AJAX
		function showProgress(tab) {
			document.getElementById("tab"+tab+"_img").style.display='block';
			document.getElementById("tab"+tab+"_content").style.display='none';
			setTimeout("GUIAjax_showContent("+tab+")", 500);
		}
		function GUIAjax_showContent(tab) {
			document.getElementById("tab"+tab+"_img").style.display='none';
			document.getElementById("tab"+tab+"_content").style.display='block';
		}
	</script>

    <style type="text/css">
       	.red_border { border: 2px solid red; }
  	</style>

        <script type="text/javascript">
        	Joomla.submitbutton = function(task)
        	{
        		if (task == 'savepaymentitemsconfig' || task == 'paymentitemsconfigapply' ) {

                    var is_valid = mainPriceValidate();

                    if (is_valid == 0) {
                        document.getElementById('price').setAttribute("class", "red_border");
                        return false;
                    } else if (is_valid == 2)  {
                        document.getElementById('default_price_categories').setAttribute("class", "red_border");
                        return false;
                    }

                    else {
                        Joomla.submitform(task, document.getElementById('mainForm'));
                        return true;
                    }
        		}
        	}
        </script>

    <script type="text/javascript">
        function mainPriceValidate() {
           // If price listing is enabled, basic price must be > 0
            var has_mainprice = document.getElementById('price');

            if (typeof(has_mainprice) != "undefined" && has_mainprice != null) {
                var mainprice = document.getElementById('price').value;
            }
            else {
                var mainprice = 0;
            }

            if ( (mainprice == "" || mainprice == 0) && has_mainprice) {
                alert('Please enter a price bigger than 0 for Payment Item Price ');
                return 0;
            }

            var radioButtonsCategories = document.getElementsByName('preferential_categories');
            var preferential_categories_enabled = false;

            if (radioButtonsCategories[1].checked) {
                preferential_categories_enabled = true;
            } else {
                preferential_categories_enabled = false;
            }

            if (preferential_categories_enabled) {

                var has_defaultCategprice = document.getElementById('default_price_categories');

                if (typeof(has_defaultCategprice) != "undefined" && has_defaultCategprice != null) {
                    var defaultCategprice = document.getElementById('default_price_categories').value;
                }
                else {
                    var defaultCategprice = 0;
                }

                if ( (defaultCategprice == "" || defaultCategprice == 0) && has_defaultCategprice) {
                    alert('Please enter a price bigger than 0 for Default Category Listing Price ');
                    return 2;
                }
            }

            return true;

        };

    </script>

	<form action="<?php echo JRoute::_('index.php'); ?>" method="post" name="adminForm" id="mainForm" class="form-validate" onsubmit="return mainPriceValidate();">

<?php

	$js = null;
	$js = price_listing_AdminHelper::addJsBlock();
	$jdoc->addScriptDeclaration($js);
	$js = null;
	$js = "window.addEvent('domready', function(){ 
	
		toggle_Price_Category({$preferential_categories}); 
		toggle_Custom_Prices({$prices_categories});
		toggle_TimeMode({$time_valability_mode});
		});";
	$jdoc->addScriptDeclaration($js);

	//Load switcher behavior
	JHTML::_('behavior.switcher');
	
	//Load tooltip behavior
	JHTML::_("behavior.tooltip");

	//Load modal behavior
	JHTML::_("behavior.modal");
        JHTML::_('behavior.mootools');
		
	jimport('joomla.html.pane');
	$tabs = &JPane::getInstance('Tabs');
?>
<fieldset>
<legend><?php echo JText::_("ADS_PLG_PAYMENT_ITEM")." ".$this->itemname;?></legend>

<table class="adminlist width100">
   <tr>
      <td valign="top" class="paramlist key" style="width: 320px; height:500px; min-height:500px !important;">
				<br />
				<fieldset>
					<legend><img src="<?php echo JURI::root();?>administrator/components/com_adsman/thefactory/fields/assets/calendar.png" class="ads_pricing_calendar" />&nbsp;<?php echo JText::_( 'ADS_PLG_VALABILITY_SETTINGS' ); ?></legend>
					<table class="admintable" style="width:300px !important;">
						<tr>
							<td class="paramlist key" id="unlimited" style="width:250px !important;">
								<?php echo JText::_("ADS_UNLIMITED_PUBLISHING");?> <br /><?php echo JText::_("ADS_UNLIMITED_PUBLISHING_INFO");?>
							</td>
							<td>
								<input type="radio" name="time_valability" value="0" <?php echo ($time_valability)?'':'checked';?> onclick="toggle_TimeMode(0);" />
							</td>
						</tr>
						<tr>
							<td class="paramlist key" id="preconfigured">
								<?php echo JText::_("ADS_VALABILITIES_RANGE_PUBLISHING");?>
							</td>
							<td>
								<input type="radio" name="time_valability" value="1" <?php echo ($time_valability)?'checked':'';?> onclick="toggle_TimeMode(1);" />
							</td>
						</tr>
					</table>
				</fieldset>
				<?php price_listing_AdminHelper::showCommonSettingsTab(); ?>
				<?php price_listing_AdminHelper::showCurrentSettingsStatus();?>
				<?php price_listing_AdminHelper::showHelpBar();?>
		</td>
		
		<td valign="top" class="td_pricing_table">
			<?php
				echo $tabs->startPane("configPane");
				echo $tabs->startPanel(JText::_("ADS_PLG_TAB_USER_DEFINED"),"tab0");
			?>
				<img id="tab0_img" src="<?php echo JURI::root();?>/components/com_adsman/img/ajax-loader.gif" style="display:none;" class="ads_noborder" />
				<div id="tab0_notice" style="display:none;">
					<?php echo JText::_("ADS_PLG_ENABLE");?> <a href="#" onclick="popup('unlimited'); return false;"><?php echo JText::_("ADS_UNLIMITED_PUBLISHING");?><?php echo JText::_("ADS_UNLIMITED_PUBLISHING_INFO");?></a>
				</div>
				
				<div id="tab0_content">
					<table width="100%" class="adminlist" border="0">
						<tr>
							<td valign="top" width="370">
							  <fieldset>
								<legend>
									<img src="<?php echo JURI::root();?>administrator/components/com_adsman/thefactory/fields/assets/categories.png" class="ads_pricing_calendar" style="width: 32px;" />
									<!--<span style="float: left;">--><?php echo JText::_("ADS_PLG_PRICE_LISTING_CAT");?><!--</span>-->
								</legend>
								<table class="adminlist" width="100%">
									<tr>
										<td class="paramlist value" align="center">
											<div id="price_listing_categories">
												<span class="tooltip_right" style="font-weight: bold;"><?php echo JText::_("ADS_PLG_PRICE_LISTING_CAT");?>&nbsp;</span>
												<fieldset id="preferential_categories" class="radio" style="border:0 none !important;">
													<?php echo FactoryHTML::selectBooleanList("preferential_categories", " onclick='sel=this.value; toggle_Price_Category(sel);'", $preferential_categories ); ?>
												</fieldset>	
											</div>	
										</td>
									</tr>
								</table>
								<table class="adminlist" width="100%" border="0">
									<tr>
									    <td class="paramlist key" title="<?php echo JText::_("ADS_PLG_DEFAULT_CAT_PRICE_HELP");?>" align="right"><?php echo JText::_("ADS_PLG_DEFAULT_CAT_PRICE_TITLE");?>:</td>
										<td><input <?php if(!$preferential_categories) echo "disabled"; ?> type="text" id="default_price_categories" name="default_price_categories" value="<?php echo (isset($this->default_price_categories))?$this->default_price_categories:'';?>" style="width:40px;" /> ( <?php echo JText::_("ADS_CREDITS");?> ) </td>
									</tr>
									<tr>
										<td class="paramlist key" valign="top"><?php echo JText::_("ADS_PLG_ALL_CAT_PRICE_TITLE");?></td>
										<td><a href="javascript:allcategories();"><?php echo JText::_("ADS_PLG_SELECT_ALL_CATEG");?></a> | <a href="javascript:nocategory();"><?php echo JText::_("ADS_PLG_SELECT_NONE_CATEG");?></a><?php echo $categories_select; ?></td>
									</tr>
									<tr>
										<td colspan="2">
										 <table id="custom_prices" width="100%">
										 	<tr>
												<td class="paramlist key" title="<?php echo JText::_("ADS_PLG_CUSTOM_PRICES_HELP");?>" align="right"><?php echo JText::_("ADS_PLG_CUSTOM_PRICES_TITLE");?>:</td>
												<td><?php echo FactoryHTML::selectBooleanList("prices_categories", "  id='sl_prices_categories'  onclick='sel1=this.value; toggle_Custom_Prices(sel1);'", $prices_categories); ?></td>
											</tr>
										 </table>
										</td>
									</tr>
								</table>
								</fieldset>
							</td>
							<td valign="top" width="330">
																
								<div id="custom_prices_info" style="opacity:0.9; filter:alpha(opacity=90); background:#FFF; position:absolute; z-index:10000; height:200px; width:320px; ">
									<img src="<?php echo JURI::root();?>administrator/components/com_adsman/img/notice-note.png" style="text-align:top;" />
									<?php echo JText::_("ADS_PLG_ENABLE");?> <a href="#price_listing_categories" onclick="popup('price_listing_categories'); return false;" ><?php echo JText::_("ADS_PLG_PRICE_LISTING_CAT");?></a> and <a href="#custom_prices" onclick="popup('custom_prices'); return false;" ><?php echo JText::_("ADS_PLG_CUSTOM_PRICES_TITLE");?></a><?php echo JText::_("ADS_PLG_CUSTOM_PRICES_FOR");?>:
									<br />
									<h2><?php echo JText::_("ADS_PLG_CUSTOM_PRICES_TITLE");?></h2><br />
									<?php echo JText::_("ADS_PLG_CUSTOM_PRICES_INFO");?>
								</div>
								
								<table id="idx_custom_pricelist" class="adminlist" border="0" style="position:absolute; margin-top: 1px; width:320px; vertical-align: top;">
									<tr>
										<td valign="top">
											<?php 
											if (isset($ListingPricesCategories) && count($ListingPricesCategories)>0) {
												price_listing_AdminHelper::showAddCustomPriceForm($ListingPricesCategories);
											}
											?>
										</td>
									</tr>
								</table>
								
							</td>
							<?php //if (isset($ListingPricesCategories) && count($ListingPricesCategories)>0) { ?>
							<td valign="top">
							<?php	price_listing_AdminHelper::showCustomPriceList($customListingPricesCategories, $this);
								//}
								?>
							</td>	
						</tr>
					</table>
				</div>
			<?php echo $tabs->endPanel(); ?>
			
			<?php echo $tabs->startPanel(JText::_("ADS_PLG_TAB_PRECONFIGURED"),"tab1"); ?>
			
				<img id="tab1_img" src="<?php echo JURI::root();?>/components/com_adsman/img/ajax-loader.gif" style="display:none;" class="ads_noborder" />
				<div id="tab1_notice" style="display:none;width:100% !important;">
					<?php echo JText::_("ADS_PLG_ENABLE");?> <a href="#" onclick="javascript:popup('preconfigured'); return false;"><?php echo JText::_("ADS_VALABILITIES_RANGE_PUBLISHING");?></a>
				</div>	
				<div id="tab1_content" class="width100">
					<table class="adminlist" width="100%">
						<tr>
							<td><?php price_listing_AdminHelper::showAddNewValabilityForm();?></td>
							<td valign="top"><?php price_listing_AdminHelper::showValabiltyList();?></td>
						</tr>
					</table>
				</div>
		<?php
			echo $tabs->endPanel();
			echo $tabs->endPane();
		?>
		</td>
	</tr>
</table>

	<input type="hidden" id="pl_task" name="action" value="" />
	<input type="hidden" name="option" value="<?php echo APP_EXTENSION;?>"/>
    <input type="hidden" name="task" value="savepaymentitemsconfig"/>
    <input type="hidden" name="itemname" value="<?php echo $this->itemname;?>"/>
</form>
</fieldset>

<?php
    }
    
    
    function save_admin_config()
    {
        $action = JRequest::getVar('action','');
        $commisionable_categories = JRequest::getVar('commisionable_categories',null,'REQUEST','array');

		//$commisionable_categories = JRequest::getVar('commisionable_categories',null,$_REQUEST,'array');
		if($commisionable_categories){
            $commisionable_categoriesVar = implode(",",$commisionable_categories);
            JRequest::setVar('commisionable_categories',$commisionable_categoriesVar);

            //$_REQUEST["commisionable_categories"] = implode(",",$commisionable_categories);
		} else {
            JRequest::setVar('commisionable_categories',null);
        }
			
        switch($action){
        	case "add_valability_pricing":
        		$this->add_valability_pricing();
				FactoryLayer::redirect("index.php?option=com_adsman&task=paymentitemsconfig&itemname=$this->itemname", JText::_("ADS_PLG_PRICE_ADDED"));
        		return;
        	break;
        	case "add_custom_pricing":
        		$this->add_custom_pricing();
				FactoryLayer::redirect("index.php?option=com_adsman&task=paymentitemsconfig&itemname=$this->itemname", JText::_("ADS_PLG_PRICE_ADDED"));
        		return;
        	break;
        	case "del_custom_price":
        		$this->delete_custom_pricing();
				FactoryLayer::redirect("index.php?option=com_adsman&task=paymentitemsconfig&itemname=$this->itemname", JText::_("ADS_PLG_PRICE_REMOVED"));
        		return;
        	break;
        	case "del_valability_price":
        		$this->del_valability_pricing();
				FactoryLayer::redirect("index.php?option=com_adsman&task=paymentitemsconfig&itemname=$this->itemname", JText::_("ADS_PLG_PRICE_REMOVED"));
        		return;
        	break;
        }


        $storePaymentItem = parent::save_admin_config();
        $task = JRequest::getCmd("task");

        if ($storePaymentItem) {
            if ($task == 'paymentitemsconfigapply') {
                // Redirect to the detail page.
                FactoryLayer::redirect( "index.php?option=com_adsman&task=paymentitemsconfig&itemname=$this->itemname",JText::_("ADS_SETTINGS_SAVED") );
            }

            FactoryLayer::redirect("index.php?option=com_adsman&task=paymentitems", JText::_("ADS_SETTINGS_SAVED"));
            //$mainframe->redirect( "index.php?option=com_adsman&task=listadds",JText::_("ADS_SETTINGS_SAVED") );
        } else {
            if ($task == 'paymentitemsconfigapply' ) {
                // Redirect to the detail page.
                FactoryLayer::redirect( "index.php?option=com_adsman&task=paymentitemsconfig&itemname=$this->itemname",JText::_("ADS_NOT_SAVED") );
            }

            FactoryLayer::redirect( "index.php?option=com_adsman&task=paymentitemsconfig&itemname=$this->itemname",JText::_("ADS_NOT_SAVED") );
        }


		FactoryLayer::redirect("index.php?option=com_adsman&task=paymentitems", JText::_("ADS_SETTINGS_SAVED"));
    }
    
    function delete_custom_pricing(){
        $cat = JRequest::getVar('cat','');
        $price_type = "listing";
        $database = JFactory::getDbo();
        $database->setQuery("DELETE FROM #__ads_custom_prices WHERE category = '{$cat}'  AND price_type ='$price_type' ");
        $database->query();
        //echo $database->_sql;exit;
    }
    
    function add_custom_pricing(){
    	
        $cat = JRequest::getVar('custom_pricing_category','');
        $price = (int)JRequest::getVar('custom_pricing_category_price','');
        $price_type = "listing";
        
        // some low validation: administrators ain't hackers
        if( $price >= 0){
	        $this->_db->setQuery("SELECT * FROM #__ads_custom_prices WHERE category = '{$cat}' AND price_type ='$price_type' ");
	        $this->_db->query();
	        $cur = $this->_db->getNumRows();
	        if($cur==0){
		        $this->_db->setQuery("INSERT INTO #__ads_custom_prices SET category = '{$cat}', price = '{$price}',price_type ='$price_type'");
		        $this->_db->query();
	        }
        }

    }
    
    function add_valability_pricing(){
        
      $valability   = (int)JRequest::getVar('valability','');
      $val_price 	= (int)JRequest::getVar('valability_price','');

      if( !$valability || !$val_price )
			FactoryLayer::redirect("index.php?option=com_adsman&task=paymentitemsconfig&itemname=$this->itemname", JText::_("ADS_PLG_PRICE_NOT_VALID"));
      	
	  $val_string = $valability.":".$val_price;
	  $params_arr = array();
	  if($this->time_valability_prices)
			$params_arr = explode("|",$this->time_valability_prices);
	  $params_arr[] = $val_string;
		
	  $_REQUEST["time_valability_prices"] = implode("|",$params_arr);
      $this->saveParams($_REQUEST);
      
    }
    
    function del_valability_pricing(){
        
      $valability = JRequest::getVar('valability','');
      if ($valability) {
			$params_arr = array();
			if($this->time_valability_prices)
				$params_arr = explode("|",$this->time_valability_prices);
				
			$new_params_arr = array();	
			foreach ($params_arr as $price)	{
				$price_interval = explode(":",$price);
				$price_interval = $price_interval[0];
				if ( $price_interval!=$valability ){
					$new_params_arr[] = $price;
				}
			}
			
			$_REQUEST["time_valability_prices"] = implode("|",$new_params_arr);
			
	      $this->saveParams($_REQUEST);

      }
      //FactoryLayer::redirect("index.php?option=com_adsman&task=paymentitemsconfig&itemname=$this->itemname", JText::_("ADS_PLG_PRICING_REMOVED"));
      
    }

}

class price_listing_AdminHelper extends price_object_AdminHelper {
	
	function showCommonSettingsTab(){
		?>
	<fieldset>
		<legend><img src="<?php echo JURI::root();?>administrator/components/com_adsman/thefactory/fields/assets/payment_listing.png" class="ads_pricing_calendar" style="width: 32px;" />
		<?php echo JText::_("ADS_PLG_PAYMENT_ITEM")." ".$this->itemname;?></legend>
        <table class="adminlist" width="100%" id="price_box">
        <tr>
            <td class="paramlist key" width="120px"><?php echo JText::_("ADS_PLG_PAYMENT_ITEM_PRICE");?>: </td>
            <td><input size="5" name="price" id="price" class="inputbox" value="<?php echo $this->price;?>"><?php echo JText::_("ADS_CREDITS");?></td>
        </tr>
       
        </table>
	</fieldset>
		<?php
	}

	function showCustomPriceList($customListingPricesCategories, &$this_pricing ){
		?>
		
			<table id="custom_price_list_table" class="adminlist">
				<thead>
					<tr id="idx_custom_pricelist_header">
						<th width="20" ></th>
						<th width="100"><?php echo JText::_("ADS_PLG_CATEGORY"); ?></th>
						<th width="200"><?php echo JText::_("ADS_PAYMENT_ITEM_PRICE"); ?></th>
					</tr>
				</thead>
	<?php	if( isset($customListingPricesCategories) && count($customListingPricesCategories)>0 )
				foreach( $customListingPricesCategories as $i => $customCategory ){ ?>
				<tr class="row<?php if($i%2) echo "1"; else echo "0";?>">
					<td align="right">
						<?php if($customCategory->price > 0) { ?>
							<a style="color:#FF0000 !important;" href="index.php?option=com_adsman&task=savepaymentitemsconfig&itemname=listing&action=del_custom_price&cat=<?php echo $customCategory->catid; ?>">
								<?php echo JText::_("ADS_PLG_REMOVE_X");?></a>
						<?php } ?>
					</td>
					<td><?php echo $customCategory->catname;?></td>
					<td align="center" >
					<strong>
						<?php if( isset($customCategory->price) && $customCategory->price >=0 ) 
								echo $customCategory->price; 
							  else 
								echo (isset($this_pricing->default_price_categories)) ? JText::_("ADS_PLG_DEFAULT_CATEGORY_PRICE").$this_pricing->default_price_categories:'--' ;?>
					</strong>
					<?php echo JText::_("ADS_CREDITS"); ?>
					</td>
				</tr>
			<?php } ?>
			</table>
			<?php // <== Custom Prices enabled
			?>
		<?php
	}
	
	function showAddCustomPriceForm($ListingPricesCategories=""){
	?>
	<fieldset>
		<legend><?php echo JText::_("ADS_PLG_ADD_CUSTOM_PRICE");?></legend>
		<table class="paramlist admintable">
			<?php if($ListingPricesCategories!="") { ?>
			<tr>
				<td class="paramlist key"><?php echo JText::_("ADS_PLG_CATEGORY");?>:</td>
				<td>
					<?php
						echo $ListingPricesCategories; 
					?>
				</td>
			</tr>
			<tr>
				<td class="paramlist key"><?php echo JText::_("ADS_PAYMENT_ITEM_PRICE");?></td>
				<td>
				<input name="custom_pricing_category_price" type="text" /><?php echo JText::_("ADS_CREDITS");?>
				<a href="#" onclick="document.getElementById('pl_task').value='add_custom_pricing';document.adminForm.submit();"><?php echo JText::_("ADS_ADD");?></a>
				</td>
			</tr>
			<tr>
				<td colspan="2">
				 <?php echo JHTML::tooltip(JText::_("ADS_PLG_CUSTOM_PRICES_TIP"));?><?php echo JText::_("ADS_PLG_CUSTOM_PRICES_TIP");?>
				</td>
			</tr>
			<?php } ?>
		</table>
	</fieldset>
	<?php
	}
	
	function showAddNewValabilityForm(){
		
		$params_arr = array();
		$allready 	= array();

		if( isset($this->time_valability_prices) && trim(strip_tags($this->time_valability_prices) )	!="" ){
			$params_arr = explode("|",trim(strip_tags($this->time_valability_prices) )		 );
			foreach ($params_arr as $p){
				$p1 = explode(":",$p);
				$allready[] = $p1[0];
			}
		}
		?>
		<input type="hidden" name="time_valability_prices" value="<?php echo isset($this->time_valability_prices) ? $this->time_valability_prices : '';?>" /> 
		<script type="text/javascript">
			var allreadyDefined = new Array();
			<?php $max=count($allready);for ($ii=0;$ii<$max;$ii++){?>
			allreadyDefined[<?php echo $ii;?>] = '<?php echo $allready[$ii]?>';
			<?php } ?>
			Array.prototype.in_array = function(p_val){for(var i=0,l=this.length;i<l;i++){if(this[i] == p_val){return true;}}return false;}
			function submitMyPrice(){
				var dc = document.getElementById('price_valability_val').value;
				if (allreadyDefined.in_array(dc)){
					popup("valability_"+dc);
					alert('Price for this valability allready defined!');
				}else{
					document.getElementById('pl_task').value='add_valability_pricing';
                    document.adminForm.submit();
				}
			}
		</script>
		<fieldset>
			<legend><?php echo JText::_("ADS_ADD_NEW_VALABILITY_PRICE");?></legend>
			<table class="paramlist admintable">
				<tr>
					<td class="paramlist ket"><?php echo JText::_("ADS_PLG_VALABILITY");?></td>
					<td class="paramlist value">
						<input type="textbox"  class="input" id="price_valability_val" name="valability" size="5" />
						<?php echo JText::_("ADS_PLG_DAYS");?>
					</td>
				</tr>
				<tr>
					<td class="paramlist ket"><?php echo JText::_("ADS_PAYMENT_ITEM_PRICE");?></td>
					<td class="paramlist value">
						<input type="textbox"  class="input" name="valability_price" size="5" />
						<?php echo JText::_("ADS_CREDITS");?>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<input type="button" onclick="submitMyPrice();" value="Add" />
					</td>
				</tr>
			</table>
		</fieldset>
		<?php
	}
	
	function showValabiltyList(){
		$params_arr = array();
		
		if( isset($this->time_valability_prices) && trim(strip_tags($this->time_valability_prices) ) !="" )
			$params_arr = explode("|",trim(strip_tags($this->time_valability_prices) ) );
		
		$valabs = array();
		if(count($params_arr)>0){
			foreach ($params_arr as $p){
				if($p){
					$pi = explode(":",$p);
					$valabs[$pi[0]] = $pi[1];
				}
			}
		}
		ksort($valabs);
		
		?>
			<table class="adminlist">
			<thead>
				<tr>
					<th><?php echo JText::_("ADS_PLG_VALABILITY");?></th>
					<th><?php echo JText::_("ADS_PLG_PRICE_CREDITS");?></th>
					<th><?php echo JText::_("ADS_DELETE");?></th>
				</tr>
			</thead>
			<?php 
			if(count($valabs)>0){
				$j = 0;
				foreach ($valabs as $pi => $p){
					$j = 1 - $j;
			?>
				<tr class="row<?php echo $j;?>">
					<td><?php echo $pi;?><?php echo JText::_("ADS_PLG_DAYS");?></td>
					<td id="valability_<?php echo $pi;?>"><?php echo $p;?><?php echo JText::_("ADS_PLG_CREDITS");?></td>
					<td>
						<a style="color:#FF0000 !important;" href="index.php?option=com_adsman&task=savepaymentitemsconfig&itemname=listing&action=del_valability_price&valability=<?php echo $pi; ?>"><?php echo JText::_("ADS_PLG_REMOVE_X");?></a>
					</td>
				</tr>
			<?php }
			}
			?>	
			</table>
		<?php
	}
	
	function addJsBlock(){
		$js = "
			function allcategories() {
				var e = document.getElementById('commisionable_categories');
				if(e.disabled){ return;	}
				var i = 0; var n = e.options.length; for (i = 0; i < n; i++) { e.options[i].selected = true;}
			}
			function nocategory() {
				var e = document.getElementById('commisionable_categories');
				var i = 0;
				var n = e.options.length;
				for (i = 0; i < n; i++) { e.options[i].selected = false;}
			}
			
			var pitem;
			function resize(){ document.getElementById(pitem).style.border =  'none';}
			
			function popup(item){
				document.getElementById(item).style.border =  '1px solid #FF0000';
				pitem = item; setTimeout('resize()',1000);
			}
			
			
			function toggle_Price_Category(val){
			  //val==0 no Price Listing on Categories	
			
			  if (val==0) {
					document.getElementById('default_price_categories').disabled='disabled'; 
					document.getElementById('commisionable_categories').disabled='disabled';
					document.getElementById('prices_categories0').disabled='disabled'; 
					document.getElementById('prices_categories1').disabled='disabled'; //custom prices
					document.getElementById('custom_prices_info').style.display='';  //div
					
					/* if Custom Prices: no, display info and do not show Add custom price */
					document.getElementById('custom_prices_info').style.display='';
					document.getElementById('idx_custom_pricelist').style.display='none';
					
				} else {
				  //Default Category Listing Price input box
					document.getElementById('default_price_categories').disabled='';   
					document.getElementById('commisionable_categories').disabled='';
					document.getElementById('prices_categories0').disabled=''; 
					document.getElementById('prices_categories1').disabled='';
					
					document.getElementById('idx_custom_pricelist').style.display='';
									
					if ( document.getElementById('prices_categories0').checked ) { 
						document.getElementById('custom_prices_info').style.display='';
						//document.getElementById('idx_custom_pricelist').style.display='none';
						
					} else { 
								
						document.getElementById('custom_prices_info').style.display='none';
						document.getElementById('idx_custom_pricelist').style.display='';
					}
					
					document.getElementById('custom_prices_info').style.display='';	
					document.getElementById('idx_custom_pricelist').style.display='';
					
				}
				
			}
			
			function toggle_Custom_Prices(val){
					
				if(val==0){ 
					document.getElementById('custom_prices_info').style.display='';	
				} 
				else { 
					document.getElementById('custom_prices_info').style.display='none';
					//document.getElementById('idx_custom_pricelist').style.display='';
				}
			}
			
			function toggle_TimeMode(mode){
				document.getElementById('tab'+(1-mode)+'_content').style.display = 'none'; document.getElementById('tab'+(1-mode)+'_notice').style.display = '';
				document.getElementById('tab'+mode+'_notice').style.display = 'none'; document.getElementById('tab'+mode+'_content').style.display = '';
				$$('#tab'+mode).fireEvent('click');
				showProgress(mode);
			}
			";
		return $js;
	}
	
	function showCurrentSettingsStatus(){
	?>
		<fieldset>
			<legend><?php echo JText::_("ADS_PLG_CURRENT_SETTINGS");?></legend>
			<table class="adminlist">
				<tr>
					<td class="paramlist key"><?php echo JText::_("ADS_PLG_GLOBAL_PRICE");?></td>
					<td class="paramlist value"><strong><?php echo $this->price;?> <?php echo JText::_("ADS_CREDITS");?></strong></td>
				</tr>
				<tr>
					<td class="paramlist key"><?php echo JText::_("ADS_PLG_UNLIMITES_LISTING_VALABILITY");?></td>
					<td><?php echo (@$this->time_valability) ? JText::_("ADS_NO") : JText::_("ADS_YES"); ?></td>
				</tr>
				<tr>
					<td class="paramlist key"><?php echo JText::_("ADS_PRICES_PER_CATEGORIES");?></td>
					<td><?php echo (@$this->preferential_categories) ? JText::_("ADS_YES") : JText::_("ADS_NO"); ?></td>
				</tr>
				<tr>
					<td class="paramlist key"><?php echo JText::_("ADS_CUSTOM_PRICES_PER_CATEGORIES");?></td>
					<td><?php echo (@$this->prices_categories) ? JText::_("ADS_YES") : JText::_("ADS_NO");?></td>
				</tr>
				</tr>
			</table>
		</fieldset>	
	<?php
	}
	
	function showHelpBar(){
	JHTML::_('behavior.mootools');
	JHTML::_('behavior.modal');
	?>
		<fieldset>
			<legend><?php echo JText::_("ADS_HELP");?></legend>
			<div class="helpIndex">
				<ul class="subext">
					<li><a href="#help_single_price" class="modal" rel="{handler: 'clone',target:'help_single_price', size: {x: 570, y: 200}}"><?php echo JText::_("ADS_SET_SINGLE_PRICE");?></a></li>
					<li><a href="#help_valabilities_price" class="modal" rel="{handler: 'clone', target:'help_valabilities_price', size: {x: 570, y: 200}}"><?php echo JText::_("ADS_SET_PRICES_PER_VALABILITIES");?></a></li>
					<li><a href="#help_free_categories" class="modal" rel="{handler: 'clone', target:'help_free_categories', size: {x: 570, y: 200}}"><?php echo JText::_("ADS_SET_FREE_CATEGORIES");?></a></li>
					<li><a href="#help_prices_categories" class="modal" rel="{handler: 'clone', target:'help_prices_categories', size: {x: 570, y: 200}}"><?php echo JText::_("ADS_SET_CUSTOM_PRICES_PER_CATEGORIES");?></a></li>
				</ul>
			</div>
		</fieldset>

		<div style="display:none;">
			<div id="help_single_price" style="padding:10px;">
			<h2><?php echo JText::_("ADS_SET_GLOBAL_LISTING_PRICE");?></h2>
			<?php echo JText::_("ADS_TO_SETUP_SINGLE_PRICE");?><br />
			<ul>
				<li> <?php echo JText::_("ADS_PLG_ENABLE");?><a href="#" onclick="popup('unlimited'); return false;"><?php echo JText::_("ADS_UNLIMITED_PUBLISHING") . JText::_("ADS_UNLIMITED_PUBLISHING_INFO");?></a>.</li>
				<li> <?php echo JText::_("ADS_FILL_IN_GLOBAL_PRICE_IN");?> <a href="#" onclick="popup('price_box'); return false;"><?php echo JText::_("ADS_PAYMENT_ITEM_LISTING_BOX");?></a></li>
				<li> <?php echo JText::_("ADS_SET_PRICE_LISTING_CATEGORIES_NO");?></li>
			</ul>
			</div>
		</div>

		<div style="display:none;">	
			<div id="help_valabilities_price" style="padding:10px;">
			<h2><?php echo JText::_("ADS_CONFIGURE_VALABILITIES_AND_PRICES");?></h2>
			<?php echo JText::_("ADS_CONFIGURE_VALABILITIES_AND_PRICES_INFO");?><br />
			<?php echo JText::_("ADS_FOR_THIS");?><br />
			<ul>
				<li> <?php echo JText::_("ADS_PLG_ENABLE");?><a href="#" onclick="popup('preconfigured'); return false;"><?php echo JText::_("ADS_VALABILITIES_RANGE_PUBLISHING");?></a>.</li>
				<li> <?php echo JText::_("ADS_FILL_IN_GLOBAL_PRICE_IN");?><a href="#" onclick="popup('price_box'); return false;"><?php echo JText::_("ADS_PAYMENT_ITEM_LISTING_BOX");?></a></li>
				<li> <?php echo JText::_("ADS_ADD_VALABILITIES_AND_PRICES");?> <a href="#" onclick="$$('#tab1').fireEvent('click'); return false;"><?php echo JText::_("ADS_PAYMENT_ITEM_LISTING_BOX");?></a></li>
			</ul>
			</div>
			
			<div id="help_free_categories" style="padding:10px;">
			<h2><?php echo JText::_("ADS_SET_FREE_CATEGORIES");?></h2>
			<?php echo JText::_("ADS_SET_FREE_CATEGORIES_HELP");?>
			<ul>
				<li> <?php echo JText::_("ADS_PLG_ENABLE");?><a href="#" onclick="popup('unlimited'); return false;"><?php echo JText::_("ADS_UNLIMITED_PUBLISHING") . JText::_("ADS_UNLIMITED_PUBLISHING_INFO");?></a>.</li>
				<li> <?php echo JText::_("ADS_SET_PRICE_LISTING_CATEGORIES_YES");?> </li>
				<li> <?php echo JText::_("ADS_CHOOSE_NON_FREE_CATEGORIES");?> </li>
			</ul>
			</div>
			<div id="help_prices_categories" style="padding:10px;">
			<h2><?php echo JText::_("ADS_SET_PRICES_PER_CATEGORIES");?></h2>
			<?php echo JText::_("ADS_SET_PRICES_PER_CATEGORIES_HELP");?>
			<ul>
				<li> <?php echo JText::_("ADS_PLG_ENABLE");?> <a href="#" onclick="popup('unlimited'); return false;"><?php echo JText::_("ADS_UNLIMITED_PUBLISHING") . JText::_("ADS_UNLIMITED_PUBLISHING_INFO");?></a>.</li>
				<li> <?php echo JText::_("ADS_SET_PRICE_LISTING_CATEGORIES_YES");?> </li>
				<li> <?php echo JText::_("ADS_CHOOSE_NON_FREE_CATEGORIES");?> </li>
				<li> <?php echo JText::_("ADS_ENABLE_CUSTOM_PRICES");?></li>
				<li> <?php echo JText::_("ADS_ADD_PRICES");?></li>
			</ul>
			</div>
		</div>
	<?php
	}
	
}

class price_listing_DBHelper{
	
	function getCategory($id){
		
		$db = JFactory::getDbo();
		$id = $db->getEscaped($id);
		$db->setQuery("SELECT category FROM #__ads WHERE id = '$id'");
		return $db->loadResult();
	}
	
	function getQuant(){
		
		$var = JRequest::get('REQUEST');
		$Itemid = JRequest::getVar('Itemid', 0, 'REQUEST', 'integer');
        
		if ( isset($this->time_valability) && $this->time_valability ) {
			$params_arr = array();
			if( isset( $this->time_valability_prices )  && trim( strip_tags($this->time_valability_prices) ) !="" )
				$params_arr = explode("|", trim( strip_tags($this->time_valability_prices) ) );
				
			$valabs = array();
			if( count($params_arr) > 0 ) {
				foreach ($params_arr as $p){
					if($p!=""){
						$pi = explode(":",$p);
						$valabs[] = $pi[1];
					}
				}
			}
			sort($valabs);
			$valability = JRequest::getInt('ad_valability',0);
		
			if( $valability >= 0 ){
				
				return $valabs[$valability];
			}
		} else {
			$quant = 0;
			$cati = JRequest::getVar('category','');
			
			if(!$cati)
				$cati = JRequest::getVar('cat','');
			if(!$cati)
				$cati = JRequest::getVar('category_selected','');
			
			if ( isset($this->preferential_categories) && ($this->preferential_categories==1) ) {
				if( !$cati ) {
					// go select a category
					FactoryLayer::redirect("index.php?option=com_adsman&task=selectcat&Itemid=$Itemid");
				} else
					// fetch the category price  ( credit price actually )
					$quant = price_listing_DBHelper::getPrice($cati);
			} else {
				//$quant = 1;
				$quant =  price_listing_DBHelper::getListingPrice();
			}
			
			return $quant;
		}
	}
	
	function getPrice($catid){
		
		$price = 0;

		if( $this->preferential_categories == 1 ) {

			//if ($this->default_price_categories && $this->prices_categories == 0) {
			//	$price = $this->default_price_categories;
			//} else {
				if ($catid) {
					$money_takin_categories = explode(",",$this->commisionable_categories);

					if( in_array($catid,$money_takin_categories) ) {
                           $ptype= "listing";
                            $sql = " SELECT price+0 ".
                                    " FROM #__ads_custom_prices ".
                                    " WHERE category = '$catid' AND price_type= '$ptype'";

                            $this->_db->setQuery($sql);
                            $custom_price_Result = $this->_db->loadResult();

                            if( isset($custom_price_Result) && (int)$custom_price_Result >= 0 ){
                                $price =  $custom_price_Result;
                            } elseif ($this->default_price_categories && $this->prices_categories == 1) {
                                $price = $this->default_price_categories;
                            }
                    } else {
						$price = 0;
					}	
				}
			//}
		}
		
		return $price;
    }
    
    function getListingPrice(){
    	$price = 0;
    	$this->_db->setQuery("SELECT price FROM #__ads_pricing WHERE itemname = '$this->itemname' ");
		$price =  $this->_db->LoadResult();
		
		return $price;
    }

	function getCategoriesHTML( $name, $selected, $params=null ){
		
		$cat = new JTheFactoryCategoryObj();
		$cat->show_active_only = true;
		
		if(isset($params["multiple"]) && (int)$params["multiple"]!=0)
			return $cat->makeCatSelectMultiple($selected, $name);
		else
			return $cat->HTMLCatTree($name,$selected, $params);
		
	}
	
	function getCategoriesPrices($id_list){
		if(!$id_list) return null;
		$_sql = " SELECT ag.id,ag.catname, ag.id as catid,cp.price ". 
		" FROM ".APP_CATEGORY_TABLE." AS ag  LEFT JOIN #__ads_custom_prices as cp  ON cp.category = ag.id AND price_type='listing' ".
		" WHERE  ag.id IN ($id_list) ";
		$this->_db->setQuery( $_sql );
		return $this->_db->loadObjectList();
		
	}
	
	function getCategoriesSelect($name, $id_list){
		
		if(!$id_list) return "";
		
		$this->_db->setQuery("SELECT * FROM ".APP_CATEGORY_TABLE." WHERE id IN ($id_list)");
		$opts = $this->_db->loadObjectList();
		
		return $otherListingPricesCategories = JHTML::_('select.genericlist',$opts,'custom_pricing_category',' class="inputbox" style="width:190px;" "','id', 'catname');
		
	}
	
	function getEnabledPayListing(){
		
		$db = JFactory::getDbo();
		$db->setQuery( "SELECT enabled FROM #__ads_pricing WHERE itemname = 'listing' ");
		$isListingEnabled = $db->loadResult();
		
		return $isListingEnabled;
	}
	
}

?>
