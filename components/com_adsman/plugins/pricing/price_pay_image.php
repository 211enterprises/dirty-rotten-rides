<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

// no direct access
//defined( '_JEXEC' ) or die( 'Restricted access' );
require_once(dirname(__FILE__).DS."pricing_object.php");

class price_pay_image extends generic_pricing
{
	var $featured_type='image';
		
	function price_pay_image(&$db) {
		
		$this->classname = 'price_pay_image';
        $this->classdescription='Module for buying featured Ads (Pay per image)';
        parent::generic_pricing($db);
		$this->itemname = "pay_image";
	}
	
	function getPrice($d){
				
		$p = JRequest::getVar('itemprice');
	
		//like packages
		if (!$p)
    		$p = parent::getPrice($d);
    	return $p;
		
    	return ($d["itemprice"]);
    }

    function setRequestVar($var,$value)
    {
        $_REQUEST[$var]=$value;
        if (isset($_POST[$var])) $_POST[$var]=$value;
        if (isset($_GET[$var])) $_GET[$var]=$value;
    }
    
    function getCurrency($d) {
    	
    	$currency = JRequest::getVar('currency');
    	
    	if (!$currency) {
    	
	    	if(isset($d['item_number']) && ($d['item_number'] == 'packages')) {
	    		$package_id = $d['custom'];
				//$selected_package = price_packages_DBHelper::getPackage($d['item_name']);
								
				$package = price_packages_DBHelper::getPackage((int)$package_id);
				return $package->currency;
	    	} else {
	    		return $currency;
	    	}
    	}
    	
    	return ($d["currency"]);
    }
      	
	function checktask($task,$d) {

		$Itemid = JRequest::getVar('Itemid', 0, 'REQUEST', 'integer');
	  	$mainframe = JFactory::getApplication();
        
        $language = JFactory::getLanguage();
        $language->load('com_adsman');
        $lng = substr( $language->get("tag") , 0, 2);
        if (file_exists(ADS_COMPONENT_PATH."/plugins/payment/pay_paypal.$lng.php"))
            require_once(ADS_COMPONENT_PATH."/plugins/payment/pay_paypal.$lng.php");
        else
            require_once(ADS_COMPONENT_PATH."/plugins/payment/pay_paypal.en.php");
	  	//require_once(ADS_COMPONENT_PATH."/plugins/payment/pay_paypal.en.php");
	  	
	  	$id = JRequest::getVar('id',null); 
		
	  	$my = JFactory::getUser();
	  	$db = JFactory::getDbo();
        $d['pay_image'] = 0;
	  	
	  	$credits = $this->getCredits($my->id);


      if($task=='republish' || $task=="new" || ( $task=="edit" && !$id  ) ) {

      	// check if enabled pay_image
		$isImageEnabled = price_pay_image_DBHelper::getEnabledPayImage();

		if ($isImageEnabled ) {
				
			$isListingEnabled = price_listing_DBHelper::getEnabledPayListing();
			
			$free_array =  price_pay_image_DBHelper::getFreeImage();
		
			// test this condition 
			if (!empty($free_array) ) {
				foreach ($free_array as $k=>$val) {
					if ($k == 0) {
						
						$free_no = $k;
						echo sprintf(ads_paypal_main_img_upload_free);
					} else {
						$free_no = $k;	 
					}
				}
					
			} else {
					$free_no = 0;	
			}

			if ($free_no > 0) {
				echo sprintf(ads_paypal_images_upload_free, $free_no);
			}
				
			// if has credits
			$image_credits = parent::getCredits($my->id);
				
			if ( $image_credits <= 0 && !$isListingEnabled) {
	            	echo sprintf(ads_paypal_credit_situation, 0);
					//echo sprintf(ads_paypal_images_saved_tmp);
	            }
					
			}
      }
      
       if ($task=='save' ) {
       	  // consume image credits
          $d['pay_image'] = 1;
          price_listing::checktask($task,$d);	
       }
       
      return false;
    }
    
    function processTemplate($task,$d, &$smarty){

    	$Itemid = JRequest::getVar('Itemid', 0, 'REQUEST', 'integer');
		$mainframe = JFactory::getApplication();
		
		$id = JRequest::getVar('id', null);
		
		if($task=='republish' || $task=="new" || ( $task=="edit" && $id ) ){      	
      	//if pay image
				$isImageEnabled = price_pay_image_DBHelper::getEnabledPayImage();
				$img_prices = price_pay_image_DBHelper::getPayImageAmount();
						
				if ( $isImageEnabled ) {
					// display credits costs for selected option
					$jDocImg = & JFactory::getDocument();
					$js_blockImg = "";
					$js_blockImg .= "
						var pricingCurrency = 'credit(s)';
						var iList = new Array();
						var iPriceList = new Array();
						
						function showImagePrice(price) {
							document.getElementById('pricing_img_info').innerHTML ='".JText::_("ADS_PRICE_LISTING_IMAGES")."'+ price +' '+pricingCurrency;
						}
					";
					
					if (count($img_prices) ) {
						//$first_index = key($img_prices);
						//$first_index = 'none';
						
						$js_blockImg .= " 
						\r\n
							iList['none'] = 'none';\r\n
							iPriceList['none'] = '0';\r\n
							";
						
							foreach ($img_prices as $k => $v){
							
							$js_blockImg .= "
							\r\n
								iList[{$k}] = '{$k}';\r\n
								iPriceList[{$k}] = '".$v."';\r\n
							";
							
							}
							$smarty->assign("credits_row_ini", 0);
						
						$jDocImg->addScriptDeclaration($js_blockImg);
					}
				}
				
				$img_main_price = price_pay_image_DBHelper::getMainImageAmount(); //credits to pay for main img
				$smarty->assign("img_main_price", $img_main_price);
		}
		
    }
      
    function consume_credits($selected_images_index, $user_id)
    {
    	// CONSUME CREDITS
        $db = JFactory::getDbo();
    	$credits_price = price_pay_image_DBHelper::getCreditImg($selected_images_index);

		$db->setQuery("UPDATE #__ads_user_credits SET credits_no=credits_no - $credits_price WHERE userid='$user_id' ");
		$db->query();
    	
    }
    
	function ShowSelectFromCredits($existing_images) { 
    	
    	$db = JFactory::getDbo();
    	$my	= JFactory::getUser();
		$credits = parent::getCredits($my->id);
	
		$lists_image_credit = array();	
		
		if ( $credits > 0 ) {

		  $opts = array();
		  $opts[] = JHTML::_('select.option', 'none',JText::_("ADS_CHOOSE_IMG"));
				  
		  $indexes_array = array();
		  $payimage_credits = price_pay_image_DBHelper::getPayImageAmount();
		  
		  if ( !empty($payimage_credits) ) {
		  
		  	  //$indexes_array[0] = 'none';
			  foreach ($payimage_credits as $k=>$row) {
			  
			  	if( $row > 0 ) {
		  			$index = $k;
		  			$val = $k;

		    		$opts[] = JHTML::_('select.option', $index, $val);
		    		$indexes_array[$k] = $val;
			  	}
			  }
			  	
		  	 $lists_image_credit['list'] =	JHTML::_('select.genericlist',  $opts, 'image_credit', 'class="inputbox" alt="image_credit" onchange="showImagePrice(iPriceList[this.value]);"',  'value', 'text','none');
		 	 $lists_image_credit['indexes'] = $indexes_array;
		  }
		}
		return $lists_image_credit;	
    }	
		
    function UploadImageCredits($d) {
		    		
    	//$d->id = ad id; load($d->id);
		$my	= JFactory::getUser();
		$smarty = AdsUtilities::SmartyLoaderHelper();
		$db = JFactory::getDbo();
		$add = new JAdsAddsman($db);
		
		$id = $d['id'];
		$selected_images_index = $d['image_credit'];
		$main_image_add = $d['main_image_add'];
		
		if ($id) {
			$add->ancLoad($id);
		}
		$add->images = $add->getImages();
		$existing_images = count($add->images);
		
		$credits =  generic_pricing::getCredits($my->id);
		$img_prices_array = price_pay_image_DBHelper::getPayImageAmount();
		$img_main_price = price_pay_image_DBHelper::getMainImageAmount(); //credits to pay for main img
						
		$img_prices = ( isset($selected_images_index) && $selected_images_index!= 'none' ) ? $img_prices_array[$selected_images_index] : 0;	
		$needed_credits = $img_prices + $img_main_price;
	
		
		if ($credits > 0 && $credits >= $needed_credits) {
		
			$selected_images_index 		= ( isset($selected_images_index) && $selected_images_index!= 'none' ) ? $selected_images_index : 'none';
			$selected_images_number 	= ( isset($selected_images_index) && $selected_images_index!= 'none' ) ? $selected_images_index : 'none';
			$select_credits_main 		= isset($main_image_add) ? $main_image_add : 0;
			
            JHTML::_('script', 'adsman.js', 'components/com_adsman/js/');
            JHTML::_('script', 'multifile.js', 'components/com_adsman/js/');

			$smarty->assign('add',$add);
			$smarty->assign('object_id',$id);
			$smarty->assign('my',$my);
			$smarty->assign('item_object', $this);

			$smarty->assign('select_credits_main',$select_credits_main);
			$smarty->assign('selected_images_number',$selected_images_number);
			$smarty->assign('selected_images_index',$selected_images_index);
				
			$smarty->display('t_featured_image_list.tpl');
			
		} else {
				echo JText::_("ADS_NO_CREDITS_AVAILABLE");
				$this->ShowPurchaseDialog($d);
		}
    }
        			
						
    function getDescription() {
        return "Buy credits for images";
    }
    
    function save_admin_config()
    {
        $itemname = JRequest::getVar('itemname','');
        $payOb = JTheFactoryPaymentLib::getInstance();
    	$pricings = $payOb->price_plugins;
   	
    	//ads_images_pricing store
    	JTable::addIncludePath(JPATH_SITE.DS.'administrator'.DS.'components'.DS.'com_adsman'.DS.'tables');
	    $payimage =& JTable::getInstance('Priceimage', 'Table');
       
        $payimage_posts['image_price_id'] 	= JRequest::getVar('image_price_id', 0,'post','DOUBLE');
        $payimage_posts['credits_img_0'] 		= JRequest::getVar('credits_img_0',0,'post','DOUBLE');
        $payimage_posts['credits_img_1'] 		= JRequest::getVar('credits_img_1',0,'post','DOUBLE');
        $payimage_posts['credits_img_2'] 		= JRequest::getVar('credits_img_2',0,'post','DOUBLE');
        $payimage_posts['credits_img_3'] 		= JRequest::getVar('credits_img_3',0,'post','DOUBLE');
        $payimage_posts['credits_img_4'] 		= JRequest::getVar('credits_img_4',0,'post','DOUBLE');
        $payimage_posts['credits_img_5'] 		= JRequest::getVar('credits_img_5',0,'post','DOUBLE');
        $payimage_posts['credits_img_6'] 		= JRequest::getVar('credits_img_6',0,'post','DOUBLE');
        $payimage_posts['credits_img_7'] 		= JRequest::getVar('credits_img_7',0,'post','DOUBLE');
        
        if ($payimage_posts['image_price_id']  != 0)   
    	{
	    	$payimage->load($payimage_posts['image_price_id']);	
	    	
	    	if (!$payimage->bind($payimage_posts))
		    {
		      $this->setError($this->_db->getErrorMsg());
		      return false;
		    }
	    	
	    	if (!$payimage->store())
		    {
		      $this->setError($this->_db->getErrorMsg());
		      return false;
		    }
    	}  else {
		    if (!$payimage->bind($payimage_posts))
		    {
		      $this->setError($this->_db->getErrorMsg());
		      return false;
		    }
	  	    
		    if (!$payimage->check())
		    {
		      $this->setError($this->_db->getErrorMsg());
		      return false;
		    }
		
		    if (empty($payimage->id)) {
			    if (!$payimage->store())
			    {
			      $this->setError($this->_db->getErrorMsg());
			     # die('stopped');
			      return false;
			    }
		    }
	    }
	    
    }

    function show_admin_config() {
    	
    	$this->_db->setQuery("SELECT * FROM ".constant("APP_CURRENCY_TABLE_".strtoupper(APP_PREFIX)));
		$currencies = $this->_db->loadObjectList();
    	$pricings = price_pay_image_DBHelper::getFrontPayImages();
    	
    	JHTML::_('behavior.mootools');
    	JHTML::_ ( 'script', 'imageprices.js', 'components/com_adsman/js/' );
    	
    	$count = (defined("ads_opt_maxnr_images") && ads_opt_maxnr_images > 7 ) ? 7 : ads_opt_maxnr_images;
     	$doc =& JFactory::getDocument();
		$doc->addScriptDeclaration('var count = '.$count.';');
   	?>
 	<fieldset>
	  <legend><?php echo JText::_("ADS_PLG_PAYMENT_ITEM")." ".$this->itemname;?></legend>
	  <p><?php echo JText::_("Maximum number of images allowed to upload for an ad is set to ")." ".ads_opt_maxnr_images;?></p>
 	  <table class="adminlist">
		<tr>
		  <td valign="top">
			<script type="text/javascript">
				var priceInputList = new Array();
				var priceValuesList = new Array();
				var initAux = 0;
			</script>
            <form action="<?php echo JRoute::_('index.php'); ?>" method="post" name="adminForm" id="image_price">
            <fieldset>
            <legend><?php echo JText::_("ADS_PLG_PRICE_PAYPERIMAGE");?></legend>
			<table class="adminlist" border=1 width="100%">
				
            	<tr>
            		<td class="paramlist key" style="width:110px !important;"><?php echo JText::_("ADS_IMAGES_CREDITS");?>
            		<br /><span><?php echo JText::_(' (credits)'); ?> </span></td>
            		
            		<td>
            			<table class="paramlist admintable">
            				<?php 
            				$t = 0;
				
							//if ($pricings) {
								for ($i = 0; $i <= 7; $i++) {
            						$item_name = "credits_img_$i";
            						$item_radio_name = "credits_image_$i";
            						
            						switch ($i) {
	            						case 0:	
	            							$label = JText::_('ADS_PRICE_IMG_MAIN'); 
	            							break;
	            						case 7:
	            							$label = JText::_('ADS_PRICE_IMG_7'); 
	            							break;	
	            						default:
	            							//$k = $i+1;
	            							$label = JText::_('ADS_PRICE_IMG').$i.JText::_('ADS_PRICE_IMAGES');
	            							break;	
	            					}
								?>
	            				<tr>
	            					<td class="paramlist key" style="text-align:left !important;"><?php echo $label; ?></td>
            						<td>
	            					<?php
	            					$opts = array();
			            			$disable_radio = ( $i <= $count ) ? '' : ' disabled="disabled"';
									$opts[] = JHTML::_('select.option', 0, JText::_('ADS_FREE'));
									$opts[] = JHTML::_('select.option', 1, JText::_('ADS_YES_SET_PRICE'));

									$lists[$item_radio_name] 	= JHTML::_('select.radiolist', $opts, "$item_radio_name",'class="inputbox" "'.$disable_radio.'" ','value', 'text', ( isset($pricings->$item_name) && ($pricings->$item_name != 0) ) ? 1 : 0,"$item_radio_name");
									echo $lists[$item_radio_name]; 
									?>
	            					
									
	            					<?php 
										if (isset($pricings->$item_name)  && ($pricings->$item_name != 0) ) {
											$readonly = '';
											$disabled = '';
										} else {
											$readonly = 'readonly="readonly"';
											$disabled = 'disabled="disabled"';
										}
									?>
									<input <?php echo $disabled; ?> <?php echo $readonly; ?> type="text" id="credits_img_<?php echo $i; ?>" name="credits_img_<?php echo $i;?>" 
	            				value="<?php echo (isset($pricings->$item_name)) ? $pricings->$item_name : 0; ?>" size="7" />
	            					<span style="float: left;"><?php echo JText::_('ADS_CREDITS');?></span>
	            					<script type="text/javascript">
	            						priceInputList[<?php echo $t;?>] = 'credits_img_<?php echo $i;?>';
	            						priceValuesList[<?php echo $t;?>] = '<?php echo $pricings->$item_name; ?>';
										initAux++;
					    				<?php // } ?>
	            					</script>
	            					
	            					</td>
	            					
	            				</tr>
	            				<?php 
	            				$t++;
	            				} 
            			//	} else 
            			//		echo JText::_("ADS_NO_PRICES_ENABLED");
            				?>
            			</table>
            		</td>
            	</tr>
            </table>
            </fieldset>
            <input type="hidden" name="option" value="<?php echo APP_EXTENSION;?>"/>
            <input type="hidden" name="task" value="savepaymentitemsconfig"/>
            <input type="hidden" name="itemname" value="<?php echo $this->itemname;?>"/>
           	<input type="hidden" name="image_price_id" value="<?php echo ( ($pricings != null)) ? $pricings->id : 0; ?>"/>
          </form>
		</td>
	</tr>
</table>

</fieldset>	
<?php
}

}


class price_pay_image_DBHelper {
	
	function getCreditImg($selected_images_index){
		$db = JFactory::getDbo();
        $column = "credits_img_$selected_images_index";

        $query = 'SELECT credits_img_'.$selected_images_index.' FROM `#__ads_images_pricing` ';
		$db->setQuery($query);
				
		$selected_images_number = $db->loadResult();
		
		return $selected_images_number;
	}

	function getPayImageAmount() {
		
		$id	= JRequest::getVar('id', null );	
		$db = JFactory::getDbo();
				
		if ($id) {
		
			$controller = new AdsController();
				
			$session	= &JFactory::getSession();
			$session->set("session.token", null);
			$session->set("token", null);
				
			$model = &$controller->getModel( 'Adsman','adsModel' );
			$add =& $model->getAddData($id);
			$add->images = $add->getImages();
			$existing_images = count($add->images);
		
			if ( defined("ads_opt_maxnr_images") && ads_opt_maxnr_images > 0) {
				$remaining_img_upload = ads_opt_maxnr_images - $existing_images;
			} else {
				$remaining_img_upload = 7 - $existing_images;
			}
		}
		
		$db->setQuery("SELECT * FROM #__ads_images_pricing ");
		$payimages_price = $db->loadObjectList();
		$price = array();

		if($payimages_price) {
		  if (!$id && defined('ads_opt_maxnr_images') && ads_opt_maxnr_images > 0) {
			$max_img_upload = ads_opt_maxnr_images;
		  }
		  else {
			$max_img_upload = 7;
		  }
		
		  for ( $p=1; $p <= $max_img_upload; $p++ ) {
			 if ($p < 8)   
			 	  $img_item = "credits_img_$p";
			 else {
			  	  $img_item = "credits_img_7";			
			 }

			  if ($payimages_price[0]->$img_item != 0) {
			  		$price[$p] = $payimages_price[0]->$img_item;
			  } else {
                  if ($p > 1) {
					$price[$p] = $price[$p-1];
                  }
			  }
		  }
		}

		return $price;
	}
	
	function getMainImageAmount() {
		
		$main_image_price = 0;
		$db = JFactory::getDbo();
		$db->setQuery("SELECT credits_img_0 FROM #__ads_images_pricing ");
		
		$main_image_price = $db->loadResult();
				
		return $main_image_price;
	}
		
	function getFreeImage() {
		$db = JFactory::getDbo();
		$db->setQuery("SELECT * FROM #__ads_images_pricing ");
		$prices  = $db->loadAssoc();
		$payImage_result = 0;
		$freeImage_result = array();
		
        if (!empty($prices) ) {

            for ( $p=0; $p < 8; $p++ ) {

                $img_item = "credits_img_$p";
                if ($prices[$img_item] == 0 && $payImage_result == 0) {
                    $freeImage_result[$p] = $p;
                }
                else {
                    $payImage_result = $p;
                }
            }
        }
		
		return $freeImage_result;
		
	}
	
	
    function getFrontPayImages(){

		$db = JFactory::getDbo();
		$db->setQuery("SELECT * FROM #__ads_images_pricing ");
		$payImagesPrices  = $db->loadObject();
		
		if ($payImagesPrices)
			return  $payImagesPrices;
		else 
			return null;	
	}
	
	function getEnabledPayImage(){
		
		$db = JFactory::getDbo();
		$db->setQuery( "SELECT enabled FROM #__ads_pricing WHERE itemname = 'pay_image' ");
		$isImageEnabled = $db->loadResult();
		
		return $isImageEnabled;
	}

}
?>