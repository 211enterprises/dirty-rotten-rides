<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

define('ads_paypal_disclaimer','<strong>For your safety</strong> all payments will be redirected to paypals https:// server for processing');
define('ads_paypal_buynow','Secure paypal payment');
define('ads_paypal_paypalemail','Your Paypal email');
define('ads_paypal_use_sandbox','Use Sandbox');
define('ads_paypal_enabled','Enabled');
define('ads_paypal_auto_accept','Auto Confirm Pending Orders');

define('ads_paypal_listing_is_not_free_default',"Default listing costs %s credit(s) <br />");
define('ads_paypal_listing_is_not_free',"Listing costs %s credit(s)<br />");
define('ads_paypal_credit_situation', 'You have %s credits.<br />');
define('ads_paypal_buy_credits',"Select a credit package from the list %s to buy. <br />");

define('ads_paypal_allready_processing', 'Your payment for this listing is processing!');

define('ads_paypal_saved_tmp', "Your ad will be saved unpublished untill a listing credit is bought  <br /> <br />");
//define('ads_paypal_saved_tmp', "Your ad will be saved unpublished until credits are bought (a listing costs %s %s ) <br /><br />");
define('ads_paypal_valability_saved_tmp', "Your ad will be saved unpublished until credits are purchased <br /><br />");


define('ads_paypal_credit_valability', 'Select an availability period for the ad price.<br />');


define('ads_paypal_images_upload_is_not_free'," You need %s credit(s) to upload %s image(s) <br />");
define('ads_paypal_main_img_upload_is_not_free'," You need %s credit(s) to upload 1 picture <br />" );
define('ads_paypal_images_saved_tmp', "Your can add more pictures after credits are bought.  <br /><br />");
define('ads_paypal_main_img_upload_free', '<strong>You can upload Main Picture free.</strong><br />');
define('ads_paypal_images_upload_free', '<strong>You can upload %s Image(s) free.</strong><br />');

//define('ads_paypal','');
?>