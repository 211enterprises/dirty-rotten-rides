<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

define('ads_mbookers_disclaimer','<strong>For your safety</strong> all payments will be redirected to moneybooker https:// server for processing');
define('ads_mbookers_buynow','Secure Moneybookers payment');
define('ads_mbookers_use_sandbox','Use Sandbox');
define('ads_mbookers_enabled','Enabled');
define('ads_mbookers_email','Email');

?>