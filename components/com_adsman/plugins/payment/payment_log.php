<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

class JTheFactoryPaymentLog extends JTable {
    var $id				= null;
    var $date			= null;
    var $amount			= null;
    var $currency		= null;
    var $refnumber		= null;
    var $invoice		= null;
    var $ipn_response	= null;
    var $ipn_ip			= null;
    var $comission_id	= null;
    var $status			= null;
    var $userid			= null;    
    var $itemname		= null;
    var $payment_method	= null;
    var $object_id		= null;
    var $var_name1		= null;
    var $var_name2		= null;
    var $var_name3		= null;
    var $var_name4		= null;
    var $var_value1		= null;
    var $var_value2		= null;
    var $var_value3		= null;
    var $var_value4		= null;
    var $_db			= null;

	function __construct( &$db ) {
		parent::__construct( PAYTABLE_PAYLOG, 'id', $db );
	}
	
}
?>