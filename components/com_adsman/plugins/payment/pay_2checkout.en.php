<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

define('ads_2checkout_disclaimer','<strong>For your safety</strong> all payments will be redirected to moneybooker https:// server for processing');
define('ads_2checkout_buynow','Secure Moneybookers payment');
define('ads_2checkout_test_mode','Use Sandbox');
define('ads_2checkout_enabled','Enabled');
define('ads_2checkout_x_login','X Login');
define('ads_2checkout_secret_word','Secret Word');

?>