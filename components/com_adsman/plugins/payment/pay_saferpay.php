<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

if(!defined("ADS_COMPONENT_PATH"))
  define("ADS_COMPONENT_PATH", JPATH_ROOT."/components/com_adsman");

define('SAFERPAY_LOG', 1);

//require_once(JPATH_ROOT."/components/com_adsman/plugins/payment/payment_object.php");
require_once(ADS_COMPONENT_PATH."/plugins/payment/payment_object.php");

class pay_saferpay extends payment_object
{
  var $_db              = null;
  var $classname        = "pay_saferpay";
  var $classdescription = "Saferpay Payment Method";
  var $ipn_response     = null;
  var $action           = null;

  // Done.
  function pay_2checkout(&$db)
  {
    parent::payment_object($db);
  }

  // Done.
  function ipn($d)
  {
    ob_clean();

    $log = new JTheFactoryPaymentLog($this->_db);
    $log->set('_tbl_key', "invoice");

    $ipn     = $this->getIpn();
    $invoice = $ipn['order_id'];

    // THIS IS FAR MORE IMPORTANT TO FORGET
    $log->load($invoice);
    $log->params = new JRegistry($log->params);

    if ( $log->status == "ok" ) {
      // nothing to accept ( accepted manually before IPN )
      return; exit; die;
    }

    $total_price_ad = $log->params->get('total_price_ad');
    $featured_ad		= $log->var_value3;

    $log->date				   = date('Y-m-d h:i:s');
    $log->amount			   = $ipn['amount'];
    $log->currency			 = $ipn['currency'];
    $log->refnumber			 = $ipn['reference'];
    $log->invoice			   = $invoice;
    $log->ipn_response	 = print_r($_REQUEST,true);
    $log->ipn_ip			   = $_SERVER['REMOTE_ADDR'];
    $log->comission_id	 = 0;
    $log->payment_method = $this->classname;

    $validIpn = $this->validateIpn($ipn);

    // Check if IPN is valid.
    if (false === $validIpn) {
      $log->var_value4 = 'failed validateIPN';
      $log->status = 'error';
      $log->store();
    } else {

      if (!$this->validatePayment($ipn)) {
        $log->var_value4 = 'failed validatePayment';
        $log->status = 'error';
        $log->store();
      }

      parse_str(substr($validIpn, 3));

      $password = '' == $this->params->get('sppassword') ? '' : 'spPassword='.$this->params->get('sppassword') . '&';

      $gateway = 'https://www.saferpay.com/hosting/PayComplete.asp';
      $url     = $gateway.'?'.$password.'ACCOUNTID='.$this->params->get('account_id').'&ID='.urlencode($ipn['reference']).'&TOKEN='.urlencode($ipn['token']);
      $result  = join("", file($url));

      if (!substr($result, 0, 2) == "OK") {
        $log->var_value4 = 'failed complete payment';
        $log->status = 'manual_check';
        $log->store();
      } else {

        $log->status = 'ok';

        //validate amounts and add item
        $price_classname="price_".$log->itemname;

        if (file_exists(ADS_COMPONENT_PATH."/plugins/pricing/$price_classname.php")) {
          require_once(ADS_COMPONENT_PATH."/plugins/pricing/$price_classname.php");

          $price_class	= new $price_classname($this->_db);

          //
          $d = array(
            'item_number' => $log->itemname,
            'itemname' => $log->itemname,
            'custom' => $log->var_value1,
          );

          $value			= $log->params->get('quantity') * $price_class->getPrice($d);
          $currency		= $price_class->getCurrency($d);

          $this->_db->setQuery("SELECT * FROM #__ads_pricing WHERE itemname='maincredit' ");
          $row = $this->_db->LoadObject();

          if ($log->itemname == 'packages') {
            $credits_purchased = $price_class->getCredits($d);
            $package_id = $log->var_value1;
            $package = price_packages_DBHelper::getPackage((int)$package_id);

            $quantity = $package->credits * $log->params->get('quantity');
          } else {

            $value = $row->price  * $log->params->get('needed_credits');
            $currency = $row->currency;

            $quantity = $log->params->get('needed_credits');
          }

          $auto_accept 	= $this->params->get('auto_accept','');

          $mc_gross = number_format($ipn['amount'], 2);
          $value    = number_format($value, 2);

          if ( ($mc_gross == $value) && ($log->status == 'ok' || $auto_accept)) {

            $price_class->acceptOrder($log, $quantity, $total_price_ad, $featured_ad);
            $log->status = 'ok';
            $log->store();
          } else {

            $log->var_value4 = 'failed complete mc gross == value' . $mc_gross . ' ' . $value;
            $log->status = 'manual_check';
            $log->store();
          }
        }
      }
    }

    exit;
  }

  // Done.
  function show_admin_config()
  {
    $account_id = $this->params->get('account_id','');
    $sppassword = $this->params->get('sppassword','');
    $enabled    = JHTML::_("select.booleanlist",'enabled','class="inputbox"',$this->enabled);

    ?>
  <form action="<?php echo JRoute::_('index.php'); ?>" method="post" name="adminForm" id="adminForm">
    <table width="100%">
      <tr>
        <td width="120px"><?php echo ads_saferpay_account_id;?>: </td>
        <td><input size="40" name="account_id" class="inputbox" value="<?php echo $account_id;?>"></td>
      </tr>
      <tr>
        <td width="120px"><?php echo ads_saferpay_sppassword;?>: </td>
        <td><input size="40" name="sppassword" class="inputbox" value="<?php echo $sppassword;?>"></td>
      </tr>
      <tr>
        <td width="120px"><?php echo ads_saferpay_enabled;?>: </td>
        <td><?php echo $enabled; ?></td>
      </tr>
    </table>
    <input type="hidden" name="option" value="com_adsman"/>
    <input type="hidden" name="task" value="savepaymentconfig"/>
    <input type="hidden" name="paymenttype" value="<?php echo $this->classname;?>"/>
  </form>
  <?php

  }

  // Done.
  function save_admin_config()
  {
    $enabled = JRequest::getVar('enabled', '');
    $params = new JRegistry();

    $params->set('account_id', JRequest::getVar('account_id', ''));
    $params->set('sppassword', JRequest::getVar('sppassword',  ''));
    $params->set('enabled',	   $enabled);

    $paysystem = "saferpay";

    JTable::addIncludePath(JPATH_SITE.DS.'administrator'.DS.'components'.DS.'com_adsman'.DS.'tables');
    $paySystem =& JTable::getInstance('PaySystems','Table');
    $paySystem->id = null;
    $paySystem->set('_tbl_key', "classname");
    $paySystem->load($this->classname);

    if(!$paySystem->id){
      $this->_db->setQuery("INSERT INTO #__ads_paysystems set paysystem='$paysystem', params='$params',enabled='$enabled',classname='$this->classname'");
      $this->_db->query();
    } else {
      $this->_db->setQuery("update #__ads_paysystems set params='$params',enabled='$enabled' where classname='$this->classname'");
      $this->_db->query();
    }
  }

  // Done.
  function show_payment_form($order_id, $item_description, $itemname, $quantity, $price, $currency, $return_url = null)
  {
    $log = new JTheFactoryPaymentLog($this->_db);
    $log->set('_tbl_key', "invoice");

    // THIS IS FAR MORE IMPORTANT TO FORGET
    $log->load($order_id);
    $log->params = new JRegistry($log->params);

    $base    = JURI::root().'components/com_adsman/plugins/payment/saferpay/';
    $gateway = 'https://www.saferpay.com/hosting/CreatePayInit.asp';

    $array = array();
    $array['spPassword']  = $this->params->get('sppassword');
    $array['ACCOUNTID']   = $this->params->get('account_id');
    $array['ORDERID']     = $order_id;
    $array['AMOUNT']      = $price * $log->params->get('needed_credits') * 100;
    $array['CURRENCY']    = $currency;
    $array['DESCRIPTION'] = urlencode('"'.$item_description.'"');
    $array['SUCCESSLINK'] = '"'.$base.'success.php"';
    $array['FAILLINK']    = '"'.$base.'fail.php"';
    $array['BACKLINK']    = '"'.$base.'back.php"';
    $array['NOTIFYURL']   = '"'.$base.'notify.php"';

    if ('' == $array['spPassword']) {
      unset($array['spPassword']);
    }

    $attributes = array();
    foreach ($array as $key => $value) {
      $attributes[] = $key.'='.$value;
    }

    $url = $gateway.'?'.implode('&', $attributes);

    /* get the PayInit URL from the hosting server */
    $payinit_url = join("", file($url));

    $needed_credits = $log->params->get('needed_credits');
    $itemname       = $log->params->get('itemname');
    $total_price_ad = $log->params->get('total_price_ad');
    $credits        = $log->params->get('credits');

    if ($itemname != 'packages') {
      if ($itemname == 'contact') {
        echo JText::_("User Contact information costs")." ".$total_price_ad.' '.JText::_("ADS_CREDITS").' ('.JText::_("You have")." ".$credits.' '.JText::_("ADS_CREDITS").').';
      } else {
        echo JText::_("Ad costs")." ".$total_price_ad.' '.JText::_("ADS_CREDITS").' ('.JText::_("You have")." ".$credits.' '.JText::_("ADS_CREDITS").').';
      }
    }

    ?>

    <script src="http://www.saferpay.com/OpenSaferpayScript.js"></script>

    <h1><?php echo ads_saferpay_confirm_title; ?></h1>
    <p><?php echo sprintf(ads_saferpay_confirm_text, $item_description, number_format($price * $needed_credits, 2) . ' ' . $currency); ?></p>
    <a href="<?php echo $payinit_url; ?>" onclick="OpenSaferpayTerminal('<?php echo $payinit_url; ?>', this, 'LINK');"><?php echo ads_saferpay_continue; ?></a>

  <?php
  }

  // Done.
  function log_ipn_results($success)
  {
    if (!SAFERPAY_LOG) return;  // is logging turned off?
    // Timestamp
    $text = '['.date('m-d-Y g:i A').'] - ';

    // Success or failure being logged?
    if ($success) $text .= "SUCCESS!\n";
    else $text .= 'FAIL: '.$this->last_error."\n";

    // Log the POST variables
    $text .= "IPN POST Vars from 2Checkout:\n";
    foreach ($_POST as $field=>$value) {
      $text .= "$field=$value, ";
    }

    // Log the response from the paypal server
    $text .= "\nIPN Response from 2Checkout Server:\n ".$this->ipn_response;


  }

  // Done.
  function prepareLog($log, $variables = array())
  {
    $params = new JRegistry();

    $params->set('quantity',       $variables['quantity']);
    $params->set('itemname',       $variables['itemname']);
    $params->set('total_price_ad', $variables['total_price_ad']);
    $params->set('credits',        $variables['credits']);

    if ($variables['itemname'] == 'packages') {
      $needed_credits = $variables['quantity']; //$itemamount_package;
    } else {
      $needed_credits = $variables['total_price_ad'] - $variables['credits'];
    }

    $params->set('needed_credits', $needed_credits);

    $log->params = $params->toString();

    return $log;
  }

  // Done.
  protected function getIpn()
  {
    $data = $_POST['DATA'];
    $signature = $_POST['SIGNATURE'];

    $ipn = array(
      'order_id'   => 0,
      'amount'     => 0,
      'currency'   => '',
      'account_id' => 0,
      'reference'  => '',
      'token'      => '',
      'data'       => $data,
      'signature'  => $signature,
    );

    if (preg_match('/ORDERID=\\\"(.+)\\\"/U', $data, $match)) {
      $ipn['order_id'] = $match[1];
    }

    if (preg_match('/AMOUNT=\\\"(.+)\\\"/U', $data, $match)) {
      $ipn['amount'] = $match[1] / 100;
    }

    if (preg_match('/CURRENCY=\\\"(.+)\\\"/U', $data, $match)) {
      $ipn['currency'] = $match[1];
    }

    if (preg_match('/ACCOUNTID=\\\"(.+)\\\"/U', $data, $match)) {
      $ipn['account_id'] = $match[1];
    }

    if (preg_match('/ ID=\\\"(.+)\\\"/U', $data, $match)) {
      $ipn['reference'] = $match[1];
    }

    if (preg_match('/TOKEN=\\\"(.+)\\\"/U', $data, $match)) {
      $ipn['token'] = $match[1];
    }

    return $ipn;
  }

  // Done.
  protected function validateIpn($ipn)
  {
    $gateway = 'https://www.saferpay.com/hosting/VerifyPayConfirm.asp';
    $url     = $gateway.'?DATA='.urlencode($ipn['data']).'&SIGNATURE='.urlencode($ipn['signature']);
    $result  = join("", file($url));

    return substr($result, 0, 3) == "OK:" ? $result : false;
  }

  protected function validatePayment($ipn)
  {
    // Validate account id
    if ($this->params->get('account_id') != $ipn['account_id']) {
      return false;
    }

    return true;
  }
}
