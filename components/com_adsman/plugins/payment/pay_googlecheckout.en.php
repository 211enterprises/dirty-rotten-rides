<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

define('ads_googlecheckout_disclaimer','<strong>For your safety</strong> all payments will be redirected to Google checkout https:// server for processing');
define('ads_googlecheckout_buynow','Secure Google checkout payment');
define('ads_googlecheckout_email','Your Google checkout email');
define('ads_googlecheckout_use_sandbox','Use Sandbox');
define('ads_googlecheckout_enabled','Enabled');
define('ads_googlecheckout_merchantID', 'Merchant ID');
define('ads_googlecheckout_merchantKey', 'Merchant Key');
define('ads_googlecheckout_auto_accept','Auto Confirm Pending Orders');

//define('ads_googlecheckout','');
?>