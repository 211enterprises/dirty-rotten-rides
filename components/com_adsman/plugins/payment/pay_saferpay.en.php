<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

define('ads_saferpay_enabled','Enabled');
define('ads_saferpay_account_id','Account Id');
define('ads_saferpay_sppassword','SP Password (only for testing)');
define('ads_saferpay_confirm_title','Confirm Purchase');
define('ads_saferpay_confirm_text','Are you sure you want to purchase <b>%s</b> for <b>%s</b> using Saferpay?');
define('ads_saferpay_continue','Continue');
