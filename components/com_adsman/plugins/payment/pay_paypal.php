<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
define('PAY_PAYPAL_LOG',0);

if(!defined("ADS_COMPONENT_PATH"))
	define("ADS_COMPONENT_PATH", JPATH_ROOT."/components/com_adsman");

require_once(JPATH_ROOT."/components/com_adsman/plugins/payment/payment_object.php");

class pay_paypal extends payment_object{
    var $_db=null;
    var $classname="pay_paypal";
    var $classdescription="Paypal Payment method";

    var $ipn_response=null;
    var $action=null;
    
    function pay_paypal(&$db){
        parent::payment_object($db);

        $use_sandbox=$this->params->get('use_sandbox','');
        if ($use_sandbox){
            $this->action="https://www.sandbox.paypal.com/cgi-bin/webscr";
        }else{
            $this->action="https://www.paypal.com/cgi-bin/webscr";
        }

    }

    function ipn($d){
    	
        ob_clean();
        $log			= new JTheFactoryPaymentLog($this->_db);

        $log->set('_tbl_key', "invoice");

        $business 			= trim(stripslashes($_POST['business']));
        $item_name 			= trim(stripslashes($_POST['item_name']));
        $item_number 		= trim(stripslashes(@$_POST['item_number']));
        $payment_status 	= trim(stripslashes($_POST['payment_status']));

        // The order total amount including taxes, shipping and discounts
        $mc_gross 			= trim(stripslashes($_POST['mc_gross']));

        // Can be USD, GBP, EUR, CAD, JPY
        $currency_code 		=  trim(stripslashes($_POST['mc_currency']));

        $txn_id 			= trim(stripslashes($_POST['txn_id']));
        $receiver_email 	= trim(stripslashes($_POST['receiver_email']));
        $payer_email 		= trim(stripslashes($_POST['payer_email']));
        $payment_date 		= trim(stripslashes($_POST['payment_date']));

        // The Order Number (not order_id !)
        $invoice 			=  trim(stripslashes($_POST['invoice']));
		// THIS IS FAR MORE IMPORTANT TO FORGET
        $log->load($invoice);
        
        if( $log->status == "ok" )
        {
        	// nothing to accept ( accepted manually before IPN ) 
        	return; exit; die;
        }
                
        $amount 			= trim(stripslashes(@$_POST['amount']));
        $quantity 			= trim(stripslashes($_POST['quantity']));
        $pending_reason 	= trim(stripslashes(@$_POST['pending_reason']));
        $payment_method 	= trim(stripslashes(@$_POST['payment_method'])); // deprecated
        $payment_type 		= trim(stripslashes(@$_POST['payment_type']));

        // Billto
        $first_name 		= trim(stripslashes($_POST['first_name']));
        $last_name 			= trim(stripslashes($_POST['last_name']));
        $address_street 	= trim(stripslashes(@$_POST['address_street']));
        $address_city 		= trim(stripslashes(@$_POST['address_city']));
        $address_state 		= trim(stripslashes(@$_POST['address_state']));
        $address_zipcode 	= trim(stripslashes(@$_POST['address_zip']));
        $address_country 	= trim(stripslashes(@$_POST['address_country']));
        $residence_country 	= trim(stripslashes(@$_POST['residence_country']));
        $address_status 	= trim(stripslashes(@$_POST['address_status']));

        $payer_status 		= trim(stripslashes($_POST['payer_status']));
        $notify_version 	= trim(stripslashes($_POST['notify_version']));
        $verify_sign 		= trim(stripslashes($_POST['verify_sign']));
        $custom 			= trim(stripslashes(@$_POST['custom']));
        $txn_type 			= trim(stripslashes($_POST['txn_type']));
        
        $option_selection[1] = trim(stripslashes($_POST['option_selection1']));
        $option_selection[2] = trim(stripslashes($_POST['option_selection2']));
        $option_selection[3] = trim(stripslashes($_POST['option_selection3']));
        $option_selection[4] = trim(stripslashes($_POST['option_selection4']));
        
        $total_price_ad		= $option_selection[3];
		$featured_ad		= $option_selection[4];
        
        $log->date				= date('Y-m-d h:i:s');
        $log->amount			= $mc_gross;
        $log->currency			= $currency_code;
        $log->refnumber			= $txn_id;
        $log->invoice			= $invoice;
        $log->ipn_response		= print_r($_REQUEST,true);
        $log->ipn_ip			= $_SERVER['REMOTE_ADDR'];
        $log->comission_id		= 0;
        $log->userid			= $option_selection[1];
        $log->itemname			= $item_number;
        $log->payment_method	= $this->classname;

        switch  ($payment_status){
            case "Completed":
            case "Processed":
                $log->status='ok';

                //$msgC 		= JText::_("ADS_SUCCESS_CONTACT_PAYED");
            break;
            case "Failed":
            case "Denied":
            case "Canceled-Reversal":
            case "Expired":
            case "Voided":
            case "Reversed":
            case "Refunded":
                $log->status='error';
            break;
            default:
            case "In-Progress":
            case "Pending":
                $log->status='manual_check';
            break;
        }
        
        $log->store();
        
        $validate = $this->validate_ipn() && (strtolower($receiver_email)==$business=strtolower($this->params->get('paypalemail','')) );

        if ( $validate ) {
            //validate amounts and add item
            $price_classname="price_$item_number";

            if (file_exists(ADS_COMPONENT_PATH."/plugins/pricing/$price_classname.php"))
            {
                require_once(ADS_COMPONENT_PATH."/plugins/pricing/$price_classname.php");
                
                $price_class	= new $price_classname($this->_db);
                
                $value			= $quantity * $price_class->getPrice($d);
                $currency		= $price_class->getCurrency($d);
              
                $this->_db->setQuery("SELECT * FROM #__ads_pricing WHERE itemname='maincredit' ");
				$row = $this->_db->LoadObject();
                
                if ($item_number == 'packages') {
                	$credits_purchased = $price_class->getCredits($d);
                	$package_id = $custom;
					$package = price_packages_DBHelper::getPackage((int)$package_id);
										
					$quantity = $package->credits * $quantity;
                } else {
                	 $value = $row->price  * $quantity;
                	 $currency = $row->currency;
                }
                
				$auto_accept 	= $this->params->get('auto_accept','');
				
				$mc_gross = number_format($mc_gross, 2);
				$value    = number_format($value, 2);

				if ( ($mc_gross == $value) && $currency_code == $currency && ($log->status == 'ok' || $auto_accept)){
					
                    $price_class->acceptOrder($log, $quantity,$total_price_ad,$featured_ad);
                    $log->status = 'ok';
                    $log->store();
                    
                } else {

					$log->status = 'manual_check';
					$log->store();
                }
            }
        } else {
            $log->status = 'error';
            $log->store();
        }
        exit;
    }
    
    function show_admin_config()
    {

        $paypal_address	= $this->params->get('paypalemail','');
        $use_sandbox	= $this->params->get('use_sandbox','');
		$auto_accept 	= $this->params->get('auto_accept','');
        
		$sandbox	= JHTML::_("select.booleanlist",'use_sandbox','class="inputbox"',$use_sandbox);
		$enabled	= JHTML::_("select.booleanlist",'enabled','class="inputbox"',$this->enabled);
		
		$auto_accept_html	= JHTML::_("select.booleanlist",'auto_accept','class="inputbox"',$auto_accept);
        ?>
        <form action="<?php echo JRoute::_('index.php'); ?>" method="post" name="adminForm" id="adminForm">
		<table width="100%" class="adminlist" >
		<tr>
			<td class="paramlist key" style="width:250px;"><?php echo ads_paypal_paypalemail;?>: </td>
			<td><input size="40" name="paypalemail" class="inputbox" value="<?php echo $paypal_address;?>"></td>
		</tr>
		<tr>
            <td class="paramlist key" ><?php echo ads_paypal_use_sandbox;?>: </td>
            <td><?php echo $sandbox;?></td>
        </tr>
        <tr>
            <td class="paramlist key"><?php echo ads_paypal_enabled;?>: </td>
            <td><?php echo $enabled; ?></td>
        </tr>
        <tr>
            <td class="paramlist key"><?php echo ads_paypal_auto_accept;?>: </td>
            <td><?php echo $auto_accept_html; ?></td>
        </tr>
        </table>
        <input type="hidden" name="option" value="com_adsman"/>
        <input type="hidden" name="task" value="savepaymentconfig"/>
        <input type="hidden" name="paymenttype" value="<?php echo $this->classname;?>"/>
        </form>
        <?php

    }
    
    function save_admin_config()
    {
        $email		 = JRequest::getVar('paypalemail','');
        $use_sandbox = JRequest::getVar('use_sandbox',2);
        $enabled	 = JRequest::getVar('enabled','');
        $auto_accept = JRequest::getVar('auto_accept','');

        //$text		 = "paypal_email=$email\nuse_sandbox=$use_sandbox\nauto_accept=$auto_accept\n";

        $paypal_params = new JRegistry();

        $paypal_params->set('paypalemail',	$email);
        $paypal_params->set('use_sandbox',	$use_sandbox);
        $paypal_params->set('enabled',	$enabled);
        $paypal_params->set('auto_accept',	$auto_accept);

        $this->_db->setQuery("update #__ads_paysystems set params='$paypal_params',enabled='$enabled' where classname='$this->classname'");
        $this->_db->query();

    }

    function show_payment_form($order_id,$item_description,$itemname,$quantity,$price,$currency,$return_url=null)
    {
        $my 			= JFactory::getUser();
        $paypal_address	= $this->params->get('paypalemail','');
        $action			= $this->action;

        $session	= &JFactory::getSession();
        $session_vars = $session->get('posted_pm');

        $session->set('page_session',1);
        setcookie('page_cookie', 1);
        $post_itemname = $session_vars['itemname'];

        $object_id 		= JRequest::getVar("id",null);
        $var_name1 		= JRequest::getVar("var_name1",'package_id');
        $package_id 	= (int)JRequest::getVar("var_value1",null);

        $itemamount_package = JRequest::getVar('itemamount_package',1);
        $total_price_ad = (int)JRequest::getVar("var_value2",null);

		if ($total_price_ad == 0)
            $total_price_ad = $session_vars['total_price_ad'];

        $featured_ad 	= JRequest::getVar("var_value3",null);
        if ($featured_ad == null)
            $featured_ad = $session_vars['featured'];

        $credits = AdsUtilities::getUserCredits($my->id);
		if (!$return_url) 
			$return_url	= JURI::root()."index.php?option=com_adsman&task=payment&itemname=".$itemname."&paymenttype=".$this->classname."&act=return";

            $session->set('posted_pm',null);
            $session->set('packages_purchase_pm',null);

        if ($itemname == 'packages') {
            $needed_credits = $quantity; //$itemamount_package;
        } else {
            $needed_credits = $total_price_ad-$credits;

            if ($itemname == 'contact') {
                echo JText::_("User Contact information costs")." ".$total_price_ad.' '.JText::_("ADS_CREDITS").' ('.JText::_("You have")." ".$credits.' '.JText::_("ADS_CREDITS").').';
            } else {
                echo JText::_("Ad costs")." ".$total_price_ad.' '.JText::_("ADS_CREDITS").' ('.JText::_("You have")." ".$credits.' '.JText::_("ADS_CREDITS").').';
            }
        }

        ?>
            <div><strong><?php echo $item_description?></strong>&nbsp;: &nbsp;<?php echo number_format($price*$needed_credits,2,".","")," ",$currency;?></div>
            <form name='paypalForm' action="<?php echo $action;?>" method="post" name="paypal" onsubmit=''>
	    		<input type="hidden" name="cmd" value="_xclick">
	    		<input type="hidden" name="business" value="<?php echo $paypal_address; ?>">
	    		<input type="hidden" name="item_name" value="<?php echo $item_description; ?>">
	    		<input type="hidden" name="item_number" value="<?php echo $itemname; ?>">
	    		<input type="hidden" name="invoice" value="<?php echo $order_id; ?>">
	    		<input type="hidden" name="amount" value="<?php echo $price; ?>">
                <input type="hidden" name="quantity" value="<?php echo $needed_credits; ?>">
	    		<input type="hidden" name="return" value="<?php echo $return_url;?>">
	    		<input type="hidden" name="cancel_return" value="<?php echo JURI::root(); ?>index.php?option=com_adsman&task=payment&itemname=<?php echo $itemname;?>&paymenttype=<?php echo $this->classname;?>&act=cancel">
                <input type="hidden" name="notify_url" value="<?php echo JURI::root(); ?>components/com_adsman/plugins/payment/pay_paypal.notify.php">
                <input type="hidden" name="custom" value="<?php echo $package_id;?>">
            	<input type="hidden" name="on0" value="userid" />
            	<input type="hidden" name="os0" value="<?php echo $my->id; ?>" />
            	<input type="hidden" name="on1" value="object_id" />
            	<input type="hidden" name="os1" value="<?php echo $object_id; ?>" />
            	<input type="hidden" name="on2" value="total_price_ad" />
            	<input type="hidden" name="os2" value="<?php echo $total_price_ad; ?>">
	    		<input type="hidden" name="on3" value="featured_ad" />
            	<input type="hidden" name="os3" value="<?php echo $featured_ad; ?>">
            	
	    		<input type="hidden" name="tax" value="0" />
	    		<input type="hidden" name="rm" value="2" />
	    		<input type="hidden" name="no_note" value="1" />
	    		<input type="hidden" name="no_shipping" value="1" />
	    		<input type="hidden" name="currency_code" value="<?php echo $currency;?>">
		   		<input type="image" src="https://www.paypal.com/en_US/i/btn/x-click-but23.gif" name="submit" alt="<?php echo ads_paypal_buynow;?>" style="margin-left: 30px;">
            </form>
            <div><?php echo ads_paypal_disclaimer;?></div>
            <div></div>
            <br />
        <?php
    }
    
    function log_ipn_results($success) {
      
      if (!PAY_PAYPAL_LOG) return;  // is logging turned off?
      
      // Timestamp
      $text = '['.date('m-d-Y g:i A').'] - ';

      // Success or failure being logged?
      if ($success) {
      	$text .= "SUCCESS!\n";
      } else {
      	$text .= 'FAIL: '.$this->last_error."\n";
      }

      // Log the POST variables
      $text .= "IPN POST Vars from Paypal:\n";
      foreach ($_POST as $field=>$value) {

      	$text .= "$field=$value, ";
      	
      }

      // Log the response from the paypal server
      $text .= "\nIPN Response from Paypal Server:\n ".$this->ipn_response;
   }
   
   function validate_remote_ip()
   {
   	//DEV PURPOSE	
   	//return true;
        $paypal_iplist 	= gethostbynamel('www.paypal.com');
		$paypal_iplist2 = gethostbynamel('notify.paypal.com');
        $paypal_iplist 	= array_merge( $paypal_iplist, $paypal_iplist2 );

        $paypal_sandbox_hostname = 'ipn.sandbox.paypal.com';
        $remote_hostname = gethostbyaddr( $_SERVER['REMOTE_ADDR'] );

        $valid_ip = false;

        if( $paypal_sandbox_hostname == $remote_hostname ) 
        {
            $valid_ip = true;
            $hostname = 'www.sandbox.paypal.com';
        } else {
            $ips = "";
            // Loop through all allowed IPs and test if the remote IP connected here
            // is a valid IP address
            foreach ( $paypal_iplist as $ip ) {
            	
                $ips 	.= "$ip,\n";
                $parts 	 = explode( ".", $ip );
                $first_three = $parts[0].".".$parts[1].".".$parts[2];
                
                if ( preg_match("/^$first_three/", $_SERVER['REMOTE_ADDR']) ) {
                    $valid_ip = true;
                }
            }
            $hostname = 'www.paypal.com';
        }
        
       return $valid_ip;
   }
   
   	function validate_ipn() {
   	
   	//DEV PURPOSE	
	//return true;

      // parse the paypal URL
      $url_parsed = parse_url($this->action);
      //$url_parsed = parse_url($_SERVER["HTTP_REFERER"]);

      // generate the post string from the _POST vars aswell as load the
      // _POST vars into an arry so we can play with them from the calling
      // script.
      $post_string = '';
      
      foreach ($_POST as $field=>$value) {
         $post_string .= $field.'='.urlencode($value).'&';
      }
            
      $post_string.="cmd=_notify-validate"; // append ipn command
      // open the connection to paypal
      $fp = fsockopen("ssl://".$url_parsed['host'],443,$err_num,$err_str,30);

      if(!$fp) {

         // could not open the connection.  If loggin is on, the error message
         // will be in the log.
         $this->ipn_response = "fsockopen error no. $err_num: $err_str";
         $this->log_ipn_results(false);
         return false;

      } else {

         // Post the data back to paypal
         fputs($fp, "POST $url_parsed[path] HTTP/1.1\r\n");
         fputs($fp, "Host: $url_parsed[host]:443\r\n");
         fputs($fp, "Content-type: application/x-www-form-urlencoded\r\n");
         fputs($fp, "Content-length: ".strlen($post_string)."\r\n");
         fputs($fp, "Connection: close\r\n\r\n");
         fputs($fp, $post_string . "\r\n\r\n");

         // loop through the response from the server and append to variable
         while (!feof($fp)) {
            $this->ipn_response .= fgets($fp, 1024);
         }

         fclose($fp); // close connection

      }

      if (@eregi("VERIFIED",$this->ipn_response)) {

	     // Valid & VERIFIED payment IPN transaction.
         $this->log_ipn_results(true);
         return true;

      } else {
         // Invalid IPN transaction.  Check the log for details.
         $this->log_ipn_results(false);
         return false;

      }

   }
}
?>