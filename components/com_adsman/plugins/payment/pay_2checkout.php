<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
define('TWOCHECKOUT_LOG',0);

if(!defined("ADS_COMPONENT_PATH"))
	define("ADS_COMPONENT_PATH", JPATH_ROOT."/components/com_adsman");

//require_once(JPATH_ROOT."/components/com_adsman/plugins/payment/payment_object.php");
require_once(ADS_COMPONENT_PATH."/plugins/payment/payment_object.php");

class pay_2checkout extends payment_object{
    var $_db=null;
    var $classname="pay_2checkout";
    var $classdescription="2checkout Payment method";

    var $ipn_response=null;
    var $action=null;

    function pay_2checkout(&$db){
        parent::payment_object($db);

        $this->action="https://2checkout.com/app/payment.pl";

    }

    /**
     * 
     *
     * @param $d
     * @return 
     */
    function ipn($d){
        ob_clean();
        $log    = new JTheFactoryPaymentLog($this->_db);

		foreach ($_POST as $field=>$value){
			$this->ipnData["$field"] = $value;
		}

        $vendorNumber   = ($this->ipnData["vendor_number"] != '') ? $this->ipnData["vendor_number"] : $this->ipnData["sid"];
        $orderNumber    = $this->ipnData["order_number"];
        $orderTotal     = $this->ipnData["total"];

        // If demo mode, the order number must be forced to 1
        if($this->demo == "Y" || $this->ipnData['demo'] == 'Y')
        {
			$orderNumber = "1";
        }

        // Calculate md5 hash as 2co formula: md5(secret_word + vendor_number + order_number + total)
        $key = strtoupper(md5($this->secret . $vendorNumber . $orderNumber . $orderTotal));

        // verify if the key is accurate
        if($this->ipnData["key"] == $key || $this->ipnData["x_MD5_Hash"] == $key)
        {
			$this->log_ipn_results(true);
            return true;
        }
        else
        {
            $this->lastError = "Verification failed: MD5 does not match!";
            $this->log_ipn_results(false);
            return false;
        }
	}
    
    function show_admin_config()
    {
		$x_login    = $this->params->get('x_login','');
		$my_secret  = $this->params->get('my_secret','');
        $test_mode  = $this->params->get('test_mode','0');
		$enabled    = JHTML::_("select.booleanlist",'enabled','class="inputbox"',$this->enabled);
		$test       = JHTML::_("select.booleanlist",'test_mode','class="inputbox"',$test_mode);

        ?>
        <form action="<?php echo JRoute::_('index.php'); ?>" method="post" name="adminForm" id="adminForm">
        <table width="100%">
        <tr>
            <td width="120px"><?php echo ads_2checkout_x_login;?>: </td>
            <td><input size="40" name="x_login" class="inputbox" value="<?php echo $x_login;?>"></td>
        </tr>
        <tr>
            <td width="120px"><?php echo ads_2checkout_secret_word;?>: </td>
            <td><input size="40" name="my_secret" class="inputbox" value="<?php echo $my_secret;?>"></td>
        </tr>
        <tr>
            <td width="120px"><?php echo ads_2checkout_enabled;?>: </td>
            <td><?php echo $enabled; ?></td>
        </tr>
        <tr>
            <td width="120px"><?php echo ads_2checkout_test_mode;?>: </td>
            <td><?php echo $test; ?></td>
        </tr>
        </table>
        <input type="hidden" name="option" value="com_adsman"/>
        <input type="hidden" name="task" value="savepaymentconfig"/>
        <input type="hidden" name="paymenttype" value="<?php echo $this->classname;?>"/>
        </form>
        <?php

    }
    function save_admin_config()
    {
    	
    	$x_login    = JRequest::getVar('x_login','');
    	$my_secret  = JRequest::getVar('my_secret','');
        $test_mode  = JRequest::getVar('test_mode','');
        $enabled    = JRequest::getVar('enabled','');

        $pay2checkout_params = new JRegistry();

        $pay2checkout_params->set('x_login',	$x_login);
        $pay2checkout_params->set('my_secret',	$my_secret);
        $pay2checkout_params->set('test_mode',	$test_mode);
        $pay2checkout_params->set('enabled',	$enabled);

        //$text="test_mode=$test_mode\nx_login=$x_login\n\nmy_secret=$my_secret\n\n";
        $paysystem = "2checkout";
        
        JTable::addIncludePath(JPATH_SITE.DS.'administrator'.DS.'components'.DS.'com_adsman'.DS.'tables');
		$paySystem =& JTable::getInstance('PaySystems','Table');
        $paySystem->id=null;
        $paySystem->set('_tbl_key', "classname");
        $paySystem->load($this->classname);

        if(!$paySystem->id){
	        $this->_db->setQuery("INSERT INTO #__ads_paysystems set paysystem='$paysystem', params='$pay2checkout_params',enabled='$enabled',classname='$this->classname'");
	        $this->_db->query();
        } else {
	        $this->_db->setQuery("update #__ads_paysystems set params='$pay2checkout_params',enabled='$enabled' where classname='$this->classname'");
	        $this->_db->query();
        }
        
    }

    function show_payment_form($order_id,$item_description,$itemname,$quantity,$price,$currency,$return_url=null)
    {
    	$language = JFactory::getLanguage();
        $language->load('com_adsman');

        $my = JFactory::getUser();
        $x_login    = $this->params->get('x_login','');
        $test_mode  = $this->params->get('test_mode','');
        $action     = $this->action;
        $return_url = JURI::root()."/index.php?option=com_adsman&task=payment&itemname=".$itemname."&paymenttype=".$this->classname."&act=return";

        ?>
        <div><strong><?php echo $item_description?></strong>&nbsp;-&nbsp;<?php echo number_format($price*$quantity,2,".","")," ",$currency;?></div>
   	    <form action="https://www2.2checkout.com/2co/buyer/purchase" method="post" target="_blank">
			<input type='hidden' name='x_login' value='<?php echo $x_login;?>' />
			<input type='hidden' name='x_email_merchant' value='TRUE' />
			<input type='hidden' name='x_first_name' value='' />
			<input type='hidden' name='x_last_name' value='' />
			<input type='hidden' name='x_company' value='' />
			
			<input type='hidden' name='x_address' value='' />
			<input type='hidden' name='x_city' value='' />
			<input type='hidden' name='x_state' value='' />
			<input type='hidden' name='x_zip' value='' />
			<input type='hidden' name='x_country' value='' />
			<input type='hidden' name='x_phone' value='' />
			<input type='hidden' name='x_fax' value='' />
			<input type='hidden' name='x_email' value='' />
			<input type='hidden' name='x_ship_to_first_name' value='' />
			
			<input type='hidden' name='x_ship_to_last_name' value='' />
			<input type='hidden' name='x_ship_to_company' value='' />
			<input type='hidden' name='x_ship_to_address' value='' />
			<input type='hidden' name='x_ship_to_city' value='' />
			<input type='hidden' name='x_ship_to_state' value='' />
			<input type='hidden' name='x_ship_to_zip' value='' />
			<input type='hidden' name='x_ship_to_country' value='' />
			<input type='hidden' name='x_invoice_num' value='<?php echo $order_id; ?>' />
			<input type='hidden' name='x_receipt_link_url' value='<?php echo $return_url;?>' />
			
			<?php if($test_mode=="1") { ?>
			<input type='hidden' name='demo' value='Y' />
			<?php } ?>
			<input type='hidden' name='x_amount' value='<?php echo $price; ?>' />
			<p>Click on the Image below to pay...</p>
			<input type="image" name="submit" src="https://www.2checkout.com/images/buy_logo.gif" class="ads_noborder" alt="Make payments with 2Checkout, it's fast and secure!" title="Pay your Order with 2Checkout, it's fast and secure!" />
      </form>
		<div><?php echo ads_2checkout_disclaimer;?></div>
        <?php
    }

    function log_ipn_results($success) {
      if (!TWOCHECKOUT_LOG) return;  // is logging turned off?
      // Timestamp
      $text = '['.date('m-d-Y g:i A').'] - ';

      // Success or failure being logged?
      if ($success) $text .= "SUCCESS!\n";
      else $text .= 'FAIL: '.$this->last_error."\n";

      // Log the POST variables
      $text .= "IPN POST Vars from 2Checkout:\n";
      foreach ($_POST as $field=>$value) {
         $text .= "$field=$value, ";
      }

      // Log the response from the paypal server
      $text .= "\nIPN Response from 2Checkout Server:\n ".$this->ipn_response;


   }
   
    function validate_remote_ip()
   {
       //not known
       return true;
   }
   
    function validate_ipn()
    {
      $pay_to_email = trim(stripslashes($_POST['pay_to_email']));
      $md5sig = trim(stripslashes($_POST['md5sig']));
      //to do MD5 check
      $mbooker_address=trim($this->params->get('email',''));
      $payment_status = trim(stripslashes($_POST['status']));

      $verified_ok=(strtolower($mbooker_address)==strtolower($pay_to_email) && $payment_status=='2');

      $this->log_ipn_results($verified_ok);

      return $verified_ok;

   }
}
