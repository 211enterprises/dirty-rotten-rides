<?php

// Initialize Joomla framework
define('_JEXEC', 1);
define('DS', DIRECTORY_SEPARATOR);
define('JPATH_BASE', dirname(__FILE__).'/../../../../..');

/* Required Files */
require_once JPATH_BASE.'/includes/defines.php';
require_once JPATH_BASE.'/includes/framework.php';

$root = str_replace('components/com_adsman/plugins/payment/saferpay/', '', JURI::root());
$redirect	= $root ."index.php?option=com_adsman&view=user&task=myuserprofile";

$app = JFactory::getApplication('site');
$app->redirect($redirect, 'Payment failed!');
