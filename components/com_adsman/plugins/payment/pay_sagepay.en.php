<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

define('ads_sagepay_disclaimer','<strong>For your safety</strong> all payments will be redirected to Sage Pay checkout https:// server for processing');
define('ads_sagepay_buynow','Secure Sage Pay checkout payment');
define('ads_sagepay_vendor_name','Sage Pay Vendor Login Name');
define('ads_sagepay_encryption_password','Sage Pay Encryption password');
define('ads_sagepay_referrer_id','Sage Pay Referrer ID');
define('ads_sagepay_confirmation_email','Confirmation email address');
define('ads_sagepay_send_email','Send confirmation email');
define('ads_sagepay_mail_message', 'Email message');
define('ads_sagepay_enable_gift_aid','Enable Gift Aid');
define('ads_sagepay_apply_avscv2','AVS/CV2 checks');
define('ads_sagepay_apply_3dsecure','3D-Secure checks');

define('ads_sagepay_mode', 'Mode');
define('ads_sagepay_enabled','Enabled');

define('ads_sagepay_live_site','Live Site Sage Pay URL');
define('ads_sagepay_test_site','Test Site Sage Pay URL');
define('ads_sagepay_simulator_site','Simulator Site Sage Pay URL');

define('ads_sagepay_email','Sage Pay checkout email');

?>