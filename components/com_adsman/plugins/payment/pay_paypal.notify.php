<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/


// no direct access
    header("Status: 200 OK");

    require_once(dirname(__FILE__)."/pay_paypal.loadframework.php");
    $database = & JFactory::getDBO();
    
/*
$logfile=JPATH_ROOT."/a_paypal_ipn.log";
$fp=fopen($logfile,"w+");
fwrite($fp, print_r($_REQUEST,true) . "\n\n");
fwrite($fp, print_r($_SERVER,true) . "\n\n");
fclose($fp);  // close file
*/
    require_once(JPATH_ADMINISTRATOR.'/components/com_adsman/tables/addsman.php');
    require_once(JPATH_ADMINISTRATOR.'/components/com_adsman/tables/adsuser.php');
    require_once(dirname(__FILE__)."/pay_paypal.php");

	$payment = new pay_paypal($database);
	$payment->ipn($_REQUEST);
	
?>