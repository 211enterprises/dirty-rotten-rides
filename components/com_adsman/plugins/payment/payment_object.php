<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/


// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.html.parameter' );

require_once(dirname(__FILE__).DS."payment_log.php");
require_once(dirname(__FILE__).DS."..".DS. ".." .DS. "helpers".DS."helper.php");
JHTML::stylesheet ( 'main.css', 'components/com_adsman/css/' );

class payment_object
{
  var $_db			  = null;
  var $classname		  = "payment_object";
  var $classdescription = "generic Payment method";
  var $param_text;
  var $params			  = null;
  var $enabled		  = null;
  var $isdefault		  = null;

  function payment_object(&$db)
  {
    $this->_db=$db;
    $this->_db->setQuery("select params, enabled, isdefault from #__ads_paysystems where classname='$this->classname'");
    $r	= $this->_db->loadRow();

    $this->param_text	= $r[0];
    $this->enabled		= $r[1];
    $this->isdefault	= $r[2];

    $parameters = new JRegistry;
    $parameters->loadString($this->param_text);
    $this->params = $parameters;

    //$this->params	= new JRegistry($this->param_text);
    $this->loadPluginLanguage();
  }

  function loadPluginLanguage()
  {
    $language = JFactory::getLanguage();
    $language->load('com_adsman');
    //$lng = $language->getTag();
    $lng = substr( $language->get("tag") , 0, 2);

    if(file_exists(ADS_COMPONENT_PATH."/plugins/payment/{$this->classname}.$lng.php")){
      require_once(ADS_COMPONENT_PATH."/plugins/payment/{$this->classname}.$lng.php");
    } elseif (file_exists(ADS_COMPONENT_PATH."/plugins/payment/{$this->classname}.en.php"))
      require_once(ADS_COMPONENT_PATH."/plugins/payment/{$this->classname}.en.php");

  }

  function ipn($d)
  {
    //IPN Processing
    return;
  }

  function checkout($d)
  {
    //Checkout
    $my         = JFactory::getUser();
    $Itemid     = JRequest::getVar('Itemid', 0, 'REQUEST', 'integer');
    $session	= JFactory::getSession();
    $selected_paymenttype = JRequest::getVar('paymenttype', 'pay_paypal','REQUEST','word');
    $itemamount_package = JRequest::getVar('itemamount_package',0);

    $session_vars = $session->get('posted_pm');
    $post_itemname = $session_vars['itemname'];
    // $post_itemamount = $session_vars['needed_credits'];
    // $post_total_price_ad = isset($session_vars['total_price_ad']) ? $session_vars['total_price_ad'] : 0;

    $packages_purchase_pm = $session->get('packages_purchase_pm');

    $itemname   	 = JArrayHelper::getValue($d,'itemname','');
    $itemamount 	 = JArrayHelper::getValue($d,'itemamount',1);
    $itemamount 	 = (int)JArrayHelper::getValue($d,'itemamount',1);
    $return_url 	 = JArrayHelper::getValue($d,'return_url',null);
    $price_classname = ($itemname!= '') ? "price_$itemname" : "price_$post_itemname";

    if ($itemname == '') {
      $itemname = $post_itemname;
    }

    if (file_exists(ADS_COMPONENT_PATH."/plugins/pricing/$price_classname.php")) {
      require_once(ADS_COMPONENT_PATH."/plugins/pricing/$price_classname.php");
    } else {
      echo ads_err_no_payment_item;
      return;
    }

    $price_class = new $price_classname($this->_db);

    $item_description = $price_class->getDescription();

    if ($itemname == 'packages') {
      $value = $price_class->getPrice($d); //= featured silver price in credits -we need 1 credit value in currency
      //$package_id = JRequest::getVar('package_id', 0, 'POST', 'integer');
      $package_id 	= (int)JRequest::getVar("var_value1",null);
      $total_price_ad = (int)JRequest::getVar("var_value2",null);
      $featured_ad    = 	JRequest::getVar("var_value3",null);

      if (isset($itemamount_package) && $itemamount_package != 0) $itemamount = $itemamount_package;

    }
    else {
      $this->_db->setQuery("SELECT * FROM #__ads_pricing WHERE itemname='maincredit' ");
      $row = $this->_db->LoadObject();
      $value = $row->price;
    }

    $currency = $price_class->getCurrency($d);

    if ($currency == 'credits' || $currency =='') {
      //$db->setQuery("SELECT * FROM #__ads_pricing WHERE itemname='maincredit' ");
      $currency = $row->currency;
    }

    // Invoice actually
    $order_id = $price_class->initiateOrder();

    // If BUY stuff for a specific item ; else could buy plain credits for later usage
    $object_id = JRequest::getVar("id",null);

    if ($object_id == null && isset($session_vars['id'])) {
      $object_id = $session_vars['id'];
    }

    if ($itemname == 'packages' && isset($packages_purchase_pm['ad_id'])) {
      $object_id = $packages_purchase_pm['ad_id'];
    }

    if ($itemamount == 1 && $itemname == 'contact') {
      $itemamount = $session_vars['itemamount'];
      $return_url = $session_vars['return_url'];
    }

    /**
     * Checkout starts with adding the payment to log pending like
     *  TO DO: move this in an action exactly when redirecting to the payment portal
     *
     */
    $log = new JTheFactoryPaymentLog($this->_db);
    $log->set('_tbl_key', "invoice");

    $cuc = $session->get("current_order");
    $log->load($order_id);
    $hour = date("H",time());

    if( 1 || (!$log->id && $cuc!=($value*$itemamount)." ".$object_id." ".$hour) ) {
      $session->set("current_order",($value*$itemamount)." ".$object_id." ".$hour);

      $log->set('_tbl_key', "id");
      $log->date		= date('Y-m-d h:i:s');
      $log->invoice 	= $order_id;
      $log->amount 	= ($value*$itemamount);
      $log->object_id = $object_id;
      $log->userid 	= $my->id;
      $log->ipn_ip 	= "Payment Waiting";
      $log->currency 	= $currency;
      $log->itemname 	= $itemname;
      //$log->var_name1 = trim(stripslashes($_REQUEST['var_name1']));
      $log->var_name1 = 'ad_id';
      //$log->var_name2 = trim(stripslashes($_REQUEST['var_name2']));
      $log->var_name2 = 'total_price_ad';
      //$log->var_name3 = trim(stripslashes($_REQUEST['var_name3']));
      $log->var_name3 = 'featured_ad';
      //$log->var_name4 = trim(stripslashes($_REQUEST['var_name4']));

      $value1 = ($itemname == 'packages') ? $package_id : $session_vars['id'];
      $value2 = ($itemname == 'packages') ? $total_price_ad : $session_vars['total_price_ad'];
      $value3 = ($itemname == 'packages') ? $featured_ad : $session_vars['featured'];


      $log->var_value1 = trim(stripslashes($value1));
      $log->var_value2 = trim(stripslashes($value2));
      $log->var_value3 = trim(stripslashes($value3));
      //$log->var_value4 = trim(stripslashes($_REQUEST['var_value4']));

       $my 			= JFactory::getUser();
       $credits = AdsUtilities::getUserCredits($my->id);

      $variables = array(
        'quantity'       => $itemamount,
        'itemname'       => $itemname,
        'total_price_ad' => $value2,
        'credits'        => $credits,
      );

      $log = $this->prepareLog($log, $variables);

      $log->store();
    }

    if ($selected_paymenttype != 'pay_sagepay') {
      $this->show_payment_form($order_id,$item_description,$itemname,$itemamount,$value,$currency,$return_url);
    } else {
      $this->show_sagepay_details_form($order_id,$item_description,$itemname,$itemamount,$value,$currency,$return_url);
    }
  }

  function show_payment_form($order_id,$item_description,$itemname,$quantity,$price,$currency,$return_url=null)
  {
    //Here comes the HTML for the payment form
  }

  function show_admin_config()
  {
    //admin
  }

  function save_admin_config()
  {
    //save
  }

  function accept_ipn($d,$order_id,$price_classname,$quantity,$amount_received,$currency_code)
  {
    if (file_exists(ADS_COMPONENT_PATH."/plugins/pricing/$price_classname.php")){
      require_once(ADS_COMPONENT_PATH."/plugins/pricing/$price_classname.php");

      $price_class = new $price_classname($this->_db);

      $value 		= $quantity * $price_class->getPrice($d);
      $currency 	= $price_class->getCurrency($d);

      if ($amount_received == $value && $currency_code == $currency) {

        $price_class->acceptOrder($order_id,$quantity);
        return true;

      } else {
        return false;
      }
    } else return false;

  }

  function getLogo()
  {
    $base = JURI::root();

    if (file_exists(ADS_COMPONENT_PATH."/plugins/payment/{$this->classname}.gif"))
      return $base."components/com_adsman/plugins/payment/{$this->classname}.gif";
    if (file_exists(ADS_COMPONENT_PATH."/plugins/payment/{$this->classname}.png"))
      return $base."components/com_adsman/plugins/payment/{$this->classname}.png";
    if (file_exists(ADS_COMPONENT_PATH."/plugins/payment/{$this->classname}.jpg"))
      return $base."components/com_adsman/plugins/payment/{$this->classname}.jpg";

    return $base."components/com_adsman/plugins/payment/payment.jpg";
  }

  function prepareLog($log, $variables = array())
  {
    return $log;
  }
}
?>
