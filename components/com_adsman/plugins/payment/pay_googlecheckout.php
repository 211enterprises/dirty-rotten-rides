<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
define('PAY_GOOGLECHECKOUT_LOG',0);

if(!defined("ADS_COMPONENT_PATH"))
	define("ADS_COMPONENT_PATH", JPATH_ROOT."/components/com_adsman");

require_once(JPATH_ROOT."/components/com_adsman/plugins/payment/payment_object.php");

class pay_googlecheckout extends payment_object {
    var $_db = null;
    var $classname = "pay_googlecheckout";
    var $classdescription = "Google Checkout Payment method";

    var $ipn_response = null;
    var $action = null;
    
    function pay_googlecheckout(&$db){

        parent::payment_object($db);

        $use_sandbox    = $this->params->get('use_sandbox','');

        if ($use_sandbox){
            $this->action = "https://sandbox.google.com/checkout/api/checkout/v2/checkoutForm/Merchant/MERCHANT_ID";
        }else{
            $this->action = "https://checkout.google.com/api/checkout/v2/checkoutForm/Merchant/MERCHANT_ID";
        }

        $this->action = str_replace('MERCHANT_ID', $this->params->get('merchant_id'), $this->action);

    }

    function ipn($d){

        // Send notification acknowledgment
        header("Status: 200");
        echo '_type=notification-acknowledgment&serial-number=' . $d->get('serial-number');

        // Check notification type
        if ('authorization-amount-notification' != $d->get('_type')) {
          return false;
        }

        // Preprocess ipn
        // Create payment (save into paylog table)
        ob_clean();
        $log	= new JTheFactoryPaymentLog($this->_db);
        $log->set('_tbl_key', "invoice");

        // The Order Number (not order_id !)
        $invoice = $d->get('order-summary_shopping-cart_items_item-1_merchant-item-id');
        $log->load($invoice);

        if( $log->status == "ok" )
        {
        	// nothing to accept ( accepted manually before IPN )
        	return; exit; die;
        }

        $log->date				= date('Y-m-d h:i:s');
        $log->amount			= $d->get('order-summary_authorization_authorization-amount');
        $log->currency			= $d->get('order-summary_authorization_authorization-amount_currency');
        $log->refnumber			= $d->get('google-order-number');
        $log->invoice			= $d->get('order-summary_shopping-cart_items_item-1_merchant-item-id');

        $log->ipn_response		= print_r($_REQUEST,true);
        $log->ipn_ip			= $_SERVER['REMOTE_ADDR'];
        $log->comission_id		= 0;
        $log->userid			= $d->get('order-summary_shopping-cart_items_item-1_item-description');
        $log->userid            = str_replace('user id: ', '', $log->userid);
        $log->payment_method	= $this->classname;

        $payment_status = $d->get('order-summary_financial-order-state');

        switch ($payment_status) {
            case 'CHARGED':
            case 'CHARGEABLE':
                $log->status = 'ok';
                break;
            case 'PAYMENT_DECLINED':
                $log->status = 'error'; // Failed
                break;

            default:
            case 'REVIEWING':
                $log->status = 'manual_check'; //Pending
                break;
        }

        $log->store();

        $itemname           = $d->get('order-summary_shopping-cart_items_item-1_item-name');
        $quantity           = $d->get('order-summary_shopping-cart_items_item-1_quantity');
        $package_price      = $d->get('order-summary_shopping-cart_items_item-1_unit-price');
        $package_currency   = $d->get('order-summary_shopping-cart_items_item-1_unit-price_currency');

        $total_price_ad = $log->var_value2;
        $featured_ad = $log->var_value3;

        //check for errors
        $validate = $this->validate_ipn();

        if ( $validate ) {
            //validate amounts and add item
            $price_classname="price_$itemname";
            
            if (file_exists(ADS_COMPONENT_PATH."/plugins/pricing/$price_classname.php"))
            {
                require_once(ADS_COMPONENT_PATH."/plugins/pricing/$price_classname.php");
                
                $price_class	= new $price_classname($this->_db);
                
                $value			= $quantity * $package_price;   // $price_class->getPrice($d);
                $currency		= $package_currency;    //$price_class->getCurrency($d);
              
                $this->_db->setQuery("SELECT * FROM #__ads_pricing WHERE itemname='maincredit' ");
				$row = $this->_db->LoadObject();
                
                if ($itemname == 'packages') {
                	$credits_purchased = $price_class->getCredits($d);
                	$package_id = $log->var_value1;
					$package = price_packages_DBHelper::getPackage((int)$package_id);
										
					$quantity = $package->credits * $quantity;
                } else {
                	 $value = $row->price  * $quantity;
                	 $currency = $row->currency;
                }
                
				$auto_accept 	= $this->params->get('auto_accept','');

                $order_amount = number_format($d->get('order-summary_authorization_authorization-amount'), 2);
				$value    = number_format($value, 2);

                $currency_code = $d->get('order-summary_authorization_authorization-amount_currency');

				if ( ($order_amount == $value) && $currency_code == $currency && ($log->status == 'ok' || $auto_accept)){

                    $price_class->acceptOrder($log, $quantity,$total_price_ad,$featured_ad);
                    $log->status = 'ok';
                    $log->store();
                    
                } else {

					$log->status = 'manual_check';
					$log->store();
                }
            }
        } else {
            $log->status = 'error';
            $log->store();
        }
        exit;
    }
    
    function show_admin_config()
    {

        $googlecheckout_address	= $this->params->get('googlecheckoutemail','');
        $googlecheckout_merchantID	= $this->params->get('googlecheckout_merchantID','');
        $googlecheckout_merchantKey	= $this->params->get('googlecheckout_merchantKey','');
        $use_sandbox	= $this->params->get('use_sandbox','');
		$auto_accept 	= $this->params->get('auto_accept','');
        
		$sandbox	= JHTML::_("select.booleanlist",'use_sandbox','class="inputbox"',$use_sandbox);
		$enabled	= JHTML::_("select.booleanlist",'enabled','class="inputbox"',$this->enabled);
		
		$auto_accept_html	= JHTML::_("select.booleanlist",'auto_accept','class="inputbox"',$auto_accept);
        ?>
        <form action="<?php echo JRoute::_('index.php'); ?>" method="post" name="adminForm" id="adminForm">
		<table width="100%" class="adminlist" >
		<tr>
			<td class="paramlist key required" style="width:250px;"><?php echo ads_googlecheckout_email;?>: </td>
			<td><input size="40" name="googlecheckoutemail" class="inputbox required" value="<?php echo $googlecheckout_address;?>"></td>
		</tr>
		<tr>
            <td class="paramlist key" ><?php echo ads_googlecheckout_use_sandbox;?>: </td>
            <td><?php echo $sandbox;?></td>
        </tr>
        <tr>
            <td class="paramlist key"><?php echo ads_googlecheckout_enabled;?>: </td>
            <td><?php echo $enabled; ?></td>
        </tr>
        <tr>
			<td class="paramlist key" style="width:250px;"><?php echo ads_googlecheckout_merchantID;?>: </td>
			<td><input size="50" name="googlecheckout_merchantID" class="inputbox required" value="<?php echo $googlecheckout_merchantID;?>"></td>
		</tr>
        <tr>
			<td class="paramlist key" style="width:250px;"><?php echo ads_googlecheckout_merchantKey;?>: </td>
			<td><input size="50" name="googlecheckout_merchantKey" class="inputbox required" value="<?php echo $googlecheckout_merchantKey;?>"></td>
		</tr>
        

        <tr>
            <td class="paramlist key"><?php echo ads_googlecheckout_auto_accept;?>: </td>
            <td><?php echo $auto_accept_html; ?></td>
        </tr>
        </table>
        <input type="hidden" name="option" value="com_adsman"/>
        <input type="hidden" name="task" value="savepaymentconfig"/>
        <input type="hidden" name="paymenttype" value="<?php echo $this->classname;?>"/>
        </form>
        <?php

    }
    
    function save_admin_config()
    {
        $email		 = JRequest::getVar('googlecheckoutemail','');
        $use_sandbox = JRequest::getVar('use_sandbox',2);
        $enabled	 = JRequest::getVar('enabled','');
        $merchantID  = JRequest::getVar('googlecheckout_merchantID','');
        $merchantKey = JRequest::getVar('googlecheckout_merchantKey','');
        $auto_accept = JRequest::getVar('auto_accept','');
        $paysystem   = "googlecheckout";

        $google_params = new JRegistry();

        $google_params->set('googlecheckoutemail',	$email);
        $google_params->set('use_sandbox',	$use_sandbox);
        $google_params->set('enabled',	$enabled);
        $google_params->set('googlecheckout_merchantID',	$merchantID);
        $google_params->set('googlecheckout_merchantKey',	$merchantKey);
        $google_params->set('auto_accept',	$auto_accept);

       // $text		 = "googlecheckout_email=$google_params->email\nuse_sandbox=$use_sandbox\nauto_accept=$auto_accept\ngooglecheckout_merchantID=$merchantID\ngooglecheckout_merchantKey=$merchantKey\n";

        JTable::addIncludePath(JPATH_SITE.DS.'administrator'.DS.'components'.DS.'com_adsman'.DS.'tables');
		$paySystem =& JTable::getInstance('PaySystems','Table');
        $paySystem->id=null;
        $paySystem->set('_tbl_key', "classname");
        $paySystem->load($this->classname);

        if(!$paySystem->id){
	        $this->_db->setQuery("INSERT INTO #__ads_paysystems set paysystem='$paysystem', params='$google_params',enabled='$enabled' ,classname='$this->classname'");
	        $this->_db->query();
        } else {
	        $this->_db->setQuery("update #__ads_paysystems set params='$google_params',enabled='$enabled' where classname='$this->classname'");
	        $this->_db->query();
        }

    }

    function show_payment_form($order_id,$item_description,$itemname,$quantity,$price,$currency,$return_url=null)
    {
        $my 	= JFactory::getUser();
        $googlecheckout_address	= $this->params->get('googlecheckout_email','');
        $merchantID = $this->params->get('googlecheckout_merchantID','');
        $action	= $this->action . $merchantID;

        $session	= JFactory::getSession();
        $session_vars = $session->get('posted_pm');

        $session->set('page_session',1);
        setcookie('page_cookie', 1);
        $post_itemname = $session_vars['itemname'];

        $object_id 		= JRequest::getVar("id",null);
        
        $var_name1 		= JRequest::getVar("var_name1",'package_id');
        $package_id 	= (int)JRequest::getVar("var_value1",null);

        $itemamount_package = JRequest::getVar('itemamount_package',1);
        $total_price_ad = (int)JRequest::getVar("var_value2",null);

		if ($total_price_ad == 0)
            $total_price_ad = $session_vars['total_price_ad'];

        $featured_ad 	= JRequest::getVar("var_value3",null);
        if ($featured_ad == null)
            $featured_ad = $session_vars['featured'];

        $credits = AdsUtilities::getUserCredits($my->id);
		if (!$return_url) 
			$return_url	= JURI::root()."index.php?option=com_adsman&task=payment&itemname=".$itemname."&paymenttype=".$this->classname."&act=return";

            $session->set('posted_pm',null);
            $session->set('packages_purchase_pm',null);

        if ($itemname == 'packages') {
            $needed_credits = $quantity;
                    //$itemamount_package;
        } else {
            $needed_credits = $total_price_ad-$credits;
            echo JText::_("Ad costs")." ".$total_price_ad.' '.JText::_("ADS_CREDITS").' ('.JText::_("You have")." ".$credits.' '.JText::_("ADS_CREDITS").').';
        }

        if (file_exists(ADS_COMPONENT_PATH."/plugins/payment/buynow_google.gif"))
			$buy_button = JURI::root()."components/com_adsman/plugins/payment/buynow_google.gif";
        else
            $buy_button = "http://sandbox.google.com/checkout/buttons/buy.gif?merchant_id=$merchantID&w=117&h=48&style=white&variant=text&loc=en_US";

        ?>
            <div><strong><?php echo $item_description?></strong>&nbsp;:&nbsp;<?php echo number_format($price*$needed_credits,2,".","")," ",$currency;?></div>
            <form name='GoogleCheckoutForm' action="<?php echo $action;?>" method="post" onsubmit=''>
	    	    <input type="hidden" name="item_name_1" value="<?php echo $itemname; ?>"/>
                <input type="hidden" name="item_description_1" value="user id: <?php echo $my->id; ?>"/>
                <input type="hidden" name="item_price_1" value="<?php echo $price; ?>"/>
                <input type="hidden" name="item_currency_1" value="<?php echo $currency;?>"/>
                <input type="hidden" name="item_quantity_1" value="<?php echo $needed_credits; ?>"/>
                <input type="hidden" name="item_merchant_id_1" value="<?php echo $order_id; ?>"/>
                <input type="hidden" name="continue_url" value="<?php echo $return_url; ?>"/>
                <input type="hidden" name="_type" value="checkout-shopping-cart"/>
  	   		    <input type="image" name="submit" src="<?php echo $buy_button; ?>" alt="<?php echo ads_googlecheckout_buynow;?>" style="margin-left: 30px;">
            </form>
            <div><?php echo ads_googlecheckout_disclaimer;?></div>
            <div></div>
            <br />
        <?php
    }
    
    function log_ipn_results($success) {
      
      if (!PAY_GOOGLECHECKOUT_LOG) return;  // is logging turned off?
      
      // Timestamp
      $text = '['.date('m-d-Y g:i A').'] - ';

      // Success or failure being logged?
      if ($success) {
      	$text .= "SUCCESS!\n";
      } else {
      	$text .= 'FAIL: '.$this->last_error."\n";
      }

      // Log the POST variables
      $text .= "IPN POST Vars from GoogleCheckout:\n";
      foreach ($_POST as $field=>$value) {

      	$text .= "$field=$value, ";
      	
      }

      // Log the response from the paypal server
      $text .= "\nIPN Response from GoogleCheckout Server:\n ".$this->ipn_response;
   }
   

    protected function validate_ipn() {
   	
   	    //DEV PURPOSE
	    //return true;

        $authorization = base64_encode($this->params->get('googlecheckout_merchantID') . ':' .  $this->params->get('googlecheckout_merchantKey'));
        if (!isset($_SERVER['HTTP_AUTHORIZATION']) || $authorization != $_SERVER['HTTP_AUTHORIZATION']) {
            return false;
        }

        return true;
   }

}
?>