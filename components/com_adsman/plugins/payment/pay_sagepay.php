<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
define('PAY_SAGEPAYCHECKOUT_LOG',0);

if(!defined("ADS_COMPONENT_PATH"))
	define("ADS_COMPONENT_PATH", JPATH_ROOT."/components/com_adsman");

require_once(JPATH_ROOT."/components/com_adsman/plugins/payment/payment_object.php");

class pay_sagepay extends payment_object {
    var $_db = null;
    var $classname = "pay_sagepay";
    var $classdescription = "Sage Pay Payment method";

    var $ipn_response = null;
    var $action = null;
    
    function pay_sagepay(&$db){

        parent::payment_object($db);
    }

    function ipn(){

        $crypt     = JRequest::getString('crypt');
        $decrypted = $this->simpleXor($this->base64Decode($crypt), $this->params->get('sagepay_encryption_password'));
        $exploded  = explode('&', $decrypted);

        $d = $ipn = new JRegistry();
        foreach ($exploded as $field)
        {
          $field = explode('=', $field);
          $vars[$field[0]] = $field[1];
          $d->set($field[0], $field[1]);
        }

        // Preprocess ipn
        // Create payment (save into paylog table)
        ob_clean();
        $log	= new JTheFactoryPaymentLog($this->_db);
        $log->set('_tbl_key', "invoice");

        // The Order Number (not order_id !)
        $invoice = $d->get('VendorTxCode'); //order_id
        $log->load($invoice);

        if( $log->status == "ok" )
        {
        	// nothing to accept ( accepted manually before IPN )
        	return; exit; die;
        }

        //$quantity 			= trim(stripslashes($_POST['quantity'])); == needed credits

        //$log->date				= JFactory::getDate()->toMySQL(); //date('Y-m-d h:i:s');
        $log->amount			= $d->get('Amount');
        $log->refnumber			= $d->get('VPSTxId');
        $log->invoice			= $d->get('VendorTxCode');
        $log->ipn_response		= print_r($d,true);
        $log->ipn_ip			= $_SERVER['REMOTE_ADDR'];
        //$log->status            = $d->get('Status');
        //$log->userid            = JFactory::getUser()->id;
        $log->payment_method	= $this->classname;

        $payment_status = $d->get('Status');

        switch ($payment_status) {
            case 'OK':
                $log->status = 'ok';
                break;

            case 'NOTAUTHED':
            case 'MALFORMED':
            case 'INVALID':
            case 'ABORT':
            case 'REJECTED':
            case 'ERROR':
                $log->status = 'error'; // Failed
                break;

            default:
            //case 'REVIEWING':
                $log->status = 'manual_check'; //Pending
                break;
        }

        $log->store();

        $itemname = $log->itemname;
        $total_price_ad = $log->var_value2;
        $featured_ad = $log->var_value3;

        if ( $payment_status == 'OK' ) {
            //validate amounts and add item
            $price_classname="price_$itemname";
            
            if (file_exists(ADS_COMPONENT_PATH."/plugins/pricing/$price_classname.php"))
            {
                require_once(ADS_COMPONENT_PATH."/plugins/pricing/$price_classname.php");
                
                $price_class	= new $price_classname($this->_db);

                $order_amount = number_format($d->get('Amount'), 2);
                $log_value = number_format($log->amount,2);
                //$value			= $quantity * $price_class->getPrice($d);
                //$currency		= $price_class->getCurrency($d);
              
                $this->_db->setQuery("SELECT * FROM #__ads_pricing WHERE itemname='maincredit' ");
				$row = $this->_db->LoadObject();

                if ($itemname == 'packages') {
                	$credits_purchased = $price_class->getCredits($d);
                	$package_id = $log->var_value1;
					$package = price_packages_DBHelper::getPackage((int)$package_id);

                    if ($package->credits != 0) {
                        //$quantity = $package->credits * $quantity;
                        $packages_quantity = $order_amount / $package->price;
                        $quantity = $packages_quantity * $package->credits;
                    }

                } else {
                    if ($row->price != 0) {
                	    //$value = $row->price  * $quantity;
                        $quantity = $order_amount / $row->price;
                    } else {
                        $quantity = 0;
                    }
                	//$currency = $row->currency;
                }

				if ( ($order_amount == $log_value) && ($log->status == 'ok')){

                    $price_class->acceptOrder($log, $quantity,$total_price_ad,$featured_ad);
                    $log->status = 'ok';
                    $log->store();
                    
                } else {

					$log->status = 'manual_check';
					$log->store();
                }
            }
        } else {
            $log->status = 'error';
            $log->store();
        }
        exit;
    }
    
    function show_admin_config()
    {
        $enabled	    = JHTML::_("select.booleanlist",'enabled','class="inputbox"',$this->enabled);

        $sagepay_email	= $this->params->get('sagepay_email','');
        $select_mode	= $this->params->get('mode','');
        $sagepay_vendorname	            = $this->params->get('sagepay_vendorname','');
        $sagepay_encryption_password    = $this->params->get('sagepay_encryption_password','');
        $select_send_email              = $this->params->get('send_email',0);
        $select_apply_avscv2            = $this->params->get('apply_avscv2',0);
        $select_apply_3dsecure          = $this->params->get('apply_3dsecure',0);

        // Send confirmation email
        $confirmation_opts = array();
        $confirmation_opts[] = JHTML::_('select.option', 0, JText::_('ADS_SAGEPAY_SEND_CONFIRMATION_EMAIL_NO_EMAILS'));
        $confirmation_opts[] = JHTML::_('select.option', 1, JText::_('ADS_SAGEPAY_SEND_CONFIRMATION_EMAIL_BOTH'));
        $confirmation_opts[] = JHTML::_('select.option', 2, JText::_('ADS_SEND_CONFIRMATION_EMAIL_VENDOR'));
        $send_email =	JHTML::_('select.genericlist',  $confirmation_opts, 'send_email', 'class="inputbox" alt="send email confirmation"',  'value', 'text',$select_send_email);

        // Mode
        $opts = array();
        $opts[] = JHTML::_('select.option', 0, JText::_('ADS_SAGEPAY_LIVE_MODE'));
        $opts[] = JHTML::_('select.option', 1, JText::_('ADS_SAGEPAY_TEST_MODE'));
        $opts[] = JHTML::_('select.option', 2, JText::_('ADS_SAGEPAY_SIMULATOR_MODE'));
        $mode =	JHTML::_('select.genericlist',  $opts, 'mode', 'class="inputbox" alt="pay mode"',  'value', 'text',$select_mode);

        // AVS/CV2 checks
        $avscv2_opts = array();
        $avscv2_opts[] = JHTML::_('select.option', 0, JText::_('ADS_SAGEPAY_APPLY_AVSCV_CHECK'));
        $avscv2_opts[] = JHTML::_('select.option', 1, JText::_('ADS_SAGEPAY_APPLY_AVSCV_FORCE_CHECK'));
        $avscv2_opts[] = JHTML::_('select.option', 2, JText::_('ADS_SAGEPAY_APPLY_AVSCV_NO_CHECK'));
        $avscv2_opts[] = JHTML::_('select.option', 3, JText::_('ADS_SAGEPAY_APPLY_AVSCV_FORCE_DONT_APPLY'));
        $apply_avscv2 =	JHTML::_('select.genericlist',  $avscv2_opts, 'apply_avscv2', 'class="inputbox" alt="apply avscv2"',  'value', 'text',$select_apply_avscv2);

        // 3D-Secure checks
        $apply_3dsecure_opts = array();
        $apply_3dsecure_opts[] = JHTML::_('select.option', 0, JText::_('ADS_SAGEPAY_SECURE_CHECKS_PERFORM'));
        $apply_3dsecure_opts[] = JHTML::_('select.option', 1, JText::_('ADS_SAGEPAY_SECURE_CHECKS_FORCE'));
        $apply_3dsecure_opts[] = JHTML::_('select.option', 2, JText::_('ADS_SAGEPAY_SECURE_CHECKS_NO_PERFORM'));
        $apply_3dsecure_opts[] = JHTML::_('select.option', 3, JText::_('ADS_SAGEPAY_SECURE_CHECKS_FORCE_AUTH_CODE'));
        $apply_3dsecure =	JHTML::_('select.genericlist',  $apply_3dsecure_opts, 'apply_3dsecure', 'class="inputbox" alt="apply 3dsecure"',  'value', 'text',$select_apply_3dsecure);

        //$auto_accept 	= $this->params->get('auto_accept','');
		//$auto_accept_html	= JHTML::_("select.booleanlist",'auto_accept','class="inputbox"',$auto_accept);


        ?>
        <form action="<?php echo JRoute::_('index.php'); ?>" method="post" name="adminForm" id="adminForm">
		<table width="100%" class="adminlist" >
        <tr>
            <td class="paramlist key"><?php echo ads_sagepay_enabled;?>: </td>
            <td><?php echo $enabled; ?></td>
        </tr>
		<tr>
			<td class="paramlist key" style="width:250px;"><?php echo ads_sagepay_email;?><span class="red"> * </span></td>
			<td><input size="40" name="sagepay_email" class="inputbox required" value="<?php echo $sagepay_email;?>"></td>
		</tr>
        <tr>
            <td><?php echo ads_sagepay_vendor_name;?><span class="red"> * </span></td>
            <td><input size="40" name="sagepay_vendorname" class="inputbox required" value="<?php echo $sagepay_vendorname;?>"></td>
        </tr>
         <tr>
            <td><?php echo ads_sagepay_encryption_password; ?><span class="red"> * </span></td>
            <td><input size="40" name="sagepay_encryption_password" class="inputbox required" value="<?php echo $sagepay_encryption_password;?>"></td>
        </tr>
        <tr>
            <td><?php echo ads_sagepay_referrer_id; ?></td>
            <td><input size="40" name="referrer_id" class="inputbox" value="<?php //echo ;?>"></td>
        </tr>
         <tr>
            <td><?php echo ads_sagepay_confirmation_email; ?></td>
            <td><input size="40" name="confirmation_email" class="inputbox" value="<?php //echo ;?>"></td>
        </tr>
        <tr>
            <td><?php echo ads_sagepay_send_email; ?></td>
            <td><?php echo $send_email; ?></td>
        </tr>
        <tr>
            <td><?php echo ads_sagepay_mail_message;?></td>
            <td><textarea name="email_message" cols="40" rows="10" class="inputbox"></textarea></td>
        </tr>

        <tr>
            <td><?php echo ads_sagepay_enable_gift_aid; ?></td>
            <td><select name="enable_gift_aid" id="enable_gift_aid">
	            <option value="0" <?php echo (!$this->params->get('enable_gift_aid')) ? 'selected="selected"' : ''; ?>><?php echo JText::_('JNO'); ?></option>
	            <option value="1" <?php echo ($this->params->get('enable_gift_aid')) ? 'selected="selected"' : ''; ?>><?php echo JText::_('JYES'); ?></option>
	          </select>
            </td>
        </tr>

        <tr>
            <td><?php echo ads_sagepay_apply_avscv2; ?></td>
            <td><?php echo $apply_avscv2; ?></td>
        </tr>
        <tr>
            <td><?php echo ads_sagepay_apply_3dsecure; ?></td>
            <td><?php echo $apply_3dsecure; ?></td>
        </tr>

        <tr>
            <td class="paramlist key" ><?php echo ads_sagepay_mode;?>: </td>
            <td><?php echo $mode;?></td>
        </tr>

        <!-- Live Site Sage Pay URL -->
	    <tr>
            <td class="paramlist key" ><?php echo ads_sagepay_live_site;?>: </td>
            <td><input name="url_live" type="text" class="inputbox" size="80" value="https://live.sagepay.com/gateway/service/vspform-register.vsp" required="true" /></td>
        </tr>

        <!-- Test Site Sage Pay URL -->
        <tr>
            <td class="paramlist key" ><?php echo ads_sagepay_test_site;?>: </td>
            <td><input name="url_test" type="text" class="inputbox" size="80" value="https://test.sagepay.com/gateway/service/vspform-register.vsp" required="true" /></td>
        </tr>

        <!-- Simulator Site Sage Pay URL -->
        <tr>
            <td class="paramlist key" ><?php echo ads_sagepay_simulator_site;?>: </td>
            <td><input name="url_simulator" type="text" class="inputbox" size="80" value="https://test.sagepay.com/Simulator/VSPFormGateway.asp" required="true" /></td>
        </tr>

        </table>
        <input type="hidden" name="option" value="com_adsman"/>
        <input type="hidden" name="task" value="savepaymentconfig"/>
        <input type="hidden" name="paymenttype" value="<?php echo $this->classname;?>"/>
        </form>
        <?php

    }
    
    function save_admin_config()
    {
        $enabled	        = JRequest::getVar('enabled','');
        $email		        = JRequest::getVar('sagepay_email','');
        $vendorname         = JRequest::getVar('sagepay_vendorname','');
        $encryption_password    = JRequest::getVar('sagepay_encryption_password','');
        $referrer_id        = JRequest::getVar('referrer_id','');
        $confirmation_email = JRequest::getVar('confirmation_email','');
        $send_email         = JRequest::getVar('send_email',0);
        $email_message      = JRequest::getVar('email_message','');
        $enable_gift_aid    = JRequest::getVar('enable_gift_aid',0);
        $apply_avscv2       = JRequest::getVar('apply_avscv2',0);
        $apply_3dsecure     = JRequest::getVar('apply_3dsecure',0);
        $mode               = JRequest::getVar('mode',0);
        $url_live           = JRequest::getVar('url_live',"https://live.sagepay.com/gateway/service/vspform-register.vsp");
        $url_test           = JRequest::getVar('url_test',"https://test.sagepay.com/gateway/service/vspform-register.vsp");
        $url_simulator      = JRequest::getVar('url_simulator',"https://test.sagepay.com/Simulator/VSPFormGateway.asp");

        //$auto_accept = JRequest::getVar('auto_accept','');
        $paysystem   = "sagepay";

        $sagepay_params = new JRegistry();

        $sagepay_params->set('enabled',	$enabled);
        $sagepay_params->set('sagepay_email',	$email);
        $sagepay_params->set('sagepay_vendorname',	$vendorname);
        $sagepay_params->set('sagepay_encryption_password',	$encryption_password);
        $sagepay_params->set('referrer_id',	$referrer_id);
        $sagepay_params->set('confirmation_email',	$confirmation_email);
        $sagepay_params->set('send_email',	$send_email);
        $sagepay_params->set('email_message',	$email_message);
        $sagepay_params->set('enable_gift_aid',	$enable_gift_aid);
        $sagepay_params->set('apply_avscv2',	$apply_avscv2);
        $sagepay_params->set('apply_3dsecure',	$apply_3dsecure);
        $sagepay_params->set('mode',	$mode);
        $sagepay_params->set('url_live',	$url_live);
        $sagepay_params->set('url_test',	$url_test);
        $sagepay_params->set('url_simulator',	$url_simulator);

       // $text		 = "googlecheckout_email=$google_params->email\nuse_sandbox=$use_sandbox\nauto_accept=$auto_accept\ngooglecheckout_merchantID=$merchantID\ngooglecheckout_merchantKey=$merchantKey\n";

        JTable::addIncludePath(JPATH_SITE.DS.'administrator'.DS.'components'.DS.'com_adsman'.DS.'tables');
		$paySystem =& JTable::getInstance('PaySystems','Table');
        $paySystem->id=null;
        $paySystem->set('_tbl_key', "classname");
        $paySystem->load($this->classname);

        if(!$paySystem->id){
	        $this->_db->setQuery("INSERT INTO #__ads_paysystems set paysystem='$paysystem', params='$sagepay_params',enabled='$enabled',classname='$this->classname'");
	        $this->_db->query();
        } else {
	        $this->_db->setQuery("UPDATE #__ads_paysystems set params='$sagepay_params',enabled='$enabled' where classname='$this->classname'");
	        $this->_db->query();
        }

    }

    protected function getURLAction() {
        switch ($this->params->get('mode')) {
          case 0:
          default:
            return $this->params->get('url_live');

          case 1:
            return $this->params->get('url_test');

          case 2:
            return $this->params->get('url_simulator');
        }
    }

    function base64Encode($plain) {
      // Initialise output variable
      $output = "";

      // Do encoding
      $output = base64_encode($plain);

      // Return the result
      return $output;
    }

    function base64Decode($scrambled) {
      // Initialise output variable
      $output = "";

      // Fix plus to space conversion issue
      $scrambled = str_replace(" ","+",$scrambled);

      // Do encoding
      $output = base64_decode($scrambled);

      // Return the result
      return $output;
    }

    /*  The SimpleXor encryption algorithm */
    function simpleXor($InString, $Key) {
      // Initialise key array
      $KeyList = array();
      // Initialise out variable
      $output = "";

      // Convert $Key into array of ASCII values
      for($i = 0; $i < strlen($Key); $i++){
        $KeyList[$i] = ord(substr($Key, $i, 1));
      }

      // Step through string a character at a time
      for($i = 0; $i < strlen($InString); $i++) {
        // Get ASCII code from string, get ASCII code from key (loop through with MOD), XOR the two, get the character from the result
        // % is MOD (modulus), ^ is XOR
        $output.= chr(ord(substr($InString, $i, 1)) ^ ($KeyList[$i % strlen($Key)]));
      }

      // Return the result
      return $output;
    }

    // Filters unwanted characters out of an input string. Useful for tidying up FORM field inputs
    function cleanInput($strRawText,$strType)
    {

        if ($strType=="Number") {
        $strClean="0123456789.";
        $bolHighOrder=false;
        }
        else if ($strType=="VendorTxCode") {
        $strClean="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_.";
        $bolHighOrder=false;
        }
        else {
        $strClean=" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.,'/{}@():?-_&£$=%~<>*+\"";
        $bolHighOrder=true;
        }

        $strCleanedText="";
        $iCharPos = 0;

        do
        {
        // Only include valid characters
        $chrThisChar=substr($strRawText,$iCharPos,1);

        if (strspn($chrThisChar,$strClean,0,strlen($strClean))>0) {
        $strCleanedText=$strCleanedText . $chrThisChar;
        }
        else if ($bolHighOrder==true) {
        // Fix to allow accented characters and most high order bit chars which are harmless
        if (bin2hex($chrThisChar)>=191) {
        $strCleanedText=$strCleanedText . $chrThisChar;
        }
        }

        $iCharPos=$iCharPos+1;
        }
        while ($iCharPos<strlen($strRawText));

        $cleanInput = ltrim($strCleanedText);
        return $cleanInput;

    }

    protected function createCrypt($vars) {

        $log	= new JTheFactoryPaymentLog($this->_db);
        $log->set('_tbl_key', "invoice");

        // The Order Number (not order_id !)
        $invoice 	=  $vars['order_id'];
		// THIS IS FAR MORE IMPORTANT TO FORGET
        $log->load($invoice);

        $item_total = $vars['price'] * $vars['needed_credits']; // will be returned as Amount from sagepay

        $strPost = 'VendorTxCode=' . $log->invoice;
        $strPost .= '&ReferrerID='  . $this->params->get('referer_id');
        $strPost .= '&Amount='      . $item_total; //$log->amount;
        $strPost .= '&Currency='    . $log->currency;
        $strPost .= '&Description=' . $log->itemname;

        $strPost .= '&SuccessURL=' . JURI::root()."components/com_adsman/plugins/payment/pay_sagepay.notify.php";
        $strPost .= '&FailureURL=' . JURI::root()."index.php?option=com_adsman&task=payment&itemname=".$log->itemname."&paymenttype=".$this->classname."&act=return";

        $strPost .= '&CustomerName='  . $vars['BillingFirstnames'] . ' ' . $vars['BillingSurname'];
        $strPost .= '&CustomerEMail=' . JFactory::getUser($log->userid)->email;
        $strPost .= '&VendorEMail='   . $this->params->get('confirmation_email', '');
        $strPost .= '&SendEMail='     . $this->params->get('send_email', '');
        $strPost .= '&eMailMessage='  . $this->params->get('email_message', '');

        // Billing Details:
        $strPost .= '&BillingFirstnames=' . $vars['BillingFirstnames'];
        $strPost .= '&BillingSurname='    . $vars['BillingSurname'];
        $strPost .= '&BillingAddress1='   . $vars['BillingAddress1'];

        if (strlen($vars['BillingAddress2']) > 0)
        {
          $strPost .= '&BillingAddress2=' . $vars['BillingAddress2'];
        }

        $strPost .= '&BillingCity='     . $vars['BillingCity'];
        $strPost .= '&BillingPostCode=' . $vars['BillingPostCode'];
        $strPost .= '&BillingCountry='  . $vars['BillingCountry'];

        if (strlen($vars['BillingState']) > 0)
        {
          $strPost .= '&BillingState=' . $vars['BillingState'];
        }

        if (strlen($vars['BillingPhone']) > 0)
        {
          $strPost .= '&BillingPhone=' . $vars['BillingPhone'];
        }

        // Delivery Details:
        $strPost .= '&DeliveryFirstnames=' . $vars['DeliveryFirstnames'];
        $strPost .= '&DeliverySurname='    . $vars['DeliverySurname'];
        $strPost .= '&DeliveryAddress1='   . $vars['DeliveryAddress1'];

        if (strlen($vars['DeliveryAddress2']) > 0)
        {
          $strPost .= '&DeliveryAddress2=' . $vars['DeliveryAddress2'];
        }

        $strPost .= '&DeliveryCity='     . $vars['DeliveryCity'];
        $strPost .= '&DeliveryPostCode=' . $vars['DeliveryPostCode'];
        $strPost .= '&DeliveryCountry='  . $vars['DeliveryCountry'];

        if (strlen($vars['DeliveryState']) > 0)
        {
          $strPost .= '&DeliveryState=' . $vars['DeliveryState'];
        }

        if (strlen($vars['DeliveryPhone']) > 0)
        {
          $strPost .= '&DeliveryPhone=' . $vars['DeliveryPhone'];
        }

        // Basket
        $strPost .= '&Basket=' . '1:' . $log->itemname . ':'. $vars['needed_credits'] . ':' . $vars['price'] . ':0:' . $item_total . ':' . $item_total;
        //$strPost .= '&Basket=' . '1:' . $log->itemname . ':1:' . $vars['needed_credits'] . ':0:' . $vars['needed_credits'] . ':' . $vars['needed_credits'];
//$log->amount//$log->amount//
        $strPost .= '&AllowGiftAid='  . $this->params->get('enable_gift_aid', '');
        $strPost .= '&ApplyAVSCV2='   . $this->params->get('apply_avscv2', '');
        $strPost .= '&Apply3DSecure=' . $this->params->get('apply_3dsecure', '');

        $strCrypt = $this->base64Encode($this->SimpleXor($strPost, $this->params->get('sagepay_encryption_password', '')));

        return $strCrypt;
    }

     
    function show_sagepay_details_form($order_id,$item_description,$itemname,$quantity,$price,$currency,$return_url=null) {
        $vars = 'GET' == $_SERVER['REQUEST_METHOD'] ? @$_SESSION['sagepay_vars'] : array();
        $Itemid = JRequest::getInt('Itemid');

        $package_id 	= (int)JRequest::getVar("var_value1",null);

        ?>
        <script>

            <?php $this->writeJavascriptCountries(); ?>

            function getCountryOptionsListHtml(strSelectedValue)
            {
              var strCountryOptionsList = '';
              for (var i = 0; i < countries.length; i++)
              {
                strCountryOptionsList += '<option value="' + countries[i].code + '"'
                if (strSelectedValue == countries[i].code) {
                    strCountryOptionsList += " SELECTED"
                }
                strCountryOptionsList += ">" + countries[i].name + "</option>\n";
              }

              return strCountryOptionsList;
            }

            function validation()
              {
                name    = document.getElementById("BillingFirstnames").value;
                surname = document.getElementById("BillingSurname").value;
                address = document.getElementById("BillingAddress1").value;
                city    = document.getElementById("BillingCity").value;
                code    = document.getElementById("BillingPostCode").value;
                country = document.getElementById("BillingCountry").selectedIndex;

                error = '';

                if ('' == name)
                {
                  error += "<?php echo addslashes(JText::_('SAGEPAY_ENTER_FIRST_NAME')); ?>" + "\n";
                }

                if ('' == surname)
                {
                  error += "<?php echo addslashes(JText::_('SAGEPAY_ENTER_SURNAME')); ?>" + "\n";
                }

                if ('' == address)
                {
                  error += "<?php echo addslashes(JText::_('SAGEPAY_ENTER_ADDRESS')); ?>" + "\n";
                }

                if ('' == city)
                {
                  error += "<?php echo addslashes(JText::_('SAGEPAY_ENTER_CITY')); ?>" + "\n";
                }

                if ('' == code)
                {
                  error += "<?php echo addslashes(JText::_('SAGEPAY_ENTER_ZIP')); ?>" + "\n";
                }

                if (0 == country)
                {
                  error += "<?php echo addslashes(JText::_('SAGEPAY_SELECT_COUNTRY')); ?>" + "\n";
                }

                if ('' != error)
                {
                  alert(error);
                  return false;
                }

                return true;
              }
          </script>


        <form action="<?php echo JRoute::_('index.php'); ?>" method="POST" onsubmit="return validation();">

        <h1><?php echo JText::_('ADS_SAGEPAY_DETAILS'); ?></h1>
        <table class="payment_details">

          <!-- Billing details -->
          <tr>
            <th colspan="2"><?php echo JText::_('ADS_SAGEPAY_BILLING_DETAILS_FORM'); ?></th>
          </tr>

          <tr>
            <td class="td300"><label for="BillingFirstnames"><?php echo JText::_('ADS_SAGEPAY_FIRST_NAME_FORM'); ?><span class="red">*</span> :</label></td>
            <td class="td200"><input id="BillingFirstnames" name="BillingFirstnames" type="text" maxlength="20" value="<?php echo @$vars['BillingFirstnames']; ?>" /></td>
          </tr>

          <tr>
            <td class="td200"><label for="BillingSurname"><?php echo JText::_('ADS_SAGEPAY_SURNAME_FORM'); ?><span class="red">*</span> :</label></td>
            <td class="td200"><input id="BillingSurname" name="BillingSurname" type="text" maxlength="20" value="<?php echo @$vars['BillingSurname']; ?>" /></td>
          </tr>

          <tr>
            <td class="td200"><label for="BillingAddress1"><?php echo JText::_('ADS_SAGEPAY_ADDRESS_LINE_1_FORM'); ?><span class="red">*</span> :</label></td>
            <td class="td200"><input id="BillingAddress1" name="BillingAddress1" type="text" maxlength="100" value="<?php echo @$vars['BillingAddress1']; ?>" /></td>
          </tr>

          <tr>
            <td class="td200"><label for="BillingAddress2"><?php echo JText::_('ADS_SAGEPAY_ADDRESS_LINE_2_FORM'); ?>:</label></td>
            <td class="td200"><input id="BillingAddress2" name="BillingAddress2" type="text" maxlength="100" value="<?php echo @$vars['BillingAddress2']; ?>" /></td>
          </tr>

          <tr>
            <td class="td200"><label for="BillingCity"><?php echo JText::_('ADS_SAGEPAY_CITY_FORM'); ?><span class="red">*</span> :</label></td>
            <td class="td200"><input id="BillingCity" name="BillingCity" type="text" maxlength="40" value="<?php echo @$vars['BillingCity']; ?>" /></td>
          </tr>

          <tr>
            <td class="td300"><label for="BillingPostCode"><?php echo JText::_('ADS_SAGEPAY_ZIP_FORM'); ?><span class="red">*</span> :</label></td>
            <td class="td200"><input id="BillingPostCode" name="BillingPostCode" type="text" maxlength="10" value="<?php echo @$vars['BillingPostCode']; ?>" /></td>
          </tr>

          <tr>
            <td class="td300"><label for="BillingCountry"><?php echo JText::_('ADS_SAGEPAY_COUNTRY_FORM'); ?><span class="red">*</span> :</label></td>
            <td class="td200">
              <select id="BillingCountry" name="BillingCountry" style="width: 200px;" >
                <option value=""><?php echo JText::_('ADS_SAGEPAY_SELECT_FORM'); ?></option>
                <script type="text/javascript" language="javascript">
                          document.write(getCountryOptionsListHtml("<?php echo @$vars['BillingCountry']; ?>"));
                        </script>
              </select>
            </td>
          </tr>

          <tr>
            <td class="td300"><label for="BillingState"><?php echo JText::_('ADS_SAGEPAY_STATE_FORM'); ?>:</label></td>
            <td class="td200"><input id="BillingState" name="BillingState" type="text" maxlength="2" value="<?php echo @$vars['BillingState']; ?>" /></td>
          </tr>

          <tr>
            <td class="td300"><label for="BillingPhone"><?php echo JText::_('ADS_SAGEPAY_PHONE_FORM'); ?>:</label></td>
            <td class="td200"><input id="BillingPhone" name="BillingPhone" type="text" maxlength="20" value="<?php echo @$vars['BillingPhone']; ?>" /></td>
          </tr>
        </table>

        <input type="submit" value="<?php echo JText::_('ADS_SAGEPAY_PAYMENT_FORM'); ?>" />
        <input type="hidden" name="option"      value="com_adsman" />
        <input type="hidden" name="task"        value="process" />
        <input type="hidden" name="step"        value="2" />
        <input type="hidden" name="order_id"    value="<?php echo $order_id; ?>" />
        <input type="hidden" name="item_description"    value="<?php echo $item_description; ?>" />
        <input type="hidden" name="itemname"    value="<?php echo $itemname; ?>" />
        <input type="hidden" name="quantity"    value="<?php echo $quantity; ?>" />
        <input type="hidden" name="price"       value="<?php echo $price; ?>" />
        <input type="hidden" name="currency"    value="<?php echo $currency; ?>" />
        <input type="hidden" name="package_id"  value="<?php echo $package_id; ?>" />
        <input type="hidden" name="Itemid"      value="<?php echo $Itemid; ?>" />
    </form>
<?php
    }

    function show_payment_form($order_id,$item_description,$itemname,$quantity,$price,$currency,$return_url=null)
    {
        $my 	= JFactory::getUser();
        $session	= JFactory::getSession();
        $session->set('page_session',1);
        setcookie('page_cookie', 1);

        $session_vars       = $session->get('posted_pm');
        //$sagepay_details    = $session->get('sagepay_details');
        $post_itemname = $session_vars['itemname'];

        $packages_purchase_pm = $session->get('packages_purchase_pm');

        //$object_id 		= JRequest::getVar("id",null);
        //$var_name1 		= JRequest::getVar("var_name1",'package_id');
        $package_id 	= (int)JRequest::getVar("package_id",null);

        $itemamount_package = $quantity;
        $itemamount_package_userprofile = JRequest::getVar('itemamount_package',1);

        $total_price_ad = ($itemname == 'packages') ? $packages_purchase_pm['total_price_ad'] : $session_vars['total_price_ad'];//(int)JRequest::getVar("var_value2",null);

        if ($total_price_ad == 0)
            $total_price_ad = $session_vars['total_price_ad'];

        $featured_ad 	= JRequest::getVar("var_value3",null);
        if ($featured_ad == null)
            $featured_ad = $session_vars['featured'];

        $credits = AdsUtilities::getUserCredits($my->id);
        if (!$return_url)
            $return_url	= JURI::root()."index.php?option=com_adsman&task=payment&itemname=".$itemname."&paymenttype=".$this->classname."&act=return";

            $session->set('posted_pm',null);

        if ($itemname == 'packages') {
            $needed_credits = $itemamount_package;

            $value1 = $package_id; //$packages_purchase_pm['package_id'];
            $value2 = $packages_purchase_pm['total_price_ad'];
            $value3 = $packages_purchase_pm['featured_ad'];

        } else {
            $needed_credits = $total_price_ad - $credits;
            echo JText::_("Ad costs")." ".$total_price_ad.' '.JText::_("ADS_CREDITS").' ('.JText::_("You have")." ".$credits.' '.JText::_("ADS_CREDITS").').';
        }

        // Get the values
        $vars = array();
        $IsDeliverySame = 'YES';

        $vars['BillingFirstnames'] = $this->cleanInput(JRequest::getVar('BillingFirstnames', '', 'POST', 'string'), 'Text');
        $vars['BillingSurname']    = $this->cleaninput(JRequest::getVar('BillingSurname',    '', 'POST', 'string'), 'Text');
        $vars['BillingAddress1']   = $this->cleaninput(JRequest::getVar('BillingAddress1',   '', 'POST', 'string'), 'Text');
        $vars['BillingAddress2']   = $this->cleaninput(JRequest::getVar('BillingAddress2',   '', 'POST', 'string'), 'Text');
        $vars['BillingCity']       = $this->cleaninput(JRequest::getVar('BillingCity',       '', 'POST', 'string'), 'Text');
        $vars['BillingPostCode']   = $this->cleaninput(JRequest::getVar('BillingPostCode',   '', 'POST', 'string'), 'Text');
        $vars['BillingCountry']    = $this->cleaninput(JRequest::getVar('BillingCountry',    '', 'POST', 'string'), 'Text');
        $vars['BillingState']      = $this->cleaninput(JRequest::getVar('BillingState',      '', 'POST', 'string'), 'Text');
        $vars['BillingPhone']      = $this->cleaninput(JRequest::getVar('BillingPhone',      '', 'POST', 'string'), 'Text');

        $vars['DeliveryFirstnames'] = $vars['BillingFirstnames'];
        $vars['DeliverySurname']    = $vars['BillingSurname'];
        $vars['DeliveryAddress1']   = $vars['BillingAddress1'];
        $vars['DeliveryAddress2']   = $vars['BillingAddress2'];
        $vars['DeliveryCity']       = $vars['BillingCity'];
        $vars['DeliveryPostCode']   = $vars['BillingPostCode'];
        $vars['DeliveryCountry']    = $vars['BillingCountry'];
        $vars['DeliveryState']      = $vars['BillingState'];
        $vars['DeliveryPhone']      = $vars['BillingPhone'];

        $vars['order_id']   = $order_id; //JRequest::getVar('order_id', '', 'POST', 'string')
        $vars['needed_credits']   = $needed_credits; //JRequest::getVar('order_id', '', 'POST', 'string')
        $vars['price'] = $price;
        $vars['package_id'] = JRequest::getVar('package_id', 0, 'POST', 'integer');

        // Check for errors
        $error = $this->validateSagePay($vars);

        // If errors were found, show the form again
        if ($error != '')
        {
          // Store the values in session, so we can fill in the fields
          $_SESSION['sagepay_vars'] = $vars;

          $this->setError($error);
          return false;
        }

        $vendorname         = $this->params->get('sagepay_vendorname','');
        $encryption_password = $this->params->get('sagepay_encryption_password','');

        $session->set('packages_purchase_pm',null);

        // Create the crypt
        $crypt = $this->createCrypt($vars);

        if (file_exists(ADS_COMPONENT_PATH."/plugins/payment/buynow_sagepay.jpg"))
                $buy_button = JURI::root()."components/com_adsman/plugins/payment/buynow_sagepay.jpg";

        ?>
        <script>
          <?php $this->writeJavascriptCountries(); ?>
          function getCountryName(strCountryCode)
          {
            for (var i = 0; i < countries.length; i++)
            {
              if (strCountryCode == countries[i].code)
              {
                return countries[i].name;
              }
            }

            return "";
          }
        </script>


            <div><strong><?php echo $item_description?></strong>&nbsp;-&nbsp;<?php echo number_format($price*$needed_credits,2,".","")," ",$currency;?></div>
            <br />

            <table class="payment_details">
              <!-- Billing details -->
              <tr>
                <th colspan="2" class="td200"><?php echo JText::_('ADS_SAGEPAY_BILLING_DETAILS'); ?></th>
              </tr>

              <tr>
                <td class="td300"><?php echo JText::_('ADS_SAGEPAY_FIRST_NAME'); ?>:</td>
                <td class="td200"><?php echo $vars['BillingFirstnames']; ?></td>
              </tr>

              <tr>
                <td class="td300"><?php echo JText::_('ADS_SAGEPAY_SURNAME'); ?></td>
                <td class="td200"><?php echo $vars['BillingSurname']; ?></td>
              </tr>

              <tr>
                <td class="td300"><?php echo JText::_('ADS_SAGEPAY_ADDRESS_LINE_1'); ?>:</td>
                <td class="td200"><?php echo $vars['BillingAddress1']; ?></td>
              </tr>

              <tr>
                <td class="td300"><?php echo JText::_('ADS_SAGEPAY_ADDRESS_LINE_2'); ?>:</td>
                <td class="td200"><?php echo $vars['BillingAddress2']; ?></td>
              </tr>

              <tr>
                <td class="td300"><?php echo JText::_('ADS_SAGEPAY_CITY'); ?>:</td>
                <td class="td200"><?php echo $vars['BillingCity']; ?></td>
              </tr>

              <tr>
                <td class="td300"><?php echo JText::_('ADS_SAGEPAY_ZIP'); ?>:</td>
                <td class="td200"><?php echo $vars['BillingPostCode']; ?></td>
              </tr>

              <tr>
                <td class="td300"><?php echo JText::_('ADS_SAGEPAY_COUNTRY'); ?>:</td>
                <td class="td200"><script type="text/javascript" language="javascript">
                          document.write(getCountryName("<?php echo $vars['BillingCountry']; ?>"));
                    </script>
                </td>
              </tr>

              <tr>
                <td class="td300"><?php echo JText::_('ADS_SAGEPAY_STATE'); ?>:</td>
                <td class="td200"><?php echo $vars['BillingState']; ?></td>
              </tr>

              <tr>
                <td class="td300"><?php echo JText::_('ADS_SAGEPAY_PHONE'); ?>:</td>
                <td class="td200"><?php echo $vars['BillingPhone']; ?></td>
              </tr>
            </table>

          <form action="<?php echo $this->getURLAction(); ?>" method="POST" id="SagePayForm" name="SagePayForm">
          <input type="hidden" name="navigate" value="" />
            <input type="hidden" name="VPSProtocol" value="2.23">
            <input type="hidden" name="TxType" value="PAYMENT">
            <input type="hidden" name="Vendor" value="<?php echo $vendorname; ?>">
            <input type="hidden" name="Crypt" value="<?php echo $crypt; ?>">
            <input type="hidden" name="continue_url" value="<?php echo JURI::root(); ?>components/com_adsman/plugins/payment/pay_sagepay.notify.php">
            <input type="image" name="submit" src="<?php echo $buy_button; ?>" alt="<?php echo ads_sagepay_buynow;?>" style="margin-left: 130px;">
            <!--<input type="submit" value="<?php //echo JText::_('FACTORY_PAYMENT_PLUGIN_SAGEPAY_SUBMIT'); ?>" />-->
          </form>

          <div><?php echo ads_sagepay_disclaimer;?></div>
          <div></div>
          <br />
      
    <?php
    }

   protected function validateSagePay($sagepay_vars) {
    $bIsDeliverySame =& JRequest::getVar('IsDeliverySame', '', 'POST', 'string');
    $bIsDeliverySame =  ($bIsDeliverySame == 'YES') ? true : false;

    // Validate the compulsory fields
    $error = '';
    if (strlen($sagepay_vars['BillingFirstnames'])==0)
      $error=JText::_('ADS_SAGEPAY_FIRST_NAME_FORM');
    else if (strlen($sagepay_vars['BillingSurname'])==0)
      $error=JText::_('ADS_SAGEPAY_SURNAME_FORM');
    else if (strlen($sagepay_vars['BillingAddress1'])==0)
      $error=JText::_('ADS_SAGEPAY_ADDRESS_LINE_1_FORM');
    else if (strlen($sagepay_vars['BillingCity'])==0)
      $error=JText::_('ADS_SAGEPAY_CITY_FORM');
    else if (strlen($sagepay_vars['BillingPostCode'])==0)
      $error=JText::_('ADS_SAGEPAY_ZIP_FORM');
    else if (strlen($sagepay_vars['BillingCountry'])==0)
      $error=JText::_('ADS_SAGEPAY_SELECT_FORM');
    else if ((strlen($sagepay_vars['BillingState'])==0) and ($sagepay_vars['BillingCountry'] == "US"))
      $error="Please enter your State code as you have selected United States for billing country.";
    else if (($bIsDeliverySame==false) and strlen($sagepay_vars['DeliveryFirstnames'])==0)
      $error="Please enter your Delivery First Names(s) where requested below.";
    else if (($bIsDeliverySame==false) and strlen($sagepay_vars['DeliverySurname'])==0)
      $error="Please enter your Delivery Surname where requested below.";
    else if (($bIsDeliverySame==false) and strlen($sagepay_vars['DeliveryAddress1'])==0)
      $error="Please enter your Delivery Address Line 1 where requested below.";
    else if (($bIsDeliverySame==false) and strlen($sagepay_vars['DeliveryCity'])==0)
      $error="Please enter your Delivery City where requested below.";
    else if (($bIsDeliverySame==false) and strlen($sagepay_vars['DeliveryPostCode'])==0)
      $error="Please enter your Delivery Post Code where requested below.";
    else if (($bIsDeliverySame==false) and strlen($sagepay_vars['DeliveryCountry'])==0)
      $error="Please select your Delivery Country where requested below.";
    else if (($bIsDeliverySame==false) and (strlen($sagepay_vars['DeliveryState'])==0) and ($sagepay_vars['DeliveryCountry'] == "US"))
      $error="Please enter your State code as you have selected United States for delivery country.";

    $error = JText::_($error);

    return $error;
  }

   protected function writeJavascriptCountries() {
    ?>
    // ISO 3166-1 country names and codes from http://opencountrycodes.appspot.com/javascript
countries = [
    {code: "GB", name: "United Kingdom"},
    {code: "AF", name: "Afghanistan"},
    {code: "AX", name: "Aland Islands"},
    {code: "AL", name: "Albania"},
    {code: "DZ", name: "Algeria"},
    {code: "AS", name: "American Samoa"},
    {code: "AD", name: "Andorra"},
    {code: "AO", name: "Angola"},
    {code: "AI", name: "Anguilla"},
    {code: "AQ", name: "Antarctica"},
    {code: "AG", name: "Antigua and Barbuda"},
    {code: "AR", name: "Argentina"},
    {code: "AM", name: "Armenia"},
    {code: "AW", name: "Aruba"},
    {code: "AU", name: "Australia"},
    {code: "AT", name: "Austria"},
    {code: "AZ", name: "Azerbaijan"},
    {code: "BS", name: "Bahamas"},
    {code: "BH", name: "Bahrain"},
    {code: "BD", name: "Bangladesh"},
    {code: "BB", name: "Barbados"},
    {code: "BY", name: "Belarus"},
    {code: "BE", name: "Belgium"},
    {code: "BZ", name: "Belize"},
    {code: "BJ", name: "Benin"},
    {code: "BM", name: "Bermuda"},
    {code: "BT", name: "Bhutan"},
    {code: "BO", name: "Bolivia"},
    {code: "BA", name: "Bosnia and Herzegovina"},
    {code: "BW", name: "Botswana"},
    {code: "BV", name: "Bouvet Island"},
    {code: "BR", name: "Brazil"},
    {code: "IO", name: "British Indian Ocean Territory"},
    {code: "BN", name: "Brunei Darussalam"},
    {code: "BG", name: "Bulgaria"},
    {code: "BF", name: "Burkina Faso"},
    {code: "BI", name: "Burundi"},
    {code: "KH", name: "Cambodia"},
    {code: "CM", name: "Cameroon"},
    {code: "CA", name: "Canada"},
    {code: "CV", name: "Cape Verde"},
    {code: "KY", name: "Cayman Islands"},
    {code: "CF", name: "Central African Republic"},
    {code: "TD", name: "Chad"},
    {code: "CL", name: "Chile"},
    {code: "CN", name: "China"},
    {code: "CX", name: "Christmas Island"},
    {code: "CC", name: "Cocos (Keeling) Islands"},
    {code: "CO", name: "Colombia"},
    {code: "KM", name: "Comoros"},
    {code: "CG", name: "Congo"},
    {code: "CD", name: "Congo, The Democratic Republic of the"},
    {code: "CK", name: "Cook Islands"},
    {code: "CR", name: "Costa Rica"},
    {code: "CI", name: "Côte d'Ivoire"},
    {code: "HR", name: "Croatia"},
    {code: "CU", name: "Cuba"},
    {code: "CY", name: "Cyprus"},
    {code: "CZ", name: "Czech Republic"},
    {code: "DK", name: "Denmark"},
    {code: "DJ", name: "Djibouti"},
    {code: "DM", name: "Dominica"},
    {code: "DO", name: "Dominican Republic"},
    {code: "EC", name: "Ecuador"},
    {code: "EG", name: "Egypt"},
    {code: "SV", name: "El Salvador"},
    {code: "GQ", name: "Equatorial Guinea"},
    {code: "ER", name: "Eritrea"},
    {code: "EE", name: "Estonia"},
    {code: "ET", name: "Ethiopia"},
    {code: "FK", name: "Falkland Islands (Malvinas)"},
    {code: "FO", name: "Faroe Islands"},
    {code: "FJ", name: "Fiji"},
    {code: "FI", name: "Finland"},
    {code: "FR", name: "France"},
    {code: "GF", name: "French Guiana"},
    {code: "PF", name: "French Polynesia"},
    {code: "TF", name: "French Southern Territories"},
    {code: "GA", name: "Gabon"},
    {code: "GM", name: "Gambia"},
    {code: "GE", name: "Georgia"},
    {code: "DE", name: "Germany"},
    {code: "GH", name: "Ghana"},
    {code: "GI", name: "Gibraltar"},
    {code: "GR", name: "Greece"},
    {code: "GL", name: "Greenland"},
    {code: "GD", name: "Grenada"},
    {code: "GP", name: "Guadeloupe"},
    {code: "GU", name: "Guam"},
    {code: "GT", name: "Guatemala"},
    {code: "GG", name: "Guernsey"},
    {code: "GN", name: "Guinea"},
    {code: "GW", name: "Guinea-Bissau"},
    {code: "GY", name: "Guyana"},
    {code: "HT", name: "Haiti"},
    {code: "HM", name: "Heard Island and McDonald Islands"},
    {code: "VA", name: "Holy See (Vatican City State)"},
    {code: "HN", name: "Honduras"},
    {code: "HK", name: "Hong Kong"},
    {code: "HU", name: "Hungary"},
    {code: "IS", name: "Iceland"},
    {code: "IN", name: "India"},
    {code: "ID", name: "Indonesia"},
    {code: "IR", name: "Iran, Islamic Republic of"},
    {code: "IQ", name: "Iraq"},
    {code: "IE", name: "Ireland"},
    {code: "IM", name: "Isle of Man"},
    {code: "IL", name: "Israel"},
    {code: "IT", name: "Italy"},
    {code: "JM", name: "Jamaica"},
    {code: "JP", name: "Japan"},
    {code: "JE", name: "Jersey"},
    {code: "JO", name: "Jordan"},
    {code: "KZ", name: "Kazakhstan"},
    {code: "KE", name: "Kenya"},
    {code: "KI", name: "Kiribati"},
    {code: "KP", name: "Korea, Democratic People's Republic of"},
    {code: "KR", name: "Korea, Republic of"},
    {code: "KW", name: "Kuwait"},
    {code: "KG", name: "Kyrgyzstan"},
    {code: "LA", name: "Lao People's Democratic Republic"},
    {code: "LV", name: "Latvia"},
    {code: "LB", name: "Lebanon"},
    {code: "LS", name: "Lesotho"},
    {code: "LR", name: "Liberia"},
    {code: "LY", name: "Libyan Arab Jamahiriya"},
    {code: "LI", name: "Liechtenstein"},
    {code: "LT", name: "Lithuania"},
    {code: "LU", name: "Luxembourg"},
    {code: "MO", name: "Macao"},
    {code: "MK", name: "Macedonia, The Former Yugoslav Republic of"},
    {code: "MG", name: "Madagascar"},
    {code: "MW", name: "Malawi"},
    {code: "MY", name: "Malaysia"},
    {code: "MV", name: "Maldives"},
    {code: "ML", name: "Mali"},
    {code: "MT", name: "Malta"},
    {code: "MH", name: "Marshall Islands"},
    {code: "MQ", name: "Martinique"},
    {code: "MR", name: "Mauritania"},
    {code: "MU", name: "Mauritius"},
    {code: "YT", name: "Mayotte"},
    {code: "MX", name: "Mexico"},
    {code: "FM", name: "Micronesia, Federated States of"},
    {code: "MD", name: "Moldova"},
    {code: "MC", name: "Monaco"},
    {code: "MN", name: "Mongolia"},
    {code: "ME", name: "Montenegro"},
    {code: "MS", name: "Montserrat"},
    {code: "MA", name: "Morocco"},
    {code: "MZ", name: "Mozambique"},
    {code: "MM", name: "Myanmar"},
    {code: "NA", name: "Namibia"},
    {code: "NR", name: "Nauru"},
    {code: "NP", name: "Nepal"},
    {code: "NL", name: "Netherlands"},
    {code: "AN", name: "Netherlands Antilles"},
    {code: "NC", name: "New Caledonia"},
    {code: "NZ", name: "New Zealand"},
    {code: "NI", name: "Nicaragua"},
    {code: "NE", name: "Niger"},
    {code: "NG", name: "Nigeria"},
    {code: "NU", name: "Niue"},
    {code: "NF", name: "Norfolk Island"},
    {code: "MP", name: "Northern Mariana Islands"},
    {code: "NO", name: "Norway"},
    {code: "OM", name: "Oman"},
    {code: "PK", name: "Pakistan"},
    {code: "PW", name: "Palau"},
    {code: "PS", name: "Palestinian Territory, Occupied"},
    {code: "PA", name: "Panama"},
    {code: "PG", name: "Papua New Guinea"},
    {code: "PY", name: "Paraguay"},
    {code: "PE", name: "Peru"},
    {code: "PH", name: "Philippines"},
    {code: "PN", name: "Pitcairn"},
    {code: "PL", name: "Poland"},
    {code: "PT", name: "Portugal"},
    {code: "PR", name: "Puerto Rico"},
    {code: "QA", name: "Qatar"},
    {code: "RE", name: "Réunion"},
    {code: "RO", name: "Romania"},
    {code: "RU", name: "Russian Federation"},
    {code: "RW", name: "Rwanda"},
    {code: "BL", name: "Saint Barthélemy"},
    {code: "SH", name: "Saint Helena"},
    {code: "KN", name: "Saint Kitts and Nevis"},
    {code: "LC", name: "Saint Lucia"},
    {code: "MF", name: "Saint Martin"},
    {code: "PM", name: "Saint Pierre and Miquelon"},
    {code: "VC", name: "Saint Vincent and the Grenadines"},
    {code: "WS", name: "Samoa"},
    {code: "SM", name: "San Marino"},
    {code: "ST", name: "Sao Tome and Principe"},
    {code: "SA", name: "Saudi Arabia"},
    {code: "SN", name: "Senegal"},
    {code: "RS", name: "Serbia"},
    {code: "SC", name: "Seychelles"},
    {code: "SL", name: "Sierra Leone"},
    {code: "SG", name: "Singapore"},
    {code: "SK", name: "Slovakia"},
    {code: "SI", name: "Slovenia"},
    {code: "SB", name: "Solomon Islands"},
    {code: "SO", name: "Somalia"},
    {code: "ZA", name: "South Africa"},
    {code: "GS", name: "South Georgia and the South Sandwich Islands"},
    {code: "ES", name: "Spain"},
    {code: "LK", name: "Sri Lanka"},
    {code: "SD", name: "Sudan"},
    {code: "SR", name: "Suriname"},
    {code: "SJ", name: "Svalbard and Jan Mayen"},
    {code: "SZ", name: "Swaziland"},
    {code: "SE", name: "Sweden"},
    {code: "CH", name: "Switzerland"},
    {code: "SY", name: "Syrian Arab Republic"},
    {code: "TW", name: "Taiwan, Province of China"},
    {code: "TJ", name: "Tajikistan"},
    {code: "TZ", name: "Tanzania, United Republic of"},
    {code: "TH", name: "Thailand"},
    {code: "TL", name: "Timor-Leste"},
    {code: "TG", name: "Togo"},
    {code: "TK", name: "Tokelau"},
    {code: "TO", name: "Tonga"},
    {code: "TT", name: "Trinidad and Tobago"},
    {code: "TN", name: "Tunisia"},
    {code: "TR", name: "Turkey"},
    {code: "TM", name: "Turkmenistan"},
    {code: "TC", name: "Turks and Caicos Islands"},
    {code: "TV", name: "Tuvalu"},
    {code: "UG", name: "Uganda"},
    {code: "UA", name: "Ukraine"},
    {code: "AE", name: "United Arab Emirates"},
    {code: "GB", name: "United Kingdom"},
    {code: "US", name: "United States"},
    {code: "UM", name: "United States Minor Outlying Islands"},
    {code: "UY", name: "Uruguay"},
    {code: "UZ", name: "Uzbekistan"},
    {code: "VU", name: "Vanuatu"},
    {code: "VE", name: "Venezuela"},
    {code: "VN", name: "Viet Nam"},
    {code: "VG", name: "Virgin Islands, British"},
    {code: "VI", name: "Virgin Islands, U.S."},
    {code: "WF", name: "Wallis and Futuna"},
    {code: "EH", name: "Western Sahara"},
    {code: "YE", name: "Yemen"},
    {code: "ZM", name: "Zambia"},
    {code: "ZW", name: "Zimbabwe"}
];
    <?php
  }

}
?>