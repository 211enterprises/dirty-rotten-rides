<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

	define( '_JEXEC', 1 );

/*** access Joomla's configuration file ***/
	define( 'DS', DIRECTORY_SEPARATOR );


    $my_path = dirname(__FILE__).DS."..".DS."..".DS."..".DS."..";
    if( file_exists($my_path."/../../../../configuration.php")) {
        require_once($my_path."/../../../../configuration.php");
		define( 'JPATH_BASE', dirname( $my_path."/../../../../configuration.php") );
    }elseif( file_exists($my_path."/../../../configuration.php")) {
        require_once($my_path."/../../../configuration.php");
		define( 'JPATH_BASE', dirname( $my_path."/../../../configuration.php") );
    }
    elseif( file_exists($my_path."/../../configuration.php")){
        require_once($my_path."/../../configuration.php");
		define( 'JPATH_BASE', dirname( $my_path."/../../configuration.php") );
    }
    elseif( file_exists($my_path."/configuration.php")){
		require_once( $my_path."/configuration.php" );
		define( 'JPATH_BASE', dirname( $my_path."/configuration.php") );
    }
    else
        die( "Joomla Configuration File not found!" );

	// Load the framework
	require_once ( $my_path . DS . 'includes' . DS . 'defines.php' );
	require_once ( $my_path . DS . 'includes' . DS . 'framework.php' );

	// create the mainframe object
	$mainframe = &JFactory::getApplication( 'site' );

	// Initialize the framework
	$mainframe->initialise();

	//create the database object
	$database = &JFactory::getDBO();

    JPluginHelper::importPlugin('system');
	require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_adsman'.DS.'thefactory'.DS.'admin.application.php');
	
	$Tapp	= &JTheFactoryApp::getInstance();

?>
