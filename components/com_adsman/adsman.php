<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

// no direct access
defined('_JEXEC') or die('Restricted access');

require_once(  JPATH_SITE . DS . 'components' . DS . 'com_adsman'.DS.'helpers'.DS.'helper.php' );
require_once(  JPATH_SITE . DS . 'components' . DS . 'com_adsman'.DS.'helpers'.DS.'datetime.php' );
require_once( JPATH_COMPONENT.DS.'helpers'.DS.'route.php' );
require_once (JPATH_COMPONENT.DS.'config.php');

Ads_Debug::resetLog();
Ads_Debug::showTicker("Start");

/*@var $app JTheFactoryApp*/
$app = JFactory::getApplication();

JPluginHelper::importPlugin('system');
//error_reporting(E_ALL);

require_once (JPATH_SITE . DS . 'components' . DS . 'com_adsman'.DS.'thefactory'.DS.'application.php');
$JTheFactoryAppClass = new JTheFactoryApp();
$Tapp = $JTheFactoryAppClass->getInstance(null,true);
//$Tapp = &JTheFactoryApp::getInstance(null,true);

require_once (JPATH_SITE . DS . 'components' . DS . 'com_adsman'.DS.'controller.php');
require_once (JPATH_SITE . DS . 'components' . DS . 'com_adsman'.DS.'options.php');
require_once (JPATH_ADMINISTRATOR.DS.'components'.DS.'com_adsman'.DS.'tables'.DS.'addsman.php');
require_once (JPATH_ADMINISTRATOR.DS.'components'.DS.'com_adsman'.DS.'tables'.DS.'adsuser.php');

JHtml::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR . DS . 'elements');

global $cb_fieldmap;
$Itemid = JRequest::getVar('Itemid', 0, 'REQUEST', 'integer');

$id = intval( JRequest::getVar('id', 0 ) );
$db = JFactory::getDbo();

$view	= JRequest::getCmd( 'view' );
$task	= JRequest::getCmd( 'task' );

if(!$task) $task=JRequest::getCmd( 'layout' );

if(!$task)
{
	switch($view){
		case "adsman":
			$task = "listadds";
		break;
		case "user":
			$task = "myuserprofile";
		break;
	}
}

$AdsUtilitiesClass = new AdsUtilities();
$AdsUtilitiesClass->loadLanguage();

if(!$AdsUtilitiesClass->adsAuthorize($task)){
	$app->redirect(JURI::root()."index.php?option=com_adsman&task=listadds");
	exit;
}

if(!$task) $task = 'listadds';

if ($Tapp->checktask($task)) return;

$controller = new AdsController();

// Register Extra tasks
$controller->registerTask( 'new' , 'edit' );
$controller->registerTask( 'republish' , 'edit' );
$controller->registerTask( 'showsearch' , 'show_search' );


// Perform the Request task
$controller->execute( $task );

// Redirect if set by the controller
$controller->redirect();

Ads_Debug::showTicker("Exit");
Ads_Debug::getSQL("Queries",1,0);
