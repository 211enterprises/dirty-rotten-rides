<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

class JTheFactoryUpgrade
{
    function upgrade($oldversion)
    {
        // <= 3.2.1
        $j17version = '3.3.0';
        //$j17version = '3.2.1';
        $x = strcmp($oldversion,$j17version);

        //if ($oldversion  != '3.2.1') {
        if ($x <= 0 ) {

            $db = JFactory::getDBO();
            $struct = $db->getTableFields('#__ads');

            if (!array_key_exists('ad_city',$struct['#__ads'])&& !array_key_exists('`ad_city`',$struct['#__ads']) )
            {
                $db->setQuery("ALTER TABLE `#__ads` ADD `ad_city` varchar(200) AFTER `MapY`;");
                $db->query();
            }

            if (!array_key_exists('ad_postcode',$struct['#__ads'])&& !array_key_exists('`ad_postcode`',$struct['#__ads']) )
            {
                $db->setQuery("ALTER TABLE `#__ads` ADD `ad_postcode` varchar(10) AFTER `ad_city`;");
                $db->query();
            }

            //ALTER TABLE `#__ads_pictures`
            $struct_pictures = $db->getTableFields('#__ads_pictures');
            if (!array_key_exists('published',$struct_pictures['#__ads_pictures'])&& !array_key_exists('`published`',$struct_pictures['#__ads_pictures']) )
            {
                $db->setQuery("ALTER TABLE `#__ads_pictures` ADD `published` tinyint(1) AFTER `modified`;");
                $db->query();
            }



        }


        $j25version = '3.3.6';
        $x25 = strcmp($oldversion,$j25version);
        if ($x25 <= 0 ) {

            $db = JFactory::getDBO();

            //ALTER TABLE `#__ads_report`
            $struct_reports = $db->getTableFields('#__ads_report');
            if (!array_key_exists('status',$struct_reports['#__ads_report'])&& !array_key_exists('`status`',$struct_reports['#__ads_report']) )
            {
                $db->setQuery("ALTER TABLE `#__ads_report` ADD `status` varchar(150) NOT NULL default 'unprocessed' AFTER `message`;");
                $db->query();
            }

            if (array_key_exists('processing',$struct_reports['#__ads_report'])|| array_key_exists('`processing`',$struct_reports['#__ads_report']) )
            {
                $db->setQuery("ALTER TABLE `#__ads_report` DROP COLUMN `processing` ;");
                $db->query();
            }

            if (array_key_exists('solved',$struct_reports['#__ads_report']) || array_key_exists('`solved`',$struct_reports['#__ads_report']) )
            {
                $db->setQuery("ALTER TABLE `#__ads_report` DROP COLUMN `solved` ;");
                $db->query();
            }

            //ALTER TABLE `#__ads_paylog`
            $struct_paylog = $db->getTableFields('#__ads_paylog');
            if (!array_key_exists('params',$struct_paylog['#__ads_paylog'])&& !array_key_exists('`params`',$struct_paylog['#__ads_paylog']) )
            {
                $db->setQuery("ALTER TABLE `#__ads_paylog` ADD `params` mediumtext DEFAULT NULL AFTER `var_value4`;");
                $db->query();
            }

        }



    }
}

?>