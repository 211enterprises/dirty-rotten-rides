<?php
/**------------------------------------------------------------------------
com_jobsfactory - Reverse Auction Factory 3.0.0
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
 * @build: 01/04/2012
 * @package: Jobs
-------------------------------------------------------------------------*/ 

defined('_JEXEC') or die('Restricted access');
class AdsHelperDateTime
{
    function isoDateToUTC($isodate)
    {
        $config = & JFactory::getConfig();
        $DateTime = JFactory::getDate($isodate , $config->getValue('config.offset'));
        return $DateTime->toMySQL();
        
    }
    function dateDiff($isodate)
    {
        $config = & JFactory::getConfig();
        $d1=JFactory::getDate($isodate);
        $d2=JFactory::getDate('now' , $config->getValue('config.offset'));
        $diff = $d2->toUnix()-$d1->toUnix();
        return $diff;
    }
    function dateToCountdown($isodate)
    {
        $diff=-self::dateDiff($isodate);
    	if ($diff>0){
    		$s=sprintf("%02d",$diff%60); $diff=intval($diff/60);
    		$m=sprintf("%02d",$diff%60); $diff=intval($diff/60);
    		$h=sprintf("%02d",$diff%24); $d=intval($diff/24);
    		if ($d>0) 
                return "$d ".JText::_("COM_JOBS_DAYS").", $h:$m:$s";
    		else 
                return "$h:$m:$s";
    	}else 
            return JText::_("COM_JOBS_EXPIRED");
        
        
    }
   function dateFormatConversion($dateformat)
    {
        $strftime_format=$dateformat;
        $strftime_format=str_replace('%','%%',$strftime_format);
        $strftime_format=str_replace('Y','%Y',$strftime_format);
        $strftime_format=str_replace('y','%y',$strftime_format);
        $strftime_format=str_replace('d','%d',$strftime_format);
        $strftime_format=str_replace('D','%A',$strftime_format);
        $strftime_format=str_replace('m','%m',$strftime_format);
        $strftime_format=str_replace('F','%B',$strftime_format);

        return $strftime_format;
    }
     
    function DateToIso($date) {
        //$cfg=&JTheFactoryHelper::getConfig();
        if (!$date)
            return $date; //empty date
        if (ads_opt_date_format == 'Y-m-d') {
            return $date;
        }

        if (ads_opt_date_format == 'm/d/Y') {
            if (preg_match("/([0-9]+)\/([0-9]+)\/([0-9]+)/", $date, $matches))
                return $matches[3] . "-" . $matches[1] . "-" . $matches[2];
        }
        if (ads_opt_date_format == 'd/m/Y') {
            if (preg_match("/([0-9]+)\/([0-9]+)\/([0-9]+)/", $date, $matches))
                return $matches[3] . "-" . $matches[2] . "-" . $matches[1];
        }
        if (ads_opt_date_format == 'd/m/Y') {
            if (preg_match("/([0-9]+)\/([0-9]+)\/([0-9]+)/", $date, $matches))
                return $matches[3] . "-" . $matches[2] . "-" . $matches[1];
        }
        if (ads_opt_date_format == 'd.m.Y') {
            if (preg_match("/([0-9]+)\.([0-9]+)\.([0-9]+)/", $date, $matches))
                return $matches[3] . "-" . $matches[2] . "-" . $matches[1];
        }
        if (ads_opt_date_format == 'D, F d Y') {
            $d=strtotime($date);
            return date("Y-m-d",$d);
        }

        if (ads_opt_date_format=='m-d-Y') {
            preg_match("/([0-9]+)-([0-9]+)-([0-9]+)/",$date,$matches);
            return $matches[3]."-".$matches[1]."-".$matches[2];
        }
        if (ads_opt_date_format=='d-m-Y') {
            preg_match("/([0-9]+)-([0-9]+)-([0-9]+)/",$date,$matches);
            return $matches[3]."-".$matches[2]."-".$matches[1];
        }

        if (ads_opt_date_format=='Y-d-m'){
            preg_match("/([0-9]+)-([0-9]+)-([0-9]+)/",$date,$matches);
            return $matches[1]."-".$matches[3]."-".$matches[2];
        }

        return $date;
    }

}


?>
