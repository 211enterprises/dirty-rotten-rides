<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

class AdsSmarty extends Smarty {
	function __construct(){
		parent::__construct();
		
		$this->register_function('printdate',array($this,'smarty_printdate'));
		$this->register_function('infobullet',array($this,'smarty_infobullet'));
		$this->register_function('set_css',array($this,'smarty_set_css'));
		$this->register_function('set_js',array($this,'smarty_set_js'));
		$this->register_function('createtab',array($this,'smarty_createtab'));
		$this->register_function('startpane',array($this,'smarty_startpane'));
		$this->register_function('starttab',array($this,'smarty_starttab'));
		$this->register_function('endpane',array($this,'smarty_endpane'));
		$this->register_function('endtab',array($this,'smarty_endtab'));
		$this->register_function('set_verified_position',array($this,'smarty_set_verified_position'));
		$this->register_function('package_image',array($this,'smarty_package_image'));
		$this->register_function('get_now',array($this,'smarty_get_now'));
		$this->register_modifier('add_day',array($this,'smarty_add_day'));
        
		$this->register_block('import_js_block',array($this,'smarty_import_js_block'));
		$this->register_block('import_css_block',array($this,'smarty_import_css_block'));
	}
	
	function display($tpl_name){
		$task = JRequest::getCmd('task',"");
		$view	= JRequest::getCmd( 'view' );
		$task	= JRequest::getCmd( 'task' );
		
		if(!$task)
			$task	= JRequest::getCmd( 'layout' );

		if(!$task)
		{
			switch($view){
				case "adsman":
					$task = "listadds";
				break;
				case "user":
					$task = "myuserprofile";
				break;
			}
		}
		
		if(!$task){
			$task = 'listadds';
		}
		
		$database = JFactory::getDbo();
		
		$payment = &JTheFactoryPaymentController::getInstance();
		$payment->processTemplate($task, $_REQUEST, $this);
		
		if(file_exists($this->template_dir.$tpl_name))
			parent::display($tpl_name);
		else 	
			parent::display(ADS_COMPONENT_PATH.'/templates/default/'.$tpl_name);
	}
	
	function _smarty_include($params){
		
		$tpl_inc = $params['smarty_include_tpl_file'];
		if(!file_exists($this->template_dir.$tpl_inc))
			$tpl_inc = ADS_COMPONENT_PATH.'/templates/default/'.$params['smarty_include_tpl_file'];
		
		$params['smarty_include_tpl_file'] = $tpl_inc;
		parent::_smarty_include($params);
	}


    function smarty_set_js($params){
    	$jdoc = JFactory::getDocument();
    
    	if(!empty($params['file']))
    		$jdoc->addScript($params['file']);
    	else
    		$jdoc->addScript(JURI::base()."components/com_adsman/js/adsman.js");
    }
    function smarty_import_js_block($params, $content, &$smarty, &$repeat) {
        if ($repeat)
            return; //ignore opening tag
        $doc = & JFactory::getDocument();
        $doc->addScriptDeclaration($content);
        if ($doc->getType() == 'raw')
            return "<script type=\"text/javascript\">$content</script>";
    }

    function smarty_import_css_block($params, $content, &$smarty, &$repeat) {
        if ($repeat)
            return; //ignore opening tag
        $doc = & JFactory::getDocument();
        $doc->addStyleDeclaration($content);
        if ($doc->getType() == 'raw')
            return "<style type=\"text/css\">$content</style>";
    }
    
    function smarty_ads_print_encoded($params, &$smarty){
        $extra = '';
    
        if (empty($params['address'])) {
            return "";
        } else {
            $address = $params['address'];
        }
    
        $text = $address;
        $encode = (empty($params['encode'])) ? 'none' : $params['encode'];
        
        if (!in_array($encode,array('javascript','javascript_charcode','hex','none')) ) {
            $smarty->trigger_error("print_encoded: 'encode' parameter must be none, javascript or hex");
            return;
        }
    
        if ($encode == 'javascript' ) {
            $string = 'document.write(\''.$text.'\');';
    
            $js_encode = '';
            for ($x=0; $x < strlen($string); $x++) {
                $js_encode .= '%' . bin2hex($string[$x]);
            }
    
            return '<script type="text/javascript">eval(unescape(\''.$js_encode.'\'))</script>';
    
        } elseif ($encode == 'javascript_charcode' ) {
            $string = $text;
    
            for($x = 0, $y = strlen($string); $x < $y; $x++ ) {
                $ord[] = ord($string[$x]);   
            }
    
            $_ret = "<script type=\"text/javascript\" language=\"javascript\">\n";
            $_ret .= "<!--\n";
            $_ret .= "{document.write(String.fromCharCode(";
            $_ret .= implode(',',$ord);
            $_ret .= "))";
            $_ret .= "}\n";
            $_ret .= "//-->\n";
            $_ret .= "</script>\n";
            
            return $_ret;
            
            
        } elseif ($encode == 'hex') {
    
            preg_match('!^(.*)(\?.*)$!',$address,$match);
            if(!empty($match[2])) {
                $smarty->trigger_error("mailto: hex encoding does not work with extra attributes. Try javascript.");
                return;
            }
            $text_encode = '';
            for ($x=0; $x < strlen($text); $x++) {
                $text_encode .= '&#x' . bin2hex($text[$x]).';';
            }
    
            $mailto = "&#109;&#97;&#105;&#108;&#116;&#111;&#58;";
            return $text_encode;
    
        } else {
            // no encoding
            return $text;
    
        }
    
    	
    }
    
    function smarty_get_now(){
    	return Ads_Time::getNow();
    }
    
    function smarty_add_day($string, $offset="1"){
    	return Ads_Time::apendDays($string, $offset);
    }
    
    function smarty_set_verified_position($params, &$smarty) {
    	
        $res="";
        if(!empty($params['pos'])) {
            $pos = $params['pos'];
            $db = JFactory::getDbo();
            $db->setQuery("UPDATE #__ads_positions SET verified = 1 WHERE name = '{$pos}'");
            $db->query();
        }
        return $res;
    
    }
    
    function smarty_infobullet($params, &$smarty) {
        $res="";
        if(!empty($params['text'])) {
        	JHTML::_('behavior.tooltip'); //load the tooltip behavior
            $res=JHTML::_('tooltip', $params['text']);
        }
        return $res;
    }
    
    function smarty_printdate($params, &$smarty) {
        $res="";
        if(!empty($params['date'])) {
            $dateformat=ads_opt_date_format;
            if ($params['use_hour']) $dateformat.=" H:i";
            $res=date($dateformat,strtotime($params['date']));
        }
        return $res;
    
    }
    
    function smarty_set_css($params, &$smarty) {
    
    	$jdoc = JFactory::getDocument();
    	if ( defined("ADDSMAN_TPL_THEME") && ADDSMAN_TPL_THEME!="" && file_exists(JPATH_ROOT."/components/com_adsman/templates/".ADDSMAN_TPL_THEME."/adsman.css") )
    		$jdoc->addStyleSheet(JURI::root()."components/com_adsman/templates/".ADDSMAN_TPL_THEME."/adsman.css");
    	else	
    		$jdoc->addStyleSheet(JURI::root()."components/com_adsman/templates/default/adsman.css");
    		
    	$jdoc->addStyleSheet(JURI::root()."components/com_adsman/css/main.css");	
    	
    }
    
    function smarty_createtab($params, &$smarty) {
    	jimport('joomla.html.pane');
    	global $pane;
    	$pane = &JPane::getInstance('Tabs');
    }
    
    function smarty_startpane($params, &$smarty) {
    	jimport('joomla.html.pane');
    	$pane = &JPane::getInstance('Tabs');
        $pane_id=empty($params['id'])?"ads-pane":$params['id'];
       	$res = $pane->startPane( $pane_id );
        return $res;
    }
    
    function smarty_endpane($params, &$smarty) {
    	$pane = &JPane::getInstance('Tabs');
    	return $pane->endPane();
    }
    
    function smarty_starttab($params, &$smarty) {
    	$pane = &JPane::getInstance('Tabs', array('allowAllClose' => true));
        $pane_id=empty($params['paneid'])?"ads-pane":$params['paneid'];
    	$tabText = JText::_($params['text']);
    	return $pane->startPanel( $tabText, $pane_id);
    }
    
    function smarty_endtab($params, &$smarty) {
    	$pane = &JPane::getInstance('Tabs', array('allowAllClose' => true));
        return $pane->endPanel();
    }
    function smarty_package_image($params, &$smarty) {
        $pack=str_replace(' ','_',strtolower($params['package']));
        
        $filename=JPATH_COMPONENT.DS.'images/ads/packages/'.$pack.'.jpg';
        if (file_exists($filename)) {
            $path = JURI::root() . 'images/ads/packages/'.$pack.'.jpg';
        } else {
            $path = JURI::root() . 'components/com_adsman/img/package.png';
        }
        return '<img src="'.$path.'" class="ads_package_image" />';
        
    }
}

?>