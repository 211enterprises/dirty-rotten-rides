<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/


class AdsUtilities {
	
	function loadLanguage(){
		
		$lang = JFactory::getLanguage();
		$lng = substr( $lang->get("tag") , 0, 2);
    	
	    if (file_exists(ADS_COMPONENT_PATH.'/lang/'.$lng.".php"))
	    	require_once(ADS_COMPONENT_PATH.'/lang/'.$lng.".php");
	    else
	    	require_once(ADS_COMPONENT_PATH.'/lang/default.php');
	}
	
	function redirectToProfile($id=null){
		
		if(defined("ads_opt_profile_mode"))
			return JTheFactoryUserProfile::setRedirectToProfile(ads_opt_profile_mode,$id);
			
		return null;
	}
	
    function getPublicTasks()
    {
    	$my = JFactory::getUser();
    	
		$public_tasks = array( "report_show", "details","listadds","downloadfile","terms", "show_search","listcats","userprofile","googlemap","sendMessage","showsearch","tellFriend_show","tellFriend","searchBookmarks","showBookmarks","ajax_form_customfields","ajax_search_customfields","ajax_selectcategories","ajax_search_selectcategories","add_credits_calculate","category_listing_cost");
		
		if( defined("ads_opt_logged_posting") && ads_opt_logged_posting==1 && !$my->id  )
        	array_push($public_tasks,"new", "edit", "save", "selectcat");
        	
		return $public_tasks;
    }
    
    
	function SmartyLoaderHelper($debug=false){
		
      $Itemid = JRequest::getVar('Itemid', 0, 'REQUEST', 'integer');
      $option = Jrequest::getCmd("option");
      
      $my = JFactory::getUser();
      
		$act =  JRequest::getVar('act', '' );
        // set path to Smarty directory *nix style
        if (!defined('SMARTY_DIR')) define('SMARTY_DIR', ADS_COMPONENT_PATH.'/smarty/libs/');
        // include the smarty class Note 'S' is upper case
        require_once(SMARTY_DIR . 'Smarty.class.php');
        require_once(ADS_COMPONENT_PATH . DS .'helpers' . DS . 'ads.smarty.plugins.php');

		$smarty	= new AdsSmarty();
		
		if( defined("ADDSMAN_TPL_THEME") && ADDSMAN_TPL_THEME!="" )
			$smarty->template_dir=ADS_COMPONENT_PATH.'/templates/'.ADDSMAN_TPL_THEME.'/';
		else	
			$smarty->template_dir=ADS_COMPONENT_PATH.'/templates/default/';

				
		$smarty->compile_dir=ADS_COMPONENT_PATH.'/templates/cache/';
		$smarty->debugging= $debug;

		$smarty->assign('IMAGE_ROOT',JURI::root().'components/com_adsman/img/');
		$smarty->assign('ROOT_HOST',JURI::root());
		$smarty->assign('ROOT_HOST_FEATURED',JURI::root().'index.php?option=com_adsman&amp;task=set_featured');

        $smarty->assign('ADSMAN_PICTURES',ADSMAN_PICTURES);

		$task	= JRequest::getCmd( 'task','listadds' );
		if($task=="myadds")
			$reset_f_task = "myadds";
		else
			$reset_f_task = "listadds";
		
		$smarty->assign('resetFilters', JRoute::_("index.php?option=com_adsman&amp;view=adsman&amp;task=$reset_f_task&amp;reset=all&amp;Itemid=$Itemid") );
		
		//$Qnow = date(ads_opt_date_format." ".ads_opt_date_time_format,Ads_Time::getNow());
		
		$smarty->assign('Itemid',$Itemid);
		$smarty->assign('option',$option);
		$smarty->assign('act',$act);
		$smarty->assign('task',$task);
		$smarty->assign('is_logged_in',($my->id)?"1":"0");
		$smarty->assign('userid',$my->id);
		
		$smarty->assign('AdsShowPostions', 0);
		
		$smarty->assign('local_site_time', JHTML::_('date',  'now', JText::_('DATE_FORMAT_LC2')));

		// some section for this
		$arr_dateformat=array(
			'Y-m-d'=>'%Y-%m-%d',
            'm/d/Y'=>'%Y-%m-%d',
            'd/m/Y'=>'%Y-%m-%d',
            'd.m.Y'=>'%Y-%m-%d',
			'D, F d Y'=>'%Y-%m-%d'
		);

		$smarty->assign('opt_date_format',$arr_dateformat[ads_opt_date_format]);
		
		return $smarty;
	}

	/**
	 * Chyrilik Transliteration
	 *
	 * @param  $string
	 * @return string
	 * 
	 * @since 1.5.3
	 */
	function transliterate($string)
	{
		$string = AdsUtilities::transliterate_cyr($string);
		$string = AdsUtilities::transliterate_co($string);
		return $string;
    }
    
    function transliterate_cyr( &$str ){
    	
	    $niddle = array("а", "б", "в", "г", "д", "ђ", "е", "ж", "з", "и", "�?", "к", "л", "љ", "м", "н",
	    "њ", "о", "п", "р", "�?", "т", "ћ", "�?", "ф", "х", "ц", "ч", "џ", "�?",
	    "�?", "Б", "В", "Г", "Д", "Ђ", "Е", "Ж", "З", "�?", "�?", "К", "Л", "Љ", "М", "�?",
	    "Њ", "О", "П", "Р", "С", "Т", "Ћ", "У", "Ф", "Х", "Ц", "Ч", "�?", "Ш");
	
	    $replace = array ("a", "b", "v", "g", "d", "d", "e", "z", "z", "i", "j", "k", "l", "lj", "m", "n", "nj", "o", "p",
	    "r", "s", "t", "c", "u", "f", "h", "c", "c", "dz", "s",
	    "A", "B", "B", "G", "D", "D", "E", "Z", "Z", "I", "J", "K", "L", "LJ", "M", "N", "NJ", "O", "P",
	    "R", "S", "T", "C", "U", "F", "H", "C", "C", "DZ", "S"
    	);
    	
		return str_replace($niddle, $replace, $str);
    }
    
	function transliterate_co($string)
	{
		// &
		$string = str_replace("&","a",$string);
	
	    // TMJ MOD
	    $string = urldecode("$string");
	    
		if (function_exists('iconv')) {
 			
		  	$lang =& JFactory::getLanguage();
		  	
		  	setlocale(LC_ALL, $lang->getLocale());  
		  	$string=iconv('UTF-8','ASCII//TRANSLIT',$string);
	  	
      	$string = htmlentities($string);
      	
		} else {
			$string = htmlentities(utf8_decode($string));
			$string = preg_replace(
			array('/&szlig;/','/&(..)lig;/', '/&([aouAOU])uml;/','/&(.)[^;]*;/'),
			array('ss',"$1","$1".'e',"$1"),
			$string);
	      }
	
	      $cw = array('^a','^i','^I','^A','quot;','&','$','@','%3C','%3E','?','%','amp','gt;','lt;','+', '*');
	      $cw2 = array('a','i','I','A',"","","\$","","","","\?","\%","","",""," ",'\*');
	      $string = str_replace($cw,$cw2,$string);
		
	      return $string;
	}
   
	function makeLinks($add) 
	{
		$Itemid = JRequest::getVar('Itemid', 0, 'REQUEST', 'integer');
	
		// SEF Optimisations
        require_once (JPATH_ROOT.DS.'components'.DS.'com_adsman'.DS.'options.php');

        if (ads_opt_transliterate == 1) {
            $add->title_sef = JFilterOutput::stringURLSafe(AdsUtilities::transliterate($add->title));
        } else {
            $add->title_sef = JFilterOutput::stringURLUnicodeSlug($add->title);
        }

		if(trim(str_replace('-','',$add->title_sef)) == '') {
			$datenow =& JFactory::getDate();
			$add->title_sef = $datenow->toFormat("%Y-%m-%d-%H-%M-%S");
		}
		// COMMENTED: brakes URL unsafe accents:
		//$add->title_sef = AdsUtilities::str_clean($add->title);
		
      	$links['otheradds']= JRoute::_( 'index.php?option=com_adsman&amp;task=listadds&amp;userid='. $add->userid .'&amp;Itemid='. $Itemid );
	    $links['details'] = JRoute::_('index.php?option=com_adsman&task=details&title='.$add->title_sef.'&id='. $add->id .'&Itemid='. $Itemid );

        $uri = JURI::getInstance();
        $root = $uri->root();
        $pathonly = $uri->base(true);

        $replace = '#'.$pathonly.'/#';
        $basepath = preg_replace($replace,'',$root);

        $links['tweeter'] = urlencode($basepath.$links['details']);
        //$links['tweeter'] = urlencode(JURI::root().$links['details']);

	  	$links['download_attachement'] = JRoute::_( 'index.php?option=com_adsman&amp;task=downloadfile&id='.$add->id.'&amp;Itemid='. $Itemid );
	  	$links['edit'] = JRoute::_( 'index.php?option=com_adsman&amp;&task=edit&view=adsman&id='.$add->id.'&amp;Itemid='. $Itemid );
	  	
	  	$links['republish'] = JRoute::_( 'index.php?option=com_adsman&amp;task=republish&amp;id='.$add->id.'&amp;Itemid='. $Itemid );
	  	$links['extend_add'] = JRoute::_( 'index.php?option=com_adsman&amp;task=extend_add&amp;id='.$add->id.'&amp;Itemid='. $Itemid );
	  	$links['publish'] = JRoute::_( 'index.php?option=com_adsman&amp;task=publish&amp;cid[]='.$add->id.'&amp;Itemid='. $Itemid );
	  	$links['cancel'] = JRoute::_( 'index.php?option=com_adsman&amp;task=canceladd&amp;id='.$add->id.'&amp;Itemid='. $Itemid );
		$links['user_profile'] = JRoute::_( 'index.php?option=com_adsman&amp;view=user&amp;task=userprofile&amp;aid='.$add->id.'&amp;id='. $add->userid .'&amp;Itemid='. $Itemid );
		$links['report'] = JRoute::_( 'index.php?option=com_adsman&amp;task=report_show&amp;tmpl=component&amp;   id='.$add->id.'&amp;Itemid='. $Itemid);
      
		// add category sef route ; if null process
		$catslug = "";
		$catslug_parent = "";
			if(isset($add->catslug)){
				$separator = PHP_EOL;
				$catslug = str_replace($separator,"/",str_replace("/","-",$add->catslug));
				$catslug = "&amp;catslug=$catslug";
				
				if (isset($add->catparent_id)) {
					$catslug_parent = "&amp;parent=$add->catparent_id";
				}
			}
			
        $links['filter_cat'] = JRoute::_( 'index.php?option=com_adsman&amp;task=listadds&amp;cat='.$add->category.$catslug.$catslug_parent.'&amp;Itemid='. $Itemid);
        //$links['filter_cat'] = JRoute::_( 'index.php?option=com_adsman&amp;task=listadds&amp;cat='.$add->category.$catslug.'&amp;Itemid='. $Itemid);
        
        $links['add_to_favorite'] = JRoute::_( 'index.php?option=com_adsman&amp;task=addtofav&amp;id='.$add->id.'&amp;Itemid='. $Itemid );
        $links['del_from_favorite'] = JRoute::_( 'index.php?option=com_adsman&amp;task=delfav&amp;id='.$add->id.'&amp;Itemid='. $Itemid );
		$links['terms'] = JRoute::_( 'index.php?option=com_adsman&amp;task=terms&view=adsman' );
		$links['user_rss'] = JRoute::_( 'index.php?option=com_adsman&amp;view=adsman&amp;format=feed&amp;userid='.$add->userid );
		$links['tags'] = '';
		if( isset($add->tags) )
		for($i=0;$i<count($add->tags);$i++){
		    if (isset($add->tags[$i])){
    		    $href=JRoute::_( 'index.php?option=com_adsman&amp;task=listadds&amp;Itemid='.$Itemid.'&amp;tag='.$add->tags[$i]);
    		    $links['tags'] .= "<span class='add_tag'><a href='$href'>".$add->tags[$i]."</a>".(($i+1<count($add->tags))?",":"")."</span>";
		    }
		}
		 
		return $links;
	}

    public static function getLangfilter()
      	{
    		// check for activation of languagefilter
    		$db		= JFactory::getDBO();
    		$query	= $db->getQuery(true);
    		$query->select('COUNT(*)');
    		$query->from($db->nameQuote('#__extensions'));
    		$query->where('type = '.$db->Quote('plugin'));
    		$query->where('element = '.$db->Quote('languagefilter'));
    		$query->where('enabled= 1');
    		$db->setQuery($query);
    		return $db->loadResult();
      	}


    function isFavorite($adid) {
        $my = JFactory::getUser();
        if (!$my->id || (isset($this->userid) && $my->id==$this->userid))
			return null;

		$db = JFactory::getDbo();

		$db->setQuery("SELECT * FROM #__ads_favorites WHERE adid = '".$adid."' AND userid = '".$my->id."'");
		$db->query();
		if($db->getNumRows()=="1")
			return 1;
		else
			return 0;
	}

	function getCurrencyName($currid) {
		$db = JFactory::getDbo();
		$db->setQuery("SELECT name FROM #__ads_currency WHERE id = '".$currid."'");
		if($tmp = $db->loadObject())
			return $tmp->name;
		else return false;
	}

    function getUserNameReported($adid) {
        $db = JFactory::getDbo();
        $db->setQuery("SELECT userid FROM #__ads WHERE id = '".$adid."'");
        $userid = $db->LoadResult();

        if ($userid) {
            $db = JFactory::getDbo();
            $db->setQuery("SELECT name FROM #__users WHERE id = '".$userid."'");

            if($tmp = $db->loadObject())
                return $tmp->name;
            else return '';
        }
        else
            return '';
    }
	
	function isMyAdd($ad_id) {
        $my = JFactory::getUser();

        $ad_id=($ad_id)?$ad_id:"-1";
        if ($my->id){
    		$database = JFactory::getDbo();
    		$database->setQuery("select count(*) from #__ads where userid='$my->id' and id in ($ad_id)");
            return $database->LoadResult();
        }
        else return false;
    }
    
    /**
     * Test if submiter has reached any limit
     *
     * @return false for no limit; true for limit reached
     */
    function testPosterLimit() {
        
    	$user = JFactory::getUser();
    	$userid = $user->get('id', true);

    	$getGroups = JAccess::getGroupsByUser($userid);
        $user_groups = implode(',',$getGroups);

    	$database = JFactory::getDbo();

    	$database->setQuery("SELECT limit_size FROM #__ads_limits WHERE owner='$userid' AND limit_type='user' ");
    	$limit = $database->LoadResult();
    	
    	if(!$limit){
	    	$database->setQuery("SELECT limit_size FROM #__ads_limits WHERE owner IN ( $user_groups ) AND limit_type='group' ");
	    	$limit = $database->LoadResult();
    	}

    	if(!$limit){
    		$limit = defined("ads_opt_global_limit") ? ads_opt_global_limit : null;
    	}
    	
    	if($limit){
			$database->setQuery("select count(*) from #__ads where userid='$user->id' and close_by_admin=0 and closed=0");
    		$cnt = $database->LoadResult();
    		if($cnt >= $limit)
    			return true;
    		else
    			return false;
    	}else
    		return false;
    }

	function makeHtmlFilters($filters){
		$db = JFactory::getDbo();
		$sfilters = array();

		
		if(!empty($filters["username"])){
			$sfilters["username"] = JText::_("ADS_USER")." - ".$filters["username"];
		}
		if(!empty($filters["userid"])){
			$u = & JTable::getInstance("user");
			$u->load($filters["userid"]);
			$sfilters["userid"] = JText::_("ADS_USER")." - ".$u->username;
			unset($u);
		}
		if(!empty($filters["tag"]))
			$sfilters["tag"] = JText::_("ADS_NEW_TAG")." - ".$filters["tag"];

		if(!empty($filters["keyword"]))
			$sfilters["keyword"] = JText::_("ADS_SEARCH_KEYWORD")." - ".$filters["keyword"];

/*		if(!empty($filters["in_description"]))
			$sfilters["in_description"] = "Searching in description";
*/
		if(!empty($filters["city"]))
			$sfilters["city"] = JText::_("ADS_CITY")." - ".$filters["city"];

		if(!empty($filters["state"]))
			$sfilters["state"] = JText::_("ADS_STATE")." - ".$filters["state"];

		if(!empty($filters["country"])){
			if (!CB_DETECT || ads_opt_profile_mode!="cb"){
				$c = new JAdsCountry($db);
				$c->load($filters["country"]);
				$sfilters["country"] = JText::_("ADS_COUNTRY")." - ".$c->name;
			}else{
				$sfilters["country"] = JText::_("ADS_COUNTRY")." - ".$filters["country"];
			}

		}

		if(!empty($filters["ad_type"])){
			$sfilters["ad_type"] = JText::_("ADS_AD_TYPE")." - ";
			
			switch ($filters["ad_type"]) {
				case '0':
					$sfilters["ad_type"] .= JText::_("ADS_ADTYPE_PUBLIC");
					break;
				case '1':
					$sfilters["ad_type"] .= JText::_("ADS_ADTYPE_PRIVATE");	
					break;
				case '2':
					$sfilters["ad_type"] .= JText::_("ADS_ADTYPE_ALL");	
					break;	
			}
			
			//$sfilters["ad_type"] .=($filters["ad_type"]=="0") ? JText::_("ADS_ADTYPE_PUBLIC") : JText::_("ADS_ADTYPE_PRIVATE");
		}

        if(!empty($filters["price_min"]))
                    $sfilters["price_min"] = JText::_("ADS_SEARCH_MIN_PRICE")." - ".$filters["price_min"];

        if(!empty($filters["price_max"]))
                    $sfilters["price_max"] = JText::_("ADS_SEARCH_MAX_PRICE")." - ".$filters["price_max"];

        if(!empty($filters["currency"])) {
         //GETcURRENCYNAME
            $currencyFilter = AdsUtilities::getCurrencyName($filters["currency"]);
            $sfilters["currency"] = JText::_("ADS_AD_CURRENCY")." - ".$currencyFilter;
        }

		if(!empty($filters["start_date"]))
			$sfilters["start_date"] = JText::_("ADS_START_DATE")." - ".$filters["start_date"];

		if(!empty($filters["end_date"]))
			$sfilters["end_date"] = JText::_("ADS_END_DATE")." - ".$filters["end_date"];

        if (!empty($filters["ad_city"])) {
            $sfilters["ad_city"] = JText::_("ADS_AD_CITY")." - ".$filters["ad_city"];
        }
        if (!empty($filters["ad_postcode"])) {
             $sfilters["ad_postcode"] = JText::_("ADS_AD_POSTCODE")." - ". $filters["ad_postcode"] . "' ";
        }


		$Fi = & FactoryFieldsFront::getInstance();
		$Fi->setHTMLFilters($filters,$sfilters);

		return $sfilters;
	}

		
	function ImportFromCSV(){ //initial

	 	$Itemid = JRequest::getVar('Itemid', 0, 'REQUEST', 'integer');
	 	$mainframe = JFactory::getApplication();
		jimport('joomla.filesystem.path');
error_reporting(E_ALL);

	 	$database = JFactory::getDbo();
	 	$my = JFactory::getUser();

		$tmpdir 		= uniqid( 'images_' );
		$base_Dir 		= JPath::clean( JPATH_ROOT .'/media/' );
		$extractdir 	= JPath::clean( $base_Dir . $tmpdir );
		@mkdir($extractdir);

		$msg="";

		require_once (JPATH_ADMINISTRATOR.DS.'components'.DS.'com_adsman'.DS.'tables'.DS.'addsman.php');
		$ads= new JAdsAddsman($database);

        require_once(JPATH_COMPONENT_SITE.DS.'thefactory'.DS.'front.images.php');
   		$imgTrans = new JTheFactoryImages();


		$errors=array();

		
		if(!is_uploaded_file($_FILES['csv']['tmp_name'])){
			
			$mainframe->redirect( "index.php?option=com_adsman",JText::_("ADS_UPLOAD_CSV"));
			
		}
		
		if($_FILES['csv']['tmp_name']){
			$csv_fname = $_FILES['csv']['name'];
			
			$ext=substr($csv_fname, strrpos($csv_fname, '.')+1, strlen($csv_fname));
			
			if(strtolower($ext)!="csv" && strtolower($ext)!="txt"){
			 	$msg .= JText::_("ADS_ERR_EXTENSION_NOT_ALLOWED");
			}
		    
			$upload_path = $base_Dir.$csv_fname;

			if (!move_uploaded_file($_FILES['csv']['tmp_name'],$upload_path)){
				$msg .= JText::_("ADS_ERR_UPLOAD_FAILED");
			}
			@chmod($upload_path,0777);
			
			if(!file_exists($upload_path)){
				$msg .= JText::_("ADS_ERR_FILE_NOT_THERE");
			}
		}

 		if($_FILES['arch']['tmp_name']){
 			$upload_arch = $base_Dir.$_FILES['arch']['name'];
 			
 			if(!move_uploaded_file($_FILES['arch']['tmp_name'],$upload_arch)){
				$msg .= ads_err_csv_import_error;
			}
			
			@chmod($upload_arch,0777);

			$archivename 	= $upload_arch;

			jimport('joomla.filesystem.path');
			$archivename = JPath::clean($archivename);


            JLoader::import('joomla.filesystem.archive');
            //$zipAdapter = JArchive::getAdapter('zip');
            $ret = JArchive::extract($archivename, $extractdir);

	 		if(substr(PHP_OS, 0, 3) == 'WIN') {
				define('OS_WINDOWS',1);
			} else {
				define('OS_WINDOWS',0);
			}

            //echo $archivename;exit;
            if($ret == 0) {
                die( 'Unrecoverable error ' );
            }

 		}
		$handle = fopen($upload_path, "r");

		set_time_limit(0);
		$i=1;
		
		while (($data = fgetcsv($handle, 30000, ",")) !== FALSE) {
		//	while (($data = fgetcsv($handle, 30000, "\t")) !== FALSE) {
		
			$ads->id = null;
		 	$ads->title = strip_tags(str_replace(array("`","´"),"'",$data[0]));
		 	$ads->short_description =  strip_tags(str_replace(array("`","´"),"'",$data[1]));
		 	$ads->description = preg_replace('/<script[^>]*?>.*?<\/script>/si','',$data[2]);
		 	$ads->picture = trim(str_replace(array("`","´"),"'",$data[3]));
		 	$ads->askprice=floatval(str_replace(array("`","´"),"'",$data[4]));
		 	
	 	    if(is_numeric($data[5]))
		 		$ads->currency = $data[5];
		 	else
		 		$ads->SetCurrency($data[5]);
		 		
		 	$ads->addtype = str_replace(array("`","´"),"'",$data[6]);
		 	
	 		switch ($ads->addtype){
	 		    default:
	 			case 'public':
					$ads->addtype = ADDSMAN_TYPE_PUBLIC;
				break;
				case 'private':
					$ads->addtype = ADDSMAN_TYPE_PRIVATE;
				break;
	 		}
	 		
	 		$ads->start_date = date("Y-m-d H:i:s",strtotime(str_replace(array("`","´"),"'",$data[7])));
		 	$ads->end_date = date("Y-m-d H:i:s",strtotime(str_replace(array("`","´"),"'",$data[8])));

		 	$ads->SetCategory($data[9]);
			if($data[10])
		 		$ads->userid=$data[10];
		 	else
 			    $ads->userid=$my->id;
 			
 			if (!$ads->title)
 			    $err[]='Title is empty';

 			$err = array();
		 	if(count($err)>0) {
		 		$errors[] ="$i : ".join('<br>',$err);
		 		$i++;
		 		continue;

		 	}
		 	
		 	$ads->atachment=str_replace(array("`","´"),"'",$data[11]);
		 	$ads->store();

			if(!empty($data[12])){
				$tags = new adsTags($database);
				$tags->setTags($ads->id,str_replace(array("`","´"),"'",$data[12]));
			}

			$arr_pics = array();
			$k=13;
			$j=30;
			for($m=$k;$m<=$j;$m++){
				if(isset($data[$m])){
					$arr_pics[] = trim(str_replace(array("`","´"),"'",$data[$m]));
				}

			}
                //exit;
            if ($ads->atachment && file_exists($extractdir.DS.$ads->atachment))
            {
                $file_name ="attach_{$ads->id}.fil";
				$path= ADDSMAN_IMAGE_PATH.$file_name;
                //$path= ADS_COMPONENT_PATH.DS."images/$file_name";

                @unlink($path);
				if(!rename($extractdir.DS.$ads->atachment, $path))
				    $msg.=$ads->atachment." - ".ads_err_upload_failed;

            }
			if ( $ads->picture && file_exists($extractdir.DS.$ads->picture ) ){
				$fname = $ads->picture;
				$ext=AdsUtilities::extract_file_ext($fname);

				if (!$ads->isAllowedImage($ext)) {
		          $msg .= ads_err_not_allowed_ext.': '.$ext;
		        } else {
                   if (filesize($extractdir.DS.$ads->picture)>ads_opt_max_picture_size*1024) {
                        $msg.=$ads->picture."- ".ads_err_imagesize_too_big."<br><br>";
                   } else {
            			$file_name="main_{$ads->id}.{$ext}";
						$path= ADDSMAN_IMAGE_PATH.$file_name;
                        @unlink($path);
	                       
                        if (rename($extractdir.DS.$ads->picture, $path)) {

                            $imgTrans->resize_image(ADDSMAN_IMAGE_PATH.$file_name,ads_opt_thumb_width,ads_opt_thumb_height,'resize');
                            $imgTrans->resize_image(ADDSMAN_IMAGE_PATH.$file_name,ads_opt_medium_width,ads_opt_medium_height,'middle');
                            $ads->picture=$file_name;
                            $ads->store();
					    } else {
						     $msg.=$ads->picture."- ".ads_err_upload_failed."<br><br>";
					    }
                   }
		        }
			}

		    $nrfiles=0;
			for ($m=0;$m<count($arr_pics);$m++) {
			    if ($nrfiles>=ads_opt_maxnr_images) continue;

				if (file_exists($extractdir.DS.$arr_pics[$m])) {
					
                   if (filesize($extractdir.DS.$arr_pics[$m])>ads_opt_max_picture_size*1024) {
                        $msg.=$arr_pics[$m]."- ".ads_err_imagesize_too_big."<br><br>";
                        continue;
                   }

					$fname = $arr_pics[$m];
					$ext=AdsUtilities::extract_file_ext($fname);
			        if(!$ads->isAllowedImage($ext)){
			          $msg .= ads_err_not_allowed_ext.': '.$ext;
			          continue;
			        }

			        $file_name=$fname.'.'.$ext;
					$pic=new JAdsPicture($database);
					$pic->id_ad     = $ads->id;
					$pic->userid    = $my->id;
					$pic->picture   = $file_name;
					$pic->modified  = date("Y-m-d H:i:s",time());
                    $pic->published = 1;
          			$pic->store();

					$file_name=$ads->id."_img_$pic->id.$ext";
					$pic->picture   = $file_name;
					$pic->store();

					$path= ADDSMAN_IMAGE_PATH.$file_name;
					if (rename($extractdir.DS.$arr_pics[$m], $path)) {
						
                        $imgTrans->resize_image(ADDSMAN_IMAGE_PATH.$file_name,ads_opt_thumb_width,ads_opt_thumb_height,'resize');
                        $imgTrans->resize_image(ADDSMAN_IMAGE_PATH.$file_name,ads_opt_medium_width,ads_opt_medium_height,'middle');
    				    
      				} else {

		    			$msg.=$arr_pics[$m]."- ".ads_err_upload_failed."<br><br>";
			       	}
			       	$nrfiles++;
				}
			}

		 	$i++;
		 }

    	fclose($handle);
		if(file_exists($extractdir)){
		 JFolder::delete($extractdir);
		}
        @unlink($archivename);
        @unlink($archivename);

		return $errors;
	}

	function GetCurrentPaymentSystem()
	{
	    return JRequest::getVar('paymenttype','pay_paypal');
	}

	function detectIntegration() {
	    global $cb_fieldmap;
	    $database = JFactory::getDbo();

	    //detect cb
	    $database->setQuery("SELECT count(*) FROM #__extensions WHERE `element`='com_comprofiler'");
	    if($database->loadResult()>0){
	    	define('CB_DETECT',1);
	    	$database->setQuery("SELECT field,cb_field FROM #__ads_cbfields");
	    	$r=$database->loadAssocList();
	    	for($i=0;$i<count($r);$i++){
	    	    $cb_fieldmap[$r[$i]['field']]=$r[$i]['cb_field'];
	    	}
	    }else {
	        define('CB_DETECT',0);
	    }
	    //detect cb

	}

/**
 * Utility to draw html select box populated with countries
 * @param: country to select
 * @param: only user with ads
 * @param: width in px of the selectbox
 */
	function makeCountryList($filter_country="",$existing_users = null ,$width=200){

		$active_only = 1;
		

		$active="";
		if($active_only){
			$active = " WHERE c.active=1";
		}
		
		$database = JFactory::getDbo();
		$filter = "";
		if($existing_users)
			$filter = " INNER JOIN #__ads_users AS p ON p.country = c.id ";
		$countryid = array();
		$sql = "SELECT DISTINCT c.id AS countryid, c.name FROM #__ads_country c {$filter} $active ORDER BY name";
		$database->setQuery($sql);
		$countryid[] = JHTML::_('select.option', '0', JText::_("ADS_SELECT_COUNTRY"),'countryid','name');
		$countryid = array_merge( $countryid, $database->loadObjectList() );

        return JHTML::_('select.genericlist', $countryid, 'country', 'class="inputbox"  style="width:'.$width.'px;" ', 'countryid', 'name', $filter_country);
	}


/**
 * Utility to draw html select box populated with cities
 * @param: city to select
 * @param: width in px of the selectbox
 */
	function makeUsersList($filter_user="", $width=200){
		$database = JFactory::getDbo();
		$tmp = array();
		$sql = "SELECT DISTINCT usr.id, CONCAT(usr.username, '".PHP_EOL."' ) as username FROM #__ads AS a RIGHT JOIN #__users AS usr ON usr.id = a.userid ORDER BY usr.username";
		$database->setQuery($sql);
		$tmp[] = JHTML::_('select.option', '0',JText::_("ADS_CHOOSE_USER"),'id','username');
		$tmp = array_merge( $tmp, $database->loadObjectList() );
		
		return JHTML::_('select.genericlist', $tmp, 'userid', 'class="inputbox"  style="width:'.$width.'px;" ', 'id', 'username', $filter_user);
	}
/**
 * Utility to draw html select box populated with cities
 * @param: city to select
 * @param: width in px of the selectbox
 */
	function makeCityList($filter_city="", $width=200){
		$database = JFactory::getDbo();
		$tmp = array();
		$sql = "SELECT DISTINCT city FROM #__ads_users  ORDER BY name";
		$database->setQuery($sql);
		$tmp[] = JHTML::_('select.option', '', JText::_("ADS_SELECT_CITY"),'city');
		$tmp = array_merge( $tmp, $database->loadObjectList() );
		return JHTML::_('select.genericlist', $tmp, 'city', 'class="inputbox"  style="width:'.$width.'px;" ', 'city', 'city', $filter_city);
	}

/**
 * Utility to draw html select box populated with tree categories
 * @param: category to select
 * @param: width in px of the selectbox
 * 
 * @deprecated: use JTheFactoryCategoryObj::makeCatTree instead
 * 
 */
	function makeCatTree($filter_category="", $id_select="cat", $width=200,$include_root=false,$all=null, $kids_only=0){
		$cat = new JTheFactoryCategoryObj();
		$cat->show_active_only=true;
		return $cattree = $cat->makeCatTree($filter_category, $id_select, $width,$include_root,$all,$kids_only);
	}
	
	function has_children($id)
	{
		return JTheFactoryCategoryObj::has_children($id);
		
	}

	function makeCurrencySelect($selected = "") {
		$database = JFactory::getDbo();

		$query = "SELECT id as value, name as text "
		. "\n FROM #__ads_currency"
		. "\n ORDER BY id"
		;
		$opts=null;
		$database->setQuery( $query );
		$opts = $database->loadObjectList();
		$html_curr =	JHTML::_('select.genericlist', $opts, 'currency', 'class="inputbox" size="1"',  'value', 'text',$selected);
		return $html_curr;
	}

    function makeCurrencyRadio($selected = 0) {
        $database = JFactory::getDbo();
        $query = "SELECT id as value, name as text "
        . "\n FROM #__ads_currency"
        . "\n ORDER BY id"
        ;

        $database->setQuery( $query );
        $currencies = $database->loadObjectList();

        //$html_curr =	JHTML::_('select.genericlist', $opts, 'currency', 'class="inputbox" size="1"',  'value', 'text',$selected);

        $optsCurrency = array();
        $optsCurrency[] = JHTML::_('select.option', '0',JText::_("ADS_ALL_CURRENCIES"));
        foreach ($currencies as $v=>$currency) {
            $optsCurrency[] = JHTML::_('select.option', $currency->value, $currency->text);
        }

        $html_currency_radio =	JHTML::_('select.radiolist', $optsCurrency,'currency','class="inputbox" ','value', 'text', $selected,'currency');
        return $html_currency_radio;
    }

    function makeCurrencyDropDown($selected = 0) {
        $database = JFactory::getDbo();
        $query = "SELECT id as value, name as text "
        . "\n FROM #__ads_currency"
        . "\n ORDER BY id"
        ;

        $database->setQuery( $query );
        $currencies = $database->loadObjectList();

        //$html_curr =	JHTML::_('select.genericlist', $opts, 'currency', 'class="inputbox" size="1"',  'value', 'text',$selected);

        $optsCurrency = array();
        $optsCurrency[] = JHTML::_('select.option', '0',JText::_("ADS_ALL_CURRENCIES"));
        foreach ($currencies as $v=>$currency) {
            $optsCurrency[] = JHTML::_('select.option', $currency->value, $currency->text);
        }

        $html_currency_radio =	JHTML::_('select.genericlist', $optsCurrency,'currency','class="inputbox" ','value', 'text', $selected);
        return $html_currency_radio;
    }

	function adsAuthorize($task) {
		$mainframe = JFactory::getApplication();

		$view = JRequest::getCmd( 'view' );
		if($view=="maps") return true;

        if (in_array($task,AdsUtilities::getPublicTasks())) return true;
        
		$db = JFactory::getDbo();
		$my = JFactory::getUser();
		
		if(!$my->id){
			JError::raiseWarning(1, JText::_("ADS_ERR_LOGIN_FIRST") );
			return false;
		}else
			return true;
	}
   
	function addDatetoIso($date)
	{
	
		if (ads_opt_date_format=='Y-m-d'){
	        return $date;
	    }

	    if (ads_opt_date_format=='Y-d-m'){
	        preg_match("/([0-9]+)-([0-9]+)-([0-9]+)/",$date,$matches);
	        return $matches[1]."-".$matches[3]."-".$matches[2];
	    }
	    if (ads_opt_date_format=='m-d-Y') {
	        preg_match("/([0-9]+)-([0-9]+)-([0-9]+)/",$date,$matches);
	        return $matches[3]."-".$matches[1]."-".$matches[2];
	    }
	    if (ads_opt_date_format=='d-m-Y') {
	        preg_match("/([0-9]+)-([0-9]+)-([0-9]+)/",$date,$matches);
	        return $matches[3]."-".$matches[2]."-".$matches[1];
	    }
        if (ads_opt_date_format == 'm/d/Y') {
            if (preg_match("/([0-9]+)\/([0-9]+)\/([0-9]+)/", $date, $matches))
                return $matches[3] . "-" . $matches[1] . "-" . $matches[2];
        }

        if (ads_opt_date_format == 'd/m/Y') {
            if (preg_match("/([0-9]+)\/([0-9]+)\/([0-9]+)/", $date, $matches))
                return $matches[3] . "-" . $matches[2] . "-" . $matches[1];
        }

        if (ads_opt_date_format == 'd.m.Y') {
            if (preg_match("/([0-9]+)\.([0-9]+)\.([0-9]+)/", $date, $matches))
                return $matches[3] . "-" . $matches[2] . "-" . $matches[1];
        }

        if (ads_opt_date_format == 'D, F d Y') {
            $d=strtotime($date);
            return date("Y-m-d",$d);
        }
	   
	    return $date;
	}

	function changeContent( $state = 0 ) {
		
		$mainframe = JFactory::getApplication();
		$db		= JFactory::getDbo();
		$cid	= JRequest::getVar( 'cid', array(), 'request', 'array' );
		$cids	= implode(',', $cid);

		$query = 'UPDATE #__ads' .
				' SET status = '. (int) $state .
				' WHERE id IN ( '. $cids .' ) ';
		$db->setQuery($query);
		if (!$db->query()) {
			JError::raiseError( 500, $db->getErrorMsg() );
			return false;
		}
		switch ($state)
		{
			case 1 :
				$msg = JText::sprintf('ADS_ITEMS_PUBLISHED', $total);
				break;
			default:
				$msg = JText::sprintf('ADS_ITEMS_UNPUBLISHED', $total);
				break;
		}

		if($state==1){
			require_once (JPATH_ADMINISTRATOR.DS.'components'.DS.'com_adsman'.DS.'tables'.DS.'addsman.php');
			$add = new JAdsAddsman($db);
			foreach ($cid as $c => $ci) {
				
				$add->load($ci);
				$id_string = $add->userid;
				$query = "SELECT u.* FROM #__users u
						  WHERE u.id IN ($id_string)";
				$db->setQuery($query);
				$mails = $db->loadObjectList();
				$add->SendMails($mails,'add_published');
				
				
            	$query = " SELECT u.* FROM #__users u
    				   LEFT JOIN #__ads_watchlist f ON f.userid = u.id
    				   WHERE f.catid = '$add->category' AND u.id <> '$add->userid' ";
	            $db->setQuery($query);
	            $mails = $db->loadObjectList();
	            $add->SendMails($mails, 'add_aded_to_fav');
				
			}
		}

		$rtask = '&task=listadds';
		$mainframe->redirect('index.php?option=com_adsman'.$rtask, $msg);
	}

	function changeCat( $state = 0 ) {
		
		$mainframe = JFactory::getApplication();
		$db		= JFactory::getDbo();
		$cid	= JRequest::getVar( 'cid', array(), 'post', 'array' );
		$cids	= implode(',', $cid);

		$query = 'UPDATE #__ads_categories' .
				' SET status = '. (int) $state .
				' WHERE id IN ( '. $cids .' ) ';
		$db->setQuery($query);
		if (!$db->query()) {
			JError::raiseError( 500, $db->getErrorMsg() );
			return false;
		}
		switch ($state) {
			case 1 :
				$msg = JText::sprintf('ADS_ITEMS_PUBLISHED', $total);
				break;
			default:
				$msg = JText::sprintf('ADS_ITEMS_UNPUBLISHED', $total);
				break;
		}

		$rtask = '&task=categories';
		$mainframe->redirect('index.php?option=com_adsman'.$rtask, $msg);
	}

	function block_add( $state = 0 ) {
		
		$mainframe = JFactory::getApplication();
		$db		= JFactory::getDbo();
		$cid	= JRequest::getVar( 'cid', array(), 'post', 'array' );
		$cids	= implode(',', $cid);

		require_once (JPATH_ADMINISTRATOR.DS.'components'.DS.'com_adsman'.DS.'tables'.DS.'addsman.php');
		$add = new JAdsAddsman($db);

		$query = 'UPDATE #__ads' .
				' SET close_by_admin = '. (int) $state .
				' WHERE id IN ( '. $cids .' ) ';
		$db->setQuery($query);
		if (!$db->query()) {
			JError::raiseError( 500, $db->getErrorMsg() );
			return false;
		}
		switch ($state)
		{

			case 1 :
				$msg = JText::sprintf('ADS_ITEMS_CLOSED', $total);
				break;
			default:
				$msg = JText::sprintf('ADS_ITEMS_OPENED', $total);
				break;
		}
		if($state==1){
			foreach ($cid as $c => $ci){
				$add->load($ci);
				$id_string = $add->userid;
				$query = "SELECT u.* FROM #__users u
						  WHERE u.id IN ($id_string)";
				$db->setQuery($query);
				$mails = $db->loadObjectList();
				$add->SendMails($mails,'add_admin_blocked');
			}
		}
		$rtask = '&task=listadds';
		$mainframe->redirect('index.php?option=com_adsman'.$rtask, $msg);
	}
	
	function block_user( $state = 1 ) {
		
		$mainframe = JFactory::getApplication();
		$db		= JFactory::getDbo();
		$cid	= JRequest::getVar( 'cid', array(), 'request', 'array' );
		$cids	= implode(',', $cid);

		require_once (JPATH_ADMINISTRATOR.DS.'components'.DS.'com_adsman'.DS.'tables'.DS.'addsman.php');
		require_once (JPATH_ADMINISTRATOR.DS.'components'.DS.'com_adsman'.DS.'tables'.DS.'adsuser.php');
				
		$add = new JAdsAddsman($db);
					
		foreach ($cid as $c => $cid) {
			$user    = JFactory::getUser($cid);
			$user->block = $state;

			if (!$user->save()) {
				$this->setError($user->getError());
				return false;
			}
		}

		switch ($state)
		{
			case 0 :
				$msg = JText::sprintf('ADS_USER_UNBLOCKED', $total);
				break;
			default:
				$msg = JText::sprintf('ADS_USER_BLOCKED', $total);
				break;
		}
		
		if ($state==1) {
			$query = "SELECT u.* FROM #__users u
					  WHERE u.id IN ($cids)";
			$db->setQuery($query);
			$mails = $db->loadObjectList();
			
			$add->SendMails($mails,'user_admin_blocked');
		}
		
		$rtask = '&task=users';
		$mainframe->redirect('index.php?option=com_adsman'.$rtask, $msg);
	}

    function processing_report($state = 1){

        $mainframe = JFactory::getApplication();
        $db		= JFactory::getDbo();
        $cid	= JRequest::getVar( 'cid', array(), 'post', 'array' );
        $cids	= implode(',', $cid);

        $query = 'UPDATE #__ads_report' .
                ' SET status = "processing"'.
                ' WHERE adid IN ( '. $cids .' ) ';
        $db->setQuery($query);

        if (!$db->query()) {
            JError::raiseError( 500, $db->getErrorMsg() );
            return false;
        }
        switch ($state)
        {
            case 0 :
                $msg = JText::sprintf('ADS_ITEMS_MARKED_UNPROCESSED');
                break;

            default :
                //"processing"
                $msg = JText::sprintf('ADS_ITEMS_MARKED_IN_PROGRESS');
                break;
        }

        $mainframe->redirect('index.php?option=com_adsman&task=reported_adds ', $msg);
    }

    function solved_report($state = 1){
        $mainframe = JFactory::getApplication();
        $db		= JFactory::getDbo();
        $cid	= JRequest::getVar( 'cid', array(), 'post', 'array' );
        $cids	= implode(',', $cid);

        $query = 'UPDATE #__ads_report' .
                ' SET status = "solved"'.
                ' WHERE adid IN ( '. $cids .' ) ';
        $db->setQuery($query);

        if (!$db->query()) {
            JError::raiseError( 500, $db->getErrorMsg() );
            return false;
        }
        switch ($state)
        {
            case 1 :
                $msg = JText::sprintf('ADS_ITEMS_SOLVED');
                break;

            default :
                $msg = JText::sprintf('ADS_ITEMS_UNSOLVED');
                break;
        }

        $mainframe->redirect('index.php?option=com_adsman&task=reported_adds ', $msg);
    }


	function get_file_ext($fname){
	    $ext="";
	    if (strrpos($fname, '.'))
	      $ext=substr($fname, strrpos($fname, '.')+1, strlen($fname));
	
	    return $ext;
	}
	
	function extract_file_ext(&$fname)
	{
	    $ext="";
	    if (strrpos($fname, '.')){
	      $ext=substr($fname, strrpos($fname, '.')+1, strlen($fname));
	      $fname=substr($fname,0, strrpos($fname, '.'));
	    }
	    return $ext;
	}
	
	function str_clean($s){
		return JTheFactoryHelper::str_clean($s);
	}
	
	function init_captcha(){
		
		if(ads_opt_enable_captcha && defined("ads_opt_recaptcha_public_key")){
			if(!function_exists('recaptcha_get_html'))
			{
				require(JPATH_COMPONENT_SITE.DS.'helpers'.DS.'recaptcha'.DS.'recaptchalib.php');
			}
			
			$params = null;
			if(defined("ads_opt_recaptcha_theme")){
				$params["theme"] = ads_opt_recaptcha_theme;
			}
			return recaptcha_get_html(ads_opt_recaptcha_public_key, $params);
		}
		
	}
	
	function verify_captcha(){
		if( !defined("ads_opt_recaptcha_private_key") )
			return true;
			
		if(!function_exists('recaptcha_get_html') || !function_exists('recaptcha_check_answer'))
		{
			require(JPATH_COMPONENT_SITE.DS.'helpers'.DS.'recaptcha'.DS.'recaptchalib.php');
		}
		
		$recaptcha_challenge_field = JRequest::getVar('recaptcha_challenge_field');
		$recaptcha_response_field = JRequest::getVar('recaptcha_response_field');

		$resp = recaptcha_check_answer (ads_opt_recaptcha_private_key, $_SERVER["REMOTE_ADDR"], $recaptcha_challenge_field, $recaptcha_response_field);
		return $resp->is_valid;
	}
	
	function cloack_email($mail_address){

		jimport( 'joomla.html.parameter' );
		
		if(defined("ads_opt_enable_antispam_bot") && ads_opt_enable_antispam_bot==1 && defined("ads_opt_choose_antispam_bot")){
			
			if (ads_opt_choose_antispam_bot == "recaptcha" ){

				if(!function_exists('recaptcha_mailhide_url'))
				{
					require(JPATH_COMPONENT_SITE.DS.'helpers'.DS.'recaptcha'.DS.'recaptchalib.php');
				}
				$mail = recaptcha_mailhide_url("01WxCXdKklKdG2JpOlMY15jw==","2198178B23BFFB00CBAEA6370CE7A0B2", $mail_address);
				return "<a href=\"$mail.\" onclick=\"window.open('$mail', '', 'toolbar=0,scrollbars=0,location=0,statusbar=0, menubar=0,resizable=0,width=500,height=300');	return false;\" title=\"Reveal this e-mail address\">".JText::_("ADS_CAPTCHA_SHOW_EMAIL")."</a>";
				
			} elseif (ads_opt_choose_antispam_bot  == "joomla" ) {
				
				// Discutable if use Content Mail Plugin or ... just JHTML_('email.cloac' it does the same .. just global configuration is in question
				$mainframe = JFactory::getApplication();
				
			    $plugin =& JPluginHelper::getPlugin('content', 'emailcloak');
			    $pluginParams = new JParameter( $plugin->params );
		    
			    require_once(JPATH_SITE.DS.'plugins'.DS.'content'.DS.'emailcloak'.DS.'emailcloak.php');
			    
			    $dispatcher = JDispatcher::getInstance();
			    $params = new JRegistry();
			    $result = $dispatcher->trigger('onContentPrepare', array('com_adsman.adsman', &$mail_address, &$params));

			    return $mail_address;
				
			} elseif (ads_opt_choose_antispam_bot =="smarty") {
				$smarty = AdsUtilities::SmartyLoaderHelper();
				require_once(SMARTY_DIR."plugins/function.mailto.php");
				return smarty_ads_print_encoded(array("address"=>$mail_address, "encode"=>"hex"), $smarty);
			}
		}
		else
			return $mail_address;
	}
	
	function process_countdown( & $add ){
    	
   	$time = Ads_Time::getNow();
		
		if ( ads_opt_enable_countdown ){
			
    		$config = & JFactory::getConfig();
			$offset = $config->getValue("offset");
		
			if ( defined("ads_opt_component_offset") && ads_opt_component_offset!="" )
				$offset = ads_opt_component_offset;

			if ($offset != 'UTC') {
				$corelate = 0;
			} else {
				$corelate = 1;
			}
			
			$expiredate = Ads_Time::getTimeStamp($add->end_date,$corelate);
    		//$expiredate = Ads_Time::getTimeStamp($add->end_date,1);
    		$wtf1 = date(ads_opt_date_format." ".ads_opt_date_time_format,$time);
    		$wtf2 = date(ads_opt_date_format." ".ads_opt_date_time_format, $expiredate );
    		
        	$diff = $expiredate - $time;
        	
        	if ( $diff > 0 ) {
        		$s	= sprintf("%02d",$diff%60); 
        		$diff	= intval($diff/60);
        		
        		$m	= sprintf("%02d",$diff%60); 
        		$diff	= intval($diff/60);
        		
        		$h	= sprintf("%02d",$diff%24); 
        		
        		$d		= intval($diff/24);
        		
        		if ($d > 0) 
        			$add->countdown	= "$d ".JText::_("ADS_DAYS").", $h:$m:$s";
        		else 
        			$add->countdown	= "$h:$m:$s";
        			
        	} else {
        		$add->countdown	= JText::_("ADS_EXPIRED");
        	}
        	
		}

        if ( $add->closed == '1' && ($add->closed_date > $add->end_date) && $add->status == '1' && $add->close_by_admin == '0') {
            $add->archived=true;
        } else {
            $add->archived=false;
        }

        if ( (Ads_Time::getTimeStamp($add->end_date) <= $time) && $add->closed == 0 && $add->status == 1 && $add->close_by_admin == 0 ) {
			$add->expired=true;
        }
        else {
            $add->expired=false;
        }



			
	    $add->start_date_text = date(ads_opt_date_format, strtotime($add->start_date) );
	    
	    if ($add->end_date && $add->end_date!='0000-00-00 00:00:00' ) {
			if (ads_opt_enable_hour) {
				$dateformat=ads_opt_date_format." H:i";
			} else 
				$dateformat=ads_opt_date_format;
			
			$add->end_date_text=date($dateformat, strtotime($add->end_date) );
		}
		
	}
	
	/**
	 * Clean array for DB queries
	 * 
	 */
	function sanitize( &$all_to_filter){
		
		$db = JFactory::getDbo();

		// sql escape the request
		foreach ($all_to_filter as $key => $value){
			if (is_array($value)) {
                $array_to_string = implode(',',$value);

                $all_to_filter[$key] = $array_to_string;
            }

			$all_to_filter[$key] = $db->getEscaped( $all_to_filter[$key] );
			
		}
	}

	function getCategoriesDepth() {
	
		$database = JFactory::getDbo();
		$q = "SELECT c.* FROM `#__ads_categories_sef` c ";
		$database->setQuery($q);
		$children = $database->loadAssocList('id');
	
		$max_depth = 10;
		$depth = 2;
		
		foreach ($children as $row) {
			
			$x = nl2br($row['categories']);
					
			$result = preg_split('#<br \/>#', $x);
			$row_depth = count($result);
			
			if ($row_depth >= $depth) 
				$depth = $row_depth;
		
		}

		if ($depth > $max_depth)
			$categories_depth = $max_depth; // limit to 10 left join	
		else 
			$categories_depth = $depth; 		
		
		return $categories_depth;	
	}
	
	public function joinCategories($cat) {
		
		$depth = AdsUtilities::getCategoriesDepth();
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		
		$query->select('c1.id')
 			 ->from('#__ads_categories c1')
			 ->where('c1.parent ='.$cat, ' OR ');
		
		
		for ($k=2; $k<$depth+1; $k++) {
			$prev_table = $k-1;
			$query->leftJoin('#__ads_categories c'.$k.' ON c'.$k.'.id = c'.$prev_table.'.parent')
			 ->where('c'.$k.'.parent = '.(int) $cat ,' OR ');
		}
	
		return $query;
	}
	
	public function getItemPriceCredits($itemname) {
		
		$db = JFactory::getDbo();
		$db->setQuery("SELECT price FROM #__ads_pricing WHERE itemname = '$itemname' ");
		$no_credits =  $db->LoadResult();
		
		return (int)$no_credits;
	}
	
	function getCategoryPrice($catid,$default_price_categories,$preferential_categories,$commisionable_categories){
		
		$price = 0;
		$db = JFactory::getDbo();
		
		if( $preferential_categories == 1 ) {
			
			if($default_price_categories)
				$price = $default_price_categories;
				
			if($catid) {
				$money_takin_categories = explode(",",$commisionable_categories);
				
				if ( in_array($catid,$money_takin_categories) ) {
					$ptype= "listing";
					$sql = " SELECT price+0 ".
							" FROM #__ads_custom_prices ".
							" WHERE category = '$catid' AND price_type= '$ptype'";
							$db->setQuery($sql);
					$custom_price_Result = $db->loadResult();
					
					if( isset($custom_price_Result) && (int)$custom_price_Result >= 0 ){
						$price =  $custom_price_Result;
					}
						
				} else { 
					$price = 0;
				}	
			}
		}
		
		return $price;
    }
	
	function getUserCredits($user){
    	$db = JFactory::getDbo();
		$db->setQuery("SELECT credits_no FROM #__ads_user_credits WHERE userid='$user' ");
		
		$credits =  $db->LoadResult();
		
		return (int)$credits;
	}
	
	function getCountUserAds($userid) {
		$db = JFactory::getDbo();
		$db->setQuery("SELECT count(a.id) FROM #__ads a WHERE a.userid='$userid' AND a.closed != 1 AND a.status = 1 AND a.close_by_admin != 1 ");
		$ads_number =  $db->LoadResult();
		
		return (int)$ads_number;
	}

    function getMainImgReplace($id) {
		$db = JFactory::getDbo();
        $db->setQuery("SELECT a.id FROM #__ads a WHERE a.id='$id' AND picture IS NOT NULL AND picture='' AND a.status = 1 ");
        
		$can_replace_img =  ($db->LoadResult() != null) ? 1 : 0;
        
		return (int)$can_replace_img;
	}

    function getPicturesReplace($id) {
        $db 	= JFactory::getDbo();
        $db->setQuery("SELECT count(*) FROM #__ads_pictures WHERE id_ad ='$id' AND `published` = 0 ");

        $images_count	= $db->loadResult();
        if ($images_count != null)
            return $images_count;
        else
            return 0;
    }

	function add_credits_calculate($id) {
        
		if ($id && !AdsUtilities::isMyAdd($id)) { 
			JError::raiseWarning(1,JText::_("ALERTNOTAUTH")); 
			return;
		}
					
    	$post_values = JRequest::get('POST');	
    	$my = JFactory::getUser();
		$db = JFactory::getDbo();
		$credits = AdsUtilities::getUserCredits($my->id);

    	$nrfiles_images = 0;
		$is_main_pic = 0;
		
    	foreach ($_FILES as $k => $file) {
            
	       	if (substr($k, 0, 7) != "picture")
	               continue;
	            
			if ( !isset($file['name']) || $file['name']=="") 
	           	continue;
 
	        if ( !is_uploaded_file(@$file['tmp_name']) ){
	            continue;
	        }
    
	        if ( !ads_opt_resize_if_larger && filesize($file['tmp_name'] ) > ads_opt_max_picture_size * 1024 ) {
	            continue;
	        }
	
	        $fname = $file['name'];
	        $ext = AdsUtilities::get_file_ext($fname);
	
	        /*if (!$add_obj->isAllowedImage($ext)) {
	            //$msg .= JText::_("ADS_ERR_EXTENSION_NOT_ALLOWED") . ': ' . $file['name'];
	            $nrfiles_images = 0;
	        }*/
	                
	        if ( $k == "picture" ) {
	            $is_main_pic = 1;
	            //count main pic 
	        } else { 
	          	$nrfiles_images++;
	        }
    	}
			
		$ad_valability = (isset($post_values['ad_valability'])) ? $post_values['ad_valability'] : -1;

		$selected_feature = isset($post_values['featured']) ? $post_values['featured'] : 'none'; 	//'silver'
		$ad_valability = $ad_valability;  //0 => 2 days = 4 credits

		$total_price = 0;
		$listing_price = 0;
		$main_img_price = 0;
		$pictures_price = 0;
		$featured_price = 0;
		
		$payOb = JTheFactoryPaymentLib::getInstance();
	    $payOb->loadAvailabeItems();
		$pricings = $payOb->price_plugins;  // array['price_featured_bronze'], ['price_packages'], ['price_pay_image'],['price_listing']
		
		if (isset($pricings['price_listing']) && $pricings['price_listing']->enabled == 1) {
			
		//	if ($ad_valability != -1) {
			//Preconfigured Availabilities Ranges
			  if ( isset($pricings['price_listing']->time_valability) && $pricings['price_listing']->time_valability == 1 ) {  // ==1
								
				$params_arr = array();
			
				if ($pricings['price_listing']->time_valability_prices  && trim( strip_tags($pricings['price_listing']->time_valability_prices) ) !=""  ) {
					$params_arr = explode("|", trim( strip_tags($pricings['price_listing']->time_valability_prices) ) );
						
					$valabs = array();
					$vprices = array();
					
					if (count($params_arr) > 0) {
						foreach ($params_arr as $p){
							if ($p) {
								$pi = explode(":",$p);
								$valabs[] = $pi[0];
								$vprices[$pi[0]] = $pi[1];
							}
						}
						
						$index_period = (int)($valabs[(int)$ad_valability]);
						$listing_price = $vprices[$index_period];
					}
				} else {
					$listing_price = $pricings['price_listing']->price;
				}
			
			  }  else {
				//Unlimited Publishing 
				$listing_price = 0;
				$catid = $post_values['category'];

				if ( isset($pricings['price_listing']->preferential_categories) && $pricings['price_listing']->preferential_categories == 1) {
									
					if ($pricings['price_listing']->default_price_categories && $pricings['price_listing']->prices_categories == 0) {
						$listing_price = $pricings['price_listing']->default_price_categories;
						
					} else {
						if ( isset($catid) && $catid > 0 ) {
							$listing_price = AdsUtilities::getCategoryPrice($catid,$pricings['price_listing']->default_price_categories,$pricings['price_listing']->preferential_categories,$pricings['price_listing']->commisionable_categories);
						}
						else	
							$listing_price = $pricings['price_listing']->price;
					} 
				} else {
					$listing_price = $pricings['price_listing']->price;
				}
		
			  }

	} else {
		$listing_price = 0;
		
    }

    $add_pictures_selected_index = 0;
    if ( isset($pricings['price_pay_image']) && $pricings['price_pay_image']->enabled == 1) {

        $count_replace_main = AdsUtilities::getMainImgReplace($id);

		//$add_main_img = $is_main_pic;		//0 - NO, 1
		$add_pictures_selected_index = $nrfiles_images; //0 = none, 1 = 1 picture to upload,...

        if ($id) {
            $query = "SELECT count(*) FROM #__ads_pictures WHERE id_ad = '".$id."' AND `published` = 0 ";
            $db->setQuery( $query );
            $count_replace_images	= $db->loadResult();
        } else {
            $count_replace_images = 0;
        }

		$img_prices = price_pay_image_DBHelper::getPayImageAmount();

        if ($is_main_pic == 1 && $count_replace_main == 0) {
            $main_img_price = price_pay_image_DBHelper::getCreditImg(0);
        }

  		if ($add_pictures_selected_index != 'none' && $add_pictures_selected_index > $count_replace_images) {
			$images_to_charge = $add_pictures_selected_index - $count_replace_images;

            $pictures_price = $img_prices[$images_to_charge];
            //$pictures_price = $img_prices[$add_pictures_selected_index];
    	}
	} else {
		$main_img_price = 0;
		$pictures_price = 0;
	}

	
	if ( $selected_feature != 'none') {
		
		$featured_request = "price_featured_$selected_feature";
		
		if ( isset($pricings[$featured_request]) && $pricings[$featured_request]->enabled == 1) {
			$featured_price = AdsUtilities::getItemPriceCredits('featured_'.$selected_feature);
		}
	} else {
		$featured_price = 0;
	}

		$total_price_ad = $listing_price + $main_img_price + $pictures_price + $featured_price;
	
		$root = JUri::root();
		$link = JRoute::_ ( 'index.php?option=com_adsman&task=add_credits_calculate&format=raw' );
		
		$document = JFactory::getDocument();
		$document->addScriptDeclaration(' var root = "'.$root.'";');
		$document->addScriptDeclaration(' var link = "'.$link.'";');
		
		$needed_credits = $total_price_ad - $credits;
		
		$d['needed_credits'] = $needed_credits;
		$d['total_price_ad'] = $total_price_ad;
		
		$prices	= array();
		$prices['total_price_ad'] = $total_price_ad;
		$prices['listing_price'] = $listing_price;
		$prices['main_img_price'] = $main_img_price;
		$prices['pictures_price'] = $pictures_price;
		$prices['featured_price'] = $featured_price;
		$prices['needed_credits'] = $needed_credits;
		$prices['image_credit_selected'] = $add_pictures_selected_index;
		$prices['featured_type'] = $selected_feature;
        $prices['nrfiles_images'] = $nrfiles_images;
        $prices['is_main_pic'] = $is_main_pic;

        return $prices;
					
	}

    /**
     * buildLetterFilter
     *
     * @static
     *
     * @param $filter_cat
     *
     * @internal param $url
     *
     * @return string
     */
   /* static function buildLetterFilter($filter_cat)
    {

        $letters = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
        $filter_letter = JRequest::getString('filter_letter', 'all');

        $letters_filter = "<div id='box_letters_filter'>";
        foreach ($letters as $letter) {

            $active = strtolower($filter_letter) == strtolower($letter) ? 'active' : '';
            $letters_filter .= "<a href='" . AdsmanHelperRoute::getCategoryRoute($filter_cat, $letter) . "' class='" . $active . "'>" . $letter . "</a>";
        }
        // List all categories
        $letters_filter .= "<a href='" . AdsmanHelperRoute::getCategoryRoute($filter_cat) . "'>" . JText::_('COM_ADS_RESET_FILTER') . "</a>";
        $letters_filter .= "</div>";

        return $letters_filter;
    }*/
	static function getMapsXML($rows)
    {
		$Itemid = AdsmanHelperRoute::getMenuItemByTaskName("listadds");
		// Start XML file, create parent node
		$dom = new DOMDocument("1.0");
		$node = $dom->createElement("markers");
		$parnode = $dom->appendChild($node);
		
		// Iterate through the rows, adding XML nodes for each
		foreach ($rows as $row){
		  $catlink=JRoute::_( 'index.php?option=com_adsman&amp;task=listadds&amp;cat='.$row->category.'&amp;Itemid='. $Itemid);
          if (ads_opt_transliterate == 1) {
                $title_sef = JFilterOutput::stringURLSafe(AdsUtilities::transliterate($row->title));
          } else {
                $title_sef = JFilterOutput::stringURLUnicodeSlug($row->title);
          }
          $adlink=JRoute::_('index.php?option=com_adsman&task=details&title='.$title_sef.'&id='. $row->id .'&Itemid='. $Itemid );
		  $node = $dom->createElement("marker");
		  $newnode = $parnode->appendChild($node);
		  $newnode->setAttribute("name", $row->name);
		  $newnode->setAttribute("info", "<div class='infoWindow'><span><a href='{$catlink}' class='category_link'>{$row->catname}</a></span>
                                            <br />
                                            <span class='title'><a href='{$adlink}'>{$row->name}</a></span>
                                            ".($row->picture?("<img src='".JURI::root()."media/com_adsman/images/resize_".$row->picture."'>"):"")."
                                            <div class='adsman_shortdescription'>{$row->short_description}</div>
                                            </div>
                                            ");
		  $newnode->setAttribute("lat", $row->lat);
		  $newnode->setAttribute("lng", $row->lng);
		  $newnode->setAttribute("link", JRoute::_('index.php?option=com_adsman&task=details&id='. $row->id .'&Itemid='. $Itemid ));
		  $newnode->setAttribute("img", JURI::root()."media/com_adsman/images/resize_".$row->picture);
		  $newnode->setAttribute("distance", $row->distance);
		  $newnode->setAttribute("id", $row->id);
          
		}
		
		return $dom->saveXML();
		
        
    }
}

class adsTags extends JTable {
    var $id=null;
    var $parent_id=null;
    var $tagname=null;

    function adsTags(&$db){
		$this->__construct( '#__ads_tags', 'id', $db );
    }
    
    function setTags($parent_id,$tags){

        $this->_db->setQuery("DELETE FROM ".$this->_tbl." WHERE parent_id='$parent_id'");
        $this->_db->query();

        $tag_arr=explode(',',$tags);

        for ($i=0; $i<min(count($tag_arr),ads_opt_max_nr_tags); $i++){
            $this->id=null;
            $this->parent_id=$parent_id;
            $this->tagname=trim($tag_arr[$i]);
            if( $this->tagname !="")
            	$this->store();
        }
    }
    function getTags($parent_id, $string_type=1)
    {
    	$this->_db->setQuery("SELECT tagname FROM ".$this->_tbl." WHERE parent_id='".$parent_id."' ORDER BY id");
    	$tmp = $this->_db->loadObjectList("tagname");
		$tagList = array();
    	foreach ( $tmp as $key=> $value)
    		$tagList[] = $key;
		return $tagList;
    }


}

class AdImageUtilies {
	
	function AdImageUtilities(){

	}
	
	function loadImagePlugin(){
		$database = JFactory::getDbo();
		$gallery_name = "gl_".ads_opt_gallery;
		
		require_once(ADS_COMPONENT_PATH.DS."gallery".DS."$gallery_name.php");
		$gallery = new $gallery_name($database,JURI::root()."media/com_adsman/images/");
		return $gallery; 
	}

}

class Ads_Debug {
	
	function showTicker($label="Ads", $exit=false){
		if(ADDSMAN_DEBUG==1){
			$db = JFactory::getDbo();
			echo "<div style='color:#FFA000;'>$label: <strong>{$db->_ticker}</strong> </div>";
			if($exit)
				exit();
		}
	}
	
	function resetLog(){
		if(ADDSMAN_DEBUG==1){
			$db = JFactory::getDbo();
			$db->_log=array();
			$db->_ticker=0;
		}
	}
	
	function getSQL($label="Ads",$all=false, $exit=false){
		if(ADDSMAN_DEBUG==1){
			$db = JFactory::getDbo();
			if($all)
				echo "<div style='color:#FFA000; width:350px !important;'>$label: <strong>".Ads_Debug::var_export($db->_log)."</strong> </div>";
			else
				echo "<div style='color:#FFA000; width:350px !important;'>$label: <strong>{$db->_sql}</strong> </div>";
			if($exit)
				exit();
		}
	}
	
	function var_dump($to_dump, $exit=false){
		if(ADDSMAN_DEBUG==1){
			echo "<pre>";
			var_dump($to_dump,true);
			echo "</pre>";
			if($exit)
				exit();
		}	
	}

	function print_r($to_dump, $exit=false){
		if(ADDSMAN_DEBUG==1){
			echo "<pre>";
			print_r($to_dump);
			echo "</pre>";
			if($exit)
				exit();
		}	
	}
	
	function var_export($to_dump){
		if(ADDSMAN_DEBUG==1){
			$ret = "<pre>";
			$ret .= var_export($to_dump,true);
			$ret .= "</pre>";
			return $ret;
		}	
	}
	
	function break_point($label="Break here!",$exit=0){
		if(ADDSMAN_DEBUG==1){
			echo "<strong style='color:#FF0000;'>$label</strong>";
			if($exit==1){
				exit;
			}
		}
	}
	
	function backtrace($step=null){
		if(ADDSMAN_DEBUG==1){
			$bk = debug_backtrace(false);
			if($step)
				echo Ads_Debug::var_export($bk[$step]);
			else echo Ads_Debug::var_export($bk);
		}
	}
	
	
}

class Ads_Time {
	
	function getNow(){
		
		$config = JFactory::getConfig();
		$offset = $config->getValue("offset");

		if ( defined("ads_opt_component_offset") && ads_opt_component_offset!="" )
			$offset = ads_opt_component_offset;
			
		$timestamp = strtotime(gmdate("M d Y H:i:s", time()));
		
		return $timestamp + 3600*$offset;
	}
	
	function getNowSql(){
		$jnow		=& JFactory::getDate();
		return  $jnow->toMySQL();
	}
	
	function apendDays($time, $nrOfdays) {
		
		return $time + 86400*$nrOfdays;
		
	}
	
	function corelate($datetime){

		$config = & JFactory::getConfig();
		$offset = $config->getValue("offset");
		
		if ( defined("ads_opt_component_offset") && ads_opt_component_offset!="" )
			$offset = ads_opt_component_offset;
				
		return $datetime + 3600 * $offset;
	}
	
	/**
	 * Returns a MySql Database Formated timestamp
	 *
	 * @param $time timestamp
	 * @return date
	 */
	function getDateSql($time){
		$jdate = & JFactory::getDate($time);
		return  $jdate->toMySQL();
	}

	function getTimeStamp($date,$corelate=1){
	
		$result = null;
		
		if (preg_match('~(\\d{4})-(\\d{2})-(\\d{2})[T\s](\\d{2}):(\\d{2}):(\\d{2})(.*)~', $date, $matches))
		{
			
			$result = gmmktime(
				$matches[4], $matches[5], $matches[6],
				$matches[2], $matches[3], $matches[1]
			);
		}
		
		
		if($corelate==1)
			return 	Ads_Time::corelate($result);
		else 
			return 	$result;
	}
	
}

class Ads_FilterInput_Helper {
	
	function cleanText(&$varText, $filterType=null,$params=null){
		
		$def_tagBlacklist = array ('applet', 'body', 'bgsound', 'base', 'basefont', 'frame', 'frameset', 'head', 'html', 'iframe', 'ilayer', 'layer', 'link', 'meta', 'name', 'script', 'style', 'xml');
		$def_attrBlacklist = array ('action', 'background', 'codebase', 'dynsrc', 'lowsrc'); // also will strip ALL event handlers
		
		if(isset($params['filter_tags']))
			$filterTags		= preg_split( '#[,\s]+#', trim( @$params['filter_tags'] ) );
		else	
			$filterTags	= $def_tagBlacklist;

		if(isset($params['filter_attritbutes']))
			$filterAttrs	= preg_split( '#[,\s]+#', trim( @$params['filter_attritbutes'] ) );
		else	
			$filterAttrs	= $def_attrBlacklist;
		
		switch ($filterType)
		{
			case 'NH':
				$filter	= new JFilterInput();
				break;
			case 'WL':
				$filter	= new JFilterInput( $filterTags, $filterAttrs, 0, 0 ,0 );
				break;
			case 'BL':
			default:
				$filter	= new JFilterInput( $filterTags, $filterAttrs, 1, 1 ,0);
				break;
		}
		$varText	= $filter->clean( $varText, "" );
	}
	
}

class Ads_HTML {
	
	function selectCategory( $name, $params=array() ){
		
		$cat = new JTheFactoryCategoryObj();
		$cat->show_active_only = true;
		
		$filter_category = null;
		if(isset($params["select"]) && (int)$params["select"]!=0)
			$filter_category = $params["select"];

		return $cattree = $cat->HTMLCatTree($name,$filter_category, $params);
	}
	
}

?>