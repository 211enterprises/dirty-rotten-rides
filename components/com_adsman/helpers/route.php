<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.helper');
$app = JFactory::getApplication();

if (!$app->isAdmin() && !defined('APP_CATEGORY_TABLE')){
	define('APP_CATEGORY_TABLE','#__ads_categories');
	define('APP_CATEGORY_DEPTH','2');
	require_once(JPATH_ROOT.DS.'components'.DS.'com_adsman'.DS.'thefactory'.DS.'front.helper.php');
	JLoader::register('JTheFactoryCategoryTable',JPATH_ADMINISTRATOR.DS.'components'.DS.'com_adsman'.DS.'thefactory'.DS.'category'.DS.'table.category.php');
	JLoader::register('JTheFactoryCategoryObj',JPATH_ADMINISTRATOR.DS.'components'.DS.'com_adsman'.DS.'thefactory'.DS.'category'.DS.'category.core.php');
}


/**
 * AdsMan Component Route Helper
 *
 * @static
 * @package		Ads Factory
 * @subpackage	Router
 * @since 1.5.0
 */
class AdsmanHelperRoute
{
	
	
	function getSEFCatString(&$id){
		
		$cat = new JTheFactoryCategoryObj();
		$pm = $cat->string_cat_name($id,0);
		return $pm;
	}
	
	function getSEFCat(&$catid,&$subcatid) {
		$cat = new JTheFactoryCategoryObj();
		return $cat->getSEFCat($catid,$subcatid);
	}

	/**
	 * Get the Parameter Task ItemID 
	 *  	if more than one menu items of same task, returns the first
	 *
	 * @param  $task_name
	 * @return int
	 */
	function getMenuItemByTaskName($task_name){
		$database = JFactory::getDbo();
		$database->setQuery("SELECT id FROM #__menu WHERE link LIKE '%task=$task_name%' OR '%layout=$task_name%' ORDER BY id DESC LIMIT 0 , 1 ");
		return $database->loadResult();
	}

	/**
	 * Finds The Menu Item Of the Component 
	 *  by the needles as params
	 *
	 * needle example: 'view' => 'category'
	 * 
	 * 
	 * @param array $needle
	 * @since 1.5.0
	 */
	function getMenuItemId( $needles ){
		$component =& JComponentHelper::getComponent('com_adsman');

		$menus	= &JApplication::getMenu('site', array());
		$items	= $menus->getItems('componentid', $component->id);
		
		$match = null;
		
		foreach($items as $item)
		{
			
			$ok = true;
			foreach($needles as $needle => $id)
			{
				if ( @$item->query[$needle] != $id ) {
					
					$ok = false;
					break;
				}
			}
			if($ok==true){
				$match=$item;
				break;
			}
		}
		
		if (isset($match)) {
			
			return $match->id;
			
		}else 
			return null;
	}
	
}


