<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/


// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport('joomla.application.component.controller');

/**
 * Controller Ads Factory
 *
 * @package		AdsFactory
  */
class AdsController extends JController
{
	var $_name='adsman';
	
// Ads tasks ==> 	adsman model; adsman view
	function details() {
		$view = $this->setViewParams(JRequest::getVar('view', 'adsman', 'default', 'cmd'));
		$view->details();
	}
	
	function extend_add() {
		$Itemid = JRequest::getVar('Itemid', 0, 'REQUEST', 'integer');
		$database = JFactory::getDbo();
		$id = JRequest::getVar('id','');
		
		$add = new JAdsAddsman($database);
		$add->load($id);
	    
	    if ($add->extend > 0 && defined("ads_opt_extend_days") && ads_opt_extend_days > 0 ) {
	    	
			$end_date = Ads_Time::corelate(strtotime($add->end_date));
			$new_end_date = gmdate(ads_opt_date_format, mktime(null,null,null,date("n",$end_date),date("j",$end_date)+ads_opt_extend_days,date("Y",$end_date)));
			
			$add->end_date = AdsUtilities::addDatetoIso($new_end_date);
			
			$add->closed = 0; 
			$add->closed_date = null; 
			$add->extend--; 
			
			$add->store();
	    	
			$this->setRedirect("index.php?option=com_adsman&view=adsman&task=details&id=$id&Itemid=$Itemid", JText::_("ADS_EXTENDED_AD"));
	    } else
	    	$this->setRedirect("index.php?option=com_adsman&view=adsman&task=details&id=$id&Itemid=$Itemid", JText::_("ADS_EXTENDED_NOT"));
	}
	
	function selectcat()
	{
		$Itemid = JRequest::getVar('Itemid', 0, 'REQUEST', 'integer');
	    $id     = JRequest::getVar('id','');
	    
		if(!$id && AdsUtilities::testPosterLimit())
			$this->setRedirect(JURI::root()."index.php?option=com_adsman&Itemid=$Itemid", JText::_("ADS_LIMIT_REACHED"));
        
		if ($id && !AdsUtilities::isMyAdd($id))
        {
            JError::raiseWarning(1,JText::_("ALERTNOTAUTH"));
            return;
        }
        
		$view = $this->setViewParams(JRequest::getVar('view', 'adsman', 'default', 'cmd'));
		$view->catselect();
	}
	
	function edit()
	{
		$Itemid = JRequest::getVar('Itemid', 0, 'REQUEST', 'integer');
		$app    = JFactory::getApplication();
		
		$document = JFactory::getDocument();
		$db = JFactory::getDbo();
		
		$id= JRequest::getVar('id',''); 
		
		if( !$id && AdsUtilities::testPosterLimit() )
			$this->setRedirect(JURI::root()."index.php?option=com_adsman&Itemid=$Itemid" ,JText::_("ADS_LIMIT_REACHED"));
		
		if ( !defined("ads_opt_logged_posting") || ads_opt_logged_posting==0  ){
	        if ( $id && !AdsUtilities::isMyAdd($id) ){
	            JError::raiseWarning(1,JText::_("ALERTNOTAUTH"));
	            return;
	        }
		}
		
		if( !JTheFactoryUserProfile::hasProfile(ads_opt_profile_mode) && (defined("ads_opt_profile_required") && ads_opt_profile_required==1) ){
			if (ads_opt_profile_mode=="ads") {
				
				$userItemId = AdsmanHelperRoute::getMenuItemId(array("view"=>"user","task"=>"myuserprofile"));
				
				if(!$userItemId)
					$userItemId = AdsmanHelperRoute::getMenuItemId(array("view"=>"user","layout"=>"","task"=>""));
				
				if(!$userItemId)
					$userItemId = $Itemid;
				
				$r = JURI::root()."index.php?option=com_adsman&view=user&task=myuserprofile&Itemid={$userItemId}";
			} else
				$r = AdsUtilities::redirectToProfile();
				$app->redirect($r, JText::_("ADS_ERR_USER_DETAILS") );
		}
				
		$view = $this->setViewParams(JRequest::getVar('view', 'adsman', 'default', 'cmd'));

		if (ads_opt_category_page) {
			$db->setQuery("SELECT * FROM #__ads_categories"); 
			$db->query();
			$numRows = $db->getNumRows();
		}
		
		$session	= &JFactory::getSession();

		if( !$session->has("tmp_ad", "tmpadserialized") && !$id && ads_opt_category_page && $numRows>0 ) {
			
			$post = JRequest::getVar('category_hit','');
			$category = JRequest::getVar('category','');
			
			if ($post!="" && $category!="") {
				JRequest::setVar('custom_fields_category',$category);
				$view->edit();
			}
			else{
				if($post!="" && $category=="")
					JError::raiseWarning(1, JText::_("ADS_SELECT_CATEGORY"));
				$view->catselect();
			}
		} else {

			//$session->clear("tmp_ad","tmpadserialized");
			$view->edit();
		}
	}

	function canceladd() {
		$Itemid = JRequest::getVar('Itemid', 0, 'REQUEST', 'integer');
        /*@var $model adsModelAdsman*/
        if (!AdsUtilities::isMyAdd(JRequest::getVar('id','')))
        {
            JError::raiseWarning(1,JText::_("ALERTNOTAUTH"));
            return;
        }
		$id = JRequest::getVar('id',null);
		if($id){
			$model = &$this->getModel( 'Adsman' ,'adsModel');
			$err = $model->cancelAdd();
			$this->setRedirect("index.php?option=com_adsman&view=adsman&task=details&id=$id&Itemid=$Itemid",$err);
		}
	}

	function publish(){
		$Itemid = JRequest::getVar('Itemid', 0, 'REQUEST', 'integer');
        if (!AdsUtilities::isMyAdd(JRequest::getVar('id','')))
        {
            JError::raiseWarning(1,JText::_("ALERTNOTAUTH"));
            return;
        }
		AdsUtilities::changeContent(1);
		$refferer = $_SERVER['HTTP_REFERER'];
		if($refferer)
			$this->setRedirect($refferer);
		else
			$this->setRedirect("index.php?option=com_adsman&view=adsman&task=listadds&Itemid=$Itemid");
	}

	
	function downloadfile(){
		@ob_end_clean();
		$id   = JRequest::getVar('id',null);
		$add = new JAdsAddsman(JFactory::getDBO());
		$add->load($id);
		if($add->atachment){
			$path = ADDSMAN_IMAGE_PATH."attach_{$id}.fil";
			header('Content-Disposition: attachment; filename="'.$add->atachment.'";');
			$fHandle = fopen($path,"r");
			while ( $buffer = fread($fHandle, 4096) ) {
				echo $buffer;
			}
	      	ob_start(false); // prevent unwanted output
	      	exit;
		}
	}

	function save() {
		
		$Itemid     = JRequest::getVar('Itemid', 0, 'REQUEST', 'integer');
		$app 		= JFactory::getApplication();
		$database 	= JFactory::getDbo();
		$session	= JFactory::getSession();
		$my 		= JFactory::getUser();

        $session->set("session.token", null);
		$session->set("token", null);
        $session->clear("posted_pm");
        $session->clear("controller_status");
        $session->clear('packages_purchase_pm');

		$id = JRequest::getVar('id',null);
        $video_url = JRequest::getVar('video-link', '');

		$add = new JAdsAddsman($database);
		$model = &$this->getModel( 'Adsman','adsModel' );

        if($id) {
            $add->load($id);
        }

        $credits = generic_pricing::getCredits($my->id);
        $prices = AdsUtilities::add_credits_calculate($id);
        
        $total_price_ad = $prices['total_price_ad'];
        $listing_price  = $prices['listing_price'];
        $main_img_price = $prices['main_img_price'];
        $pictures_price = $prices['pictures_price'];
        $featured_price = $prices['featured_price'];
        $needed_credits = $prices['needed_credits'];
        $selected_feature = $prices['featured_type'];
        $ad_images_price = 0;
        $ad_featured_price = 0;

        $isListingEnabled = price_listing_DBHelper::getEnabledPayListing();
        $isImageEnabled = price_pay_image_DBHelper::getEnabledPayImage();
        $featured = JRequest::getVar('featured', null);
        $img_prices = price_pay_image_DBHelper::getPayImageAmount();

        if ($isImageEnabled && !$id) {
            $nrfiles_images =  $prices['nrfiles_images']; // how many img were uploaded
            $is_main_pic = $prices['is_main_pic']; // if main img uploaded

            if ( ($is_main_pic || ($nrfiles_images > 0)) &&  (($pictures_price != 0) || ($main_img_price != 0)) && ($credits < $total_price_ad) ) {
                $session->set('controller_status','unpublished');
            }
        }

        //check if featured add
        if (isset($featured) && $featured_price != 0 ) {
            if (!$id) {
                if ( ($featured_price != 0) &&  ($credits < $total_price_ad) ) {
                    $session->set('controller_status','unpublished');
                }
            } else {
                // from db = new setted feature + calculate price for edit ad + features
                if (isset($featured)) {
                    if (($add->featured != $featured) || ($add->status == 0))  {
                        $ad_featured_price = $featured_price;
                    }
                }
            }
        }

        $status = JRequest::getVar('status', 0);

        if (!$id && $session->get("controller_status") == null) {
            $session->set('controller_status',ads_opt_adpublish_val);
        } else {
            if ($id) {
                $session->set('controller_status',$status);
            }
        }

        if ($isImageEnabled && $id) {

            $orig_has_main_image = ($add->picture != null) ? 1 : 0; // had main img
            $delete_main_picture = JRequest::getVar('delete_main_picture', '');
            $count_replace_main = AdsUtilities::getMainImgReplace($id);

            $orig_images_count = $add->getCountImages(); // initial no if img
            $count_deleted_images = $add->getCountDeletedImages();
            $delete_pictures = JRequest::getVar('delete_pictures', null, 'POST', 'array' );
            $selected_to_delete = count($delete_pictures);

            $count_replace_images = number_format($selected_to_delete,0) + number_format($count_deleted_images,0);

            $delete_video = JRequest::getVar('delete_video', '');
        }

		$model->bindAdd( $add );
	    	    
		if( defined("ads_opt_logged_posting") && ads_opt_logged_posting==1  ){
			if( ads_opt_enable_captcha && !$my->id){
				if(!AdsUtilities::verify_captcha()  && !$id){
					$session->set("tmp_ad", serialize($add) ,"tmpadserialized" );
					$app->redirect("index.php?option=com_adsman&view=adsman&task=new&Itemid=$Itemid", JText::_("ADS_ERROR_CAPTCHA"));
					return;
				}		
			}
		}else{
			if ($id && !AdsUtilities::isMyAdd($id)){
				JError::raiseWarning(1,JText::_("ALERTNOTAUTH"));
				return;
			}
		}
		
		$err = $model->saveAdd( $add );

        if ($isImageEnabled && $id) {  //if $id = EDIT AD
                // calculate price for edit ad + images
                $count_images = $add->getCountImages();
                $nrfiles_images =  $prices['nrfiles_images']; // how many img were uploaded
                $is_main_pic = $prices['is_main_pic']; // if main img uploaded

                if ( ($add->status == 1 && $count_replace_main == 1) || ($delete_main_picture) ) {
                    $main_img_price = 0;
                } else {

                    if ( (!$orig_has_main_image && $is_main_pic ) || ($orig_has_main_image && $add->status == 0) ) {

                        $main_img_price = price_pay_image_DBHelper::getCreditImg(0);
                    }
                }
                //$images_uploaded = number_format($nrfiles_images,0) - number_format($orig_images_count,0);
                if ($count_images && $add->status == 0) {
                    //if ($orig_images_count && $add->status == 0) {
                    //$pictures_price = price_pay_image_DBHelper::getCreditImg($count_images);
                    $pictures_price = $img_prices[$count_images];
                    //$pictures_price = price_pay_image_DBHelper::getCreditImg($orig_images_count);
                } elseif ($nrfiles_images > 0 && $nrfiles_images > $count_replace_images) {

                    $total_images = $nrfiles_images - $count_replace_images;

                    if ($orig_images_count && $add->status == 0) {
                        $total_images = $nrfiles_images + $orig_images_count;
                    }

                    $pictures_price = $img_prices[$total_images];

                } else {
                    // no pay, just replace payed images
                    $pictures_price = 0;
                }

                $ad_images_price = $main_img_price + $pictures_price;
        }

        $edit_total_price_ad = $ad_images_price + $ad_featured_price;
        $total_price_ad = ($id == null) ? $prices['total_price_ad'] : $edit_total_price_ad;

        if( $err !== true ) {	
			
			$session->set("tmp_ad", serialize($add) ,"tmpadserialized" );
			$this->setRedirect("index.php?option=com_adsman&view=adsman&task=new&Itemid=$Itemid",$err);
			
		} else {
			if ($add->id) {
				// after ad is saved, save video url too


				if (isset($video_url) && $video_url != '' && !$delete_video) {
					$saved_url = $model->saveUrls($add->id,$video_url);
										
					if( $saved_url !== true ) {	
						$session->set("tmp_ad", serialize($add) ,"tmpadserialized" );
						$this->setRedirect("index.php?option=com_adsman&view=adsman&task=new&Itemid=$Itemid",$err);
					}
				}

				if ($credits >= $total_price_ad) {
                    $database->setQuery("UPDATE #__ads_user_credits SET credits_no=credits_no - ".$total_price_ad." WHERE userid='$my->id'");
					$database->query();

                    // UPDATE OBJECT
                    if ( ads_opt_adpublish_enable == '0') {

                        $status = ads_opt_adpublish_val;

                        $database->setQuery("UPDATE #__ads SET status='$status' where id='$add->id'");
		                $database->query();
                    } else {
                            $database->setQuery("UPDATE #__ads SET status='$add->status' where id='$add->id'");
                            $database->query();
                    }

                    $edit_24hours_info = '';
                    if ( (isset($featured) && $featured_price != 0) || $isListingEnabled) {
                        $edit_24hours_info = JText::_('ADS_EDIT_24HOURS');
                    }

					$session->clear("tmp_ad","tmpadserialized");
					$app->redirect( JURI::root()."index.php?option=com_adsman&view=adsman&Itemid=$Itemid&task=details&id=" .$add->id, JText::_("ADS_SAVED"). ' <br />'.$edit_24hours_info );
					
		        } else {
                    $d	= $_REQUEST;

                   	$d["id"] 		= $add->id;
                    $d["category"] 	= $add->category;
					$d['needed_credits'] = $needed_credits;
					$d['total_price_ad'] = $total_price_ad;
					$d['featured'] = $selected_feature;
					$d['itemname'] = 'maincredit';
                    $d["status"] 	= $add->status;

                    $session->set('posted_pm',$d);
                    $session->set("payment_listing_dialog", null);


                    $packages_purchase_pm['featured_ad'] = (isset ($d['featured']) ) ? $d['featured'] : 'none';
                    $packages_purchase_pm['total_price_ad'] = $total_price_ad;
                    $packages_purchase_pm['needed_credits'] = $needed_credits;
                    $packages_purchase_pm['ad_id'] = $add->id;

                    $session->set('packages_purchase_pm',$packages_purchase_pm);

		       		generic_pricing::ShowPurchaseDialog($d);
			        return true;
		       	}
				
			}
			else
				$this->setRedirect("index.php?option=com_adsman&view=adsman&task=new&Itemid=$Itemid", JText::_("ADS_ERR_SAVE") );
		}
	}

	
	function listadds() {
		
		$view = $this->setViewParams(JRequest::getVar('view', 'adsman', 'default', 'cmd'));
		$view->listadd();
	}

	function myadds() {
		$view = $this->setViewParams(JRequest::getVar('view', 'adsman', 'default', 'cmd'));
		$view->listadd(1);
	}

	function addtofav(){
		$Itemid = JRequest::getVar('Itemid', 0, 'REQUEST', 'integer');
		$app = JFactory::getApplication();
        if (!AdsUtilities::isMyAdd(JRequest::getVar('id','')))
        {
    		$model = &$this->getModel( 'Adsman' ,'adsModel');
    		$model->addToFav();
        }

        $app->enqueueMessage(JText::_("ADS_ADDED_TO_FAVORITES"));
        
		$refferer = $_SERVER['HTTP_REFERER'];
		if($refferer)
			$this->setRedirect($refferer);
		else
			$this->setRedirect( "index.php?option=com_adsman&view=adsman&task=favorites&Itemid=$Itemid");
	}

	function delfav(){
		$Itemid = JRequest::getVar('Itemid', 0, 'REQUEST', 'integer');
		$app = JFactory::getApplication();
		$model = &$this->getModel( 'Adsman' ,'adsModel');
		$model->delfav();
		
        $app->enqueueMessage(JText::_("ADS_REMOVED_FROM_FAVORITES"));
        
		$refferer = $_SERVER['HTTP_REFERER'];
		if($refferer)
			$this->setRedirect($refferer);
		else
			$this->setRedirect("index.php?option=com_adsman&view=adsman&task=favorites&Itemid=$Itemid");
	}

	function favorites() {
		$view = $this->setViewParams(JRequest::getVar('view', 'adsman', 'default', 'cmd'));
		$view->listadd(0,1);
	}

	function sendMessage() {
		$Itemid = JRequest::getVar('Itemid', 0, 'REQUEST', 'integer');
		$id   = JRequest::getVar('id',null);
		$my  = JFactory::getUser();

		$reply_toid   = JRequest::getVar('to_reply',null);
		$reply_message_textarea   = JRequest::getVar('message_textarea_reply',null);
			
		if(ads_opt_enable_captcha && !$my->id){
			if(!AdsUtilities::verify_captcha()){
				$this->setRedirect("index.php?option=com_adsman&view=adsman&task=details&Itemid=$Itemid&id=".$id, JText::_("ADS_ERROR_CAPTCHA"));
				$this->redirect();
				return;
			}		
		}
		
		$model = &$this->getModel( 'Adsman' ,'adsModel');
		$success = $model->sendMessage($reply_toid,$reply_message_textarea);
		
		if($success===true)
			$this->setRedirect("index.php?option=com_adsman&view=adsman&task=details&Itemid=$Itemid&id=".$id, JText::_("ADS_MESSAGE_SENT"));
		else{
			$this->setRedirect("index.php?option=com_adsman&view=adsman&task=details&id=".$id,$success);
		}
	}

	function report_show() {
		$my  = JFactory::getUser();
		
		if( !$my->id ){
			echo JText::_("ADS_ERR_LOGIN_FIRST");
			exit;
		}
		
		$view = $this->setViewParams(JRequest::getVar('view', 'adsman', 'default', 'cmd'));
		$view->report();
	}

	function terms() {
		$view = $this->setViewParams(JRequest::getVar('view', 'adsman', 'default', 'cmd'));
		$view->terms();
	}

	function report_add() {
		$id   = JRequest::getVar('id',null);
		$model = &$this->getModel( 'Adsman' ,'adsModel');
		$success = $model->report($id);
		if($success===true)
			$this->setRedirect("index.php?option=com_adsman&view=adsman&task=details&id=".$id, JText::_("ADS_AD_REPORTED"));
		else{
			$this->setRedirect("index.php?option=com_adsman&view=adsman&task=details&id=".$id, $success);
		}
	}

// <== Ads tasks

// ==> Search tasks

	function show_search() {
		$view = $this->setViewParams(JRequest::getVar('view', 'adsman', 'show_search', 'cmd'));
		$view->show_search();
	}

// <== Search tasks

// ==> Categories tasks
	function listcats() {
		$view = $this->setViewParams(JRequest::getVar('view', 'adsman', 'listcats', 'cmd'));
		$view->listcats();
	}

	function addwatchcat(){
		$Itemid = JRequest::getVar('Itemid', 0, 'REQUEST', 'integer');
		$id = JRequest::getVar('cat',null);
		$model = &$this->getModel( 'Adsman' ,'adsModel');
		if($model->addWatch($id))
			$this->setRedirect( "index.php?option=com_adsman&view=adsman&task=listcats&Itemid=$Itemid");
	}

	function delwatchcat(){
		$Itemid = JRequest::getVar('Itemid', 0, 'REQUEST', 'integer');
		$id = JRequest::getVar('cat',null);
		$model = &$this->getModel( 'Adsman' ,'adsModel');
		if($model->delWatch($id))
			$this->setRedirect( "index.php?option=com_adsman&view=adsman&task=listcats&Itemid=$Itemid");
	}
// <== Categories tasks

// User tasks ==> user model; user view
	function myuserprofile()
	{
		$Itemid = JRequest::getVar('Itemid', 0, 'REQUEST', 'integer');
		$r = AdsUtilities::redirectToProfile();
		if($r)
			$this->setRedirect( $r );


		$document 	= JFactory::getDocument();
		$viewType	= $document->getType();
		$model		= &$this->getModel( 'User','adsModel' );

		$view = &$this->getView('user', $viewType);
		$view->display("myuserdetails");
	}
    
	function googlemap(){
		$smarty = AdsUtilities::SmartyLoaderHelper();
		$x   = JRequest::getVar('x',null);
		$y   = JRequest::getVar('y',null);
        $addid = JRequest::getVar('id',null);

        if($addid){
            $add = new JAdsAddsman(JFactory::getDBO());
            $add->load($addid);
            $smarty->assign("add", $add);
        }
        JHTML::_("adsmanmap.js","map_canvas","listmap","gmap_centermap({pointTox:$x,pointToy:$y});");
        
		$smarty->assign("_googleX", $x);
		$smarty->assign("_googleY", $y);
		$smarty->display("t_googlemap.tpl");
 	}

	function userprofile()
	{
		$Itemid = JRequest::getVar('Itemid', 0, 'REQUEST', 'integer');

		$id = (int)JRequest::getVar('id',null);

		$r = AdsUtilities::redirectToProfile($id);
		if($r)
			$this->setRedirect( $r );


		$document 	=& JFactory::getDocument();
		$viewType	= $document->getType();
		$model		= &$this->getModel( 'User' ,'adsModel');

		$view = &$this->getView('user', $viewType);
		$view->display("userdetails");
	}

	function saveUserDetails(){
		$Itemid = JRequest::getVar('Itemid', 0, 'REQUEST', 'integer');
		$model = &$this->getModel( 'User' ,'adsModel');
		$err = $model->saveUserDetails();
		$msg = null;
		if($err!==true)
			$msg = $err;
		else 
			$msg = JText::_("ADS_USER_DETAILS_SAVED");
			$this->setRedirect( JURI::root().'/'."index.php?option=com_adsman&view=user&task=myuserprofile&Itemid=$Itemid", $msg );
	}
// <== User details

	function cat_select(){
		$view = $this->setViewParams(JRequest::getVar('view', 'adsman', 'listcats', 'cmd'));
		
		$view->catselect();
	}

	function setViewParams($_viewName){
		$document 	= JFactory::getDocument();
		$viewType	= $document->getType();
		$view 	= $this->getView($_viewName, $viewType);
		
		$model	= $this->getModel( "adsman",'adsModel' );
		
		if (!JError::isError( $model )) {
			$view->setModel( $model, true );
		}
		
		$model_user	= $this->getModel( "user",'adsModel' );
		if (!JError::isError( $model_user )) {
			$view->setModel( $model_user );
		}
		
		$view->getModel("adsman");
		//$view->_defaultModel = "adsman";
		return $view;
	}

	/**
	 * @since: 1.5.5
	 *
	 */
	function tellFriend_show(){
		$view = $this->setViewParams(JRequest::getVar('view', 'adsman', 'tellFriend_show', 'cmd'));
		
		$view->tellFriend_show();
	}
	
	/**
	 * @since: 1.5.5
	 *
	 */
	function tellFriend(){
		
		$Itemid = JRequest::getVar('Itemid', 0, 'REQUEST', 'integer');
		$app = JFactory::getApplication();
		
		$db     = JFactory::getDbo();
		$view   = $this->setViewParams(JRequest::getVar('view', 'adsman', 'tellFriend_show', 'cmd'));
		$my     = JFactory::getUser();
		
		$id   = JRequest::getVar('id',null,'default','int');
		if ( !$id ){
			echo JText::_("ADS_ADD_NOT_FOUND"); return;
		}
		
		if(!JRequest::checkToken() && ADDSMAN_DEBUG!=1){
			$this->setRedirect( "index.php?option=com_adsman&view=adsman&task=tellFriend_show&tmpl=component&Itemid=$Itemid&id=$id" , "Token Error");
			return;
		}
		
		if(ads_opt_enable_captcha && !$my->id){
			if(!AdsUtilities::verify_captcha()){
				$this->setRedirect( "index.php?option=com_adsman&view=adsman&task=tellFriend_show&tmpl=component&Itemid=$Itemid&id=$id", JText::_("ADS_ERROR_CAPTCHA"));
				return;
			}		
		}
		$friend_email	= JRequest::getVar('friend_email', '', 'default', 'string');
		
        $add = new JAdsAddsman($db);
        $add->load($id);
		
		$user = new stdclass;
		$user->email = $friend_email;
		$add->sendMails(array($user),"add_send_to_friend");
		
		echo JText::_("ADS_TELL_FRIEND_SUCCESS");
	}
	
/**
 * @since 1.5.6
 *
 */
	function showBookmarks(){
		
		error_reporting(0);
		
		$cat = (int)JRequest::getVar('cat',0);
		$start = (int)JRequest::getVar('limitstart',0);
		$limit = (int)JRequest::getVar('limit',20);
		
		if($limit==0){
			$limit_sql = "";
		} else {
			$limit_sql = "LIMIT $start,$limit";
		}
		
		$db = JFactory::getDbo();
		
		$where = array();
		$where[] 	= " `a`.`close_by_admin` = 0 ";
		$where[] = " a.closed = 0 ";
		$where[] = " a.status = 1 ";
		$where[] = " MapX != '' ";
		$where[] = " MapY != '' ";
				
		if($cat!=0)
			$where[] = " a.category = '{$cat}' ";
		
		// Search the rows in the markers table
		$db->setQuery("
		SELECT a.id, a.short_description, a.title as name, a.category,a.picture,
			c.catname,
			MapX as lat, MapY as lng 
		FROM #__ads a 
		LEFT JOIN `#__ads_categories` c ON a.category = c.id 
		WHERE 
		".
			implode("AND",$where)
		."
		$limit_sql ");

		$rows = $db->loadObjectList();
		ob_end_clean();
		header("Content-type: text/xml");
        echo AdsUtilities::getMapsXML($rows);
		exit;
	
	}

/**
 * @since 1.5.6
 *
 */
	function searchBookmarks(){
		
		error_reporting(0);
		$cat = (int)JRequest::getVar('cat',0);

		$center_lat = (JRequest::getVar('lat',0));
		$center_lng = (JRequest::getVar('lng',0));
		$radius = (int)JRequest::getVar('radius',0);

		$db = JFactory::getDbo();
				
		$where = array();
		$where[] = " `a`.`close_by_admin` = 0 ";
		$where[] = " a.closed = 0 ";
		$where[] = " a.status = 1 ";
		$where[] = " MapX != '' ";
		$where[] = " MapY != '' ";
		
		if ($cat!=0)
			$where[] = " a.category = '{$cat}' ";

			
		//To search by kilometers instead of miles, replace 3959 with 6371. 
		$distance_setting = 3959;
		
		if (defined("ads_opt_googlemap_distance") && ads_opt_googlemap_distance!=""){
			if(ads_opt_googlemap_distance==1)
				$distance_setting = 6371;
		}
		
		
		// Search the rows in the markers table
		$db->setQuery("
			SELECT a.id, a.short_description, a.title as name, a.category,a.picture,
				c.catname,
				MapX as lat, MapY as lng ,
			( $distance_setting * acos( cos( radians('$center_lat') ) * cos( radians( MapX ) ) * cos( radians( MapY ) - radians('$center_lng') ) + sin( radians('$center_lat') ) * sin( radians( MapX ) ) ) ) AS distance 
			FROM #__ads a 
			LEFT JOIN `#__ads_categories` c ON a.category = c.id 
			WHERE ".
			implode("AND",$where)
			."
			HAVING distance < '$radius' ORDER BY distance 
			");
		$rows = $db->loadObjectList();
		
		ob_end_clean();
		header("Content-type: text/xml");
        echo AdsUtilities::getMapsXML($rows);
		exit;
	}
	
	function fields_assign(){

		error_reporting(0);
		
		$database = JFactory::getDbo();
		
		$id = JRequest::getInt( "catid", 0 );
		$database->setQuery(
		"
		SELECT  a.db_name as a_fields, a.field_id, a.compulsory, LOWER(a.validate_type) as validate_type
		FROM #__ads_fields_categories 
		LEFT JOIN #__ads_fields as a ON fid=a.id 
		WHERE cid = {$id} 
		");
		$rows = $database->loadObjectList();
		
		ob_end_clean();
		header("Content-type: text/xml");
		
		// Start XML file, create parent node
		$dom = "<xml>";
		$dom .= "<markers>";
		
		// Iterate through the rows, adding XML nodes for each
		foreach ($rows as $row){
		  $field_id = $row->a_fields;
		  if($row->validate_type=='number')
			  $row->validate_type = "numeric";
		  if( $row->field_id )
			  $field_id = $row->field_id;
			  
		  $node = '<marker name="'.$row->a_fields.'" field_id="'.$field_id.'" compulsory="'.(int)$row->compulsory.'" validate_type="'.$row->validate_type.'">';
		  $node .= "</marker>";
		  $dom .= $node;
		}
		
		$dom .= "</markers>";
		$dom .= "</xml>";
		echo $dom;
		exit;
	}
	
	function ajax_form_customfields(){
		
		$Fi = & FactoryFieldsFront::getInstance();
		$id = JRequest::getInt( "catid", 0 );

		JRequest::setVar("custom_fields_category",$id);
        $session	= JFactory::getSession();
	    $session->set('custom_fields_category',$id);

		$smarty 	= AdsUtilities::SmartyLoaderHelper();
		$db = JFactory::getDbo();
		
		$adid = JRequest::getInt( "adid", 0 );
		$add = new JAdsAddsman($db);
		$add->loadData( $adid );

		//$fields = $Fi->getHTMLFields("ads", $add->id, $add,0 );

        if($add->id) {
            $fields = $Fi->getHTMLFields("ads", $add->id, $add,0);
        } else {
            $fields = $Fi->getHTMLFields("ads", $add->id, $add,1);
        }

		$doc = JFactory::getDocument();

        $root = JUri::root();
        $doc->addScriptDeclaration(' var root = "'.$root.'";');

        ob_end_clean();
		if(count($fields)){
			$smarty->assign("doc", $doc);
			$smarty->assign("profiler", $fields);
			$smarty->display("elements/display_edit_fields.tpl");
		}else 
			echo "";
		jexit();
	}
	
	function ajax_search_customfields(){
		
		$smarty 	= AdsUtilities::SmartyLoaderHelper();
		
		$Fi = & FactoryFieldsFront::getInstance();
      	JRequest::setVar('custom_fields_category',JRequest::getInt( "catid", null ));
      	
		$fields = $Fi->getSearchFields(null, null);
        $doc = JFactory::getDocument();

        $root = JUri::root();
        $doc->addScriptDeclaration(' var root = "'.$root.'";');
		ob_end_clean();

		$smarty->assign("profiler", $fields);
		$smarty->display("elements/display_search_fields.tpl");
		jexit();
	}
	
	function ajax_selectcategories(){
		$include_root_params = array();
		if (defined("ads_opt_category_root_post") && ads_opt_category_root_post==0) {
			$include_root_params = array("include_root"=>0);
		}
		
		$category = JRequest::getInt( "catid", 0 );

		if( isset($category) && $category!="" ) {
			$categoryHTML = Ads_HTML::selectCategory("category", $include_root_params + array("select"=>$category,"script"=>"onchange='aiai(this.value);'"));
		}
		else 
		{	
			$categoryHTML = Ads_HTML::selectCategory("category", $include_root_params + array("script"=>"onchange='aiai(this.value);'"));
		}	
		ob_end_clean();
		echo $categoryHTML;
		jexit();
	}
	
	function ajax_search_selectcategories(){
		ob_end_clean();
		$include_root_params = array();
		if (defined("ads_opt_category_root_post") && ads_opt_category_root_post==0) {
			$include_root_params = array("include_root"=>0);
		}
		
		echo Ads_HTML::selectCategory("cat", $include_root_params + array("script"=>"onchange='aiai_search(this.value);'"));
		jexit();
	}
		
	function category_listing_cost() {
		$var = JRequest::get('GET');

		$payOb = JTheFactoryPaymentLib::getInstance();
	    $payOb->loadAvailabeItems();
		$pricings = $payOb->price_plugins;  
		$listing_price = $pricings['price_listing']->price;
		
		if (isset($pricings['price_listing']) && $pricings['price_listing']->enabled == 1) {
		
			if ( isset($pricings['price_listing']->time_valability) && $pricings['price_listing']->time_valability == 0 ) {  
			// get price listing
				$catid = (int)$var['catid'];
								
				if ( isset($pricings['price_listing']->preferential_categories) && $pricings['price_listing']->preferential_categories == 1) {
									
					//if ( $pricings['price_listing']->default_price_categories != '' && $pricings['price_listing']->prices_categories == 0) {
					//	$listing_price = $pricings['price_listing']->default_price_categories;
							
					//} else {
						if ( isset($catid) && $catid > 0 ) {
							$listing_price = AdsUtilities::getCategoryPrice($catid,$pricings['price_listing']->default_price_categories,$pricings['price_listing']->preferential_categories,$pricings['price_listing']->commisionable_categories);
								
						}
						else	$listing_price = $pricings['price_listing']->price;
					//}
				} else {
					$listing_price = $pricings['price_listing']->price;
				}
			} else {
				//Preconfigured Availabilities Ranges 
			}
		} else {
			$listing_price = 0;
		}

		echo $listing_price;
		
	}

    function add_credits_calculate() {

		$var = JRequest::get('POST');

        $ad_id = $var['ad_id'];
        $ad_featured = $var['ad_featured'];
		$add_main_img = $var['add_main_img'];		//0 - NO, 1
		$add_pictures_selected_index = $var['add_pictures']; //0 = none, 1 = 1 picture to upload,...

        $count_deleted_images = AdsUtilities::getPicturesReplace($ad_id);
        //$count_deleted_images = $var['count_delete_images'] + $count_replace_db_pictures;
        $count_replace_images = number_format($var['count_delete_images'],0)  + number_format($count_deleted_images,0);

        $selected_feature = $var['selected_feature']; 	//'silver'
		$ad_valability = $var['ad_valability'];  //0 => 2 days = 4 credits

        $count_replace_main = AdsUtilities::getMainImgReplace($ad_id);

		$my = JFactory::getUser();
		$db = JFactory::getDbo();
		$credits = AdsUtilities::getUserCredits($my->id);

		$total_price = 0;
		$listing_price = 0;
		$main_img_price = 0;
		$pictures_price = 0;
		$featured_price = 0;

		$payOb = JTheFactoryPaymentLib::getInstance();
	    $payOb->loadAvailabeItems();
		$pricings = $payOb->price_plugins;  // array['price_featured_bronze'], ['price_packages'], ['price_pay_image'],['price_listing']

		if ( isset($pricings['price_pay_image']) && $pricings['price_pay_image']->enabled == 1) {

            //$count_replace_images
            $img_prices = price_pay_image_DBHelper::getPayImageAmount();

			if ($add_main_img == 1 && $count_replace_main == 0) {
				$main_img_price = price_pay_image_DBHelper::getCreditImg(0);
			}

			if ($add_pictures_selected_index != 0 && $add_pictures_selected_index > $count_replace_images) {
                $total_images_to_pay = $add_pictures_selected_index - $count_replace_images;
                                
                $pictures_price = $img_prices[$total_images_to_pay];

			} else {
                $pictures_price = 0;
            }

		} else {
			$main_img_price = 0;
			$pictures_price = 0;
		}

        if ( $selected_feature != 'none' && $selected_feature != $ad_featured) {
			$featured_request = "price_featured_$selected_feature";

			if ( isset($pricings[$featured_request]) && $pricings[$featured_request]->enabled == 1) {
				$featured_price = AdsUtilities::getItemPriceCredits('featured_'.$selected_feature);
			}
		} else {
			$featured_price = 0;
		}


		$total_price = $main_img_price + $pictures_price + $featured_price;

		$root = JUri::root();
		$link = JRoute::_ ( 'index.php?option=com_adsman&task=add_credits_calculate&format=raw' );

		$document	= JFactory::getDocument();
		$document->addScriptDeclaration(' var root = "'.$root.'";');
		$document->addScriptDeclaration(' var link = "'.$link.'";');
        
		if ($credits >= $total_price || ($total_price == 0) )
		 	echo json_encode(1);
		else {
			echo json_decode(0);
		}

		return;

	}

    function process()
    {
        //$session	= JFactory::getSession();
        $db         = JFactory::getDbo();
		
        $sagepay_details = JRequest::get('POST');

        $order_id           = $sagepay_details['order_id'];
        $item_description   = $sagepay_details['item_description'];
        $itemname           = $sagepay_details['itemname'];
        $quantity           = $sagepay_details['quantity'];
        $price              = $sagepay_details['price'];
        $currency           = $sagepay_details['currency'];
        $package_id         = $sagepay_details['package_id'];

        /*$session->set("session.token", null);
		$session->set("token", null);
        $session->clear("sagepay_details");
        $session->set('sagepay_details',$sagepay_details);*/

        require_once(ADS_COMPONENT_PATH."/plugins/payment/pay_sagepay.php");
        $payment_obj = new pay_sagepay($db);
        $payment_obj->show_payment_form($order_id,$item_description,$itemname,$quantity,$price,$currency,$return_url=null);

        return true;
    }
	
	function display(){
		$document = JFactory::getDocument();
		$viewName	= JRequest::getVar('view', 'adsman', 'default', 'cmd');
		$viewType	= $document->getType();

		$view = &$this->getView($viewName, $viewType);
		$view->display();
	}
	function getView( $name = '', $type = 'html', $prefix = 'adsView', $config = array() )
	{
		return parent::getView($name,$type,$prefix,$config);
	}

}
?>
