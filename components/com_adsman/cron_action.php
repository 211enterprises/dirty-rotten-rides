<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

/* cron script */
define( '_JEXEC', 1 );
//Change the password in the next line
define('ADS_CRON_PASSWORD','pass');

define( 'DS', DIRECTORY_SEPARATOR );
define('JPATH_BASE', str_replace("components".DS."com_adsman","",dirname(__FILE__)));
require_once( '../../includes/defines.php' );
require_once ( '../../includes/framework.php' );
$mosConfig_live_site = JPATH_SITE;
$mainframe = JFactory::getApplication('site');

$mainframe->initialise();

JPluginHelper::importPlugin('system');

JDEBUG ? $_PROFILER->mark('afterInitialise') : null;
$mainframe->triggerEvent('onAfterInitialise');

$database = JFactory::getDbo();
if(!defined("JPATH_COMPONENT_SITE"))
	define("JPATH_COMPONENT_SITE",JPATH_ROOT.DS."components".DS."com_adsman".DS);
define("ADS_COMPONENT_PATH",JPATH_COMPONENT_SITE);


require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_adsman'.DS.'thefactory'.DS.'admin.application.php');
$Tapp = JTheFactoryApp::getInstance();

require_once( JPATH_ADMINISTRATOR.DS.'components'.DS.'com_adsman'.DS.'tables'.DS.'addsman.php' );
require_once( 'options.php' );

require_once( JPATH_ADMINISTRATOR.DS.'components'.DS.'com_adsman'.DS.'thefactory'.DS.'payments'.DS.'payments.lib.php');
require_once( JPATH_SITE.DS.'components'.DS.'com_adsman'.DS.'thefactory'.DS.'front.payment.php' );
require_once( 'helpers/helper.php' );
require_once( 'helpers/route.php' );

if(! isset($_SESSION)){
	session_name( md5( $mosConfig_live_site ) );
	session_start();
}
@set_time_limit(0);
@ignore_user_abort(true);

if(php_sapi_name()=='cli')
{
    $argv=$_SERVER['argv'];
    $argc=$_SERVER['argc'];
    if ($argc > 0)
    {
      for ($i=1;$i < $argc;$i++)
      {
        parse_str($argv[$i],$tmp);
        $_REQUEST = array_merge($_REQUEST, $tmp);
      }
    }
}

$pass = JRequest::getVar('pass','');
$daily= JRequest::getVar('daily','');

if($pass !== ADS_CRON_PASSWORD){
    die ("AUTH ERROR");
}

// -------------- EXPIRED ADS----------- //
	$query = "SELECT a.* from #__ads a
			  WHERE '".Ads_Time::getNowSql()."' > a.end_date AND a.closed!= 1 AND a.status = 1 AND a.close_by_admin!=1";
	$database->setQuery($query);
	$rows = $database->loadObjectList();

	if(count($rows)>0){
		$row = new JAdsAddsman( $database );
		foreach ($rows as $r){
    		// Close expired ads
    		$row->load( $r->id );
    		$row->closed=1;
    		$row->closed_date=Ads_Time::getNowSql();
    		$row->catname = null;

    		if ($row->ancStore()){
    			$query = "select u.* from #__users u
    					  where u.id in ( $row->userid )";
    			$database->setQuery($query);
    			$mails = $database->loadObjectList();
    			$row->SendMails($mails,'add_expired');

    			$query = "SELECT u.* from #__users u
    					LEFT JOIN #__ads_favorites AS f on u.id = f.userid
    					where f.adid = $row->id";

    			$database->setQuery($query);
    			$favorites_mails = $database->loadObjectList();
    			//Notify Users that aded this add to fav that add will expire
    			$row->SendMails($favorites_mails,'add_expire_fav');
    		}

	   }
	}
// END EXPIRED ADS
// BEGIN DAILY TASKS --> notifications , watchlist
    if ($daily){
        //Notify upcoming expirations
    	$query = "SELECT a.* FROM #__ads a
    		 	  WHERE '".Ads_Time::getNowSql()."' >=DATE_ADD(end_date,INTERVAL -1 DAY) AND a.closed != 1 AND status = 1 AND a.close_by_admin!=1";
    	$database->setQuery($query);
    	$rows = $database->loadObjectList();

		$add = new JAdsAddsman($database);
		$usr = new JUser();

    	if (count($rows))
        	foreach ($rows as $row){
        		$add->load($row->id);
    			$usr->load($row->userid);
    			// Notify Owner that his add will soon expire
    			$add->SendMails(array($usr),'add_your_will_expire');

    			$query = "SELECT u.* from #__users u
    					LEFT JOIN #__ads_favorites AS f on u.id = f.userid
    					WHERE f.adid = $row->id";

    			$database->setQuery($query);
    			$favorites_mails = $database->loadObjectList();
    			//Notify Users that aded this add to fav that add will expire
    			$add->SendMails($favorites_mails,'add_add_soon_expire_fav');
        	}



    	//Close all ads without a parent user (deleted users?)
    	$query = "UPDATE #__ads AS a LEFT JOIN #__users AS b ON a.userid=b.id SET close_by_admin = 1,closed_date= '".Ads_Time::getNowSql()."' where b.id is null";
    	$database->setQuery($query);
    	$database->query();
    	//delete Very old ads (past Archived time)
    	$interval =  intval(ads_opt_availability);

    	if ( $interval>0 ){
        	$query = "SELECT id
        				FROM #__ads
        				WHERE '".Ads_Time::getNowSql()."' > DATE_ADD( closed_date, INTERVAL $interval MONTH )
        					AND (closed =1 OR close_by_admin=1)";
            $database->setQuery($query);
        	$idx = $database->loadResultArray(); //select ads that have to be purged
        	
            $row = new JAdsAddsman( $database );
            if (count($idx))
        		foreach ($idx as $id){
        		    $row->delete((int)$id);
        		}
    	}
    }
//plugin tasks
    $payment=&JTheFactoryPaymentController::getInstance();
    $payment->processCron($daily);

?>