<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

/** ensure this file is being included by a parent file */
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('joomla.html.pagination');

if (!defined('ITEMS_PER_PAGE')) {
	define("ITEMS_PER_PAGE", '5');
}

class getmyadsTab extends cbTabHandler {

	function getmyadsTab() {
		$this->cbTabHandler();
	}

	function getDisplayTab($tab,$user,$ui){
		$Itemid = JRequest::getVar('Itemid', 0, 'REQUEST', 'integer');
		require_once (JPATH_ROOT.DS.'components'.DS.'com_adsman'.DS.'helpers'.DS.'route.php');
		$AdsItemid = AdsmanHelperRoute::getMenuItemByTaskName("listadds");

		$database = JFactory::getDbo();
		$my = JFactory::getUser();
        $lang = JFactory::getLanguage();
        $lang->load('com_adsman');
		if(!file_exists(JPATH_ROOT."/components/com_adsman/adsman.php")){
            return "<div>You must First install <a href='http://www.thefactory.ro/shop/joomla-components/reverse-auction-factory.html'> Adsman Factory </a></div>";
		}
				
		require_once (JPATH_ROOT.DS.'components'.DS.'com_adsman'.DS.'lang'.DS.'default.php' );
		require_once (JPATH_ROOT.DS.'components'.DS.'com_adsman'.DS.'config.php');
		require_once (JPATH_ROOT.DS.'components'.DS.'com_adsman'.DS.'options.php');


		$limitstart = JRequest::getVar('limitstart','0');

		$params = $this->params;

		$query = "SELECT u.username,b.*, c.name as currency_name
			FROM #__ads b
			LEFT JOIN #__ads_currency c on b.currency=c.id
			LEFT JOIN #__users u on b.userid=u.id
			WHERE userid = '$user->user_id' order by id desc";

		$database->setQuery($query);
		$nrads = $database->loadObjectList();

        if (ads_opt_enable_countdown) {
            $jdoc = JFactory::getDocument();
            $js = JURI::root()."components/com_adsman/js/countdown.js";
            $jdoc->addScript( $js );
            $js = JURI::root()."components/com_adsman/js/startcounter.js";
            $jdoc->addScript( $js );
            $js_declaration = "var days='".JText::_('ADS_DAYS').",';	var expired='".JText::_('ADS_EXPIRED')."';var nrcounters=".count($nrads).";";
            $jdoc->addScriptDeclaration( $js_declaration );
        }

		$total = count($nrads);

		$pageNav = new JPagination( $total, $limitstart, ITEMS_PER_PAGE);

		$query .= " LIMIT $pageNav->limitstart, $pageNav->limit";
		$database->setQuery($query);
		$myads = $database->loadObjectList();

		$return  = "\t\t<div>\n";
		$return .="<table width='100%'>";
		$return .='<form name="topForm'.$tab->tabid.'" action="index.php" method="post">';
		$return .="<input type='hidden' name='option' value='com_comprofiler' />";
		$return .="<input type='hidden' name='task' value='userProfile' />";
		$return .="<input type='hidden' name='user' value='".$user->user_id."' />";
		$return .="<input type='hidden' name='tab' value='".$tab->tabid."' />";
		$return .="<input type='hidden' name='act' value='' />";


		if($myads) {
			$return	.= '<tr>';
			  $return .= '<th class="list_ratings_header">'.JText::_("Title").'</th>';
			  //$return .= '<th class="list_ratings_header">'.JText::_("Start date").'</th>';
			  $return .= '<th class="list_ratings_header">'.JText::_("End date").'</th>';
			  $return .= '<th class="list_ratings_header" style="text-align:right;">'.JText::_("Ask Price").'</th>';
			$return .= '</tr>';
			$k=0;
			foreach ($myads as $ma){
		     $link_view_details = JRoute::_("index.php?option=com_comprofiler&task=userprofile&user=$ma->userid&Itemid=$Itemid");
			 $link_view_add = JRoute::_("index.php?option=com_adsman&task=details&view=adsman&id=$ma->id&Itemid=$AdsItemid");

             AdsUtilities::process_countdown($ma);

			 $return .='<tr class="mywatch'.$k.'">';
			 $return .='<td>';
			 $return .=' <a href="'.$link_view_add.'">'.$ma->title.'</a>';
			 $return .='</td>';
			 //$return .='<td>';
			 //$return .= date(ads_opt_date_format,strtotime($ma->start_date));
			 //$return .='</td>';
			 $return .='<td>';
                if (ads_opt_enable_countdown) {
                    if ($ma->expired) {
                        $return .= '<span class="expired">'.JText::_("ADS_EXPIRED").'</span> '.$ma->end_date_text;
                    } else {
                        $return .= '<span class="add_text_medium_bold">'.
                         JText::_("Expires in").':&nbsp;<span id="time1">'.$ma->countdown.'</span>
                            </span>';
                    }
                } else {
                    $return .= date(ads_opt_date_format,strtotime($ma->end_date));
                }


			 //$return .= date(ads_opt_date_format,strtotime($ma->end_date));
			 $return .='</td>';
			 $return .='<td style="text-align:right;">';
			 $return .= $ma->askprice." ".$ma->currency_name;
			 $return .='</td>';
			 $return .= "</tr>";
			 $k=1-$k;

			 }
		} else {

			$return .=	"".JText::_("No Ads")."";

		}
		$return .= "<tr height='20px'>";
		$return .= "<td colspan='4' align='center'>";
		$return .= "</td>";
		$return .= "</tr>";
		
		$return .= "<tr>";
		$return .= "<td colspan='4' align='center'>";
		$return .= '<div class="pagination">';
		$return .= '<p class="counter">'.$pageNav->getPagesCounter().'</p>';
		$return .= $pageNav->getPagesLinks();
		$return .= '</div>';
		$return .= "</td>";
		$return .= "</tr>";
		$return .= "</form>";
		$return .= "</table>";
		$return .= "</div>";


		return $return;
	}
}
?>