<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined( '_JEXEC' ) or die( 'Restricted access' );

class getmymap extends cbTabHandler {

	function getmymapTab() {
		$this->cbTabHandler();
	}

	function getDisplayTab($tab,$user,$ui){
		
		$Itemid = JRequest::getVar('Itemid', 0, 'REQUEST', 'integer');
		$database = JFactory::getDbo();
		$my = JFactory::getUser();
		
		if(!file_exists(JPATH_ROOT."/components/com_adsman/adsman.php")){
            return "<div>You must First install <a href='http://www.thefactory.ro/shop/joomla-components/ads-factory-classifieds-extension.html'> Ads Factory </a></div>";
		}
		
		if(!defined("ADSMAN_COMPONENT_PATH"))
			define("ADSMAN_COMPONENT_PATH", JPATH_ROOT."/components/com_adsman/");
		if(!defined("ADSMAN_COMPONENT"))
			define("ADSMAN_COMPONENT", JURI::root()."/components/com_adsman/");
			
		require_once(ADSMAN_COMPONENT_PATH."/options.php");

		$params = $this->params;

		$cb_fields = array();
    	$database->setQuery("SELECT field,cb_field FROM #__ads_cbfields");
    	$r=$database->loadAssocList();
    	for($i=0;$i<count($r);$i++){
    		$cb_fields[$r[$i]['field']]=$r[$i]['cb_field'];
    	}
    	$googleMaps = array();
    	
    	if(isset($cb_fields['googleX']) && isset($cb_fields['googleY']) && $cb_fields['googleX']!="" && $cb_fields['googleY']!=""){
    	$database->setQuery("SELECT {$cb_fields['googleX']},{$cb_fields['googleY']} FROM #__comprofiler WHERE user_id = '{$user->id}'");
		$googleMaps=$database->loadRow();
	}
    	
$return = "<div>";

if(count($googleMaps)>0 && isset($googleMaps[0]) && $googleMaps[0]!="" && isset($googleMaps[1]) && $googleMaps[1]!="")
{
    $return .= "<div id=\"map_canvas\" style=\"width: 500px; height: 300px\"></div>";
    $return .= "
    <script src=\"http://maps.google.com/maps?file=api&amp;v=2&amp;key=".ads_opt_google_key."\" type=\"text/javascript\"></script> \r\n
    <script type=\"text/javascript\">\r\n
	\r\n
    function initialize() {\r\n
		if (GBrowserIsCompatible()) {\r\n
		\r\n
			var map = new GMap2(document.getElementById(\"map_canvas\"));\r\n
			var map = new GMap2(document.getElementById(\"map_canvas\"),\r\n
			  { size: new GSize(640,320) } );\r\n
			map.removeMapType(G_HYBRID_MAP);\r\n
			var point = new GLatLng({$googleMaps[0]}, {$googleMaps[1]});\r\n
			var centerpoint = new GLatLng({$googleMaps[0]}, {$googleMaps[1]});\r\n
			var mapControl = new GMapTypeControl();\r\n
			map.addControl(mapControl);\r\n
			map.addControl(new GLargeMapControl());\r\n
			map.setCenter(centerpoint, 12);\r\n
			map.addOverlay(new GMarker(point));
	  }\r\n
    }\r\n
	\r\n\r\n
	initialize();
    </script>\r\n";
}
else{
	$return .= "No map found!";
}
$return .= "</div>";

		return $return;
	}
}
?>