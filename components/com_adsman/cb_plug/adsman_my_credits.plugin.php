<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

/** ensure this file is being included by a parent file */
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('joomla.html.pagination');

if (!defined('ITEMS_PER_PAGE')) {
	define("ITEMS_PER_PAGE", '5');
}

class getmycreditsTab extends cbTabHandler {

	function getmycreditsTab() {
		$this->cbTabHandler();
	}

	function getDisplayTab($tab,$user,$ui){
		$Itemid = JRequest::getVar('Itemid', 0, 'REQUEST', 'integer');
		require_once (JPATH_ROOT.DS.'components'.DS.'com_adsman'.DS.'helpers'.DS.'route.php');
		$AdsItemid = AdsmanHelperRoute::getMenuItemByTaskName("listadds");

        JHTML::stylesheet ( 'main.css', 'components/com_adsman/css/' );

		$database = JFactory::getDbo();
		$my = JFactory::getUser();
        $lang = JFactory::getLanguage();
        $lang->load('com_adsman');

		if(!file_exists(JPATH_ROOT."/components/com_adsman/adsman.php")){
			  return "<div>You must First install <a href='http://www.thefactory.ro/shop/joomla-components/ads-factory-classifieds-extension.html'> Ads Factory </a></div>";
		}
				
		require_once (JPATH_ROOT.DS.'components'.DS.'com_adsman'.DS.'lang'.DS.'default.php' );
		require_once (JPATH_ROOT.DS.'components'.DS.'com_adsman'.DS.'config.php');
		require_once (JPATH_ROOT.DS.'components'.DS.'com_adsman'.DS.'options.php');
        require_once (JPATH_ROOT.DS.'components'.DS.'com_adsman'.DS.'plugins'.DS.'pricing'.DS.'price_packages.php');
        require_once (JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_adsman'.DS.'thefactory'.DS.'payments'.DS.'payments.lib.php');

        $query = "SELECT c.credits_no, c.userid FROM #__ads_user_credits c "
        			. " WHERE c.userid='$my->id' ";

        $database->setQuery( $query );
        $mycredits = $database->loadObjectList();

        $payOb = JTheFactoryPaymentLib::getInstance();
        $payOb->loadAvailabeItems();
        $pricings = $payOb->price_plugins;

        $user_free_package = price_packages_DBHelper::getUserFreePackage($my->id);

        if ($user_free_package != null)
            $had_free_package = $user_free_package->package_free;
        else
            $had_free_package = 0;

        if ($had_free_package == 0) {
            //he can get free package once
            $where = 'price >= 0';
        } else {
            $where = 'price > 0';
        }

        $database->setQuery("SELECT * FROM #__ads_packages WHERE status=1 AND ".$where." ORDER BY ordering ASC ");
  		$packages  = $database->loadObjectList();

        $package_image_path = JPATH_ROOT.DS.'images'.DS.'ads'.DS.'packages'.DS;

		$return  = "\t\t<div>\n";
		$return .='<table width="100%" cellpadding="2" cellspacing="2" style="border: 0 none !important;">';
		$return .='<form name="topForm'.$tab->tabid.'" action="index.php" method="post">';
		$return .="<input type='hidden' name='option' value='com_comprofiler' />";
		$return .="<input type='hidden' name='task' value='userProfile' />";
		$return .="<input type='hidden' name='user' value='".$my->id."' />";
		$return .="<input type='hidden' name='tab' value='".$tab->tabid."' />";
		$return .="<input type='hidden' name='act' value='' />";

		if($mycredits) {
			$k=0;
			foreach ($mycredits as $ma){

            $return .='<div class="mycredits'.$k.'">';
            $return .='<span style="line-height:40px;">';
            $return .=	"".JText::_("User Logged in ").": ".$my->username;
            $return .='   <br />';
            $return .=	"".JText::_("My current Status").": ";
            $return .='</span>';
               if ($ma->credits_no) {
                   $return .= '<span class="mycredits'.$k.'">'.$ma->credits_no. ' ' . JText::_("Credits");;
               } else {
                   $return .='<span class="mycredits'.$k.'">'.JText::_("0 Credits");
               }

            $return .='</span>';
            $return .='</div>';
            $return .='   <hr /><br />';
			 $k=1-$k;
			 }

		} else {
            $return .=	"".JText::_("User Logged in").": ".$my->username;
            $return .='   <br />';
			$return .=	"".JText::_("My current Status").": ".JText::_("No credits")."";
            $return .='   <hr /><br />';
		}

        if($packages) {

            $price_maincredit = $pricings['price_maincredit']->price;
            $currency_maincredit = $pricings['price_maincredit']->currency;

            $return .= '<table width="100%" cellpadding="2" cellspacing="2" class="ads_table">';
            $return	.= '<tr>';
            $return .= '<th><span style="color:#993333;">'.JText::_("Credits packages").'</span></th>';

            $return	.= '</tr>';
            $return	.= '<tr>
            			  <td valign="top">
            			    <table width="100%">
            				  <tr>';

            			foreach ($packages as $pac) {
                            $return	.= '<td align="center" valign="top">
            							<table>
            								<tr>';
            								if ($pac->price != 0){
                                                $return .= '<td align="center">';
                                                $return .= '<a class="ads_packages_link" href="index.php?option=com_adsman&task=buy_package&package='.$pac->name.'&amp;Itemid='.$Itemid.'" >';
                                                $return .= '<span style="color:#CC0000;font-weight: bold;">'.JText::_("ADS_BUY").'</span>';
                                                $return .= '</a>
            									</td>';
                                            } else {
                                                $return .= '<td align="center">';
                                                $return .= '<a class="ads_packages_link" href="index.php?option=com_adsman&task=free_package&package='.$pac->name.'&amp;Itemid='.$Itemid.'>'.JText::_("ADS_GET_FREE_CREDITS").'</a>';
                                                $return .= '</td>';
            								}
                            $return	.= '</tr>';
                            $return	.= '<tr>';
                            $return .= '<td align="center"><strong>'.$pac->name.'</strong></td>';
                            $return .= '</tr>';
                            $return .= '<tr>';
                            $return .= '<td align="center">'.$pac->price. ' '.$pac->currency.'</td>';
                            $return .= '</tr>';
                            $return .= '<tr>';
                            $return .= '<td align="center">'.$pac->credits.' '.JText::_("ADS_CREDITS").'</td>';
                            $return .= '</tr>';
            								if ($currency_maincredit == $pac->currency){
                            $return .= '<tr>';
                            $return .= '<td style="text-align:center;">'.JText::_("ADS_ECONOMY");
            									$economy =  $price_maincredit * $pac->credits - $pac->price;
                                                $return .= $economy . $currency_maincredit .JText::_("ADS_ECONOMY_PER_PACKAGE");
                            $return .= '</td>';
                            $return .= '</tr>';
                                            } else {
                            $return .= '<tr>';
                            $return .= '<td>&nbsp;</td>';
                            $return .= '</tr>';
            								}

                            $pac_img = $pac->name.".jpg";
                            $img_path = $package_image_path.$pac_img;

                            if (file_exists($img_path)) {
                                $imgurl = JURI::root().'images'.DS.'ads'.DS.'packages'.DS.$pac_img;

                                $return .= '<tr>';
                                $return .= '<td height="100" align="center">';
                                $return .= '<a href="index.php?option=com_adsman&task=buy_package&package='.$pac->name.'&amp;Itemid='.$Itemid.'">';
                                $return .= '<img src="'.$imgurl.'" width="80" class="ads_noborder" />';
                                $return .= '</a>';
                                $return .= '</td>';
                                $return .= '</tr>';
                            }

                            $return .= '</table>';
                            $return .= '</td>';
                        }
                $return	.= '</tr>
                            </table>
                            </td>';
                $return	.= '</tr>';
                $return	.= '</table>';
        }


		$return .= "</form>";
		$return .= "</table>";
		$return .= "</div>";


		return $return;
	}
}
?>