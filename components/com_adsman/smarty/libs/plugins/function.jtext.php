<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty {jtext} function plugin
 *
 * Type:     function<br>
 * Name:     jtext<br>
 * Purpose:  displays ini contants<br>
 *
 * @param array
 * @param Smarty
 */
function smarty_function_jtext($params, &$smarty)
{
  $text = isset($params['text']) ? $params['text'] : $params['_'];

  return JText::_($text);
}

/* {jtext text="abc"} */

?>
