<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.2.1
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

/**
 * Smarty {file_exists} function plugin
 *
 * Type:     function<br>
 * Name:     file_exists<br>
 * Purpose:  displays img for package<br>
 *
 * @param array
 * @param Smarty
 */

function smarty_function_file_exists($params, &$smarty)
{
    if (file_exists($params['file'])) {
        $path = JURI::root() . 'images/ads/packages/'. $params['img_name'];
        return '<img src="'.$path.'" width="80" class="ads_noborder" />';
    } else {
        return '';
    }
}

/* {file_exists file="js/jquery-1.2.6.js" img_name=$pac_img}
        <script language="javascript" type="text/javascript" src="js/jquery-1.2.6.js"></script>
   {/file_exists}
*/

?>