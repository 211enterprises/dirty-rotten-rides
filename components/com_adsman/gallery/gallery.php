<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

// no direct access
defined('_JEXEC') or die('Restricted access');


class ads_gallery{
	
	/*@var $_db DBTable */
	var $_db = null;
	var $imagelist = array();
	var $imagelist_ids = array();

	function getGalleryForAdd($ad)
	{
		if($ad->picture!="")
			$this->addImage($ad->picture);
		
		$sql = "SELECT * from #__ads_pictures a where id_ad='$ad->id' AND a.`published`=1 ";

        $this->_db->setQuery($sql);
		$pictures = $this->_db->loadObjectList();

		for($i=0; $i<count($pictures); $i++) {
		  $this->addImage($pictures[$i]->picture,$pictures[$i]->id);
		}
	}
	
	function addImage($imagename,$id=-1)
	{
		$this->imagelist[]		= $imagename;
		$this->imagelist_ids[]	= $id;
	}
	
	function clearImages()
	{
		$this->imagelist=array();
		$this->imagelist_ids=array();

	}

	function writeJS()
	{

		echo $this->getJS();
		
	}
	
	function writeThumb($thumbnr=0,$overlib=1,$medium=0)
	{
	    echo $this->getThumb($thumbnr,$overlib,$medium);
	}
	
	function writeGallery(){
		
	    echo $this->getGallery();
	    
	}
	
}

?>