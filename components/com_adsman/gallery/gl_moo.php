<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

// no direct access
defined('_JEXEC') or die('Restricted access');

include_once(JPATH_ROOT . DS . "components". DS ."com_adsman" . DS . "gallery" . DS . "gallery.php");

class gl_moo extends ads_gallery{
	var $_pathToJS;
	var $_pathToImages;

	function gl_moo(&$db,$pathToImages)
	{
		$this->_db=$db;
		$this->_pathToJS=JURI::root() . "/components/com_adsman/gallery/moo/";
		$this->_pathToImages=$pathToImages;
	}
	function getJS()
	{
	}
	
	function writeJS(){
		
		JHTML::_('behavior.mootools');
		JHTML::_('behavior.modal');
		jimport('joomla.html.pane');
		JHTML::_("behavior.tooltip");
	}

	function getThumb($thumbnr=0,$overlib=1,$medium=0)
	{
		$img = "";
		if ($this->imagelist[$thumbnr]) {
			
		   $img_small	= $this->_pathToImages.'/resize_'.$this->imagelist[$thumbnr];
   	       $img_full	= $this->_pathToImages.$this->imagelist[$thumbnr];
   	       $img_middle	= $this->_pathToImages.'/middle_'.$this->imagelist[$thumbnr];
	       $img_middle_html	= "<img src='$img_middle' class='ads_noborder'>";
	       $img_full_html	= "<img src='$img_full' class='ads_noborder'>";
		   if ($overlib){ 
		   {
				JHTML::_("behavior.tooltip",".hasTip");
                $img = "$img_middle_html";
				//$img = "$img_full_html";
				echo '<style type="text/css">.tool-tip{max-width:100% !important;}</style>';
				$img='<img src="'.$img_small.'" class="editlinktip hasTip ads_noborder" title="'.$img.'" >';
		   }

		   } else {
    	       $img='<a href="'.$img_full.'" class="modal">';
               //$img='<a href="'.$img_middle.'" class="modal">';
		       $img.='<img src="'.$img_small.'" class="ads_noborder" />';
		       $img.='</a>';
		   }
		} else {
 	       if ($medium)
	           $width= intval(ads_opt_medium_width);
	       else
	           $width= intval(ads_opt_thumb_width);
           $img.='<img src="'.$this->_pathToImages.'/no_image.png" class="ads_noborder" />';

		}
        return $img;
	}
	function writeThumb($thumbnr=0,$overlib=1,$medium=0)
	{
		//JHTMLBehavior::mootools();
	    echo $this->getThumb($thumbnr,$overlib,$medium);
	}

	function getGallery()
	{
		JHTML::_('behavior.modal');
		
		$tabs = &JPane::getInstance('Tabs');
		$img = "";
        $width= intval(ads_opt_medium_width);
	    if (count($this->imagelist)>0){

	    	$nr=0;
	        for($i=0;$i<count($this->imagelist);$i++){
	        	if ($this->imagelist[$i]=='') continue;
	        	$nr++;
	        }

	        $img.=$tabs->startPane("photoMooTabs");
			        for($i=0;$i<count($this->imagelist);$i++){
			            if ($this->imagelist[$i]=='') continue;
			            $img.=$tabs->startPanel("Photo {$i}","photo_{$i}");
			            $img.='<a class="modal" href="'.$this->_pathToImages.$this->imagelist[$i].'">'
				            	 .'<img src="'.$this->_pathToImages.'middle_'.$this->imagelist[$i].'" alt="" class="ads_noborder" width="'.$width.'" >'
				            	 .'</a>'."\n";
				         $img.=$tabs->endPanel();   	 
			        }
	            $img.=$tabs->endPane();
	    } else {
	        $img='<img src="'.$this->_pathToImages.'/no_image.png" class="ads_noborder" alt="No image" >';
	    }
	    return $img;
	}
	function writeGallery(){
	    echo $this->getGallery();

	}
}

?>
