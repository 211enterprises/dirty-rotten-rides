<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

// no direct access
defined('_JEXEC') or die('Restricted access');

include_once(JPATH_ROOT . DS . "components". DS ."com_adsman" . DS . "gallery" . DS . "gallery.php");

class gl_simple_carousel extends ads_gallery{
	
	var $_pathToJS;
	var $_pathToImages;

	function gl_simple_carousel(&$db,$pathToImages)
	{
		$this->_db=$db;
		$this->_pathToJS = JURI::root() . "components/com_adsman/gallery/simple_carousel/";
		$this->_pathToImages = $pathToImages;
	}
	
	function getJS()
	{
      $jdoc = & JFactory::getDocument();
      
      $width = intval(ads_opt_medium_width);
      $height = intval(ads_opt_medium_height);
		
      $box_width = $width+20;
      $box_height = $height+20;
      
      $gal_width = $width + 100;
      
      $gal_width_half = (int)($box_height/2) - 30;
      
		$css = "
			#id_waid{
				display:none; 
				margin-top:30px;
				margin-left:{$gal_width_half}px;
				margin-bottom:30px;
			}
			.gallery_wrap{
				
				width:{$gal_width}px !important;
				height:{$box_width}px !important;
			}
			
			.gallery_left{
				float:left;
				cursor:pointer;
			}
			
			.gallery_right{
				float:left;
				cursor:pointer;
			}
			
			#gallery_preview{
				border:1px solid #000;
				float:left;
				width:{$box_width}px;
				background:#FFF;
			}
			
			#preview_pic{
				margin:10px;
				max-width:{$width}px;
				max-height:{$height}px;
				width:{$width}px;
				border: 0 none;
			}
		";
		
		$jdoc->addStyleDeclaration($css);
		$js = "
			var gl_index = 0;
			var gl_images = new Array();
			var gl_path_to_js = '".$this->_pathToJS."';
		";
		$jdoc->addScriptDeclaration($js);
		$jdoc->addScript($this->_pathToJS."/js/gl_js.js");
		}
	function writeJS(){
		$this->getJS();
		JHTML::_('behavior.modal');
	}
	
	function getThumb($thumbnr=0,$overlib=1,$medium=0)
	{
		$img = "";
		if($this->imagelist[$thumbnr]){
		   	$img_small=$this->_pathToImages.'resize_'.$this->imagelist[$thumbnr];
   	   		$img_full=$this->_pathToImages.$this->imagelist[$thumbnr];
   	   		$img_middle=$this->_pathToImages.'/middle_'.$this->imagelist[$thumbnr];
	      	$img_small_html="<img src='$img_small' class='ads_noborder'>";
	      	$img_middle_html="<img src='$img_middle' class='ads_noborder'>";
	      	$img_full_html="<img src='$img_full' class='ads_noborder'>";
		   
	      	if ($overlib){{
				JHTML::_("behavior.tooltip",".hasTip");
                $img = "$img_middle_html";
			    //$img = "$img_full_html";
			    echo '<style type="text/css">.tool-tip{max-width:100% !important;}</style>';
			    $img='<img src="'.$img_small.'" class="editlinktip hasTip ads_noborder" title="'.$img.'" >';
		    }

		   } else {
               //$img='<a href="'.$img_middle.'" class="modal">';
    	       $img='<a href="'.$img_full.'" class="modal">';
		       $img.='<img src="'.$img_small.'" class="ads_noborder" />';
		       $img.='</a>';
		   }
		}else{
 	       if ($medium)
	           $width= intval(ads_opt_medium_width);
	       else
	           $width= intval(ads_opt_thumb_width);
           $img.='<img src="'.$this->_pathToImages.'no_image.png" class="ads_noborder" />';

		}
        return $img;
	}
	
	function writeThumb($thumbnr=0,$overlib=1,$medium=0)
	{
		//JHTMLBehavior::mootools();
	    echo $this->getThumb($thumbnr,$overlib,$medium);
	}
	
	function getGallery()
	{
		
		$img = "";
        $width= intval(ads_opt_medium_width);
	    if (count($this->imagelist)>0){

	        if ($this->imagelist[0]){
		        $main_img = $this->imagelist[0];
	        }
		
			  $img.='<div class="gallery_wrap">';
	        
	        $img.='
				      <a class="gallery_left" onclick="gl_left()" onmouseover="gl_hover(\'left\')" onmouseout="gl_out(\'left\')" ><img id="left_bar" src="'.$this->_pathToJS.'left.png" alt="left" /></a>
	        			<div id="gallery_preview">
							<img id="id_waid" src="'.$this->_pathToJS.'ajax-loader.gif" alt="wait" />
							<a id="preview_modal" class="modal" href="'.$this->_pathToImages.$main_img.'"><img id="preview_pic" src="'.$this->_pathToImages.'middle_'.$main_img.'" alt="thumb" /></a>
						</div>
						<a class="gallery_right" onclick="gl_right()" onmouseover="gl_hover(\'right\')" onmouseout="gl_out(\'right\')" ><img id="right_bar" src="'.$this->_pathToJS.'right.png" alt="right" /></a>';
	        
			        $img.= '<script type="text/javascript">';
			        for($i=0;$i<count($this->imagelist);$i++){
			            if ($this->imagelist[$i]=='') continue;
			            
			            $img.= 'gl_images["'.$i.'"]='."'".$this->_pathToImages.'middle_'.$this->imagelist[$i]."';";
				            	 
			        }
			        $img .='</script>';
           $img.='</div>';
           
	    } else {
	        $img='<img src="'.$this->_pathToImages.'no_image.png" class="ads_noborder" alt="No image" >';
	    }
	    
	    return $img;
	}
	
	function writeGallery(){
	    echo $this->getGallery();

	}

}

?>