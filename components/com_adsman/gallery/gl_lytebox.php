<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

// no direct access
defined('_JEXEC') or die('Restricted access');

include_once(JPATH_ROOT . DS . "components". DS ."com_adsman" . DS . "gallery" . DS . "gallery.php");

class gl_lytebox extends ads_gallery{
	var $_pathToJS;
	var $_pathToImages;

	function gl_lytebox(&$db,$pathToImages)
	{
		$this->_db=$db;
		$this->_pathToJS=JURI::root() . "/components/com_adsman/gallery/js";
		$this->_pathToImages=$pathToImages;
		$this->_pathToMainCss=JURI::root()."components/com_adsman/css/main.css";
	}
	function getJS()
	{
		
        $width= intval(ads_opt_medium_width);
        $height= intval(ads_opt_medium_height);
		$code="
		<script type=\"text/javascript\">
		if(typeof window.jQuery == 'undefined') {
		  document.writeln('<scr'+'ipt type=\"text/javascript\" src=\"$this->_pathToJS/jquery.js\"></scr'+'ipt>');
		}
        if(typeof window.jQuery != 'undefined') {
            jQuery.noConflict();
        }
		</script>
		<script type=\"text/javascript\" src=\"$this->_pathToJS/jquery.jcarousel.js\"></script>
		<script type=\"text/javascript\" src=\"$this->_pathToJS/thickbox/thickbox.js\"></script>
		
		<link rel=\"stylesheet\" href=\"$this->_pathToMainCss\" type=\"text/css\" media=\"screen\" />
		
		<link rel=\"stylesheet\" href=\"$this->_pathToJS/jquery.jcarousel.css\" type=\"text/css\" media=\"screen\" />
		<link rel=\"stylesheet\" href=\"$this->_pathToJS/thickbox/thickbox.css\" type=\"text/css\" media=\"screen\" />
		<link rel=\"stylesheet\" href=\"$this->_pathToJS/skin.css\" type=\"text/css\" media=\"screen\" />
		<style type=\"text/css\">
            .jcarousel-skin-tango.jcarousel-container-horizontal {
                width: ".($width)."px;
                padding: 20px 40px;

            }
            .jcarousel-skin-tango .jcarousel-clip-horizontal {
                width:  ".($width)."px;
                height: ".($height)."px;
            }
            .jcarousel-skin-tango .jcarousel-item {
                width: ".($width)."px;
                height: ".($height)."px;
            }
		</style>
		";
		return $code;
	}

	function getThumb($thumbnr=0,$overlib=1,$medium=0)
	{
		$img = "";
		if($this->imagelist[$thumbnr]){
		   $img_small	= $this->_pathToImages.'/resize_'.$this->imagelist[$thumbnr];
   	       $img_full	= $this->_pathToImages.$this->imagelist[$thumbnr];
   	       $img_middle	= $this->_pathToImages.'/middle_'.$this->imagelist[$thumbnr];
   	       $img_middle_html	= "<img src='$img_middle' class='ads_noborder'>";
	       $img_full_html	= "<img src='$img_full' class='ads_noborder'>";
	       
		   if ($overlib){ 
		   		JHTML::_("behavior.tooltip",".hasTip");
				//$img = "$img_full_html";
				$img = "$img_middle_html";
				$img='<img src="'.$img_small.'" class="editlinktip hasTip ads_noborder" title="'.$img.'" >';
		   } else {
    	       //$img='<a href="'.$img_middle.'" class="thickbox">';
               $img='<a href="'.$img_full.'" class="thickbox">';
		       $img.='<img src="'.$img_small.'" class="ads_noborder" />';
		       $img.='</a>';
		   }
		}else{
 	       if ($medium)
	           $width= intval(ads_opt_medium_width);
	       else
	           $width= intval(ads_opt_thumb_width);
           $img.='<img src="'.$this->_pathToImages.'/no_image.png" class="ads_noborder" />';

		}
        return $img;
	}
	
	function getGallery()
	{
		$img = "";
        $width= intval(ads_opt_medium_width);
	    if (count($this->imagelist)>1){

	    	$nr=0;
	        for($i=0;$i<count($this->imagelist);$i++){
	        	if ($this->imagelist[$i]=='') continue;
	        	$nr++;
	        }

	        $img.='
	           <script type="text/javascript">
                jQuery(document).ready(function() {
                    jQuery(\'#mycarousel\').jcarousel({
                        size: '.$nr.',
                        scroll: 1

                    });
                });
                jQuery.noConflict();
                </script>
	        ';
	        $img.='<ul id="mycarousel" class="jcarousel-skin-tango" >';
	        for($i=0;$i<count($this->imagelist);$i++){
	            if ($this->imagelist[$i]=='') continue;
	            $img.='<li style="background-image:url(); list-style:none;width:'.($width).'px">'
	            	 .'<a href="'.$this->_pathToImages.'/'.$this->imagelist[$i].'" class="thickbox">'
	            	 .'<img src="'.$this->_pathToImages.'/middle_'.$this->imagelist[$i].'" alt="" class="ads_noborder" />'
	            	 .'</a>'
	            	 .'</li>'."\n";
	        }
            $img.='
                   </ul>
	        ';
	    } elseif(count($this->imagelist)==1) {
           $img.= '<a href="'.$this->_pathToImages.'/'.$this->imagelist[0].'" class="thickbox">'
            	 .'<img src="'.$this->_pathToImages.'/middle_'.$this->imagelist[0].'" alt="" class="ads_noborder" />'
            	 .'</a>';
	    	
	    } else {
	        $img='<img src="'.$this->_pathToImages.'/no_image.png" class="ads_noborder" alt="" />';
	    }
	    return $img;
	}
	
}

?>