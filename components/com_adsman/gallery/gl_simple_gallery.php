<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

// no direct access
defined('_JEXEC') or die('Restricted access');

include_once(JPATH_ROOT . DS . "components". DS ."com_adsman" . DS . "gallery" . DS . "gallery.php");

class gl_simple_gallery extends ads_gallery{
	var $_pathToJS;
	var $_pathToImages;

	function gl_simple_gallery(&$db,$pathToImages)
	{
		$this->_db=$db;
		$this->_pathToJS=JURI::root() . "components/com_adsman/gallery/simple_gallery/";
		$this->_pathToImages=$pathToImages;
	}
	
	function getJS()
	{
      $jdoc = & JFactory::getDocument();
      
      $width = intval(ads_opt_medium_width);
      $height = intval(ads_opt_medium_height);
		
      $box_width = $width+20;
      $box_height = $height+20;
      
      $gal_width = $width + 100;
      
		$css = "
			.gallery_wrap{
				width:{$gal_width}px !important;
				height:{$box_height}px !important;
			}
			
			#gallery_preview{
				width:{$box_width}px !important;
				height:{$box_height}px !important;
				float:left;
				background:#FFF;
			}
			
			#preview_pic{
				margin:6px;
				border: 0 none;
				max-width:{$width}px !important;
				max-height:{$height}px !important;
				width:{$width}px;
			}
			
			#gallery_thumblist{
				background:#FFF;
				float:left;
				width:80px;
				height:{$box_height}px !important;
				overflow:auto;
			}
			
			.img_thumb{
				margin:0px;
				background:url('".$this->_pathToJS."/photo_bg.jpg') no-repeat;
				width:60px !important;
				height:60px !important;
				text-align:center !important;
				
			}
			.img_thumb img{
				margin-top:5px !important;
				width:45px !important;
				max-width:45px !important;
				max-height:45px !important;
				cursor:pointer;
			}
		";
		$jdoc->addStyleDeclaration($css);
		$js = "
			function showPic(src){
				
				document.getElementById('id_waid').style.display='none';
				document.getElementById('preview_pic').style.display='block';
				document.getElementById('preview_pic').src = src;
				document.getElementById('preview_pic_big').href = src.replace('middle_','');
			}
		
			function picSelect(imgO){
				
				document.getElementById('id_waid').style.display='block';
				document.getElementById('preview_pic').style.display='none';
				//imgO.style.width='70px';
				setTimeout(\"showPic('\"+imgO.src+\"')\", 300);
				
			}
		";
		$jdoc->addScriptDeclaration($js);
	}
	function writeJS(){
		$this->getJS();
		JHTML::_('behavior.modal');
	}
	
	function getThumb($thumbnr=0,$overlib=1,$medium=0)
	{
		$img = "";
		if($this->imagelist[$thumbnr]){
		   $img_small=$this->_pathToImages.'/resize_'.$this->imagelist[$thumbnr];
   	   $img_full=$this->_pathToImages.$this->imagelist[$thumbnr];
   	   $img_middle=$this->_pathToImages.'/middle_'.$this->imagelist[$thumbnr];
	      $img_small_html="<img src='$img_small' class='ads_noborder'>";
	      $img_middle_html="<img src='$img_middle' class='ads_noborder'>";
	      $img_full_html="<img src='$img_full' class='ads_noborder'>";
		   if ($overlib){{
				JHTML::_("behavior.tooltip",".hasTip");
                $img = "$img_middle_html";
			    //$img = "$img_full_html";
			    echo '<style type="text/css">.tool-tip{max-width:100% !important;}</style>';
			    $img='<img src="'.$img_small.'" class="editlinktip hasTip ads_noborder" title="'.$img.'" >';
		   }

		   }else{
    	       //$img='<a href="'.$img_middle.'" class="modal">';
               $img='<a href="'.$img_full.'" class="modal">';
		       $img.='<img src="'.$img_small.'" class="ads_noborder" />';
		       $img.='</a>';
		   }
		}else{
 	       if ($medium)
	           $width= intval(ads_opt_medium_width);
	       else
	           $width= intval(ads_opt_thumb_width);
           $img.='<img src="'.$this->_pathToImages.'/no_image.png" class="ads_noborder" />';

		}
        return $img;
	}
	function writeThumb($thumbnr=0,$overlib=1,$medium=0)
	{
		//JHTMLBehavior::mootools();
	    echo $this->getThumb($thumbnr,$overlib,$medium);
	}
	
	function getGallery()
	{
		
		$img = "";
        $width= intval(ads_opt_medium_width);
	    if (count($this->imagelist)>0){

	    	$nr=0;
	        for($i=0;$i<count($this->imagelist);$i++){
	        	if ($this->imagelist[$i]=='') continue;
	        	$nr++;
	        }
	        
	       $img.='<div class="gallery_wrap">';
	        
	        if($this->imagelist[0]){
		        $main_img = $this->imagelist[0];
	        }
	        
	        $img.='<div id="gallery_preview">
							<img id="id_waid" src="'.$this->_pathToJS.'ajax-loader.gif" style="display:none; margin-top:50px; margin-left:50px;" />
							<a class="modal" id="preview_pic_big" href="'.$this->_pathToImages.$main_img.'"><img id="preview_pic" src="'.$this->_pathToImages.'middle_'.$main_img.'" /></a>
						</div>
						<div id="gallery_thumblist">';
	        
			        for($i=0;$i<count($this->imagelist);$i++){
			            if ($this->imagelist[$i]=='') continue;
			            $img.='<div class="img_thumb">';
			            
			            $img.= '<img src="'.$this->_pathToImages.'middle_'.$this->imagelist[$i].'" onclick="picSelect(this);" alt="" class="ads_noborder" width="'.$width.'" >';
				            	 
				         $img.='</div>';
			        }
	            $img.='</div>
							 <div style="clear:both;">&nbsp;</div>
					 </div>';
	    } else {
	        $img='<img src="'.$this->_pathToImages.'/no_image.png" class="ads_noborder" alt="No image" >';
	    }
	    
	    return $img;
	}
	function writeGallery(){
	    echo $this->getGallery();

	}
	

}

?>