function showPic(src){
	
	document.getElementById('id_waid').style.display='none';
	document.getElementById('preview_pic').style.display='block';
	document.getElementById('preview_pic').src = src;
	document.getElementById('preview_modal').href = src.replace("middle_","");
	
	
}

function gl_left(){
	
	if( gl_index > 0 ){
		gl_index--;
		document.getElementById('id_waid').style.display='block';
		document.getElementById('preview_pic').style.display='none';
		setTimeout("showPic('"+gl_images[gl_index]+"')", 300);
	}
	if(gl_index==0){
		document.getElementById('left_bar').src = gl_path_to_js+'left_disabled.png';
	}
	document.getElementById('right_bar').src = gl_path_to_js+'right.png';
	
}

function gl_right(){
	if( gl_index < gl_images.length - 1 ){
		gl_index++;
		document.getElementById('id_waid').style.display='block';
		document.getElementById('preview_pic').style.display='none';
		setTimeout("showPic('"+gl_images[gl_index]+"')", 300);
	}
	if(gl_index==gl_images.length - 1){
		document.getElementById('right_bar').src = gl_path_to_js+'right_disabled.png';
	}
	document.getElementById('left_bar').src = gl_path_to_js+'left.png';
	
}

function gl_hover(bar){
	
	var disbld = '_over';
	if(bar=='left' && gl_index==0 ){
			disbld = '_disabled';
	}else if(bar=='right' && gl_index==(gl_images.length - 1) ){
			disbld = '_disabled';
	}
	document.getElementById(bar+'_bar').src=gl_path_to_js+bar+disbld+'.png';
	
}

function gl_out(bar){
	
	var disbld = '';
	if(bar=='left' && gl_index==0 ){
			disbld = '_disabled';
	}else if(bar=='right' && gl_index==(gl_images.length - 1) ){
			disbld = '_disabled';
	}
	document.getElementById(bar+'_bar').src=gl_path_to_js+bar+disbld+'.png'
}
