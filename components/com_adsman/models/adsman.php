<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/


// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');
jimport('joomla.application.component.helper');

/**
 * @package		Ads manager
 */
class adsModelAdsman extends JModel
{
	var $_name='Adsman';
    var $_listView = null;
    var $_myadds = null;
    var $_favorites = null;
    var $_where = null;
    var $_join = null;
    var $_filters = null;
    var $_filter_order_Dir = null;
    var $_filter_order = null;
    var $_archive = null;
    var $_keyword = null;

    
    function getfilter_order_Dir()
    {
        return $this->getState('filter_order_Dir');
    }

    function getreversefilter_order_Dir()
    {
        if ($this->getState('filter_order_Dir') == "ASC")
            return "DESC";
        else
            return "ASC";
    }

    function getfilter_order()
    {
        return $this->getState('filter_order');
    }

    function getArchive()
    {
        return $this->getState('archive');
    }

    function getCurrentFilters()
    {
	
        return $this->_filters;
    }
    
    function getAddData( &$id ){
    	
		$add = new JAdsAddsman($this->_db);
		
		$add->loadData( $id );
		
		$add->is_my_add = $add->isMyAdd();
		
		
		$add->start_date_text	=	date( ads_opt_date_format, (strtotime($add->start_date) ) );
		$add->end_date_text		=	date( ads_opt_date_format, (strtotime($add->end_date)) );
		
		if( isset($add->tags)) 
			$add->tags = explode(",", $add->tags);

// Image Gallery
		$gallery = AdImageUtilies::loadImagePlugin();
		$gallery->getGalleryForAdd($add);

        if (count($gallery->imagelist)>0){
        	$gallery->writeJS();
	    	$add->thumbnail = $gallery->getThumb(0,0);
	    	$add->gallery   = $gallery->getGallery();
        }
		$cfields_translations = FactoryFieldsFront::getCustomFieldsOptions();
		FactoryFieldsFront::translateRowOptions($add,$cfields_translations);
        
		AdsUtilities::process_countdown($add);
        $my = JFactory::getUser();

		// show only my messages
        if ( defined('ads_opt_messages_public') && ads_opt_messages_public != '0' ){
            //private messages
			$add->messages = array();

            if ($my->id){
				$add->messages = $add->getMessages( $my->id );
			}
        } else {
            //if ad private and messages public
            if ($my->id) {
                $add->messages = $add->getMessages($my->id);
            } else
			    $add->messages = $add->getMessages();
        }
			
		$add->links = AdsUtilities::makeLinks($add);
		$add->guest_email = AdsUtilities::cloack_email($add->guest_email);
    	return $add;
    }
    
    function getUrls($add_id) {
    	
    	$jtable = null;
    	JTable::addIncludePath(JPATH_SITE.DS.'administrator'.DS.'components'.DS.'com_adsman'.DS.'tables');
		$jtable =& JTable::getInstance('Urls','Table');
    	
    	//$url = new JAdsUsers($this->_db);
		$jtable->set('_tbl_key', "ad_id");
    	$jtable->load($add_id);
    	
        return $jtable;
    }

    function addToFav()
    {
        $id = JRequest::getInt('id', 0);
        $my = JFactory::getUser();
        $db = JFactory::getDbo();
        $db->setQuery("SELECT count(id) FROM #__ads_favorites WHERE userid = $my->id and adid = '$id'");
        $result = $db->loadResult();
        if ($result)
            return;
        $db->setQuery("INSERT INTO #__ads_favorites SET adid = '" . $id . "', userid='" .$my->id . "'");
        $db->query();
    }

    function delFav()
    {
        $id = JRequest::getInt('id', 0);
        $my = JFactory::getUser();
        $db = JFactory::getDbo();
        $db->setQuery("DELETE FROM #__ads_favorites WHERE adid = '" . $id ."' AND userid='" . $my->id . "'");
        $db->query();
    }

     function sendMessage($reply_toid,$reply_message_textarea){
        $db = JFactory::getDbo();
        $id = JRequest::getInt('id', 0); //ad id
        $my = JFactory::getUser();
       
        if (ads_opt_logged_posting) {
        	
        	if ($reply_toid != null ) {
        		$to = $db->Quote($reply_toid);
        	} else {
		        $to = $db->Quote(JRequest::getVar('to', 0));
        	}
		        
		    $from = $db->Quote(JRequest::getVar('from', 0));
        
        } else {
        	
	        $to = $db->Quote(JRequest::getVar('to', null));
	        $from = $db->Quote(JRequest::getVar('from', null)); //userid
        	
        }

        if ($reply_message_textarea != null)
        	$text = $db->Quote($db->getEscaped($reply_message_textarea));
        else 
     	   $text = $db->Quote($db->getEscaped(JRequest::getVar('message_textarea', null)));

        if ($text != "" && $id != "" && $to != "" && $from != "") {
            $db->setQuery(
	            "INSERT INTO #__ads_messages SET adid = " . $id . ", to_user=" . $to .
                ", from_user = " . $from . ", comment = " . $text . " , datesend = '".Ads_Time::getNowSql()."' "
                );
           $db->query();
	        
           if( ads_opt_logged_posting && !$my->id){
	            $add = new JAdsAddsman($db);
	            $add->load($id);
	            $mails = new stdClass();
	            $mails->email = $add->guest_email;
	            $mails->name =  $add->guest_username;
	            $add->SendMails(array($mails), 'new_message',$text);
	            unset($add);
	        }
	        else
	        {
	        		
	            // SEND MAIL
	            //$id_string = $to;
	            $id_string = $to . "," . $from;
	            $query = "SELECT u.* FROM #__users u
						  WHERE u.id IN ($id_string)";
	            $db->setQuery($query);
	            $mails = $db->loadObjectList();
	            $add = new JAdsAddsman($db);
	            $add->load($id);
	            $add->SendMails($mails, 'new_message',$text);
	            unset($add);
	        	
	        }

        } else {
            $msg = array();
            if ($text == "")
                $msg[] = JText::_("ADS_ERR_ENTER_MESSAGE");
            if ($id == "")
                $msg[] = JText::_("ADS_ERR_NO_AD_SELECTED");
            if ($to == "")
                $msg[] = JText::_("ADS_ERR_NO_USER_SELECTED");
            if ($from == "")
                $msg[] = JText::_("ADS_ERR_LOGIN_TO_SEND_MESSAGE");
        }

        if (isset($msg) && count($msg) > 0)
            return implode(",", $msg);
        else
            return true;
    }

    function report($id) {
        $my = JFactory::getUser();
        $text = JRequest::getVar('comment', null);

        if ($text != "" && $id  && $my->id) {
            $db = JFactory::getDbo();
            require_once (JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_adsman' . DS .'tables' . DS . 'addsman.php');
            $add = new JAdsAddsman($db);
            
            if ( !$add->load($id) ) 
            	return JText::_("ADS_ERR_NO_AD_SELECTED");

            $db->setQuery("INSERT INTO #__ads_report SET adid = '" . $id . "', userid='" . $my->
                id . "', message = '" . $db->getEscaped($text) . "' , modified = '".Ads_Time::getNowSql()."' ");
            $db->query();
            
            
            $query = "SELECT u.* FROM #__users u
					  WHERE usertype='Super Administrator'";
            $db->setQuery($query);
            $mails = $db->loadObjectList();
            $add->SendMails($mails, 'add_reported');

        } else {
            $msg = array();
            if ($text == "")
                $msg[] = JText::_("ADS_ERR_ENTER_MESSAGE");
            if ($id == "")
                $msg[] = JText::_("ADS_ERR_NO_AD_SELECTED");
            if (!$my->id)
                $msg[] = JText::_("ADS_ERR_LOGIN_TO_SEND_MESSAGE");
        }

        if (isset($msg) && count($msg) > 0)
            return implode(",", $msg);
        else
            return true;
    }

    function getFavorites()
    {

        $this->setState('favorites', 1);
        return $this->getAdds();
    }

    function getMyAdds()
    {

        $this->setState('myadds', 1);
        return $this->getAdds();
    }

	function getAdds()
    {
        
		$app = JFactory::getApplication();
		
		// get ads type / archive/active/ published / unpublished
		$this->setState('archive', $app->getUserStateFromRequest('com_adsman.archive', 'archive', 'active'));
		
		if ($this->getState("myadds") != "1") {

		    if ($this->getState("archive") == "unpublish")
                $this->setState('archive', "active");

		} else {
			$this->setState('archive', $app->getUserStateFromRequest('com_adsman.archive','archive', 'active'));
        }
        // filters myads only, favorites
		$this->setFiltersList();
		
		$this->buildFiltersQuery();

		$config = JFactory::getConfig();
		$this->setState('listView', $app->getUserStateFromRequest('com_adsman.list_view', 'list_view', 'list') );

		if ( ads_opt_nr_items_per_page > 0 )
			$list_limit = ads_opt_nr_items_per_page;
		else
			$list_limit = $config->getValue('config.list_limit');

		// Get the pagination request variables
		$this->setState('limit', $app->getUserStateFromRequest('com_adsman.limit','limit', $list_limit, 'int'));
		$this->setState('limitstart', JRequest::getVar('limitstart', 0, '', 'int'));
		// In case limit has been changed, adjust limitstart accordingly
		$this->setState('limitstart', ($this->getState('limit') != 0 ? (floor($this->getState('limitstart') / $this->getState('limit')) * $this->getState('limit')) :0));

		$this->setState('filter_order_Dir', $app->getUserStateFromRequest('com_adsman.filter_order_Dir', 'filter_order_Dir', "DESC"));

        $ads_opt_sort_date = ads_opt_sort_date.'_date';
        if ($app->getUserStateFromRequest('com_adsman.filter_order','filter_order') == 'start_date' ||
            $app->getUserStateFromRequest('com_adsman.filter_order','filter_order') == 'end_date'  ) {

            $app->setUserState('com_adsman.filter_order', $ads_opt_sort_date);
            $this->setState('filter_order',$ads_opt_sort_date);
        } else {
            $this->setState('filter_order', $app->getUserStateFromRequest('com_adsman.filter_order','filter_order', $ads_opt_sort_date));
            //$this->setState('filter_order', $app->getUserStateFromRequest('com_adsman.filter_order','filter_order', "end_date"));
        }

		$query = $this->buildQuery();
		$list = $this->_getList($query, $this->getState('limitstart'), $this->getState('limit'));
        
		return $list;
    }

    function setFiltersList()
    {
		$app = JFactory::getApplication();
		$filters = array();

        $reset = JRequest::getVar('reset', "");
		$remove = JRequest::getVar('remove', "");

		if ($reset != "all") {

         $filters["keyword"] 		= JRequest::getVar('keyword', $app->getUserStateFromRequest('com_adsman.keyword', 'keyword'));
         $filters["in_description"] = JRequest::getVar('in_description', $app->getUserStateFromRequest('com_adsman.in_description', 'in_description'));
         $filters["cat"] 			= (int)JRequest::getVar('cat', 0);
         $filters["country"] 		= JRequest::getVar('country', $app->getUserStateFromRequest('com_adsman.country', 'country'));
         $filters["state"] 			= JRequest::getVar('state', $app->getUserStateFromRequest('com_adsman.state', 'state'));
         $filters["city"] 			= JRequest::getVar('city', $app->getUserStateFromRequest('com_adsman.city', 'city'));
         $filters["userid"] 		= JRequest::getVar('userid', $app->getUserStateFromRequest('com_adsman.userid', 'userid'));
         $filters["username"] 		= JRequest::getVar('username', $app->getUserStateFromRequest('com_adsman.username', 'username'));
         $filters["ad_type"] 		= JRequest::getVar('ad_type', $app->getUserStateFromRequest('com_adsman.ad_type', 'ad_type'));
         $filters["price_min"] 		= JRequest::getVar('price_min', $app->getUserStateFromRequest('com_adsman.price_min', 'price_min'));
         $filters["price_max"] 		= JRequest::getVar('price_max', $app->getUserStateFromRequest('com_adsman.price_max', 'price_max'));
         $filters["currency"] 		= JRequest::getVar('currency', $app->getUserStateFromRequest('com_adsman.currency', 'currency'));
         $filters["start_date"] 	= JRequest::getVar('start_date', $app->getUserStateFromRequest('com_adsman.start_date', 'start_date'));
         $filters["end_date"] 		= JRequest::getVar('end_date', $app->getUserStateFromRequest('com_adsman.end_date', 'end_date'));
         $filters["tag"] 			= JRequest::getVar('tag', $app->getUserStateFromRequest('com_adsman.tag','tag'));
         $filters["ad_city"]		= JRequest::getVar('ad_city', $app->getUserStateFromRequest('com_adsman.ad_city', 'ad_city'));
         $filters["ad_postcode"]	= JRequest::getVar('ad_postcode', $app->getUserStateFromRequest('com_adsman.ad_postcode', 'ad_postcode'));
         
		} else {
			// reset filters; set null to state
			$this->setState("filters", array());
			$app->setUserState('com_adsman.keyword', '');
			$app->setUserState('com_adsman.in_description', '');
			$app->setUserState('com_adsman.cat', '');
			$app->setUserState('com_adsman.country', null);
			$app->setUserState('com_adsman.state', null);
			$app->setUserState('com_adsman.city', null);
			$app->setUserState('com_adsman.userid', '');
			$app->setUserState('com_adsman.username', '');
			$app->setUserState('com_adsman.tag', '');
			$app->setUserState('com_adsman.ad_type', '');
            $app->setUserState('com_adsman.price_min', '');
            $app->setUserState('com_adsman.price_max', '');
            $app->setUserState('com_adsman.currency', '');
			$app->setUserState('com_adsman.start_date', '');
			$app->setUserState('com_adsman.end_date', '');
            $app->setUserState('com_adsman.ad_city', null);
            $app->setUserState('com_adsman.ad_postcode', null);
        }
        
        if($remove!="" && in_array($remove, array("username","keyword","in_description","cat","country","state","city","userid","tag","ad_type","price_min","price_max","currency","start_date","end_date","ad_city","ad_postcode"))){
        	$app->setUserState("com_adsman.{$remove}", '');
        	$filters[$remove] = "";
        }

		$Fi = & FactoryFieldsFront::getInstance();
		$Fi->setFiltersToState($filters, $reset);
		
        $this->_filters = $filters;
    }

    function getCategories()
    {
		$option =  JRequest::getVar('option', 'com_adsman', 'REQUEST', 'word');
		$Itemid = JRequest::getVar('Itemid', 0, 'REQUEST', 'integer');
        $letter= strtoupper(JRequest::getWord('startletter'));
				
		$curentItemid = AdsmanHelperRoute::getMenuItemByTaskName("listcats");
		$AdsItemid = AdsmanHelperRoute::getMenuItemByTaskName("listadds");
		
		$database = JFactory::getDbo();
		$my = JFactory::getUser();
		
		$database->setQuery(
		"SELECT 
			category, count(*) as nr,
			IF( (DATE_SUB(NOW(), INTERVAL 30 DAY) <= MAX(start_date) AND  MAX(start_date) IS NOT NULL ) , 1 , 0 ) as is_new 
		 FROM #__ads 
		 WHERE status=1 AND closed = 0 AND close_by_admin = 0 
		 GROUP BY category
		");
		
		$category_data = $database->loadObjectList("category");
		
        $database->setQuery("
        SELECT c . *
        \r\n".
        " FROM #__ads_categories AS c  \r\n".
        (($letter)?" where c.catname like '$letter%'  \r\n":"").
        " ORDER BY c.id, c.ordering, catname ASC
        ");

		$dbrows = $database->loadObjectList("id");
		//echo nl2br($database->_sql);exit;

		$catslug_parent = array();
		$cats = array();
		if ($dbrows) 
		{
			foreach ($dbrows as $k =>$rw){

                $cats[$rw->parent][$rw->id] = $rw;
				$nr_a = isset($category_data[$rw->id]->nr) ? $category_data[$rw->id]->nr : 0;
				
				if (defined("ads_opt_inner_categories") && ads_opt_inner_categories==1 ) {
					
					if( isset($category_data[$rw->parent]) )
						$category_data[$rw->parent]->nr += $nr_a;
					else 
						$category_data[$rw->parent]->nr = $nr_a;
						
					$pu = $rw->parent;
					
					$catslug_parent[$rw->id] = "&amp;parent=$pu";

                    if (isset($dbrows[$pu])) {
                        while ($pu>0) {

                            $parentElement = 	$dbrows[$pu];
                            if( isset($category_data[$parentElement->parent]) )
                                $category_data[$parentElement->parent]->nr += $nr_a;
                            else
                                $category_data[$parentElement->parent]->nr = $nr_a;

                            $pu = $parentElement->parent;
                        }
					}
				}
			}
		}
	
		$cat = (int)JRequest::getVar("cat", 0);

        if($letter)
            $cat_filter=" and c.catname like '$letter%' ";
        else
            $cat_filter = " AND c.parent = " . $database->Quote($cat);

		// ===> prepare List of WatchListed Categories in $idslist
		$idslist = array();
		if ($my->id) {
		   $database->setQuery("SELECT catid FROM #__ads_watchlist WHERE userid=" .$my->id);
		   $idslist = $database->loadResultArray();
		}
		// <=== prepare List of WatchListed Categories in $idslist
		
		$JoinList = array();
		$SelectCols = array();
        
		FactoryFieldsFront::getCustomFieldsJoin($JoinList, $SelectCols, "categories", array("#__ads_categories"=>"c" ));
		$select_cols_sql = "";
		
		if(count($SelectCols)>0)
		  $select_cols_sql  = ",".implode(",",$SelectCols);
        
        //, COUNT( DISTINCT a.id ) as nr_a,  IF( (DATE_SUB(NOW(), INTERVAL 30 DAY) <= MAX(a.start_date) AND  MAX(a.start_date) IS NOT NULL ) , 1 , 0 ) as is_new  \r\n
        
		$database->setQuery(" SELECT c . * , catsef.categories as catslug ".$select_cols_sql.PHP_EOL.
		   " FROM #__ads_categories AS c " .PHP_EOL.
		 	implode(PHP_EOL,$JoinList).PHP_EOL.
			" LEFT JOIN `#__ads_categories_sef` AS `catsef` ON  `c`.`id`=`catsef`.`catid` ".PHP_EOL.
		   	" WHERE c.status=1 $cat_filter " .PHP_EOL.
			" GROUP BY c.id  order by c.ordering, catname ASC");

		//echo nl2br($database->_sql);exit;
		$rows = $database->loadObjectList();
		$site = JURI::root();

		for ($i = 0; $i < count($rows); $i++) {
			
			// add category sef route ; if null process
			$catslug = "";

			if(isset($rows[$i]->catslug)) {
				$separator = PHP_EOL;
				$catslug = str_replace($separator,"/",str_replace("/","-",$rows[$i]->catslug));
				$catslug = "&amp;catslug=$catslug";
					
				$catslug_parent_string = $rows[$i]->parent;
				$catslug_parent = "&amp;parent=$catslug_parent_string";		
			}
			
			$rows[$i]->nr_a = 0;
			
			if (isset($category_data[$rows[$i]->id])) {
	        	$rows[$i]->nr_a = $category_data[$rows[$i]->id]->nr;
	        	$rows[$i]->is_new = 0;

	        	if (isset($category_data[$rows[$i]->id]->is_new)) {
		        	$rows[$i]->is_new = $category_data[$rows[$i]->id]->is_new;
	        	}
			}
			
			if (in_array($rows[$i]->id, $idslist)) {
				// watchlisted
				$rows[$i]->watchListed_flag = 1;
				$rows[$i]->link_watchlist = JRoute::_("index.php?option=$option&task=delwatchcat&cat=" .$rows[$i]->id . "&Itemid=$curentItemid".$catslug.$catslug_parent);
				$rows[$i]->img_src_watchlist = "{$site}components/{$option}/img/delete.png";
                
			} else {
				// not watchlisted
				$rows[$i]->watchListed_flag = 0;
				$rows[$i]->link_watchlist = JRoute::_("index.php?option=$option&task=addwatchcat&cat=" .$rows[$i]->id . "&Itemid=$curentItemid".$catslug.$catslug_parent);
				$rows[$i]->img_src_watchlist = "{$site}components/{$option}/img/add.png";
			}

            /*$langfilter_status = AdsUtilities ::getLangfilter();
            $lang_code = JRequest::getVar('lang', '', 'REQUEST', 'string');

            if ($langfilter_status) {
                  if ($lang_code == 'en') {

                $rows[$i]->link = JRoute::_("index.php?option=$option&view=adsman&task=listcats&amp;lang='.$lang_code.'&amp;cat=" .$rows[$i]->id . "&Itemid=$curentItemid".$catslug.$catslug_parent);
                $rows[$i]->view = JRoute::_( "index.php?option=$option&view=adsman&task=listadds&amp;lang='.$lang_code.'&amp;&cat=".$rows[$i]->id . "&Itemid=$AdsItemid".$catslug.$catslug_parent);
              } else {
                $rows[$i]->link = JRoute::_("index.php?option=$option&view=adsman&task=listcats&amp;lang=bg&amp;cat=" .$rows[$i]->id . "&Itemid=$curentItemid".$catslug.$catslug_parent);
                $rows[$i]->view = JRoute::_( "index.php?option=$option&view=adsman&task=listadds&amp;lang=bg&amp;cat=".$rows[$i]->id . "&Itemid=$AdsItemid".$catslug.$catslug_parent);
              }
            } else {
                $rows[$i]->link = JRoute::_("index.php?option=$option&view=adsman&task=listcats&cat=" .$rows[$i]->id . "&Itemid=$curentItemid".$catslug.$catslug_parent);
                $rows[$i]->view = JRoute::_( "index.php?option=$option&view=adsman&task=listadds&cat=".$rows[$i]->id . "&Itemid=$AdsItemid".$catslug.$catslug_parent);
            }*/

			$rows[$i]->link = JRoute::_("index.php?option=$option&view=adsman&task=listcats&cat=" .$rows[$i]->id . "&Itemid=$curentItemid".$catslug.$catslug_parent);
			$rows[$i]->view = JRoute::_( "index.php?option=$option&view=adsman&task=listadds&cat=".$rows[$i]->id . "&Itemid=$AdsItemid".$catslug.$catslug_parent);
			$rows[$i]->kids = JTheFactoryCategoryObj::has_children($rows[$i]->id, true);
			$rows[$i]->catname = $rows[$i]->catname;
			$rows[$i]->descr = $rows[$i]->description;
			
			// Subcategories
			$database->setQuery(" SELECT c . * , catsef.categories as catslug ".$select_cols_sql.PHP_EOL.
			" FROM #__ads_categories AS c " .PHP_EOL.
			implode(PHP_EOL,$JoinList).PHP_EOL.
			" LEFT JOIN `#__ads_categories_sef` AS `catsef` ON  `c`.`id`=`catsef`.`catid` ".PHP_EOL.
			" WHERE c.status=1  AND c.parent = '" . (int)$rows[$i]->id . "'" .PHP_EOL.
			" GROUP BY c.id order by c.ordering, catname ASC");
                
			$rows[$i]->subcategories = $database->loadObjectList();
			//echo nl2br($database->_sql);exit;
			
			for ($j = 0; $j < count($rows[$i]->subcategories); $j++) {
				// add category sef route ; if null process
				$catslug = "";
				if(isset($rows[$i]->subcategories[$j]->catslug)){
					$separator = PHP_EOL;
					$catslug = str_replace($separator,"/",str_replace("/","-",$rows[$i]->subcategories[$j]->catslug));
					$catslug = "&amp;catslug=$catslug";
					
					$catslug_parent_string = $rows[$i]->subcategories[$j]->parent;
					$catslug_parent = "&amp;parent=$catslug_parent_string";
				}
				
				if (in_array($rows[$i]->subcategories[$j]->id, $idslist)) {
				  $rows[$i]->subcategories[$j]->watchListed_flag = 1;
				  $rows[$i]->subcategories[$j]->link_watchlist = JRoute::_("index.php?option=$option&task=delwatchcat&cat=" .$rows[$i]->subcategories[$j]->id . "&Itemid=$curentItemid".$catslug.$catslug_parent);
				  $rows[$i]->subcategories[$j]->img_src_watchlist = "{$site}components/{$option}/img/_delete.png";
				} else {
				  $rows[$i]->subcategories[$j]->watchListed_flag = 0;
				  $rows[$i]->subcategories[$j]->link_watchlist = JRoute::_("index.php?option=$option&task=addwatchcat&cat=" .$rows[$i]->subcategories[$j]->id . "&Itemid=$curentItemid".$catslug.$catslug_parent);
				  $rows[$i]->subcategories[$j]->img_src_watchlist = "{$site}components/{$option}/img/add.png";
				}

				$rows[$i]->subcategories[$j]->nr_a = 0;
				if (isset($category_data[$rows[$i]->subcategories[$j]->id])) {
		        	$rows[$i]->subcategories[$j]->nr_a = $category_data[$rows[$i]->subcategories[$j]->id]->nr;
		        	$rows[$i]->subcategories[$j]->is_new = 0;
		        	if (isset($category_data[$rows[$i]->subcategories[$j]->id]->is_new))
			        $rows[$i]->subcategories[$j]->is_new = $category_data[$rows[$i]->subcategories[$j]->id]->is_new;
				}
             $rows[$i]->subcategories[$j]->catname = $rows[$i]->subcategories[$j]->catname;
             $rows[$i]->subcategories[$j]->link = JRoute::_("index.php?option=$option&view=adsman&task=listcats&cat=" .$rows[$i]->subcategories[$j]->id . "&Itemid=$curentItemid".$catslug.$catslug_parent);
             $rows[$i]->subcategories[$j]->view = JRoute::_("index.php?option=$option&view=adsman&task=listadds&cat=" .$rows[$i]->subcategories[$j]->id . "&Itemid=$AdsItemid".$catslug.$catslug_parent);
             $rows[$i]->subcategories[$j]->kids = JTheFactoryCategoryObj::has_children($rows[$i]->subcategories[$j]->id);
			}
		}

		return $rows;
	}

    // CATEGORIES WATCHLIST (favoriteS)
    function addWatch($id)
    {
        $database = JFactory::getDbo();
        $my = JFactory::getUser();
        if ($my->id && $id != "") {
            $database->setQuery("INSERT INTO #__ads_watchlist SET userid = '" . (int)$my->id ."',catid='" . (int)$id . "'");
            $database->query();
        }
        return true;
    }

    function delWatch($id)
    {
        $database = JFactory::getDbo();
        $my = JFactory::getUser();
        if ($my->id && $id != "") {
            $database->setQuery("DELETE FROM #__ads_watchlist WHERE userid = '" . (int)$my->id ."' AND catid='" . (int)$id . "'");
            $database->query();
        }
        return true;
    }

    function getListView()
    {
        return $this->getState('listView');
    }

    function getTotal()
    {
        $query = $this->buildQuery(1);
        return $this->_getListCount($query);
    }

    function buildQuery( $countQuery=0 )
	 {
    	$favorites = $this->getState('favorites');
    	$myadds = $this->getState('myadds');
		$my = JFactory::getUser();
    	
    	$SelectCols 	= array();
    	$JoinList 		= array();
    	$WhereClauses 	= array();
		$Orderings	 	= array();

		$filter_order_Dir 	= strtoupper($this->_db->getEscaped($this->getState('filter_order_Dir')));
		$filter_order 		= $this->_db->getEscaped($this->getState('filter_order'));
		$filter_order_Dir =in_array($filter_order_Dir,array('ASC','DESC'))?$filter_order_Dir:"ASC";
        $filter_order = in_array($filter_order,array('end_date','start_date','askprice','askprice+0','title'))?$filter_order:"end_date";
/**
 * 
 *  Table Fields to select
 * 
 * 
 **/	
		$SelectCols[] 	= " `a`.* ";
		if(!$countQuery){
			$SelectCols[] 	= " `cat`.`id` as cati, `cat`.`catname`, `catsef`.`categories` as catslug ";
			$SelectCols[] 	= " `cat`.`parent` AS catparent_id ";
			$SelectCols[] 	= " `cur`.`name` as currency_name ";
			$SelectCols[] 	= " `u`.`username` \r\n ";
			$SelectCols[] 	= " `u`.`name` as uname \r\n ";
			$SelectCols[] 	= " IF (COUNT( DISTINCT `pics`.`id`) >0 , 1, 0 ) AS more_pictures ";
		}

		$SelectCols[] 	= " GROUP_CONCAT(DISTINCT `t`.`tagname`) AS alltags ";
		

/**
 * 
 *  Where conditions
 *
 *  
 **/


         if ( $w = $this->getState("where") )
			$WhereClauses = $w;
		
	
		$WhereClauses[] 	= " (`cat`.`status`=1 OR `cat`.`status` IS NULL ) ";
		//$WhereClauses[] 	= " `a`.`close_by_admin` = 0 ";
		
		$config = JFactory::getConfig();
		$offset = $config->get("offset");

        if ( defined("ads_opt_component_offset") && ads_opt_component_offset!="" )
         	$offset = ads_opt_component_offset;

		$timestamp = strtotime(gmdate("M d Y H:i:s", time()));
		//$add_days = $timestamp + 3600*$offset + 86400*3; //added 3 days
		$now = date( 'Y-m-d H:i:s', $timestamp + 3600*$offset);

		$WhereClauses[] 	= " `a`.`start_date` <=  '".$now."' ";
		// USER LOGGED COMMON
		if($my->id){

			$SelectCols[] 	= "  IF ( `a`.`userid` = '" . (int)$my->id . "', 1, 0 ) AS `is_my_add` ";
	
			if ( $myadds==1 ) 
				$WhereClauses[] = " `a`.`userid` = '" . (int)$my->id . "'";
	        
	        
			$SelectCols["fav"] 	= "  IF ( `f`.`id` >0, 1, 0 ) AS `favorite` ";
			
			if ( $favorites==1 ) {
				$JoinList[] = " RIGHT JOIN `#__ads_favorites` AS `f` ON `a`.`id`=`f`.`adid` AND  `f`.`userid`='" . (int)$my->id . "' AND `f`.`userid` <> `a`.`userid` ";
			}else{
				$JoinList[] = " LEFT JOIN `#__ads_favorites` AS `f` ON `a`.`id`=`f`.`adid` AND  `f`.`userid`='" . (int)$my->id . "' AND `f`.`userid` <> `a`.`userid` ";
			}
		}else{
			$SelectCols["fav"] 	= "  -1 AS `favorite` ";
		}
		
		$where = "";

		if(count($WhereClauses)>0)
			$where = "WHERE ".implode(" AND ",$WhereClauses);

/**
 * 
 *  Joins
 * 
 **/
		$JoinList[] = " LEFT JOIN `#__ads_categories` AS `cat` ON  `a`.`category`=`cat`.`id` ";
		$JoinList[] = " LEFT JOIN `#__ads_categories_sef` AS `catsef` ON  `a`.`category`=`catsef`.`catid` ";
		
		
		$exclusive_profile = 0;
		if( defined('ads_opt_profile_required') && ads_opt_profile_required==1  ){
			$exclusive_profile = 1;
		}
		
		if( defined('ads_opt_logged_posting') && ads_opt_logged_posting==1  ){
			$JoinList[] = " LEFT JOIN `#__users` AS `u` ON `a`.`userid` = `u`.`id` ";
			// for custom fields
			$exclusive_profile = 0;
		}else{
			$JoinList[] = " INNER JOIN `#__users` AS `u` ON `a`.`userid` = `u`.`id` ";
		}
		
		// Profile JOINING
		if(defined("ads_opt_profile_mode") && ads_opt_profile_mode!= "")
			$profile_mode = ads_opt_profile_mode;
		else
			$profile_mode = '';
			
		JTheFactoryUserProfile::getListJoin($profile_mode,$exclusive_profile, $JoinList, $SelectCols );
		
      $JoinList[] = " LEFT JOIN `#__ads_tags` AS `t` ON  `t`.`parent_id`=`a`.`id`  ";
      
		if(!$countQuery){
        $JoinList[] = " LEFT JOIN `#__ads_currency` AS `cur` ON  `cur`.`id`=`a`.`currency` ";
        $JoinList[] = " LEFT JOIN `#__ads_pictures` AS `pics` ON `id_ad` = `a`.`id` AND `pics`.`published` = 1 ";
		}

// modifies the Join List and the select list
		if($profile_mode=="ads"){
			FactoryFieldsFront::getCustomFieldsJoin($JoinList, $SelectCols, null, array("#__ads_users"=>"prof", "#__ads_categories"=>"cat" ));
		}
		else{
			//FactoryFieldsFront::getCustomFieldsJoin($JoinList, $SelectCols, "ads", array("#__ads"=>"a", "#__ads_categories"=>"cat"));
			FactoryFieldsFront::getCustomFieldsJoin($JoinList, $SelectCols, "categories", array( "#__ads_categories"=>"cat"));
		}

// Featurings first
		$Orderings[]	= "`a`.`featured`='gold' DESC";
		$Orderings[]	= "`a`.`featured`='silver' DESC";
		$Orderings[]	= "`a`.`featured`='bronze' DESC";
// Required ordering filter
if($filter_order)
		$Orderings[]	= "`a`.$filter_order $filter_order_Dir";
		
		$Orderings[]	= "`a`.`id` $filter_order_Dir ";
		
$query = " SELECT ".implode(",",$SelectCols)." \r\n ".
		 " FROM `#__ads` AS `a` \r\n".
		 implode(" \r\n ",$JoinList)."\r\n".
		 $where."\r\n".
		 " GROUP BY `a`.`id` ".
		 " ORDER BY ".implode(",",$Orderings)." \r\n "; 

		#echo "<strong>Adsman List QUERY</strong><br /><br />".(str_replace("#__","t0diu_",  nl2br($query)));//exit;
		return $query;
	}
	
    function buildFiltersQuery()
    {
        global $cb_fieldmap;
        $filters = $this->_filters;

        AdsUtilities::sanitize($filters);

        $where = array();

        if (!empty($filters["keyword"])) {
            $keyword = array();
            $searchTerms = explode(" ", $filters["keyword"]); // Split the words

            if (!empty($searchTerms)) {
                foreach ($searchTerms as $searchTerm) {

                    $keyword[] = "  (a.title) LIKE ('%" . $searchTerm . "%') ";
                    $keyword[] = "  (catsef.categories) LIKE ('%" . $searchTerm . "%') ";
                    $keyword[] = " (cat.catname) LIKE ('%" . $searchTerm . "%') ";

                    if (!empty($filters["in_description"])) {
                        $keyword[] = "  (a.description) LIKE ('%" . $searchTerm . "%') ";
                    }
                    if (!empty($filters["in_metatags"])) {
                        $keyword[] = "  (a.metadecription) LIKE ('%" . $searchTerm . "%')  ";
                        $keyword[] = "  (a.metakeywords) LIKE ('%" . $searchTerm . "%')  ";
                    }

                    $searchTermArr[] = implode(" OR", $keyword);
                }

                $where[] = "(" . implode(" OR", $searchTermArr) . " )";
            }
        }

        if (!empty($filters["city"])) {
            if (ads_opt_profile_mode=="ads"){
					$where[] = " prof.city = '" . $filters["city"] . "' ";
        		}else {
					$where[] = JTheFactoryIntegration::getFilterSql(ads_opt_profile_mode, "city", $filters["city"]);
	         }
        }
        
        if (!empty($filters["state"])) {
            if (ads_opt_profile_mode=="ads"){
					$where[] = " prof.state = '" . $filters["state"] . "' ";
        		}else {
					$where[] = JTheFactoryIntegration::getFilterSql(ads_opt_profile_mode, "state", $filters["state"]);
	         }
        }
        
        if (!empty($filters["username"])) {
            $where[] = " u.username LIKE '%" . $filters["username"] . "%' ";
        }
        if (!empty($filters["userid"])) {
            $where[] = " a.userid = '" . $filters["userid"] . "' ";
        }
        if (!empty($filters["country"])) {
            if (ads_opt_profile_mode=="ads"){
                $where[] = " prof.country = '" . $filters["country"] . "' ";
        		}else {
   	     		$where[] = JTheFactoryIntegration::getFilterSql(ads_opt_profile_mode, "country", $filters["country"]);
	         }
        }
        if (!empty($filters["tag"])) {
            $where[] = " t.tagname = '" . $filters["tag"] . "' ";
        }
        
        if ( isset($filters["ad_type"] ) && ($filters["ad_type"] != '') ) {
            if ( $filters["ad_type"] != '2')
                $where[] = " a.addtype = " . $filters["ad_type"] . " ";
            //else
            //	$where[] = " a.addtype = 0 OR a.addtype = 1 ";
        }

        if (!empty($filters["price_min"])) {
            $pricemin = $filters["price_min"];
            //$pricemin = number_format($filters["price_min"], 2);

            if (!empty($filters["price_max"])) {
                $pricemax = $filters["price_max"];
                //$pricemax = number_format($filters["price_max"], 2);
                $where[] = " a.askprice <= " . $pricemax . " AND a.askprice >= " . $pricemin . " ";

            } else {
                $where[] = " a.askprice >= " . $pricemin . " ";
            }
        } else {
            if (!empty($filters["price_max"])) {
                $pricemax = $filters["price_max"];
                //$pricemax = number_format($filters["price_max"], 2);
                $where[] = " a.askprice <= " . $pricemax . "";
            }
        }

        if ( isset($filters["currency"] ) && ($filters["currency"] != 0) ) {
        		$where[] = " a.currency = " . $filters["currency"] . " ";
        }

        if (!empty($filters["cat"])) {
            if (!ads_opt_inner_categories)
                $where[] = " a.category= '" . $filters["cat"] . "' ";
            else{
            	$cat = new JTheFactoryCategoryObj();
            	
            	$cTree = $cat->build_child($filters["cat"],true);
            	$cat_ids = array();
            	if($cTree){
            		foreach ($cTree as $cc){
            			if(isset($cc["id"]) && $cc["id"]!="")
            				$cat_ids[] = $cc["id"];
            		}
            	}
            	if( count($cat_ids) )
            		$cat_ids = implode(",", $cat_ids);
                $where[] = " ( a.category  IN (" . $cat_ids . ") )";
            }

        }
        if (!empty($filters["start_date"])) {
            $where[] = " a.start_date >= '" . Ads_Time::getDateSql(strtotime(AdsUtilities::addDatetoIso($filters["start_date"]) ) ) . "' ";
        }
        if (!empty($filters["end_date"])) {
        	$where[] = " a.end_date <= '" . Ads_Time::getDateSql(strtotime(AdsUtilities::addDatetoIso($filters["end_date"]) ) ) . "' ";
        }

        if (!empty($filters["ad_city"])) {
             $where[] = " a.ad_city = '" . $filters["ad_city"] . "' ";
        }
        if (!empty($filters["ad_postcode"])) {
             $where[] = " a.ad_postcode = '" . $filters["ad_postcode"] . "' ";
        }

        if ($this->getState("archive") == "archive") {
            $where[] = "  a.closed = 1 AND a.closed_date > a.end_date AND a.status = 1 ";

        } elseif ($this->getState("archive") == "active") {
            $where[] = " a.closed = 0 AND a.status = 1 AND '".Ads_Time::getNowSql()."' <= a.end_date AND a.close_by_admin = 0 ";
        }

        if ($this->getState("archive") == "expired") {
            $where[] = "  a.closed = 0 and a.status = 1 AND a.close_by_admin = 0 AND '".Ads_Time::getNowSql()."' >= a.end_date ";
        }

        /*if ($this->getState("archive") == "active" || $this->getState("archive") != "unpublish") {
            $where[] = " a.status = 1 AND '".Ads_Time::getNowSql()."' <= a.end_date AND a.closed = 0 AND a.close_by_admin = 0 ";
        } else*/
        if ($this->getState("archive") == "unpublish") {
            //$where[] = " a.status = 0 ";
            // not published || clodes by admin || canceled
            $where[] = " a.status = 0  OR a.close_by_admin = 1 OR (a.closed = 1 AND a.status = 1 AND a.close_by_admin = 0 AND a.closed_date <= a.end_date) ";
        }

		$Fi = & FactoryFieldsFront::getInstance();
		$Fi->setSQLFilters($where);

		$this->setState( "where", $where );
    }

    function saveAdd( &$add )
    {
        $Itemid = JRequest::getVar('Itemid', 0, 'REQUEST', 'integer');
        $app = JFactory::getApplication();

        $isNew = !(bool)$add->id;
        
        $database = JFactory::getDbo();

        $error = $this->validateSave($add);
        
        if (count($error) > 0) {
        	return $error;
        }

        /**
         *  Get rid of assigned non DB table properties
         * 
         */
        $tags = $add->tags;	
        unset($add->tags);
        
		if( $isNew && defined("ads_opt_logged_posting") && ads_opt_logged_posting==1  ){
			$add->token_id = md5($add->title.time());
		}
	
		JRequest::setVar('custom_fields_category',$add->category);
        
		/* @var $add JAdsAddsman */
		$error = $add->store();
        
        if( $error !== true ) {
        	return $error;
        }
        
        // TAGS
		$tag_obj = new adsTags($database, "#__ads");
		$tag_obj->setTags($add->id, $tags);

        $this->uploadAtachment($add);
        $this->deleteFiles($add);

		// UPLOAD FILES
        $msg = $this->uploadFiles($add);
        
        //if($msg!==true){
        if($msg != ''){
        	$error = $msg;
        }

		// TO DO: register events
        // MAILS
        if ( defined("ads_opt_logged_posting") && ads_opt_logged_posting==1  ){
            $user = new stdClass();
            if ($isNew) {
            	
	            $user->name = $add->guest_username;
	            $user->email = $add->guest_email;
	            $add->SendMails(array($user), 'add_guest_post_key');
	            
            }
        }
        
        if ($error) {
        //if (!$error) {	
			$mail_from = $app->getCfg('mailfrom');
			$site_name = $app->getCfg('sitename');
			
			
			// If mailfrom is not defined A super administrator is notified
			if ( ! $mail_from  || ! $site_name ) {
				
				//get all super administrator
				$query = 'SELECT name, email, sendEmail' .
						' FROM #__users' .
						' WHERE LOWER( usertype ) = "super administrator" ';
				$database->setQuery( $query );
				$rows = $database->loadObjectList();
	
				$site_name = $rows[0]->name;
				$mail_from = $rows[0]->email;
			}
			
         	$user = new stdClass();
        	
         	if($isNew && $add->status != 1){
            	
	            $user->name = "Administrator";
	            $user->email = $mail_from;
	            $add->SendMails(array($user), 'add_posted_admin');
            }
        }

        if ($add->status == 1) {
        	        	
            $my = JFactory::getUser();
            $add->SendMails(array($my), 'add_published');

            $query = " SELECT u.* FROM #__users u
    				   LEFT JOIN #__ads_watchlist f ON f.userid = u.id
    				   WHERE f.catid = '$add->category' AND u.id <> '$my->id' ";
            $database->setQuery($query);
            $mails = $database->loadObjectList();
            $add->SendMails($mails, 'add_aded_to_fav');
            
			$mail_from = $app->getCfg('mailfrom');
            $user = new stdClass();
            if($isNew){
            	
	            $user->name = "Administrator";
	            $user->email = $mail_from;
	            $add->SendMails(array($user), 'add_published_admin');
            }
        }

		return $error;
    }
    
    function bindAdd( &$add ){
        
    	$database = JFactory::getDbo();
    	$session  = JFactory::getSession();
        $oldid = (int)JRequest::getVar('oldid', 0);
        $plugin_status = (int)JRequest::getVar('status', 0);
        $isListingEnabled = price_listing_DBHelper::getEnabledPayListing();

        if ($oldid)
            $this->isRepost = true;
        else
            $this->isRepost = false;
        
    	$add->id = (int)JRequest::getVar('id', null);
        $add->load( $add->id );
                
        $posted_pm_values = JRequest::get('POST');

        $add->bind( $posted_pm_values );
                
        $add->category 			= $posted_pm_values["category"];
        $add->short_description = JRequest::getVar('short_description', '', 'post', 'string', JREQUEST_ALLOWRAW);
        $add->description 		= JRequest::getVar('description', '', 'post', 'string', JREQUEST_ALLOWRAW);
        
        $add->tags 				= $posted_pm_values["tags"];
        
        Ads_FilterInput_Helper::cleanText($add->description, "BL" );
        Ads_FilterInput_Helper::cleanText($add->short_description, "BL"  );
        Ads_FilterInput_Helper::cleanText($add->title, "BL"  );

        if ($isListingEnabled) {
            $add->status = $plugin_status;
        } else {
            if ($session->get("controller_status") != null && ($session->get("controller_status") == 'unpublished' || $session->get("controller_status") == 0 )) {
                $add->status = 0;
                //$session->set("controller_status", null);
            }
        }

        if ($add->id) {

        // edit custom field prep
			$sadd = new JAdsAddsman($database);
			$sadd->load($add->id);

			// do not edit these fields
			$add->start_date = $sadd->start_date;
			$add->end_date = $sadd->end_date;
			
			if($add->atachment!="")
				$this->attachment = $add->atachment;
				
	    	if (isset($_FILES["atachment"])){
	    		if (is_uploaded_file(@$_FILES["atachment"]['tmp_name'])){
	    			$this->attachment = $_FILES["atachment"];
	    			$this->attachment_uploaded = 1;
	    		}
	    	} else {
	    		$this->attachment = null;
	    		$this->attachment_uploaded = null;
	    	}
        } else {
        
        	if($this->isRepost && isset($oldid) && $oldid > 0 ){
				$sadd = new JAdsAddsman($database);
				$sadd->load($oldid);
				
				if( !$sadd->isMyAdd() ) {
					echo JText::_("ADS_ERROR_PERMISSIONS"); exit;
				}
				
				// IF republish : keep some files
        		$this->attachment = $sadd->atachment;
        		$add->atachment = $sadd->atachment;
        	}
        // new custom field prep
        	
			$end_hour = JRequest::getVar('end_hour', '00');
			$end_minutes = JRequest::getVar('end_minutes', '00');
						
			$add->end_date   = AdsUtilities::addDatetoIso($add->end_date);
			$add->end_date  .= " $end_hour:$end_minutes";
	        $add->start_date = AdsUtilities::addDatetoIso($add->start_date); //2012-09-29

            $my = JFactory::getUser();
            $add->userid = $my->id;

	    	if (isset($_FILES["atachment"])) {
	    		if (is_uploaded_file(@$_FILES["atachment"]['tmp_name'])){
	    			$this->attachment = $_FILES["atachment"];
	    			$this->attachment_uploaded = 1;
	    		}
	    	} else {
	    		$this->attachment = null;
	    		$this->attachment_uploaded = null;
	    	}
        }

    }

    function cancelAdd()
    {
    	$app = JFactory::getApplication();
    	
        @ignore_user_abort(true);
        $database = JFactory::getDbo();
        $add = new JAdsAddsman($database);
        $add->id = JRequest::getVar('id', null);
        
        $add->load($add->id);
        $add->closed = 1;
        $add->closed_date = date("Y-m-d H:i:s", time());
        if ($add->ancStore()) {
            // SEND MAIL
            $id_string = $add->userid;
            $query = "SELECT u.* FROM #__users u
					  WHERE u.id IN ($id_string)";
            $database->setQuery($query);
            $mails = $database->loadObjectList();
            
            $add->SendMails($mails, 'add_canceled');
            
            $msg = JText::_("ADS_SUCCESS_CANCEL_ADD");

        } else {
            $msg = JText::_("ADS_ERR_CANCEL_ADD");
        }

        return $msg;
        //$app->redirect( JRoute::_("index.php?option=com_adsman&amp;view=adsman&amp;task=listadds&amp"), $msg);
    }

    function deleteFiles($add_obj)
    {
        $my             = JFactory::getUser();
        $database       = JFactory::getDbo();
        $context_app    = JFactory::getApplication();
        
        $delete_pictures = JRequest::getVar('delete_pictures', null, 'POST', 'array' );
        $delete_main_picture = JRequest::getVar('delete_main_picture', '');
        $delete_atachment = JRequest::getVar('delete_atachment', '');
        $delete_video = JRequest::getVar('delete_video', '');

        $id = (int)$add_obj->id;
        if ($delete_main_picture) {
            $query = "select picture from #__ads where id=$id";
            $database->setQuery($query);
            $main_pic = $database->loadResult();

            if ($add_obj->status == 1) {
                $query = "update #__ads set picture = '' where id = $id";
            } else {
                if ($add_obj->status == 0) {
                    $query = "update #__ads set picture = null where id = $id";

                    @unlink(ADDSMAN_IMAGE_PATH . $main_pic);
                    @unlink(ADDSMAN_IMAGE_PATH . "small_" . $main_pic);
                    @unlink(ADDSMAN_IMAGE_PATH . "middle_" . $main_pic);
                }
            }
            $database->setQuery($query);
            $database->query();

            //@unlink(ADDSMAN_IMAGE_PATH . $main_pic);
            //@unlink(ADDSMAN_IMAGE_PATH . "small_" . $main_pic);
            //@unlink(ADDSMAN_IMAGE_PATH . "middle_" . $main_pic);
        }
        if ($delete_atachment) {
            $query = "update #__ads set atachment = '' where id = $id";
            $database->setQuery($query);
            $database->query();

            @unlink(ADDSMAN_IMAGE_PATH . "attach_{$id}.fil");
        }

		$user_pics_sql = " and userid=$my->id";
		if( $context_app->isAdmin() )
			$user_pics_sql = "";

        if($delete_pictures) {
	        foreach ($delete_pictures as $dele_id) {
	        		
	            $query = "select picture from #__ads_pictures where id=$dele_id {$user_pics_sql}";
	            $database->setQuery($query);
	            $pic = $database->loadResult();
	
	            if ($add_obj->status == 0) {
	                $query = "DELETE FROM #__ads_pictures WHERE id='$dele_id' {$user_pics_sql}";
                } else {
                    $query = "UPDATE #__ads_pictures SET picture='', published=0 WHERE id='$dele_id' {$user_pics_sql}";
                }

	            $database->setQuery($query);
	            $database->query();

                if ($add_obj->status == 0) {
                    @unlink(ADDSMAN_IMAGE_PATH . $pic);
                    @unlink(ADDSMAN_IMAGE_PATH . "small_" . $pic);//15_img_1.jpg
                    @unlink(ADDSMAN_IMAGE_PATH . "middle_" . $pic);
                }
	        }
        }
        
         if ($delete_video) {
            $database->setQuery("DELETE FROM #__ads_urls WHERE ad_id= $id ");
            $database->query();
         }
    }

    function uploadAtachment($add_obj)
    {
		
        $oldid = JRequest::getVar('oldid', 0);
    	
    	if (isset($this->attachment_uploaded) && $this->attachment_uploaded==1 ) {
        	
            $file_name = "attach_{$add_obj->id}.fil";
            $path = ADDSMAN_IMAGE_PATH . "$file_name";

            if (move_uploaded_file($this->attachment['tmp_name'], $path)) {
                $add_obj->atachment = $this->attachment["name"];
                $add_obj->store();
            }
            
        } elseif (isset($oldid) && $oldid>0 ) {
            
    		$old_file_name = "attach_{$oldid}.fil";
            $old_path = ADDSMAN_IMAGE_PATH . "$old_file_name";
            
            if(file_exists($old_path)) {
	    		$file_name = "attach_{$add_obj->id}.fil";
	            $path = ADDSMAN_IMAGE_PATH . "$file_name";
                copy($old_path, $path);
            }
    	}
    }

    function moveOldAdFiles( $add_obj, $oldid ,$delete_main_picture , $delete_pictures){
    	
        $context_app    = JFactory::getApplication();
        $my             = JFactory::getUser();
        $database       = JFactory::getDbo();
        
		$user_pics_sql = " and userid=$my->id";
		if ( $context_app->isAdmin() ) {
		  $user_pics_sql = "";
		}
        		 
        $database->setQuery("SELECT count(*) FROM #__ads WHERE id='$oldid' $user_pics_sql ");
        
        if (!$database->loadResult()) {
            JError::raiseWarning(1,JText::_("ALERTNOTAUTH"));
            exit;
        }

        $database->setQuery("SELECT picture FROM #__ads WHERE id=$oldid");
        $oldpic = $database->loadResult();

        $new_id = $add_obj->id;

        if (!empty($oldpic) && !$delete_main_picture) {
			if (file_exists(ADDSMAN_IMAGE_PATH . $oldpic)) {
                $new_pic_name = "main_{$new_id}." . AdsUtilities::get_file_ext($oldpic);
                copy(ADDSMAN_IMAGE_PATH . $oldpic, ADDSMAN_IMAGE_PATH . $new_pic_name);
                copy(ADDSMAN_IMAGE_PATH . "middle_$oldpic", ADDSMAN_IMAGE_PATH . "middle_$new_pic_name");
                copy(ADDSMAN_IMAGE_PATH . "resize_$oldpic", ADDSMAN_IMAGE_PATH . "resize_$new_pic_name");
                $add_obj->picture = $new_pic_name;
                $add_obj->store();
            }
        }

        if ($delete_main_picture) {
            $add_obj->picture = '';
            $add_obj->store();
        }

        $database->setQuery("SELECT * FROM #__ads_pictures WHERE id_ad=$oldid AND `published`=1 ");
        $pictures = $database->loadObjectList();
        for ($i = 0; $i < count($pictures); $i++) {
            $ext = AdsUtilities::get_file_ext($pictures[$i]->picture);
            if ($add_obj->isAllowedImage($ext) && !in_array($pictures[$i]->id, $delete_pictures)) {

                if (file_exists(ADDSMAN_IMAGE_PATH . $pictures[$i]->picture)) {
                    $pic = new JAdsPicture($database);

                    $pic->id_ad = $add_obj->id;
                    $pic->userid = $add_obj->userid;
                    $pic->modified = date('Y-m-d');
                    $pic->published = 1;
                    $pic->store();

                    $pic->picture = $add_obj->id . "_img_$pic->id.$ext";
                    $pic->store();

                    copy(ADDSMAN_IMAGE_PATH . $pictures[$i]->picture, ADDSMAN_IMAGE_PATH . $pic->picture);
                    copy(ADDSMAN_IMAGE_PATH . "middle_" . $pictures[$i]->picture, ADDSMAN_IMAGE_PATH ."middle_" . $pic->picture);
                    copy(ADDSMAN_IMAGE_PATH . "resize_" . $pictures[$i]->picture, ADDSMAN_IMAGE_PATH ."resize_" . $pic->picture);
                }
            }
        }
    }
    
    function uploadFiles(&$add_obj)
    {
        $my 				= JFactory::getUser();
        $context_app 		= JFactory::getApplication();
        $database 			= JFactory::getDbo();
        $user_pics_author 	= $my->id;
        $msg 				= "";

        if( $context_app->isAdmin() )
			$user_pics_author = $add_obj->userid;

		jimport('joomla.filesystem.file');
             
        $database->setQuery("SELECT id FROM #__ads_pictures WHERE id_ad=$add_obj->id AND `published`=1 ");
        $pictures = $database->loadResultArray();
        
         if ( !empty($pictures) ) {
         	$nrfiles = count($pictures);
         } else {
         	$nrfiles = 0;
         }

        require_once(JPATH_COMPONENT_SITE.DS.'thefactory'.DS.'front.images.php');
		$imgTrans = new JTheFactoryImages();

        $oldid = JRequest::getVar('oldid', 0);
        $delete_pictures = JRequest::getVar('delete_pictures', null, 'POST', 'array' );
        //$delete_pictures = JArrayHelper::getValue($_POST,'delete_pictures', null );
        $i = 0;
        
        if (!count($delete_pictures))
            $delete_pictures = array();

        $delete_main_picture 	= JRequest::getVar('delete_main_picture', '');
        $delete_atachment 		= JRequest::getVar('delete_atachment', '');
        $delete_video 			= JRequest::getVar('delete_video', '');

        if ($oldid)
            $this->isRepost = true;
        else
            $this->isRepost = false;
            
        if (!is_dir(ADDSMAN_IMAGE_PATH))
            @mkdir(ADDSMAN_IMAGE_PATH, 0755);

        if ($this->isRepost) {
        	$this->moveOldAdFiles( $add_obj, $oldid ,$delete_main_picture , $delete_pictures);
        }

        $isImageEnabled = price_pay_image_DBHelper::getEnabledPayImage();

        if ($isImageEnabled && isset($add_obj->id)) {
            $query = "SELECT id FROM #__ads_pictures WHERE id_ad = '".$add_obj->id."' AND `published` = 0 ";
		    $database->setQuery( $query );

            $deleted_images_id = $database->loadResultArray();
            $nrfiles_deleted = $add_obj->getCountDeletedImages();

        } else {
            $nrfiles_deleted = 0;
        }

        foreach ($_FILES as $k => $file) {
            
        	if (substr($k, 0, 7) != "picture")
                continue;
             
			if ( !isset($file['name']) || $file['name']=="") 
            	continue;

            if ( !is_uploaded_file(@$file['tmp_name']) ){
                continue;
            }
            
            if ( !ads_opt_resize_if_larger && filesize($file['tmp_name'] ) > ads_opt_max_picture_size * 1024 ) {
                continue;
            }

            $fname = $file['name'];
            $ext = AdsUtilities::get_file_ext($fname);

            if (!$add_obj->isAllowedImage($ext)) {
                $msg .= JText::_("ADS_ERR_EXTENSION_NOT_ALLOWED") . ': ' . $file['name'];
                continue;
            }
                     
            if ( $k == "picture" ) {
                $file_name = "main_{$add_obj->id}.{$ext}";

                $add_obj->picture = $file_name;
                $add_obj->store();
               
            } else {
              if ( isset($nrfiles) ) { //existing images

                if ($nrfiles >= ads_opt_maxnr_images)
                    continue;

                $pic = new JAdsPicture(JFactory::getDbo());

                if (isset($add_obj->id) && $nrfiles_deleted > 0 && isset($deleted_images_id[$i])) {
                    // update empty img records
                    $file_name 		= $add_obj->id . "_img_{$deleted_images_id[$i]}.$ext";

                    $pic->load($deleted_images_id[$i]);
                    $pic->picture 	= $file_name;
                    $pic->modified 	= date("Y-m-d H:i:s", time());
                    $pic->published	= 1;
                    $pic->store();

                    $nrfiles++;
                    $i++;

                } else {
                    $pic->id_ad = $add_obj->id;
                    $pic->userid 	= $user_pics_author;
                    $pic->picture 	= "temp";
                    $pic->modified 	= date("Y-m-d H:i:s", time());
                    $pic->published	= 1;
                    $pic->store();

                    $file_name 		= $add_obj->id . "_img_{$pic->id}.$ext";
                    $pic->picture 	= $file_name;
                    $pic->store();
                    $nrfiles++;
                }

              }
            }

            $path = ADDSMAN_IMAGE_PATH . "/$file_name";
            
            if ( ads_opt_resize_if_larger && filesize($file['tmp_name']) > ads_opt_max_picture_size * 1024 )
            {
				$res = $imgTrans->resize_to_filesize($file['tmp_name'], $path, ads_opt_max_picture_size * 1024);
            }
            else
            {
				$res = move_uploaded_file($file['tmp_name'], $path);
            }

            if ($res) {
				@chmod($path, 0755);
                
				$s = $imgTrans->resize_image(ADDSMAN_IMAGE_PATH.$file_name, ads_opt_thumb_width, ads_opt_thumb_height, 'resize');
				
				if(!$s) {
					$msg .= $file['name'] . " thumb - " . JText::_("ADS_ERR_LOADING_PICTURE") . "<br><br>";
				}
				
				$s = $imgTrans->resize_image(ADDSMAN_IMAGE_PATH.$file_name, ads_opt_medium_width, ads_opt_medium_height, 'middle');
				
				if(!$s){
					$msg .= $file['name'] . " middle - " . JText::_("ADS_ERR_LOADING_PICTURE") . "<br><br>";
				}

            } else {
                $msg .= $file['name'] . " - " . JText::_("ADS_ERR_UPLOAD_FAILED") . "<br><br>";
            }
        }

        return $msg;

    }

    function validateSave($add)
    {
		$error = null;
		// Title validation
		if ($add->title == "" || !isset($add->title))
			$error .= "  " . JText::_("ADS_ERR_ENTER_TITLE");
			
		// Description validation	
		if ($add->description == "" || !isset($add->description))
			$error .= "  " . JText::_("ADS_ERR_ENTER_DESCRIPTION");

		// Dates validations
		if (!$add->id) {
			
			$start_date = $add->start_date;
			$end_date   = $add->end_date;
			
			if (!$start_date) { 
				$error .= JText::_("ADS_ERR_START_DATE_INVALID"); 
			}
			
			if (!$end_date) { 
				$error .= JText::_("ADS_ERR_END_DATE_INVALID");
			}

			if ( strlen($start_date) != 10 ) { 
				$error.= JText::_("ADS_ERR_START_DATE_INVALID_FORMAT") ." <br /> ";
			}
			
			if( ads_opt_enable_hour==1 ){
				if ( strlen($end_date) != 16 ){ 
					$error.= JText::_("ADS_ERR_END_DATE_INVALID_FORMAT") ." <br /> ";
				}
			}
			
			$year 		= date("Y",strtotime($start_date));
			$month 		= date("m",strtotime($start_date));
			$day 		= date("d",strtotime($start_date));
			$year_end 	= date("Y",strtotime($end_date));
			$month_end 	= date("m",strtotime($end_date));
			$day_end 	= date("d",strtotime($end_date));
			
			if ($year<1900 && $year>2200) {
				$error.= JText::_("ADS_ERR_START_DATE_INVALID_YEAR") ." <br /> ";
			}
			if($month<1 && $month>12) {
				$error.= JText::_("ADS_ERR_START_DATE_INVALID_MONTH") ." <br /> ";
			}
			if($day<1 && $day>31) {
				$error.= JText::_("ADS_ERR_START_DATE_INVALID_DAY") ." <br /> ";
			}

			if($year_end<1900 && $year_end>2200) {
				$error.= JText::_("ADS_ERR_END_DATE_INVALID_YEAR") ." <br /> ";
			}
			if($month_end<1 && $month_end>12) {
				$error.= JText::_("ADS_ERR_END_DATE_INVALID_MONTH") ." <br /> ";
			}
			if($day_end<1 && $day_end>31) {
				$error.= JText::_("ADS_ERR_END_DATE_INVALID_DAY") ." <br /> ";
			}

			$datedif = mktime(0,0,0,$month_end,$day_end,$year_end) - mktime(0,0,0,$month,$day,$year);

			if( $datedif < 0 ){
				$error.= JText::_("ADS_ERR_DATES_INVALID") ." <br /> ";
			}
		
			/**
			 * SOON
			 * 
			if(ads_opt_max_allow_valability >0)
			if( floor($datedif/60/60/24)>=ads_opt_max_allow_valability*31){
				$error_msg.=ads_not_valid_interval.": ".ads_opt_max_allow_valability."\n";
			}
			*/
		}	
	
		if( defined('ads_allow_atachment') && ads_allow_atachment == 1 ){

			if ( isset($this->attachment) ) {
            	if ($this->attachment != null && $this->attachment_uploaded) {
               	
               		if ( is_array($this->attachment) )
						$ext = AdsUtilities::get_file_ext($this->attachment['name']);
					else 	
						$ext = AdsUtilities::get_file_ext($this->attachment);
						
					if ( !in_array($ext, explode(",",ads_allowed_attachments) ) ) {
						
						$error .= JText::_("ADS_ERR_EXTENSION_NOT_ALLOWED").sprintf(JText::_("ADS_ERR_ALLOWED_EXT"),ads_allowed_attachments)." !"."<br />\r\n";
						
						unset($this->attachment);
						unset($this->attachment_uploaded);
					}
            	}
        	}
		}
		
		// Attachment validation
		if ( defined('ads_allow_atachment') && ads_allow_atachment==1 && defined('ads_require_atachment') && ads_require_atachment==1 ){
			if( !$this->attachment_uploaded && !$this->attachment )
				$error .= JText::_("ADS_ERR_ENTER_ATTACHMENT") . "<br /><br />";
		}
		
		// Image Validation
		$nrfiles=0;
		
		if (count($_FILES))
		foreach ($_FILES as $k=>$file) {
			
        	if (substr($k, 0, 7) != "picture")
				continue;
             
			if ( !isset($file['name']) || $file['name']=="") 
				continue;
					
            if ( !is_uploaded_file(@$file['tmp_name']) ) {
                
               	//$error .= $file['name'] . "-- " . JText::_("ADS_ERR_UPLOAD_FAILED") . "<br><br>";
                continue;
            }
           
            if ( !ads_opt_resize_if_larger && filesize($file['tmp_name'] ) > ads_opt_max_picture_size * 1024 ) {
                $error .= $file['name'] . "- " . JText::_("ADS_ERR_IMAGESIZE_TOO_BIG") . "<br><br>";
                continue;
            }

            $fname = $file['name'];

            $ext = AdsUtilities::get_file_ext($fname);

            if (!$add->isAllowedImage($ext)) {
                $error .= JText::_("ADS_ERR_EXTENSION_NOT_ALLOWED") . ': ' . $file['name'];
                continue;
            }
			$nrfiles++;
		}
		
		if ( !$add->id && !$this->isRepost && !$nrfiles && (ads_opt_require_picture == '1') ){
			$error .= JText::_("ADS_ERR_UPLOAD_PICTURE") . "<br /><br />";
		}
		// <= image validation
	
		return $error;
    }
    
    function saveUrls($addid,$video_url)
    {
    	JTable::addIncludePath(JPATH_SITE.DS.'administrator'.DS.'components'.DS.'com_adsman'.DS.'tables');
	    $urls_table =& JTable::getInstance('Urls', 'Table');
       
	    $video_url_test = JRequest::getVar('video-link', '');
	    $err = true;
	    
		$url = array();
	        
        $url['video_link'] 	= JRequest::getVar('video-link','');
        $url['ad_id']		= $addid;
       
        if ( $url['video_link'] != '') {

		  if(function_exists("curl_init")) {
			
		    $vlink = $url['video_link']; /* gets the data from a URL */
	 		$ch = curl_init();
			$timeout = 30;
			curl_setopt($ch,CURLOPT_URL,$vlink);
			curl_setopt($ch,CURLOPT_RETURNTRANSFER,1); /*Set curl to return the data instead of printing it to the browser*/
			curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
			curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
			
			$result_video_link = curl_exec($ch); /*Execute the fetch */
			
			$err     = curl_errno( $ch );
	    	$errmsg  = curl_error( $ch );
			curl_close($ch);
	
		
		} else {
			if (function_exists('file_get_contents')) {
			 
				$result_video_link = @file_get_contents($url['video_link']);
			}
		}


		preg_match('#<meta([^>]*)name="([^>\"]*)title"([^>]*)>#i', 			$result_video_link, $matchesVideoTitle);												
		preg_match('#<meta([^>]*)name="([^>\"]*)description"([^>]*)>#i', 	$result_video_link, $matchesVideoDescription);
		preg_match('#<link([^>]*)rel="([^>\"]*)image_src"([^>]*)>#i', 		$result_video_link, $imgVideoSourceThumb);
		preg_match('#<link([^>]*)rel="([^>\"]*)video_src"([^>]*)>#i', 		$result_video_link, $matchesVideoSource);
		
		preg_match('#<meta([^>]*)property="([^>\"]*)og:site_name"([^>]*)>#i', 	$result_video_link, $matchesVideoSitename);
		preg_match('#<meta([^>]*)property="([^>\"]*)og:url"([^>]*)>#i', 		$result_video_link, $matchesVideoUrl);
		preg_match('#<meta([^>]*)property="([^>\"]*)og:title"([^>]*)>#i', 		$result_video_link, $matchesVideoTitle_og);
		preg_match('#<meta([^>]*)property="([^>\"]*)og:image"([^>]*)>#i', 		$result_video_link, $matchesVideoThumbnail);
		preg_match('#<meta([^>]*)property="([^>\"]*)og:description"([^>]*)>#i', $result_video_link, $matchesVideoDescription_og);

		$videotags = get_meta_tags($url['video_link']);

		$url['video_title'] = '';
		$url['video_description'] = '';
		$url['video_thumbnail'] = '';
		$url['video_sitename'] = '';
		$url['video_sourceThumb'] = '';
		
		$db = JFactory::getDBO();
		
		if (!empty($matchesVideoSitename)) {
		  foreach ($matchesVideoSitename as $i=>$val) {
		
		  	$count[$i] = substr_count($val, ' content=');
		  	
			if ($count[$i] > 0) {
				$matchesSitename = ltrim($matchesVideoSitename[$i], ' content="/');
				$matchesSitename = ltrim($matchesSitename, ' "\/');
				$matchesSitename = rtrim($matchesSitename, ' "/');
				$url['video_sitename'] = $db->getEscaped($matchesSitename);
			}

		  }
			
		  if ( ($url['video_sitename'] == 'YouTube') || ($url['video_sitename'] == 'Vimeo') ) {
			//get URL
			if (!empty($matchesVideoUrl)) {
				foreach ($matchesVideoUrl as $i=>$val) {
					
					$count[$i] = substr_count($val, ' content="');
					if ($count[$i] > 0) {
						$matchesUrl = ltrim($matchesVideoUrl[$i], ' content="/');
						$matchesUrl = ltrim($matchesUrl, ' "\/');
						$matchesUrl = rtrim($matchesUrl, ' "/');
						$url['video_link'] = $db->getEscaped($matchesUrl);
					}
				}
			}
		
			//get title
			if (!empty($matchesVideoTitle_og)) {
				foreach ($matchesVideoTitle_og as $i=>$val) {
					
					$count[$i] = substr_count($val, ' content="');
					if ($count[$i] > 0) {
						$matchesTitle = ltrim($matchesVideoTitle_og[$i], ' content="/');
						$matchesTitle = ltrim($matchesTitle, ' "\/');
						$matchesTitle = rtrim($matchesTitle, ' "/');
						$url['video_title'] = $db->getEscaped($matchesTitle);
					}
				}
			}

			// if null get meta name=title
			
			//get image 
			if (!empty($matchesVideoThumbnail)) {
				foreach ($matchesVideoThumbnail as $i=>$val) {
					
					$count[$i] = substr_count($val, ' content="');
					if ($count[$i] > 0) {
						$matchesThumbnail = ltrim($matchesVideoThumbnail[$i], ' content="/');
						$matchesThumbnail = ltrim($matchesThumbnail, ' "\/');
						$matchesThumbnail = rtrim($matchesThumbnail, ' "/');
						$matchesThumbnail = ltrim($matchesThumbnail, ' "https:\/\/');
						$matchesThumbnail = ltrim($matchesThumbnail, ' "http:\/\/');
						$url['video_thumbnail'] = $db->getEscaped($matchesThumbnail);
					}
				}
			}

			//get description
			if (!empty($matchesVideoDescription_og)) {
				foreach ($matchesVideoDescription_og as $i=>$val) {
					
					$count[$i] = substr_count($val, ' content="');
					if ($count[$i] > 0) {
						$matchesDescription = ltrim($matchesVideoDescription_og[$i], ' content="/');
						$matchesDescription = ltrim($matchesDescription, ' "\/');
						$matchesDescription = rtrim($matchesDescription, ' "/');
						$url['video_description'] = $db->getEscaped($matchesDescription);
					}
				}
			}
				
			} else {
				// if Metacafe
				if ($url['video_sitename'] == 'Metacafe') {
			
				//get URL
				if (!empty($matchesVideoSource)) {
					foreach ($matchesVideoSource as $i=>$val) {
						
						$count[$i] = substr_count($val, ' href=');
						if ($count[$i] > 0) {
							$matchesVideo = ltrim($matchesVideoSource[$i], '  href=');
							$matchesVideo = ltrim($matchesVideo, ' "\/');
							$matchesVideo = rtrim($matchesVideo, ' "/');
							$url['video_link'] = $db->getEscaped($matchesVideo);
						}
					}
				} 
				
				//get title
				if (!empty($matchesVideoTitle)) {
					foreach ($matchesVideoTitle as $i=>$val) {
							
						$count[$i] = substr_count($val, ' content="');
						if ($count[$i] > 0) {
							$matchesTitle = ltrim($matchesVideoTitle[$i], ' content="/');
							$matchesTitle = ltrim($matchesTitle, ' "\/');
							$matchesTitle = rtrim($matchesTitle, ' "/');
							$url['video_title'] = $db->getEscaped($matchesTitle);
						}
					}
				} 
				
				//get image 		
				if (!empty($imgVideoSourceThumb)) {	
					foreach ($imgVideoSourceThumb as $i=>$val) {
						
						$count[$i] = substr_count($val, 'href');
						if ($count[$i] > 0) {
							$imgSourceThumb = ltrim($imgVideoSourceThumb[$i], ' href=');
							$imgSourceThumb = ltrim($imgSourceThumb, ' "\/');
							$imgSourceThumb = rtrim($imgSourceThumb, ' "/');
							$url['video_sourceThumb'] = $db->getEscaped($imgSourceThumb);
						}
					}
				}
						
				//get description	
				if (!empty($matchesVideoDescription)) {
					foreach ($matchesVideoDescription as $i=>$val) {
						
						$count[$i] = substr_count($val, ' content="');
						if ($count[$i] > 0) {
							$matchesDescription = ltrim($matchesVideoDescription[$i], ' content="/');
							$matchesDescription = ltrim($matchesDescription, ' "\/');
							$matchesDescription = rtrim($matchesDescription, ' "/');
							$url['video_description'] = $db->getEscaped($matchesDescription);
						}
					}
				}
				}
		}
		
	}	else {
		
		//get URL
		if (!empty($matchesVideoSource)) {
			foreach ($matchesVideoSource as $i=>$val) {
		
				$count[$i] = substr_count($val, ' href=');
								
				if ($count[$i] > 0) {
					$matchesVideo = ltrim($matchesVideoSource[$i], ' href=');
					$matchesVideo = ltrim($matchesVideo, ' "\/');
					$matchesVideo = rtrim($matchesVideo, ' "/');
					$url['video_link'] = $db->getEscaped($matchesVideo);
				}
			}
		}
		
		//get title	
		if (!empty($matchesVideoTitle)) {
			foreach ($matchesVideoTitle as $i=>$val) {
		
			$count[$i] = substr_count($val, ' content="');
			if ($count[$i] > 0) {
				$matchesTitle = ltrim($matchesVideoTitle[$i], ' content="/');
				$matchesTitle = ltrim($matchesTitle, ' "\/');
				$matchesTitle = rtrim($matchesTitle, ' "/');
				$url['video_title'] = $db->getEscaped($matchesTitle);
			}
		}
		} else {
			if (!empty($matchesVideoTitle_og)) {
				foreach ($matchesVideoTitle_og as $i=>$val) {
				
					$count[$i] = substr_count($val, ' content="');
					if ($count[$i] > 0) {
						$matchesTitle2 = ltrim($matchesVideoTitle_og[$i], ' content="/');
						$matchesTitle2 = ltrim($matchesTitle2, ' "\/');
						$matchesTitle2 = rtrim($matchesTitle2, ' "/');
						$url['video_title'] = $db->getEscaped($matchesTitle2);
					}
				}
			}
		}	
		
		//get description	
		if (!empty($matchesVideoDescription)) {
			foreach ($matchesVideoDescription as $i=>$val) {
			
				$count[$i] = substr_count($val, ' content="');
				if ($count[$i] > 0) {
					$matchesDescription = ltrim($matchesVideoDescription[$i], ' content="/');
					$matchesDescription = ltrim($matchesDescription, ' "\/');
					$matchesDescription = rtrim($matchesDescription, ' "/');
					$url['video_description'] = $db->getEscaped($matchesDescription);
				}
			}
		}
			
		//get image 			
		if (!empty($matchesVideoThumbnail)) {
		  foreach ($matchesVideoThumbnail as $i=>$val) {
		
			$count[$i] = substr_count($val, ' content="');
			if ($count[$i] > 0) {
				$matchesThumbnail = ltrim($matchesVideoThumbnail[$i], ' content="/');
				$matchesThumbnail = ltrim($matchesThumbnail, ' "\/');
				$matchesThumbnail = rtrim($matchesThumbnail, ' "/');
				$url['video_thumbnail'] = $db->getEscaped($matchesThumbnail);
			}
		  }
		}	else {
			if (!empty($imgVideoSourceThumb)) {	
				foreach ($imgVideoSourceThumb as $i=>$val) {
					$count[$i] = substr_count($val, 'href');
						if ($count[$i] > 0) {
						$imgSourceThumb = ltrim($imgVideoSourceThumb[$i], ' href=');
						$imgSourceThumb = ltrim($imgSourceThumb, ' "\/');
						$imgSourceThumb = rtrim($imgSourceThumb, ' "/');
						$url['video_sourceThumb'] = $db->getEscaped($imgSourceThumb);
					}
				}
			}
		}

		}

	}

    if ( $url['video_link'] != "" ) {

    	$url_query = ' SELECT id from #__ads_urls  '
		    			. ' WHERE ad_id = '.$addid;
		 
	    $this->_db->setQuery($url_query);
    	$result = $this->_db->loadResult();

    	if (!$result) {
	    		
	    	if (!$urls_table->bind($url))
		    {
		      $this->setError($this->_db->getErrorMsg());
		      return false;
		    }
		
		    if (!$urls_table->check())
		    {
		      $this->setError($this->_db->getErrorMsg());
		      return false;
		    }
		
		    if (empty($urls_table->id))
		    {
		      $isNew = true;
		      $urls_table->date_added = date('Y-m-d H:i:s');
		    }
		    
		    if (!$urls_table->store())
		    {
		      $this->setError($this->_db->getErrorMsg());
		      return false;
		    }
		    
	   } else {
	   		//update
	 		$urls_table->load($result);	

	    	if (!$urls_table->bind($url))
		    {
		      $this->setError($this->_db->getErrorMsg());
		      return false;
		    }
	    	
	    	if (!$urls_table->store())
		    {
		      $this->setError($this->_db->getErrorMsg());
		      return false;
		    }
	   }

	   return true;
    }    

    }
    
    function getPagination()
    {
    	
        // Lets load the content if it doesn't already exist
        if (empty($this->_pagination)) {
            jimport('joomla.html.pagination');
            $this->_pagination = new JPagination($this->getTotal(), $this->getState('limitstart'), $this->getState('limit'));
        }
        return $this->_pagination;
    }
}

?>
