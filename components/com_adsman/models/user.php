<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

// no direct access
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );
jimport( 'joomla.application.component.helper' );
require_once (JPATH_ADMINISTRATOR.DS.'components'.DS.'com_adsman'.DS.'tables'.DS.'adsuser.php');

/**
 * @package		Ads manager
 */
class adsModelUser extends JModel
{
	var $_name='User';
	
	function getUserData($id)
	{
		$user = new JAdsUsers($this->_db);
		$user = $user->getUserDetails( $id );
		$user->id = $id;
        return $user;
	}
	
	function saveUserDetails(){
		$my = JFactory::getUser();
		$database = JFactory::getDbo();
		$user = new JAdsUsers($database);
		
		if (!$user->load($my->id)){
			//no record yet
			$user->set("_tbl_key","id");
			$user->id=null;
		}else{
			$user->set("_tbl_key","userid");
		}
		
		$user->bind(JRequest::get('POST'));
        //$posted_pm_values = JRequest::get('POST');
		$user->userid=$my->id;
		
		$date = JFactory::getDate(time());
		$user->modified = $date->toMySQL();
		$s = $user->store();

		return $s;
	}
}


?>