	jQueryFactory(document).ready( function ($) {

	var link = root + "index.php?option=com_adsman&task=add_credits_calculate&format=raw";
	var theForm = f = document.forms["adsForm"];

	var ad_valability = -1;
	var add_pictures = 0;
	var category = -1;
    

    $('input:checkbox[name="delete_pictures[]"]').click(function() {
        if ($(this).is(':checked')) {
            count_delete_images++;
        } else {
            if (count_delete_images > 0)
                count_delete_images--;
            else
                count_delete_images = 0;
        }
    });

  	$('input[name="save"]').live('click', function(event) {

        var if_valid = myValidate(f);

	   	if (if_valid == false) {
	   		return false;
	   	} else {
	   	  if (was_payed_listing) {
	   		event.preventDefault();

            var ad_id = $('input[name="id"]').attr('value');
			var selected_main_img1 = document.getElementById('delete_main_picture');
            var add_main_img = 0;

            if ( typeof(selected_main_img1) != "undefined" && selected_main_img1 != null) {
               if (document.getElementById('delete_main_picture').value == "1") {
                    add_main_img = 0;
               }
            } else {
                if (document.getElementById('picture').getTextInRange() == "") {
                    add_main_img = 0;
                } else {
                    add_main_img = 1;
                }
            }

		   	var image_credit = document.getElementById('files_list3');
		   	if (typeof(image_credit) != "undefined" && image_credit != null) {
                // added some pictures
		   		var uploaded_pictures = image_credit.childElementCount;

                if (typeof(uploaded_pictures) != "undefined" && uploaded_pictures != 0)
                   add_pictures = uploaded_pictures;
                else
                   add_pictures = 0;
		   	}

	   		var is_selected_feature = $('input[name="featured"]').is(':checked');
		   	if (is_selected_feature == true )
		   		var selected_feature = $('input[name="featured"]:checked').val();
		   	else
		   		var selected_feature = 'none';

		   	var ad_valability_element = theForm.elements["ad_valability"];
		   	if (ad_valability_element != null) {
		   		ad_valability = theForm.elements["ad_valability"].selectedIndex;
		   	}

		   	var ad_category_element = theForm.elements["category"];
		   	if (ad_category_element != null) {
		   		ad_category = theForm.elements["category"].selectedIndex;
		   	}


	   		$.ajax({
			  type: 'POST',
			  url: link,
			  data: "ad_featured="+ad_featured+"&add_main_img="+add_main_img+"&add_pictures="+add_pictures+"&count_delete_images="+count_delete_images+"&ad_id="+ad_id+"&selected_feature="+selected_feature+"&ad_valability="+ad_valability+"&ad_category="+ad_category,
			  async:false,

			  success:function(data) {

				if (data == 1)
					// user has enough credits
					document.adsForm.submit();
				else  {
					// user doesn't have enough credits
                    alert('Not enough credits for new features. Remove new added Feature.');
					//$('#credits_info').empty();
					//$('#credits_info').append("<h3> You don't have enough credits </h3>");
					};

				},

			  error:function(){
				    // failed request; give feedback to user
			    $('#credits_info').html('<p class="error"><strong>Oops!</strong> Try that again in a few moments.</p>');
			  }
		    }, "json");
		    return false;

             }
           }
    });
	
});


function myValidate(f) {
	
	// If price is required
	if ( require_price )
		var adprice = document.getElementById('askprice').value;
	else 
		var adprice = "";
			
	if (adprice == "" && require_price) {
		alert(language["adsman_err_price"]);
		return false;
	}
	// check attachment extension	 

     var file_ext1 = document.getElementById('atachment');

     if ( typeof(file_ext1) != "undefined" && file_ext1 != null) {
         var file_ext = document.getElementById('atachment').value;

         if (file_ext != "") {

             if (dim_attachments > 0) {

                var ext = (/[.]/.exec(file_ext)) ? /[^.]+$/.exec(file_ext) : undefined;

                var inArray = false;

                if (allowed_attachments.match(ext) != null) {
                    inArray = true;
                }

                if ( inArray == false ) {
                    alert(language["adsman_err_extension"]);
                    return false;
                }

             } else {
                return false;
             }
         }
     }

    // If terms required
	if ( require_terms )
		var terms = document.getElementById('terms').checked;
	else 
		var terms = 1;
			
	if(!terms) {
		alert(language["adsman_err_terms"]);
		return false;
	}
		
	else
	{
		editorSave();
	       if (document.formvalidator.isValid(f)) {
			document.getElementById('invalid_info').innerHTML = "";
			document.getElementById('invalid_info').style.display="none";
			return true;
	       }
	       else {
        		var ErrrorMsg = "<img src='"+ROOT_HOST+"components/com_adsman/img/con_info.png'  class='ads_noborder' />&nbsp;<br />"+language["adsman_fill_in_fields"]+"<br />";
	        	ErrrorMsg += buildMessageQueue(f);
				document.getElementById('invalid_info').style.display="";
				document.getElementById('invalid_info').innerHTML = ErrrorMsg;
				alert(language["adsman_fill_in_fields"]);
	        }
	        return false;
	}

    //compare dates
    var end_date = document.getElementById('end_date').value;
    var start_date = document.getElementById('start_date').value;

    if (isNaN(start_date) || isNaN(end_date)) {
        // error date;
        alert(language["adsman_err_dates_invalid"]);
        return false;
    }

    if (start_date > end_date) {
        alert('Dates Invalid (end date is before start date or current time )');
        return false;
    }

}

function closeIframe(){
	this.window.close();
}

