var mapsArray=[];
var sidebarArray=[];


function load_gmaps(maptype,mapcenter,zoom,canvasid,canvas_width,canvas_height,mapname) {
    var myOptions = {
      center: new google.maps.LatLng(mapcenter.pointTox, mapcenter.pointToy),
      zoom: zoom,
      mapTypeId: maptype
    };
    if (typeof mapname === 'undefined') mapname='listmap';
    
    map_canvas=document.getElementById(canvasid);
    if (!map_canvas) return;
    map_canvas.setAttribute("style","width:"+canvas_width+"px");
    map_canvas.setAttribute("style","height:"+canvas_height+"px");
    map = new google.maps.Map(map_canvas,myOptions);
    infoWindow = new google.maps.InfoWindow();    
    mapsArray.push({mapname:mapname,map:map,markers:[],infoWindow:infoWindow});

    $$("dl.tabs>dt").each(function(el){
       el.addEvent('click',function(){
                setTimeout(gmap_refreshallmaps,500);
       }); 
    });    
    
}
Array.prototype.gmap_findmap=function (mapname){
    if (typeof mapname === 'undefined') mapname='listmap';
    for (var i = 0; i < this.length; i++ ) 
        if(this[i].mapname==mapname) return this[i].map;
    
    return null;
}
Array.prototype.gmap_infowindow=function (mapname){
    if (typeof mapname === 'undefined') mapname='listmap';
    for (var i = 0; i < this.length; i++ ) 
        if(this[i].mapname==mapname) return this[i].infoWindow;
    
    return null;
}
Array.prototype.gmap_markers=function (mapname){
    if (typeof mapname === 'undefined') mapname='listmap';
    for (var i = 0; i < this.length; i++ ) 
        if(this[i].mapname==mapname) return this[i].markers;
    
    return null;
}
var gmap_refreshallmaps=function ()
{
    for (var i = 0; i < mapsArray.length; i++ ) {
        google.maps.event.trigger(mapsArray[i].map, 'resize');
    }
}
var gmap_selectposition=function ()
{
    map=mapsArray.gmap_findmap(mapname);
    markersArray=mapsArray.gmap_markers(mapname);
    
    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(mapcenter.pointTox, mapcenter.pointToy),
      map: map,
      draggable:true,
      title:language["ads_current_position"]
    });
    markersArray.push(marker);
    
    document.getElementById('gmap_posx').value=marker.getPosition().lat();
    document.getElementById('gmap_posy').value=marker.getPosition().lng();
    google.maps.event.addListener(marker, 'position_changed', function() {
        document.getElementById('gmap_posx').value=marker.getPosition().lat();
        document.getElementById('gmap_posy').value=marker.getPosition().lng();
      });
    google.maps.event.addListener(map, 'click', function(mouseEvent) {        
            marker.setPosition(mouseEvent.latLng);
      });
    
}
function submit_gmap_coords()
{
	var window = this.opener;
	var xcoord = window.document.getElementById("googleX");
	var ycoord = window.document.getElementById("googleY");
	xcoord.value = document.getElementById("gmap_posx").value;
	ycoord.value = document.getElementById("gmap_posy").value;
	this.close();
}
var gmap_centermap=function (mapcenter,mapname,draggable)
{
    if (typeof mapname === 'undefined') mapname='listmap';
    if (typeof draggable === 'undefined') draggable=false;
    
    markersArray=mapsArray.gmap_markers(mapname);
    map=mapsArray.gmap_findmap(mapname);
    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(mapcenter.pointTox, mapcenter.pointToy),
      map: map,
      draggable:draggable,
      title:language["ads_current_position"]
    });
    map.setCenter(marker.position);
    markersArray.push(marker);    
    if(draggable){
          infoWindow=mapsArray.gmap_infowindow(mapname);
          infoWindow.setContent(language["ads_drag_to_change_position"]);
          infoWindow.setPosition(marker.position);
          infoWindow.open(map);
          
          google.maps.event.addListener(marker, "dragend", function(latlng) {
            gx=document.getElementById('googleX');
            gy=document.getElementById('googleY');
            var point = marker.getPosition();
            gx.value=point.lat();
            gy.value=point.lng();
            infoWindow.setPosition(marker.position);
            map.setCenter(marker.position);
          });  
    }
}   
var gmap_move_marker=function(mapcenter,mapname)
{
    if (typeof mapname === 'undefined') mapname='listmap';
    map=mapsArray.gmap_findmap(mapname);
    gmap_clearmarkers(mapname);
    gmap_centermap(mapcenter,mapname,true);

    gx=document.getElementById('googleX');
    gy=document.getElementById('googleY');
    gx.value=mapcenter.pointTox;
    gy.value=mapcenter.pointToy;
} 
var gmap_refreshAds=function()
{
    $("wait_loader").setStyle('display','block');
    limit = $("ads_limitbox").value;
   	cat   = $("category").value;
    searchUrl = 'index.php?option=com_adsman&task=showBookmarks&limit='+limit+'&cat='+cat;
    gmap_clearmarkers('listmap');
    new Request({
        method: 'GET', 
        url: searchUrl,
        onSuccess: function(data) {
            $("wait_loader").setStyle('display','none');
            gmap_putmarkers(data,'listmap');
        },
        onFailure: function(){
            $("wait_loader").setStyle('display','none');
        }
    }).send();   
}
var gmap_refreshAds_search=function()
{
    map=mapsArray.gmap_findmap('searchmap');
    markersArray=mapsArray.gmap_markers('searchmap');

    $("wait_loader2").setStyle('display','block');
    address = $('addressInput').value;

    $('search_sidebar').set('html','');
    sidebarArray=[];
    
    gmap_clearmarkers('searchmap');
    
    geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'address': address },function(results, status){
        if (! results) {
            $('search_sidebar').set('html',language["bid_noresults"]);
        }else
            if (status == google.maps.GeocoderStatus.OK) {
    		radius = $('radiusSelect').value;
            for (var i = 0; i < results.length; i++) {
                center=results[i].geometry.location;
                if (i==0) map.panTo(center);
                var circle=new google.maps.Circle({
                    center:center,
                    radius:radius*1000,
                    map:map,
                    fillOpacity:0.3,
                    fillColor:'#fff1e6',
                    editable:false   
                });
                markersArray.push(circle);
                searchUrl = 'index.php?option=com_adsman&task=searchBookmarks&lat=' + center.lat() + '&lng=' + center.lng() + '&radius=' + radius;
                new Request({
                    method: 'GET', 
                    url: searchUrl,
                    onSuccess: function(data) {
                        gmap_putmarkers(data,'searchmap');
                        gmap_putsidebar(data);
                        $("wait_loader2").setStyle('display','none');
                    },
                    onFailure: function(){
                        $("wait_loader2").setStyle('display','none');
                    }
                }).send();   
            }             
        }else{
            $('search_sidebar').set('html',language["bid_noresults"]);
        }
    });
    
    
}
function gmap_putmarkers(xmldata,mapname)
{
    if (typeof mapname === 'undefined') mapname='listmap';
    map=mapsArray.gmap_findmap(mapname);
    infoWindow=mapsArray.gmap_infowindow(mapname);
    markersArray=mapsArray.gmap_markers(mapname);
    
    var xml = parseXml(xmldata);
    var markers = xml.documentElement.getElementsByTagName("marker");
    for (var i = 0; i < markers.length; i++) {
        var marker = new google.maps.Marker({
          position: new google.maps.LatLng(parseFloat(markers[i].getAttribute('lat')), parseFloat(markers[i].getAttribute('lng'))),
          map: map,
          draggable:false,
          title:markers[i].getAttribute('name'),
          info:markers[i].getAttribute('info')
        });
        google.maps.event.addListener(marker,'click', function () {
           infoWindow.setContent(this.info);
           infoWindow.setPosition(this.position);
           infoWindow.open(this.map);
        });
        markersArray.push(marker);
    }
}
function gmap_putsidebar(xmldata)
{
    map=mapsArray.gmap_findmap('searchmap');
    infoWindow=mapsArray.gmap_infowindow('searchmap');

    var xml = parseXml(xmldata);
    var markers = xml.documentElement.getElementsByTagName("marker");
    for (var i = 0; i < markers.length; i++) {
        if (sidebarArray[markers[i].getAttribute('id')]) continue;        
        var div = new Element('div', {class: 'sidebar_element'});
        cell='';
        if (markers[i].getAttribute('img')) cell+='<img src="' + markers[i].getAttribute('img')+ '" />';
        cell+='<strong>'+markers[i].getAttribute('name')+'</strong>';
        div.set('html',cell);
        div.set('adid',markers[i].getAttribute('id'));
        sidebarArray[markers[i].getAttribute('id')]={
            info:markers[i].getAttribute('info'),
            position:new google.maps.LatLng(parseFloat(markers[i].getAttribute('lat')), parseFloat(markers[i].getAttribute('lng'))),
            id:markers[i].getAttribute('id')
        };
        div.addEvent('click',function(el){
           adid=this.get('adid');
           marker=sidebarArray[adid];
           infoWindow.setContent(marker.info);
           infoWindow.setPosition(marker.position);
           infoWindow.open(map);
        })
        div.inject($('search_sidebar'));
    }       
}
function gmap_clearmarkers(mapname)
{
  markersArray=mapsArray.gmap_markers(mapname);
  infoWindow=mapsArray.gmap_infowindow(mapname);
  if (markersArray) 
    for (var i = 0; i < markersArray.length; i++ ) {
      markersArray[i].setMap(null);
    }
  markersArray=[];
  infoWindow.close()
    
}
function parseXml(str) {
  if (window.ActiveXObject) {
    var doc = new ActiveXObject('Microsoft.XMLDOM');
    doc.loadXML(str);
    return doc;
  } else if (window.DOMParser) {
    return (new DOMParser).parseFromString(str, 'text/xml');
  }
}
