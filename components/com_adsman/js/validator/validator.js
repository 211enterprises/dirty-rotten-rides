//
// Invalid Fields Detailed Message
//
function buildMessageQueue(f){
	var ErrrorMsg = "";
	// for radio ,checklists not to be displayed more than once
	var CheckedList = new Array();
	var oo=0;
	for (var i=0;i < f.length; i++) {
		
		var handler;
		if ( !$(f.elements[i]).getValue() && $(f.elements[i]).hasClass('required')) handler = "required";
		else handler = (f.elements[i].className && f.elements[i].className.search(/validate-([a-zA-Z0-9\_\-]+)/) != -1) ? f.elements[i].className.match(/validate-([a-zA-Z0-9\_\-]+)/)[1] : "";
		
		if(!document.formvalidator.validate(f.elements[i]) && !CheckedList.in_array(f.elements[i].name) ){
			
			if (!(f.elements[i].labelref)) {
				var labels = $$('label');
				labels.each(function(label){
					if (label.getProperty('for') == f.elements[i].getProperty('name')) {
						f.elements[i].labelref = label;
					}
				});
			}
			
			if (!(f.elements[i].labelref)) {
        		ErrrorMsg += f.elements[i].name + " ; ";
			}else{
				ErrrorMsg += f.elements[i].labelref.innerHTML + " ; ";
			}
			if($("tip_"+f.elements[i].name)){
				if ( Validation_Messages[handler] )
					$("tip_"+f.elements[i].name).innerHTML = Validation_Messages[handler];
			}
			
    		CheckedList[oo++] =  f.elements[i].name;
		}else{
			if (document.formvalidator.validate(f.elements[i]) && $("tip_"+f.elements[i].name))
					$("tip_"+f.elements[i].name).innerHTML = "";
		}
	}
	return ErrrorMsg;
}

function getCheckedValue(radioObj)
{
	var selectorName = radioObj.name;
	var form = radioObj.form;
	for (var i=0;i < form.elements.length; i++) {
		if(form.elements[i].name == selectorName && form.elements[i].checked){
			return form.elements[i].value;
		}
	}
    return "";
}

Array.prototype.in_array = function(p_val) {
	for(var i = 0, l = this.length; i < l; i++) {
		if(this[i] == p_val) {
			return true;
		}
	}
	return false;
}

// JForm Custom Validators
window.addEvent('domready', function() {
//Window.onDomReady(function() {
	    Element.extend({
	    	getValue: function(){
	    		
	    	   //original getValue did not work for file and radio
	    		switch(this.getTag()){
	    			case 'select':
	    				var values = [];
	    				$each(this.options, function(option){
	    					if (option.selected) values.push($pick(option.value, option.text));
	    				});
	    				return (this.multiple) ? values : values[0];
	    			case 'input':
	                    if (this.type=='radio'){
	                    	return getCheckedValue(this);
	                    }
	                    if (!(this.checked && ['checkbox', 'radio'].contains(this.type)) && !['hidden', 'text', 'password','file'].contains(this.type)) break;
	    			case 'textarea': return this.value;
	    		}
	    		return false;
	    	}
	    	
	    });
	    
		document.formvalidator.setHandler('url', function(value) {
                regex=/^(([\w]+:)?\/\/)?(([\d\w]|%[a-fA-f\d]{2,2})+(:([\d\w]|%[a-fA-f\d]{2,2})+)?@)?([\d\w][-\d\w]{0,253}[\d\w]\.)+[\w]{2,4}(:[\d]+)?(\/([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)*(\?(&?([-+_~.\d\w]|%[a-fA-f\d]{2,2})=?)*)?(#([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)?$/;
                return regex.test(value);
        }
        );


});
