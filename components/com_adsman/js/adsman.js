/**
 * if textarea is valid sets task to sendMessage and submits form
 *
*/
function getcontent(id){
    if (document.getElementById){
    	el=document.getElementById(id);
    	if (el) return el.innerHTML;
    }
    return '&nbsp;';
}

function sendMessage(destination) {
	
	if (document.getElementById('message_textarea').value!="" && document.getElementById('from').value!="") { /* && document.getElementById('from').value!="0") {*/
		
		document.getElementById('task_message').value = 'sendMessage';
				
		if ( (destination == 'reply') && (document.getElementById('adsreply_to').value != "") ) {
		
			var test = document.getElementById('adsreply_to').value;
			document.getElementById('to_reply').value = test;
						
			var test_textarea = document.getElementById('message_textarea').value;
			document.getElementById('message_textarea_reply').value = test_textarea;
		} 
		
		document.adsForm.submit();
	} else {
		if(document.getElementById('message_textarea').value=="")
			alert(language["adsman_err_enter_message"]);
	}
}

function reply(to) {
	document.getElementById('adsreply_to').value=to;	
	document.getElementById('adsreply_username').innerHTML=document.getElementById('adsusername_'+to).innerHTML;
	//SelectTab('send');
}

function SelectTab(tab)
{
    if ( (tab=="messages" || document.location.hash=='#messages') && tabPane1.pages.length>1){
    	tabPane1.setSelectedIndex(1);
    }
    if ((tab=="send" || document.location.hash=='#send') && tabPane1.pages.length>2){
         tabPane1.setSelectedIndex(2);
    }
}

function tableAdsOrdering( order, dir) {
	var form = document.adsForm;

	form.filter_order.value 	= order;
	form.filter_order_Dir.value	= dir;
	form.submit();
}

function viewStyleAds( listview) {
	var form = document.adsForm;

	form.list_view.value 	= listview;
    form.submit();
}

function viewArchiveAds(statusview,task) {
    var form = document.adsForm;

    form.archive.value 	= statusview;
    document.adsForm.submit();
}

function isValidURL(url){
    var RegExp = /^(([\w]+:)?\/\/)?(([\d\w]|%[a-fA-f\d]{2,2})+(:([\d\w]|%[a-fA-f\d]{2,2})+)?@)?([\d\w][-\d\w]{0,253}[\d\w]\.)+[\w]{2,4}(:[\d]+)?(\/([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)*(\?(&?([-+_~.\d\w]|%[a-fA-f\d]{2,2})=?)*)?(#([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)?$/;
    if(RegExp.test(url)){
        return true;
    }else{
        return false;
    }
}

function checkReportForm(){
	if(document.getElementById("comment").value=="") {
		alert(language['adsman_err_enter_message']);
	}else{
		var x=document.getElementById("adsForm");
		x.target="_parent";
		x.submit();
	}
}

function theFactoryToggleFields(){
	
	var ahtml = this.response.text;
	document.getElementById("custom_fields_container").innerHTML=ahtml;
}

function theFactoryCategoryPrice(){

	var chtml = this.response.text;
	if (document.getElementById('category_price_container') != null) {
		document.getElementById("category_price_container").innerHTML='Cost ' + chtml + ' credit(s) ';
	}
}

function show_waiting(){
	var ahtml = "<img src=root + 'components/com_adsman/img/components/com_adsman/img/ajax-loader.gif' />";
	document.getElementById("custom_fields_container").innerHTML=ahtml;
}

function display_listing_cost(sel) {
	var urlk = "index.php?option=com_adsman&task=category_listing_cost&catid="+sel+"&format=raw";
	
	var FieldsAjaxCall = new Request({
		method: 'get',
		url: urlk,
					
		onComplete: theFactoryCategoryPrice
		 
	}).send();
	
}

function aiai(sel){
	
	show_waiting();
	
	
	var id_q = "";
	if(AFIDid!="")
		id_q = "&adid="+AFIDid;
		
	var positionID_q;
	var url;
		
	link = "index.php?option=com_adsman&task=ajax_form_customfields&catid=" + sel + id_q;
	var FieldsAjaxCalls = new Request({
		 method: "get", 
		 url: link, 
		 
		 onSuccess: function() {
        	display_listing_cost(sel);
		 },
		 onComplete:theFactoryToggleFields
		}).send();
	
}

function aiai_search(sel){
	var urli = "index.php?option=com_adsman&task=ajax_search_customfields&catid="+sel;
		
	var FieldsAjaxCall = new Request({
		method: 'get',
		url: urli,
		onComplete: theFactoryToggleFields
		 
	}).send();

}


function categorySelectPopulateResponse(){
	var ahtml = this.response.text;
	document.getElementById("category_axj_space").innerHTML=ahtml;

	if ( page == 'search') {
		aiai_search(document.getElementById("cat").value);
	}
	else {
		aiai(document.getElementById("category").value);	
		if (document.getElementById('category_price_container') != null) {
			document.getElementById('category_price_container').innerHTML = 'Listing costs '+ document.getElementById("category").value + ' credits';
		}
	}
}

function categorySelectPopulate(task,catid){
	var link = "index.php?option=com_adsman&task="+task+"&catid="+catid;
	
	var CategoryAjaxCall = new Request({
		method: "get",	
		url: link,
		onComplete: categorySelectPopulateResponse 
	}).send();
		
}


function addman_reply(touser,tousername)
{
    userspan=document.getElementById('username_reply');
    userspan.innerHTML=tousername;
    
    msgdiv=document.getElementById('adds_reply_message');
    msgdiv.style.display='block';
    
    frm=document.adsForm_reply;
    frm.to_reply.value=touser;    
    
}

function addman_reply_close()
{
    msgdiv=document.getElementById('adds_reply_message');
    msgdiv.style.display='none';
    
}

function check_gatewayform()
{
    var frm=document.auctionForm;
    for (var i=0; i < frm.paymenttype.length; i++)
        if (frm.paymenttype[i].checked)
            return true;

    alert(language["ads_choose_gateway"]);
    return false;
}