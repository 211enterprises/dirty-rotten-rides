    function selectCategory()
    {
        document.forms['category_select'].submit();  
    }

    function removeChildrenFromNode(node) {
        if(node.hasChildNodes()) {
            while(node.childNodes.length >= 1 ) {
                node.removeChild(node.firstChild);
            }
        }
    }
    function selectcat(catid)
    {
        ul=document.getElementById('category_list');
        removeChildrenFromNode(ul);
        k=0;
        nrcats=0;
        for(i in CatList)
        {
            k=1-k;
            if (CatParents[i]==catid)
            {
                var li =document.createElement("li");
                li.innerHTML="<span class='adds_category_link'><a href='javascript:selectcat("+i+")'>"+CatList[i]+"</a></span>";
                if (k)
                    li.style.backgroundColor="#EEEEEE";
                else
                    li.style.backgroundColor="#E0E0E0";
                ul.appendChild(li);
                nrcats++;
            }              
        }
        document.forms['category_select'].category.value=catid;
        span=document.getElementById('current_cat');
        span.innerHTML+='/'+CatList[catid];
        if (nrcats==0) selectCategory();

    }
    function toggleButton(){
    	document.getElementById('button_up').innerHTML = language["adsman_choose_category"];
    	document.getElementById('button_dwn').innerHTML = language["adsman_choose_category"];
    }
