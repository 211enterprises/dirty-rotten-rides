<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/


	require_once( JPATH_ROOT.DS.'components'.DS.'com_adsman'.DS.'helpers'.DS.'helper.php' );
	require_once( JPATH_ROOT.DS.'components'.DS.'com_adsman'.DS.'helpers'.DS.'route.php' );

	function adsmanBuildRoute(&$query){

		$database = JFactory::getDbo();
		$segments = array();
		if(!isset($query['task']))
			if(isset($query['layout']))
				$query['task'] = $query['layout'];
			
    	if(isset($query['task']))
    	{
    		switch ($query['task'])
    		{
    			case 'details':
    				if(!empty($query['id'])){
    					$segments[]="details";
    					$segments[]=(int)$query['id'];
    					if(isset($query['title']) &&  $query['title']!=''){

    						$segments[]=$query['title'].".html";
    						unset($query['title']);

    					}else{

    						$q 	 = "SELECT a.title FROM #__ads AS a WHERE id='".$query['id']."'"; $database->setQuery($q);
    						$rec = $database->loadObject();
    						$rec->title= JFilterOutput::stringURLSafe($rec->title);
    						$segments[]=AdsUtilities::str_clean($rec->title).".html";

    					}
    					unset($query['id']);
    					unset($query['task']);
    					unset($query['view']);
    				}
    			break;
    			
    			case 'listadds':
    				//unset($query['view']);
    				
    				if(isset($query['cat'])){
    					$segments[]="listadds";

    					// dev
    					//unset($query['catslug']);
    					if (isset($query['catslug']) && $query['catslug']!='') {
    						$segments[]=$query['catslug'];
    						unset($query['catslug']);

    					}
    					else
    						$segments[]=AdsmanHelperRoute::getSEFCatString($query['cat']);

    					unset($query['cat']);
    					unset($query['task']);

    				}
    				if(!empty($query['userid'])){
    					$q="SELECT username FROM #__users WHERE id='".(int)$query['userid']."'"; $database->setQuery($q);
    					$rec = $database->loadObject();
    					if ($rec){
    						$segments[]="listadds"; $segments[]='user/'.$query['userid'].'/'.AdsUtilities::str_clean($rec->username);
    						unset($query['userid']);
    						unset($query['task']);
    					}
    				}
    				if(!empty($query['tag'])){
    					$segments[]="listadds";
    					$segments[]='tag/'.$query['tag'];
    					unset($query['tag']);
    					unset($query['task']);

    				}
    			break;
    			
    			case 'listcats':
//    				$segments[] = $query['view'];
    				unset($query['view']);
    				
    				if (isset($query['cat']))
    				{
    					$segments[] = "listcats";

    					// dev
    					//unset($query['catslug']);

    					if (isset($query['catslug']) && $query['catslug']!='')
    					{
    						$segments[]=$query['catslug'];
    						unset($query['catslug']);

    					}
    					else
    					{
    						$segments[]=AdsmanHelperRoute::getSEFCatString($query['cat']);
    					}

    					unset($query['cat']);
    					unset($query['task']);
    				}
    				
    				if(!empty($query['userid'])){
    					$q="SELECT username FROM #__users WHERE id='".(int)$query['userid']."'"; $database->setQuery($q);
    					$rec = $database->loadObject();
    					if ($rec){
    						$segments[]="listcats"; 
    						$segments[]='user/'.$query['userid'].'/'.AdsUtilities::str_clean($rec->username);
    						unset($query['userid']);
    						unset($query['task']);
    					}
    				}
    				if(!empty($query['tag'])){
    					$segments[]="listcats";
    					$segments[]='tag/'.$query['tag'];
    					unset($query['tag']);
    					unset($query['task']);

    				}
    			break;
    		}
    	}

		return $segments;
	}

	/**
	 * Route Parsing
	 *
	 * @param $segments
	 * @return 
	 */
	function adsmanParseRoute($segments){
	  //Set the active menu item
		$vars = array();
		
		switch($segments[0])
		{
			case 'details':
				$vars['task'] = 'details';
				$v=explode('/',$segments[1]);
				
				$vars['id']=$v[0];
			break;

			case 'listadds':
				$vars['task'] = 'listadds';
				$vars['layout'] = 'default';

				$categories = $segments;
				unset($categories[0]);
				
				if($segments[1]=="tag"){
					$vars['tag']=$segments[2];
					unset($categories[1]);
					unset($categories[2]);
					
				} elseif( $segments[1]=="user" ) {
					$vars['userid']=$segments[2];
					unset($categories[1]);
					unset($categories[2]);
				} else {
					$qu = count($categories);

					if ( $qu > 0 ) {
						if( $qu == 1 ) {
							$pcat = isset($categories[1]) ? $categories[1] : null;
							$pscat = null;
							$pcat = preg_replace('/:/', '-', $pcat, 1);
							$vars['catslug'] = $categories[1];
							
						} else {
							$pcat = isset($categories[$qu-1]) ? $categories[$qu-1]:null;
							$pscat = isset($categories[$qu]) ? $categories[$qu] : null;
							
							$pcat = preg_replace('/:/', '-', $pcat, 1);
							$pscat = preg_replace('/:/', '-', $pscat, 1);

                            $vars['catslug'] = $categories[$qu-1].'/'.$categories[$qu];

						}
						
						$vars['cat']=AdsmanHelperRoute::getSEFCat($pcat, $pscat);
					
					}
				}
			break;
			
			case 'listcats':
				$vars['task'] = 'listcats';
				$vars['layout'] = 'default';
				
				$categories = $segments;
				unset($categories[0]);
				
				if($segments[1]=="tag")
				{
					$vars['tag']=$segments[2];
					unset($categories[1]);
					unset($categories[2]);
				}
				elseif( $segments[1]=="user" ) 
				{
					$vars['userid']=$segments[2];
					unset($categories[1]);
					unset($categories[2]);
				}
				else
				{
					$qu = count($categories);
					if($qu>0){
						if($qu==1){
							$pcat = isset($categories[1])?$categories[1]:null;
							$pscat = null;
							$pcat = preg_replace('/:/', '-', $pcat, 1);
						//	$vars['catslug'] = $categories[1];
						}else{
							$pcat = isset($categories[$qu-1])?$categories[$qu-1]:null;
							$pscat = isset($categories[$qu])?$categories[$qu]:null;
							
							$pcat = preg_replace('/:/', '-', $pcat, 1);
							$pscat = preg_replace('/:/', '-', $pscat, 1);
						//	$vars['catslug'] = $categories[$qu-1];
						}
						
						$vars['cat']=AdsmanHelperRoute::getSEFCat($pcat, $pscat);
					}
				}
			break;
		}
		return $vars;

	}
?>