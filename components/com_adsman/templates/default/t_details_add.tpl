{include file='t_javascript_language.tpl'}
{set_css}

<!--<div class="componentheading">{jtext text="ADS_PAGE_VIEW_AD"}</div>-->

<form action="{$ROOT_HOST}index.php" method="post" name="adsForm" enctype="multipart/form-data">
<input type="hidden" name="Itemid" value="{$Itemid}" />
<input type="hidden" name="option" value="{$option}" />
<input type="hidden" name="view" value="adsman" />
<input type="hidden" name="id" value="{$add->id}" />
<input type="hidden" name="task" id="task_message" value="" />

{include file='elements/details/t_details_add_plugins.tpl'}

<!-- [+]Add Container -->
<div class="addContainer">
	<!-- [+]TOOLBAR -->{include file='elements/details/t_details_add_toolbar.tpl'}<!-- [-]TOOLBAR -->
	<!-- [+]Title -->
	<p class="add_detail_title">
        {if $add->catname}{*jtext text="ADS_CATEGORY"*} <a href="{$add->links.filter_cat}">{$add->catname}</a>{/if}<br />
        {$add->title}
		{if $add->status==0}({jtext text="ADS_UNPUBLISHED"}){/if}
	</p>
	<!-- [-] Title -->
	<!-- [+] Default Page Position -->
	<p>
	{include file='elements/display_fields.tpl' position='defdetailspageheader'}
	</p>
	<!-- [-] Default Page Position -->
	<!-- [+] Gallery -->
<div style="float:left; width: 95%;">	
	<div class="ads_gallery_left">
		{if $add->gallery}
			{$add->gallery}
		{else}
			<div style="width:150px;">
				<img src="{$IMAGE_ROOT}no_image.png" class="ads_noborder" alt="No image" />
			</div><br />
		{/if}

        <br /><span class="add_text_medium_bold">{jtext text="ADS_HITS"}: {$add->hits}</span>
		<!--<br /><br />-->
		
	</div>
	<!-- [-] Gallery -->
	
	<div style="float:left; width:60%; padding: 2px; border:0px solid gray;">
		<table width="100%" align="center" class="ads_noborder ads_table">
			<tr>
                <td style="width: 50%; border: 1px solid green; vertical-align: top;">
                    {if $add->askprice}
                        <span style="float: right; font-weight: bold;">
                                {if is_numeric($add->askprice)}
                                    <span style="font-size: 1.6em;">{$add->askprice|string_format:"%.2f"}&nbsp;</span> <span class="currency">{$add->currency_name}</span>
                                {else}
                                    {$add->askprice}
                                {/if}
                        </span>
                    {/if}

                    <br /><br /> <br />
                    {if $add->ad_city}
                        <span class="add_text_medium_bold">{jtext text="ADS_AD_CITY"}: {$add->ad_city}</span><br />
                    {/if}
                    {if $add->ad_postcode}
                        <span class="add_text_medium_bold">{jtext text="ADS_AD_POSTCODE"}: {$add->ad_postcode}</span><br />
                    {/if}
                    {if $item->expired}
                        <span class='expired'>{jtext text="ADS_EXPIRED"}</span>({$add->end_date_text}) <br />
                    {else}
                        &nbsp;
                    {/if}
                    {if $add->closed}
                        <h3>{jtext text="ADS_AD_CLOSED"} {if $add->closed_date_text} ({$add->closed_date_text}) {/if}</h3>
                    {else}
                        &nbsp;
                    {/if}

                </td>
                <!--<td>

                </td>
                <td>

                </td>-->

                <td style="float:right; width: 230px; border: 1px solid green; vertical-align: top; ">
                    <fieldset>
                       <legend><span style="float:left;">{jtext text="ADS_POSTED_BY"}</span></legend>
                        <table width="100%" align="center" class="ads_noborder ads_table">
                            {include file='elements/details/t_details_add_user.tpl'}
                        </table>
                    </fieldset>
                </td>

				<!--<td style="word-wrap: break-word; white-space:normal;">
                    {*if $add->links.tags != ''}
                        <div style="float:left; text-align:right !important; width: 180px; word-wrap: break-word; white-space:normal;">
                            {jtext text="ADS_TAGS"}: {$add->links.tags}
                        </div>
                    {/if*}
				</td>-->
			</tr>
		</table>

        <div style="float:left; width:100%;">
            <!--<span class="add_text_medium_bold">{jtext text="ADS_POSTED_ON"}: {$add->start_date_text}</span>-->

            {if $add->closed==0}
                <!--<span class="add_text_medium_bold" style="float:right;">
                    {*jtext text="ADS_END_DATE"}: {$add->end_date_text*}</span>-->

                {if $smarty.const.ads_opt_enable_countdown}
                    <span class="add_text_medium_bold">
                        {jtext text="ADS_EXPIRES_IN"}:&nbsp;<span id="time1">{$add->countdown}</span>
                    </span>
                {else}
                    &nbsp;
                {/if}
            {else}
                &nbsp;
            {/if}
            <br />
        </div>

		<div class="left_detail" style="float:right; text-align: left; width:98%; padding: 3px; border:0px solid red;">

        {if !$add->is_my_add}
			{if $add->favorite==0 }
				<span id='add_to_favorite'>
					<a href='{$add->links.add_to_favorite}'><img src="{$IMAGE_ROOT}f_favoritelist_1_24.png" class="ads_noborder detail_img_social" title="{jtext text='ADS_ADD_TO_FAVORITE'}" alt="{jtext text='ADS_ADD_TO_FAVORITE'}" /></a>
				</span>
			{elseif $add->favorite===1}
				<span id='add_to_favorite'>
					<a href='{$add->links.del_from_favorite}'><img src="{$IMAGE_ROOT}f_favoritelist_0_24.png" class="ads_noborder detail_img_social" title="{jtext text='ADS_REMOVE_FROM_FAVORITE'}" alt="{jtext text='ADS_REMOVE_FROM_FAVORITE'}"  /></a>
				</span>
			{/if}
			
			{if ( $add->closed==0 ) }
				<a class="modal" href="{$add->links.report}" {literal} rel="{handler: 'iframe', size: {x: 450, y: 300}}" {/literal}" ><img src="{$IMAGE_ROOT}report.png" title="{jtext text='ADS_REPORT'}" class="ads_noborder detail_img_social" alt="{jtext text='ADS_REPORT'}" /></a>
			{/if}
	
		{/if}	
			
        {if $add->more_pictures}
              <img src="{$IMAGE_ROOT}morepics_on_24.png" class="ads_noborder detail_img_social" title="{jtext text='ADS_MORE_PICTURES'}" alt="{jtext text='ADS_MORE_PICTURES'}" />
        {else}
              <img src="{$IMAGE_ROOT}morepics_off_24.png" class="ads_noborder detail_img_social" title="{jtext text='ADS_NO_MORE_PICTURES'}" alt="{jtext text='ADS_NO_MORE_PICTURES'}" />
        {/if}
			
        {if $smarty.const.ads_allow_atachment=="1" }
            {if $add->atachment}
               <a target="_blank" href="index.php?option=com_adsman&view=adsman&task=downloadfile&id={$add->id}"><img src="{$IMAGE_ROOT}downloads_on_24.png" title="{jtext text='ADS_HAS_DOWNLOADS'}" alt="{jtext text='ADS_HAS_DOWNLOADS'}" class="ads_noborder detail_img_social" /></a>
            {else}
                   <img src="{$IMAGE_ROOT}downloads_off_24.png" title="{jtext text='ADS_HAS_NO_DOWNLOADS'}" alt="{jtext text='ADS_HAS_NO_DOWNLOADS'}" class="ads_noborder detail_img_social" />
            {/if}

        {/if}

		{*if $smarty.const.ads_allow_atachment=="1" && $add->atachment}
	       <a target="_blank" href="index.php?option=com_adsman&view=adsman&task=downloadfile&id={$add->id}" style="margin-right:5px;"><img src="{$IMAGE_ROOT}downloads_on.png" title="{jtext text='ADS_HAS_DOWNLOADS'}" alt="{jtext text='ADS_HAS_DOWNLOADS'}" class="ads_noborder detail_img_social" /></a>
	 	{/if*}
	 	
		<a href="http://twitter.com/home?status=Currently reading {$add->links.details}" title="Click to send this page to Twitter!" target="_blank"><img src="{$ROOT_HOST}components/com_adsman/img/tweet.png" class="ads_noborder detail_img_social" alt="Tweet This!" /></a>
        <a href="http://www.facebook.com/sharer.php?u={$add->links.tweeter|rawurlencode}&amp;title={$add->title|rawurlencode}" target="_blank" title="{jtext text='ADS_FACEBOOK'}"><img src="{$ROOT_HOST}components/com_adsman/templates/demo/image/facebook_button.png" alt="Share on Facebook" class="ads_noborder detail_img_social" /></a>
		<!--<img src="http://static.delicious.com/img/delicious.small.gif" height="15" width="15" alt="{*jtext text='ADS_DELICIOUS'*}" />-->

		<a href="http://delicious.com/save" title="{jtext text='ADS_DELICIOUS'}" onclick="window.open('http://delicious.com/save?v=5&amp;noui&amp;jump=close&amp;url='+encodeURIComponent(location.href)+'&amp;title='+encodeURIComponent(document.title), 'delicious','toolbar=no,width=550,height=550'); return false;"><img src="{$ROOT_HOST}components/com_adsman/img/Social_Delicious.png" class="ads_noborder detail_img_social" alt="{jtext text='ADS_DELICIOUS'}" /></a>
		<a class="modal" href="index.php?option=com_adsman&view=adsman&amp;task=tellFriend_show&id={$add->id}&tmpl=component" {literal} rel="{handler: 'iframe', size: {x: 500, y: 300}}" {/literal}><img src="{$ROOT_HOST}components/com_adsman/img/tell.png" alt="{jtext text='ADS_TELL_FRIEND'}" title="{jtext text='ADS_TELL_FRIEND'}" class="ads_noborder" style='vertical-align:top; width: 32px'  /></a>

	</div>

	</div>
	
</div>	
 <!-- next row -->
	<div style="clear:both;" ></div>
   {include file='elements/details/t_details_add_tab.tpl'}
		
	<div style="clear: both;"></div>	

    <div style="float:left; width:100%;">
        <span class="add_text_medium_bold">{jtext text="ADS_POSTED_ON"}: {$add->start_date_text}</span>

        {if $add->closed==0}
            <span class="add_text_medium_bold" style="float:right;">
                {jtext text="ADS_END_DATE"}: {$add->end_date_text}</span>
        {else}
            &nbsp;
        {/if}
        <br />
    </div>


    {if $add->links.tags != ''}
        <br /><br />
        <div style="float:left; width: 100%; word-wrap: break-word; white-space:normal;">
            {jtext text="ADS_TAGS"}: {$add->links.tags}
        </div>
    {/if}

	<!-- next row -->
	<div style="clear:both;" ></div>

</div>

<!-- [-]Add Container -->
</form>

<!-- next row -->
<div style="clear:both;" ></div>