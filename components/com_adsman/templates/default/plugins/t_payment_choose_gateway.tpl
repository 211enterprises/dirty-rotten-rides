{include file='t_javascript_language.tpl'}
{set_css}
{set_js}
<div class="componentheading">{jtext text="ADS_CHOOSE_GATEWAY"}</div>
<form action="{$ROOT_HOST}index.php?Itemid={$Itemid}" method="get" name="auctionForm" onsubmit="return check_gatewayform();">
<input type="hidden" name="option" value="com_adsman"/>
<input type="hidden" name="task" value="{$task}"/>
<input type="hidden" name="itemname" value="{$itemname}"/>
<input type="hidden" name="currency" value="{$currency}" />
<input type="hidden" name="itemamount" value="{$itemamount|default:1}" />
<input type="hidden" name="itemprice" value="{$itemprice}"/>
<input type="hidden" name="id" value="{$object_id}"/>
<input type="hidden" name="return_url" value="{$return_url}"/>
<input type="hidden" name="act" value="{$act}"/>
<input type="hidden" name="Itemid" value="{$Itemid}"/>
                       
<input name="var_name1" type="hidden" value="{$var_name1}" />
<input name="var_value1" type="hidden" value="{$var_value1}" />
<input name="var_name2" type="hidden" value="{$var_name2}" />
<input name="var_value2" type="hidden" value="{$var_value2}" />
<input name="var_name3" type="hidden" value="{$var_name3}" />
<input name="var_value3" type="hidden" value="{$var_value3}" />
<input name="var_name4" type="hidden" value="{$var_name4}" />
<input name="var_value4" type="hidden" value="{$var_value4}" />

<table align="center" cellpadding="0" cellspacing="0" width="100%" id="auction_list_container" class="ads_table">
    {section name=payloop loop=$payment_systems}
        {assign var=pay value=`$payment_systems[payloop]`}
        <tr>
            <td><input name="paymenttype" type="radio" value="{$pay->classname}" {if $pay->isdefault} checked="checked" {/if} ></td>
            <td><img src='{$pay->thumbnail}' class="ads_noborder" /></td>
            <td>{$pay->classdescription} </td>
        </tr>
    {/section}
        <tr>
            <td colspan="3"><input name="submit" class="back_button" type="submit" value="{jtext text='ADS_CHOOSE_PAYMENT'}"></td>
        </tr>
</table>

</form>
