{set_css}

<form method="POST" action="{$ROOT_HOST}index.php?Itemid={$Itemid}" name="purchase_item">
	<input name="option" type="hidden" value="com_adsman" />
	<input name="task" type="hidden" value="purchase" />
	<input name="paymenttype" type="hidden" value="{$paymenttype}" />
	<input name="currency" type="hidden" value="{$pac->currency}" />
	<input name="act" type="hidden" value="checkout" />
	<input name="return_url" type="hidden" value="{$return_url}" />
	<input name="id" type="hidden" value="{$object_id}" />
	<input name="Itemid" type="hidden" value="{$Itemid}" />
	<input name="var_name1" type="hidden" value="package_id" />
	<input name="var_value1" type="hidden" value="{$pac->id}" />
	<input name="var_name2" type="hidden" value="total_price_ad" />
	<input name="var_value2" type="hidden" value="{$total_price_ad}" />
	<input name="var_name3" type="hidden" value="featured_ad" />
	<input name="var_value3" type="hidden" value="{$featured_ad}" />
	<input name="itemname" type="hidden" value="packages" />
	<input name="itemprice" type="hidden" value="{$pac->price}" />
	
	<table class="ads_table">
		<tr>
			<td align="center"><strong>{$pac->name}</strong></td>
		</tr>
		<tr>
			<td align="center">{$pac->price} {$pac->currency}</td>
		</tr>
		<tr>
			<td align="center">{jtext text="ADS_CREDITS"}: {$pac->credits}</td>
		</tr>
		<tr>
			<td height="100" align="center">
				<a href="index.php?option=com_adsman&task=buy_packages&package={$pac->name}">
					 {package_image package=$pac->name}
				</a>
			</td>
		</tr>
		<tr>
			<td align="center">
			{if $selected_type!=$pac->name}
				<a href="index.php?option=com_adsman&task=buy_packages&package={$pac->name}" ><strong>{jtext text="ADS_SELECT"}</strong></a>
			{/if}
			<br />
			</td>
		</tr>
		<tr>
			<td align="center">{jtext text="ADS_PACKAGE_QUANTITY"}: {$package_quant}</td>
		</tr>
		{if $currency_maincredit == $pac->currency}
		<tr>
			<td align="center">{jtext text="ADS_ECONOMY"}
			{math equation="(( x * y ) - z)" x=$price_maincredit y=$pac->credits z=$pac->price format="%.2f"}
			{$currency_maincredit}
			{jtext text="ADS_ECONOMY_PER_PACKAGE"} 
			</td>
		</tr>
		{/if}
		<tr>
			<td align="center">
				<input type="submit" name="submit" class="back_button" value="{jtext text='ADS_PAY_PAYMENT_PURCHASE'}" />
			</td>
		</tr>
	</table>
	<input name="itemamount" type="hidden" value="1" />
</form>