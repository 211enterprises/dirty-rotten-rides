{set_css}
<h2>{jtext text="ADS_PAY_PAYMENTITEM_DESC_FEATURED"}</h2>

<form method="POST" action="{$ROOT_HOST}index.php?Itemid={$Itemid}" name="purchase_item">
<input name="option" type="hidden" value="com_adsman"/>
<input name="task" type="hidden" value="purchase"/>
<input name="paymenttype" type="hidden" value="{$paymenttype}"/>
<input name="act" type="hidden" value="checkout" />
<input name="return_url" type="hidden" value="{$return_url}"/>
<input name="id" type="hidden" value="{$object_id}"/>
<input name="Itemid" type="hidden" value="{$Itemid}" />
<table width="350" class="ads_table">
    {section name=itemloop loop=$pricing}
        {assign var=price value=`$pricing[itemloop]`}
        
        {if $price->itemname|substr:0:8 == 'featured' }
        
            {assign var=txt value=$price->itemname|substr:9}
            
            {if $selected_type==$txt}
                {assign var=sel value="checked='yes'"}
            {else}
                {assign var=sel value=""}
            {/if}
            
            {assign var=txt value="adsman_payment_featured_"|cat:$txt}
            
            {assign var=txt2 value=$txt|cat:"_HELP"}
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input type="radio" name="itemname" value="{$price->itemname}" {$sel}>
                    {$smarty.const.$txt }
                    <!--{jtext text="$txt"}-->
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <p style="padding-left:50px" class="featured_desc"><!--{jtext text="$txt2"}-->{$smarty.const.$txt2}</p>
                </td>
            </tr>
        {/if}
    {/section}
</table>
{if $object_id==""}
	{"Credits"}: <input name="itemamount" type="text" value="1" />
{else}
	Upgrade <strong>{$item_object->info.payment_description}</strong>
{/if}
<br /><br />
<input type="submit" name="submit" class="ads_button" value="{jtext text='ADS_PAY_PAYMENT_PURCHASE'}" />
</form>
