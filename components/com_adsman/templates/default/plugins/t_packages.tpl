{set_css}
<table width="100%">
<tr>
{foreach from=$packages item=pac key=key}
	<td align="center" valign="top">
		<table>
			<tr>
			{if $pac->price != 0}
				<td align="center">
					<a class="ads_packages_link" href="index.php?option=com_adsman&task=buy_package&package={$pac->name}&amp;Itemid={$Itemid}" >
						<span style="color:#CC0000;font-weight: bold;">{jtext text="ADS_BUY"}</span></a>
					<!--<a class="ads_packages_link" href="index.php?option=com_adsman&task=buy_packages&package={$pac->name}&amp;Itemid={$Itemid}" >Select!</a>-->
				</td>
			{else}
				<td align="center">
					<a class="ads_packages_link" href="index.php?option=com_adsman&task=free_package&package={$pac->name}&amp;Itemid={$Itemid}" >{jtext text="ADS_GET_FREE_CREDITS"}</a>
				</td>
			{/if}	
			</tr>
			<tr>
				<td align="center"><strong>{$pac->name}</strong></td>
			</tr>
			<tr>
				<td align="center">{$pac->price} {$pac->currency}</td>
			</tr>
			<tr>
				<td align="center">{jtext text="ADS_CREDITS"}: {$pac->credits}</td>
			</tr>
			
			{if $currency_maincredit == $pac->currency}
			<tr>
				<td style="text-align:center;">{jtext text="ADS_ECONOMY"}
				{math equation="(( x * y ) - z)" x=$price_maincredit y=$pac->credits z=$pac->price format="%.2f"}
				{$currency_maincredit}
				{jtext text="ADS_ECONOMY_PER_PACKAGE"} 
				</td>
			</tr>
            {else}
            <tr>
                <td>&nbsp;</td>
            </tr>
			{/if}
			<tr>
				<td height="100" align="center">
					<a href="index.php?option=com_adsman&task=buy_package&package={$pac->name}&amp;Itemid={$Itemid}">
            {package_image package=$pac->name}					
					</a>
				</td>
			</tr>
		</table>
	</td>
{/foreach}

</tr>
</table>
