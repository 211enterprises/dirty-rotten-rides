{include file='t_javascript_language.tpl'}
{set_css}
{set_js}
<div align="center" class="ads_letterlist">
    {section name=letters start=65 loop=91}
        <a href="{$HOST_ROOT}index.php?option=com_adsman&view=adsman&task=listcats&startletter={$smarty.section.letters.index|chr}">
             {$smarty.section.letters.index|chr}
        </a>
        &nbsp;
    {/section}
    <a href="{$HOST_ROOT}index.php?option=com_adsman&view=adsman&task=listcats">
         {jtext text='ADS_ALL'}
    </a>
</div><br />
<div class="adds_man_list">
	<span>{$category_pathway}</span>
	{if $current_category->catname!=""}
	  <div style="border:2px solid #000;">
            {include file='elements/display_fields.tpl' position='categoryicon' this_add=$current_category->id}

            <strong>{$current_category->catname}</strong>
                <a href="{$current_category->view}" title="{jtext text='ADS_VIEW_LISTINGS'}">
                    <img src="{$IMAGE_ROOT}view_listings.gif" class="ads_noborder" alt="{jtext text='ADS_VIEW_LISTINGS'}" /></a>
                ({$current_category->nr_ads_current} {jtext text="ADS_ADS"})

            <div class="adds_subcat" style="text-align:right;" >
            {include file='elements/display_fields.tpl' position='categorydetails' this_add=$current_category->id}
            <span style="font-size:12px;">
                {jtext text="ADS_SUBCATEGORIES"}:  {$current_category->kids} &nbsp;|&nbsp; {jtext text="ADS_TOTAL_ADS"}: {$current_category->nr_ads}
            </span>
	  </div>
    {/if}
</div>

{if $categories|@count<=0}
    <h2>{jtext text="ADS_NO_CATEGORIES_DEFINED"}</h2>
{/if}
<table id="adds_categories" class="ads_table">
  {section name=category loop=$categories}
	{if $smarty.section.category.rownum is odd}
		<tr>
	{/if}
			<td width="50%" class="adsman_catcell" valign="top">
				<div class="adds_maincat" {if $categories[category]->watchListed_flag}class="cat_watchlist"{/if}> 
					
					{include file='elements/display_fields.tpl' position='categoryicon' this_add=$categories[category]->id}
					<a href="{if $categories[category]->kids>0}{$categories[category]->link}{else}{$categories[category]->view}{/if}" >
						{$categories[category]->catname}
					</a>
					{if $categories[category]->is_new}
						<!--NEW!!-->
						<img src="{$IMAGE_ROOT}new.png" alt="new ads" />
					{/if}
					<a href="{$categories[category]->view}" title="{jtext text='ADS_VIEW_LISTINGS'}">
						<img src="{$IMAGE_ROOT}view_listings.gif" class="ads_noborder" alt="{jtext text='ADS_VIEW_LISTINGS'}" />
					</a>					
					{if $is_logged_in}
						{if $categories[category]->watchListed_flag}
							<img src="{$IMAGE_ROOT}watchlist.png" width="16"class="ads_noborder" />
						{/if}
						<a href="{$categories[category]->link_watchlist}">
							<img src="{$categories[category]->img_src_watchlist}" style="vertical-align: bottom;" width="16" class="ads_noborder" title="{if $categories[category]->watchListed_flag}{jtext text='ADS_REMOVE_FROM_WATCHLIST'}{else}{jtext text='ADS_ADD_TO_WATCHLIST'}{/if}"/>
						</a>
					{/if}
					{*if $categories[category]->nr_a > 0*}
                    	({$categories[category]->nr_a} {jtext text="ADS_ADS"} )
					{*/if*}
					<br />
				</div>
				<div class="adds_subcat">
				{include file='elements/display_fields.tpl' position='categorydetails' this_add=$categories[category]->id}
				<br />
                    {*if $categories[category]->nr_a > 0*}   {jtext text="ADS_TOTAL_ADS"}: {$categories[category]->total_nr_ads} {*/if*}
                <br />
				<span style="font-size:12px;">{jtext text="ADS_SUBCATEGORIES"}:  {$categories[category]->kids} </span>
				<br />

				{section name=subcategory loop=$categories[category]->subcategories}
				
					{include file='elements/display_fields.tpl' position='categoryicon' this_add=$categories[category]->subcategories[subcategory]->id}
					<a href="{if $categories[category]->subcategories[subcategory]->kids>0}{$categories[category]->subcategories[subcategory]->link}{else}{$categories[category]->subcategories[subcategory]->view}{/if}">{$categories[category]->subcategories[subcategory]->catname}</a> 
					{if $categories[category]->subcategories[subcategory]->is_new==1}
						<img src="{$IMAGE_ROOT}new.png" alt="new ads" />
				 	 {/if}
					{*if $categories[category]->subcategories[subcategory]->nr_a > 0*}
						({$categories[category]->subcategories[subcategory]->nr_a} {jtext text="ADS_ADS"})
					{*/if*}
					{if $categories[category]->subcategories[subcategory]->watchListed_flag}
						<img src="{$IMAGE_ROOT}watchlist.png" width="16" class="ads_noborder" />
					{/if}
					{if $is_logged_in}
					<a href="{$categories[category]->subcategories[subcategory]->link_watchlist}">
						<img src="{$categories[category]->subcategories[subcategory]->img_src_watchlist}" width="14" class="ads_noborder" title="{if $categories[category]->subcategories[subcategory]->watchListed_flag}{jtext text='ADS_REMOVE_FROM_WATCHLIST'}{else}{jtext text='ADS_ADD_TO_WATCHLIST'}{/if}"/>
					</a>
					{/if}
					<br />
					<p style="margin-left:25px;">
					{include file='elements/display_fields.tpl' position='categorydetails' this_add=$categories[category]->id}
					</p>
				{/section}
				</div>
			</td>
		{if $smarty.section.category.rownum is not odd}
		</tr>
		{/if}
  {/section}
</table>
</div>
