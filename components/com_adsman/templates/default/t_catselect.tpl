{set_css}
	{include file='t_javascript_language.tpl'}
<div class="adsCatContainer">
	<h3 class="adCatHeader">{jtext text="ADS_PAGE_CATEGORIES"}</h3>
	<form name="category_select" action="{$ROOT_HOST}index.php?option=com_adsman&amp;task=edit&amp;{if $id}id={$id}&amp;{/if}Itemid={$Itemid}" method="post">
		<input type="hidden" name="category_hit" value="1" />
		<div class="category_select_toolbox">
			<a href="javascript:document.category_select.submit();">
				<span id="button_up">{jtext text="ADS_SELECT_CATEGORY"}</span>
			</a>
		</div>
	
		<table id="adds_categories_select" class="ads_table" width="100%">
		{foreach from=$categories key=key item=category}
		<tr>
			<td>
					<div id="cat_{$category.id}" style="display:none;">{$category.description}</div>
					<input type="radio" name="category" value="{$category.id}" onclick="toggleButton();" {if $cat==$category.id} checked="checked" {/if} />
					<sup>|_</sup>
					{section name="cur" loop=$category.depth}
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					{/section}
					<span {if $category.description } class="hasTip" title="{$category.description|htmlentities}" {/if} >
						{$category.catname}
					</span>
			</td>
		</tr>
		{/foreach}
		</table>
	
	</form>
	
	<div class="category_select_toolbox">
		<a href="javascript:document.category_select.submit();">
			<span id="button_dwn">{jtext text="ADS_SELECT_CATEGORY"}</span>
		</a>
	</div>
</div>