{include file='t_javascript_language.tpl'}
{assign var=nr_columns value="4"}
{assign var=col_width value="25"}
{assign var=nr_items value=$items|@count}
{set_css}
{set_js}

<!--<div class="componentheading">
  {if $task=="myadds"}{jtext text="ADS_PAGE_MY_ADS"}{else}{jtext text="ADS_PAGE_LISTADDS"}{/if}
</div>-->
<form action="{$action}" method="post" name="adsForm" id="adsForm">
	<input type="hidden" name="Itemid" value="{$Itemid}" />
	<input type="hidden" name="option" value="{$option}" />
	<input type="hidden" name="task" value="{$task}" />
	<input type="hidden" name="filter_order_Dir" value="{$filter_order_Dir}" />
	<input type="hidden" name="filter_order" value="{$filter_order}" />
    <input type="hidden" name="list_view" value="{$list_view}" />
    <input type="hidden" name="archive" value="{$archive}" />

	{foreach from=$filters item=item key=key}
	    <input type="hidden" name="{$key}" value="{$item}" />
	{/foreach}

	{include file="t_listadds_header.tpl"}
</form>

<span class="adds_man_list ">
{if $link_new_listing}
        <a href = "{$link_new_listing}"><img src = "{$IMAGE_ROOT}new_listing.png" border = "0"
           alt = "{jtext text="COM_ADS_NEW_LISTING_IN_CATEGORY"}"
           title = "{jtext text="COM_ADS_NEW_LISTING_IN_CATEGORY"}" /></a>
{/if}
</span>

<div class="adds_man_list_grid">
<table width="100%" cellpadding="0" cellspacing="0" class="ads_noborder ads_table">

{foreach from=$items item=item key=key name=item_loop}
    {if $item->featured && 	$item->featured!='none'}
        {assign var=class_featured value="listing-"|cat:$item->featured}
    {else}
        {assign var=class_featured value=""}
    {/if}

  {if $smarty.foreach.item_loop.index % $nr_columns == 0}
    <tr>
  {/if}
        <td align="left" valign="top" width="{$col_width}%" class="adds_man_item_grid">
            <div style="width: 180px;">
             <div style="float: left; padding-top: 0px;">
                 <div class="adds_man_item_thumb">
                    <a href="index.php?option={$option}&task=details&view=adsman&id={$item->id}">{$item->thumbnail}</a>
                 </div>
                 <div class="adds_man_item_grid_container">
                    <div class="adds_man_item_grid_title">
                       <a href="index.php?option={$option}&task=details&view=adsman&id={$item->id}">{$item->title}</a>
                       <span style="line-height: 13px;">
                           <div style="max-height:105px">
                               {if $smarty.const.ads_short_desc_long!=""}
                                   {$item->short_description|truncate:$smarty.const.ads_short_desc_long}
                               {else}
                                   {$item->short_description}
                                {/if}
                           </div>
                       </span>
                       <div style="position:absolute;bottom:0;margin-top:2px;">
                            {include file="elements/list/t_listadds_grid_user.tpl"}
                            <!--<span class="add_text_small">{*jtext text="ADS_CATEGORY"*}:</span>-->
                            <a href='{$item->links.filter_cat}' class="category_link">{$item->catname}</a>
                       </div>
                    </div>

                    <div class="adds_man_item_small_box {$class_featured}">

                       {if !$item->is_my_add}
                            {if $item->favorite==0 }
                                <span id='add_to_favorite'>
                                <a href='{$item->links.add_to_favorite}'><img src="{$IMAGE_ROOT}f_favoritelist_1.png" title="{jtext text='ADS_ADD_TO_FAVORITE'}" alt="{jtext text='ADS_ADD_TO_FAVORITE'}" height="16" class="ads_noborder" /></a>
                                </span>
                            {elseif $item->favorite==1}
                                <span id='add_to_favorite'><a href='{$item->links.del_from_favorite}'><img src="{$IMAGE_ROOT}f_favoritelist_0.png" title="{jtext text='ADS_REMOVE_FROM_FAVORITE'}" alt="{jtext text='ADS_REMOVE_FROM_FAVORITE'}" height="16" class="ads_noborder" /></a>
                                </span>
                            {/if}
                        {else}
                                <a href='{$item->links.edit}'><img src="{$IMAGE_ROOT}edit.png" title="{jtext text='ADS_EDIT'}" alt="{jtext text='ADS_EDIT'}" height="16" class="ads_noborder" /></a>
                        {/if}
                        {if $item->more_pictures}
                            <img src="{$IMAGE_ROOT}morepics_on.png" title="{jtext text='ADS_MORE_PICTURES'}" alt="{jtext text='ADS_MORE_PICTURES'}" height="16" class="ads_noborder" />
                        {else}
                            <img src="{$IMAGE_ROOT}morepics_off.png" title="{jtext text='ADS_NO_MORE_PICTURES'}" alt="{jtext text='ADS_NO_MORE_PICTURES'}" height="16" class="ads_noborder" />
                        {/if}
                        {if $item->atachment}
                            <img src="{$IMAGE_ROOT}downloads_on.png" title="{jtext text='ADS_HAS_DOWNLOADS'}" alt="{jtext text='ADS_HAS_DOWNLOADS'}" height="16" class="ads_noborder" />
                        {else}
                            <img src="{$IMAGE_ROOT}downloads_off.png" title="{jtext text='ADS_HAS_NO_DOWNLOADS'}" alt="{jtext text='ADS_HAS_NO_DOWNLOADS'}" height="16" class="ads_noborder" />
                        {/if}

                        <div style="clear: both;"></div>
                    </div>

                    <div class="adds_man_item_small_box">
                        <strong><span style="float: right !important; font-size: 1.6em;">{$item->askprice}&nbsp;
                                                 {if is_numeric($item->askprice)}{$item->currency_name}{/if}</span></strong>
                    </div>

                    <div class="adds_man_item_small_box">

                        {if  $item->expired }
                            <div>
                                <span class='expired'>{jtext text="ADS_EXPIRED"}</span>
                            </div>
                        {elseif $item->closed && $item->status == 1 && !$item->archived && !$item->close_by_admin && $item->closed_date <= $item->end_date}
                           <div>
                                <span class='expired'>{jtext text="ADS_AD_CANCELED"}</span>
                            </div>
                        {elseif $smarty.const.ads_opt_enable_countdown && !$item->closed && !$item->close_by_admin}
                            <div>
                                {if $smarty.const.ads_opt_sort_date == 'start'}
                                    {jtext text="ADS_POSTED_ON"}: {$item->start_date_text}
                                {else}
                                    <span id="time{$key+1}">{$item->countdown}</span>
                                {/if}
                            </div>
                        {elseif $item->archived}
                           <div>
                                <span class='expired'>{jtext text="ADS_AD_ARCHIVED"}</span>
                            </div>
                        {elseif $item->close_by_admin}
                            <div>
                                <span class='expired'>{jtext text="ADS_AD_CLOSED_BY_ADMIN"}</span>
                            </div>
                        {/if}
                    </div>

                    <!--<div class="adds_man_item_small_box">
                        <strong>{*jtext text="ADS_TAGS"}</strong>: {$item->links.tags*}
                    </div>-->

                 <!-- <div class="adds_man_item_small_box">
                    {*include file="elements/list/t_listadds_grid_user.tpl"*}
                  </div>-->
                 </div>
             </div>
            </div>
        </td>

  {if $smarty.foreach.item_loop.index % $nr_columns == $nr_columns-1 || $nr_items-1==$smarty.foreach.item_loop.index}
    {if $nr_items-1==$smarty.foreach.item_loop.index}
        {assign var=nrt value=`$nr_columns-$smarty.foreach.item_loop.index%$nr_columns-1`}
        {section name=trailing loop=$nrt}

        <td>&nbsp;</td>

        {/section}
    {/if}
    </tr>
    <tr><td>&nbsp;</td></tr>
  {/if}
{/foreach}
</table>
{if !($items|@count>0) }
	<div class="adds_man_item1">
	 {jtext text="ADS_NO_ADS"}
	</div>
{/if}
</div>
