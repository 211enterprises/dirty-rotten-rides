{include file='t_javascript_language.tpl'}
{set_css}
<form id="adsForm" name="adsForm" method="post" onsubmit="return checkReportForm()">
    <input type="hidden" name="option" value="com_adsman" />
    <input type="hidden" name="task" value="report_add" />
    <table class="add_reports">
    	<tr>
    		<th align="center"><span>{jtext text="ADS_REPORT_INFO"}</span></th>
    	</tr>
    	<tr>
    		<td><textarea id="comment" name="comment"></textarea></td>
    	</tr>
    	<tr>
    		<td align="center"><input type="submit" value="{jtext text='ADS_REPORT'}" class="back_button" /></td>
    	</tr>
    </table>
</form>