{set_css}

<h2>Set featured image</h2>

<form action="{$ROOT_HOST}index.php" method="post" name="adsForm" id="adsForm" enctype="multipart/form-data" >
<input type="hidden" name="Itemid" value="{$Itemid}" />
<input name="option" type="hidden" value="com_adsman" />
<input type="hidden" name="id" value="{$add->id}" />
<input type="hidden" name="task" value="save_images" />
<input type="hidden" name="image_index_selected" value="{$selected_images_index}" />
<input type="hidden" name="select_credits_main" value="{$select_credits_main}" />
<!--<input type="hidden" name="oldpic" value="{$oldpic}" />-->



<table>
	{if $add->picture}
 	<tr>
 		<td>{jtext text="ADS_MAIN_PICTURE"}</td>
		<td style="background:#EDEDED;"><img src="{$ADSMAN_PICTURES}/resize_{$add->picture}" />
            <!--<img src="{$ROOT_HOST}components/com_adsman/images/resize_{$add->picture}">-->
        </td>
 	</tr>
 	{else}
	 	{if $select_credits_main == 1}
	 	  <tr>
	 		<td>{jtext text="ADS_MAIN_PICTURE"}</td>
	 		<td><input type="file" name="picture" /></td>
	 	  </tr>
	 	{/if}
 	{/if}
 	{if $add->images|@count}
   	 	<tr>
   	 		<td colspan="2">{jtext text="ADS_NEW_IMAGES"}</td>
   	 	</tr>
   	 	{section name=image loop=$add->images}
  		<tr>
   			<td><img src="{$ADSMAN_PICTURES}/resize_{$add->images[image]->picture}" />
                   <!--<img src="{$ROOT_HOST}components/com_adsman/images/resize_{$add->images[image]->picture}" />-->
               </td>
   		</tr>
   		{/section}
	{/if}

	{if $add->images|@count < $smarty.const.ads_opt_maxnr_images }
 	<tr>
 		<td>{jtext text="ADS_NEW_IMAGES"}</td>
	 	<td>
			<div id="files">
			{if $selected_images_number != 0}
				{if $smarty.const.ads_opt_maxnr_images - $selected_images_number - $add->images|@count >= 0}
				
					<input class="inputbox" id="my_file_element" type="file" name="pictures_1" />
					<div id="files_list"></div>
					{literal}
					<script>
						var max_img_upload = {/literal}{$smarty.const.ads_opt_maxnr_images} - {$add->images|@count}{literal};
						var selected_images_no = {/literal}{$selected_images_number}{literal};
		        //alert(max_img_upload);
		        
		        
						if (max_img_upload >= 0 && selected_images_no <= max_img_upload)
						{
							var multi_selector = new MultiSelector( document.getElementById('files_list'),selected_images_no )
								
							multi_selector.addElement( document.getElementById( 'my_file_element' ) );
						}
						else {
							//var max_img_upload = {/literal}{$selected_images_number}{literal};
							alert('You can not upload '+ selected_images_no +' pictures');
						}
				
					
					</script>
					{/literal}
				
				{elseif  $smarty.const.ads_opt_maxnr_images - $selected_images_number - $add->images|@count == 0}
					
				{else}
					{assign var=total value=$add->images|@count} 	
					{jtext text="ADS_IMAGE_UPLOAD_REACHED"}  {jtext text="ADS_NEW_IMAGES"}
				{/if}
			{/if}	
			</div>
		 </td>
	 </tr>
	{/if}
</table>
<input name="save" value="{jtext text='ADS_SAVE'}" class="back_button" type="submit" />
<a href="{$lists.cancel_form}" class="back_button_cancel"><input name="cancel" value="{jtext text='ADS_CANCEL'}" class="back_button" type="button" /></a>
</form>