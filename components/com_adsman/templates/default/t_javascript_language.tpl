{import_js_block}
var ROOT_HOST = '{$ROOT_HOST}';
var Validation_Messages=Array();
Validation_Messages["required"] = "This field is required!";
Validation_Messages["email"] = "This field is not a valid email!";

var language=Array();

language["adsman_err_enter_message"] 			= '{jtext text="ADS_ERR_ENTER_MESSAGE"}';
language["adsman_err_login_to_send_message"] 	= '{jtext text="ADS_ERR_LOGIN_TO_SEND_MESSAGE"}';
language["adsman_err_enter_title"] 				= '{jtext text="ADS_ERR_ENTER_TITLE"}';
language["adsman_err_dates_invalid"] 			= '{jtext text="ADS_ERR_DATES_INVALID"}';
language["adsman_err_main_img_comp"] 			= '{jtext text="ADS_ERR_MAIN_IMG_COMPULSORY"}';
language["adsman_err_price"] 					= '{jtext text="ADS_ERR_PRICE"}';
language["adsman_err_price_valid"] 				= '{jtext text="ADS_ERR_PRICE_VALID"}';
language["adsman_err_terms"] 					= '{jtext text="ADS_ERR_TERMS"}';
language["adsman_err_adtype"] 					= '{jtext text="ADS_ERR_ADTYPE"}';
language["adsman_select_category"] 				= '{jtext text="ADS_SELECT_CATEGORY"}';
language["adsman_choose_category"] 				= '{jtext text="ADS_CHOOSE_CATEGORY"}';
language["adsman_fill_in_fields"] 				= '{jtext text="ADS_FILL_IN_FIELDS"}';
language["adsman_err_extension"] 				= '{jtext text="ADS_ERR_EXTENSION"}';
language["adsman_err_postcode"] 				= '{jtext text="ADS_ERR_POSTCODE"}';
language["ads_current_position"] 				= '{jtext text="ADS_CURRENT_POSITION"}';
language["ads_drag_to_change_position"]         = '{jtext text="ADS_DRAG_TO_CHANGE_POSITION"}';
language["ads_choose_gateway"]                  = '{jtext text="ADS_CHOOSE_GATEWAY"}'

if(typeof Calendar != "undefined")
	Calendar._TT["DEF_DATE_FORMAT"]=dateformat('{$smarty.const.ads_opt_date_format}');

{literal}
function dateformat(php_format)
{
    d='%Y-%M-%d';
    if (php_format=='Y-m-d') d='%Y-%M-%d';
    if (php_format=='Y-d-m') d='%Y-%d-%M';
    if (php_format=='Y/m/d') d='%Y/%M/%d';

    return d;
}
{/literal}
{/import_js_block}