{set_css}
{set_js}
{include file='t_javascript_language.tpl'}

<form action="{$action}" method="POST" name="adsForm">
<input type="hidden" name="Itemid" value="{$Itemid}" />
<input type="hidden" name="option" value="{$option}" />
<input type="hidden" name="tag" value="" />

<div class="componentheading">
	<span style="float:left;"><img src="{$IMAGE_ROOT}search.png" title="{jtext text='ADS_PAGE_SEARCH'}" alt="{jtext text='ADS_PAGE_SEARCH'}" height="32" />&nbsp;</span>
	<span style="vertical-align:middle;">{jtext text="ADS_PAGE_SEARCH"}</span>
	&nbsp;&nbsp;
	<span style="vertical-align:middle;">	
 		<input name="save" value="{jtext text='ADS_SEARCH'}" class="back_button" type="submit" />
 		<a href="{$lists.cancel_form}" class="back_button_cancel"><input name="cancel" value="{jtext text='ADS_CANCEL'}" class="back_button" type="button" /></a>
 	</span>	
</div>

<table border="0" width="100%" cellpadding="2" cellspacing="8" class="add_search_form ads_table">
	<tr>
		<td>{jtext text="ADS_SEARCH_KEYWORD"}</td>
		<td colspan="3"><input type="text" name="keyword" class="inputbox" size="55" /></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="3" align="left" style="padding-bottom:20px;">
			<input name="in_description" type="checkbox" value="1" />&nbsp;{jtext text="ADS_SEARCH_IN_DESCRIPTION"}<br />
		</td>
	</tr>
	<tr>
		<td>{jtext text="ADS_CATEGORY"}</td>
		<td colspan="3">
		{if $lists.category==""}
			<span id="category_axj_space"><img src="{$IMAGE_ROOT}ajax-loader.gif" /></span>
		{else}
			{$lists.category}
		{/if}
		</td>
	</tr>
	<tr>
		<td>{jtext text="ADS_AD_TYPE"}</td>
		<td colspan="3">{$lists.addtype}</td>
	</tr>
    <tr><td colspan="4">&nbsp;</td></tr>
    <tr>
      <td>{jtext text="ADS_SEARCH_MIN_PRICE"}</td>
      <td colspan="3">{$lists.price_min} &nbsp;&nbsp; - &nbsp;&nbsp; {jtext text="ADS_SEARCH_MAX_PRICE"} &nbsp;&nbsp; {$lists.price_max}  &nbsp;&nbsp; {$lists.currency}</td>
    </tr>
	<tr>
	  <td>{jtext text="ADS_SEARCH_START_DATE"}</td>
 	  <td>{$lists.start_date_calendar}</td>
	  <td style="padding-left: 5px;">{jtext text="ADS_SEARCH_END_DATE"}</td>
	  <td>{$lists.end_date_calendar}</td>
 	</tr>
    <tr><td colspan="4">&nbsp;</td></tr>
    <tr>
        <td class="add_search_label">{jtext text="ADS_AD_CITY"}</td>
        <td>{$lists.ad_city}</td>
        <td class="add_search_label" style="padding-left: 5px;">{jtext text="ADS_AD_POSTCODE"}</td>
        <td>{$lists.ad_postcode}</td>
    </tr>
    <tr><td colspan="4">&nbsp;</td></tr>
 	<tr>
		<td>{jtext text="ADS_SEARCH_USERS"}</td>
		<td>{$lists.users}</td>
		<td style="padding-left: 5px;">{jtext text="ADS_COUNTRY"}</td>
		<td>{$lists.country}</td>
	</tr>
	<tr>
		<td>{jtext text="ADS_CITY"}</td>	
		<td>{$lists.city}</td>
		<td style="padding-left: 5px;">{jtext text="ADS_STATE"}</td>
		<td>{$lists.state}</td>
	</tr>
    <tr><td colspan="4">&nbsp;</td></tr>


	
</table>
	<div id="custom_fields_container"></div>
</form>