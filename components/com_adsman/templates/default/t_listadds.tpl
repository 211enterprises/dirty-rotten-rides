{include file='t_javascript_language.tpl'}
{set_css}
{set_js}

<form action="{$action}" method="post" name="adsForm" id="adsForm">
	<input type="hidden" name="Itemid" value="{$Itemid}" />
	<input type="hidden" name="option" value="{$option}" />
	<input type="hidden" name="task" value="{$task}" />
	<input type="hidden" name="filter_order_Dir" value="{$filter_order_Dir}" />
	<input type="hidden" name="filter_order" value="{$filter_order}" />
    <input type="hidden" name="list_view" value="{$list_view}" />
    <input type="hidden" name="archive" value="{$archive}" />

    {include file="t_listadds_header.tpl"}

</form>
<div class="adds_man_list">
{if $link_new_listing}
        <a href = "{$link_new_listing}"><img src = "{$IMAGE_ROOT}new_listing.png" class="ads_noborder"
           alt = "{jtext text="COM_ADS_NEW_LISTING_IN_CATEGORY"}"
           title = "{jtext text="COM_ADS_NEW_LISTING_IN_CATEGORY"}" /></a>
{/if}

    <table style="width:100%;" cellpadding="0" cellspacing="0" class="ads_noborder">
        {foreach from=$items item=item key=key}
            {if $item->featured && 	$item->featured!='none'}
                {assign var=class_featured value="listing-"|cat:$item->featured}
            {else}
                {assign var=class_featured value=""}
            {/if}
            {cycle assign=class_suffix values="1,2"}
            <tr class="adds_man_item{$class_suffix}_title {$class_featured}">
                <td valign="top" style="min-width:75px; max-width: 105px; padding: 2px;">
                    <a href="{$item->links.details}">{$item->thumbnail}</a>
                </td>
                <td valign="top">
                   <a href="{$item->links.details}"><span style="text-decoration: none; font-size: 14px;">{$item->title}</span></a>
                   <div class="adsman_shortdescription">
                       {if $smarty.const.ads_short_desc_long!="" && $item->short_description != ""}
                            {$item->short_description|truncate:$smarty.const.ads_short_desc_long}
                       {elseif $item->short_description != ""}
                           {$item->short_description}
                       {else}
                          &nbsp;
                       {/if}
                   </div>
                    <div style="font-size: 11px; bottom: 2px; margin-top: 5px;">
                        <a href='{$item->links.filter_cat}' class="category_link">{$item->catname}</a>&nbsp;
                   </div>

                   {include file='elements/display_fields.tpl' position='listingmiddle' this_add=$item->id}

                </td>
                <td valign="middle" class="price">{if $item->askprice}{$item->askprice}{/if} {if is_numeric($item->askprice)} <span class="currency">{$item->currency_name}</span>{/if}</td>
                <td valign="middle" style="text-align: right; width: 140px; max-width: 150px; padding-right: 3px;">
                    {if  $item->expired }
                        <div>
                            <span class='expired'>{jtext text="ADS_EXPIRED"}</span>
                        </div>
                    {elseif $item->closed && $item->status == 1 && !$item->archived && !$item->close_by_admin && $item->closed_date <= $item->end_date}
                       <div>
                            <span class='expired'>{jtext text="ADS_AD_CANCELED"}</span>
                        </div>
                    {elseif $smarty.const.ads_opt_enable_countdown && !$item->closed && !$item->close_by_admin}
                        <div>
                            {if $smarty.const.ads_opt_sort_date == 'start'}
                                {jtext text="ADS_POSTED_ON"}: {$item->start_date_text}
                            {else}
                                <span id="time{$key+1}">{$item->countdown}</span>
                            {/if}
                        </div>
                    {elseif $item->archived}
                       <div>
                            <span class='expired'>{jtext text="ADS_AD_ARCHIVED"}</span>
                        </div>
                    {elseif $item->close_by_admin}
                        <div>
                            <span class='expired'>{jtext text="ADS_AD_CLOSED_BY_ADMIN"}</span>
                        </div>
                    {/if}
                </td>
            </tr>
            <tr class="adds_man_item{$class_suffix}_footer {$class_featured}">
               <td>
                   <span style="padding: 3px 2px; font-size: 11px;">{jtext text="ADS_HITS"}: {$item->hits}</span>
               </td>
               <td colspan="3" style="font-size: 11px;">
                    {*
                        Ex field: {$item->ex_field};
                    *}
                    {*jtext text="ADS_TAGS"}: {$item->links.tags*}
                    {if !$item->is_my_add}
                        {if $item->favorite==0 }
                            <span id='add_to_favorite'>
                                <a href='{$item->links.add_to_favorite}'>
                                    <img src="{$IMAGE_ROOT}f_favoritelist_1.png" title="{jtext text='ADS_ADD_TO_FAVORITE'}" alt="{jtext text='ADS_ADD_TO_FAVORITE'}" height="16"  class="ads_noborder" />
                                </a>
                            </span>
                        {elseif $item->favorite==1}
                            <span id='add_to_favorite'>
                                <a href='{$item->links.del_from_favorite}'>
                                    <img src="{$IMAGE_ROOT}f_favoritelist_0.png" title="{jtext text='ADS_REMOVE_FROM_FAVORITE'}" alt="{jtext text='ADS_REMOVE_FROM_FAVORITE'}" height="16"  class="ads_noborder" />
                                </a>
                            </span>
                        {/if}
                    {else}
                        <a href='{$item->links.edit}'>
                            <img src="{$IMAGE_ROOT}edit.png" title="{jtext text='ADS_EDIT'}" alt="{jtext text='ADS_EDIT'}" height="16"  class="ads_noborder" />
                        </a>
                    {/if}

                    {if $item->more_pictures}
                        <img src="{$IMAGE_ROOT}morepics_on_24.png" title="{jtext text='ADS_MORE_PICTURES'}" alt="{jtext text='ADS_MORE_PICTURES'}" height="16"  class="ads_noborder" />
                    {else}
                        <img src="{$IMAGE_ROOT}morepics_off_24.png" title="{jtext text='ADS_NO_MORE_PICTURES'}" alt="{jtext text='ADS_NO_MORE_PICTURES'}" height="16"  class="ads_noborder" />
                    {/if}

                    {if $smarty.const.ads_allow_atachment}
                       {if $item->atachment}
                         <img src="{$IMAGE_ROOT}downloads_on.png" title="{jtext text='ADS_HAS_DOWNLOADS'}" alt="{jtext text='ADS_HAS_DOWNLOADS'}" height="16" class="ads_noborder" />
                       {else}
                         <img src="{$IMAGE_ROOT}downloads_off.png" title="{jtext text='ADS_HAS_NO_DOWNLOADS'}" alt="{jtext text='ADS_HAS_NO_DOWNLOADS'}" height="16" class="ads_noborder" />
                       {/if}
                    {/if}
                    {if $smarty.const.ads_opt_google_key!="" && $smarty.const.ads_map_in_ads_details=="1"}
                       {if $item->MapX!="" && $item->MapY!=""}
                           <a class="modal" href="{$ROOT_HOST}index.php?option=com_adsman&amp;task=googlemap&amp;x={$item->MapX}&amp;y={$item->MapY}&amp;id={$item->id}&amp;tmpl=component"{literal} rel="{handler: 'iframe', size: { {/literal} x:{$smarty.const.ads_opt_googlemap_gx+25|default:'260'}, y:{$smarty.const.ads_opt_googlemap_gy+20|default:'100'}} {literal} } " {/literal}>
                               <img src="{$IMAGE_ROOT}map.png" title="{jtext text='ADS_GOOGLEMAP'}" alt="{jtext text='ADS_GOOGLEMAP'}" height="16" class="ads_noborder" />
                           </a>
                       {/if}
                    {/if}
                    <span style="font-size: 12px; vertical-align: top;  padding: 3px;">{include file="elements/list/t_listadds_user.tpl"}</span>
                    {include file='elements/display_fields.tpl' position='favoritesads'}
               </td>
            </tr>
            <tr>
               <td colspan="4" style="height:1px;line-height:7px;border:0px; border-bottom:1px dashed #003563;">
               &nbsp;
               </td>
            </tr>

        {/foreach}
        </table>

<div class="pagination">
		{$pagination->getPagesLinks()}
	</div>

{if !($items|@count>0) }
	<div class="adds_man_item1">
	 {jtext text="ADS_NO_ADS"}
	</div>
{/if}
</div>