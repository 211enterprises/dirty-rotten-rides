{set_css}
{include file='t_javascript_language.tpl'}


<form action="{$ROOT_HOST}index.php" method="post" name="adsForm" id="adsForm" enctype="multipart/form-data" class="form-validate" onSubmit="return myValidate(this);">
<input type="hidden" name="Itemid" value="{$Itemid}" />
<input type="hidden" name="option" value="{$option}" />
<input type="hidden" name="id" value="{$add->id}" />
<input type="hidden" name="task" value="save" />
<input type="hidden" name="oldid" value="{$oldid}" />
<input type="hidden" name="oldpic" value="{$oldpic}" />

{if ($add->id && $add->featured != 'none') || $listing_enabled}
    <span style="font-size: 100%;">{$edit_24hours_info}</span><br />
{/if}


<div class="componentheading">
<span style="vertical-align:middle;">
{if $add->id}
  {jtext text="ADS_PAGE_EDIT"}
  {if $add->status}  ( {jtext text="ADS_PAGE_PUBLISHED"} )
  {else}
    ( {jtext text="ADS_PAGE_UNPUBLISHED"} )
  {/if}
{else}
  {jtext text="ADS_PAGE_ADD_NEW_AD"}
{/if}
</span>

	<!--<input name="save" id="save_ad" value="{jtext text='ADS_SAVE'}" class="back_button" type="submit" />
	<a href="{$lists.cancel_form}" class="back_button_cancel"><input name="cancel" value="{jtext text='ADS_CANCEL'}" class="back_button" type="button" /></a>-->
</div>
	<div id="invalid_info" style="display:none; border:#FF0000 solid 2px; padding:2px; color:#FF0000;"></div>
	<table style="border-collapse: separate !important;" cellpadding="1" cellspacing="5" >
		<tr>
			<td valign="top"><label for="title">{jtext text="ADS_TITLE"}<span style="color:#FF0000;">*</span></label></td>
			<td>{if $allow_edit==1}
			    <input class="inputbox required" type="text" id="title" name="title" value="{$add->title}" size="80" />
                {else}
                    {$add->title}
                    <input type="hidden" name="title" value="{$add->title}" />
                {/if}
		  	<span class="validate_tip" id="tip_title"></span>
			</td>
		</tr>
		<tr>
			<td valign="top"><label for="category">{jtext text="ADS_CATEGORY"}</label></td>
			<td>

                    {if $lists.category==""}
                        <span id="category_axj_space"><img src="{$IMAGE_ROOT}ajax-loader.gif" /></span>
                    {else}
                        {$lists.category}
                    {/if}
                    {if $smarty.const.ads_opt_category_page || $smarty.post.category>0  || $smarty.post.cat>0}
                        {if $add->id}
                            <a href="{$ROOT_HOST}index.php?option=com_adsman&task=selectcat&id={$add->id}">{jtext text="ADS_CHANGE_CATEGORY"}</a>
                        {else}
                            <a href="{$ROOT_HOST}index.php?option=com_adsman&task=selectcat">{jtext text="ADS_CHANGE_CATEGORY"}</a>
                        {/if}
                    {/if}

				{if $time_limited==0 && $listing_enabled==1}
					<span id="category_price_container" style="padding:2px; color:#FF0000;">
						{jtext text="ADS_PRICE_LISTING_IMAGES"} {$credits_category_ini} {jtext text="ADS_CREDITS"}
					</span>
				{/if}
			</td>
		</tr>
		{if $smarty.const.ads_opt_adtype_enable==0 && $smarty.const.ads_opt_adtype_val!=''}
			{$lists.addtype}
		{else}
		<tr>
			<td valign="top">{jtext text="ADS_AD_TYPE"}</td>
			<td>{$lists.addtype}&nbsp;{jtext text="ADS_HELP_PRIVATE"}</td>
		</tr>
		{/if}
		{if $smarty.const.ads_opt_adpublish_enable == 0 && $smarty.const.ads_opt_adpublish_val != ''}
			{$lists.status}
		{else}
	</table>
	<table class="ads_noborder ads_table" style="border-collapse: separate !important;" cellpadding="1" cellspacing="5">
        {if $listing_enabled == 1}
            {$lists.status}
        {else}
            <tr>
                <td valign="top">{jtext text="ADS_PUBLISHED"}</td>
                <td>{$lists.status}</td>
            </tr>
        {/if}

		{/if}
		<tr>
			<td valign="top"><label for="short_description">{jtext text="ADS_SHORT_DESCRIPTION"}</label></td>
			<td><textarea id="short_description" name="short_description" style="width:350px;" class="inputbox">{$add->short_description}</textarea>(<span id='descr_counter'>0</span>/{$smarty.const.ads_short_desc_long} chars)</td>
		</tr>


		{if $smarty.const.ads_opt_price_enable==1}
		<tr>
			<td valign="top"><label for="askprice">{jtext text="ADS_ASKPRICE"}{if $smarty.const.ads_price_compulsory==1}<span style="color:#FF0000;">*</span>{/if}</label></td>
			<td><input class="inputbox {if $smarty.const.ads_price_compulsory==1} required {/if} validate-numeric" type="text" id="askprice" name="askprice" value="{$add->askprice}" size="10" style="text-align: right !important;" /> {$lists.currency}</td>
		</tr>
		{/if}
		
		{* Listing Pricing Enabled Valability Option *}
		
		{if $time_limited==0}
		<tr>
			<td valign="top"><label for="start_date">{jtext text="ADS_START_DATE"}</label><span style="color:#FF0000;">*</span></td>
			<td>
				{if $add->id}
					{printdate date=$add->start_date}
					<input class="text_area" type="hidden" name="start_date" id="start_date" size="15" maxlength="19" value="{$add->start_date|date_format:$opt_date_format}" alt="start_date"/>
				{else}
                    {$lists.start_date_calendar}
				{/if}
			</td>
		</tr>
		<tr>
			<td valign="top"><label for="end_date">{jtext text="ADS_END_DATE"}</label><span style="color:#FF0000;">*</span></td>
			<td>
				{if $add->id}
					{printdate date=$add->end_date use_hour=1}
					<input class="text_area" type="hidden" name="end_date" id="end_date" size="15" maxlength="19" value="{$add->end_date|date_format:$opt_date_format}" alt="end_date"/>
				{else}

					{$lists.end_date_calendar}
					
					{if $smarty.const.ads_opt_enable_hour}
						<input name="end_hour" size="1" value="00" alt="" class="inputbox" /> :
						<input name="end_minutes" size="1" value="00" alt="" class="inputbox" />
					{/if}
				{/if}
			</td>
	 	</tr>
	 	{else}
	 	<tr>
	 		<td valign="top"><label for="end_date">{jtext text="ADS_AVAILABILITY"}</label></td>
	 		<td valign="top">
                 <input class="text_area" readonly type="hidden" name="start_date" id="start_date" size="15" maxlength="19" value="{$lists.valab_now|date_format:$opt_date_format}" alt="start_date"/>
                 <input class="text_area" type="hidden" name="end_date" id="end_date" size="15" maxlength="19" value="{$lists.valab_end|date_format:$opt_date_format}" alt="end_date"/>

                {if $add->id}

                    <select id="ad_valability" name="ad_valability" onchange="document.getElementById('end_date').value=VList[this.value]; showValabilityPrice(VPriceList[this.value]);">
						{foreach from=$valabs_list key=key item=item}
                            {if $add->status}
                                {*jtext text="valab_period_not editable"*}
                                {if $item == $lists.db_no_of_days}
                                    <option value="{$key}" selected="selected" title="{get_now|add_day:$item|date_format:$opt_date_format}">{$item} days</option>
                                {/if}
                            {else}
                                {*"display selected period but editable"*}
                                {if $item == $lists.db_no_of_days}
                                    <option value="{$key}" selected="selected" title="{get_now|add_day:$item|date_format:$opt_date_format}">{$item} days</option>
                                {else}
                                 <!--   <option value="{$key}" title="{get_now|add_day:$item|date_format:$opt_date_format}">{$item} days</option>-->
                                {/if}
                            {/if}
						{/foreach}
					</select>
                    <!--<span id="pricing_info" style="padding:2px; color:#FF0000;">{*jtext text="ADS_PRICE_LISTING_VALABILITY"}{$valabs_prices.$valabs_default_val} {$valabs_currency*}</span>-->
                    {printdate date=$add->start_date} - {printdate date=$add->end_date use_hour=1}
                    {*/if*}

                {else}
	 										
					<select id="ad_valability" name="ad_valability" onchange="document.getElementById('end_date').value=VList[this.value]; showValabilityPrice(VPriceList[this.value]);">
						{foreach from=$valabs_list key=key item=item}
							<option value="{$key}" title="{get_now|add_day:$item|date_format:$opt_date_format}">{$item} days</option>
						{/foreach}
					</select>
					<span id="pricing_info" style="padding:2px; color:#FF0000;">{jtext text="ADS_PRICE_LISTING_VALABILITY"} {$valabs_prices.$valabs_default_val} {$valabs_currency}</span>
                {/if}
	 		</td>
	 	</tr>
	 	{/if}
		{* end Listing Pricing Enabled Valability Option *}

        <tr>
			<td valign="top">{jtext text="ADS_NEW_DESCRIPTION"}<span style="color:#FF0000;">*</span></td>
			<td class="required">{$lists.description}</td>
		</tr>

        {if $smarty.const.ads_opt_google_key!=""}
            <tr><td colspan="2">&nbsp;</td></tr>
            <tr>
                <td colspan="2">
                    {if $user->googleX !="" && $user->googleY !="" }
                          <a href="#" onclick="gmap_move_marker({literal}{{/literal}pointTox:{$user->googleX},pointToy:{$user->googleY}{literal}}{/literal}); return false;">{jtext text="ADS_KEEP_PROFILE_COORDINATES"}</a> 
                    {/if}
                    <div id="map_canvas"></div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <label for="MapX">{jtext text="ADS_GOOGLE_X_COORDINATE"}:</label>
                    <input class="inputbox" type="text" id="googleX" name="MapX" value="{$add->MapX}" size="30" />
                    <label for="MapY">{jtext text="ADS_GOOGLE_Y_COORDINATE"}:</label>
                    <input class="inputbox" type="text" id="googleY" name="MapY" value="{$add->MapY}" size="30"/>
                </td>
            </tr>
            <tr><td colspan="2">&nbsp;</td></tr>
		{/if}

        <tr>
			<td valign="top">{jtext text="ADS_AD_CITY"}</td>
			<td><input class="inputbox" type="text" id="ad_city" name="ad_city" value="{$add->ad_city}" size="20" /></td>
		</tr>
        <tr>
			<td valign="top">{jtext text="ADS_AD_POSTCODE"}</td>
			<td><input class="inputbox validate" type="text" id="ad_postcode" name="ad_postcode" value="{$add->ad_postcode}" size="20" /></td>
		</tr>

	 	{if $add->picture}
		 	<tr>
		 		<td colspan="2">{jtext text="ADS_MAIN_PICTURE"}</td>
		 	</tr>
		 	<tr>
		 		<td>&nbsp;</td>    		
				<td style="background:#EDEDED;">
	    			<table>
	    			  <tr>
						<td>
                            <img src="{$ADSMAN_PICTURES}/resize_{$add->picture}" />
                            <!--<img src="{$ROOT_HOST}components/com_adsman/images/resize_{$add->picture}">-->
                            </td>
						<td>{if $allow_edit==1}
                            <input type="checkbox" id="delete_main_picture" name="delete_main_picture" value="1" />&nbsp;{jtext text="ADS_DELETE_PICTURE"}
                            {else}
                                &nbsp;
                            {/if}
                        </td>
					  </tr>
					</table>	
    			</td>
		 	</tr>
	 	{else}
          {if $allow_edit==1}
	 		{if $pay_img_enabled==0 || $main_img_free}
			 	<tr>
			 		<td>{jtext text="ADS_MAIN_PICTURE"}</td>
			 		<td><input type="file" name="picture" id="picture" /></td>
			 	</tr>
			 {else}
				 {*if $pay_img_enabled==1 && $credits > 0*}
				 {if $pay_img_enabled==1 && $isGuest == 0}
					<tr>
				 		<td>{jtext text="ADS_ADD_MAIN_PICTURE"}{if $smarty.const.ads_opt_require_picture==1}<span style="color:#FF0000;">*</span>{/if}</td>
				 		<td>
				 		{if $smarty.const.ads_opt_require_picture==1}
				 			<input type="file" name="picture" id="picture" />
				 		{else}
							<input type="file" name="picture" id="picture" />
				 			{*$select_credits_main*}
				 		{/if}
				 		<span id="pricing_main_img_info" style="padding:2px; color:#FF0000;">{jtext text="ADS_PRICE_MAIN_IMAGE"} {$credits_main} {jtext text="ADS_CREDITS"}</span>
				 		</td>
				 	</tr>
				 {/if}		
	 		{/if}
          {else}
            &nbsp;
          {/if}
	 	{/if}
	 	
	 	{if $add->images|@count}
    	 	<tr>
    	 		<td colspan="2">{jtext text="ADS_NEW_PICTURES"}</td>
    	 	</tr>
    	 	{section name=image loop=$add->images}
    		<tr>
				<td>&nbsp;</td>    		
				<td style="background:#EDEDED;">
	    			<table>
	    				<tr>
	    					<td>
                                <img src="{$ADSMAN_PICTURES}/resize_{$add->images[image]->picture}" />
                                <!--<img src="{$ROOT_HOST}components/com_adsman/images/resize_{$add->images[image]->picture}" />-->
                            </td>
	    					<td>{if $allow_edit==1}
                                    <input type="checkbox" name="delete_pictures[]" value="{$add->images[image]->id}" />&nbsp;{jtext text="ADS_DELETE_PICTURE"}
                                {else}
                                    &nbsp;
                                {/if}
                            </td>
	    				</tr>
	    			</table>	
    			</td>
    		</tr>
    		{/section}
		{/if}

		{if $add->images|@count < $smarty.const.ads_opt_maxnr_images }
          {if $allow_edit==1}
			{if $pay_img_enabled==1}
		  
		    	{if $images_upload_free_no >= 1}
		  
					{if $images_upload_free_no > $add->images|@count && $images_upload_free_no - $add->images|@count <= $smarty.const.ads_opt_maxnr_images}
						<tr>
				 			<td>{jtext text="ADS_NEW_IMAGES"}</td>
					 		<td>
					 			<div id="files">
								<input class="inputbox" id="my_file_element1" type="file" name="pictures_1" />
								<div id="files_list1"></div>
								{literal}
								<script>
									var max_img_upload = {/literal}{$smarty.const.ads_opt_maxnr_images} - {$add->images|@count}{literal};
									var images_upload_free_no = {/literal}{$images_upload_free_no} - {$add->images|@count}{literal};


									if (max_img_upload >= 0 && images_upload_free_no <= max_img_upload)
									{
										var multi_selector1 = new MultiSelector( document.getElementById('files_list1'),max_img_upload )
                                        //var multi_selector1 = new MultiSelector( document.getElementById('files_list1'),images_upload_free_no )
										multi_selector1.addElement( document.getElementById( 'my_file_element1' ) );
									}
									else {
										alert('You can not upload '+ images_upload_free_no +' pictures');
									}
								</script>
								{/literal}
							</td>
					 	</tr>
				 	{else}
				   		{if $images_upload_free_no > $add->images|@count && $smarty.const.ads_opt_maxnr_images <= $images_upload_free_no }
						 	<tr>
						 		<td>{jtext text="ADS_NEW_IMAGES"}</td>
							 	<td>
							 	<div id="files">
									<input class="inputbox" id="my_file_element2" type="file" name="pictures_2" />
										<div id="files_list2"></div>
										{literal}
										<script>
											var max_img_upload2 = {/literal}{$smarty.const.ads_opt_maxnr_images} - {$add->images|@count}{literal};
							        
											if (max_img_upload2 >= 0)
											{
												var multi_selector2 = new MultiSelector( document.getElementById('files_list2'),max_img_upload2 )
												
												multi_selector2.addElement( document.getElementById( 'my_file_element2' ) );
											}
											else {
												alert('You can not upload '+ max_img_upload2 +' pictures');
											}
										</script>
										{/literal}
									 </td>
							 </tr>
				   		{/if}
			  		{/if}
				{else}
                    {if $isGuest == 0}
						<tr>
							<td>{jtext text="ADS_NEW_PICTURES"}</td>
							<td>
								<div id="files">
									<input class="inputbox" id="my_file_element3" type="file" name="pictures_3" />
									<div id="files_list3"></div>
									{literal}
									<script>
									  var max_img_upload3 = {/literal}{$smarty.const.ads_opt_maxnr_images}-{$add->images|@count}{literal};
									  if (max_img_upload3 >= 0) {
										var multi_selector3 = new MultiSelector( document.getElementById('files_list3'),max_img_upload3 )
										multi_selector3.addElement( document.getElementById( 'my_file_element3' ) );
									  } else {
											alert('You can not upload '+ max_img_upload3 +' pictures');
									  }
									  {/literal}
									</script>
								</div>
							</td>
						</tr>
					{/if}
				{/if}
			{else} {* not pay image enbaled*}
				 	<tr>
				 		<td>{jtext text="ADS_NEW_PICTURES"}</td>
					 	<td>
							<div id="files">
								<input class="inputbox" id="my_file_element" type="file" name="pictures_1" />
								<div id="files_list"></div>
								<script>
								var multi_selector = new MultiSelector( document.getElementById('files_list'),{$smarty.const.ads_opt_maxnr_images}-{$add->images|@count} )
								multi_selector.addElement( document.getElementById( 'my_file_element' ) );
								</script>
							</div>
						 </td>
					 </tr>
			{/if}
          {else}
            &nbsp;
          {/if}
		{/if}
		{*/if*}	
        <tr><td colspan="2">&nbsp;</td></tr>
        {if $smarty.const.ads_allow_atachment=="1"}
		 	{if $add->atachment}
		 	<tr>
				<td style="background:#EDEDED;"><a target="_blank" href="{$add->links.download_attachement}">{jtext text="ADS_DOWNLOAD"}</a></td>
				<td style="background:#EDEDED;">
				{if $smarty.const.ads_require_atachment=="1"}
					{jtext text="ADS_REPLACE_ATTACHMENT"}
					<input type="file" name="atachment" id="atachment" />
					{jtext text="ADS_ALLOWED_EXT"} {$smarty.const.ads_allowed_attachments}
				{else}
					<input type="checkbox" name="delete_atachment" value="1" />{jtext text="ADS_DELETE_ATTACHMENT"}
				{/if}
				</td>
		 	</tr>
		 	{else}
		 	<tr>
		 		<td>{jtext text="ADS_ATTACHMENT"}{if $smarty.const.ads_allow_atachment==1 && $smarty.const.ads_require_atachment==1}<span style="color:#FF0000;">*</span>{/if}</td>
		 		<td><input type="file" name="atachment" id="atachment" />{jtext text="ADS_ALLOWED_EXT"} {$smarty.const.ads_allowed_attachments}</td>
		 	</tr>
		 	{/if}
	 	{/if}


        <tr>
            <td width="85">
              <label for="video">{jtext text="ADS_DETAILS_VIDEO"}</label>
            </td>
            <td>
            <div id="video-sources" name="video-sources">
              <ul style="margin: 0px; padding: 0px 0px 0px 10px; list-style-type: none;">
                <li style="margin: 0px; padding: 0px;">
                <a target="_blank" alt="YouTube" title="YouTube" href="http://www.youtube.com/">YouTube</a></li>
                <li><img src="{$ROOT_HOST}components/com_adsman/gallery/images/bullet_grey.png" /></li>
                <li><a target="_blank" alt="MySpace Video" title="MySpace Video" href="http://vids.myspace.com/">MySpace Video</a></li>
                <li><img src="{$ROOT_HOST}components/com_adsman/gallery/images/bullet_grey.png" /></li>
                <li><a target="_blank" alt="Vimeo" title="Vimeo" href="http://www.vimeo.com/">Vimeo</a></li>
                <li><img src="{$ROOT_HOST}components/com_adsman/gallery/images/bullet_grey.png" /></li>
                <li><a target="_blank" alt="Metacafe" title="Metacafe" href="http://www.metacafe.com/">Metacafe</a></li>
                <li><img src="{$ROOT_HOST}components/com_adsman/gallery/images/bullet_grey.png" /></li>
                <li><a target="_blank" alt="Howcast" title="Howcast" href="http://www.howcast.com/">Howcast</a></li>
              </ul>
            </div>
            </td>
        </tr>
        <tr>
            <td style="background:#EDEDED;" colspan="2"><input id="video-link" type="text" size="75" maxlength="255" class="input_border" name="video-link" value="{$video_url}" >
                <input type="checkbox" name="delete_video" value="{$video_id}" />&nbsp;{jtext text="ADS_DELETE_VIDEO"}
            </td>
        </tr>
        <tr><td colspan="2">&nbsp;</td></tr>

        <tr>
            <td valign="top">{jtext text="ADS_TAGS"}</td>
            <td><input class="inputbox" type="text" id="tags" name="tags" value="{$add->tags}" size="70" /></td>
        </tr>


		<tr>
			<td colspan="2">
				
				{* Custom fields will pop here: *}
				<div id="custom_fields_container">
					<table>
					{foreach from=$profiler item=field}
						<tr id="search_{$field.row_id}">
							<td>
							<label for="{$field.field_id}">
							{$field.field_name}
							</label>
							{if $field.compulsory}<span style="color:#FF0000;">*</span>{/if}
									{if $field.help}{infobullet text=$field.help}{/if}
							</td>
							<td>
								{$field.field_html}
								<span class="validate_tip" id="tip_{$field.field_id}"></span>
							</td>
						</tr>
					{/foreach}
					</table>				
				</div>
			</td>
		</tr>


		<tr><td colspan="2">&nbsp;</td></tr>	
		{if $isFeaturedEnabled==1}
		<tr>
			<td width="85">{jtext text="ADS_SELECT_FEATURED"}</td>
			<td>{$lists.credits}</td>
		</tr>
		<tr><td colspan="2">&nbsp;</td></tr>	
		{/if}

        {if !$add->id && $lists.terms}
		<tr>
			<td colspan="2">
				<input type="checkbox" value="1" id="terms" name="terms" checked="checked" /><a class="modal" href="{$add->links.terms}" {literal} rel="{handler: 'iframe', size: {x: 450, y: 300}}" {/literal}>&nbsp;{jtext text="ADS_PAY_AGREE_TERMS_CONDITION"}</a>
		 	</td>
		 </tr>
		{/if}

	</table>
	{if !$userid  && $smarty.const.ads_opt_logged_posting}
	<input type="hidden" name="tk" value="{$add->token_id}" />
	<table class="adds_guest_info">
		<tr>
			<td colspan="2" align="center" class="ads_guest_info_cell">{jtext text="ADS_POSTER_INFO"}:</td>
		</tr>
		<tr>
			<td align="right">
				{jtext text="ADS_USER"} {jtext text="ADS_NAME"}:
			</td>
			<td>
				<input class="inputbox required" type="text" id="guest_username" name="guest_username" value="{$add->guest_username}" />
			</td>
		</tr>
		<tr>
			<td align="right">
				{jtext text="ADS_EMAIL"}:
			</td>
			<td>
				<input class="inputbox required validate-email" type="text" id="guest_email" name="guest_email" value="{$add->guest_email}" />
			</td>
		</tr>
		{if $smarty.const.ads_opt_enable_captcha && !$add->id}
		<tr>
			<td colspan="2" align="center">
				{$cs}
				{jtext text="ADS_CAPTCHA"}
			</td>
		</tr>
		{/if}
		<tr>
			<td colspan="2" align="center" class="ads_guest_info_cell">&nbsp;</td>
		</tr>
	</table>
	{/if}
	<input name="save" id="save_ad2" value="{jtext text='ADS_SAVE'}" class="back_button" type="submit" />&nbsp;&nbsp;
	<a href="{$lists.cancel_form}" class="back_button_cancel"><input name="cancel" value="{jtext text='ADS_CANCEL'}" class="back_button" type="button" /></a>
	{$ads_token}
	<br />
	<!--<span id="credits_info" style="padding:2px; color:#FF0000;"></span>-->
	<input type="hidden" id="credits_info" name="credits_info" value="0">
	
</form>
{php}
{echo JHTML::_('behavior.keepalive');}
{/php}