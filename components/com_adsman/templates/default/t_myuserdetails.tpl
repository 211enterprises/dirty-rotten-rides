{set_css}
{include file='t_javascript_language.tpl'}

<div class="componentheading">{$page_title}</div>

{createtab}
{startpane id="content-pane" usecookies=0}
  {* Profile Tab *}
  {starttab paneid="tab1" text="ADS_MY_PROFILE"}
        <form action="{$ROOT_HOST}index.php" method="post" name="adsForm" id="WV-form" enctype="multipart/form-data" class="form-validate" onsubmit="return myValidate(this);">
        <input type="hidden" name="Itemid" value="{$Itemid}" />
        <input type="hidden" name="option" value="{$option}" />
        <input type="hidden" name="task" value="saveUserDetails" />
        	
       	<div id="invalid_info" style="display:none; border:#FF0000 solid 2px; padding:2px; color:#FF0000;"></div>
        		
       	<table width="100%" class="ads_noborder ads_table userdetailstable"  style="border-collapse: separate !important;" cellpadding="2" cellspacing="7">
        <tr>
            <td width="130"><label for="name">{jtext text="ADS_NAME"}</label></td>
        	<td>
                   <input class="inputbox required" type="text" id="name" name="name" value="{$user->name}" size="30" />
                   <span class="validate_tip" id="tip_name"></span>
        	       <label for="surname">{jtext text="ADS_SURNAME"}</label>
        	   	   <input class="inputbox required" type="text" id="surname" name="surname" value="{$user->surname}" size="30" />
        	       <span class="validate_tip" id="tip_surname"></span>
                   
        	</td>
        </tr>
        <tr><td colspan="2"><hr /></td> </tr>
        <tr>
            <td><label for="address">{jtext text="ADS_ADDRESS"}</label></td>
            <td><textarea id="address" name="address" class="inputbox" rows="3" style="width:230px;"  cols="8">{$user->address}</textarea>
               <label for="city">{jtext text="ADS_CITY"}</label>
                <input class="inputbox" type="text" id="city" name="city" value="{$user->city}" size="30" />
            </td>
        </tr>
        <tr>
            <td><label for="state">{jtext text="ADS_STATE"}</label></td>
            <td><input class="inputbox" type="text" id="state" name="state" value="{$user->state}" size="30" />
                &nbsp;<label for="country">{jtext text="ADS_COUNTRY"}</label>
                {$lists.country}
            </td>
        </tr>
        {foreach from=$profiler item=field}
        <tr>
            <td>
                <label for="{$field.field_id}">
                {$field.field_name}
                </label>
                {if $field.compulsory}<span style="color:#FF0000;">*</span>{/if}
                        {if $field.help}{infobullet text=$field.help}{/if}
            </td>
            <td>
                {$field.field_html}
                <span class="validate_tip" id="tip_{$field.field_id}"></span>
            </td>
        </tr>
        {/foreach}
        
        {if $smarty.const.ads_opt_google_key!=""}
            <tr><td colspan="2"><hr /></td> </tr>
            <tr>
                <td colspan="2">
               	    <div id="map_canvas"></div>    
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <label>{jtext text="ADS_GOOGLE_X_COORDINATE"}:</label> 
                        <input class="inputbox" type="text" id="googleX" name="googleX" value="{$user->googleX}" size="20" />
                    <label>{jtext text="ADS_GOOGLE_Y_COORDINATE"}:</label>
                        <input class="inputbox" type="text" id="googleY" name="googleY" value="{$user->googleY}" size="20" />
                </td>
            </tr>
        {/if}    
        
        <tr><td colspan="2"><hr /></td> </tr>
        <tr>
           <td><label for="phone">{jtext text="ADS_PHONE"}</label></td>
           <td><input class="inputbox" type="text" id="phone" name="phone" value="{$user->phone}" size="50" /></td>
        </tr>
        <tr>
           <td><label for="email">{jtext text="ADS_EMAIL"}</label></td>
           <td>
            <input class="inputbox validate-email" type="text" id="email" name="email" value="{$user->email}" size="50" />
            <span class="validate_tip" id="tip_email"></span>
           </td>
        </tr>
        </table>
        <br />
        <input name="save" value="{jtext text='ADS_SAVE'}" class="back_button" type="submit" />
        <a href="{$lists.cancel_form}" class="back_button_cancel"><input name="cancel" value="{jtext text='ADS_CANCEL'}" class="back_button" type="button" /></a>
  </form>

    {endtab}
    {* Credits Tab *}
    {starttab paneid="tab2" text="ADS_TAB_CREDITS"}
        {if $lists.credits|@count}
            <span class="add_myprofile_balance">{jtext text="ADS_ACCOUNT_BALANCE"}:&nbsp;{$lists.credits[0]->credits_no|default:0}&nbsp;{jtext text="ADS_CREDITS"}</span>
        {/if}
        <br /><hr /><br />
        {if $lists.packages|@count}
			<table width="100%" class="auction_caption_box">
			<tr>
			    <th><span style="color:#993333;">{jtext text="ADS_CREDITS"} {jtext text="ADS_PACKAGES"}</span>
			</tr>
			<tr>
			    <td valign="top">
					<table width="100%">
					<tr>
					{foreach from=$lists.packages item=pac key=key}
						<td align="center" valign="top">
							<table>
								<tr>
								{if $pac->price != 0}
									<td align="center">
										<a class="ads_packages_link" href="index.php?option=com_adsman&task=buy_package&package={$pac->name}&amp;Itemid={$Itemid}" >
											<span style="color:#CC0000;font-weight: bold;">{jtext text="ADS_BUY"}</span>
										</a>
									</td>
								{else}
									<td align="center">
										<a class="ads_packages_link" href="index.php?option=com_adsman&task=free_package&package={$pac->name}&amp;Itemid={$Itemid}" >{jtext text="ADS_GET_FREE_CREDITS"}</a>
									</td>
								{/if}
								</tr>
								<tr>
									<td align="center"><strong>{$pac->name}</strong></td>
								</tr>
								<tr>
									<td align="center">{$pac->price} {$pac->currency}</td>
								</tr>
								<tr>
									<td align="center">{$pac->credits} {jtext text="ADS_CREDITS"}</td>
								</tr>
								{if $currency_maincredit == $pac->currency}
								<tr>
									<td style="text-align:center;">{jtext text="ADS_ECONOMY"}
									{math equation="(( x * y ) - z)" x=$price_maincredit y=$pac->credits z=$pac->price format="%.2f"}
									{$currency_maincredit}
									{jtext text="ADS_ECONOMY_PER_PACKAGE"}
									</td>
								</tr>
                                {else}
                                <tr>
									<td>&nbsp;</td>
								</tr>
								{/if}
								<tr>
									<td height="100" align="center">
										<a href="index.php?option=com_adsman&task=buy_package&package={$pac->name}&amp;Itemid={$Itemid}">
								            {package_image package=$pac->name}
    									</a>
									</td>
								</tr>

							</table>
						</td>
					{/foreach}
					</tr>
					</table>
			    </td>
			</tr>
			</table>
    	{/if}
    {endtab}
{endpane}