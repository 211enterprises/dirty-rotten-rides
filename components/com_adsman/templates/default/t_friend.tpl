{include file='t_javascript_language.tpl'}
{set_css}
<div class="componentheading">{jtext text="ADS_TELL_FRIEND"}</div>
<form id="adsForm" name="adsForm" method="post" id="WV-form" class="form-validate" >
	<input type="hidden" name="Itemid" value="{$Itemid}" />
	<input type="hidden" name="option" value="com_adsman" />
	<input type="hidden" name="view" value="adsman" />
	<input type="hidden" name="task" value="tellFriend" />
	<input type="hidden" name="id" value="{$add->id}" />
	{$ads_token}
    <div class="add_detail_container">
        <div class="add_detail_title">{$add->title}</div>
        <div class="add_text_medium_bold">{$add->short_description}</div>
    </div>
    <div style="padding:20px">
        {jtext text="ADS_TELL_FRIEND_EMAIL"}:
        <input type="text" class="inputbox required validate-email" name="friend_email" size="40" />
    </div>    
	{if !$userid && $smarty.const.ads_opt_enable_captcha}
    	{$cs}
    	{jtext text="ADS_CAPTCHA"}
	{/if}
	<input type="submit" value="{jtext text='ADS_TELL_FRIEND'}" class="back_button" />
</form>