{*
 Displays a certain template position
 custom fields in defined position template 
*}
{if $TPL_DEBUG=="1"}
	{* Previewing & Developing usage *}
<span style="border:solid 1px #FF0000;">
	{if $this_add}
		{$positions.$this_add.$position.title}
	{else}
		{$positions.$position.title}
	{/if}
{/if}
{* Customising can be done *}
{if $this_add}
	{foreach from=$positions.$this_add.$position.fields item=TemplatePostionItem key=TemplatePositionKey}
		{$TemplatePostionItem.html}
	{/foreach}
{else}
	{foreach from=$positions.$position.fields item=TemplatePostionItem key=TemplatePositionKey}
		{$TemplatePostionItem.html}
	{/foreach}
{/if}
{if $TPL_DEBUG=="1"}
	{* Previewing & Developing usage *}
	</span>
{/if}
{* Customising can be done *}
