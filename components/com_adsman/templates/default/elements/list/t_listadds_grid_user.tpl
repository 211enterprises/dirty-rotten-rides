            {if $item->addtype==$smarty.const.ADDSMAN_TYPE_PUBLIC}
	            {*jtext text="ADS_AUTHOR"*}

                {if $smarty.const.ads_opt_logged_posting && !$item->username }
                    {jtext text="ADS_BY"}: <strong>{$item->guest_username}</strong><br />
                {else}
                    {jtext text="ADS_BY"}: <a href="{$item->links.otheradds}"><strong>{$item->username}</strong></a><br />
	            {/if}    
            {else}
                <strong>{jtext text="ADS_ADTYPE_PRIVATE_AD"}</strong><br />
            {/if}
