{createtab}

<input type="hidden" id="to_reply" name="to_reply" value="{if !$add->is_my_add}{$add->userid}{/if}" />
<input type="hidden" id="from" name="from" value="{$userid}" />
<input type="hidden" id="message_textarea_reply" name="message_textarea_reply" value="" />

    {startpane id="content-pane" usecookies=0}
	    {* description Tab *}
       {starttab paneid="tab1" text="ADS_TAB_DESCRIPTION"}
            <table width="100%" class="ads_table">
                <tr>
                    <td width="100%">
                    <b>{*jtext text="ADS_NEW_DESCRIPTION"*}</b>
                    {$add->description}
                    </td>
                </tr>
            </table>
        {endtab}

        {if $video_link != ''}
		
        {* video Tab *}
	    {starttab paneid="tab4" text="ADS_TAB_VIDEO"}
		
            <div class="round_media">
              <table width="100%" class="ads_table" cellspacing="0" cellpadding="0" border="0">
                {if $video_link_source == 'youtube'}
                    <tr>
                      <td class="media_width" rowspan="2">
                        <div class="media_thumbnail">
                          <a title="{$video_title}" onclick="jQueryFactory.prettyPhoto.open('{$video_link}', '', '')" href="javascript:void(0);">
                            <img src="http://{$thumb_source}" alt="YouTube" class="media_width" /><br />
                          </a>
                        </div>
                      </td>
                    </tr>
                 {/if}
                 {if $video_link_source =='vimeo'}
                    <tr>
                      <td class="media_width" rowspan="2">
                        <div class="media_thumbnail">
                          <a title="{$video_title}" onclick="jQueryFactory.prettyPhoto.open('{$video_link}', '', '')" href="javascript:void(0);">
                            <img src="http://{$thumb_source}" rel="prettyPhoto" alt="<?php echo $link->video_sitename; ?>" class="media_width" /><br />
                          </a>
                        </div>
                      </td>
                    </tr>
                  {/if}
                  {if $video_link_source == 'metacafe'}
                    <tr>
                      <td class="media_width" rowspan="2">
                        <div class="media_thumbnail">
                            <a href="{$video_link}?width=70%&amp;height=70%" rel="prettyPhoto" title="{$video_title}">
                            <img  src="{$thumb_source}" class="media_width" alt="{$video_title}"></a>

                        </div>
                      </td>

                    </tr>
                  {/if}
                  {if $video_link_source == 'myspace'}
                    <tr>
                       <td class="media_width" rowspan="2">
                        <div class="media_thumbnail">
                          <a title="{$video_title}"  onclick="jQueryFactory.prettyPhoto.open('{$video_link}', '', '')" href="javascript:void(0);">
                            <img src="{$thumb_source}" alt="{$video_sitename}" class="media_width" /><br />
                          </a>
                        </div>
                      </td>
                    </tr>
                  {/if}
                  {if $video_link_source == 'howcast'}
                    <tr>
                      <td class="media_width" rowspan="2">
                        <div class="media_thumbnail">
                            <a href="{$video_link}?width=500&height=350" rel="prettyPhoto" title="{$video_title}">
                            <img  src="{$video_sourceThumb}" class="media_width" alt="{$video_title}"></a>
                            <br />
                        </div>
                      </td>

                    </tr>
                  {/if}
			  
				    <tr>
                      <td class="align-top">
                        <div class="video_content">
                            <span class="media_title">{$video_title}</span>
                            <div class="video-description">{$video_description}</div>
                        </div>
                      </td>
                    </tr>
				
			  </table>
		    </div>
	    {endtab}
    {/if}

    {if $smarty.const.ads_opt_allow_messaging}
	   {starttab paneid="tab2" text="ADS_TAB_MESSAGES"}
	    {* messages Tab *}
	   <table width="100%">
	   	<tr>
	   		<th width="80">{jtext text="ADS_SEND_MESSAGE_SENDER"}</th>
	   		<th width="80">{jtext text="ADS_SEND_MESSAGE_TO"}</th>
			<th>{jtext text="ADS_SENT_MESSAGE"}</th>
			<th width="140">{jtext text="ADS_DATE"}</th>
	   	</tr>
	   	{foreach from=$add->messages item=mes key=k}
	   	
	   	<tr>
		   	<td>
			<span id="adsusername_{$mes->from_user}">{$mes->fromusername|default:"Guest"}</span>
			
			{if $mes->from_user!=$userid && $add->closed==0 && $mes->from_user!=0}
				<input type="hidden" id="adsreply_to" name="to" value="{if !$add->is_my_add}{$add->userid}{/if}" />
				<input type="hidden" id="from" name="from" value="{$userid}" />
					<a href="javascript:void(false);" onclick="document.getElementById('adsm_mailsend_box').style.display='block'; reply({$mes->from_user}); void(false);">{jtext text="ADS_REPLY"}</a>
			{/if}
			<div class="ads_mailsend_box" id="adsm_mailsend_box">
	
			{if (!$add->is_my_add) && ($add->closed==0) }
			{else}
				<input type="hidden" id="adsreply_to" name="to" value="{if !$add->is_my_add}{$add->userid}{/if}" />
				<input type="hidden" id="to" name="to" value="{if !$add->is_my_add}{$add->userid}{/if}" />   
	    		<input type="hidden" id="from" name="from" value="{$userid}" />
				<input type="hidden" id="message_textarea" name="message_textarea" value="" />
	{/if}
		<table>
			<!--<tr>
				<td><b>{jtext text="ADS_SEND_MESSAGE"}</b></td>
			</tr>-->
			<tr>
				<td>
				{jtext text="ADS_SEND_MESSAGE_TO"}:&nbsp;<strong id="adsreply_username"></strong><br />
				<textarea class="inputbox" id="reply_message_textarea" rows="3" cols="55" {if $userid == 0} disabled="disabled" {/if} ></textarea>
				</td>
			</tr>
			<tr>
				<td><input type="button" name="sendmessagebuton" onclick="document.getElementById('message_textarea').value = document.getElementById('reply_message_textarea').value;
sendMessage('reply');" value="{jtext text='ADS_SEND_MESSAGE_BTN'}" class="back_button" {if $userid == 0} disabled="disabled" {/if} /><input name="cancelButton" type="button" class="back_button" value="{jtext text='ADS_CANCEL'}" onclick="document.getElementById('adsm_mailsend_box').style.display='none';" /></td>
			</tr>
		</table>
	   </div>
			</td>
		   	<td>
			{if $smarty.const.ads_opt_logged_posting && !$mes->tousername }
		   		{$add->guest_username}
		   	{else}
		   		{$mes->tousername}
		   	{/if}
		   	</td>
		   	<td>{$mes->comment|replace:'\r\n':'<br />'|stripslashes}</td>
		   	<td>{$mes->datesend|date_format:$opt_date_format}</td>
		</tr>
		{/foreach}
	   </table>
	   
	  {endtab}

	  {if (!$add->is_my_add) && ($add->closed==0) }
	  
	   {starttab paneid="tab3" text="ADS_TAB_SEND_MESSAGE"}
	  
		{if !($smarty.const.ads_opt_allow_messaging && ( $userid!=0 || $smarty.const.ads_opt_allow_guest_messaging )) }
			<h2>{jtext text="ADS_ERR_LOGIN_TO_SEND_MESSAGE"}</h2>
		{/if}
	    <input type="hidden" id="to" name="to" value="{if !$add->is_my_add}{$add->userid}{/if}" />
		<input type="hidden" id="from" name="from" value="{$userid}" />
		<table>
			<tr>
				<td><b>{jtext text="ADS_SEND_MESSAGE"}</b></td>
			</tr>
			<tr>
				<td>
				{jtext text="ADS_SEND_MESSAGE_TO"}:&nbsp;<strong>
				{if !$add->is_my_add}
				{if $smarty.const.ads_opt_logged_posting && !$user->name }{$add->guest_username}{else}{$user->name}{/if}{/if}</strong>
				<br />
				<textarea class="inputbox" name="message_textarea" id="message_textarea" rows="3" cols="50" {if !(( $userid!=0 || $smarty.const.ads_opt_allow_guest_messaging )) } disabled="disabled" {/if} ></textarea>
				</td>
			</tr>
			{if !$userid && $smarty.const.ads_opt_enable_captcha && $smarty.const.ads_opt_allow_guest_messaging}
			<tr>
				<td>
					<table>
					<tr>
						<td>
							{$cs}
						</td>
						<td>
							{jtext text="ADS_CAPTCHA"}
						</td>
					</tr>
					</table>
				</td>
			</tr>
			{/if}
			<tr>
				<td><input name="sendMessage_button" type="button" onclick="sendMessage('message');" value="{jtext text='ADS_SEND_MESSAGE_BTN'}" class="back_button" {if !(( $userid!=0 || $smarty.const.ads_opt_allow_guest_messaging )) } disabled="disabled" {/if} /></td>
			</tr>
		</table>
		{endtab}
		{/if}
{/if}
{if $pricing_plugins.price_contact->enabled==1 && !$add->is_my_add}
    {starttab paneid="tab5" text="ADS_GOOGLEMAP"}
        {jtext text="ADS_HIDDEN_MAP"}
    {endtab}
{else}
    {if $smarty.const.ads_opt_google_key!="" && $smarty.const.ads_map_in_ads_details=="1"
        && ( ($add->MapX!="" && $add->MapY!="") || ($user->googleX!="" && $user->googleY!="") )}
	   {starttab paneid="tab5" text="ADS_GOOGLEMAP"}
           <div id="map_canvas"></div>
   		{*endtab}
       {/if*}
        {*if $add->MapX!="" && $add->MapY!=""}
        	<iframe src="{$ROOT_HOST}index.php?option=com_adsman&amp;task=googlemap&amp;x={$add->MapX}&amp;y={$add->MapY}&amp;tmpl=component" style="width:{$smarty.const.ads_opt_googlemap_gx+40|default:'260'}px; height:{$smarty.const.ads_opt_googlemap_gy+40|default:'100'}px; border:none;" frameborder="0" class="ads_noborder" cellspacing="0" scrolling="no" ></iframe>
        {elseif $user->googleX!="" && $user->googleY!=""}
        	<iframe src="{$ROOT_HOST}index.php?option=com_adsman&amp;task=googlemap&amp;x={$user->googleX}&amp;y={$user->googleY}&amp;tmpl=component" style="width:{$smarty.const.ads_opt_googlemap_gx+40|default:'260'}px; height:{$smarty.const.ads_opt_googlemap_gy+40|default:'100'}px; border:none;" frameborder="0" class="ads_noborder" cellspacing="0" scrolling="no"></iframe>
        {/if*}
		{endtab}
    {/if}
{/if}
{endpane}

