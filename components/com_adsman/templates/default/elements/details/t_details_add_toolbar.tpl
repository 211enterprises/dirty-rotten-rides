	<span class="ads_right ads_table" style="float:right;">
		{if $add->is_my_add && $add->close_by_admin == 0}
			{if !$add->closed}
			
				<a href="{$add->links.edit}" class="ads_right10">
					<img src="{$IMAGE_ROOT}pencil_48.png" class="ads_noborder" alt="{jtext text='ADS_EDIT'}" title="{jtext text='ADS_EDIT'}" height="30" />
				</a>
				<a href="javascript:history.go(-1)" class="ads_right10">
					<img src="{$IMAGE_ROOT}back.png" class="ads_noborder" width="30" alt="{jtext text='ADS_BACK'}" title="{jtext text='ADS_BACK'}" />
				</a>
				<a href="{$add->links.cancel}" onclick="return confirm('{jtext text='ADS_CONFIRM_CANCEL'}')">
					<img src="{$IMAGE_ROOT}cancel_48.png" class="ads_noborder" alt="{jtext text='ADS_CANCEL'}" title="{jtext text='ADS_CANCEL'}" height="30" />
				</a>
	
			{else}
				<input name="extendbutton" type="button" value="{jtext text='ADS_EXTEND'}"  onclick="window.location = '{$add->links.extend_add}';" class="back_button" />
				<input name="republishbutton" type="button" value="{jtext text='ADS_REPUBLISH'}"  onclick="window.location = '{$add->links.republish}';" class="back_button" />
			{/if}
		{else}
			<a href="javascript:history.go(-1)"><img src="{$IMAGE_ROOT}back.png" class="ads_noborder" width="30" alt="{jtext text='ADS_BACK'}" title="{jtext text='ADS_BACK'}" /></a>
		{/if}
	</span>
