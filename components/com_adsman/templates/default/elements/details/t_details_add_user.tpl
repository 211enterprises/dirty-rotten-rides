    {if $smarty.const.ads_opt_logged_posting &&  ( !$add->userid || $user->has_profile==0) }

		{if $add->addtype==$smarty.const.ADDSMAN_TYPE_PUBLIC}
			{if $user->has_profile==0 && $add->userid}
					<tr>
						<td colspan="4" class="ads_padding15">{jtext text="ADS_NAME"}:&nbsp;{$user->name} {if $add->links.user_profile}({$user->username}) {/if}</td>
					</tr>
			{else}
					<tr>
						<td colspan="4" class="ads_padding15">{jtext text="ADS_NAME"}:&nbsp;{$add->guest_username}</td>
					</tr>
					<tr>
						<td colspan="4"  class="ads_padding15">&nbsp;{$add->guest_email}</td>
					</tr>
			{/if}
		{else}
            {if $add->userid}
                <tr>
                    <td colspan="4" class="ads_padding15">{$user->username}
                        {if $count_user_ads && ($count_user_ads > 0) }<a href="{$add->links.otheradds}">({$count_user_ads})</a>{/if}
                    </td>
                </tr>
            {else}
                {if $add->guest_username != ''}
                        <tr>
                            <td colspan="4"  class="ads_padding15">{jtext text="ADS_NAME"}:&nbsp;{$add->guest_username}</td>
                        </tr>
                {/if}
            {/if}
		{/if}
	{else}
		{if $pricing_plugins.price_contact->enabled==0}
			{if $add->addtype==$smarty.const.ADDSMAN_TYPE_PUBLIC}
                    <tr>
                        <td colspan="4" class="ads_padding15" style="vertical-align: top;"><a href="{$add->links.user_profile}">{*jtext text="ADS_PAGE_VIEW_PROFILE"*} {$user->username}</a>
                            {if $count_user_ads && ($count_user_ads > 0) } <a href="{$add->links.otheradds}">({$count_user_ads})</a> {/if}
                            &nbsp;&nbsp;
                            <a target="_blank" href="{$add->links.user_rss}" style="float: right;">
                                <img src="{$ROOT_HOST}components/com_adsman/img/livemarks.png" class="ads_noborder" alt="RSS Ads Factory" />
                            </a>
                        </td>
            		</tr>

                    <tr>
						<td class="ads_padding15">{jtext text="ADS_NAME"}:&nbsp;{$user->name} {$user->surname}</td>
					</tr>
					<tr>
						<td colspan="4"  class="ads_padding15">{jtext text="ADS_ADDRESS"}:&nbsp;{$user->address}</td>
					</tr>
					<tr>
						<td colspan="4"  class="ads_padding15">{jtext text="ADS_CITY"}:&nbsp;{$user->city}</td>
					</tr>
					<tr>
						<td colspan="4"  class="ads_padding15">{jtext text="ADS_STATE"}:&nbsp;{$user->state}</td>
					</tr>
                    <tr>
    				    <td colspan="4"  class="ads_padding15">{jtext text="ADS_COUNTRY"}:&nbsp;{$user->country}</td>
    			    </tr>

			{else}
                <a href="{$add->links.user_profile}">{*jtext text="ADS_PAGE_VIEW_PROFILE"*} {$user->username}</a>
                {if $add->addtype==$smarty.const.ADDSMAN_TYPE_PRIVATE && $userid != $add->userid}
					<tr>
						<td colspan="4" class="ads_padding15" style="vertical-align: top;"><span class="add_text_medium_bold">{jtext text="ADS_PRIVATE_AD_WARNING"}</span></td>
					</tr>
				{/if}	
			{/if}

		{else}
					<tr>
						<td class="ads_padding15"><a href="{$add->links.user_profile}">{*jtext text="ADS_PAGE_VIEW_PROFILE"*} {$user->username}</a>

                            {if $count_user_ads && ($count_user_ads > 0) }<a href="{$add->links.otheradds}">({$count_user_ads})</a>{/if}
							&nbsp;&nbsp;
							<a target="_blank" href="{$ROOT_HOST}index.php?option=com_adsman&view=adsman&format=feed&user={$user->userid}"  style="float: right;">
								<img src="{$ROOT_HOST}components/com_adsman/img/livemarks.png" class="ads_noborder" alt="RSS Ads Factory" />
							</a>

						</td>
					</tr>
                    <tr>
                        <td class="ads_padding15">{jtext text="ADS_CITY"}:&nbsp;{$user->city}</td>
                    </tr>
                    <tr>
                        <td class="ads_padding15">{jtext text="ADS_COUNTRY"}:&nbsp;{$user->country}</td>
                    </tr>
		{/if}


	{/if}	
