<div class="componentheading">
	{$page_title}
</div>
{set_css}

{startpane usecookies=0}
    {starttab text="ADS_LIST_MAP"}
        {include file="maps/t_adsonmap.tpl"}
    {endtab}
    {starttab text="ADS_RADIUS_SEARCH"}
        {include file="maps/t_search.tpl"}
    {endtab}
{endpane}
