{set_css}
{include file='t_javascript_language.tpl'}
<div class="componentheading">
    <img src="{$ROOT_HOST}components/com_adsman/templates/demo/image/userprofile.png" title="{$page_title}" class="ads_noborder" style="vertical-align:middle"/>
	{jtext text="ADS_PAGE_VIEW_PROFILE"}
</div>
 
	<table width="100%" cellpadding="2" cellspacing="0" class="ads_noborder userdetailstable ads_table">
	  <tr>
	   <td width="160" class="add_field_label">{jtext text="ADS_NAME"}:</td>
	   <td>{$user->name}</td>
	  </tr>
	  <tr>
	   <td width="160" class="add_field_label">{jtext text="ADS_SURNAME"}:</td>
	   <td>{$user->surname}</td>
	  </tr>
	  <tr>
	   <td width="160" class="add_field_label">{jtext text="ADS_ADDRESS"}:</td>
	   <td>{$user->address}</td>
	  </tr>
	  <tr>
	   <td width="160" class="add_field_label">{jtext text="ADS_CITY"}:</td>
	   <td>{$user->city}</td>
	  </tr>
	  <tr>
	   <td width="160" class="add_field_label">{jtext text="ADS_STATE"}:</td>
	   <td>{$user->state}</td>
	  </tr>
	  <tr>
	   <td width="160" class="add_field_label">{jtext text="ADS_COUNTRY"}:</td>
	   <td>{$lists.country}</td>
	  </tr>
	  <tr>
	   <td width="160" class="add_field_label">{jtext text="ADS_PHONE"}:</td>
	   <td>{$user->phone}</td>
	  </tr>
	  <tr>
	   <td width="160" class="add_field_label">{jtext text="ADS_EMAIL"}:</td>
	   <td>{$user->email}</td>
	  </tr>
	  {* @since 1.5.5 *}
	  {include file='elements/display_fields.tpl' position='defuserviewpage'}

    {foreach from=$profiler item=field}
        <tr>
            <td width="160" class="add_field_label">
                <label for="{$field.field_id}">{$field.field_name}</label>
                {if $field.help}{infobullet text=$field.help}{/if}
            </td>
            <td>{$field.value}</td>
        </tr>
    {/foreach}

	  {if $smarty.const.ads_opt_google_key!=""}
    	  <tr>
    	   <td colspan="2"><hr /></td>
    	  </tr>
    	  <tr>
    	   <td width="160" class="add_field_label" style="padding-left:20px;">{jtext text="ADS_GOOGLE_X_COORDINATE"}:</td>
    	   <td>{$user->googleX|default:"--"}</td>
    	  </tr>
    	  <tr>
    	   <td width="160" class="add_field_label" style="padding-left:20px;">{jtext text="ADS_GOOGLE_Y_COORDINATE"}:</td>
    	   <td>{$user->googleY|default:"--"}</td>
    	  </tr>
    	  <tr>
    	   <td colspan="2">&nbsp;</td>
    	  </tr>
    	  <tr>
    	   <td colspan="2">
                <div id="map_canvas"></div>    		
    	   </td>       
    	  </tr>
      {/if}
	</table>
