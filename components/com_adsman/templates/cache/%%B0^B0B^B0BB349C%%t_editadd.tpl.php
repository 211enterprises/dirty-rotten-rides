<?php /* Smarty version 2.6.26, created on 2013-06-10 22:41:19
         compiled from t_editadd.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'set_css', 't_editadd.tpl', 1, false),array('function', 'jtext', 't_editadd.tpl', 21, false),array('function', 'printdate', 't_editadd.tpl', 114, false),array('function', 'get_now', 't_editadd.tpl', 152, false),array('function', 'infobullet', 't_editadd.tpl', 470, false),array('modifier', 'date_format', 't_editadd.tpl', 115, false),array('modifier', 'add_day', 't_editadd.tpl', 152, false),array('modifier', 'count', 't_editadd.tpl', 269, false),)), $this); ?>
<?php echo $this->_plugins['function']['set_css'][0][0]->smarty_set_css(array(), $this);?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 't_javascript_language.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>


<form action="<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
index.php" method="post" name="adsForm" id="adsForm" enctype="multipart/form-data" class="form-validate" onSubmit="return myValidate(this);">
<input type="hidden" name="Itemid" value="<?php echo $this->_tpl_vars['Itemid']; ?>
" />
<input type="hidden" name="option" value="<?php echo $this->_tpl_vars['option']; ?>
" />
<input type="hidden" name="id" value="<?php echo $this->_tpl_vars['add']->id; ?>
" />
<input type="hidden" name="task" value="save" />
<input type="hidden" name="oldid" value="<?php echo $this->_tpl_vars['oldid']; ?>
" />
<input type="hidden" name="oldpic" value="<?php echo $this->_tpl_vars['oldpic']; ?>
" />

<?php if (( $this->_tpl_vars['add']->id && $this->_tpl_vars['add']->featured != 'none' ) || $this->_tpl_vars['listing_enabled']): ?>
    <span style="font-size: 100%;"><?php echo $this->_tpl_vars['edit_24hours_info']; ?>
</span><br />
<?php endif; ?>


<div class="componentheading">
<span style="vertical-align:middle;">
<?php if ($this->_tpl_vars['add']->id): ?>
  <?php echo smarty_function_jtext(array('text' => 'ADS_PAGE_EDIT'), $this);?>

  <?php if ($this->_tpl_vars['add']->status): ?>  ( <?php echo smarty_function_jtext(array('text' => 'ADS_PAGE_PUBLISHED'), $this);?>
 )
  <?php else: ?>
    ( <?php echo smarty_function_jtext(array('text' => 'ADS_PAGE_UNPUBLISHED'), $this);?>
 )
  <?php endif; ?>
<?php else: ?>
  <?php echo smarty_function_jtext(array('text' => 'ADS_PAGE_ADD_NEW_AD'), $this);?>

<?php endif; ?>
</span>

	<!--<input name="save" id="save_ad" value="<?php echo smarty_function_jtext(array('text' => 'ADS_SAVE'), $this);?>
" class="back_button" type="submit" />
	<a href="<?php echo $this->_tpl_vars['lists']['cancel_form']; ?>
" class="back_button_cancel"><input name="cancel" value="<?php echo smarty_function_jtext(array('text' => 'ADS_CANCEL'), $this);?>
" class="back_button" type="button" /></a>-->
</div>
	<div id="invalid_info" style="display:none; border:#FF0000 solid 2px; padding:2px; color:#FF0000;"></div>
	<table style="border-collapse: separate !important;" cellpadding="1" cellspacing="5" >
		<tr>
			<td valign="top"><label for="title"><?php echo smarty_function_jtext(array('text' => 'ADS_TITLE'), $this);?>
<span style="color:#FF0000;">*</span></label></td>
			<td><?php if ($this->_tpl_vars['allow_edit'] == 1): ?>
			    <input class="inputbox required" type="text" id="title" name="title" value="<?php echo $this->_tpl_vars['add']->title; ?>
" size="80" />
                <?php else: ?>
                    <?php echo $this->_tpl_vars['add']->title; ?>

                    <input type="hidden" name="title" value="<?php echo $this->_tpl_vars['add']->title; ?>
" />
                <?php endif; ?>
		  	<span class="validate_tip" id="tip_title"></span>
			</td>
		</tr>
		<tr>
			<td valign="top"><label for="category"><?php echo smarty_function_jtext(array('text' => 'ADS_CATEGORY'), $this);?>
</label></td>
			<td>

                    <?php if ($this->_tpl_vars['lists']['category'] == ""): ?>
                        <span id="category_axj_space"><img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
ajax-loader.gif" /></span>
                    <?php else: ?>
                        <?php echo $this->_tpl_vars['lists']['category']; ?>

                    <?php endif; ?>
                    <?php if (@ads_opt_category_page || $_POST['category'] > 0 || $_POST['cat'] > 0): ?>
                        <?php if ($this->_tpl_vars['add']->id): ?>
                            <a href="<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
index.php?option=com_adsman&task=selectcat&id=<?php echo $this->_tpl_vars['add']->id; ?>
"><?php echo smarty_function_jtext(array('text' => 'ADS_CHANGE_CATEGORY'), $this);?>
</a>
                        <?php else: ?>
                            <a href="<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
index.php?option=com_adsman&task=selectcat"><?php echo smarty_function_jtext(array('text' => 'ADS_CHANGE_CATEGORY'), $this);?>
</a>
                        <?php endif; ?>
                    <?php endif; ?>

				<?php if ($this->_tpl_vars['time_limited'] == 0 && $this->_tpl_vars['listing_enabled'] == 1): ?>
					<span id="category_price_container" style="padding:2px; color:#FF0000;">
						<?php echo smarty_function_jtext(array('text' => 'ADS_PRICE_LISTING_IMAGES'), $this);?>
 <?php echo $this->_tpl_vars['credits_category_ini']; ?>
 <?php echo smarty_function_jtext(array('text' => 'ADS_CREDITS'), $this);?>

					</span>
				<?php endif; ?>
			</td>
		</tr>
		<?php if (@ads_opt_adtype_enable == 0 && @ads_opt_adtype_val != ''): ?>
			<?php echo $this->_tpl_vars['lists']['addtype']; ?>

		<?php else: ?>
		<tr>
			<td valign="top"><?php echo smarty_function_jtext(array('text' => 'ADS_AD_TYPE'), $this);?>
</td>
			<td><?php echo $this->_tpl_vars['lists']['addtype']; ?>
&nbsp;<?php echo smarty_function_jtext(array('text' => 'ADS_HELP_PRIVATE'), $this);?>
</td>
		</tr>
		<?php endif; ?>
		<?php if (@ads_opt_adpublish_enable == 0 && @ads_opt_adpublish_val != ''): ?>
			<?php echo $this->_tpl_vars['lists']['status']; ?>

		<?php else: ?>
	</table>
	<table class="ads_noborder ads_table" style="border-collapse: separate !important;" cellpadding="1" cellspacing="5">
        <?php if ($this->_tpl_vars['listing_enabled'] == 1): ?>
            <?php echo $this->_tpl_vars['lists']['status']; ?>

        <?php else: ?>
            <tr>
                <td valign="top"><?php echo smarty_function_jtext(array('text' => 'ADS_PUBLISHED'), $this);?>
</td>
                <td><?php echo $this->_tpl_vars['lists']['status']; ?>
</td>
            </tr>
        <?php endif; ?>

		<?php endif; ?>
		<tr>
			<td valign="top"><label for="short_description"><?php echo smarty_function_jtext(array('text' => 'ADS_SHORT_DESCRIPTION'), $this);?>
</label></td>
			<td><textarea id="short_description" name="short_description" style="width:350px;" class="inputbox"><?php echo $this->_tpl_vars['add']->short_description; ?>
</textarea>(<span id='descr_counter'>0</span>/<?php echo @ads_short_desc_long; ?>
 chars)</td>
		</tr>


		<?php if (@ads_opt_price_enable == 1): ?>
		<tr>
			<td valign="top"><label for="askprice"><?php echo smarty_function_jtext(array('text' => 'ADS_ASKPRICE'), $this);?>
<?php if (@ads_price_compulsory == 1): ?><span style="color:#FF0000;">*</span><?php endif; ?></label></td>
			<td><input class="inputbox <?php if (@ads_price_compulsory == 1): ?> required <?php endif; ?> validate-numeric" type="text" id="askprice" name="askprice" value="<?php echo $this->_tpl_vars['add']->askprice; ?>
" size="10" style="text-align: right !important;" /> <?php echo $this->_tpl_vars['lists']['currency']; ?>
</td>
		</tr>
		<?php endif; ?>
		
				
		<?php if ($this->_tpl_vars['time_limited'] == 0): ?>
		<tr>
			<td valign="top"><label for="start_date"><?php echo smarty_function_jtext(array('text' => 'ADS_START_DATE'), $this);?>
</label><span style="color:#FF0000;">*</span></td>
			<td>
				<?php if ($this->_tpl_vars['add']->id): ?>
					<?php echo $this->_plugins['function']['printdate'][0][0]->smarty_printdate(array('date' => $this->_tpl_vars['add']->start_date), $this);?>

					<input class="text_area" type="hidden" name="start_date" id="start_date" size="15" maxlength="19" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['add']->start_date)) ? $this->_run_mod_handler('date_format', true, $_tmp, $this->_tpl_vars['opt_date_format']) : smarty_modifier_date_format($_tmp, $this->_tpl_vars['opt_date_format'])); ?>
" alt="start_date"/>
				<?php else: ?>
                    <?php echo $this->_tpl_vars['lists']['start_date_calendar']; ?>

				<?php endif; ?>
			</td>
		</tr>
		<tr>
			<td valign="top"><label for="end_date"><?php echo smarty_function_jtext(array('text' => 'ADS_END_DATE'), $this);?>
</label><span style="color:#FF0000;">*</span></td>
			<td>
				<?php if ($this->_tpl_vars['add']->id): ?>
					<?php echo $this->_plugins['function']['printdate'][0][0]->smarty_printdate(array('date' => $this->_tpl_vars['add']->end_date,'use_hour' => 1), $this);?>

					<input class="text_area" type="hidden" name="end_date" id="end_date" size="15" maxlength="19" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['add']->end_date)) ? $this->_run_mod_handler('date_format', true, $_tmp, $this->_tpl_vars['opt_date_format']) : smarty_modifier_date_format($_tmp, $this->_tpl_vars['opt_date_format'])); ?>
" alt="end_date"/>
				<?php else: ?>

					<?php echo $this->_tpl_vars['lists']['end_date_calendar']; ?>

					
					<?php if (@ads_opt_enable_hour): ?>
						<input name="end_hour" size="1" value="00" alt="" class="inputbox" /> :
						<input name="end_minutes" size="1" value="00" alt="" class="inputbox" />
					<?php endif; ?>
				<?php endif; ?>
			</td>
	 	</tr>
	 	<?php else: ?>
	 	<tr>
	 		<td valign="top"><label for="end_date"><?php echo smarty_function_jtext(array('text' => 'ADS_AVAILABILITY'), $this);?>
</label></td>
	 		<td valign="top">
                 <input class="text_area" readonly type="hidden" name="start_date" id="start_date" size="15" maxlength="19" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lists']['valab_now'])) ? $this->_run_mod_handler('date_format', true, $_tmp, $this->_tpl_vars['opt_date_format']) : smarty_modifier_date_format($_tmp, $this->_tpl_vars['opt_date_format'])); ?>
" alt="start_date"/>
                 <input class="text_area" type="hidden" name="end_date" id="end_date" size="15" maxlength="19" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['lists']['valab_end'])) ? $this->_run_mod_handler('date_format', true, $_tmp, $this->_tpl_vars['opt_date_format']) : smarty_modifier_date_format($_tmp, $this->_tpl_vars['opt_date_format'])); ?>
" alt="end_date"/>

                <?php if ($this->_tpl_vars['add']->id): ?>

                    <select id="ad_valability" name="ad_valability" onchange="document.getElementById('end_date').value=VList[this.value]; showValabilityPrice(VPriceList[this.value]);">
						<?php $_from = $this->_tpl_vars['valabs_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
                            <?php if ($this->_tpl_vars['add']->status): ?>
                                                                <?php if ($this->_tpl_vars['item'] == $this->_tpl_vars['lists']['db_no_of_days']): ?>
                                    <option value="<?php echo $this->_tpl_vars['key']; ?>
" selected="selected" title="<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_plugins['function']['get_now'][0][0]->smarty_get_now(array(), $this))) ? $this->_run_mod_handler('add_day', true, $_tmp, $this->_tpl_vars['item']) : $this->_plugins['modifier']['add_day'][0][0]->smarty_add_day($_tmp, $this->_tpl_vars['item'])))) ? $this->_run_mod_handler('date_format', true, $_tmp, $this->_tpl_vars['opt_date_format']) : smarty_modifier_date_format($_tmp, $this->_tpl_vars['opt_date_format']));?>
"><?php echo $this->_tpl_vars['item']; ?>
 days</option>
                                <?php endif; ?>
                            <?php else: ?>
                                                                <?php if ($this->_tpl_vars['item'] == $this->_tpl_vars['lists']['db_no_of_days']): ?>
                                    <option value="<?php echo $this->_tpl_vars['key']; ?>
" selected="selected" title="<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_plugins['function']['get_now'][0][0]->smarty_get_now(array(), $this))) ? $this->_run_mod_handler('add_day', true, $_tmp, $this->_tpl_vars['item']) : $this->_plugins['modifier']['add_day'][0][0]->smarty_add_day($_tmp, $this->_tpl_vars['item'])))) ? $this->_run_mod_handler('date_format', true, $_tmp, $this->_tpl_vars['opt_date_format']) : smarty_modifier_date_format($_tmp, $this->_tpl_vars['opt_date_format']));?>
"><?php echo $this->_tpl_vars['item']; ?>
 days</option>
                                <?php else: ?>
                                 <!--   <option value="<?php echo $this->_tpl_vars['key']; ?>
" title="<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_plugins['function']['get_now'][0][0]->smarty_get_now(array(), $this))) ? $this->_run_mod_handler('add_day', true, $_tmp, $this->_tpl_vars['item']) : $this->_plugins['modifier']['add_day'][0][0]->smarty_add_day($_tmp, $this->_tpl_vars['item'])))) ? $this->_run_mod_handler('date_format', true, $_tmp, $this->_tpl_vars['opt_date_format']) : smarty_modifier_date_format($_tmp, $this->_tpl_vars['opt_date_format']));?>
"><?php echo $this->_tpl_vars['item']; ?>
 days</option>-->
                                <?php endif; ?>
                            <?php endif; ?>
						<?php endforeach; endif; unset($_from); ?>
					</select>
                    <!--<span id="pricing_info" style="padding:2px; color:#FF0000;"></span>-->
                    <?php echo $this->_plugins['function']['printdate'][0][0]->smarty_printdate(array('date' => $this->_tpl_vars['add']->start_date), $this);?>
 - <?php echo $this->_plugins['function']['printdate'][0][0]->smarty_printdate(array('date' => $this->_tpl_vars['add']->end_date,'use_hour' => 1), $this);?>

                    
                <?php else: ?>
	 										
					<select id="ad_valability" name="ad_valability" onchange="document.getElementById('end_date').value=VList[this.value]; showValabilityPrice(VPriceList[this.value]);">
						<?php $_from = $this->_tpl_vars['valabs_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
							<option value="<?php echo $this->_tpl_vars['key']; ?>
" title="<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_plugins['function']['get_now'][0][0]->smarty_get_now(array(), $this))) ? $this->_run_mod_handler('add_day', true, $_tmp, $this->_tpl_vars['item']) : $this->_plugins['modifier']['add_day'][0][0]->smarty_add_day($_tmp, $this->_tpl_vars['item'])))) ? $this->_run_mod_handler('date_format', true, $_tmp, $this->_tpl_vars['opt_date_format']) : smarty_modifier_date_format($_tmp, $this->_tpl_vars['opt_date_format']));?>
"><?php echo $this->_tpl_vars['item']; ?>
 days</option>
						<?php endforeach; endif; unset($_from); ?>
					</select>
					<span id="pricing_info" style="padding:2px; color:#FF0000;"><?php echo smarty_function_jtext(array('text' => 'ADS_PRICE_LISTING_VALABILITY'), $this);?>
 <?php echo $this->_tpl_vars['valabs_prices'][$this->_tpl_vars['valabs_default_val']]; ?>
 <?php echo $this->_tpl_vars['valabs_currency']; ?>
</span>
                <?php endif; ?>
	 		</td>
	 	</tr>
	 	<?php endif; ?>
		
        <tr>
			<td valign="top"><?php echo smarty_function_jtext(array('text' => 'ADS_NEW_DESCRIPTION'), $this);?>
<span style="color:#FF0000;">*</span></td>
			<td class="required"><?php echo $this->_tpl_vars['lists']['description']; ?>
</td>
		</tr>

        <?php if (@ads_opt_google_key != ""): ?>
            <tr><td colspan="2">&nbsp;</td></tr>
            <tr>
                <td colspan="2">
                    <?php if ($this->_tpl_vars['user']->googleX != "" && $this->_tpl_vars['user']->googleY != ""): ?>
                          <a href="#" onclick="gmap_move_marker(<?php echo '{'; ?>
pointTox:<?php echo $this->_tpl_vars['user']->googleX; ?>
,pointToy:<?php echo $this->_tpl_vars['user']->googleY; ?>
<?php echo '}'; ?>
); return false;"><?php echo smarty_function_jtext(array('text' => 'ADS_KEEP_PROFILE_COORDINATES'), $this);?>
</a> 
                    <?php endif; ?>
                    <div id="map_canvas"></div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <label for="MapX"><?php echo smarty_function_jtext(array('text' => 'ADS_GOOGLE_X_COORDINATE'), $this);?>
:</label>
                    <input class="inputbox" type="text" id="googleX" name="MapX" value="<?php echo $this->_tpl_vars['add']->MapX; ?>
" size="30" />
                    <label for="MapY"><?php echo smarty_function_jtext(array('text' => 'ADS_GOOGLE_Y_COORDINATE'), $this);?>
:</label>
                    <input class="inputbox" type="text" id="googleY" name="MapY" value="<?php echo $this->_tpl_vars['add']->MapY; ?>
" size="30"/>
                </td>
            </tr>
            <tr><td colspan="2">&nbsp;</td></tr>
		<?php endif; ?>

        <tr>
			<td valign="top"><?php echo smarty_function_jtext(array('text' => 'ADS_AD_CITY'), $this);?>
</td>
			<td><input class="inputbox" type="text" id="ad_city" name="ad_city" value="<?php echo $this->_tpl_vars['add']->ad_city; ?>
" size="20" /></td>
		</tr>
        <tr>
			<td valign="top"><?php echo smarty_function_jtext(array('text' => 'ADS_AD_POSTCODE'), $this);?>
</td>
			<td><input class="inputbox validate" type="text" id="ad_postcode" name="ad_postcode" value="<?php echo $this->_tpl_vars['add']->ad_postcode; ?>
" size="20" /></td>
		</tr>

	 	<?php if ($this->_tpl_vars['add']->picture): ?>
		 	<tr>
		 		<td colspan="2"><?php echo smarty_function_jtext(array('text' => 'ADS_MAIN_PICTURE'), $this);?>
</td>
		 	</tr>
		 	<tr>
		 		<td>&nbsp;</td>    		
				<td style="background:#EDEDED;">
	    			<table>
	    			  <tr>
						<td>
                            <img src="<?php echo $this->_tpl_vars['ADSMAN_PICTURES']; ?>
/resize_<?php echo $this->_tpl_vars['add']->picture; ?>
" />
                            <!--<img src="<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
components/com_adsman/images/resize_<?php echo $this->_tpl_vars['add']->picture; ?>
">-->
                            </td>
						<td><?php if ($this->_tpl_vars['allow_edit'] == 1): ?>
                            <input type="checkbox" id="delete_main_picture" name="delete_main_picture" value="1" />&nbsp;<?php echo smarty_function_jtext(array('text' => 'ADS_DELETE_PICTURE'), $this);?>

                            <?php else: ?>
                                &nbsp;
                            <?php endif; ?>
                        </td>
					  </tr>
					</table>	
    			</td>
		 	</tr>
	 	<?php else: ?>
          <?php if ($this->_tpl_vars['allow_edit'] == 1): ?>
	 		<?php if ($this->_tpl_vars['pay_img_enabled'] == 0 || $this->_tpl_vars['main_img_free']): ?>
			 	<tr>
			 		<td><?php echo smarty_function_jtext(array('text' => 'ADS_MAIN_PICTURE'), $this);?>
</td>
			 		<td><input type="file" name="picture" id="picture" /></td>
			 	</tr>
			 <?php else: ?>
				 				 <?php if ($this->_tpl_vars['pay_img_enabled'] == 1 && $this->_tpl_vars['isGuest'] == 0): ?>
					<tr>
				 		<td><?php echo smarty_function_jtext(array('text' => 'ADS_ADD_MAIN_PICTURE'), $this);?>
<?php if (@ads_opt_require_picture == 1): ?><span style="color:#FF0000;">*</span><?php endif; ?></td>
				 		<td>
				 		<?php if (@ads_opt_require_picture == 1): ?>
				 			<input type="file" name="picture" id="picture" />
				 		<?php else: ?>
							<input type="file" name="picture" id="picture" />
				 							 		<?php endif; ?>
				 		<span id="pricing_main_img_info" style="padding:2px; color:#FF0000;"><?php echo smarty_function_jtext(array('text' => 'ADS_PRICE_MAIN_IMAGE'), $this);?>
 <?php echo $this->_tpl_vars['credits_main']; ?>
 <?php echo smarty_function_jtext(array('text' => 'ADS_CREDITS'), $this);?>
</span>
				 		</td>
				 	</tr>
				 <?php endif; ?>		
	 		<?php endif; ?>
          <?php else: ?>
            &nbsp;
          <?php endif; ?>
	 	<?php endif; ?>
	 	
	 	<?php if (count($this->_tpl_vars['add']->images)): ?>
    	 	<tr>
    	 		<td colspan="2"><?php echo smarty_function_jtext(array('text' => 'ADS_NEW_PICTURES'), $this);?>
</td>
    	 	</tr>
    	 	<?php unset($this->_sections['image']);
$this->_sections['image']['name'] = 'image';
$this->_sections['image']['loop'] = is_array($_loop=$this->_tpl_vars['add']->images) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['image']['show'] = true;
$this->_sections['image']['max'] = $this->_sections['image']['loop'];
$this->_sections['image']['step'] = 1;
$this->_sections['image']['start'] = $this->_sections['image']['step'] > 0 ? 0 : $this->_sections['image']['loop']-1;
if ($this->_sections['image']['show']) {
    $this->_sections['image']['total'] = $this->_sections['image']['loop'];
    if ($this->_sections['image']['total'] == 0)
        $this->_sections['image']['show'] = false;
} else
    $this->_sections['image']['total'] = 0;
if ($this->_sections['image']['show']):

            for ($this->_sections['image']['index'] = $this->_sections['image']['start'], $this->_sections['image']['iteration'] = 1;
                 $this->_sections['image']['iteration'] <= $this->_sections['image']['total'];
                 $this->_sections['image']['index'] += $this->_sections['image']['step'], $this->_sections['image']['iteration']++):
$this->_sections['image']['rownum'] = $this->_sections['image']['iteration'];
$this->_sections['image']['index_prev'] = $this->_sections['image']['index'] - $this->_sections['image']['step'];
$this->_sections['image']['index_next'] = $this->_sections['image']['index'] + $this->_sections['image']['step'];
$this->_sections['image']['first']      = ($this->_sections['image']['iteration'] == 1);
$this->_sections['image']['last']       = ($this->_sections['image']['iteration'] == $this->_sections['image']['total']);
?>
    		<tr>
				<td>&nbsp;</td>    		
				<td style="background:#EDEDED;">
	    			<table>
	    				<tr>
	    					<td>
                                <img src="<?php echo $this->_tpl_vars['ADSMAN_PICTURES']; ?>
/resize_<?php echo $this->_tpl_vars['add']->images[$this->_sections['image']['index']]->picture; ?>
" />
                                <!--<img src="<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
components/com_adsman/images/resize_<?php echo $this->_tpl_vars['add']->images[$this->_sections['image']['index']]->picture; ?>
" />-->
                            </td>
	    					<td><?php if ($this->_tpl_vars['allow_edit'] == 1): ?>
                                    <input type="checkbox" name="delete_pictures[]" value="<?php echo $this->_tpl_vars['add']->images[$this->_sections['image']['index']]->id; ?>
" />&nbsp;<?php echo smarty_function_jtext(array('text' => 'ADS_DELETE_PICTURE'), $this);?>

                                <?php else: ?>
                                    &nbsp;
                                <?php endif; ?>
                            </td>
	    				</tr>
	    			</table>	
    			</td>
    		</tr>
    		<?php endfor; endif; ?>
		<?php endif; ?>

		<?php if (count($this->_tpl_vars['add']->images) < @ads_opt_maxnr_images): ?>
          <?php if ($this->_tpl_vars['allow_edit'] == 1): ?>
			<?php if ($this->_tpl_vars['pay_img_enabled'] == 1): ?>
		  
		    	<?php if ($this->_tpl_vars['images_upload_free_no'] >= 1): ?>
		  
					<?php if ($this->_tpl_vars['images_upload_free_no'] > count($this->_tpl_vars['add']->images) && $this->_tpl_vars['images_upload_free_no'] - count($this->_tpl_vars['add']->images) <= @ads_opt_maxnr_images): ?>
						<tr>
				 			<td><?php echo smarty_function_jtext(array('text' => 'ADS_NEW_IMAGES'), $this);?>
</td>
					 		<td>
					 			<div id="files">
								<input class="inputbox" id="my_file_element1" type="file" name="pictures_1" />
								<div id="files_list1"></div>
								<?php echo '
								<script>
									var max_img_upload = '; ?>
<?php echo @ads_opt_maxnr_images; ?>
 - <?php echo count($this->_tpl_vars['add']->images); ?>
<?php echo ';
									var images_upload_free_no = '; ?>
<?php echo $this->_tpl_vars['images_upload_free_no']; ?>
 - <?php echo count($this->_tpl_vars['add']->images); ?>
<?php echo ';


									if (max_img_upload >= 0 && images_upload_free_no <= max_img_upload)
									{
										var multi_selector1 = new MultiSelector( document.getElementById(\'files_list1\'),max_img_upload )
                                        //var multi_selector1 = new MultiSelector( document.getElementById(\'files_list1\'),images_upload_free_no )
										multi_selector1.addElement( document.getElementById( \'my_file_element1\' ) );
									}
									else {
										alert(\'You can not upload \'+ images_upload_free_no +\' pictures\');
									}
								</script>
								'; ?>

							</td>
					 	</tr>
				 	<?php else: ?>
				   		<?php if ($this->_tpl_vars['images_upload_free_no'] > count($this->_tpl_vars['add']->images) && @ads_opt_maxnr_images <= $this->_tpl_vars['images_upload_free_no']): ?>
						 	<tr>
						 		<td><?php echo smarty_function_jtext(array('text' => 'ADS_NEW_IMAGES'), $this);?>
</td>
							 	<td>
							 	<div id="files">
									<input class="inputbox" id="my_file_element2" type="file" name="pictures_2" />
										<div id="files_list2"></div>
										<?php echo '
										<script>
											var max_img_upload2 = '; ?>
<?php echo @ads_opt_maxnr_images; ?>
 - <?php echo count($this->_tpl_vars['add']->images); ?>
<?php echo ';
							        
											if (max_img_upload2 >= 0)
											{
												var multi_selector2 = new MultiSelector( document.getElementById(\'files_list2\'),max_img_upload2 )
												
												multi_selector2.addElement( document.getElementById( \'my_file_element2\' ) );
											}
											else {
												alert(\'You can not upload \'+ max_img_upload2 +\' pictures\');
											}
										</script>
										'; ?>

									 </td>
							 </tr>
				   		<?php endif; ?>
			  		<?php endif; ?>
				<?php else: ?>
                    <?php if ($this->_tpl_vars['isGuest'] == 0): ?>
						<tr>
							<td><?php echo smarty_function_jtext(array('text' => 'ADS_NEW_PICTURES'), $this);?>
</td>
							<td>
								<div id="files">
									<input class="inputbox" id="my_file_element3" type="file" name="pictures_3" />
									<div id="files_list3"></div>
									<?php echo '
									<script>
									  var max_img_upload3 = '; ?>
<?php echo @ads_opt_maxnr_images; ?>
-<?php echo count($this->_tpl_vars['add']->images); ?>
<?php echo ';
									  if (max_img_upload3 >= 0) {
										var multi_selector3 = new MultiSelector( document.getElementById(\'files_list3\'),max_img_upload3 )
										multi_selector3.addElement( document.getElementById( \'my_file_element3\' ) );
									  } else {
											alert(\'You can not upload \'+ max_img_upload3 +\' pictures\');
									  }
									  '; ?>

									</script>
								</div>
							</td>
						</tr>
					<?php endif; ?>
				<?php endif; ?>
			<?php else: ?> 				 	<tr>
				 		<td><?php echo smarty_function_jtext(array('text' => 'ADS_NEW_PICTURES'), $this);?>
</td>
					 	<td>
							<div id="files">
								<input class="inputbox" id="my_file_element" type="file" name="pictures_1" />
								<div id="files_list"></div>
								<script>
								var multi_selector = new MultiSelector( document.getElementById('files_list'),<?php echo @ads_opt_maxnr_images; ?>
-<?php echo count($this->_tpl_vars['add']->images); ?>
 )
								multi_selector.addElement( document.getElementById( 'my_file_element' ) );
								</script>
							</div>
						 </td>
					 </tr>
			<?php endif; ?>
          <?php else: ?>
            &nbsp;
          <?php endif; ?>
		<?php endif; ?>
			
        <tr><td colspan="2">&nbsp;</td></tr>
        <?php if (@ads_allow_atachment == '1'): ?>
		 	<?php if ($this->_tpl_vars['add']->atachment): ?>
		 	<tr>
				<td style="background:#EDEDED;"><a target="_blank" href="<?php echo $this->_tpl_vars['add']->links['download_attachement']; ?>
"><?php echo smarty_function_jtext(array('text' => 'ADS_DOWNLOAD'), $this);?>
</a></td>
				<td style="background:#EDEDED;">
				<?php if (@ads_require_atachment == '1'): ?>
					<?php echo smarty_function_jtext(array('text' => 'ADS_REPLACE_ATTACHMENT'), $this);?>

					<input type="file" name="atachment" id="atachment" />
					<?php echo smarty_function_jtext(array('text' => 'ADS_ALLOWED_EXT'), $this);?>
 <?php echo @ads_allowed_attachments; ?>

				<?php else: ?>
					<input type="checkbox" name="delete_atachment" value="1" /><?php echo smarty_function_jtext(array('text' => 'ADS_DELETE_ATTACHMENT'), $this);?>

				<?php endif; ?>
				</td>
		 	</tr>
		 	<?php else: ?>
		 	<tr>
		 		<td><?php echo smarty_function_jtext(array('text' => 'ADS_ATTACHMENT'), $this);?>
<?php if (@ads_allow_atachment == 1 && @ads_require_atachment == 1): ?><span style="color:#FF0000;">*</span><?php endif; ?></td>
		 		<td><input type="file" name="atachment" id="atachment" /><?php echo smarty_function_jtext(array('text' => 'ADS_ALLOWED_EXT'), $this);?>
 <?php echo @ads_allowed_attachments; ?>
</td>
		 	</tr>
		 	<?php endif; ?>
	 	<?php endif; ?>


        <tr>
            <td width="85">
              <label for="video"><?php echo smarty_function_jtext(array('text' => 'ADS_DETAILS_VIDEO'), $this);?>
</label>
            </td>
            <td>
            <div id="video-sources" name="video-sources">
              <ul style="margin: 0px; padding: 0px 0px 0px 10px; list-style-type: none;">
                <li style="margin: 0px; padding: 0px;">
                <a target="_blank" alt="YouTube" title="YouTube" href="http://www.youtube.com/">YouTube</a></li>
                <li><img src="<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
components/com_adsman/gallery/images/bullet_grey.png" /></li>
                <li><a target="_blank" alt="MySpace Video" title="MySpace Video" href="http://vids.myspace.com/">MySpace Video</a></li>
                <li><img src="<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
components/com_adsman/gallery/images/bullet_grey.png" /></li>
                <li><a target="_blank" alt="Vimeo" title="Vimeo" href="http://www.vimeo.com/">Vimeo</a></li>
                <li><img src="<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
components/com_adsman/gallery/images/bullet_grey.png" /></li>
                <li><a target="_blank" alt="Metacafe" title="Metacafe" href="http://www.metacafe.com/">Metacafe</a></li>
                <li><img src="<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
components/com_adsman/gallery/images/bullet_grey.png" /></li>
                <li><a target="_blank" alt="Howcast" title="Howcast" href="http://www.howcast.com/">Howcast</a></li>
              </ul>
            </div>
            </td>
        </tr>
        <tr>
            <td style="background:#EDEDED;" colspan="2"><input id="video-link" type="text" size="75" maxlength="255" class="input_border" name="video-link" value="<?php echo $this->_tpl_vars['video_url']; ?>
" >
                <input type="checkbox" name="delete_video" value="<?php echo $this->_tpl_vars['video_id']; ?>
" />&nbsp;<?php echo smarty_function_jtext(array('text' => 'ADS_DELETE_VIDEO'), $this);?>

            </td>
        </tr>
        <tr><td colspan="2">&nbsp;</td></tr>

        <tr>
            <td valign="top"><?php echo smarty_function_jtext(array('text' => 'ADS_TAGS'), $this);?>
</td>
            <td><input class="inputbox" type="text" id="tags" name="tags" value="<?php echo $this->_tpl_vars['add']->tags; ?>
" size="70" /></td>
        </tr>


		<tr>
			<td colspan="2">
				
								<div id="custom_fields_container">
					<table>
					<?php $_from = $this->_tpl_vars['profiler']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['field']):
?>
						<tr id="search_<?php echo $this->_tpl_vars['field']['row_id']; ?>
">
							<td>
							<label for="<?php echo $this->_tpl_vars['field']['field_id']; ?>
">
							<?php echo $this->_tpl_vars['field']['field_name']; ?>

							</label>
							<?php if ($this->_tpl_vars['field']['compulsory']): ?><span style="color:#FF0000;">*</span><?php endif; ?>
									<?php if ($this->_tpl_vars['field']['help']): ?><?php echo $this->_plugins['function']['infobullet'][0][0]->smarty_infobullet(array('text' => $this->_tpl_vars['field']['help']), $this);?>
<?php endif; ?>
							</td>
							<td>
								<?php echo $this->_tpl_vars['field']['field_html']; ?>

								<span class="validate_tip" id="tip_<?php echo $this->_tpl_vars['field']['field_id']; ?>
"></span>
							</td>
						</tr>
					<?php endforeach; endif; unset($_from); ?>
					</table>				
				</div>
			</td>
		</tr>


		<tr><td colspan="2">&nbsp;</td></tr>	
		<?php if ($this->_tpl_vars['isFeaturedEnabled'] == 1): ?>
		<tr>
			<td width="85"><?php echo smarty_function_jtext(array('text' => 'ADS_SELECT_FEATURED'), $this);?>
</td>
			<td><?php echo $this->_tpl_vars['lists']['credits']; ?>
</td>
		</tr>
		<tr><td colspan="2">&nbsp;</td></tr>	
		<?php endif; ?>

        <?php if (! $this->_tpl_vars['add']->id && $this->_tpl_vars['lists']['terms']): ?>
		<tr>
			<td colspan="2">
				<input type="checkbox" value="1" id="terms" name="terms" checked="checked" /><a class="modal" href="<?php echo $this->_tpl_vars['add']->links['terms']; ?>
" <?php echo ' rel="{handler: \'iframe\', size: {x: 450, y: 300}}" '; ?>
>&nbsp;<?php echo smarty_function_jtext(array('text' => 'ADS_PAY_AGREE_TERMS_CONDITION'), $this);?>
</a>
		 	</td>
		 </tr>
		<?php endif; ?>

	</table>
	<?php if (! $this->_tpl_vars['userid'] && @ads_opt_logged_posting): ?>
	<input type="hidden" name="tk" value="<?php echo $this->_tpl_vars['add']->token_id; ?>
" />
	<table class="adds_guest_info">
		<tr>
			<td colspan="2" align="center" class="ads_guest_info_cell"><?php echo smarty_function_jtext(array('text' => 'ADS_POSTER_INFO'), $this);?>
:</td>
		</tr>
		<tr>
			<td align="right">
				<?php echo smarty_function_jtext(array('text' => 'ADS_USER'), $this);?>
 <?php echo smarty_function_jtext(array('text' => 'ADS_NAME'), $this);?>
:
			</td>
			<td>
				<input class="inputbox required" type="text" id="guest_username" name="guest_username" value="<?php echo $this->_tpl_vars['add']->guest_username; ?>
" />
			</td>
		</tr>
		<tr>
			<td align="right">
				<?php echo smarty_function_jtext(array('text' => 'ADS_EMAIL'), $this);?>
:
			</td>
			<td>
				<input class="inputbox required validate-email" type="text" id="guest_email" name="guest_email" value="<?php echo $this->_tpl_vars['add']->guest_email; ?>
" />
			</td>
		</tr>
		<?php if (@ads_opt_enable_captcha && ! $this->_tpl_vars['add']->id): ?>
		<tr>
			<td colspan="2" align="center">
				<?php echo $this->_tpl_vars['cs']; ?>

				<?php echo smarty_function_jtext(array('text' => 'ADS_CAPTCHA'), $this);?>

			</td>
		</tr>
		<?php endif; ?>
		<tr>
			<td colspan="2" align="center" class="ads_guest_info_cell">&nbsp;</td>
		</tr>
	</table>
	<?php endif; ?>
	<input name="save" id="save_ad2" value="<?php echo smarty_function_jtext(array('text' => 'ADS_SAVE'), $this);?>
" class="back_button" type="submit" />&nbsp;&nbsp;
	<a href="<?php echo $this->_tpl_vars['lists']['cancel_form']; ?>
" class="back_button_cancel"><input name="cancel" value="<?php echo smarty_function_jtext(array('text' => 'ADS_CANCEL'), $this);?>
" class="back_button" type="button" /></a>
	<?php echo $this->_tpl_vars['ads_token']; ?>

	<br />
	<!--<span id="credits_info" style="padding:2px; color:#FF0000;"></span>-->
	<input type="hidden" id="credits_info" name="credits_info" value="0">
	
</form>
<?php 
{echo JHTML::_('behavior.keepalive');}
 ?>