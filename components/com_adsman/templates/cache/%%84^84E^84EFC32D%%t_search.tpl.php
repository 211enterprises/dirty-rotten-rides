<?php /* Smarty version 2.6.26, created on 2013-06-10 22:01:08
         compiled from t_search.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'set_css', 't_search.tpl', 1, false),array('function', 'set_js', 't_search.tpl', 2, false),array('function', 'jtext', 't_search.tpl', 11, false),)), $this); ?>
<?php echo $this->_plugins['function']['set_css'][0][0]->smarty_set_css(array(), $this);?>

<?php echo $this->_plugins['function']['set_js'][0][0]->smarty_set_js(array(), $this);?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 't_javascript_language.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<form action="<?php echo $this->_tpl_vars['action']; ?>
" method="POST" name="adsForm">
<input type="hidden" name="Itemid" value="<?php echo $this->_tpl_vars['Itemid']; ?>
" />
<input type="hidden" name="option" value="<?php echo $this->_tpl_vars['option']; ?>
" />
<input type="hidden" name="tag" value="" />

<div class="componentheading">
	<span style="float:left;"><img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
search.png" title="<?php echo smarty_function_jtext(array('text' => 'ADS_PAGE_SEARCH'), $this);?>
" alt="<?php echo smarty_function_jtext(array('text' => 'ADS_PAGE_SEARCH'), $this);?>
" height="32" />&nbsp;</span>
	<span style="vertical-align:middle;"><?php echo smarty_function_jtext(array('text' => 'ADS_PAGE_SEARCH'), $this);?>
</span>
	&nbsp;&nbsp;
	<span style="vertical-align:middle;">	
 		<input name="save" value="<?php echo smarty_function_jtext(array('text' => 'ADS_SEARCH'), $this);?>
" class="back_button" type="submit" />
 		<a href="<?php echo $this->_tpl_vars['lists']['cancel_form']; ?>
" class="back_button_cancel"><input name="cancel" value="<?php echo smarty_function_jtext(array('text' => 'ADS_CANCEL'), $this);?>
" class="back_button" type="button" /></a>
 	</span>	
</div>

<table border="0" width="100%" cellpadding="2" cellspacing="8" class="add_search_form ads_table">
	<tr>
		<td><?php echo smarty_function_jtext(array('text' => 'ADS_SEARCH_KEYWORD'), $this);?>
</td>
		<td colspan="3"><input type="text" name="keyword" class="inputbox" size="55" /></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="3" align="left" style="padding-bottom:20px;">
			<input name="in_description" type="checkbox" value="1" />&nbsp;<?php echo smarty_function_jtext(array('text' => 'ADS_SEARCH_IN_DESCRIPTION'), $this);?>
<br />
		</td>
	</tr>
	<tr>
		<td><?php echo smarty_function_jtext(array('text' => 'ADS_CATEGORY'), $this);?>
</td>
		<td colspan="3">
		<?php if ($this->_tpl_vars['lists']['category'] == ""): ?>
			<span id="category_axj_space"><img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
ajax-loader.gif" /></span>
		<?php else: ?>
			<?php echo $this->_tpl_vars['lists']['category']; ?>

		<?php endif; ?>
		</td>
	</tr>
	<tr>
		<td><?php echo smarty_function_jtext(array('text' => 'ADS_AD_TYPE'), $this);?>
</td>
		<td colspan="3"><?php echo $this->_tpl_vars['lists']['addtype']; ?>
</td>
	</tr>
    <tr><td colspan="4">&nbsp;</td></tr>
    <tr>
      <td><?php echo smarty_function_jtext(array('text' => 'ADS_SEARCH_MIN_PRICE'), $this);?>
</td>
      <td colspan="3"><?php echo $this->_tpl_vars['lists']['price_min']; ?>
 &nbsp;&nbsp; - &nbsp;&nbsp; <?php echo smarty_function_jtext(array('text' => 'ADS_SEARCH_MAX_PRICE'), $this);?>
 &nbsp;&nbsp; <?php echo $this->_tpl_vars['lists']['price_max']; ?>
  &nbsp;&nbsp; <?php echo $this->_tpl_vars['lists']['currency']; ?>
</td>
    </tr>
	<tr>
	  <td><?php echo smarty_function_jtext(array('text' => 'ADS_SEARCH_START_DATE'), $this);?>
</td>
 	  <td><?php echo $this->_tpl_vars['lists']['start_date_calendar']; ?>
</td>
	  <td style="padding-left: 5px;"><?php echo smarty_function_jtext(array('text' => 'ADS_SEARCH_END_DATE'), $this);?>
</td>
	  <td><?php echo $this->_tpl_vars['lists']['end_date_calendar']; ?>
</td>
 	</tr>
    <tr><td colspan="4">&nbsp;</td></tr>
    <tr>
        <td class="add_search_label"><?php echo smarty_function_jtext(array('text' => 'ADS_AD_CITY'), $this);?>
</td>
        <td><?php echo $this->_tpl_vars['lists']['ad_city']; ?>
</td>
        <td class="add_search_label" style="padding-left: 5px;"><?php echo smarty_function_jtext(array('text' => 'ADS_AD_POSTCODE'), $this);?>
</td>
        <td><?php echo $this->_tpl_vars['lists']['ad_postcode']; ?>
</td>
    </tr>
    <tr><td colspan="4">&nbsp;</td></tr>
 	<tr>
		<td><?php echo smarty_function_jtext(array('text' => 'ADS_SEARCH_USERS'), $this);?>
</td>
		<td><?php echo $this->_tpl_vars['lists']['users']; ?>
</td>
		<td style="padding-left: 5px;"><?php echo smarty_function_jtext(array('text' => 'ADS_COUNTRY'), $this);?>
</td>
		<td><?php echo $this->_tpl_vars['lists']['country']; ?>
</td>
	</tr>
	<tr>
		<td><?php echo smarty_function_jtext(array('text' => 'ADS_CITY'), $this);?>
</td>	
		<td><?php echo $this->_tpl_vars['lists']['city']; ?>
</td>
		<td style="padding-left: 5px;"><?php echo smarty_function_jtext(array('text' => 'ADS_STATE'), $this);?>
</td>
		<td><?php echo $this->_tpl_vars['lists']['state']; ?>
</td>
	</tr>
    <tr><td colspan="4">&nbsp;</td></tr>


	
</table>
	<div id="custom_fields_container"></div>
</form>