<?php /* Smarty version 2.6.26, created on 2013-07-11 15:03:23
         compiled from t_catlist.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'set_css', 't_catlist.tpl', 2, false),array('function', 'set_js', 't_catlist.tpl', 3, false),array('function', 'jtext', 't_catlist.tpl', 12, false),array('modifier', 'chr', 't_catlist.tpl', 6, false),array('modifier', 'count', 't_catlist.tpl', 35, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 't_javascript_language.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php echo $this->_plugins['function']['set_css'][0][0]->smarty_set_css(array(), $this);?>

<?php echo $this->_plugins['function']['set_js'][0][0]->smarty_set_js(array(), $this);?>

<div align="center" class="ads_letterlist">
    <?php unset($this->_sections['letters']);
$this->_sections['letters']['name'] = 'letters';
$this->_sections['letters']['start'] = (int)65;
$this->_sections['letters']['loop'] = is_array($_loop=91) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['letters']['show'] = true;
$this->_sections['letters']['max'] = $this->_sections['letters']['loop'];
$this->_sections['letters']['step'] = 1;
if ($this->_sections['letters']['start'] < 0)
    $this->_sections['letters']['start'] = max($this->_sections['letters']['step'] > 0 ? 0 : -1, $this->_sections['letters']['loop'] + $this->_sections['letters']['start']);
else
    $this->_sections['letters']['start'] = min($this->_sections['letters']['start'], $this->_sections['letters']['step'] > 0 ? $this->_sections['letters']['loop'] : $this->_sections['letters']['loop']-1);
if ($this->_sections['letters']['show']) {
    $this->_sections['letters']['total'] = min(ceil(($this->_sections['letters']['step'] > 0 ? $this->_sections['letters']['loop'] - $this->_sections['letters']['start'] : $this->_sections['letters']['start']+1)/abs($this->_sections['letters']['step'])), $this->_sections['letters']['max']);
    if ($this->_sections['letters']['total'] == 0)
        $this->_sections['letters']['show'] = false;
} else
    $this->_sections['letters']['total'] = 0;
if ($this->_sections['letters']['show']):

            for ($this->_sections['letters']['index'] = $this->_sections['letters']['start'], $this->_sections['letters']['iteration'] = 1;
                 $this->_sections['letters']['iteration'] <= $this->_sections['letters']['total'];
                 $this->_sections['letters']['index'] += $this->_sections['letters']['step'], $this->_sections['letters']['iteration']++):
$this->_sections['letters']['rownum'] = $this->_sections['letters']['iteration'];
$this->_sections['letters']['index_prev'] = $this->_sections['letters']['index'] - $this->_sections['letters']['step'];
$this->_sections['letters']['index_next'] = $this->_sections['letters']['index'] + $this->_sections['letters']['step'];
$this->_sections['letters']['first']      = ($this->_sections['letters']['iteration'] == 1);
$this->_sections['letters']['last']       = ($this->_sections['letters']['iteration'] == $this->_sections['letters']['total']);
?>
        <a href="<?php echo $this->_tpl_vars['HOST_ROOT']; ?>
index.php?option=com_adsman&view=adsman&task=listcats&startletter=<?php echo ((is_array($_tmp=$this->_sections['letters']['index'])) ? $this->_run_mod_handler('chr', true, $_tmp) : chr($_tmp)); ?>
">
             <?php echo ((is_array($_tmp=$this->_sections['letters']['index'])) ? $this->_run_mod_handler('chr', true, $_tmp) : chr($_tmp)); ?>

        </a>
        &nbsp;
    <?php endfor; endif; ?>
    <a href="<?php echo $this->_tpl_vars['HOST_ROOT']; ?>
index.php?option=com_adsman&view=adsman&task=listcats">
         <?php echo smarty_function_jtext(array('text' => 'ADS_ALL'), $this);?>

    </a>
</div><br />
<div class="adds_man_list">
	<span><?php echo $this->_tpl_vars['category_pathway']; ?>
</span>
	<?php if ($this->_tpl_vars['current_category']->catname != ""): ?>
	  <div style="border:2px solid #000;">
            <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'elements/display_fields.tpl', 'smarty_include_vars' => array('position' => 'categoryicon','this_add' => $this->_tpl_vars['current_category']->id)));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

            <strong><?php echo $this->_tpl_vars['current_category']->catname; ?>
</strong>
                <a href="<?php echo $this->_tpl_vars['current_category']->view; ?>
" title="<?php echo smarty_function_jtext(array('text' => 'ADS_VIEW_LISTINGS'), $this);?>
">
                    <img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
view_listings.gif" class="ads_noborder" alt="<?php echo smarty_function_jtext(array('text' => 'ADS_VIEW_LISTINGS'), $this);?>
" /></a>
                (<?php echo $this->_tpl_vars['current_category']->nr_ads_current; ?>
 <?php echo smarty_function_jtext(array('text' => 'ADS_ADS'), $this);?>
)

            <div class="adds_subcat" style="text-align:right;" >
            <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'elements/display_fields.tpl', 'smarty_include_vars' => array('position' => 'categorydetails','this_add' => $this->_tpl_vars['current_category']->id)));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
            <span style="font-size:12px;">
                <?php echo smarty_function_jtext(array('text' => 'ADS_SUBCATEGORIES'), $this);?>
:  <?php echo $this->_tpl_vars['current_category']->kids; ?>
 &nbsp;|&nbsp; <?php echo smarty_function_jtext(array('text' => 'ADS_TOTAL_ADS'), $this);?>
: <?php echo $this->_tpl_vars['current_category']->nr_ads; ?>

            </span>
	  </div>
    <?php endif; ?>
</div>

<?php if (count($this->_tpl_vars['categories']) <= 0): ?>
    <h2><?php echo smarty_function_jtext(array('text' => 'ADS_NO_CATEGORIES_DEFINED'), $this);?>
</h2>
<?php endif; ?>
<table id="adds_categories" class="ads_table">
  <?php unset($this->_sections['category']);
$this->_sections['category']['name'] = 'category';
$this->_sections['category']['loop'] = is_array($_loop=$this->_tpl_vars['categories']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['category']['show'] = true;
$this->_sections['category']['max'] = $this->_sections['category']['loop'];
$this->_sections['category']['step'] = 1;
$this->_sections['category']['start'] = $this->_sections['category']['step'] > 0 ? 0 : $this->_sections['category']['loop']-1;
if ($this->_sections['category']['show']) {
    $this->_sections['category']['total'] = $this->_sections['category']['loop'];
    if ($this->_sections['category']['total'] == 0)
        $this->_sections['category']['show'] = false;
} else
    $this->_sections['category']['total'] = 0;
if ($this->_sections['category']['show']):

            for ($this->_sections['category']['index'] = $this->_sections['category']['start'], $this->_sections['category']['iteration'] = 1;
                 $this->_sections['category']['iteration'] <= $this->_sections['category']['total'];
                 $this->_sections['category']['index'] += $this->_sections['category']['step'], $this->_sections['category']['iteration']++):
$this->_sections['category']['rownum'] = $this->_sections['category']['iteration'];
$this->_sections['category']['index_prev'] = $this->_sections['category']['index'] - $this->_sections['category']['step'];
$this->_sections['category']['index_next'] = $this->_sections['category']['index'] + $this->_sections['category']['step'];
$this->_sections['category']['first']      = ($this->_sections['category']['iteration'] == 1);
$this->_sections['category']['last']       = ($this->_sections['category']['iteration'] == $this->_sections['category']['total']);
?>
	<?php if ((1 & $this->_sections['category']['rownum'])): ?>
		<tr>
	<?php endif; ?>
			<td width="50%" class="adsman_catcell" valign="top">
				<div class="adds_maincat" <?php if ($this->_tpl_vars['categories'][$this->_sections['category']['index']]->watchListed_flag): ?>class="cat_watchlist"<?php endif; ?>> 
					
					<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'elements/display_fields.tpl', 'smarty_include_vars' => array('position' => 'categoryicon','this_add' => $this->_tpl_vars['categories'][$this->_sections['category']['index']]->id)));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
					<a href="<?php if ($this->_tpl_vars['categories'][$this->_sections['category']['index']]->kids > 0): ?><?php echo $this->_tpl_vars['categories'][$this->_sections['category']['index']]->link; ?>
<?php else: ?><?php echo $this->_tpl_vars['categories'][$this->_sections['category']['index']]->view; ?>
<?php endif; ?>" >
						<?php echo $this->_tpl_vars['categories'][$this->_sections['category']['index']]->catname; ?>

					</a>
					<?php if ($this->_tpl_vars['categories'][$this->_sections['category']['index']]->is_new): ?>
						<!--NEW!!-->
						<img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
new.png" alt="new ads" />
					<?php endif; ?>
					<a href="<?php echo $this->_tpl_vars['categories'][$this->_sections['category']['index']]->view; ?>
" title="<?php echo smarty_function_jtext(array('text' => 'ADS_VIEW_LISTINGS'), $this);?>
">
						<img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
view_listings.gif" class="ads_noborder" alt="<?php echo smarty_function_jtext(array('text' => 'ADS_VIEW_LISTINGS'), $this);?>
" />
					</a>					
					<?php if ($this->_tpl_vars['is_logged_in']): ?>
						<?php if ($this->_tpl_vars['categories'][$this->_sections['category']['index']]->watchListed_flag): ?>
							<img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
watchlist.png" width="16"class="ads_noborder" />
						<?php endif; ?>
						<a href="<?php echo $this->_tpl_vars['categories'][$this->_sections['category']['index']]->link_watchlist; ?>
">
							<img src="<?php echo $this->_tpl_vars['categories'][$this->_sections['category']['index']]->img_src_watchlist; ?>
" style="vertical-align: bottom;" width="16" class="ads_noborder" title="<?php if ($this->_tpl_vars['categories'][$this->_sections['category']['index']]->watchListed_flag): ?><?php echo smarty_function_jtext(array('text' => 'ADS_REMOVE_FROM_WATCHLIST'), $this);?>
<?php else: ?><?php echo smarty_function_jtext(array('text' => 'ADS_ADD_TO_WATCHLIST'), $this);?>
<?php endif; ?>"/>
						</a>
					<?php endif; ?>
					                    	(<?php echo $this->_tpl_vars['categories'][$this->_sections['category']['index']]->nr_a; ?>
 <?php echo smarty_function_jtext(array('text' => 'ADS_ADS'), $this);?>
 )
										<br />
				</div>
				<div class="adds_subcat">
				<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'elements/display_fields.tpl', 'smarty_include_vars' => array('position' => 'categorydetails','this_add' => $this->_tpl_vars['categories'][$this->_sections['category']['index']]->id)));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
				<br />
                       <?php echo smarty_function_jtext(array('text' => 'ADS_TOTAL_ADS'), $this);?>
: <?php echo $this->_tpl_vars['categories'][$this->_sections['category']['index']]->total_nr_ads; ?>
                 <br />
				<span style="font-size:12px;"><?php echo smarty_function_jtext(array('text' => 'ADS_SUBCATEGORIES'), $this);?>
:  <?php echo $this->_tpl_vars['categories'][$this->_sections['category']['index']]->kids; ?>
 </span>
				<br />

				<?php unset($this->_sections['subcategory']);
$this->_sections['subcategory']['name'] = 'subcategory';
$this->_sections['subcategory']['loop'] = is_array($_loop=$this->_tpl_vars['categories'][$this->_sections['category']['index']]->subcategories) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['subcategory']['show'] = true;
$this->_sections['subcategory']['max'] = $this->_sections['subcategory']['loop'];
$this->_sections['subcategory']['step'] = 1;
$this->_sections['subcategory']['start'] = $this->_sections['subcategory']['step'] > 0 ? 0 : $this->_sections['subcategory']['loop']-1;
if ($this->_sections['subcategory']['show']) {
    $this->_sections['subcategory']['total'] = $this->_sections['subcategory']['loop'];
    if ($this->_sections['subcategory']['total'] == 0)
        $this->_sections['subcategory']['show'] = false;
} else
    $this->_sections['subcategory']['total'] = 0;
if ($this->_sections['subcategory']['show']):

            for ($this->_sections['subcategory']['index'] = $this->_sections['subcategory']['start'], $this->_sections['subcategory']['iteration'] = 1;
                 $this->_sections['subcategory']['iteration'] <= $this->_sections['subcategory']['total'];
                 $this->_sections['subcategory']['index'] += $this->_sections['subcategory']['step'], $this->_sections['subcategory']['iteration']++):
$this->_sections['subcategory']['rownum'] = $this->_sections['subcategory']['iteration'];
$this->_sections['subcategory']['index_prev'] = $this->_sections['subcategory']['index'] - $this->_sections['subcategory']['step'];
$this->_sections['subcategory']['index_next'] = $this->_sections['subcategory']['index'] + $this->_sections['subcategory']['step'];
$this->_sections['subcategory']['first']      = ($this->_sections['subcategory']['iteration'] == 1);
$this->_sections['subcategory']['last']       = ($this->_sections['subcategory']['iteration'] == $this->_sections['subcategory']['total']);
?>
				
					<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'elements/display_fields.tpl', 'smarty_include_vars' => array('position' => 'categoryicon','this_add' => $this->_tpl_vars['categories'][$this->_sections['category']['index']]->subcategories[$this->_sections['subcategory']['index']]->id)));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
					<a href="<?php if ($this->_tpl_vars['categories'][$this->_sections['category']['index']]->subcategories[$this->_sections['subcategory']['index']]->kids > 0): ?><?php echo $this->_tpl_vars['categories'][$this->_sections['category']['index']]->subcategories[$this->_sections['subcategory']['index']]->link; ?>
<?php else: ?><?php echo $this->_tpl_vars['categories'][$this->_sections['category']['index']]->subcategories[$this->_sections['subcategory']['index']]->view; ?>
<?php endif; ?>"><?php echo $this->_tpl_vars['categories'][$this->_sections['category']['index']]->subcategories[$this->_sections['subcategory']['index']]->catname; ?>
</a> 
					<?php if ($this->_tpl_vars['categories'][$this->_sections['category']['index']]->subcategories[$this->_sections['subcategory']['index']]->is_new == 1): ?>
						<img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
new.png" alt="new ads" />
				 	 <?php endif; ?>
											(<?php echo $this->_tpl_vars['categories'][$this->_sections['category']['index']]->subcategories[$this->_sections['subcategory']['index']]->nr_a; ?>
 <?php echo smarty_function_jtext(array('text' => 'ADS_ADS'), $this);?>
)
										<?php if ($this->_tpl_vars['categories'][$this->_sections['category']['index']]->subcategories[$this->_sections['subcategory']['index']]->watchListed_flag): ?>
						<img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
watchlist.png" width="16" class="ads_noborder" />
					<?php endif; ?>
					<?php if ($this->_tpl_vars['is_logged_in']): ?>
					<a href="<?php echo $this->_tpl_vars['categories'][$this->_sections['category']['index']]->subcategories[$this->_sections['subcategory']['index']]->link_watchlist; ?>
">
						<img src="<?php echo $this->_tpl_vars['categories'][$this->_sections['category']['index']]->subcategories[$this->_sections['subcategory']['index']]->img_src_watchlist; ?>
" width="14" class="ads_noborder" title="<?php if ($this->_tpl_vars['categories'][$this->_sections['category']['index']]->subcategories[$this->_sections['subcategory']['index']]->watchListed_flag): ?><?php echo smarty_function_jtext(array('text' => 'ADS_REMOVE_FROM_WATCHLIST'), $this);?>
<?php else: ?><?php echo smarty_function_jtext(array('text' => 'ADS_ADD_TO_WATCHLIST'), $this);?>
<?php endif; ?>"/>
					</a>
					<?php endif; ?>
					<br />
					<p style="margin-left:25px;">
					<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'elements/display_fields.tpl', 'smarty_include_vars' => array('position' => 'categorydetails','this_add' => $this->_tpl_vars['categories'][$this->_sections['category']['index']]->id)));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
					</p>
				<?php endfor; endif; ?>
				</div>
			</td>
		<?php if (!((1 & $this->_sections['category']['rownum']))): ?>
		</tr>
		<?php endif; ?>
  <?php endfor; endif; ?>
</table>
</div>
