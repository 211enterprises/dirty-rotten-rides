<?php /* Smarty version 2.6.26, created on 2013-06-12 05:55:53
         compiled from t_friend.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'set_css', 't_friend.tpl', 2, false),array('function', 'jtext', 't_friend.tpl', 3, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 't_javascript_language.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php echo $this->_plugins['function']['set_css'][0][0]->smarty_set_css(array(), $this);?>

<div class="componentheading"><?php echo smarty_function_jtext(array('text' => 'ADS_TELL_FRIEND'), $this);?>
</div>
<form id="adsForm" name="adsForm" method="post" id="WV-form" class="form-validate" >
	<input type="hidden" name="Itemid" value="<?php echo $this->_tpl_vars['Itemid']; ?>
" />
	<input type="hidden" name="option" value="com_adsman" />
	<input type="hidden" name="view" value="adsman" />
	<input type="hidden" name="task" value="tellFriend" />
	<input type="hidden" name="id" value="<?php echo $this->_tpl_vars['add']->id; ?>
" />
	<?php echo $this->_tpl_vars['ads_token']; ?>

    <div class="add_detail_container">
        <div class="add_detail_title"><?php echo $this->_tpl_vars['add']->title; ?>
</div>
        <div class="add_text_medium_bold"><?php echo $this->_tpl_vars['add']->short_description; ?>
</div>
    </div>
    <div style="padding:20px">
        <?php echo smarty_function_jtext(array('text' => 'ADS_TELL_FRIEND_EMAIL'), $this);?>
:
        <input type="text" class="inputbox required validate-email" name="friend_email" size="40" />
    </div>    
	<?php if (! $this->_tpl_vars['userid'] && @ads_opt_enable_captcha): ?>
    	<?php echo $this->_tpl_vars['cs']; ?>

    	<?php echo smarty_function_jtext(array('text' => 'ADS_CAPTCHA'), $this);?>

	<?php endif; ?>
	<input type="submit" value="<?php echo smarty_function_jtext(array('text' => 'ADS_TELL_FRIEND'), $this);?>
" class="back_button" />
</form>