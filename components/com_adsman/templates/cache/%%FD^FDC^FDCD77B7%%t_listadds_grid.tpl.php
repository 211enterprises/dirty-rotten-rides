<?php /* Smarty version 2.6.26, created on 2013-06-10 22:22:28
         compiled from t_listadds_grid.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 't_listadds_grid.tpl', 4, false),array('modifier', 'cat', 't_listadds_grid.tpl', 40, false),array('modifier', 'truncate', 't_listadds_grid.tpl', 60, false),array('function', 'set_css', 't_listadds_grid.tpl', 5, false),array('function', 'set_js', 't_listadds_grid.tpl', 6, false),array('function', 'jtext', 't_listadds_grid.tpl', 9, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 't_javascript_language.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $this->assign('nr_columns', '4'); ?>
<?php $this->assign('col_width', '25'); ?>
<?php $this->assign('nr_items', count($this->_tpl_vars['items'])); ?>
<?php echo $this->_plugins['function']['set_css'][0][0]->smarty_set_css(array(), $this);?>

<?php echo $this->_plugins['function']['set_js'][0][0]->smarty_set_js(array(), $this);?>


<!--<div class="componentheading">
  <?php if ($this->_tpl_vars['task'] == 'myadds'): ?><?php echo smarty_function_jtext(array('text' => 'ADS_PAGE_MY_ADS'), $this);?>
<?php else: ?><?php echo smarty_function_jtext(array('text' => 'ADS_PAGE_LISTADDS'), $this);?>
<?php endif; ?>
</div>-->
<form action="<?php echo $this->_tpl_vars['action']; ?>
" method="post" name="adsForm" id="adsForm">
	<input type="hidden" name="Itemid" value="<?php echo $this->_tpl_vars['Itemid']; ?>
" />
	<input type="hidden" name="option" value="<?php echo $this->_tpl_vars['option']; ?>
" />
	<input type="hidden" name="task" value="<?php echo $this->_tpl_vars['task']; ?>
" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $this->_tpl_vars['filter_order_Dir']; ?>
" />
	<input type="hidden" name="filter_order" value="<?php echo $this->_tpl_vars['filter_order']; ?>
" />
    <input type="hidden" name="list_view" value="<?php echo $this->_tpl_vars['list_view']; ?>
" />
    <input type="hidden" name="archive" value="<?php echo $this->_tpl_vars['archive']; ?>
" />

	<?php $_from = $this->_tpl_vars['filters']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
	    <input type="hidden" name="<?php echo $this->_tpl_vars['key']; ?>
" value="<?php echo $this->_tpl_vars['item']; ?>
" />
	<?php endforeach; endif; unset($_from); ?>

	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "t_listadds_header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
</form>

<span class="adds_man_list ">
<?php if ($this->_tpl_vars['link_new_listing']): ?>
        <a href = "<?php echo $this->_tpl_vars['link_new_listing']; ?>
"><img src = "<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
new_listing.png" border = "0"
           alt = "<?php echo smarty_function_jtext(array('text' => 'COM_ADS_NEW_LISTING_IN_CATEGORY'), $this);?>
"
           title = "<?php echo smarty_function_jtext(array('text' => 'COM_ADS_NEW_LISTING_IN_CATEGORY'), $this);?>
" /></a>
<?php endif; ?>
</span>

<div class="adds_man_list_grid">
<table width="100%" cellpadding="0" cellspacing="0" class="ads_noborder ads_table">

<?php $_from = $this->_tpl_vars['items']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['item_loop'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['item_loop']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
        $this->_foreach['item_loop']['iteration']++;
?>
    <?php if ($this->_tpl_vars['item']->featured && $this->_tpl_vars['item']->featured != 'none'): ?>
        <?php $this->assign('class_featured', ((is_array($_tmp="listing-")) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['item']->featured) : smarty_modifier_cat($_tmp, $this->_tpl_vars['item']->featured))); ?>
    <?php else: ?>
        <?php $this->assign('class_featured', ""); ?>
    <?php endif; ?>

  <?php if (($this->_foreach['item_loop']['iteration']-1) % $this->_tpl_vars['nr_columns'] == 0): ?>
    <tr>
  <?php endif; ?>
        <td align="left" valign="top" width="<?php echo $this->_tpl_vars['col_width']; ?>
%" class="adds_man_item_grid">
            <div style="width: 180px;">
             <div style="float: left; padding-top: 0px;">
                 <div class="adds_man_item_thumb">
                    <a href="index.php?option=<?php echo $this->_tpl_vars['option']; ?>
&task=details&view=adsman&id=<?php echo $this->_tpl_vars['item']->id; ?>
"><?php echo $this->_tpl_vars['item']->thumbnail; ?>
</a>
                 </div>
                 <div class="adds_man_item_grid_container">
                    <div class="adds_man_item_grid_title">
                       <a href="index.php?option=<?php echo $this->_tpl_vars['option']; ?>
&task=details&view=adsman&id=<?php echo $this->_tpl_vars['item']->id; ?>
"><?php echo $this->_tpl_vars['item']->title; ?>
</a>
                       <span style="line-height: 13px;">
                           <div style="max-height:105px">
                               <?php if (@ads_short_desc_long != ""): ?>
                                   <?php echo ((is_array($_tmp=$this->_tpl_vars['item']->short_description)) ? $this->_run_mod_handler('truncate', true, $_tmp, @ads_short_desc_long) : smarty_modifier_truncate($_tmp, @ads_short_desc_long)); ?>

                               <?php else: ?>
                                   <?php echo $this->_tpl_vars['item']->short_description; ?>

                                <?php endif; ?>
                           </div>
                       </span>
                       <div style="position:absolute;bottom:0;margin-top:2px;">
                            <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "elements/list/t_listadds_grid_user.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                            <!--<span class="add_text_small">:</span>-->
                            <a href='<?php echo $this->_tpl_vars['item']->links['filter_cat']; ?>
' class="category_link"><?php echo $this->_tpl_vars['item']->catname; ?>
</a>
                       </div>
                    </div>

                    <div class="adds_man_item_small_box <?php echo $this->_tpl_vars['class_featured']; ?>
">

                       <?php if (! $this->_tpl_vars['item']->is_my_add): ?>
                            <?php if ($this->_tpl_vars['item']->favorite == 0): ?>
                                <span id='add_to_favorite'>
                                <a href='<?php echo $this->_tpl_vars['item']->links['add_to_favorite']; ?>
'><img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
f_favoritelist_1.png" title="<?php echo smarty_function_jtext(array('text' => 'ADS_ADD_TO_FAVORITE'), $this);?>
" alt="<?php echo smarty_function_jtext(array('text' => 'ADS_ADD_TO_FAVORITE'), $this);?>
" height="16" class="ads_noborder" /></a>
                                </span>
                            <?php elseif ($this->_tpl_vars['item']->favorite == 1): ?>
                                <span id='add_to_favorite'><a href='<?php echo $this->_tpl_vars['item']->links['del_from_favorite']; ?>
'><img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
f_favoritelist_0.png" title="<?php echo smarty_function_jtext(array('text' => 'ADS_REMOVE_FROM_FAVORITE'), $this);?>
" alt="<?php echo smarty_function_jtext(array('text' => 'ADS_REMOVE_FROM_FAVORITE'), $this);?>
" height="16" class="ads_noborder" /></a>
                                </span>
                            <?php endif; ?>
                        <?php else: ?>
                                <a href='<?php echo $this->_tpl_vars['item']->links['edit']; ?>
'><img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
edit.png" title="<?php echo smarty_function_jtext(array('text' => 'ADS_EDIT'), $this);?>
" alt="<?php echo smarty_function_jtext(array('text' => 'ADS_EDIT'), $this);?>
" height="16" class="ads_noborder" /></a>
                        <?php endif; ?>
                        <?php if ($this->_tpl_vars['item']->more_pictures): ?>
                            <img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
morepics_on.png" title="<?php echo smarty_function_jtext(array('text' => 'ADS_MORE_PICTURES'), $this);?>
" alt="<?php echo smarty_function_jtext(array('text' => 'ADS_MORE_PICTURES'), $this);?>
" height="16" class="ads_noborder" />
                        <?php else: ?>
                            <img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
morepics_off.png" title="<?php echo smarty_function_jtext(array('text' => 'ADS_NO_MORE_PICTURES'), $this);?>
" alt="<?php echo smarty_function_jtext(array('text' => 'ADS_NO_MORE_PICTURES'), $this);?>
" height="16" class="ads_noborder" />
                        <?php endif; ?>
                        <?php if ($this->_tpl_vars['item']->atachment): ?>
                            <img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
downloads_on.png" title="<?php echo smarty_function_jtext(array('text' => 'ADS_HAS_DOWNLOADS'), $this);?>
" alt="<?php echo smarty_function_jtext(array('text' => 'ADS_HAS_DOWNLOADS'), $this);?>
" height="16" class="ads_noborder" />
                        <?php else: ?>
                            <img src="<?php echo $this->_tpl_vars['IMAGE_ROOT']; ?>
downloads_off.png" title="<?php echo smarty_function_jtext(array('text' => 'ADS_HAS_NO_DOWNLOADS'), $this);?>
" alt="<?php echo smarty_function_jtext(array('text' => 'ADS_HAS_NO_DOWNLOADS'), $this);?>
" height="16" class="ads_noborder" />
                        <?php endif; ?>

                        <div style="clear: both;"></div>
                    </div>

                    <div class="adds_man_item_small_box">
                        <strong><span style="float: right !important; font-size: 1.6em;"><?php echo $this->_tpl_vars['item']->askprice; ?>
&nbsp;
                                                 <?php if (is_numeric ( $this->_tpl_vars['item']->askprice )): ?><?php echo $this->_tpl_vars['item']->currency_name; ?>
<?php endif; ?></span></strong>
                    </div>

                    <div class="adds_man_item_small_box">

                        <?php if ($this->_tpl_vars['item']->expired): ?>
                            <div>
                                <span class='expired'><?php echo smarty_function_jtext(array('text' => 'ADS_EXPIRED'), $this);?>
</span>
                            </div>
                        <?php elseif ($this->_tpl_vars['item']->closed && $this->_tpl_vars['item']->status == 1 && ! $this->_tpl_vars['item']->archived && ! $this->_tpl_vars['item']->close_by_admin && $this->_tpl_vars['item']->closed_date <= $this->_tpl_vars['item']->end_date): ?>
                           <div>
                                <span class='expired'><?php echo smarty_function_jtext(array('text' => 'ADS_AD_CANCELED'), $this);?>
</span>
                            </div>
                        <?php elseif (@ads_opt_enable_countdown && ! $this->_tpl_vars['item']->closed && ! $this->_tpl_vars['item']->close_by_admin): ?>
                            <div>
                                <?php if (@ads_opt_sort_date == 'start'): ?>
                                    <?php echo smarty_function_jtext(array('text' => 'ADS_POSTED_ON'), $this);?>
: <?php echo $this->_tpl_vars['item']->start_date_text; ?>

                                <?php else: ?>
                                    <span id="time<?php echo $this->_tpl_vars['key']+1; ?>
"><?php echo $this->_tpl_vars['item']->countdown; ?>
</span>
                                <?php endif; ?>
                            </div>
                        <?php elseif ($this->_tpl_vars['item']->archived): ?>
                           <div>
                                <span class='expired'><?php echo smarty_function_jtext(array('text' => 'ADS_AD_ARCHIVED'), $this);?>
</span>
                            </div>
                        <?php elseif ($this->_tpl_vars['item']->close_by_admin): ?>
                            <div>
                                <span class='expired'><?php echo smarty_function_jtext(array('text' => 'ADS_AD_CLOSED_BY_ADMIN'), $this);?>
</span>
                            </div>
                        <?php endif; ?>
                    </div>

                    <!--<div class="adds_man_item_small_box">
                        <strong>                    </div>-->

                 <!-- <div class="adds_man_item_small_box">
                                      </div>-->
                 </div>
             </div>
            </div>
        </td>

  <?php if (($this->_foreach['item_loop']['iteration']-1) % $this->_tpl_vars['nr_columns'] == $this->_tpl_vars['nr_columns']-1 || $this->_tpl_vars['nr_items']-1 == ($this->_foreach['item_loop']['iteration']-1)): ?>
    <?php if ($this->_tpl_vars['nr_items']-1 == ($this->_foreach['item_loop']['iteration']-1)): ?>
        <?php $this->assign('nrt', ($this->_tpl_vars['nr_columns']-($this->_foreach['item_loop']['iteration']-1)%$this->_tpl_vars['nr_columns']-1)); ?>
        <?php unset($this->_sections['trailing']);
$this->_sections['trailing']['name'] = 'trailing';
$this->_sections['trailing']['loop'] = is_array($_loop=$this->_tpl_vars['nrt']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['trailing']['show'] = true;
$this->_sections['trailing']['max'] = $this->_sections['trailing']['loop'];
$this->_sections['trailing']['step'] = 1;
$this->_sections['trailing']['start'] = $this->_sections['trailing']['step'] > 0 ? 0 : $this->_sections['trailing']['loop']-1;
if ($this->_sections['trailing']['show']) {
    $this->_sections['trailing']['total'] = $this->_sections['trailing']['loop'];
    if ($this->_sections['trailing']['total'] == 0)
        $this->_sections['trailing']['show'] = false;
} else
    $this->_sections['trailing']['total'] = 0;
if ($this->_sections['trailing']['show']):

            for ($this->_sections['trailing']['index'] = $this->_sections['trailing']['start'], $this->_sections['trailing']['iteration'] = 1;
                 $this->_sections['trailing']['iteration'] <= $this->_sections['trailing']['total'];
                 $this->_sections['trailing']['index'] += $this->_sections['trailing']['step'], $this->_sections['trailing']['iteration']++):
$this->_sections['trailing']['rownum'] = $this->_sections['trailing']['iteration'];
$this->_sections['trailing']['index_prev'] = $this->_sections['trailing']['index'] - $this->_sections['trailing']['step'];
$this->_sections['trailing']['index_next'] = $this->_sections['trailing']['index'] + $this->_sections['trailing']['step'];
$this->_sections['trailing']['first']      = ($this->_sections['trailing']['iteration'] == 1);
$this->_sections['trailing']['last']       = ($this->_sections['trailing']['iteration'] == $this->_sections['trailing']['total']);
?>

        <td>&nbsp;</td>

        <?php endfor; endif; ?>
    <?php endif; ?>
    </tr>
    <tr><td>&nbsp;</td></tr>
  <?php endif; ?>
<?php endforeach; endif; unset($_from); ?>
</table>
<?php if (! ( count($this->_tpl_vars['items']) > 0 )): ?>
	<div class="adds_man_item1">
	 <?php echo smarty_function_jtext(array('text' => 'ADS_NO_ADS'), $this);?>

	</div>
<?php endif; ?>
</div>
