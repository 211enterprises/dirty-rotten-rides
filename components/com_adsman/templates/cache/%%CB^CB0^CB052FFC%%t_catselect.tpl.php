<?php /* Smarty version 2.6.26, created on 2013-08-06 16:47:31
         compiled from t_catselect.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'set_css', 't_catselect.tpl', 1, false),array('function', 'jtext', 't_catselect.tpl', 4, false),array('modifier', 'htmlentities', 't_catselect.tpl', 23, false),)), $this); ?>
<?php echo $this->_plugins['function']['set_css'][0][0]->smarty_set_css(array(), $this);?>

	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 't_javascript_language.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<div class="adsCatContainer">
	<h3 class="adCatHeader"><?php echo smarty_function_jtext(array('text' => 'ADS_PAGE_CATEGORIES'), $this);?>
</h3>
	<form name="category_select" action="<?php echo $this->_tpl_vars['ROOT_HOST']; ?>
index.php?option=com_adsman&amp;task=edit&amp;<?php if ($this->_tpl_vars['id']): ?>id=<?php echo $this->_tpl_vars['id']; ?>
&amp;<?php endif; ?>Itemid=<?php echo $this->_tpl_vars['Itemid']; ?>
" method="post">
		<input type="hidden" name="category_hit" value="1" />
		<div class="category_select_toolbox">
			<a href="javascript:document.category_select.submit();">
				<span id="button_up"><?php echo smarty_function_jtext(array('text' => 'ADS_SELECT_CATEGORY'), $this);?>
</span>
			</a>
		</div>
	
		<table id="adds_categories_select" class="ads_table" width="100%">
		<?php $_from = $this->_tpl_vars['categories']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['category']):
?>
		<tr>
			<td>
					<div id="cat_<?php echo $this->_tpl_vars['category']['id']; ?>
" style="display:none;"><?php echo $this->_tpl_vars['category']['description']; ?>
</div>
					<input type="radio" name="category" value="<?php echo $this->_tpl_vars['category']['id']; ?>
" onclick="toggleButton();" <?php if ($this->_tpl_vars['cat'] == $this->_tpl_vars['category']['id']): ?> checked="checked" <?php endif; ?> />
					<sup>|_</sup>
					<?php unset($this->_sections['cur']);
$this->_sections['cur']['name'] = 'cur';
$this->_sections['cur']['loop'] = is_array($_loop=$this->_tpl_vars['category']['depth']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['cur']['show'] = true;
$this->_sections['cur']['max'] = $this->_sections['cur']['loop'];
$this->_sections['cur']['step'] = 1;
$this->_sections['cur']['start'] = $this->_sections['cur']['step'] > 0 ? 0 : $this->_sections['cur']['loop']-1;
if ($this->_sections['cur']['show']) {
    $this->_sections['cur']['total'] = $this->_sections['cur']['loop'];
    if ($this->_sections['cur']['total'] == 0)
        $this->_sections['cur']['show'] = false;
} else
    $this->_sections['cur']['total'] = 0;
if ($this->_sections['cur']['show']):

            for ($this->_sections['cur']['index'] = $this->_sections['cur']['start'], $this->_sections['cur']['iteration'] = 1;
                 $this->_sections['cur']['iteration'] <= $this->_sections['cur']['total'];
                 $this->_sections['cur']['index'] += $this->_sections['cur']['step'], $this->_sections['cur']['iteration']++):
$this->_sections['cur']['rownum'] = $this->_sections['cur']['iteration'];
$this->_sections['cur']['index_prev'] = $this->_sections['cur']['index'] - $this->_sections['cur']['step'];
$this->_sections['cur']['index_next'] = $this->_sections['cur']['index'] + $this->_sections['cur']['step'];
$this->_sections['cur']['first']      = ($this->_sections['cur']['iteration'] == 1);
$this->_sections['cur']['last']       = ($this->_sections['cur']['iteration'] == $this->_sections['cur']['total']);
?>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<?php endfor; endif; ?>
					<span <?php if ($this->_tpl_vars['category']['description']): ?> class="hasTip" title="<?php echo ((is_array($_tmp=$this->_tpl_vars['category']['description'])) ? $this->_run_mod_handler('htmlentities', true, $_tmp) : htmlentities($_tmp)); ?>
" <?php endif; ?> >
						<?php echo $this->_tpl_vars['category']['catname']; ?>

					</span>
			</td>
		</tr>
		<?php endforeach; endif; unset($_from); ?>
		</table>
	
	</form>
	
	<div class="category_select_toolbox">
		<a href="javascript:document.category_select.submit();">
			<span id="button_dwn"><?php echo smarty_function_jtext(array('text' => 'ADS_SELECT_CATEGORY'), $this);?>
</span>
		</a>
	</div>
</div>