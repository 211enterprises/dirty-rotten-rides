{include file='t_javascript_language.tpl'}
{set_css}
{set_js}

<form action="{$action}" method="post" name="adsForm" id="adsForm">
	<input type="hidden" name="Itemid" value="{$Itemid}" />
	<input type="hidden" name="option" value="{$option}" />
	<input type="hidden" name="task" value="{$task}" />
	<input type="hidden" name="filter_order_Dir" value="{$filter_order_Dir}" />
	<input type="hidden" name="filter_order" value="{$filter_order}" />
    <input type="hidden" name="list_view" value="{$list_view}" />
    <input type="hidden" name="archive" value="{$archive}" />
	{include file="t_listadds_header.tpl"}
<div class="adds_man_list">
    <table width="100%" cellpadding="0" cellspacing="0" class="ads_noborder">
    {foreach from=$items item=item key=key}
    	{if $item->featured && 	$item->featured!='none'}
    		{assign var=class_featured value="listing-"|cat:$item->featured}
    	{else}
    		{assign var=class_featured value=""}
    	{/if}
    	{cycle assign=class_suffix values="1,2"}
        {assign var="cellfile" value="elements/list/category-related/t_cell_catid_"|cat:$item->category}
        {assign var="cellfile" value=$cellfile|cat:".tpl"}
       
        {if file_exists("components/com_adsman/templates/demo/$cellfile")}
            {include file="$cellfile"}
        {else}
            {include file="elements/list/t_listadds_cell.tpl"}
        {/if}
        <tr>
           <td style="height:1px;line-height:4px;border:0px; background: #ffffff;">&nbsp;</td>
        </tr>
        <!--<tr><td class="add_list_spacer"><hr /></td></tr>-->
    {/foreach}
    </table>
    {if !($items|@count>0) }
    	<div class="adds_man_item1">
    	 {jtext text="ADS_NO_ADS"}
    	</div>
    	
    {/if}
	<div class="pagination">
		{$pagination->getPagesLinks()}
	</div>
</div>

	
</form>
