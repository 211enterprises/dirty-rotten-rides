
{if $smarty.const.ads_opt_allow_messaging}
    {starttab paneid="tab2" text="ADS_TAB_MESSAGES"}
        {* messages Tab *}
        <ul class="add_message_list" >
            {foreach from=$add->messages name=messages item=mes key=k}
            {assign var=t value=$k+1}
                <li>
                    {if $mes->from_user==$add->userid}{* Is Owner Reply*}
                        {if !$smarty.foreach.messages.last && $mes->to_user==$add->messages[$t]->from_user}
                            {assign var=reply value="reply"}
                            {capture name="touser"}{/capture}
                        {else}
                            {assign var=reply value=""}
                            {capture name="touser"}@{$mes->tousername}:&nbsp;{/capture}
                        {/if}
                        <span class="add_field_label top">{$mes->fromusername|default:"Guest"}&nbsp;({printdate date=$mes->datesend use_hour=1})
                        </span>
                        <p class="triangle-right top {$reply}">
                           {$smarty.capture.touser}{$mes->comment|replace:'\r\n':'<br />'|stripslashes}
                        </p>
                    {else}
                        <p class="triangle-right">
                           {$mes->comment|replace:'\r\n':'<br />'|stripslashes}
                        </p>
                        <span class="add_field_label">{$mes->fromusername|default:"Guest"}&nbsp;({printdate date=$mes->datesend use_hour=1})
                		{if ($add->is_my_add) && (!$add->closed)}
                			<a href="javascript:addman_reply({$mes->from_user},'{$mes->fromusername}');">{jtext text="ADS_REPLY"}</a>
                        {/if}
                        </span>
                    {/if}
                </li>
            {/foreach}
        </ul>
  		{if ($add->is_my_add) && (!$add->closed)}
            <div id="adds_reply_message" style="display:none">
                <h2>{jtext text="ADS_SEND_MESSAGE_TO"}:&nbsp;<span id="username_reply"></span></h2>
                <form action="{$ROOT_HOST}index.php" method="post" name="adsForm_reply">
                    <input type="hidden" name="Itemid" value="{$Itemid}" />
                    <input type="hidden" name="option" value="{$option}" />
                    <input type="hidden" name="id" value="{$add->id}" />
                    <input type="hidden" name="task" value="sendMessage" />
                    <input type="hidden" name="to_reply" value="" />
                    <input type="hidden" id="from" name="from" value="{$userid}" />
                    <textarea name="message_textarea_reply" class="inputbox"></textarea>
                    <div>
                        <input type="submit" name="sendmessagebuton" value="{jtext text='ADS_SEND_MESSAGE_BTN'}" class="back_button" {if $userid == 0} disabled="disabled" {/if} />
                        <input name="cancelButton" type="button" class="back_button" value="{jtext text='ADS_CANCEL'}" onclick="addman_reply_close();" />
                    </div>
                </form>        
            </div>
        {/if}
    {endtab}

    {if (!$add->is_my_add) && ($add->closed==0) }
	   {starttab paneid="tab3" text="ADS_TAB_SEND_MESSAGE"}
           {if !(( $userid!=0 || $smarty.const.ads_opt_allow_guest_messaging )) } 
                {assign var=disable_message value='disabled="disabled"'} 
                <h2>{jtext text="ADS_ERR_LOGIN_TO_SEND_MESSAGE"}</h2>
           {else}
                <h2>{jtext text="ADS_SEND_MESSAGE_TO"}:&nbsp;{$user->username}</h2>
           {/if}
            <form action="{$ROOT_HOST}index.php" method="post" name="adsForm_message">
                <input type="hidden" name="Itemid" value="{$Itemid}" />
                <input type="hidden" name="option" value="{$option}" />
                <input type="hidden" name="id" value="{$add->id}" />
                <input type="hidden" name="task" value="sendMessage" />
                <input type="hidden" name="to_reply" value="" />
                <input type="hidden" id="from" name="from" value="{$userid}" />
                <p class="triangle-border bottom">
                    <textarea name="message_textarea_reply" class="inputbox" {$disable_message}></textarea>
                </p>
                <input type="submit" name="sendmessagebuton" value="{jtext text='ADS_SEND_MESSAGE_BTN'}" class="back_button"  {$disable_message} />
                <br />
    			{if !$userid && $smarty.const.ads_opt_enable_captcha && $smarty.const.ads_opt_allow_guest_messaging}
                    <div class="captcha_container">
                        {jtext text="ADS_CAPTCHA"}
                        {$cs}
                    </div>
                {/if}
            </form>        

		{endtab}
    {/if}
		
{/if}
