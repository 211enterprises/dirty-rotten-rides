
{if $add->is_my_add && ($add->featured=='none' || !$add->featured) && !$add->expired && !$add->closed}

{assign var="gold_add" value="$ROOT_HOST_FEATURED&amp;featured=gold&amp;id=`$add->id`&amp;Itemid=$Itemid"}
{assign var="silver_add" value="$ROOT_HOST_FEATURED&amp;featured=silver&amp;id=`$add->id`&amp;Itemid=$Itemid"}
{assign var="bronze_add" value="$ROOT_HOST_FEATURED&amp;featured=bronze&amp;id=`$add->id`&amp;Itemid=$Itemid"}

	{if $pricing_plugins.price_featured_gold || $pricing_plugins.price_featured_silver || $pricing_plugins.price_featured_bronze}
	
		<table width="100%" class="ads_bottom_border ads_table">
			<tr>
			    <td colspan="3" align="center" class="ads_bottom_border"><span class="upgrade_title">{jtext text="ADS_PAY_UPGRADE_LISTING"}</span></td>
			</tr>
			<tr><td colspan="3">&nbsp;</tr>
            <tr>
            {if $pricing_plugins.price_featured_gold && $pricing_plugins.price_featured_gold->enabled}
             <td align="center">
                <a href="{$gold_add}"><img src="{$ROOT_HOST}components/com_adsman/templates/demo/image/featured_gold.png" class="ads_noborder" title="{jtext text='ADS_PAY_PAYMENT_FEATURED_GOLD'}" alt="{jtext text='ADS_PAY_PAYMENT_FEATURED_GOLD'}" /><br />
                    {jtext text="ADS_PAY_PAYMENT_FEATURED_GOLD"}
                </a>
             </td>
            {/if}
            {if $pricing_plugins.price_featured_silver && $pricing_plugins.price_featured_silver->enabled}
             <td align="center">
                <a href="{$silver_add}"><img src="{$ROOT_HOST}components/com_adsman/templates/demo/image/featured_silver.png" class="ads_noborder" title="{jtext text='ADS_PAY_PAYMENT_FEATURED_SILVER'}" alt="{jtext text='ADS_PAY_PAYMENT_FEATURED_SILVER'}" /><br />
                    {jtext text="ADS_PAY_PAYMENT_FEATURED_SILVER"}
                </a>
             </td>
            {/if}
            {if $pricing_plugins.price_featured_bronze && $pricing_plugins.price_featured_bronze->enabled}
             <td align="center">
                <a href="{$bronze_add}"><img src="{$ROOT_HOST}components/com_adsman/templates/demo/image/featured_bronze.png" class="ads_noborder" title="{jtext text='ADS_PAY_PAYMENT_FEATURED_BRONZE'}" alt="{jtext text='ADS_PAY_PAYMENT_FEATURED_BRONZE'}" /><br />
                    {jtext text="ADS_PAY_PAYMENT_FEATURED_BRONZE"}
                </a>
             </td>
            {/if}
            </tr>
        </table>
    {/if}
{/if}