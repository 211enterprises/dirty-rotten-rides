
{if $pricing_plugins.price_contact->enabled==1 && !$add->is_my_add}
    {starttab paneid="tab5" text="ADS_GOOGLEMAP"}
        {jtext text="ADS_HIDDEN_MAP"}
    {endtab}
{else}
    {if $smarty.const.ads_opt_google_key!="" && $smarty.const.ads_map_in_ads_details=="1"
        && ( ($add->MapX!="" && $add->MapY!="") || ($user->googleX!="" && $user->googleY!="") )}
        {starttab paneid="tab5" text="ADS_GOOGLEMAP"}
        <div id="map_canvas"></div>
		{endtab}
    {/if}
{/if}
