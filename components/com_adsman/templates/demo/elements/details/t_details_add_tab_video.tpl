
{if $video_link != ''}
		
    {* video Tab *}
	{starttab paneid="tab4" text="ADS_TAB_VIDEO"}
		
	  	<div class="round_media">
		  <table width="100%" class="ads_table" cellspacing="0" cellpadding="0" border="0">	
	  		{if $video_link_source == 'youtube'}
				<tr>
				  <td class="media_width" rowspan="2">
					<div class="media_thumbnail">
					  <a title="{$video_title}" onclick="jQueryFactory.prettyPhoto.open('{$video_link}', '', '')" href="javascript:void(0);">
						<img src="http://{$thumb_source}" alt="YouTube" class="media_width" /><br /> 
					  </a>
					</div>
				  </td>
			    </tr>
			 {/if}  
			 {if $video_link_source =='vimeo'}	
				<tr>
				  <td class="media_width" rowspan="2">
					<div class="media_thumbnail">
					  <a title="{$video_title}" onclick="jQueryFactory.prettyPhoto.open('{$video_link}', '', '')" href="javascript:void(0);">
						<img src="http://{$thumb_source}" rel="prettyPhoto" alt="<?php echo $link->video_sitename; ?>" class="media_width" /><br />
					  </a>
					</div>
				  </td>
				</tr>
			  {/if}			 
			  {if $video_link_source == 'metacafe'}
				<tr>
				  <td class="media_width" rowspan="2">
					<div class="media_thumbnail">
						<a href="{$video_link}?width=70%&amp;height=70%" rel="prettyPhoto" title="{$video_title}">
						<img  src="{$thumb_source}" class="media_width" alt="{$video_title}"></a>
			
					</div>
				  </td>
				 
				</tr>
			  {/if}
			  {if $video_link_source == 'myspace'}
				<tr>
				   <td class="media_width" rowspan="2">
					<div class="media_thumbnail">
					  <a title="{$video_title}"  onclick="jQueryFactory.prettyPhoto.open('{$video_link}', '', '')" href="javascript:void(0);">
						<img src="{$thumb_source}" alt="{$video_sitename}" class="media_width" /><br />
					  </a>
					</div>
				  </td>
				</tr>
			  {/if}	
			  {if $video_link_source == 'howcast'}
				<tr>
				  <td class="media_width" rowspan="2">
					<div class="media_thumbnail">
						<a href="{$video_link}?width=500&height=350" rel="prettyPhoto" title="{$video_title}">
						<img  src="{$video_sourceThumb}" class="media_width" alt="{$video_title}"></a>
						<br />
					</div>	
				  </td>
				  
				</tr>
			  {/if}	
			  
				<tr>
				  <td class="align-top">
					<div class="video_content">
						<span class="media_title">{$video_title}</span>
		    			<div class="video-description">{$video_description}</div>	
					</div>	
				  </td>
				</tr>	
				
			</table>		
		</div>
	{endtab}
{/if} 
