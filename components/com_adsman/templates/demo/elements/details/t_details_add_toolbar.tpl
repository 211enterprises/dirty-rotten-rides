<span class="ads_right ads_table" style="float:right;">
	{if $add->is_my_add && $add->close_by_admin == 0}
		{if !$add->closed}
			<a href="{$add->links.edit}" class="ads_right10" ><img src="{$ROOT_HOST}components/com_adsman/templates/demo/image/pencil_48.png" class="ads_noborder" alt="{jtext text='ADS_EDIT'}" title="{jtext text='ADS_EDIT'}" height="32" /></a>
			<a href="{$add->links.cancel}" class="ads_right10" onclick="return confirm('{jtext text='ADS_CONFIRM_CANCEL'}')"><img src="{$IMAGE_ROOT}cancel_48.png" class="ads_noborder" alt="{jtext text='ADS_CANCEL'}" title="{jtext text='ADS_CANCEL'}" height="32" /></a>
		{else}
			<input name="extendbutton" type="button" value="{jtext text='ADS_EXTEND'}"  onclick="window.location = '{$add->links.extend_add}';" class="back_button" />
			<input name="republishbutton" type="button" value="{jtext text='ADS_REPUBLISH'}"  onclick="window.location = '{$add->links.republish}';" class="back_button" />
		{/if}
	{/if}
	<a href="javascript:history.go(-1)"><img src="{$ROOT_HOST}components/com_adsman/templates/demo/image/arrow_left_48.png" class="ads_noborder" alt="{jtext text='ADS_BACK'}" title="{jtext text='ADS_BACK'}" height="32" /></a>
</span>
