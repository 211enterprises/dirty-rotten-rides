<fieldset>
    <legend>{jtext text="ADS_POSTED_BY"}</legend>
    <ul>
        <li>
            <a href="{$add->links.user_profile}"  title="{jtext text='ADS_PAGE_VIEW_PROFILE'}">{$user->username}</a>
                <a href="{$add->links.otheradds}" title="{jtext text='ADS_PAGE_VIEW_MORE_ADS'}&nbsp;{$user->username}">({$count_user_ads})</a>
        </li>
        {if $add->addtype==$smarty.const.ADDSMAN_TYPE_PRIVATE}
            <li><span class="add_text_medium_bold">{jtext text="ADS_PRIVATE_AD_WARNING"}</span></li>    
        {else}
    		{if !$pricing_plugins.price_contact->enabled}
                {if $add->guest_username != ''}
                    <li><label>{jtext text="ADS_NAME"}:</label>{$add->guest_username}</li>
                    <li><label>{jtext text="ADS_EMAIL"}:</label>{$add->guest_email}</li>
                {else}
                    <li><label>{jtext text="ADS_NAME"}:</label>{$user->name} {$user->surname}</li>
                    <li><label>{jtext text="ADS_ADDRESS"}:</label>{$user->address}</li>
                {/if}
            {/if}
            <li><label>{jtext text="ADS_CITY"}:</label>{$user->city}</li>
            <li><label>{jtext text="ADS_STATE"}:</label>{$user->state}</li>
            <li><label>{jtext text="ADS_COUNTRY"}:</label>{$user->country}</li>
        {/if}
    </ul>
</fieldset>
