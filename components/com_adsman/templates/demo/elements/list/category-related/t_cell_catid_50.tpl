	<tr class="adds_man_item{$class_suffix}_title {$class_featured}">
		<td class="add_list_container">
            <table class="ads_noborder">
			<tr>
                <td class="width100top" width="{$thumb_width}">
			       	<a href="{$item->links.details}" title="{$item->title}">{$item->thumbnail}</a>
				</td>
		    	<td valign="top">
                    <div class="title">                 
 	                  <a href="{$item->links.details}"><span>{$item->title}</span></a>
                    </div>
		    	   <div class="adsman_shortdescription">
                       <strong>Rooms:{$item->roomsnr}</strong><br />                      
                       {if $smarty.const.ads_short_desc_long!="" && $item->short_description != ""}
                            {$item->short_description|truncate:$smarty.const.ads_short_desc_long}
                       {elseif $item->short_description != ""}
                           {$item->short_description}
                       {else}
                          &nbsp;
                       {/if}
		   	       </div>
                   <div class="price">
                        {if $item->askprice}{$item->askprice}{/if} {if is_numeric($item->askprice)} <span class="currency">{$item->currency_name}</span>{/if}
                   </div>
                   <div class="countdown">
                        <span class="adds_date">{printdate date=$item->start_date use_hour=0}</span><br />
                        {if  $item->expired }
                                <span class='expired'>{jtext text="ADS_EXPIRED"}</span>
                        {elseif $item->close_by_admin == 1}
                                <span class='expired'>{jtext text="ADS_AD_CLOSED_BY_ADMIN"}</span>
                        {elseif $item->closed && $item->status == 1 && !$item->archived && $item->close_by_admin==0 && $item->closed_date <= $item->end_date}
                                <span class='expired'>{jtext text="ADS_AD_CANCELED"}</span>
                        {elseif $smarty.const.ads_opt_enable_countdown && !$item->closed && !$item->close_by_admin}
                                <span id="time{$key+1}">{$item->countdown}</span>
                        {elseif $item->archived}
                                <span class='expired'>{jtext text="ADS_AD_ARCHIVED"}</span>
                        {/if}
                   </div>
		    	   {include file='elements/display_fields.tpl' position='listingmiddle' this_add=$item->id}
				</td>
			</tr>
			</table>
		</td>
	</tr>
    <tr class="adds_man_item{$class_suffix}_footer {$class_featured}">
       <td>
        <div class="add_footer_container">
            <div class="adds_man_list_rest">
 	   		    <span class="category_link">&gt;<a href='{$item->links.filter_cat}'>{$item->catname}</a></span>
                {include file='elements/display_fields.tpl' position='favoritesads'}
            </div>
            <div class="adds_man_list_toolbar">
                {if !$item->is_my_add}
                    {if $item->favorite==0 }
                        <a href='{$item->links.add_to_favorite}'>
                            <img src="{$IMAGE_ROOT}f_favoritelist_1.png" title="{jtext text='ADS_ADD_TO_FAVORITE'}" alt="{jtext text='ADS_ADD_TO_FAVORITE'}" height="16" class="ads_noborder" />
                        </a>
                    {elseif $item->favorite==1}
                        <a href='{$item->links.del_from_favorite}'>
                            <img src="{$IMAGE_ROOT}f_favoritelist_0.png" title="{jtext text='ADS_REMOVE_FROM_FAVORITE'}" alt="{jtext text='ADS_REMOVE_FROM_FAVORITE'}" height="16" class="ads_noborder" />
                        </a>
                    {/if}
                {else}
                    <a href='{$item->links.edit}'><img src="{$IMAGE_ROOT}edit.png" title="{jtext text='ADS_EDIT'}" alt="{jtext text='ADS_EDIT'}" height="16" class="ads_noborder" /></a>
                {/if}
                {if $item->more_pictures}
                    <img src="{$IMAGE_ROOT}morepics_on.png" title="{jtext text='ADS_MORE_PICTURES'}" alt="{jtext text='ADS_MORE_PICTURES'}" height="16" class="ads_noborder" />
                {else}
                    <img src="{$IMAGE_ROOT}morepics_off.png" title="{jtext text='ADS_NO_MORE_PICTURES'}" alt="{jtext text='ADS_NO_MORE_PICTURES'}" height="16" class="ads_noborder" />
                {/if}
                {if $smarty.const.ads_allow_atachment}
                    {if $item->atachment}
                        <img src="{$IMAGE_ROOT}downloads_on.png" title="{jtext text='ADS_HAS_DOWNLOADS'}" alt="{jtext text='ADS_HAS_DOWNLOADS'}" height="16" class="ads_noborder" />
                    {else}
                        <img src="{$IMAGE_ROOT}downloads_off.png" title="{jtext text='ADS_HAS_NO_DOWNLOADS'}" alt="{jtext text='ADS_HAS_NO_DOWNLOADS'}" height="16" class="ads_noborder" />
                    {/if}
                {/if}
                {if $smarty.const.ads_opt_google_key!="" && $smarty.const.ads_map_in_ads_details=="1"}
                   {if $item->MapX!="" && $item->MapY!=""}
                       <a class="modal" href="{$ROOT_HOST}index.php?option=com_adsman&amp;task=googlemap&amp;x={$item->MapX}&amp;y={$item->MapY}&amp;id={$item->id}&amp;tmpl=component"{literal} rel="{handler: 'iframe', size: { {/literal} x:{$smarty.const.ads_opt_googlemap_gx+25|default:'260'}, y:{$smarty.const.ads_opt_googlemap_gy+120|default:'100'}} {literal} } " {/literal}>
                           <img src="{$IMAGE_ROOT}map.png" title="{jtext text='ADS_GOOGLEMAP'}" alt="{jtext text='ADS_GOOGLEMAP'}" height="16" class="ads_noborder" />
                       </a>
                   {/if}
                {/if}
            </div>
            <div class="adds_man_list_author">
                {include file="elements/list/t_listadds_user.tpl"}
            </div>
        </div>
       </td>
    </tr>
