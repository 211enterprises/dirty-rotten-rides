<tr class="adds_myitem{$class_suffix} {$class_featured}">
    <td>
        <a href='{$item->links.filter_cat}' class="category_link">{$item->catname}</a>
    </td>
    <td>
        <a href="{$item->links.details}" title="{$item->title}">{$item->thumbnail}</a>    
    </td>
    <td>
        <a href="{$item->links.details}">{$item->title}</a>
    </td>
    <td class="price">
        {$item->askprice} {if $item->askprice>0}<span class="currency">{$item->currency_name}</span>{/if}
    </td>
    <td class="countdown">
        {if  $item->expired }
                <span class='expired'>{jtext text="ADS_EXPIRED"}</span>
        {elseif $item->close_by_admin == 1}
                <span class='expired'>{jtext text="ADS_AD_CLOSED_BY_ADMIN"}</span>
        {elseif $item->closed && $item->status == 1 && !$item->archived && $item->close_by_admin==0 && $item->closed_date <= $item->end_date}
                <span class='expired'>{jtext text="ADS_AD_CANCELED"}</span>
        {elseif $smarty.const.ads_opt_enable_countdown && !$item->closed && !$item->close_by_admin}
                <span id="time{$key+1}">{$item->countdown}</span>
        {elseif $item->archived}
                <span class='expired'>{jtext text="ADS_AD_ARCHIVED"}</span>
        {/if}
    </td>
</tr>
<tr class="adds_myitem_spacer">
    <td colspan="5">&nbsp;</td>
</tr>