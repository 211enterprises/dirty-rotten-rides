{include file='t_javascript_language.tpl'}
{set_css}
{set_js}

<div class="componentheading">{$page_title}</div>
<div>
    <a style="cursor:pointer;" onclick="javascript:viewArchiveAds('active','myadds');"><span class="text_header_small {if $archive=='active'}active{/if}">{jtext text="ADS_ACTIVE"}</span></a> |
    <a style="cursor:pointer;" onclick="javascript:viewArchiveAds('expired','myadds');"><span class="text_header_small {if $archive=='expired'}active{/if}">{jtext text="ADS_EXPIRED"}</span></a> |
    <a style="cursor:pointer;" onclick="javascript:viewArchiveAds('archive','myadds');"><span class="text_header_small {if $archive=='archive'}active{/if}">{jtext text="ADS_ARCHIVE"}</span></a> |
    <a style="cursor:pointer;" onclick="javascript:viewArchiveAds('unpublish','myadds');"><span class="text_header_small {if $archive=='unpublish'}active{/if}">{jtext text="ADS_UNPUBLISHED"}</span></a>
</div>

<form action="{$action}" method="post" name="adsForm" id="adsForm">
	<input type="hidden" name="Itemid" value="{$Itemid}" />
	<input type="hidden" name="option" value="{$option}" />
	<input type="hidden" name="task" value="{$task}" />
	<input type="hidden" name="filter_order_Dir" value="{$filter_order_Dir}" />
	<input type="hidden" name="filter_order" value="{$filter_order}" />
    <input type="hidden" name="list_view" value="{$list_view}" />
    <input type="hidden" name="archive" value="{$archive}" />

    <div class="adds_man_mylist">
    
        <table width="100%" cellpadding="0" cellspacing="0" class="ads_noborder">
            <tr>
                <th>{jtext text="ADS_CATEGORY"}</th>
                <th colspan="2">{jtext text="ADS_TITLE"}</th>
                <th>{jtext text="ADS_PRICE"}</th>
                <th>{jtext text="ADS_ENDING_IN"}</th>
            </tr>
            <tr>
                <td align="ceenter">&nbsp;</td>
                <td colspan="2" align="ceenter">
                    {if $filter_order=="title" }
                        {if $filter_order_Dir=="ASC"}
                            <img src="{$IMAGE_ROOT}arrow-up_b.png" class="ads_noborder ads_top2" alt='Up' />
                            <a style="cursor:pointer;" onclick="javascript:tableAdsOrdering('title','DESC');"><img src="{$IMAGE_ROOT}arrow-down.png" class="ads_noborder ads_top2" alt='Down' /></a>
                        {else}
                            <a style="cursor:pointer;" onclick="javascript:tableAdsOrdering('title','ASC');"><img src="{$IMAGE_ROOT}arrow-up.png" class="ads_noborder ads_top2" alt='Down' /></a>
                            <img src="{$IMAGE_ROOT}arrow-down_b.png" class="ads_noborder ads_top2" />
                        {/if}
                    {else}
                        <a style="cursor:pointer;" onclick="javascript:tableAdsOrdering('title','ASC');"><img src="{$IMAGE_ROOT}arrow-up.png" class="ads_noborder ads_top2" alt='Up' /></a>
                        <a style="cursor:pointer;" onclick="javascript:tableAdsOrdering('title','DESC');"><img src="{$IMAGE_ROOT}arrow-down.png" class="ads_noborder ads_top2" alt='Down' /></a>
                    {/if}
                </td> 
                <td align="ceenter">
                    {if $filter_order=="askprice+0" }
                        {if $filter_order_Dir=="ASC"}
                           <img src="{$IMAGE_ROOT}arrow-up_b.png" class="ads_noborder ads_top2" alt='Up' />
                           <a style="cursor:pointer;" onclick="javascript:tableAdsOrdering('askprice+0','DESC');"><img src="{$IMAGE_ROOT}arrow-down.png" class="ads_noborder ads_top2" alt='Down' /></a>
                        {else}
                           <a style="cursor:pointer;" onclick="javascript:tableAdsOrdering('askprice+0','ASC');"><img src="{$IMAGE_ROOT}arrow-up.png" class="ads_noborder ads_top2" alt='Up' /></a>
                           <img src="{$IMAGE_ROOT}arrow-down_b.png" class="ads_noborder ads_top2" alt='Down' />
                        {/if}
                    {else}
                           <a style="cursor:pointer;" onclick="javascript:tableAdsOrdering('askprice+0','ASC');"><img src="{$IMAGE_ROOT}arrow-up.png" class="ads_noborder ads_top2" alt='Up' /></a>
                           <a style="cursor:pointer;" onclick="javascript:tableAdsOrdering('askprice+0','DESC');"><img src="{$IMAGE_ROOT}arrow-down.png" class="ads_noborder ads_top2" alt='Down' /></a>
                    {/if}
                </td>
                <td align="ceenter">
                    {if $filter_order=="end_date" }
                      {if $filter_order_Dir=="ASC"}
                         <img src="{$IMAGE_ROOT}arrow-up_b.png" class="ads_noborder ads_top2" alt='Up' />
                         <a style="cursor:pointer;" onclick="javascript:tableAdsOrdering('end_date','DESC');"><img src="{$IMAGE_ROOT}arrow-down.png" class="ads_noborder ads_top2" alt='Down' /></a>
                      {else}
                         <a style="cursor:pointer;" onclick="javascript:tableAdsOrdering('end_date','ASC');"><img src="{$IMAGE_ROOT}arrow-up.png" class="ads_noborder ads_top2" alt='Up' /></a>
                         <img src="{$IMAGE_ROOT}arrow-down_b.png" class="ads_noborder ads_top2" alt='Down' />
                      {/if}
                    {else}
                         <a style="cursor:pointer;" onclick="javascript:tableAdsOrdering('end_date','ASC');"><img src="{$IMAGE_ROOT}arrow-up.png" class="ads_noborder ads_top2" alt='Up' /></a>
                         <a style="cursor:pointer;" onclick="javascript:tableAdsOrdering('end_date','DESC');"><img src="{$IMAGE_ROOT}arrow-down.png" class="ads_noborder ads_top2" alt='Down' /></a>
                    {/if}
                </td>               
            </tr>            
            <tr class="adds_myitem_spacer">
                <td colspan="5">&nbsp;</td>
            </tr>            
            {foreach from=$items item=item key=key}
            	{if $item->featured && 	$item->featured!='none'}
            		{assign var=class_featured value="listing-"|cat:$item->featured}
            	{else}
            		{assign var=class_featured value=""}
            	{/if}
            	{cycle assign=class_suffix values="1,2"}
                {include file="elements/list/t_myadds_cell.tpl"}
            {/foreach}
        </table>
        {if !($items|@count>0) }
        	<div class="adds_man_item1">
        	 {jtext text="ADS_NO_ADS"}
        	</div>	
        {/if}
    	<div class="pagination">
    		{$pagination->getPagesLinks()}
    	</div>
    </div>

	
</form>
