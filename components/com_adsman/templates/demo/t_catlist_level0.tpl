{include file='t_javascript_language.tpl'}
{set_css}
{set_js}

<div class="adds_man_list">
	{section name=category loop=$categories}
        <div class="adsman_catcell0">
            <div class="adsman_catcell_inner0">
                <div class="adsman_cattitle">
    					<a href="{if $categories[category]->kids>0}{$categories[category]->link}{else}{$categories[category]->view}{/if}" >
    						{$categories[category]->catname}
    					</a>
    					{if $categories[category]->is_new==1}
							<img src="{$IMAGE_ROOT}new.png" alt="new ads" />
						{/if}
                </div>
                <div class="adsman_catimage">
					    {include file='elements/display_fields.tpl' position='categoryicon' this_add=$categories[category]->id}
                            <a href="{if $categories[category]->kids>0}{$categories[category]->link}{else}{$categories[category]->view}{/if}" >
                                <img src="{$ROOT_HOST}/images/ads_categories/cat_{$categories[category]->id}.jpg" class="ads_noborder" width="150"/>
                            </a>
                </div>
                <div class="addsman_catheader_nav">
    				{if $is_logged_in}
    					{if $categories[category]->watchListed_flag}
    						<img src="{$IMAGE_ROOT}watchlist.png" width="16" class="ads_noborder"/>
    					{/if}
    					<a href="{$categories[category]->link_watchlist}">
    						<img src="{$categories[category]->img_src_watchlist}" style="vertical-align: bottom;" width="16" class="ads_noborder" title="{if $categories[category]->watchListed_flag}{jtext text='ADS_REMOVE_FROM_WATCHLIST'}{else}{jtext text='ADS_ADD_TO_WATCHLIST'}{/if}"/></a>
                        &nbsp;|&nbsp;
    				{/if}
                    <a href="{$categories[category]->view}" title="{jtext text='ADS_VIEW_LISTINGS'}">
                        <img src="{$IMAGE_ROOT}view_listings.gif" class="ads_noborder" alt="{jtext text='ADS_VIEW_LISTINGS'}" /></a>
                    &nbsp;|&nbsp;{jtext text="ADS_SUBCATEGORIES"}: {$categories[category]->kids}&nbsp;|&nbsp;{jtext text="ADS_TOTAL_ADS"}: {$categories[category]->total_nr_ads}
                    {if $categories[category]->subcategories|@count}
                        <br /><span>
		        		{section name=subcategory loop=$categories[category]->subcategories}
                            <a href="{if $categories[category]->subcategories[subcategory]->kids>0}{$categories[category]->subcategories[subcategory]->link}{else}{$categories[category]->subcategories[subcategory]->view}{/if}">{$categories[category]->subcategories[subcategory]->catname}</a>
                            {if $categories[category]->subcategories[subcategory]->is_new==1}
								<img src="{$IMAGE_ROOT}new.png" alt="new ads" />
 							{/if}
                            {if !$smarty.section.subcategory.last},&nbsp;{/if}
                            {if $smarty.section.subcategory.index>=9}<a href="{$categories[category]->link}">[...]</a>{php}break;{/php}{/if}
                        {/section}
                        </span>
                    {/if}
                </div>
            </div>
        </div>
	{/section}
</div>

