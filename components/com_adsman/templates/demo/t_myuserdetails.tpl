{set_css}
{include file='t_javascript_language.tpl'}

{createtab}
{startpane id="content-pane" usecookies=0}


{* Profile Tab *}
{starttab paneid="tab1" text="ADS_MY_PROFILE"}
<div class="add_myprofile_details">
    <div class="componentheading">
            <img src="{$ROOT_HOST}components/com_adsman/templates/demo/image/myprofile.jpg" title="{$page_title}"/> 
            {$page_title}
    </div>
    <form action="{$ROOT_HOST}index.php" method="post" name="adsForm" id="WV-form" enctype="multipart/form-data" class="form-validate" onsubmit="return myValidate(this);">
        <input type="hidden" name="Itemid" value="{$Itemid}" />
        <input type="hidden" name="option" value="{$option}" />
        <input type="hidden" name="task" value="saveUserDetails" />
        
    	<table class="userdetailstable">
        <tr>
            <td>
               <label for="name">{jtext text="ADS_NAME"}:</label>
                    <input class="inputbox required" type="text" id="name" name="name" value="{$user->name}" size="30" />
               <label for="surname">{jtext text="ADS_SURNAME"}:</label>
               	   <input class="inputbox required" type="text" id="surname" name="surname" value="{$user->surname}" size="30" />
            </td>
        </tr>
        <tr><td><hr /></td> </tr>
        <tr>
            <td>
                <p>
                    <label for="address">{jtext text="ADS_ADDRESS"}:</label>
                        <textarea id="address" name="address" class="inputbox">{$user->address}</textarea>
                </p>
                <p>
                <label for="city">{jtext text="ADS_CITY"}:</label>
                    <input class="inputbox" type="text" id="city" name="city" value="{$user->city}" size="30" />
                <label for="state">{jtext text="ADS_STATE"}:</label>
                   <input class="inputbox" type="text" id="state" name="state" value="{$user->state}" size="30" />
                </p>
                <p>
                <label for="country">{jtext text="ADS_COUNTRY"}:</label>
                    {$lists.country}
                </p>
            </td>
        </tr>
        {foreach from=$profiler item=field}
        <tr>
            <td>
                <label for="{$field.field_id}">{$field.field_name}:</label>
                {if $field.help}{infobullet text=$field.help}{/if}
                {$field.field_html}
            </td>
        </tr>
        {/foreach}
    
    	{if $smarty.const.ads_opt_google_key!=""}
            <tr>
                <td>
                    <label>{jtext text="ADS_GOOGLE_X_COORDINATE"}:</label> 
                        <input class="inputbox" type="text" id="googleX" name="googleX" value="{$user->googleX}" size="20" />
                </td>
            </tr>
            <tr>
                <td>
                    <label>{jtext text="ADS_GOOGLE_Y_COORDINATE"}:</label>
                        <input class="inputbox" type="text" id="googleY" name="googleY" value="{$user->googleY}" size="20" />
                </td>
            </tr>
            <tr>
                <td>
               	    <div id="map_canvas"></div>    
                </td>
            </tr>
       	{/if}    
        <tr><td><hr /></td> </tr>
        <tr>
            <td>
                <label for="phone">{jtext text="ADS_PHONE"}:</label>
                    <input class="inputbox" type="text" id="phone" name="phone" value="{$user->phone}" size="50" />
            </td>
        </tr>
        <tr>
            <td>
                <label for="email">{jtext text="ADS_EMAIL"}:</label>
                    <input class="inputbox validate-email" type="text" id="email" name="email" value="{$user->email}" size="50" />
            </td>
        </tr>
   	    </table>
    	<input name="save" value="{jtext text='ADS_SAVE'}" class="back_button" type="submit" />
    </form>
</div>
{endtab}


{* Credits Tab *}
{starttab paneid="tab1" text="ADS_TAB_CREDITS"}
<div class="add_myprofile_credits">
    {if $lists.credits|@count}
        <span class="add_myprofile_balance">{jtext text="ADS_ACCOUNT_BALANCE"}:&nbsp;{$lists.credits[0]->credits_no|default:0}&nbsp;{jtext text="ADS_CREDITS"}</span>
    {/if}
    <hr />
    {if $lists.packages|@count}
		<table class="add_package_list">
		<tr>
		    <th>{jtext text="ADS_CREDITS"} {jtext text="ADS_PACKAGES"}</th>
		</tr>
		<tr>
		    <td>
				{foreach from=$lists.packages item=pac key=key}
				<div class="add_package">
					<table>
					<tr>
    			        <td>
        					{if $pac->price != 0}
            					<a href="index.php?option=com_adsman&task=buy_package&package={$pac->name}&amp;Itemid={$Itemid}" >
            						{jtext text="ADS_BUY"}
            					</a>
        					{else}
            					<a href="index.php?option=com_adsman&task=free_package&package={$pac->name}&amp;Itemid={$Itemid}" >
                                    {jtext text="ADS_GET_FREE_CREDITS"}
                                </a>
        					{/if}
    					</td>
					</tr>
					<tr>
						<td><strong>{$pac->name}</strong></td>
					</tr>
					<tr>
						<td class="price">{$pac->price} <span class="currency">{$pac->currency}</span></td>
					</tr>
					<tr>
						<td class="price">{$pac->credits} {jtext text="ADS_CREDITS"}</td>
					</tr>
					{if $currency_maincredit == $pac->currency}
    					<tr>
    						<td>
                                {jtext text="ADS_ECONOMY"}{math equation="(( x * y ) - z)" x=$price_maincredit y=$pac->credits z=$pac->price format="%.2f"}
                                {$currency_maincredit}
    						    {jtext text="ADS_ECONOMY_PER_PACKAGE"}
    						</td>
    					</tr>
					{/if}
					<tr>
						<td>
                            <a href="index.php?option=com_adsman&task=buy_package&package={$pac->name}&amp;Itemid={$Itemid}">
                                {package_image package=$pac->name}
                            </a>
						</td>
					</tr>
					</table>
                </div>
				{/foreach}
		    </td>
		</tr>
    	</table>
    {/if}
</div>
{endtab}


{endpane}