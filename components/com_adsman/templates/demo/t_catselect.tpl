{set_css}
{include file='t_javascript_language.tpl'}

<table class="add_newadd_header">
    <tr>
        <td class="add_newadd_header_selected">
            Step 1:<br />
            {jtext text="ADS_SELECT_CATEGORY"}
        </td>
        <td>
            Step 2:<br />
            {jtext text="ADS_ADD_INFO"}
        </td>
    </tr>
</table>

<h3 class="add_newadd_header">{jtext text="ADS_PAGE_CATEGORIES"}</h3>

<form name="category_select" action="{$ROOT_HOST}index.php" method="get">
	<input type="hidden" name="option" value="com_adsman" />
	<input type="hidden" name="task" value="edit" />
	<input type="hidden" name="category_hit" value="1" />
    <input type="hidden" name="category" value="0" />
    <input type="hidden" name="Itemid" value="{$Itemid}" />
    {if $id}
        <input type="hidden" name="id" value="{$id}" />
    {/if}    
	<div class="category_select_toolbox">
		<a href="javascript:selectCategory()">
			<img src="{$ROOT_HOST}components/com_adsman/templates/demo/image/choose_category.gif" class="ads_noborder" alt="Select" style="vertical-align:middle;"/> 
			<div class="button_up">{jtext text="ADS_SELECT_CATEGORY"}</div>
		</a>
	</div>
    <div class="clr"></div>
    
	<table class="adds_categories_select" class="ads_noborder ads_table" width="60%">
    <tbody>
        <tr>
            <th>{jtext text="ADS_CATEGORY"}: <span id="current_cat"></span></th>
        </tr>
        <tr>
        <td>
            <ul id="category_list">
        	{foreach from=$categories key=key item=category}
                {if $category.parent==0 && $category.id>0}
                	<li style="background-color:{cycle values="#EEEEEE,#E0E0E0"}">
        				<span class="adds_category_link">
                            <a href="javascript:selectcat({$category.id})">
        					   {$category.catname}
                            </a>
        				</span>
                	</li>
                {/if}
        	{/foreach}
            </ul>
        </td>
        </tr>
    </tbody> 
	</table>
</form>

<script type="text/javascript">
	/*var CatList = Array(10000);
	var CatParents = Array(10000);
    var CatLength={$categories|@count};
	{*foreach from=$categories key=key item=category}
		CatList[{$category.id}] = '{$category.catname|escape}';
		CatParents[{$category.id}] = '{$category.parent}';
	{/foreach*}*/
    </script>
