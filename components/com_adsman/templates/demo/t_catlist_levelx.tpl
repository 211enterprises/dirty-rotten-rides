{include file='t_javascript_language.tpl'}
{set_css}
{set_js}
<div class="addsman_catheader">
<table width="100%" class="ads_table">
<tr>
    <td valign="top" width="10%"><img src="{$ROOT_HOST}/images/ads_categories/cat_{$current_category->id}.jpg" class="ads_noborder" height="75"/></td>
    <td valign="top">
        <div style="height:75px;position:relative;">
            {jtext text="ADS_CATEGORY"}: {$current_category->catname}
                <a href="{$current_category->view}">
                   <img src="{$IMAGE_ROOT}view_listings.gif" class="ads_noborder" alt="{jtext text='ADS_VIEW_LISTINGS'}" /></a>
                <span style="font-size:12px;">({$current_category->nr_ads_current} {jtext text="ADS_ADS"})</span>
                <br />
            {if $current_category->description}
                <span class="add_cat_description">{$current_category->description}</span><br />
            {/if}
            <div class="addsman_catheader_nav">
                <span style="font-size:12px;">
                <!--<a href="{$current_category->view}">
                <img src="{$IMAGE_ROOT}view_listings.gif" class="ads_noborder" alt="{jtext text='ADS_VIEW_LISTINGS'}" /></a>-->
                {jtext text="ADS_SUBCATEGORIES"}:  {$categories|@count} &nbsp;|&nbsp; {jtext text="ADS_TOTAL_ADS"}: {$current_category->nr_ads}
                </span>
            </div>
        </div>
    </td>
</tr>
</table>
</div>
<div class="adds_man_list">
	{section name=category loop=$categories}
        <div class="adsman_catcell">
            <div class="adsman_catcell_inner">
                <div class="adsman_cattitle">
    					<a href="{if $categories[category]->kids>0}{$categories[category]->link}{else}{$categories[category]->view}{/if}" >
    						{$categories[category]->catname}
    					</a>
                		{if $categories[category]->is_new==1}
							<img src="{$IMAGE_ROOT}new.png" alt="new ads" />
						{/if}
                    <span style="font-size:12px;">({$categories[category]->nr_a} {jtext text="ADS_ADS"})</span>
				</div>		
                <div class="adsman_catimage">
					{include file='elements/display_fields.tpl' position='categoryicon' this_add=$categories[category]->id}
                        <a href="{if $categories[category]->kids>0}{$categories[category]->link}{else}{$categories[category]->view}{/if}" >
                            <img src="{$ROOT_HOST}/images/ads_categories/cat_{$categories[category]->id}.jpg" width="150"/>
                        </a>
                </div>
                <div class="addsman_catheader_nav">
    				{if $is_logged_in}
    					{if $categories[category]->watchListed_flag}
    						<img src="{$IMAGE_ROOT}watchlist.png" width="16" />
    					{/if}
    					<a href="{$categories[category]->link_watchlist}">
    						<img src="{$categories[category]->img_src_watchlist}" style="vertical-align: bottom;" width="16"  
                                title="{if $categories[category]->watchListed_flag}{jtext text='ADS_REMOVE_FROM_WATCHLIST'}{else}{jtext text='ADS_ADD_TO_WATCHLIST'}{/if}"/>
                        </a>
    				{/if}
    				<a href="{$categories[category]->view}" title="{jtext text='ADS_VIEW_LISTINGS'}">
    					<img src="{$IMAGE_ROOT}view_listings.gif" alt="{jtext text='ADS_VIEW_LISTINGS'}" />
                    </a>
                    |&nbsp;{jtext text="ADS_SUBCATEGORIES"}: {$categories[category]->kids}&nbsp;|&nbsp;{jtext text="ADS_TOTAL_ADS"}: {$categories[category]->total_nr_ads}
                    {if $categories[category]->subcategories|@count}
                        <br /><span>
        				{section name=subcategory loop=$categories[category]->subcategories}
                            <a href="{if $categories[category]->subcategories[subcategory]->kids>0}{$categories[category]->subcategories[subcategory]->link}{else}{$categories[category]->subcategories[subcategory]->view}{/if}">{$categories[category]->subcategories[subcategory]->catname}</a>{if !$smarty.section.subcategory.last},&nbsp;{/if}
                        {/section}
                        </span>
                    {/if}                                					
                </div>
            </div>
        </div>
	{/section}
</div>
