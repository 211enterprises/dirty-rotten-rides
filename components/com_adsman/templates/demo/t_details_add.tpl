{include file='t_javascript_language.tpl'}
{set_css}
{include file='elements/details/t_details_add_plugins.tpl'}


<!-- [+] Title -->
<div class="add_detail_title">
    <!-- [+]TOOLBAR -->{include file='elements/details/t_details_add_toolbar.tpl'}<!-- [-]TOOLBAR -->
    {$add->title}
    {if $add->status==0}<span>({jtext text="ADS_UNPUBLISHED"})</span>{/if}
    {if $add->expired}
        <span class='expired'>{jtext text="ADS_EXPIRED"}({$add->end_date_text})</span>
    {/if}
    {if $add->closed}
        <span class="closed">{jtext text="ADS_AD_CLOSED"} {if $add->closed_date_text} ({$add->closed_date_text}){/if}</span>
    {/if}
</div>
<!-- [-] Title -->
<!-- [+] Default Page Position -->
{include file='elements/display_fields.tpl' position='defdetailspageheader'}
<!-- [-] Default Page Position -->
	
	
<!-- [+]Add Container -->
<div class="add_detail_container">
  <table width="100%" cellpadding="0" cellspacing="0" class="ads_noborder">
    <tr>
        <td valign="top">
          <table width="100%">
            <tr>
                <td width="{$smarty.const.ads_opt_medium_width+25}" valign="top">
                    {if $add->gallery}
                        {$add->gallery}
                    {else}
                        <img src="{$IMAGE_ROOT}no_image.png" border="0" alt=""/>
                    {/if}
                    <ul class="add_detail_stats">
                        {if $add->catname}
                            <li>
                                <label>{jtext text="ADS_CATEGORY"}:</label>
                                &gt;&nbsp;<a href="{$add->links.filter_cat}">{$add->catname}</a>
                            </li>
                        {/if}
                        <li>
                            <label>{jtext text="ADS_HITS"}:</label>
                            {$add->hits}
                        </li>
                        <li>
                            <label>{jtext text="ADS_START_DATE"}:</label>
                            {printdate date=$add->start_date use_hour=0}
                        </li>
                       {if !($add->is_my_add && $add->close_by_admin == 0)}
                            <li>
                                <label>{jtext text="ADS_REPORT"}:</label>
                               <a class="modal" href="{$add->links.report}" {literal} rel="{handler: 'iframe', size: {x: 600, y: 400}}" {/literal}>
                                   <img src="{$IMAGE_ROOT}report.png" title="{jtext text='ADS_REPORT'}" height="20" alt="{jtext text='ADS_REPORT'}" />
                               </a>
                            </li>
                       {/if}
                    </ul>
                </td>
                <td>
                    <div class="add_detail_fields">
                        <ul>
                            {if $add->askprice}
                                {if is_numeric($add->askprice)}
                                    <li><span class="price"><label>{jtext text="ADS_PRICE"}:</label>{$add->askprice|string_format:"%.2f"}</span>&nbsp;<span class="currency">{$add->currency_name}</span></li>
                                {else}
                                    <li><span class="price">{$add->askprice}</span></li>
                                {/if}
                            {/if}
                            {if $add->ad_city}
                                <li><label>{jtext text="ADS_AD_CITY"}:</label>{$add->ad_city}</li>
                            {/if}
                            {if $add->ad_postcode}
                                <li><label>{jtext text="ADS_AD_POSTCODE"}:</label>{$add->ad_postcode}</li>
                            {/if}
                            {if !$add->closed}
                                {if $smarty.const.ads_opt_enable_countdown}
                                    <li><label>{jtext text="ADS_EXPIRES_IN"}:</label><span id="time1">{$add->countdown}</span></li>
                                {/if}
                            {/if}
{* Demo Custom Fields *}                            
                            {if $add->carmake}
                                <li><label>Make:</label> {$add->carmake}</li>
                            {/if}
                            {if $add->carmodel}
                                <li><label>Model:</label>{$add->carmodel}</li>
                            {/if}
                            {if $add->roomsnr}
                                <li><label>Nr. Rooms:</label>{$add->roomsnr}</li>
                            {/if}
                            {if $add->year}
                                <li><label>Year:</label>{$add->year}</li>
                            {/if}
                            {if $add->carmileage}
                                <li><label>Mileage:</label>{$add->carmileage}</li>
                            {/if}
                        </ul>
                    </div>
                    <div class="add_detail_socialtoolbar">
                       {if !$add->is_my_add}
                            <span class="add_socialbutton">
                               {if $add->favorite==0 }
                                   <a href='{$add->links.add_to_favorite}'>
                                       <img src="{$IMAGE_ROOT}f_favoritelist_1_24.png" class="ads_noborder detail_img_social" title="{jtext text='ADS_ADD_TO_FAVORITE'}" alt="{jtext text='ADS_ADD_TO_FAVORITE'}"  />
                                   </a>
                               {else}
                                   <a href='{$add->links.del_from_favorite}'>
                                       <img src="{$IMAGE_ROOT}f_favoritelist_0_24.png" class="ads_noborder detail_img_social" title="{jtext text='ADS_REMOVE_FROM_FAVORITE'}" alt="{jtext text='ADS_REMOVE_FROM_FAVORITE'}"  />
                                   </a>
                               {/if}
                           </span>
                       {/if}
                       <span class="add_socialbutton">
                           {if $add->more_pictures}
                                 <img src="{$IMAGE_ROOT}morepics_on_24.png" class="ads_noborder detail_img_social" title="{jtext text='ADS_MORE_PICTURES'}" alt="{jtext text='ADS_MORE_PICTURES'}" />
                           {else}
                                 <img src="{$IMAGE_ROOT}morepics_off_24.png" class="ads_noborder detail_img_social" title="{jtext text='ADS_NO_MORE_PICTURES'}" alt="{jtext text='ADS_NO_MORE_PICTURES'}" />
                           {/if}
                       </span>
                       {if $smarty.const.ads_allow_atachment=="1" }
                           <span class="add_socialbutton">
                               {if $add->atachment}
                                  <a target="_blank" href="index.php?option=com_adsman&view=adsman&task=downloadfile&id={$add->id}">
                                      <img src="{$IMAGE_ROOT}downloads_on_24.png" title="{jtext text='ADS_HAS_DOWNLOADS'}" alt="{jtext text='ADS_HAS_DOWNLOADS'}" class="ads_noborder detail_img_social" />
                                  </a>
                               {else}
                                      <img src="{$IMAGE_ROOT}downloads_off_24.png" title="{jtext text='ADS_HAS_NO_DOWNLOADS'}" alt="{jtext text='ADS_HAS_NO_DOWNLOADS'}" class="ads_noborder detail_img_social" />
                               {/if}
                           </span>
                       {/if}
                       <span class="add_socialbutton">
                           <a class="modal" href="index.php?option=com_adsman&view=adsman&amp;task=tellFriend_show&id={$add->id}&tmpl=component" {literal} rel="{handler: 'iframe', size: {x: 500, y: 300}}" {/literal}>
                               <img src="{$ROOT_HOST}components/com_adsman/templates/demo/image/tell_a_friend.png" alt="{jtext text='ADS_TELL_FRIEND'}" title="{jtext text='ADS_TELL_FRIEND'}" class="ads_noborder detail_img_social" />
                           </a>
                       </span>
                       {include file='elements/details/t_details_add_social.tpl'}
                    </div>
                    <div class="add_userdetails">
                        {include file='elements/details/t_details_add_user.tpl'}
                    </div>

                </td>
            </tr>
          </table>
          {if $add->links.tags}
            {jtext text="ADS_TAGS"}: {$add->links.tags}
          {/if}
        </td>
    </tr>

    <tr valign="top">
        <td>
            <!-- [+] Tabbed Panes -->
            {include file='elements/details/t_details_add_tab.tpl'}
            <!-- [-] Tabbed Panes -->
        </td>
    </tr>
  </table>
</div>
<!-- [-]Add Container -->

