{set_css}
<div class="componentheading">{jtext text="ADS_PAY_PAYMENTITEM_DESC_FEATURED"}</div>

<form method="POST" action="{$action}" name="purchase_item">
<input name="option" type="hidden" value="com_adsman" />
<input name="task" type="hidden" value="purchase" />
<input type="hidden" name="Itemid" value="{$Itemid}" />
<input name="paymenttype" type="hidden" value="{$paymenttype}" />
<input name="act" type="hidden" value="checkout" />
<input name="return_url" type="hidden" value="{$return_url}" />
<div class="add_featured_purchase ads_noborder">
	{section name=itemloop loop=$pricing}
		{assign var=price value=`$pricing[itemloop]`}
		{if $price->itemname|substr:0:8 == 'featured' }
			{assign var=txt value=$price->itemname|substr:9}
			{if $selected_type==$txt}
				{assign var=sel value="checked='yes'"}
			{else}
				{assign var=sel value=""}
			{/if}

            <input type="radio" name="itemname" value="{$price->itemname}" {$sel} />
            {assign var=txt value="adsman_payment_featured_"|cat:$txt}
            {assign var=txt2 value=$txt|cat:"_help"}
            <img src="{$ROOT_HOST}components/com_adsman/templates/demo/image/{$price->itemname}.jpg" class="ads_noborder" title="{$smarty.const.$txt}" alt="{$smarty.const.$txt}" height="20" style="vertical-align:middle;"/>
            <span>{$smarty.const.$txt}</span>
            <br/>
		{/if}
	{/section}
    <br />
    <br />
	<input type="submit" name="submit" value="{jtext text='ADS_PAY_PAYMENT_PURCHASE'}" />
 </div>
</form>