{include file='t_javascript_language.tpl'}
{set_css}
{set_js}
{assign var=cat value=$smarty.request.cat}
<!--<div class="componentheading">{$page_title}</div>-->
<span>{$category_pathway}</span>

	{if $categories|@count<=0}
		<h2>{jtext text="ADS_NO_CATEGORIES_DEFINED"}</h2>
    {elseif $cat}
        {include file="t_catlist_levelx.tpl"}      
    {else}
        {include file="t_catlist_level0.tpl"}      
	{/if}
<br style="clear:both;" />