<div class="adds_man_list ads_table">

    {foreach from=$filters item=item key=key}
        <input type="hidden" id="{$key}_filter" name="{$key}" value="{$item}" />
    {/foreach}
    {if $category_filter}
        <div class="ads_category_navigation{$pageclass_sfx}">
            {$category_filter->navigation}<br />
            <h3 class="ads_category_header">{jtext text="ADS_CATEGORY"}: {$category_filter->catname}</h3>
            {if $category_filter->description}
            <span class="ads_category_description">{$category_filter->description}</span>
            <br/>
            <br/>
            {/if}
        </div>
    {/if}
    {if $HTMLfilters|@count >0}
        <div>
            {foreach from=$HTMLfilters item=item key=key}
            <span class="add_filter{$pageclass_sfx}">
            {if $HTMLfilters|@count>1}
            <a href="index.php?option=com_adsman&view=adsman&task={$task}&remove={$key}">
                <img style="vertical-align:bottom;" width="16" src="{$IMAGE_ROOT}remove_filter1.png" border="0" title="{jtext text='ADS_REMOVE_FILTER'}" alt="{jtext text='ADS_REMOVE_FILTER'}" onmouseover="this.src='{$IMAGE_ROOT}remove_filter2.png';" onmouseout="this.src='{$IMAGE_ROOT}remove_filter1.png';" />
            </a>
            {/if}
            {$item}
            </span>
            {/foreach}
            &nbsp;&nbsp;
            <a href="{$resetFilters}"><img src="{$IMAGE_ROOT}remove_filter1_all.png" border="0" title="{jtext text='ADS_REMOVE_FILTERS'}" alt="{jtext text='ADS_REMOVE_FILTERS'}" onmouseover="this.src='{$IMAGE_ROOT}remove_filter2_all.png';" onmouseout="this.src='{$IMAGE_ROOT}remove_filter1_all.png';" /></a>
            <br /><br />

        </div>
    {/if}
    <table width="100%" cellpadding="0" cellspacing="0" class="ads_noborder">
      <tr>
        <td valign="bottom" class="add_list_header">
            <div style="float: left; padding-right: 25px;">
                <a style="cursor:pointer;" onclick="javascript:viewStyleAds('list');"><span class="text_header_small">{jtext text="ADS_VIEW_LISTVIEW"}</span></a> |
                <a style="cursor:pointer;" onclick="javascript:viewStyleAds('grid');"><span class="text_header_small">{jtext text="ADS_VIEW_GRIDVIEW"}</span></a>
           </div>
            <div style="float: right; padding-right: 5px;">
                 <span class="text_header_small">{$pagination->getLimitBox()} </span>
            </div>
        </td>
        <td valign="bottom">
            <div class="header_title_sort" align="center">
                <a style="cursor:pointer;" onclick="javascript:tableAdsOrdering('title','{$reverseorder_Dir}');"><span>{jtext text="ADS_TITLE"}</span></a>
            </div>
        </td>
        <td valign="bottom" align="center" style="padding-left: 2px; padding-right: 2px;">
            <div class="header_title_sort" align="center">
                <a style="cursor:pointer;" onclick="javascript:tableAdsOrdering('askprice+0','{$reverseorder_Dir}');"><span>{jtext text="ADS_PRICE"}</span></a>
            </div>
        </td>
        <td valign="bottom">
            <div class="header_title_sort" align="center">
                {if $smarty.const.ads_opt_sort_date == 'start'}
                    <a style="cursor:pointer;" onclick="javascript:tableAdsOrdering('start_date','{$reverseorder_Dir}');"><span>{jtext text="ADS_POSTED_ON"}</span></a>
                {else}
                    <a style="cursor:pointer;" onclick="javascript:tableAdsOrdering('end_date','{$reverseorder_Dir}');"><span>{jtext text="ADS_ENDING_IN"}</span></a>
                {/if}
            </div>
        </td>
      </tr>
      <tr>
        <td style="background-color:#DDDDDD;height:1px;" colspan="5"></td>
      </tr>
      <tr>
          <td valign="top" class="add_list_header">
              <div style="float: left;">
              {if $task=="myadds"}
                   <a style="cursor:pointer;" onclick="javascript:viewArchiveAds('active','myadds');"><span class="text_header_small {if $archive=='active'}active{/if}">{jtext text="ADS_ACTIVE"}</span></a> |
                   <a style="cursor:pointer;" onclick="javascript:viewArchiveAds('expired','myadds');"><span class="text_header_small {if $archive=='expired'}active{/if}">{jtext text="ADS_EXPIRED"}</span></a> |
                   <a style="cursor:pointer;" onclick="javascript:viewArchiveAds('archive','myadds');"><span class="text_header_small {if $archive=='archive'}active{/if}">{jtext text="ADS_ARCHIVE"}</span></a> |
                   <a style="cursor:pointer;" onclick="javascript:viewArchiveAds('unpublish','myadds');"><span class="text_header_small {if $archive=='unpublish'}active{/if}">{jtext text="ADS_UNPUBLISHED"}</span></a>
               {else}
                   <a style="cursor:pointer;" onclick="javascript:viewArchiveAds('active','{$task}');"><span class="text_header_small {if $archive=='active'}active{/if}">{jtext text="ADS_ACTIVE"}</span></a> |
                   <a style="cursor:pointer;" onclick="javascript:viewArchiveAds('expired','{$task}');"><span class="text_header_small {if $archive=='expired'}active{/if}">{jtext text="ADS_EXPIRED"}</span></a> |
                   <a style="cursor:pointer;" onclick="javascript:viewArchiveAds('archive','{$task}');"><span class="text_header_small {if $archive=='archive'}active{/if}">{jtext text="ADS_ARCHIVE"}</span></a>
               {/if}
              </div>
          </td>
    <td align="center">
         {if $filter_order=="title" }
             {if $filter_order_Dir=="ASC"}
                <img src="{$IMAGE_ROOT}arrow-up_b.png" class="ads_noborder ads_top2" alt='Up' />
                <a style="cursor:pointer;" onclick="javascript:tableAdsOrdering('title','DESC');"><img src="{$IMAGE_ROOT}arrow-down.png" class="ads_noborder ads_top2" alt='Down' /></a>
             {else}
                <a style="cursor:pointer;" onclick="javascript:tableAdsOrdering('title','ASC');"><img src="{$IMAGE_ROOT}arrow-up.png" class="ads_noborder ads_top2" alt='Down' /></a>
                <img src="{$IMAGE_ROOT}arrow-down_b.png" class="ads_noborder ads_top2" />
             {/if}
         {else}
                <a style="cursor:pointer;" onclick="javascript:tableAdsOrdering('title','ASC');"><img src="{$IMAGE_ROOT}arrow-up.png" class="ads_noborder ads_top2" alt='Up' /></a>
                <a style="cursor:pointer;" onclick="javascript:tableAdsOrdering('title','DESC');"><img src="{$IMAGE_ROOT}arrow-down.png" class="ads_noborder ads_top2" alt='Down' /></a>
         {/if}
    </td>


    <td align="center">
        {if $filter_order=="askprice+0" }
            {if $filter_order_Dir=="ASC"}
               <img src="{$IMAGE_ROOT}arrow-up_b.png" class="ads_noborder ads_top2" alt='Up' />
               <a style="cursor:pointer;" onclick="javascript:tableAdsOrdering('askprice+0','DESC');"><img src="{$IMAGE_ROOT}arrow-down.png" class="ads_noborder ads_top2" alt='Down' /></a>
            {else}
               <a style="cursor:pointer;" onclick="javascript:tableAdsOrdering('askprice+0','ASC');"><img src="{$IMAGE_ROOT}arrow-up.png" class="ads_noborder ads_top2" alt='Up' /></a>
               <img src="{$IMAGE_ROOT}arrow-down_b.png" class="ads_noborder ads_top2" alt='Down' />
            {/if}
        {else}
               <a style="cursor:pointer;" onclick="javascript:tableAdsOrdering('askprice+0','ASC');"><img src="{$IMAGE_ROOT}arrow-up.png" class="ads_noborder ads_top2" alt='Up' /></a>
               <a style="cursor:pointer;" onclick="javascript:tableAdsOrdering('askprice+0','DESC');"><img src="{$IMAGE_ROOT}arrow-down.png" class="ads_noborder ads_top2" alt='Down' /></a>
        {/if}
    </td>
    <td align="center">
          {if $smarty.const.ads_opt_sort_date == 'end'}
              {if $filter_order=="end_date" }
                {if $filter_order_Dir=="ASC"}
                   <img src="{$IMAGE_ROOT}arrow-up_b.png" class="ads_noborder ads_top2" alt='Up' />
                   <a style="cursor:pointer;" onclick="javascript:tableAdsOrdering('end_date','DESC');"><img src="{$IMAGE_ROOT}arrow-down.png" class="ads_noborder ads_top2" alt='Down' /></a>
                {else}
                   <a style="cursor:pointer;" onclick="javascript:tableAdsOrdering('end_date','ASC');"><img src="{$IMAGE_ROOT}arrow-up.png" class="ads_noborder ads_top2" alt='Up' /></a>
                   <img src="{$IMAGE_ROOT}arrow-down_b.png" class="ads_noborder ads_top2" alt='Down' />
                {/if}
              {else}
                   <a style="cursor:pointer;" onclick="javascript:tableAdsOrdering('end_date','ASC');"><img src="{$IMAGE_ROOT}arrow-up.png" class="ads_noborder ads_top2" alt='Up' /></a>
                   <a style="cursor:pointer;" onclick="javascript:tableAdsOrdering('end_date','DESC');"><img src="{$IMAGE_ROOT}arrow-down.png" class="ads_noborder ads_top2" alt='Down' /></a>
              {/if}

          {else}
              {if $filter_order=="start_date" }
                {if $filter_order_Dir=="ASC"}
                   <img src="{$IMAGE_ROOT}arrow-up_b.png" class="ads_noborder ads_top2" alt='Up' />
                   <a style="cursor:pointer;" onclick="javascript:tableAdsOrdering('start_date','DESC');"><img src="{$IMAGE_ROOT}arrow-down.png" class="ads_noborder ads_top2" alt='Down' /></a>
                {else}
                   <a style="cursor:pointer;" onclick="javascript:tableAdsOrdering('start_date','ASC');"><img src="{$IMAGE_ROOT}arrow-up.png" class="ads_noborder ads_top2" alt='Up' /></a>
                   <img src="{$IMAGE_ROOT}arrow-down_b.png" class="ads_noborder ads_top2" alt='Down' />
                {/if}
              {else}
                   <a style="cursor:pointer;" onclick="javascript:tableAdsOrdering('start_date','ASC');"><img src="{$IMAGE_ROOT}arrow-up.png" class="ads_noborder ads_top2" alt='Up' /></a>
                   <a style="cursor:pointer;" onclick="javascript:tableAdsOrdering('start_date','DESC');"><img src="{$IMAGE_ROOT}arrow-down.png" class="ads_noborder ads_top2" alt='Down' /></a>
              {/if}
          {/if}
    </td>
  </tr>
</table>
{*
 {$custom_filters.db_name.field_name} {$custom_filters.db_name.field_html}
*}
</div>