<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/


// no direct access
defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Ads manager Component
 *
 * @package		Ads manager
 */
class adsViewmaps extends JView
{
	
	function listadd(){
		$this->display();
	}
	
	function display(){
		
		$mainframe = JFactory::getApplication();
		
		$smarty 	= AdsUtilities::SmartyLoaderHelper();
		// Get the page/component configuration
		$params = &$mainframe->getParams();
		
		JHTML::_("behavior.mootools");
		
		$cat = new JTheFactoryCategoryObj();
		$lists["category"] = $cat->makeCatTreeVariant("category",'onchange="gmap_refreshAds();"',350);
		
		$lists["radius_units"] = JHtml::_("adsmanmap.radius");	
		$lists["maps_limit"] = JHtml::_("adsmanmap.limitbox");	
		
		$layout = JRequest::getVar('layout');
		
		if ($params->get('show_page_title', 1)){
			$page_title = $this->escape($params->get('page_title'));
			if($page_title==""){
				$page_title = JText::_('ADS_PAGE_LISTADDS');
			}
			$smarty->assign("page_title" ,  $page_title);
		}
		
		$smarty->assign("lists",$lists);

        JHtml::_("adsmanmap.js","map_canvas","listmap","gmap_refreshAds();");
        JHtml::_("adsmanmap.js","map_canvas_search","searchmap");

        if(!isset($layout))
			$smarty->display("maps/t_listadds.tpl");
		else 	
			$smarty->display("maps/t_search.tpl");
		parent::display();
	}

}
