<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/



// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

class adsViewuser extends JView
{
	function display($tpl = null)
	{
		switch($tpl){
			case "myuserdetails":$this->myuserdetails();break;
			case "userdetails":$this->userdetails();break;
		}
	}
	function myuserdetails(){
		
		$Itemid = JRequest::getVar('Itemid', 0, 'REQUEST', 'integer');
		
		$app = JFactory::getApplication();
		// Get the page/component configuration
		$params = &$app->getParams();
		$smarty = AdsUtilities::SmartyLoaderHelper();
		$database = JFactory::getDbo();
		$my       = JFactory::getUser();
		
		if ($my->id!="") {
			$user = new JAdsUsers($database);
			$user->getUserDetails($my->id);
			$lists = array(); 
			$lists["country"] = AdsUtilities::makeCountryList($user->country_id,null,205);
		}
		
		$sql = "SELECT c.credits_no, c.userid FROM #__ads_user_credits c "
			. " WHERE c.userid='$my->id' ";
	
		$database->setQuery( $sql );
		$lists["credits"] = $database->loadObjectList();
		
		$Fi = & FactoryFieldsFront::getInstance();
		$fields = $Fi->getHTMLFields("user_profile", $my->id,$my,0);

		// start Deprecated; kept for old templates
		if( count($fields)>0 ){
			$vcf = array();
			foreach ($fields as $fi){
				if($fi["compulsory"]==1 || $fi["validate_type"]!=""){
					$vcf[] = "'".$fi["field_id"]."'";
				}
			}
			$lists["validate_custom_fields"] = " new Array(".implode(",",$vcf).")";
			$lists["validate_custom_fields_count"] = count($vcf);
		}else {
			$lists["validate_custom_fields"] = " new Array()";
			$lists["validate_custom_fields_count"] = "0";
		}
		// end Deprecated; kept for old templates
		
		JHTML::_('behavior.formvalidation');
		JHTML::script("validator.js",'components/com_adsman/js/validator/');
		
		$smarty->assign("profiler", $fields);
		
	    $lists["packages"] = price_packages_DBHelper::getFrontPackages();
	    $package_image_path = JPATH_ROOT.DS.'images'.DS.'ads'.DS.'packages'.DS;

	    $lists["featured_image"] = price_pay_image_DBHelper::getFrontPayImages();
		$lists['cancel_form'] = JRoute::_( 'index.php?option=com_adsman&amp;view=adsman&amp;task=listadds&amp;Itemid='. $Itemid );
	    
		$smarty->assign("lists", $lists);
		$smarty->assign("user", $user);
		
		if ($params->get('show_page_title', 1)){
			$page_title = $this->escape($params->get('page_title'));
			if($page_title==""){
				$page_title = JText::_('ADS_PAGE_EDIT_MY_PROFILE');
				//adsman_page_edit_user_details;
			}
			$smarty->assign("page_title" ,  $page_title);
		}
        if($user->googleX && $user->googleY ){
            $mapmarker="{pointTox:$user->googleX,pointToy:$user->googleY}";
        }else{
            $mapmarker="{pointTox:".(is_float(ads_opt_googlemap_defx)?ads_opt_googlemap_defx:"0").",pointToy:".(is_float(ads_opt_googlemap_defy)?ads_opt_googlemap_defy:"0")."}";
        }
        JHTML::_("adsmanmap.js","map_canvas","listmap","gmap_centermap($mapmarker,'listmap',true);");
                
		
		$payOb = JTheFactoryPaymentLib::getInstance();
    	$payOb->loadAvailabeItems();
    	
    	$pricings = $payOb->price_plugins;
		$price_maincredit = $pricings['price_maincredit']->price;
		$currency_maincredit = $pricings['price_maincredit']->currency;
		
		$smarty->assign('price_maincredit',$price_maincredit);	
		$smarty->assign('currency_maincredit',$currency_maincredit);
		$smarty->assign('package_img_path',$package_image_path);

		$smarty->display("t_myuserdetails.tpl");
	}
	
	function userdetails() {
		$smarty = AdsUtilities::SmartyLoaderHelper();
		$database = JFactory::getDBO();
		$id = JRequest::getVar('id',null);

		if($id!=""){
			$user = new JAdsUsers($database);
			$user->load($id);
			
			$user->email = AdsUtilities::cloack_email($user->email);
			$lists = array(); 
			$lists["country"] = $user->getCountry($user->country);
			if(isset($lists["country"]->name))
				$lists["country"] = $lists["country"]->name;
			else
				$lists["country"] = null;
			
			$positions = JTheFactoryHelper::loadPositionedFields($user, "t_userdetails.tpl","#__ads_users");

            // custom fields user_profile
            $Fi = & FactoryFieldsFront::getInstance();
            $user_fields = $Fi->getHTMLFields("user_profile", $id, $user,0 );

           // ob_end_clean();
            if(count($user_fields)){
                $smarty->assign("profiler", $user_fields);
            }
            if($user->googleX && $user->googleY ){
                $mapmarker="{pointTox:$user->googleX,pointToy:$user->googleY}";
                JHTML::_("adsmanmap.js","map_canvas","listmap","gmap_centermap($mapmarker,'listmap',false);");
            }
                    

		}
		
		$smarty->assign("positions",  $positions );
		$smarty->assign("lists", $lists);
		$smarty->assign("user", $user);
		$smarty->display("t_userdetails.tpl");
	}
	
}
?>