<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/


// no direct access
defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Ads manager Component
 *
 * @package		Ads manager
 */
class adsViewadsman extends JView
{

	function listadd($myadds=null, $favorites=null) {
		
		$app 	= JFactory::getApplication();
		$doc	= JFactory::getDocument();
		$path 	= $app->getPathway();
        $Itemid = JRequest::getVar('Itemid', 0, 'REQUEST', 'integer');

		$my = JFactory::getUser();
		if ($my->block == 1) return;
		
		$task_adds = null;
		
		// Get the page/component configuration
		$params   = $app->getParams();

		$smarty   = AdsUtilities::SmartyLoaderHelper();
		$config   = JFactory::getConfig();
		$uri 	  = JFactory::getURI();
		$database = JFactory::getDBO();

		$task	= JRequest::getCmd( 'task' );
		if(!$task)
			$task	= JRequest::getCmd( 'layout', 'listadds' );
			

		if ( $myadds && !$favorites ){
			$items	= $this->get( 'MyAdds');
			$task_adds = "myadds";

		}elseif($favorites){
			
			$items	= $this->get( 'Favorites');
			$task_adds = "favorites";

		}else{
			$items	= $this->get( 'Adds');
			$task_adds = "listadds";
		}

		//$total 		=& $this->get( 'Total');
		$listView 	= $this->get( 'listView');
		$filters 	= $this->get( 'CurrentFilters');
		$pagination = $this->get( 'Pagination');
		$positions = array();
		
		$cfields_translations = FactoryFieldsFront::getCustomFieldsOptions();
			
		$gallery = AdImageUtilies::loadImagePlugin();
	    //$gallery->writeJS();
	    if($items)
	    for ($key=0; $key < count($items); $key++){
	    	
	    	$add=$items[$key];
			
	    	$gallery->clearImages();
			$gallery->addImage($add->picture);
			
			$add->thumbnail			= $gallery->getThumb(0,1,0);
			$add->thumbnail_medium	= $gallery->getThumb(0,0,1);
			
			FactoryFieldsFront::translateRowOptions($add,$cfields_translations);
            
			// Distribute all Ad , Owner of ad user custom fields to positions
			$positions[$add->id] = JTheFactoryHelper::loadPositionedFields($add, "t_{$task}.tpl" );

		    $add->start_date_text=date(ads_opt_date_format,(strtotime($add->start_date)) );
            $add->end_date_text=date(ads_opt_date_format,(strtotime($add->end_date)) );

            AdsUtilities::process_countdown($add);
			$add->is_new = 0;
   			if( strtotime($add->start_date) + 86400*30 > time() )
        		$add->is_new = 1;
			
			$add->description = JFilterOutput::cleanText($add->description);
			$add->short_description = JFilterOutput::cleanText($add->short_description);
	    	// <== LEFT TIME Display if Enabled
			
			if($add->alltags)
				$add->tags = explode( "," , $add->alltags );
			
			$add->links = AdsUtilities::makeLinks($add);
			
			// Deprecated , Leave only for customisations
			//$catObj->load($add->category);
			//$add->catname = $catObj->catname;
	    	$items[$key] = $add;
		}
		//Ads_Debug::print_r($positions);

		if (ads_opt_enable_countdown) { 
			$jdoc = JFactory::getDocument();
			$js = JURI::root()."components/com_adsman/js/countdown.js";
			$jdoc->addScript( $js );
			$js = JURI::root()."components/com_adsman/js/startcounter.js";
			$jdoc->addScript( $js );
			$js_declaration = "var days='".JText::_('ADS_DAYS').",';	var expired='".JText::_('ADS_EXPIRED')."';var nrcounters=".count($items).";";
			$jdoc->addScriptDeclaration( $js_declaration );
        }
        
		$smarty->assign("positions",  $positions);

		if (!empty($filters["cat"])) {			
			
			$c = new JAdsCategories($database);
			$c->load($filters["cat"]);
			$catOBJ = new JTheFactoryCategoryObj();
			$pm = $catOBJ->string_cat_path($c->id,"listadds");
			$c->navigation = $pm;
			
			$doc->setTitle($c->catname);
	        if ($c->description) {
		        $doc->setMetaData( 'description', strip_tags($c->description));
		        $doc->setMetaData( 'abstract', strip_tags($c->description));
		        $doc->setMetaData( 'keywords', strip_tags(implode(explode(" ",$c->description),',')));
	        	
	        }

            $cat_path[$c->id] = JTheFactoryCategoryObj::get_cat_path($c->id,1,1);
            $catName = strtolower($c->catname);
            $catslug = "&amp;catslug=$catName";
            $catslug_parent = "&amp;parent=$c->parent";

            if(count($cat_path[$c->id])<=1){
            			//return;
            } else {

                $i=1;
                foreach($cat_path[$c->id] as $cat) {
                    if ($cat['id'] && $cat['id']!=0) {
                        $catslug = "";

                        if(isset($cat['catslug'])) {
                            $separator = PHP_EOL;
                            $catslug = str_replace($separator,"/",str_replace("/","-",$cat['catslug']));
                            $catslug = "&amp;catslug=$catslug";

                            $catslug_parent_string = $cat['parent'];
                            $catslug_parent = "&amp;parent=$catslug_parent_string";
                        }
                        $link = JRoute::_( 'index.php?option='.APP_EXTENSION.'&task=listadds&cat='.$cat['id'].'&Itemid='.$Itemid.$catslug.$catslug_parent);

                        $path->addItem( $cat['catname'],$link );
                    } else {
                        $link = JRoute::_( 'index.php?option='.APP_EXTENSION.'&task='.$task.'&Itemid='.$Itemid);
                    }
                    $i++;
                }
            }

			$smarty->assign("category_filter", $c);

            $smarty->assign("link_new_listing", JRoute::_("index.php?option=com_adsman&view=adsman&task=edit&category=" .$c->id . "&Itemid=$Itemid"));
		}

        $filter_order_Dir = $app->getUserStateFromRequest('com_adsman.filter_order_Dir', 'filter_order_Dir', "DESC");
        $reverseorder_Dir = $this->get( 'reversefilter_order_Dir');
        $filter_order = $this->get( 'filter_order');
        $archive = $this->get( 'archive');
        
		$ads_opt_sort_date = ads_opt_sort_date.'_date';
		//add alternate feed link
		if($params->get('show_feed_link', 1) == 1)
		{
			$document = JFactory::getDocument();
			$link	= '&format=feed&limitstart=';
			$attribs = array('type' => 'application/rss+xml', 'title' => 'RSS 2.0');
			$document->addHeadLink(JRoute::_($link.'&type=rss'), 'alternate', 'rel', $attribs);
			$attribs = array('type' => 'application/atom+xml', 'title' => 'Atom 1.0');
			$document->addHeadLink(JRoute::_($link.'&type=atom'), 'alternate', 'rel', $attribs);
		}

		//$smarty->debugging = 1;
        $smarty->assign("action", JRoute::_(JFilterOutput::ampReplace($uri->toString())) );
		$smarty->assign("items", $items);
		$smarty->assign("filters", $filters);
		$smarty->assign("HTMLfilters", AdsUtilities::makeHtmlFilters($filters));
		$smarty->assign("filter_order_Dir", $filter_order_Dir);
		$smarty->assign("reverseorder_Dir", $reverseorder_Dir);
		$smarty->assign("filter_order", $filter_order);
		$smarty->assign("archive", $archive);
		$smarty->assign("list_view", $listView);
		$smarty->assign("pagination", $pagination);
        $smarty->assign("thumb_width", intval(ads_opt_thumb_width)+3);


		
		$Fi = & FactoryFieldsFront::getInstance();
		$smarty->assign("custom_filters", $Fi->getFilters(null, null));
		
		if ($params->get('show_page_title', 1)){
			$page_title = $this->escape($params->get('page_title'));
			if($page_title==""){
		 		if ($task=="myadds") 
				 	$page_title = adsman_page_myadds;
				else
					$page_title = adsman_page_listadds;
			}
			$smarty->assign("page_title" ,  $page_title);
		}
		$smarty->assign("pageclass_sfx" , $params->get('pageclass_sfx') );


		JHTML::_("behavior.modal");

		if($listView=="grid")
			$listView='_grid';
		else
			$listView='';
		
		$smarty->display("t_{$task}{$listView}.tpl");
	}

	function listcats(){

        require_once( JPATH_ROOT.DS.'components'.DS.'com_adsman'.DS.'helpers'.DS.'helper.php' );
        require_once( JPATH_ROOT.DS.'components'.DS.'com_adsman'.DS.'helpers'.DS.'route.php' );
        $Itemid = JRequest::getVar('Itemid', 0, 'REQUEST', 'integer');

		$app = JFactory::getApplication();
		// Get the page/component configuration
		$params = &$app->getParams();
		$smarty = AdsUtilities::SmartyLoaderHelper();
   		$path 	= $app->getPathway();
        $db = JFactory::getDbo();

		$items	=& $this->get( 'Categories' );
        $cat = (int)JRequest::getVar("cat", 0);

        $depth = AdsUtilities::getCategoriesDepth();
        
        if($cat) {
			$smarty->assign("category_pathway", JTheFactoryCategoryObj::string_cat_path($cat,'listcats'));

			$current_cat = new JAdsCategories($db);
			$current_cat->load($cat);

            $database = JFactory::getDbo();
            $database->setQuery(" SELECT c . * , catsef.categories as catslug ".PHP_EOL.
                   " FROM #__ads_categories AS c " .PHP_EOL.
                    " LEFT JOIN `#__ads_categories_sef` AS `catsef` ON  `c`.`id`=`catsef`.`catid` ".PHP_EOL.
                    " WHERE c.id=$cat ");

            //echo nl2br($database->_sql);exit;
            $row = $database->loadObject();

            $catslug = "";

            if(isset($row->catslug)) {
                $separator = PHP_EOL;
                $catslug = str_replace($separator,"/",str_replace("/","-",$row->catslug));
                $catslug = "&amp;catslug=$catslug";

                $catslug_parent_string = $row->parent;
                $catslug_parent = "&amp;parent=$catslug_parent_string";
            }

			$positions[$current_cat->id] = JTheFactoryHelper::loadPositionedFields($current_cat, "t_catlist.tpl" , null,"details" );

			$AdsItemid = AdsmanHelperRoute::getMenuItemByTaskName("listadds");

			$query = AdsUtilities::joinCategories($cat);
			$db->setQuery($query);
						
		
			$idArray = $db->loadResultArray();
			$current_cat->kids = count($idArray);
			
			$current_cat->view = JRoute::_( "index.php?option=com_adsman&view=adsman&task=listadds&cat=".$cat. "&Itemid=$AdsItemid".$catslug.$catslug_parent);
			$idArray = array_merge(array($cat),$idArray);

			$idList = implode(",",$idArray);
            $db->setQuery("SELECT count(*) FROM #__ads WHERE category IN ( $idList ) ");
			$current_cat->nr_ads = $db->loadResult();

            $db->setQuery("SELECT count(*) FROM #__ads WHERE category = '".$current_cat->id."' ");
            $current_cat->nr_ads_current = $db->loadResult();


            $cat_path[$current_cat->id] = JTheFactoryCategoryObj::get_cat_path($current_cat->id, 1,1);

            $catName = strtolower($current_cat->catname);
            $catslug = "&amp;catslug=$catName";
            $catslug_parent = "&amp;parent=$current_cat->parent";

            if(count($cat_path[$current_cat->id])<=1){
                 //return;
            } else {
                $i=1;

                foreach($cat_path[$current_cat->id] as $cat) {
                    if ($cat['id'] && $cat['id']!=0) {
                        $catslug = "";

                        if(isset($cat['catslug'])) {
                            $separator = PHP_EOL;
                            $catslug = str_replace($separator,"/",str_replace("/","-",$cat['catslug']));
                            $catslug = "&amp;catslug=$catslug";

                            $catslug_parent_string = $cat['parent'];
                            $catslug_parent = "&amp;parent=$catslug_parent_string";
                        }
                        $linkCat = JRoute::_( 'index.php?option='.APP_EXTENSION.'&task=listadds&cat='.$cat['id'].'&Itemid='.$Itemid.$catslug.$catslug_parent);
                        $path->addItem( $cat['catname'],$linkCat );
                    } else {
                     //   $linkCat = JRoute::_( 'index.php?option='.APP_EXTENSION.'&task=listadds&Itemid='.$Itemid);
                    }
                    $i++;
                }
            }

			$smarty->assign("current_category", $current_cat);
	    }
	    
	    if($items)
	      for ($key=0; $key < count($items); $key++){
	    	$categ = $items[$key];

			// Distribute all Ad , Owner of ad user custom fields to positions
			$positions[$categ->id] = JTheFactoryHelper::loadPositionedFields($categ, "t_catlist.tpl" );
	    	$items[$key] = $categ;

            for ($j = 0; $j < count($categ->subcategories); $j++) {
				// Distribute all Ad , Owner of ad user custom fields to positions
				$positions[$categ->subcategories[$j]->id] = JTheFactoryHelper::loadPositionedFields($categ->subcategories[$j], "t_catlist.tpl" );
	        }

            $query = AdsUtilities::joinCategories($categ->id);
            $db->setQuery($query);

            $idArray = $db->loadResultArray();
            $idArray = array_merge(array($categ->id),$idArray);
            $idList1 = implode(",",$idArray);

            $db->setQuery("SELECT count(*) FROM #__ads WHERE category IN ( $idList1 ) ");
            $items[$key]->total_nr_ads = $db->loadResult();

          }

		if($cat) {	    
			$smarty->assign("positions", $positions);
		}

		$smarty->assign("categories", $items);

        if ($params->get('show_page_title', 1)){
			$page_title = $this->escape($params->get('page_title'));
			if($page_title==""){
				$page_title = adsman_page_cats;
			}
			$smarty->assign("page_title" ,  $page_title);
		}
		JHTML::_("behavior.tooltip");

		$smarty->display( "t_catlist.tpl" );
	}
	
	function catselect(){
		$smarty = AdsUtilities::SmartyLoaderHelper();
		//$items	=& $this->get( 'Categories' );
	    $task=JRequest::getCmd('task');
		$database = JFactory::getDbo();
		
		$cat = new JTheFactoryCategoryObj();	
		$cat->show_active_only = true;
		
		/** To Include Root Element */
		//$items = $cat->build_children_tree(0,false);
		
		/** To Not Include Root Element */
		$items = $cat->build_children_tree(0,true);
		
		$id   = JRequest::getVar('id',null);
		if($id){
			$add = new JAdsAddsman(JFactory::getDBO());
			$add->load($id);
			$smarty->assign("cat", $add->category);
			$smarty->assign("id", $id);
		}

        JHTML::_('script', 'adsman.js', 'components/com_adsman/js/');
        JHTML::_('script', 'categories.js', 'components/com_adsman/js/');
        JHTML::_('script', 'overlib.js', 'components/com_adsman/js/');

        $js_block = "";
        $js_block .= "
            var CatList = Array(10000);
        	var CatParents = Array(10000);
        ";
        $jdoc = JFactory::getDocument();

        if (count($items) )
            foreach ($items as $k => $category){

            $js_block .= "
            \r\n
              CatList[$category[id]] = '$category[catname]';\r\n
              CatParents[$category[id]] = '$category[parent]';\r\n
            ";
            }

		$jdoc->addScriptDeclaration( $js_block );

		if($task=="selectcat")
			$task="edit";
		$smarty->assign("task", $task);
		$smarty->assign("categories", $items);
		$smarty->display("t_catselect.tpl");
	}

	/**
	 * Add detail HTML
	 *
	 * @modified: 21/09/09 
	 * */
	function details() {
		
		$app = JFactory::getApplication();
        $Itemid = JRequest::getVar('Itemid', 0, 'REQUEST', 'integer');
		$doc	= JFactory::getDocument();
		$path 	= $app->getPathway();
		
		$my 		= JFactory::getUser();
		$smarty 	= AdsUtilities::SmartyLoaderHelper();
		$database 	= JFactory::getDbo();
		$curentItemid = AdsmanHelperRoute::getMenuItemByTaskName("listadds");
/**
 * Some Validations
*/
		$id	= JRequest::getVar('id',null,'default','int');
		if ( !$id ){
			echo adsman_add_not_found; 
			return;
		}
		
		$model		= $this->getModel("adsman");
		$modelUser	= $this->getModel( 'user' );
		
		$add = $model->getAddData($id);
		
		if (!$add->id) {
			echo adsman_add_not_found; return;
		}

		if (!$add->haveAccess()) {
			JError::raiseWarning(1,JText::_("ALERTNOTAUTH"));
			return;
		}
		
		$positioned1 = JTheFactoryHelper::loadPositionedFields($add, null,"#__ads", "details" );

		$category = new JAdsCategories($database);
		$category->load($add->category);
		
		$positioned3 = JTheFactoryHelper::loadPositionedFields($category, null,"#__ads_categories");

		$user = null;
		if(isset($add->userid))
			$user =& $modelUser->getUserData($add->userid);

		$positioned2 = JTheFactoryHelper::loadPositionedFields($user, "t_details_add.tpl","#__ads_users");
		
		if(!$positioned1) $positioned1 = array();
		if(!$positioned2) $positioned2 = array();
		if(!$positioned3) $positioned3 = array();
		$positionsincredibil = array_merge_recursive($positioned1,$positioned2, $positioned3);

		/** video Urls */
		$video_url 		=& $model->getUrls($add->id);
		$video_link		= '';
	
		if (  $video_url->video_link != '' ) {
						
			$video_link_source = '';
			$thumb_source = ($video_url->video_thumbnail != "") ? $video_url->video_thumbnail : $video_url->video_sourceThumb;
			
			if (isset($video_url->video_title) && $video_url->video_title != "") 
			 	$video_title = stripslashes($video_url->video_title);
			else 
				$video_title = $video_url->video_link;
     	
     		if (isset($video_url->video_description)) 
     		 	$video_description = nl2br(html_entity_decode(stripslashes($video_url->video_description)));
     		else 
     			$video_description = '';
     			
     			
     		if (strpos($video_url->video_link,'youtube')) {
     			$video_link_source = 'youtube';
     		}
     		
     		if (strpos($video_url->video_link,'vimeo')) {
     			$video_link_source = 'vimeo';
     		}
     		
     		if (strpos($video_url->video_link,'youtube')) {
     			$video_link_source = 'youtube';
     		}
     		
     		if (strpos($video_url->video_link,'youtube')) {
     			$video_link_source = 'youtube';
     		}
     		
     		if (strpos($video_url->video_link,'youtube')) {
     			$video_link_source = 'youtube';
     		}	
     			
     		$video_link = $video_url->video_link;
     		
     		$smarty->assign("video_link_source", $video_link_source);
     		$smarty->assign("video_title", strip_tags($video_title, '<a>'));	
     		$smarty->assign("video_description", strip_tags($video_description, '<a>'));	
     		$smarty->assign("thumb_source", $thumb_source);
     		$smarty->assign("video_sourceThumb", $video_url->video_sourceThumb);
     		$smarty->assign("video_sitename", $video_url->video_sitename);
     			
     		JHTML::stylesheet ( 'prettyPhoto.css', 'components/com_adsman/css/' ); //media="screen"
     		JHTML::stylesheet ( 'jquery-ui-1.7.2.custom.css', 'components/com_adsman/css/' );
     		
     		JHTML::_('script', 'jquery-factory.js', 'components/com_adsman/js/jquery/');
     		JHTML::_('script', 'jquery.prettyPhoto.js', 'components/com_adsman/js/prettyPhoto/' );  
     		
     		JHTML::_('script', 'detailVideo.js', 'components/com_adsman/js/prettyPhoto/');

		}
		$smarty->assign("video_link", $video_link);
		
/**
 * Document prepend
*/
		$doc->setTitle($add->title);
        $doc->setMetaData('description',strip_tags($add->short_description));
        $doc->setMetaData('abstract',strip_tags($add->description));
		$add->title_sef = JFilterOutput::stringURLSafe($add->title);

        //breadcrumbs path

        $cat_path[$add->category] = JTheFactoryCategoryObj::get_cat_path($add->category, 1,1);

        if(count($cat_path[$add->category])<=1){
            //return;
        } else {

            $i=1;
            foreach($cat_path[$add->category] as $cat) {
                if ($cat['id'] && $cat['id']!=0) {
                    $catslug = "";
                    $catslug_parent = "";

                    if(isset($cat['catslug'])) {
                        $separator = PHP_EOL;
                        $catslug = str_replace($separator,"/",str_replace("/","-",$cat['catslug']));
                        $catslug = "&amp;catslug=$catslug";

                        $catslug_parent_string = $cat['parent'];
                        $catslug_parent = "&amp;parent=$catslug_parent_string";
                    }
                        $link = JRoute::_( 'index.php?option='.APP_EXTENSION.'&task=listadds&cat='.$cat['id'].'&Itemid='.$Itemid.$catslug.$catslug_parent);
                        $path->addItem( $cat['catname'],$link );
                } else {
                    $link = JRoute::_( 'index.php?option='.APP_EXTENSION.'&task=listadds&Itemid='.$Itemid);
                }
                $i++;
            }
        }

		$links_path = JRoute::_('index.php?option=com_adsman&task=details&title='.$add->title_sef.'&id='. $add->id .'&Itemid='. $curentItemid );
		
		$path->addItem( $add->title,$links_path );
        
        if( isset($add->tags) )
        	$doc->setMetaData('keywords',strip_tags(implode($add->tags,',')));
        	
		$jdoc = & JFactory::getDocument();
		$jdoc->addScript(JURI::root()."components/com_adsman/js/adsman.js");
		
		$add->images = $add->getImages();
		$existing_images = count($add->images);
		
		if ($my->id != 0) {
			$count_user_ads = AdsUtilities::getCountUserAds($user->userid);
			$smarty->assign("count_user_ads", $count_user_ads);
		}
		
	
/* PLUGINS */
		$payment	= &JTheFactoryPaymentController::getInstance();
		
		// check if enabled pay_image
		$isImageEnabled = price_pay_image_DBHelper::getEnabledPayImage();

		if ( $isImageEnabled ) {

			$free_array =  price_pay_image_DBHelper::getFreeImage();
			
			$smarty->assign("main_img_free", 0);	 // main 
		// test this condition 
			if (!empty($free_array) ) {
				foreach ($free_array as $k=>$val) {
					if ($k == 0) {
						$smarty->assign("main_img_free", 1);	 // main 
						$smarty->assign("images_upload_free_no", $k);	 // $free_no pictures	 
						$free_no = $k;
					} else {
						$smarty->assign("images_upload_free_no", $k);	 // $free_no pictures
						$free_no = $k;	 
					}
				}
				
			} else {
				$free_no = 0;	
			}

			$image_credits = generic_pricing::getCredits($my->id);
			
			if ( $image_credits ) {
				
				$payperimage_arr = price_pay_image_DBHelper::getPayImageAmount();
				
				foreach ($payperimage_arr as $k=>$row) {
			
						if( $row > 0 ) {
				  			$smarty->assign("credits_row", $row);	 // main + $free_no pictures	
							$img_allowed = $free_no+$row;
						}
				}
            } 

		if ($free_no > $existing_images)
			$smarty->assign("display_feature_image", 0);
		else 
			$smarty->assign("display_feature_image", 1);
		}

        $mapX = '';
        $mapY = '';
        if ($add->MapX != '' && $add->MapY != '') {
            $mapX = $add->MapX;
            $mapY = $add->MapY;
            JHTML::_("adsmanmap.js","map_canvas","listmap","gmap_centermap({pointTox:$mapX,pointToy:$mapY});");
        } elseif ($user->googleX!="" && $user->googleY!="") {
            $mapX = $user->googleX;
            $mapY = $user->googleY;
            JHTML::_("adsmanmap.js","map_canvas","listmap","gmap_centermap({pointTox:$mapX,pointToy:$mapY});");
        }

		$smarty->assign("pay_img_enabled", $isImageEnabled );

		$smarty->assign('pricing_plugins', $payment->payment_items);
		$smarty->assign("add", $add);
		$smarty->assign("add_video", $video_url);
		$smarty->assign("user", $user);
		$smarty->assign("cs", AdsUtilities::init_captcha());
		$smarty->assign("positions",  $positionsincredibil );
		
		JHTML::_("behavior.modal");
		$smarty->display("t_details_add.tpl");
				
		if (ads_opt_enable_countdown) { 
			$jdoc = & JFactory::getDocument();
			$js = JURI::root()."/components/com_adsman/js/countdown.js";
			$jdoc->addScript( $js );
			
			$js = JURI::root()."/components/com_adsman/js/startcounter.js";
			$jdoc->addScript( $js );
			
			$js_declaration = "var days='".JText::_('ADS_DAYS').",';	var expired='".JText::_('ADS_EXPIRED')."';var nrcounters=1;";
			$jdoc->addScriptDeclaration( $js_declaration );
        }
        
		if(!$add->isMyAdd()) {
			$add->hit();
		}
	}

	function report(){
		JHTML::_('behavior.framework');
		JHTML::_('behavior.mootools');
        JHTML::_('script', 'adsman.js', 'components/com_adsman/js/');
		
		$smarty = AdsUtilities::SmartyLoaderHelper();
		$smarty->display("t_report.tpl");
	}

	function terms() {
		
		$smarty = AdsUtilities::SmartyLoaderHelper();
		$database = JFactory::getDbo();
		$terms  = new JAdsMails($database);
		if (!$terms->load('terms_and_conditions') ) return;
		ob_clean();
		echo '<meta http-equiv="content-type" content="text/html; charset=utf-8" />';
		echo $terms->content;
		jexit();
	}

	function edit() {
		
		JHTML::_('behavior.framework');
		JHTML::_('behavior.mootools');
        jimport( 'joomla.html.html' );
		
		$Itemid = JRequest::getVar('Itemid', 0, 'REQUEST', 'integer');
		$app = JFactory::getApplication();
        $jdoc = JFactory::getDocument();
		$Tapp = JTheFactoryApp::getInstance();

		$smarty 	= AdsUtilities::SmartyLoaderHelper();
		$my 		= JFactory::getUser();
		
		if ($my->block == 1) return;
		
		$task		= JRequest::getCmd( 'task' );
		
		$database 	= JFactory::getDbo();
		
		$add 		= new JAdsAddsman($database);
		$user 		= new JAdsUsers($database);
		$lists = array();

		$id	= JRequest::getVar('id', null );
		$add->ancLoad($id); // dif: 24/08/2010 : originally: $add->load($id)
	
	    if($id)
	    {
	   	 $user->getUserDetails($add->userid);
	    }
	    elseif (isset($my))
	    {
	   	 $user->getUserDetails($my->id);
	    }
	   
		$session	= JFactory::getSession();
		if ($session->has("tmp_ad", "tmpadserialized")){
			$c = $session->get("tmp_ad",null,"tmpadserialized");
			$c = unserialize($c);
			$add = $c;
			$session->clear("tmp_ad","tmpadserialized");
		}

		if( $add->category ){
			JRequest::setVar('custom_fields_category',$add->category);
		}
		
		if( defined("ads_opt_logged_posting") && ads_opt_logged_posting==1 && !$my->id  ){
			if( $id !=null ) {
				$tk		= JRequest::getCmd( 'tk' );
				if ($tk != $add->token_id){
					JError::raiseWarning(1,JText::_("ALERTNOTAUTH"));
					return;
				}
			}
			// init captcha
			$smarty->assign( "cs", AdsUtilities::init_captcha() );
			
		} else {
			
			if($id !=null && !$add->isMyAdd()) {
				JError::raiseError( 403, JText::_("ALERTNOTAUTH") );
			}
		}

		JHTML::_('behavior.calendar');
		$arr_dateformat=array(
	            'Y-m-d'=>'%Y-%m-%d',
                'm/d/Y'=>'%Y-%m-%d',
                'd/m/Y'=>'%Y-%m-%d',
                'd.m.Y'=>'%Y-%m-%d',
	            'D, F d Y'=>'%Y-%m-%d'
	    );

	    $calendar_format = $arr_dateformat[ads_opt_date_format];
	    $calendar_format2 =  ads_opt_date_format;
	    
	    $config = JFactory::getConfig();
		$offset = $config->get("offset");           
      
		$timestamp = strtotime(gmdate("M d Y H:i:s", time()));
		$add_days = $timestamp + 3600*$offset + 86400*3; //added 3 days

        $now = AdsHelperDateTime::isoDateToUTC($timestamp + 3600*$offset);
        $end = AdsHelperDateTime::isoDateToUTC($add_days);

        $timestamp_start = AdsHelperDateTime::DateToIso($add->start_date);
        $timestamp_end = AdsHelperDateTime::DateToIso($add->end_date);

		$start_date = ($add->start_date != null) ? $timestamp_start : $now;
		$end_date = ($add->end_date != null) ? $timestamp_end : $end;

		$lists['valab_now'] = $now;
		$lists['valab_end'] = $end;


        if($id)
	    {
            if ( defined("ads_opt_component_offset") && ads_opt_component_offset!="" )
                $offset = ads_opt_component_offset;

            if ($offset != 'UTC') {
                $corelate = 0;
            } else {
                $corelate = 1;
            }

            $startdate = Ads_Time::getTimeStamp($add->start_date,$corelate);
            $expiredate = Ads_Time::getTimeStamp($add->end_date,$corelate);

            $diff = abs($expiredate - $startdate);

            // $db_no_of_days = floor($diff / 86400) + 1;
            $db_no_of_days = floor($diff / 86400) ;
            $lists['db_no_of_days'] = $db_no_of_days;
            $lists['db_key_valab'] = $db_no_of_days;

            $sql_pay = "SELECT COUNT(*) FROM ".PAYTABLE_PAYLOG." WHERE object_id = '$add->id' AND itemname = 'listing' AND status='ok' ";
            $database->setQuery($sql_pay);
            $was_payed_listing = $database->loadResult(); // payment done

            $sql = "SELECT COUNT(*) FROM ".PAYTABLE_PAYLOG." WHERE object_id = '$add->id' AND itemname = 'listing' AND ( `status` <> 'ok' OR ISNULL(`status` ) ) ";
            $database->setQuery($sql);
            $islogged_listing = $database->loadResult(); //payment started

        }
        
        if ($add->id){
            $status_listing=$add->status && $was_payed_listing && !$islogged_listing;
            $jdoc->addScriptDeclaration("
             	var was_payed_listing = ".((int)$status_listing).";
                var ad_id = {$add->id};
                var ad_featured = '{$add->featured}';
                var count_delete_images = 0;
            
            ");
        }else{
            $jdoc->addScriptDeclaration("
             	var was_payed_listing = 0;
                var ad_id = null;
                var ad_featured = 'none';
                var count_delete_images = 0;
            ");
        }

		$payOb = JTheFactoryPaymentLib::getInstance();
	    $payOb->loadAvailabeItems();
		$pricings = $payOb->price_plugins;  // array['price_featured_bronze'], ['price_packages'], ['price_pay_image'],['price_listing']
        $listing_enabled = 0;
	
		if (isset($pricings['price_listing']) && $pricings['price_listing']->enabled == 1) {
		
			if ( isset($pricings['price_listing']->time_valability) && $pricings['price_listing']->time_valability == 1 ) {  // Preconfigured Availabilities Ranges ==1
								
				$params_arr = array();
			
				if ($pricings['price_listing']->time_valability_prices  && trim( strip_tags($pricings['price_listing']->time_valability_prices) ) !=""  ) {
					$params_arr = explode("|", trim( strip_tags($pricings['price_listing']->time_valability_prices) ) );
						
					$valabs = array();
										
					if (count($params_arr) > 0) {
						foreach ($params_arr as $p){
							if ($p) {
								$pi = explode(":",$p);
								$valabs[] = $pi[0]; //days
							}
						}
						sort($valabs);

						reset($valabs);
						$first_key = key($valabs);
						$add_days = $timestamp + 3600*$offset + 86400 * $valabs[$first_key] ; //added $valabs[$first_key] days

						$lists['valab_end']  = AdsHelperDateTime::isoDateToUTC($add_days);
                            //date( $calendar_format2, $add_days);

                        $time_limited = 1;
						//$smarty->assign("time_limited", 1 );
						/*$valabs
						array
						  0 => string '2' (length=1) days
						  1 => string '3' (length=1)
						  2 => string '10' (length=2)
						*/
					}
				} 
			
			} else {
				//$smarty->assign("time_limited", 0 );
                if ( isset($pricings['price_listing']->preferential_categories) && $pricings['price_listing']->preferential_categories == 1) {

                    if ($pricings['price_listing']->default_price_categories && $pricings['price_listing']->prices_categories == 0) {
                        $time_limited = 2;
                    } else {
                        $time_limited = 0;
                    }
                }

			}

            $listing_enabled = 1;
		}
        $smarty->assign("listing_enabled", $listing_enabled );

        $allow_edit = 1;
        if ($id) {
            $time = Ads_Time::getNow();
            $startdate = Ads_Time::getTimeStamp($add->start_date,$corelate);

            $diff24 = abs($time - $startdate);
            $hours = round($diff24 / 3600,0);

            if ( $hours > 24 && ($add->featured != 'none' || $listing_enabled) ) {
                $allow_edit = 0;
            }

        }

        $smarty->assign("allow_edit", $allow_edit );
        $edit_24hours_info = JText::_('ADS_EDIT_24HOURS');
        $smarty->assign("edit_24hours_info", $edit_24hours_info);

        $lists['start_date_calendar'] = JHtml::_('adsdate.calendar', ($add->start_date != null) ? $timestamp_start : $now, 'start_date');
        $lists['end_date_calendar']     = JHTML::_('adsdate.calendar', ($add->end_date != null) ? $timestamp_end : $end, 'end_date');

		$editor = JFactory::getEditor();
		
        $jdoc->addScriptDeclaration("
            function editorSave(){
                ".$editor->getContent( 'description' )."
                ".$editor->save( 'description' )."
            }        
        ");
        
        if($id && !$allow_edit)
        {
            $lists["description"] =   $add->description."<input type='hidden' name='description' value='".htmlentities($add->description,ENT_QUOTES, 'UTF-8')."' />";
        } else {
            $lists["description"] = $editor->display('description', $add->description, '100%', '400', '70', '15', defined("ads_opt_post_editor_buttons")?(bool)ads_opt_post_editor_buttons:false );
        }
		// string $name: The control name, contents of the text area, width of the text area (px or %), height of the text area (px or %), $col, $row,
        //$buttons: True and the editor buttons will be displayed,  array $params: Associative array of editor parameters
		$lists["currency"] = AdsUtilities::makeCurrencySelect($add->currency);

		if( defined('ads_opt_adtype_enable') && ads_opt_adtype_enable==0 &&  defined('ads_opt_adtype_val') && ads_opt_adtype_val!==''){
			$lists['addtype']=	"<input type='hidden' name='addtype' value='".ads_opt_adtype_val."' >";
		} else {
			$opts = array();
			$opts[] = JHTML::_('select.option', '', JText::_('ADS_AD_TYPE_SELECT'));
			$opts[] = JHTML::_('select.option', ADDSMAN_TYPE_PUBLIC, JText::_('ADS_ADTYPE_PUBLIC'));
			$opts[] = JHTML::_('select.option', ADDSMAN_TYPE_PRIVATE, JText::_('ADS_ADTYPE_PRIVATE'));
			$lists['addtype']=	JHTML::_('select.genericlist',  $opts, 'addtype', 'class="inputbox" alt="addtype"',  'value', 'text',$add->addtype);
		}

		if ( (defined('ads_opt_adpublish_enable') && ads_opt_adpublish_enable==0 && defined('ads_opt_adpublish_val') && ads_opt_adpublish_val!=='') || (isset($listing_enabled) && $listing_enabled == 1) ){
			
			if ($id) 
				$lists['status']=	"<input type='hidden' name='status' value='".$add->status."' >";
			else 
				$lists['status']=	"<input type='hidden' name='status' value='".ads_opt_adpublish_val."' >";
		} else {
			$opts = array();
			$opts[] = JHTML::_('select.option', '', JText::_("ADS_SELECT"));
			$opts[] = JHTML::_('select.option', 1,JText::_("ADS_PUBLISHED"));
			$opts[] = JHTML::_('select.option', 0,JText::_("ADS_UNPUBLISHED"));
			$lists['status']=	JHTML::_('select.genericlist',  $opts, 'status', 'class="inputbox" alt="status"',  'value', 'text',$add->status);
		}
		
		if($add->id){
			$tag_obj=new adsTags($database, "#__ads");
			$tg = $tag_obj->getTags($add->id);
			if($tg){
				$add->tags = $tg;
				$add->tags = implode(",", $add->tags);
			}
		}

		$add->images = $add->getImages();
		$add->links = AdsUtilities::makeLinks($add);
		

		if($task=="republish"){
			$add->id = null;
			$smarty->assign("oldid", $id);
			$smarty->assign("oldpic", $add->picture);
		}
		
		
		$use_terms = (int)$Tapp->getIniValue('use_terms_and_conditions');
		$js="var require_terms = 0;\n";
        $lists['terms'] = 0;
		if (!$add->id && $use_terms == 1) {
			$terms	= new JAdsMails($database);
			$terms->load('terms_and_conditions');
			if($terms->enabled && $terms->content){
        		$js="var require_terms = 1;\n";
                $lists['terms'] = 1;
            }
   		}
        $jdoc->addScriptDeclaration($js);
        
        $jdoc->addScriptDeclaration("var require_price = ".ads_price_compulsory.";\n");
        $jdoc->addScriptDeclaration("var ROOT_HOST = '".JURI::root()."';\n");
        
		
		$credits = generic_pricing::getCredits($my->id);
		$smarty->assign('credits',$credits);
		
		$allowed_attachments = '';
        $dim_attachments=0;
		if (ads_allowed_attachments != '') {
			
			$att_keys = array();
			$allowed_attachments = explode(",",ads_allowed_attachments);
			$dim_attachments = count($allowed_attachments);
            $jdoc->addScriptDeclaration("
        	   var allowed_attachments = '".ads_allowed_attachments."';
	           var dim_attachments = {$dim_attachments};
            ");			
		} else {
            $jdoc->addScriptDeclaration("
        	   var allowed_attachments = '';
	           var dim_attachments = 0;
            ");			
		}

		$category = JRequest::getVar('category', $add->category );
        $Fi = & FactoryFieldsFront::getInstance();

        if($id) {
            $fields = $Fi->getHTMLFields("ads", $add->id, $add,0);
        } else {
            $fields = $Fi->getHTMLFields("ads", $add->id, $add,1);
        }

		if ($add->id) {
			$js = ' var AFIDid = "'.$add->id.'";';
		} else {
			$js = ' var AFIDid = "";';
		}
		$jdoc->addScriptDeclaration($js);
        

		if ( ads_opt_category_page || ( ads_opt_category_page && $category > 0 ) ) {
			$lists["category"] = "<input type='hidden' name='category' value='".$category."' />";
			if( isset($category) && $category!="" ) {
				JRequest::setVar("category",$category);
				
				$cat = new JTheFactoryCategoryObj();
				$catname = $cat->string_cat_name($category);
				$lists["category"] .="<span>".$catname."</span>";
			}
		} elseif ( ($add->id ) && (isset($time_limited) && $time_limited == 0)) {
            $lists["category"] = "<input type='hidden' name='category' value='".$category."' />";
			if( isset($category) && $category!="" ) {
				JRequest::setVar("category",$category);

				$cat = new JTheFactoryCategoryObj();
				$catname = $cat->string_cat_name($category);
				$lists["category"] .="<span>".$catname."</span>";
			}
        }
        else {
			if (!defined("ads_opt_category_ajax_load") || ads_opt_category_ajax_load==0) {
				$include_root_params = array();
				if (defined("ads_opt_category_root_post") && ads_opt_category_root_post==0) {
					$include_root_params = array("include_root"=>0);
				}
				if( isset($category) && $category!="" )
					$lists["category"] = Ads_HTML::selectCategory("category", $include_root_params + array("select"=>$category,"script"=>"onchange='aiai(this.value);'"));
				else	
					$lists["category"] = Ads_HTML::selectCategory("category", $include_root_params + array("select"=>$add->category,"script"=>"onchange='aiai(this.value);'"));
			} else {
				if ( isset($category) && $category!="" )
					$catid = $category;
				else	
					$catid = $add->category;
				$js = ' window.addEvent("domready", function() {	categorySelectPopulate("ajax_selectcategories",'.(int)$catid.'); })';
				$page = 'var page = "edit";';
				
				$jdoc->addScriptDeclaration($js);
				$jdoc->addScriptDeclaration($page);

			}
		}
		
		// check if enabled pay_image
		$isImageEnabled = price_pay_image_DBHelper::getEnabledPayImage();

		if ( $isImageEnabled ) {
			// you can upload x pictures free
			$free_array =  price_pay_image_DBHelper::getFreeImage();

			$smarty->assign("main_img_free", 0);// main 
			$smarty->assign("images_upload_free_no", 0);	 
			
			if (!empty($free_array) ) {
				foreach ($free_array as $k=>$val) {
					if ($k == 0) {
						$smarty->assign("main_img_free", 1);	 // main 
						$smarty->assign("images_upload_free_no", $k);	 // $free_no pictures	 
						$free_no = $k;
					} else {
						$smarty->assign("images_upload_free_no", $k);	 // $free_no pictures
						$free_no = $k;	 
					}
				}
				
			} else {
				$free_no = 0;	
			}
											
			$existing_images = count($add->images);

            $count_replace_images = 0;
            if ($add->id){
                $count_replace_images = $add->getCountDeletedImages();
            }
            $smarty->assign("count_replace_images", $count_replace_images);
			            
            $main_opts = array();
	  		$main_opts[] = JHTML::_('select.option', 0,JText::_("ADS_NO"));
			$main_opts[] = JHTML::_('select.option', 1,JText::_("ADS_YES"));
								
	  		$main_image_credits =	JHTML::_('select.radiolist', $main_opts, 'main_image_add', 'class="inputbox" alt="main_image_add"',  'value', 'text', 0,'main_image_add' );
	  		$main_img_price = price_pay_image_DBHelper::getMainImageAmount();
	  		$smarty->assign('credits_main',$main_img_price);

	  		$payimage_credits = price_pay_image::ShowSelectFromCredits($existing_images);
		  		
			if (!empty($payimage_credits)) {
				$select_credits = $payimage_credits['list'];
				$select_indexes = $payimage_credits['indexes'];
			
				$smarty->assign('select_credits',$select_credits);
				$smarty->assign('select_indexes',$select_indexes);
			}
		}
		$smarty->assign("pay_img_enabled", $isImageEnabled );
				
		$img_featured = array();
		$img_featured[] = JHTML::_('select.option', 0, JText::_('ADS_AD_NOT_FEATURED'));
		
		if (!empty($payimage_credits)) {
			$img_featured[] = JHTML::_('select.option', 1,  JText::_('ADS_AD_IMAGES'));
		}
				
		$lists['add_images'] =	JHTML::_('select.radiolist', $img_featured, 'img_featured', 'class="inputbox" alt="img_featured" onclick="addImages()"',  'value', 'text', 0,'img_featured' );
	
				
		// add featured options
        if (isset($my->id) && $my->id != 0) {
            $sql = "SELECT p.itemname, c.credits_no, c.userid, c.package_free FROM #__ads_pricing p "
                . " LEFT JOIN #__ads_user_credits c ON c.userid='$my->id' "
                . " LEFT JOIN #__users u ON c.userid=u.id "
                . " WHERE p.enabled=1 AND p.itemname LIKE '%featured_%' ";

            $database->setQuery( $sql );
            $resultf = $database->loadObjectList();

            $featured = array();
            $featured[] = JHTML::_('select.option', 'none', JText::_('ADS_AD_NOT_FEATURED'));

            // if credits for featured ads
            if ($resultf && !empty($resultf) ) {

                //$count_featured = substr_count($lists['credits'],'name="featured"');
                foreach ($resultf as $v=>$feature) {

                    //featured_gold, featured_silver, featured_bronze
                    $db_feature = str_replace('featured_','', $feature->itemname);
                    $featured[] = JHTML::_('select.option', $db_feature, $db_feature);
                    //$featured[] = JHTML::_('select.option', $db_feature, $feature->itemname);
                }

                $f = (isset($add->id) && $add->featured!= '') ?  $add->featured : 'none';
                $lists['credits'] =	JHTML::_('select.radiolist', $featured, 'featured', 'class="inputbox" alt="featured"',  'value', 'text', $f,'featured' );
                $isFeaturedEnabled = 1;

            }  else {
                $isFeaturedEnabled = 0;
            }
        } else {
            $isFeaturedEnabled = 0;
        }
		
		// Video URL
		if ( isset($add->id) && ($add->id != '') ) {
            $sql = "SELECT id, video_link FROM #__ads_urls "
                . " WHERE ad_id= $add->id ";
		
			$database->setQuery( $sql );
			$resultv = $database->loadAssoc() ;
			$smarty->assign("video_url", 	$resultv['video_link']);
			$smarty->assign("video_id", 	$resultv['id']);
			
		} else {
			$smarty->assign("video_url", 	'');
			$smarty->assign("video_id", 	null);
		}
		
		$lists['cancel_form'] = JRoute::_( 'index.php?option=com_adsman&amp;task=listadds&amp;Itemid='. $Itemid );
        
        if (ads_opt_google_key){
            if ($add->MapX && $add->MapY){
                $mapmarker="{pointTox:$add->MapX,pointToy:$add->MapY}";
            }elseif($user->googleX && $user->googleY ){
                $mapmarker="{pointTox:$user->googleX,pointToy:$user->googleY}";
            }else{
                $mapmarker="{pointTox:".ads_opt_googlemap_defx.",pointToy:".ads_opt_googlemap_defy."}";
            }
            JHTML::_("adsmanmap.js","map_canvas","listmap","gmap_centermap($mapmarker,'listmap',true);");
        }
                	

        $isGuest = ($my->id == 0) ? 1 : 0;
		JFilterOutput::objectHTMLSafe( $add, ENT_QUOTES );

		$smarty->assign("ads_token", 	JHTML::_( 'form.token' ));
		$smarty->assign("profiler", 	$fields);
		$smarty->assign("add", 			$add);
		$smarty->assign("user", 		$user);
        $smarty->assign('isGuest',      $isGuest);
		$smarty->assign("lists", 		$lists);
		$smarty->assign("isFeaturedEnabled", 		$isFeaturedEnabled);
		
		$document	= JFactory::getDocument();
		
		if(defined("ads_short_desc_long") && (int)ads_short_desc_long > 0 )
		{
			$js = 'window.addEvent("domready", function() {
    
			    var textArea = $("short_description");
			    var maxChars = '.ads_short_desc_long.';
			    
			    // create a custom focused property so that we only capture keystrokes when it is
			    textArea.addEvents({
			        focus: function() {
			            this.focused = true;
			        },
			        blur: function() {
			            this.focused = false;
			        }
			    });
			    
			    // attach a key listener
			    window.addEvent("keydown", function(e) {
			        if (textArea.focused) {
			            var inn = textArea.value;
			            var chars = inn.length;
			            document.getElementById("descr_counter").innerHTML = chars;
			            textArea.value = inn.substring(0,maxChars-1);
			        }
			    });
			}); 
			';
			
			$document->addScriptDeclaration($js);
		}
		
		$root = JUri::root();
		$link = JRoute::_ ( 'index.php?option=com_adsman&task=add_credits_calculate&format=raw' );
		$credit_purchase_link = JRoute::_ ( 'index.php?option=com_adsman&task=buy_packages&package=FullListing' );
				 
		$document->addScriptDeclaration(' var root = "'.$root.'";');
		$document->addScriptDeclaration(' var link = "'.$link.'";');
		$document->addScriptDeclaration(' var purchase_credits_link = "'.$credit_purchase_link.'";');
		
		JHTML::_('behavior.modal');
		JHTML::_('behavior.formvalidation');
		JHTML::script("jquery.js",'components/com_adsman/js/jquery/');
		JHTML::script("jquery.noconflict.js",'components/com_adsman/js/jquery/');
		JHTML::script("jquery-factory.js",'components/com_adsman/js/jquery/');
		JHTML::script("validator.js",'components/com_adsman/js/validator/');
		JHTML::script("multifile.js",'components/com_adsman/js/');
		JHTML::script("adsman.js",'components/com_adsman/js/');
		JHTML::script("adedit.js",'components/com_adsman/js/');
		 
		$smarty->display("t_editadd.tpl");
	}
	
	function add_images(){
			
		$id	= JRequest::getVar('id', null );
		$my	= JFactory::getUser();
		$smarty = AdsUtilities::SmartyLoaderHelper();
		$Itemid = JRequest::getVar('Itemid', 0, 'REQUEST', 'integer');
		
		$db = JFactory::getDbo();
				
		$model = &$this->getModel( 'Adsman' ,'adsModel');
		$add =& $model->getAddData($id);

		if (!$add->id) {
			echo adsman_add_not_found; return;
		}
		
		$session	= &JFactory::getSession();
	
		//$id = $d['id'];
		$selected_images_index = $session->get("image_credit");
		$main_image_add = $session->get("main_image_add");

		$session->clear("tmp_ad","tmpadserialized");
		
		$add->images = $add->getImages();
		$existing_images = count($add->images);
		
		$credits =  generic_pricing::getCredits($my->id);
		$img_prices_array = price_pay_image_DBHelper::getPayImageAmount();
		$img_main_price = price_pay_image_DBHelper::getMainImageAmount(); //credits to pay for main img

		if ( defined("ads_opt_maxnr_images") && ads_opt_maxnr_images > 0) {
			$max_img_upload = ads_opt_maxnr_images - $existing_images;
		} else {
			$max_img_upload = 7 - $existing_images;
		}
		
		//$max_img_upload = ads_opt_maxnr_images - $existing_images;
		
		if ($max_img_upload >= 0 && $selected_images_index <= $max_img_upload)
		{
			$img_prices = ( isset($selected_images_index) && $selected_images_index!= 'none' ) ? $img_prices_array[$selected_images_index] : 0;	
			$selected_images_number 	= ( isset($selected_images_index) && $selected_images_index!= 'none' ) ? $selected_images_index : 'none';
		}
		else {
			echo JText::_("ADS_IMAGE_UPLOAD_NOT_ALLOWED") . $selected_images_index . JText::_("ADS_PICTURES"); 
			echo JText::_("ADS_IMAGE_UPLOAD_ALLOWED") . $max_img_upload . JText::_("ADS_PICTURES"); 

			$img_prices = 0;
			$selected_images_number 	= 0;
		}	
		
		$main_price = ( isset($main_image_add) && $main_image_add!= null ) ? $img_main_price : 0;			
		$needed_credits = $img_prices + $main_price;
		
		if ($credits > 0 && $credits >= $needed_credits) {
		
			$selected_images_index 		= ( isset($selected_images_index) && $selected_images_index!= 'none' ) ? $selected_images_index : 'none';
			$select_credits_main 		= ( isset($main_image_add) && $main_image_add!= null  ) ? $main_image_add : 0;
			
            JHTML::_('script', 'adsman.js', 'components/com_adsman/js/');
            JHTML::_('script', 'multifile.js', 'components/com_adsman/js/');

			$smarty->assign('add',$add);
			$smarty->assign('object_id',$id);
			$smarty->assign('my',$my);
			$smarty->assign('item_object', $this);

			$smarty->assign('select_credits_main',$select_credits_main);
			$smarty->assign('selected_images_number',$selected_images_number);
			$smarty->assign('selected_images_index',$selected_images_index);
					
			$smarty->display('t_featured_image_list.tpl');
			
		} else {
				echo JText::_("ADS_NO_CREDITS_AVAILABLE"); 
				$userpage = JRoute::_('index.php?option=com_adsman&view=user&task=myuserprofile&Itemid='. $Itemid );
				?>
				<a href="<?php echo $userpage; ?>">Select a package</a>
		<?php		
		}
	}
	

	function show_search() {
		$Itemid = JRequest::getVar('Itemid', 0, 'REQUEST', 'integer');
		$database = JFactory::getDbo();
		
		$smarty = AdsUtilities::SmartyLoaderHelper();
		
		// + TO do
		if (ads_opt_profile_mode=="ads") {
			$query	= 'SELECT c.id as value, c.name as text '
				.' FROM #__ads_users as u '
				.' LEFT JOIN #__ads_country as c ON c.id = u.country '
				.' WHERE country<>"" AND country IS NOT NULL AND country <> 0' 
			  	.' GROUP BY country ';
			  	
			$database->setQuery($query);
			
			$country[] = JHTML::_('select.option','', JText::_('ADS_ALL'));
			$country = array_merge($country, $database->loadObjectList());

			$lists['country'] = JHTML::_('select.genericlist', $country,'country','class="inputbox"  style="width:190px;" ','value','text');
				
			$query	= 'SELECT DISTINCT city as value, city as text '
				.' FROM #__ads_users '
				.' WHERE city <> "" '
				.' AND city is not null ';
			
			$database->setQuery($query);
			$city[] = JHTML::_('select.option','', JText::_('ADS_ALL'));
			$city = array_merge($city, $database->loadObjectList());
			$lists['city'] = JHTML::_('select.genericlist', $city, 'city', 'class="inputbox" style="width:190px;" ', 'value', 'text');
			
			$query	= 'SELECT DISTINCT state as value, state as text '
				.' FROM #__ads_users '
				.' WHERE state <> "" '
				.' AND state is not null ';
			
			$database->setQuery($query);
			$state[] = JHTML::_('select.option','', JText::_('ADS_ALL'));
			$state = array_merge($state, $database->loadObjectList());
			$lists['state'] = JHTML::_('select.genericlist', $state, 'state', 'class="inputbox" style="width:190px;" ', 'value', 'text');
	
		} else {
			
			if(ads_opt_profile_mode=="cb")
				$cb_fieldmap = JTheFactoryIntegration::getIntegrationArray("CB");
				
			elseif(ads_opt_profile_mode=="love")
				$cb_fieldmap = JTheFactoryIntegration::getIntegrationArray("LOVE");
				
		}
		// - TO do
		
		if (ads_opt_profile_mode=="cb" && $cb_fieldmap['country']) {
			
			$query="SELECT DISTINCT ".$cb_fieldmap['country']." as value,".$cb_fieldmap['country']." as text from #__comprofiler ".
			" where ".$cb_fieldmap['country']."<>'' and ".$cb_fieldmap['country']." is not null";
			$database->setQuery($query);
			$country[]=JHTML::_('select.option','', JText::_('ADS_ALL'));
			$country = array_merge($country, $database->loadObjectList());
			$lists['country'] = JHTML::_('select.genericlist', $country,'country','class="inputbox"  style="width:190px;" ','value','text');
	
		}
		
		if (ads_opt_profile_mode=="cb"  && $cb_fieldmap['city']) {
			$query	= "SELECT DISTINCT ".$cb_fieldmap['city']." as value,".$cb_fieldmap['city']." as text FROM #__comprofiler "
					." WHERE ".$cb_fieldmap['city']."<>'' AND ".$cb_fieldmap['city']." is not null";
			
			$database->setQuery($query);
			$city[] = JHTML::_('select.option','', JText::_('ADS_ALL'));
			$city = array_merge($city, $database->loadObjectList());
			$lists['city'] = JHTML::_('select.genericlist', $city,'city','class="inputbox"  style="width:190px;" ','value','text');
		}
		
		if (ads_opt_profile_mode=="cb"  && $cb_fieldmap['state']) {
			$query	= "SELECT DISTINCT ".$cb_fieldmap['state']." as value,".$cb_fieldmap['state']." as text FROM #__comprofiler "
					." WHERE ".$cb_fieldmap['state']."<>'' AND ".$cb_fieldmap['state']." is not null";
			
			$database->setQuery($query);
			$state[] = JHTML::_('select.option','', JText::_('ADS_ALL'));
			$state = array_merge($state, $database->loadObjectList());
			$lists['state'] = JHTML::_('select.genericlist', $state,'state','class="inputbox"  style="width:190px;" ','value','text');
		}
		
		
		$users_count = 0;
		$database->setQuery("SELECT count(id) FROM #__users");
		$users_count = $database->loadResult();
		
		/*if ($users_count <= 1000) {
			$lists["users"] = AdsUtilities::makeUsersList(null,350);
		}else{*/
			$lists["users"] = "<input type='textbox' name='username' size='30' />";
		//}
		
		$opts = array();
		
		$opts[] = JHTML::_('select.option', ADDSMAN_TYPE_ALL, JText::_('ADS_ADTYPE_ALL'));
		$opts[] = JHTML::_('select.option', ADDSMAN_TYPE_PUBLIC, JText::_('ADS_ADTYPE_PUBLIC'));
		$opts[] = JHTML::_('select.option', ADDSMAN_TYPE_PRIVATE, JText::_('ADS_ADTYPE_PRIVATE'));

		$lists['addtype'] 	= JHTML::_('select.radiolist', $opts,'ad_type','class="inputbox" ','value', 'text', ADDSMAN_TYPE_ALL,'ad_type');
	
		$jdoc = & JFactory::getDocument();
		$js = ' var AFIDid = "";';
		$jdoc->addScriptDeclaration($js);
		
		if(!defined("ads_opt_category_ajax_load") || ads_opt_category_ajax_load==0) {
			
			$lists["category"] = Ads_HTML::selectCategory("cat", array("script"=>"onchange='aiai_search(this.value);'"));
						
		} else {
			$js = 'window.addEvent("domready", function() {	categorySelectPopulate("ajax_search_selectcategories"); })';
			$page = 'var page = "search";';
			$root = JUri::root();
	
			$document	= JFactory::getDocument();
			$document->addScriptDeclaration($js);
			$document->addScriptDeclaration($page);
			$document->addScriptDeclaration(' var root = "'.$root.'";');
		}

        // ad city
        $query	= 'SELECT DISTINCT ad_city as value, ad_city as text '
				.' FROM #__ads '
				.' WHERE ad_city <> "" '
				.' AND ad_city is not null ';

        $database->setQuery($query);
        $ad_city[] = JHTML::_('select.option','', JText::_('ADS_ALL'));
        $ad_city = array_merge($ad_city, $database->loadObjectList());
		$lists['ad_city'] = JHTML::_('select.genericlist', $ad_city, 'ad_city', 'class="inputbox" style="width:190px;" ', 'value', 'text');

        // ad poscode
        $query	= 'SELECT DISTINCT ad_postcode as value, ad_postcode as text '
				.' FROM #__ads '
				.' WHERE ad_postcode <> "" '
				.' AND ad_postcode is not null ';

        $database->setQuery($query);
        $ad_postcode[] = JHTML::_('select.option','', JText::_('ADS_ALL'));
        $ad_postcode = array_merge($ad_postcode, $database->loadObjectList());
		$lists['ad_postcode'] = JHTML::_('select.genericlist', $ad_postcode, 'ad_postcode', 'class="inputbox" style="width:190px;" ', 'value', 'text');

		JHTML::_('behavior.calendar');
		
		// some section for this
        $arr_dateformat=array(
                'Y-m-d'=>'%Y-%m-%d',
                'm/d/Y'=>'%Y-%m-%d',
                'd/m/Y'=>'%Y-%m-%d',
                'd.m.Y'=>'%Y-%m-%d',
                'D, F d Y'=>'%Y-%m-%d'
        );

        $lists['start_date_calendar']   = JHTML::_('adsdate.calendar', '', 'start_date'); //$date->toMySQL()
        $lists['end_date_calendar']     = JHTML::_('adsdate.calendar', '', 'end_date'); //$date->toMySQL()
        $lists['cancel_form'] = JRoute::_( 'index.php?option=com_adsman&amp;task=listadds&amp;Itemid='. $Itemid );

        //price range
        $lists["price_min"] = "<input type='textbox' name='price_min' id='price_min' size='10' />";
        $lists["price_max"] = "<input type='textbox' name='price_max' id='price_max' size='10' />";

        $lists["currency"] = AdsUtilities::makeCurrencyDropDown(0);

		$smarty->assign("lists", $lists);
		$smarty->assign("action", JRoute::_("index.php?option=com_adsman&task=listadds&Itemid=$Itemid"));
		$smarty->display("t_search.tpl");
	}
	
	/**
	 * @since: 1.5.5
	 *
	 */
	function tellFriend_show() {

		$smarty = AdsUtilities::SmartyLoaderHelper();
		$id   = JRequest::getVar('id',null,'default','int');
		if ( !$id ){
			echo adsman_add_not_found; return;
		}

		$model = &$this->getModel( 'Adsman' ,'adsModel');
		$add =& $model->getAddData($id);
		
		$smarty->assign("ads_token", JHTML::_( 'form.token' ));
		$smarty->assign( "add", $add );
		$smarty->assign( "cs", AdsUtilities::init_captcha() );
		$smarty->display( "t_friend.tpl" );
		JHTML::_( 'behavior.formvalidation' );
		//JApplication::close();
	}


}
?>
