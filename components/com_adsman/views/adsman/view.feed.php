<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/


// no direct access
defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Ads manager Component
 *
 * @package		Ads manager
 */
class adsViewadsman extends JView
{

	function listadd($myadds=null, $favorites=null) {
		
		$app = JFactory::getApplication();
	
		// Get the page/component configuration
		$params = &$app->getParams();

		$doc     = JFactory::getDocument();
		$feedEmail = (@$app->getCfg('feed_email')) ? $app->getCfg('feed_email') : 'author';
		$siteEmail = $app->getCfg('mailfrom');
		
		$config 	= JFactory::getConfig();
		$uri 		= JFactory::getURI();
		$database 	= JFactory::getDbo();
		$task		= JRequest::getCmd( 'task','listadds' );
		
		$doc->link = JRoute::_("index.php?option=com_adsman&view=feed");

		if($myadds && !$favorites){

			$items	=& $this->get( 'MyAdds');
			$task_adds = "myadds";

		}elseif($favorites){
			$items	=& $this->get( 'Favorites');
			$task_adds = "favorites";

		}else{
			$items	=& $this->get( 'Adds');
			$task_adds = "listadds";
		}

	    $catObj = new JAdsCategories($database);
	    
		if($items)
		
	    for ($key=0; $key<count($items); $key++){
	    	$add=$items[$key];
			
			$title = $this->escape( $add->title );
			$title = html_entity_decode( $title );
			
			$item = new JFeedItem();
			
			$item->title 		= $title;
			$item->link 		= JRoute::_(JURI::root()."index.php?option=com_adsman&amp;view=adsman&amp;task=details&amp;id=$add->id");
			$item->description 	= $add->description;
			$item->date			= $add->start_date;
			$catObj->load($add->category);
			$item->category   	= $catObj->catname;
			$item->author		= $add->user_id;
			
			if ($feedEmail == 'site') {
				$item->authorEmail = $siteEmail;
			}else {
				$item->authorEmail = $row->author_email;
			}
	    	
	    	$doc->addItem($item);
		}
		
	}

}
?>
