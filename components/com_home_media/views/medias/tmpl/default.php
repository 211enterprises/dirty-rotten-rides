<?php
/**
 * @version     1.0.0
 * @package     com_home_media
 * @copyright   Copyright (C) 2014. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Allan Cutler <allan@211enterprises.com> - http://www.211enterprises.com
 */
// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.tooltip');
JHTML::_('script', 'system/multiselect.js', false, true);
// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_home_media/assets/css/list.css');

$user = JFactory::getUser();
$userId = $user->get('id');
$listOrder = $this->state->get('list.ordering');
$listDirn = $this->state->get('list.direction');
$ordering = ($listOrder == 'a.ordering');
$canCreate = $user->authorise('core.create', 'com_home_media');
$canEdit = $user->authorise('core.edit', 'com_home_media');
$canCheckin = $user->authorise('core.manage', 'com_home_media');
$canChange = $user->authorise('core.edit.state', 'com_home_media');
$canDelete = $user->authorise('core.delete', 'com_home_media');
?>

<form action="<?php echo JRoute::_('index.php?option=com_home_media&view=medias'); ?>" method="post" name="adminForm" id="adminForm">
    <table class="front-end-list">
        <thead>
            <tr>
                
				<th class="align-left">
					<?php echo JHtml::_('grid.sort',  'COM_HOME_MEDIA_MEDIAS_URL', 'a.url', $listDirn, $listOrder); ?>
				</th>

				<th class="align-left">
					<?php echo JHtml::_('grid.sort',  'COM_HOME_MEDIA_MEDIAS_IMAGE_URL', 'a.image_url', $listDirn, $listOrder); ?>
				</th>

				<th class="align-left">
					<?php echo JHtml::_('grid.sort',  'COM_HOME_MEDIA_MEDIAS_TYPE', 'a.type', $listDirn, $listOrder); ?>
				</th>

				<th class="align-left">
					<?php echo JHtml::_('grid.sort',  'COM_HOME_MEDIA_MEDIAS_CREATED_BY', 'a.created_by', $listDirn, $listOrder); ?>
				</th>

                <?php if (isset($this->items[0]->state)) : ?>
                    <th class="align-left">
                        <?php echo JHtml::_('grid.sort', 'JPUBLISHED', 'a.state', $listDirn, $listOrder); ?>
                    </th>
                <?php endif; ?>

                <?php if (isset($this->items[0]->id)) : ?>
                    <th class="nowrap align-left">
                        <?php echo JHtml::_('grid.sort', 'JGRID_HEADING_ID', 'a.id', $listDirn, $listOrder); ?>
                    </th>
                <?php endif; ?>

                				<?php if ($canEdit || $canDelete): ?>
					<th class="align-center">
				<?php echo JText::_('COM_HOME_MEDIA_MEDIAS_ACTIONS'); ?>
				</th>
				<?php endif; ?>

            </tr>
        </thead>
        <tfoot>
            <tr>
                <td colspan="<?php echo isset($this->items[0]) ? count(get_object_vars($this->items[0])) : 10; ?>">
                    <?php echo $this->pagination->getListFooter(); ?>
                </td>
            </tr>
        </tfoot>
        <tbody>
            <?php foreach ($this->items as $i => $item) : ?>
                <?php $canEdit = $user->authorise('core.edit', 'com_home_media'); ?>
            
                				<?php if (!$canEdit && $user->authorise('core.edit.own', 'com_home_media')): ?>
					<?php $canEdit = JFactory::getUser()->id == $item->created_by; ?>
				<?php endif; ?>


                <tr class="row<?php echo $i % 2; ?>">
                    
					<td>
					<?php if (isset($item->checked_out) && $item->checked_out) : ?>
						<?php echo JHtml::_('jgrid.checkedout', $i, $item->editor, $item->checked_out_time, 'medias.', $canCheckin); ?>
					<?php endif; ?>
					<?php if ($canEdit): ?>
						<a href="<?php echo JRoute::_('index.php?option=com_home_media&task=media.edit&id=' . (int) $item->id); ?>">

							<?php echo $this->escape($item->url); ?>
						</a>
					<?php else: ?>
						<?php echo $this->escape($item->url); ?>
					<?php endif; ?>
					</td>

					<td>
						<?php if (!empty($item->image_url)): ?>
							$uploadPath = 'administrator' . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'com_home_media' . DIRECTORY_SEPARATOR . 'uploads' .DIRECTORY_SEPARATOR . $item->image_url;
							<a href="<?php echo JRoute::_(JUri::base() . $uploadPath, false); ?>" target="_blank" title="See the image_url"><?php echo $item->image_url; ?></a>
						<?php else: ?>
							<?php echo $item->image_url; ?>
						<?php endif; ?>
					</td>

					<td>
						<?php echo $item->type; ?>
					</td>

					<td>
							<?php echo JFactory::getUser($item->created_by)->name; ?>
					</td>

                    <?php if (isset($this->items[0]->state)) : ?>
                        <td class="align-left">
                            <button type="button" <?php echo ($canEdit || $canChange) ? 'onclick="window.location.href=\'' . JRoute::_('index.php?option=com_home_media&task=media.publish&id=' . $item->id . '&state=' . (($item->state + 1) % 2) . '\'"', false, 2) : 'disabled="disabled"'; ?>>
                                <?php if ($item->state == 1): ?>
                                    <?php echo JText::_('JPUBLISHED'); ?>
                                <?php else: ?>
                                    <?php echo JText::_('JUNPUBLISHED'); ?>
                                <?php endif; ?>
                            </button>
                        </td>
                    <?php endif; ?>
                    <?php if (isset($this->items[0]->id)) : ?>
                        <td class="align-left">
                            <?php echo (int) $item->id; ?>
                        </td>
                    <?php endif; ?>

                    				<?php if ($canEdit || $canDelete): ?>
					<td class="align-center">
						<?php if ($canEdit): ?>
							<button onclick="window.location.href = '<?php echo JRoute::_('index.php?option=com_home_media&task=media.edit&id=' . $item->id, false, 2); ?>';" class="btn btn-mini" type="button"><?php echo JText::_('COM_HOME_MEDIA_MEDIAS_EDIT'); ?></button>
						<?php endif; ?>
						<?php if ($canDelete): ?>
							<button data-item-id="<?php echo $item->id; ?>" class="delete-button" type="button"><?php echo JText::_('COM_HOME_MEDIA_MEDIAS_DELETE'); ?></button>
						<?php endif; ?>
					</td>
				<?php endif; ?>

                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <?php if ($canCreate): ?>
        <button type="button" onclick="window.location.href = '<?php echo JRoute::_('index.php?option=com_home_media&task=media.edit&id=0', false, 2); ?>';"><?php echo JText::_('COM_HOME_MEDIA_ADD_ITEM'); ?></button>
    <?php endif; ?>

    <div>
        <input type="hidden" name="task" value="" />
        <input type="hidden" name="boxchecked" value="0" />
        <input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
        <input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
        <?php echo JHtml::_('form.token'); ?>
    </div>
</form>

<script type="text/javascript">
    if (typeof jQuery == 'undefined') {
        var headTag = document.getElementsByTagName("head")[0];
        var jqTag = document.createElement('script');
        jqTag.type = 'text/javascript';
        jqTag.src = '//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js';
        jqTag.onload = jQueryCode;
        headTag.appendChild(jqTag);
    } else {
        jQueryCode();
    }

    function jQueryCode() {
        jQuery('.delete-button').click(function() {
            var item_id = jQuery(this).attr('data-item-id');
            if (confirm("<?php echo JText::_('COM_HOME_MEDIA_DELETE_MESSAGE'); ?>")) {
                window.location.href = '<?php echo JRoute::_('index.php?option=com_home_media&task=media.remove&id=', false, 2) ?>' + item_id;
            }
        });
    }

</script>