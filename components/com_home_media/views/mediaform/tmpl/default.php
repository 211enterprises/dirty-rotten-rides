<?php
/**
 * @version     1.0.0
 * @package     com_home_media
 * @copyright   Copyright (C) 2014. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Allan Cutler <allan@211enterprises.com> - http://www.211enterprises.com
 */
// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');

//Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_home_media', JPATH_ADMINISTRATOR);
$doc = JFactory::getDocument();
$doc->addStyleSheet(JUri::base() . '/components/com_home_media/assets/css/form.css');
$doc->addScript(JUri::base() . '/components/com_home_media/assets/js/form.js');
?>

<script type="text/javascript">

    getScript('//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js', function() {
        jQuery(document).ready(function() {
            jQuery('#form-media').submit(function(event) {
                
				if(jQuery('#jform_image_url').val() !== ''){
					jQuery('#jform_image_url_hidden').val(jQuery('#jform_image_url').val());
				}
            });

            
        });
    });

</script>

<div class="media-edit front-end-edit">
    <?php if (!empty($this->item->id)): ?>
        <h1>Edit <?php echo $this->item->id; ?></h1>
    <?php else: ?>
        <h1>Add</h1>
    <?php endif; ?>

    <form id="form-media" action="<?php echo JRoute::_('index.php?option=com_home_media&task=media.save'); ?>" method="post" class="form-validate" enctype="multipart/form-data">
        				<li><?php echo $this->form->getLabel('id'); ?>
				<?php echo $this->form->getInput('id'); ?></li>
				<li><?php echo $this->form->getLabel('url'); ?>
				<?php echo $this->form->getInput('url'); ?></li>
				<li><?php echo $this->form->getLabel('image_url'); ?>
				<?php echo $this->form->getInput('image_url'); ?></li>

				<?php if (!empty($this->item->image_url)) : ?>
						<a href="<?php echo JRoute::_(JUri::base() . 'administrator' . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'com_home_media' . DIRECTORY_SEPARATOR . 'uploads' .DIRECTORY_SEPARATOR . $this->item->image_url, false);?>"><?php echo JText::_("COM_HOME_MEDIA_VIEW_FILE"); ?></a>
				<?php endif; ?>
				<input type="hidden" name="jform[image_url]" id="jform_image_url_hidden" value="<?php echo $this->item->image_url ?>" />				<li><?php echo $this->form->getLabel('type'); ?>
				<?php echo $this->form->getInput('type'); ?></li>
				<li><?php echo $this->form->getLabel('created_at'); ?>
				<?php echo $this->form->getInput('created_at'); ?></li>
				<li><?php echo $this->form->getLabel('created_by'); ?>
				<?php echo $this->form->getInput('created_by'); ?></li>
        
        <div class="button-div">
            <button type="submit" class="validate"><span><?php echo JText::_('JSUBMIT'); ?></span></button>
            <a href="<?php echo JRoute::_('index.php?option=com_home_media&task=mediaform.cancel'); ?>" title="<?php echo JText::_('JCANCEL'); ?>"><?php echo JText::_('JCANCEL'); ?></a>            
        </div>

        <input type="hidden" name="option" value="com_home_media" />
        <input type="hidden" name="task" value="mediaform.save" />
        <?php echo JHtml::_('form.token'); ?>
    </form>
</div>
