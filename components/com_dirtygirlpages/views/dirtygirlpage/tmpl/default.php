<?php
/**
 * @version     1.0.0
 * @package     com_dirtygirlpages
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Allan Cutler <allan@211enterprises.com> - http://www.211enterprises.com
 */
// no direct access
defined('_JEXEC') or die;

//Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_dirtygirlpages', JPATH_ADMINISTRATOR);

$uploadPath = 'administrator' . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'com_dirtygirlpages' . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR;
?>
<style type="text/css">
	.item_fields {
		padding: 30px;
	}
	#wrapper2 #main h1 {
		color: #FFF;
		text-align: center;
		font: 16px Helvetica, Arial;
		text-transform: uppercase;
	}
	#wrapper2 #main h2 {
		color: #F4CB89;
		text-align: center;
		font: 60px 'halo', Helvetica, Arial;
	}
	#wrapper2 #main h3 {
		color: #F4CB89;
		font: 40px 'halo', Helvetica, Arial;
	}
	hr {
		border-color: #B7272D;
		margin: 10px 0;
		clear: both;
	}
	.winnerBio {
		font: 16px 'Titillium Maps', Arial;
	}
	ul.fields_list {
		display: none;
	}
	.dirty-gallery {
		float: left;
		margin-top: 10px;
		margin-bottom: 10px;
	}
	.dirty-gallery a:Link,
	.dirty-gallery a:Visited {
		position: relative;
		float: left;
		background: #000;
		width: 200px;
		height: 150px;
		margin: 0 3px 5px 2px;
		border: 5px #FFF solid;
		overflow: hidden;
	}
	.dirty-gallery a:Hover img {
		opacity: 0.6;
		filter: alpha(opacity=60);
		zoom: 1;
	}
	.dirty-gallery a:Hover .zoom {
		opacity: 1;
		filter: alpha(opacity=100);
		zoom: 1;
	}
	.dirty-gallery a img {
		min-width: 200px;
		max-width: 200px;
		min-height: 150px;
		-webkit-transition: opacity .5s ease-in-out;
	  -moz-transition: opacity .5s ease-in-out;
	  -ms-transition: opacity .5s ease-in-out;
	  -o-transition: opacity .5s ease-in-out;
	  transition: opacity .5s ease-in-out;
	}
	.dirty-gallery a .zoom {
		position: absolute;
		top: 50%;
		left: 50%;
		background: url('/templates/dirtygirls/images/magnifier.png') no-repeat;
		width: 32px;
		height: 31px;
		margin: -16px 0 0 -15px;
		-webkit-transition: opacity .5s ease-in-out;
	  -moz-transition: opacity .5s ease-in-out;
	  -ms-transition: opacity .5s ease-in-out;
	  -o-transition: opacity .5s ease-in-out;
	  transition: opacity .5s ease-in-out;
		opacity: 0;
	}
	#main ul li {
		margin-left: 8px;
	}
</style>

<?php if ($this->item) : ?>

<?php if($this->item->image_1 || $this->item->image_2 || $this->item->image_3 || $this->item->image_4 || $this->item->image_5) { ?>
<link href="/templates/system/css/lightbox.css" rel="stylesheet" />
<script src="/templates/system/js/jquery-1.10.2.min.js"></script>
<script src="/templates/system/js/lightbox-2.6.min.js"></script>
<?php } ?>

	
    <div class="item_fields">
			
		<?php if($this->item->dirty_type == 0) { ?>
			<h1>Dirty Girl <?php echo $this->item->campaign_month; ?> <?php echo $this->item->campaign_year; ?>:</h1>
		<?php } ?>
			<h2><?php echo $this->item->dirty_girl_name; ?></h2>
			<hr />
			
			<div class="dirty-gallery">
			<?php if($this->item->image_1) { ?>
				<a href="<?php echo JRoute::_(JUri::base() . $uploadPath, false) . $this->item->image_1; ?>" alt="<?php echo $this->item->image_1; ?>" data-lightbox="dirtygirl">
					<img src="<?php echo JRoute::_(JUri::base() . $uploadPath, false) . $this->item->image_1; ?>" alt="<?php echo $this->item->image_1; ?>" />
					<div class="zoom"></div>
				</a>
			<?php } ?>
			<?php if($this->item->image_2) { ?>
				<a href="<?php echo JRoute::_(JUri::base() . $uploadPath, false) . $this->item->image_2; ?>" alt="<?php echo $this->item->image_2; ?>" data-lightbox="dirtygirl">
					<img src="<?php echo JRoute::_(JUri::base() . $uploadPath, false) . $this->item->image_2; ?>" alt="<?php echo $this->item->image_2; ?>" />
					<div class="zoom"></div>
				</a>
			<?php } ?>
			<?php if($this->item->image_3) { ?>
				<a href="<?php echo JRoute::_(JUri::base() . $uploadPath, false) . $this->item->image_3; ?>" alt="<?php echo $this->item->image_3; ?>" data-lightbox="dirtygirl">
					<img src="<?php echo JRoute::_(JUri::base() . $uploadPath, false) . $this->item->image_3; ?>" alt="<?php echo $this->item->image_3; ?>" />
					<div class="zoom"></div>
				</a>
			<?php } ?>
			<?php if($this->item->image_4) { ?>
				<a href="<?php echo JRoute::_(JUri::base() . $uploadPath, false) . $this->item->image_4; ?>" alt="<?php echo $this->item->image_4; ?>" data-lightbox="dirtygirl">
					<img src="<?php echo JRoute::_(JUri::base() . $uploadPath, false) . $this->item->image_4; ?>" alt="<?php echo $this->item->image_4; ?>" />
					<div class="zoom"></div>
				</a>
			<?php } ?>
			<?php if($this->item->image_5) { ?>
				<a href="<?php echo JRoute::_(JUri::base() . $uploadPath, false) . $this->item->image_5; ?>" alt="<?php echo $this->item->image_5; ?>" data-lightbox="dirtygirl">
					<img src="<?php echo JRoute::_(JUri::base() . $uploadPath, false) . $this->item->image_5; ?>" alt="<?php echo $this->item->image_5; ?>" />
					<div class="zoom"></div>
				</a>
			<?php } ?>
			</div>
			
			<hr />
			
		<?php if($this->item->dirty_type == 0) { ?>
			<h3>About Your Winner:</h3>
		<?php } else { ?>
			<h3>About <?php echo $this->item->dirty_girl_name; ?>:</h3>	
		<?php } ?>
			
			<div class="winnerBio">
				<?php echo str_replace(array("\r\n", "\n", "\r"), '<br />', $this->item->dirty_girl_bio); ?>
			</div>
			
      <ul class="fields_list">
				<li><?php echo JText::_('COM_DIRTYGIRLPAGES_FORM_LBL_DIRTYGIRLPAGE_ID'); ?>:
				<?php echo $this->item->id; ?></li>
				<li><?php echo JText::_('COM_DIRTYGIRLPAGES_FORM_LBL_DIRTYGIRLPAGE_CAMPAIGN_MONTH'); ?>:
				<?php echo $this->item->campaign_month; ?></li>
				<li><?php echo JText::_('COM_DIRTYGIRLPAGES_FORM_LBL_DIRTYGIRLPAGE_CAMPAIGN_YEAR'); ?>:
				<?php echo $this->item->campaign_year; ?></li>
				<li><?php echo JText::_('COM_DIRTYGIRLPAGES_FORM_LBL_DIRTYGIRLPAGE_DIRTY_GIRL_NAME'); ?>:
				<?php echo $this->item->dirty_girl_name; ?></li>
				<li><?php echo JText::_('COM_DIRTYGIRLPAGES_FORM_LBL_DIRTYGIRLPAGE_DIRTY_GIRL_BIO'); ?>:
				<?php echo $this->item->dirty_girl_bio; ?></li>
				<li><?php echo JText::_('COM_DIRTYGIRLPAGES_FORM_LBL_DIRTYGIRLPAGE_THUMBNAIL_IMAGE'); ?>:
	
				<?php 
					$uploadPath = 'administrator' . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'com_dirtygirlpages' . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . $this->item->thumbnail_image;
				?>
				<a href="<?php echo JRoute::_(JUri::base() . $uploadPath, false); ?>" target="_blank"><?php echo $this->item->thumbnail_image; ?></a></li>			<li><?php echo JText::_('COM_DIRTYGIRLPAGES_FORM_LBL_DIRTYGIRLPAGE_IMAGE_1'); ?>:
	
				<?php 
					$uploadPath = 'administrator' . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'com_dirtygirlpages' . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . $this->item->image_1;
				?>
				<a href="<?php echo JRoute::_(JUri::base() . $uploadPath, false); ?>" target="_blank"><?php echo $this->item->image_1; ?></a></li>			<li><?php echo JText::_('COM_DIRTYGIRLPAGES_FORM_LBL_DIRTYGIRLPAGE_IMAGE_2'); ?>:
	
				<?php 
					$uploadPath = 'administrator' . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'com_dirtygirlpages' . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . $this->item->image_2;
				?>
				<a href="<?php echo JRoute::_(JUri::base() . $uploadPath, false); ?>" target="_blank"><?php echo $this->item->image_2; ?></a></li>			<li><?php echo JText::_('COM_DIRTYGIRLPAGES_FORM_LBL_DIRTYGIRLPAGE_IMAGE_3'); ?>:
	
				<?php 
					$uploadPath = 'administrator' . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'com_dirtygirlpages' . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . $this->item->image_3;
				?>
				<a href="<?php echo JRoute::_(JUri::base() . $uploadPath, false); ?>" target="_blank"><?php echo $this->item->image_3; ?></a></li>			<li><?php echo JText::_('COM_DIRTYGIRLPAGES_FORM_LBL_DIRTYGIRLPAGE_IMAGE_4'); ?>:
	
				<?php 
					$uploadPath = 'administrator' . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'com_dirtygirlpages' . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . $this->item->image_4;
				?>
				<a href="<?php echo JRoute::_(JUri::base() . $uploadPath, false); ?>" target="_blank"><?php echo $this->item->image_4; ?></a></li>			<li><?php echo JText::_('COM_DIRTYGIRLPAGES_FORM_LBL_DIRTYGIRLPAGE_IMAGE_5'); ?>:
	
				<?php 
					$uploadPath = 'administrator' . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'com_dirtygirlpages' . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . $this->item->image_5;
				?>
				<a href="<?php echo JRoute::_(JUri::base() . $uploadPath, false); ?>" target="_blank"><?php echo $this->item->image_5; ?></a></li>			<li><?php echo JText::_('COM_DIRTYGIRLPAGES_FORM_LBL_DIRTYGIRLPAGE_ORDERING'); ?>:
				<?php echo $this->item->ordering; ?></li>
				<li><?php echo JText::_('COM_DIRTYGIRLPAGES_FORM_LBL_DIRTYGIRLPAGE_CREATED_AT'); ?>:
				<?php echo $this->item->created_at; ?></li>


			</ul>
    </div>
    
<?php
else:
    echo JText::_('COM_DIRTYGIRLPAGES_ITEM_NOT_LOADED');
endif;
?>
