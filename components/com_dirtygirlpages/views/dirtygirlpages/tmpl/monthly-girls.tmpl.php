<div class="items">
    <ul class="items_list">
<?php
	$show = false;
	$uploadPath = 'administrator' . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'com_dirtygirlpages' . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . $this->item->thumbnail_image;
?>
	<?php foreach ($this->items as $item) : ?>
            
			<?php $show = true; ?>
			<?php if($item->dirty_type == 0) { ?>
				<li>
					<div class="featured-container">
						<div class="featured-name"><?php echo $item->campaign_month; ?></div>
						<div class="featured-image">
							<div class="image">
								<a href="<?php echo JRoute::_('index.php?option=com_dirtygirlpages&view=dirtygirlpage&id=' . (int)$item->id); ?>">
									<img src="<?php echo JRoute::_(JUri::base() . $uploadPath, false) . $item->thumbnail_image; ?>" alt="<?php echo $item->dirty_girl_name; ?>" />
								</a>
							</div>
							<div class="featureMount topLeft"></div>
							<div class="featureMount topRight"></div>
							<div class="featureMount bottomLeft"></div>
							<div class="featureMount bottomRight"></div>
						</div>
					</div>
				</li>
			<?php } ?>

<?php endforeach; ?>
        <?php
        if (!$show):
            echo JText::_('COM_DIRTYGIRLPAGES_NO_ITEMS');
        endif;
        ?>
    </ul>
</div>