<?php
/**
 * @version     1.0.0
 * @package     com_dirtygirlpages
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Allan Cutler <allan@211enterprises.com> - http://www.211enterprises.com
 */
// no direct access
defined('_JEXEC') or die;
?>
<style type="text/css">
	#main ul {
		float: left;
		width: 100%;
		padding: 0;
		margin: 40px 0;
	}
	#main ul li {
		margin-bottom: 30px;
	}
	ul.items_list li {
		float: left;
		width: 33%;
		list-style: none;
	}
	.featured-container {
		width: 240px;
		margin: 0 auto;
	}
	
	.featured-image {
		position: relative;
		float: left;
		background: #000;
		width: 240px;
		height: 300px;
		text-align: center;
	}
	.image {
		width: 240px;
		height: 300px;
		overflow: hidden;
		-webkit-transition: opacity .4s ease-in-out;
		-moz-transition: opacity .4s ease-in-out;
		-ms-transition: opacity .4s ease-in-out;
		-o-transition: opacity .4s ease-in-out;
		transition: opacity .4s ease-in-out;
	}
	.image:Hover {
		zoom: 1;
		filter: alpha(opacity=65);
		opacity: 0.65;	
	}
	.featured-name {
		width: 100%;
		color: #F4CB89;
		font-size: 32px;
		font-family: 'halo', Helvetica, Arial;
		text-align: center;
		padding-bottom: 10px;
	}
	
	.featured-image img {
		min-width: 240px;
		height: 300px;
	}
	
	div.featureMount {
		position: absolute;
		background: url('/templates/dirtygirls/images/photoMounts.png') 0 0 no-repeat;
		width: 38px;
		height: 38px;
		overflow: hidden;
		z-index: 2;
	}
	
	div.featureMount.topLeft {
		background-position: 0 0;
		top: -16px;
		left: -16px;
	}
	
	div.featureMount.topRight {
		background-position: right 0;
		top: -16px;
		right: -16px;
	}
	
	div.featureMount.bottomLeft {
		background-position: 0 bottom;
		bottom: -16px;
		left: -16px;
	}
	
	div.featureMount.bottomRight {
		background-position: right bottom;
		bottom: -16px;
		right: -16px;
	}
	div.pagination {
		border-color: #000;
	}
	#wrapper2 #main h1 {
		text-align: center;
		font: 40px 'halo', Helvetica, Arial;
	}
</style>

<figure class="nextDirtyGirl">
	<img src="/templates/dirtygirls/images/nextDirtyGirl.gif" alt="" />
</figure>

<h1 class="dirtyGirlHeader">Dirty Girls Crew</h1>
<hr />

<?php include_once('crew-girls.tmpl.php'); ?>

<h1 class="dirtyGirlHeader">Dirty Girls of the Month</h1>
<hr />

<?php include_once('monthly-girls.tmpl.php'); ?>

<?php if ($show): ?>
    <div class="pagination">
        <p class="counter">
            <?php echo $this->pagination->getPagesCounter(); ?>
        </p>
        <?php echo $this->pagination->getPagesLinks(); ?>
    </div>
<?php endif; ?>

