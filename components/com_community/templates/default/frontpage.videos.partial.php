<?php if($config->get('enablevideos')) { ?>
  <?php if($this->params->get('showlatestvideos') ){ ?>
    <?php if( $this->params->get('showlatestvideos') == '1' || ($this->params->get('showlatestvideos') == '2' && $my->id != 0 ) ) { ?>
      <div class="cModule cFrontPage-LatestVideos app-box">
        <h3 class="app-box-header">
          <?php echo JText::_('COM_COMMUNITY_VIDEOS'); ?>
        </h3>
        <div id="latest-videos-nav" class="app-box-filter">
          <i class="loading cFloat-R"></i>
          <div>
            <a class="newest-videos active-state" href="javascript:void(0);"><?php echo JText::_('COM_COMMUNITY_VIDEOS_NEWEST') ?></a>
            <b>&middot;</b>
            <a class="featured-videos" href="javascript:void(0);"><?php echo JText::_('COM_COMMUNITY_FEATURED') ?></a>
            <b>&middot;</b>
            <a class="popular-videos" href="javascript:void(0);"><?php echo JText::_('COM_COMMUNITY_VIDEOS_POPULAR') ?></a>
          </div>
        </div>
        <div id="latest-videos-container" class="app-box-content">
          <?php echo $latestVideosHTML;?>
        </div>

        <div class="app-box-footer">
          <a href="<?php echo CRoute::_('index.php?option=com_community&view=videos'); ?>"><?php echo JText::_('COM_COMMUNITY_VIDEOS_ALL'); ?></a>
        </div>
      </div>
    <?php } ?>
  <?php } ?>
<?php } ?>