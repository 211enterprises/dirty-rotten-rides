jQuery(function($) {
	
	$.fn.toggleTop3 = function() {
		var elem = $(this);
		
		elem.click(function() {
			$('h3.currentVoteLeaders').fadeToggle('slow');
			$('div.contestantImg').fadeToggle('slow');
			$('ul.polling_ul li:nth-child(1), ul.polling_ul li:nth-child(2), ul.polling_ul li:nth-child(3)').toggleClass('top3');
		});
	};
	
	$('span.polling_bottom_wrapper1, div.sexyback_icon').toggleTop3();
});