<?php
// no direct access
defined('_JEXEC') or die('Restircted access');

$doc = JFactory::getDocument();

$doc->addStyleSheet('/templates/system/css/lightbox.css');
$doc->addScript('/templates/system/js/jquery-1.10.2.min.js');
$doc->addScript('/templates/system/js/lightbox-2.6.min.js');
?>

<?php if(sizeof($pollings) > 0) { ?>
<section class="votingImages">
	<ul>
<?php 
	foreach ($pollings as $poll_index => $img) {
		foreach ($img as $k => $contestant) {
			if($contestant->answer_thumbnail != "") {
				$userLink = '';
			
				if($contestant->answer_username != "") {
					$user =& JFactory::getUser( $contestant->answer_username );
					if($user->username != "") {
						$userLink = '<a href="/index.php/'. $user->id .'-'. $user->username .'/profile">View Profile</a>';
					}
				} ?>
				<li>
					<figure>
						<div class="container">
							<a href="<?php echo $contestant->answer_thumbnail; ?>" data-lightbox="voting" title="<?php echo $contestant->answer_name;
								echo ($contestant->answer_caption ? ' - ' . htmlspecialchars($contestant->answer_caption) : ''); ?>">
								<img src="<?php echo $contestant->answer_thumbnail; ?>" alt="<?php echo $contestant->answer_name; ?>" />
							</a>
						</div>
						<figcaption>
							<?php echo $contestant->answer_name; ?><br />
							<?php echo $userLink; ?>
						</figcaption>
					</figure>
				</li>
<?php } } } ?>
	</ul>
</section>
<?php } ?>