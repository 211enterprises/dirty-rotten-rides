<?php
// no direct access
defined('_JEXEC') or die;
jimport('joomla.application.component.helper');

$user	= JFactory::getUser();
?>
<div class="votingSubHeader">
	<h3>Who wants to get Dirty?</h3>
	Dirty Rotten Rides is looking for a pinup each month to feature! We will look over all submissions and pick girls based on photo quality and answers in the form. Voting will be by members on the site. The 3 pinups with the most votes will need to submit a video at the end and one girl will be chosen by the DRR crew. That girl will be the pinup for the following month and receive a Dirty Girl jacket, and some DRR apparel. That girl may also be asked to attend events with us and might be in the 2015 calendar.  We will also select from the Dirty girls for promotions, events, calendars, and posters.<br><br>
<?php 
	if($user->authorise('core.login.offline')) {
		echo '<a href="/index.php/dirtygirl-submit">Click here to submit now!</a>';
	} else {
		echo '<b>You must be a registered user to submit your photos!</b><br>';
		echo '<a href="/index.php/register">Click here to register now!</a>';
	}
?>

	<hr class="votingHR" />
	<span class="votePermission">Must be signed up and logged in to vote!</span>
	<hr class="votingHR" />

</div>