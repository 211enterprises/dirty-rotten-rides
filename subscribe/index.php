<?php
  // Include config to fetch defines
  require_once('config.php');

  // Google Cloud SQL instance
  $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

  $error = null;
  $success = false;

  // Process user interest
  if(isset($_POST['name']) && isset($_POST['email'])) {
    if($_POST['name'] != "" && $_POST['email'] != "") {
      $error = null;

      if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
        // Check if email has been used
        $email = $mysqli->query("SELECT `email` FROM `user_subscribe` WHERE `email` = '".$mysqli->real_escape_string($_POST['email'])."'");
        if($email->num_rows == 0) {
          $mysqli->query("INSERT INTO `user_subscribe` (`name`, `email`, `created_at`) VALUES ('".$mysqli->real_escape_string($_POST['name'])."', '".$mysqli->real_escape_string($_POST['email'])."', NOW())");
        }
        $success = true;
      }
      else {
        $error = "Please provide a validate email.";
      }
    }
    else {
      $error = "Please provide your name and email.";
    }
  }
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Dirty Rotten Rides | Subscribe</title>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet" />
    <link href="style.css" rel="stylesheet" />
  </head>
  <body>
    <div class="container">
      <div class="row-fluid">
        <div class="col-md-12">
          <img src="images/drrBrand.png" style="margin: 0 auto;" />
        </div>
        <div class="col-md-6" style="padding-bottom: 20px;">
          <img src="images/dirtyGirls.png" alt="Dirty girls" />
        </div>
        <div class="col-md-6">
        <?php if(!$success) { ?>
          <img src="images/signUp.png" alt="Sign up for DRR news." />
        <?php } else { ?>
          <img src="images/thankYou.png" alt="Thank you for your interest." />
        <?php } ?>

        <?php if(!$success) { ?>
          <form action="" method="post">
          <?php if(!is_null($error)) { ?>
            <div class="col-md-12" style="padding: 30px 0 0 0; margin-top: -10px; margin-bottom: -30px;">
              <div class="alert alert-danger">
                <?php echo $error; ?>
              </div>
            </div>
          <?php } ?>

            <div class="col-md-12" style="padding: 30px 0 0 0; margin-bottom: 10px;">
              <input type="text" class="form-control" name="name" placeholder="Name:" value="<?php echo $_POST['name']; ?>" required />
            </div>
            <div class="col-md-12" style="padding: 0; margin-bottom: 10px;">
              <input type="email" class="form-control" name="email" placeholder="Email Address:" value="<?php echo $_POST['email']; ?>" required />
            </div>
            <div class="col-md-12" style="padding: 0;">
              <button type="submit" class="clearfix">Submit</button>
              <div class="clearfix"></div>
            </div>
          </form>
        <?php } ?>
        </div>
      </div>
    </div>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
  </body>
</html>