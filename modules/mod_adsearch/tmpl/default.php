<?php
/**------------------------------------------------------------------------
mod_adsearch -  Ads Factory 3.4.0
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

// no direct access
defined('_JEXEC') or die('Restricted access'); ?>
<form action="index.php?option=com_adsman&view=adsman&task=listadds&Itemid=2" method="post">
	<div class="search<?php echo $params->get('moduleclass_sfx') ?>">
	<table class="ads_table" width="100%" cellpadding="0" cellspacing="0" >
		<tr>
			<td>
				<input name="keyword" size="28" /> <input type="submit" value="<?php echo JText::_('ADS_SEARCH'); ?>" value="<?php echo (isset($val))? $val : '';?>" />
			</td>
		</tr>
	</table>
	</div>
	<input type="hidden" name="view"   value="adsman" />
	<input type="hidden" name="task"   value="listadds" />
	<input type="hidden" name="option" value="com_adsman" />
	<input type="hidden" name="Itemid" value="2" />
</form>