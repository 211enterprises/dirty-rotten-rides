<?php
/**------------------------------------------------------------------------
mod_adsearch -  Ads Factory 3.4.0
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// Include the syndicate functions only once
require_once( dirname(__FILE__).DS.'helper.php' );

$lang=JFactory::getLanguage();
$lang->load('mod_adsman');

$layout = $params->get('layout', 'default');
$layout = JFilterInput::clean($layout, 'word');

require(JModuleHelper::getLayoutPath('mod_adsearch', $layout));
