<?php
/**------------------------------------------------------------------------
mod_addcattree -  Ads Factory 3.3.5
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

// no direct access
defined('_JEXEC') or die( 'Restricted access' );
?>


<link rel="stylesheet" href="<?php echo JURI::root(); ?>/components/com_adsman/js/jquery.treeview.css" />
<?php
$jdoc = JFactory::getDocument();

/**
 * JQuery Loading
 */
$j_loaded = false;
$jf_loaded = false;

foreach ($jdoc->_scripts as $j => $jj) {
	if (strstr($j, "jquery.js") !== false) {
		$j_loaded = true;
		break;
	}

    if (strstr($j, "jquery-factory.js") !== false) {
        $jf_loaded = true;
        break;
    }
}

if ($j_loaded == false) {
    $jdoc->addScript(JURI::root().'components/com_adsman/js/jquery/jquery.js');
}

if ($jf_loaded == false) {
    $jdoc->addScript(JURI::root().'components/com_adsman/js/jquery/jquery-factory.js');
}

$jdoc->addScript(JURI::root().'components/com_adsman/gallery/js/jqueryFactory.js');
$jdoc->addScript(JURI::root().'components/com_adsman/js/jquery.cookie.js');

?>

<script src="<?php echo JURI::root(); ?>components/com_adsman/js/jquery.treeview.js" type="text/javascript"></script>
<script type="text/javascript">
    jQueryFactory(document).ready(function($){
		$("#browser").treeview({
			collapsed: true,
			animated: 250,
			persist: "cookie"
		});
    document.getElementById('browser').style.display='block';
});

if(typeof window.jQuery != 'undefined') {
    jQuery.noConflict();
}
</script>

<script src="<?php echo JURI::root(); ?>components/com_adsman/js/jquery/jquery.noconflict.js" type="text/javascript"></script>

<?php
require_once (JPATH_ROOT.DS.'components'.DS.'com_adsman'.DS.'helpers'.DS.'route.php');
$Itemid = AdsmanHelperRoute::getMenuItemByTaskName("listcats");

$database = &JFactory::getDBO();
$database->setQuery("select * FROM #__ads_categories WHERE parent='0' ORDER BY ordering,catname");
$parentcats = $database->loadObjectList();

$allow_counter		= (int) $params->get('category_counter', 0);

echo "<ul id='browser' class='filetree closed' style='display:none'>";
for ($i=0; $i<count($parentcats); $i++) {
		
    $database->setQuery("SELECT * FROM #__ads_categories WHERE parent='".$parentcats[$i]->id."' order by ordering,catname");
    $subcats = $database->loadObjectList();
    if ($allow_counter){
			$database->setQuery("SELECT count(*) FROM #__ads a
            LEFT JOIN #__ads_categories c on a.category=c.id  WHERE  a.close_by_admin = 0 AND a.closed =0  and (
            category='{$parentcats[$i]->id}' or parent='{$parentcats[$i]->id}' )
            ");
        $counter_cat = $database->loadResult();
    }

    $link_cat = JRoute::_("index.php?option=com_adsman&task=listadds&Itemid={$Itemid}&cat=".$parentcats[$i]->id."&parent=0");
	
    if ($allow_counter)
	   echo "<li  style='list-style:none'>\r\n<span class='folder category_css'>\r\n<a href='$link_cat'>".$parentcats[$i]->catname."({$counter_cat})</a></span>\r\n";
	else
	   echo "<li  style='list-style:none'>\r\n<span class='folder category_css'>\r\n<a href='$link_cat'>".$parentcats[$i]->catname."</a>\r\n</span>\r\n";
	if (count($subcats)>0) echo "<ul>";

    for($j=0;$j<count($subcats);$j++) {
        
    	if ($allow_counter){
			$database->setQuery("SELECT count(*) FROM #__ads WHERE close_by_admin = 0 AND closed =0  and category='".$subcats[$j]->id."'");
            $counter_cat = $database->loadResult();
		}
		
    	$link_cat =  JRoute::_("index.php?option=com_adsman&task=listadds&Itemid={$Itemid}&cat=".$subcats[$j]->id."&parent=".$parentcats[$i]->id);
    	
        if ($allow_counter)
        	echo "\r\n<li style='list-style:none'><span class='file products_css'>\r\n<a href='$link_cat'>".$subcats[$j]->catname."({$counter_cat})</a>\r\n</span>\r\n</li>";
        else
        	echo "\r\n<li style='list-style:none'><span class='file products_css'>\r\n<a href='$link_cat'>".$subcats[$j]->catname."</a>\r\n</span>\r\n</li>";
    }

	if (count($subcats)>0) echo "</ul>";

}
echo "</ul>";

?>