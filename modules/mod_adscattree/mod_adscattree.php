<?php
/**------------------------------------------------------------------------
mod_adsman -  Ads Factory 3.4.0
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/
// no direct access

$mosConfig_live_site = JURI::root();
require_once (JPATH_ROOT.DS.'components'.DS.'com_adsman'.DS.'helpers'.DS.'route.php');

echo "<link rel='stylesheet' type='text/css' href='$mosConfig_live_site/modules/mod_adscattree/adstree/tree.css' >";

global $allow_counter; 
$allow_counter	= (int) $params->get('category_counter', 0);

if (!function_exists('outputSubCats')) {
function outputSubCats($pcat,$index,$link_cat){
	$ItemId = AdsmanHelperRoute::getMenuItemByTaskName("listadds");
	global $allow_counter;
	$mosConfig_live_site = JURI::root();
	$database =& JFactory::getDBO();

	$html_subtree = "";
	$database->setQuery("select * from #__ads_categories where parent='$pcat->id' order by ordering,catname");
	
	$subcats = $database->loadObjectList();
	$imgHeight=9;
	$imgWidth=9;
	$padding = "padding-left: 10px;";
	
	$nr_scats = count($subcats);
	$html_subtree .= "<div class='styNodeRegion' style='display:none;' id='".$index."'>";
	if ($nr_scats > 0) {
		for($i=0; $i<$nr_scats; $i++){
			$counter = "";
			if( $allow_counter==1 ){
			    $database->setQuery("SELECT count(*) FROM #__ads a
			        left join #__ads_categories c on a.category = c.id  WHERE
			        category='{$subcats[$i]->id}' or parent='{$subcats[$i]->id}' AND a.close_by_admin = 0 AND closed =0 ");
			    $counter_cat = $database->loadResult();
			    //echo $database->_sql;
				$counter = "($counter_cat)";
			}
			$link_scat = JRoute::_($mosConfig_live_site."index.php?option=com_adsman&view=adsman&task=listadds&Itemid={$ItemId}&cat=".$subcats[$i]->id."&parent=".$pcat->id);
			$html_subtree .= "<div id='styLink' class='slink'>";
			$html_subtree .= "<img  width=".$imgWidth." height=".$imgHeight." src='".$mosConfig_live_site."modules/mod_adscattree/adstree/images/indent4.png' style=".$padding."/><span onclick='openLink(\"".$link_scat."\",\"_parent\")'>".$subcats[$i]->catname."".$counter."</span>";
			$html_subtree .= "</div>";
			
		}
	} elseif ( $nr_scats==0 ) {
		
		$html_subtree .= "<div id='styLink'>";
		$html_subtree .= "";
		$html_subtree .= "</div>";
		
	}
	$html_subtree .= "</div>";
	return $html_subtree;
}	
}

$html_tree ='';

$database =& JFactory::getDBO();
$ItemId = AdsmanHelperRoute::getMenuItemByTaskName("listcats");

$imgHeight = 9;
$imgWidth = 9;
$padding_top = 2;
$padding_bottom = 2;

$database->setQuery("select * from #__ads_categories where parent='0' order by ordering,catname");
$parentcats = $database->loadObjectList();

$nr_pcats = count($parentcats);
$html_tree .='<div id="Tree" align="left" class="Tree" style="padding-top:'.$padding_top.'px;padding-bottom:'.$padding_bottom.'px">';

$j=1;
for($i=0;$i<$nr_pcats;$i++){
	
	$counter = "";
	if($allow_counter==1){
	    $database->setQuery( "SELECT count(*) FROM #__ads a
	        left join #__ads_categories c on a.category = c.id  WHERE
	        category = '{$parentcats[$i]->id}' or parent='{$parentcats[$i]->id}' AND a.close_by_admin = 0 AND closed =0 " );
	    $counter_cat = $database->loadResult();
		$counter = "($counter_cat)";
    }
    
	$link_cat = JRoute::_($mosConfig_live_site."index.php?option=com_adsman&view=adsman&task=listadds&Itemid={$ItemId}&cat=".$parentcats[$i]->id);
	
	$html_tree .='<div id="styFolder" >';
	$html_tree .='<span class="st_tree" onclick="hideShow(\''.$j.'\',\''.$mosConfig_live_site.'/modules/mod_adscattree/adstree/images/\')"><img id="pic'.$j.'" width="'.$imgWidth.'" height="'.$imgHeight.'" src="'.$mosConfig_live_site.'/modules/mod_adscattree/adstree/images/arrow2.gif"/></span>&nbsp;<span class="st_tree" onclick="openLink(\''.$link_cat.'\',\'_parent\')"> '.$parentcats[$i]->catname.''.$counter.'</span>';
	$html_tree .= outputSubCats($parentcats[$i],$j,$link_cat);
	$html_tree .='</div>'."\r\n";
	$j++;
	
}
$html_tree .='</div>';

echo $html_tree;
echo "<script type='text/javascript' src='$mosConfig_live_site/modules/mod_adscattree/adstree/tree.js'> </script>";
echo "<script type='text/javascript'>load('".$mosConfig_live_site."/modules/mod_adscattree/adstree/images/');</script>";

?>