<?php
/**------------------------------------------------------------------------
mod_adsearch_price -  Ads Factory 3.3.5
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class mod_adsearchpriceHelper
{
	function getTags() {
		$db	= JFactory::getDBO();
		$result	= null;
		$query  = "SELECT `tagname` , `parent_id`  FROM #__ads_tags LEFT JOIN #__ads AS a ON `parent_id` = a.id LEFT JOIN #__ads_categories AS cat ON  a.category=cat.id WHERE a.status = 1 AND (cat.status=1 OR cat.status IS NULL ) AND a.close_by_admin = 0 AND closed =0  ";

		$db->setQuery($query);
		$result = $db->loadObjectList();

		return $result;
	}

    function makeCurrencySelect($selected = "") {
        $database = JFactory::getDbo();

        $query = "SELECT id as value, name as text "
        . "\n FROM #__ads_currency"
        . "\n ORDER BY id"
        ;

        $database->setQuery( $query );
        $currencies = $database->loadObjectList();

        $optsCurrency = array();
        $optsCurrency[] = JHTML::_('select.option', '0',JText::_("ADS_ALL"));
        $optsCurrency = array_merge( $optsCurrency, $currencies );

        $html_currency =	JHTML::_('select.genericlist', $optsCurrency, 'currency', 'class="inputbox" size="1"',  'value', 'text',$selected);
        return $html_currency;
    }


}

?>
