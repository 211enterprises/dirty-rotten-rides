<?php
/**------------------------------------------------------------------------
mod_adsearch_price -  Ads Factory 3.3.5
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

// no direct access
defined('_JEXEC') or die('Restricted access'); ?>
<style type="text/css">
	.ads_table { border: 0 none !important; }
    /* for opera */
    .ads_table tr, td { border: 0 none !important; }
    .spacer { border-spacing: 5px !important; }
</style>

<form action="index.php?option=com_adsman&view=adsman&task=listadds&Itemid=2" method="post">
	<div class="search<?php echo $params->get('moduleclass_sfx') ?>">
	<table class="ads_table" width="100%" style="border-collapse: separate !important;" cellpadding="2" cellspacing="3">
		<tr>
            <td width="23"><?php echo JText::_('ADS_SEARCH_TITLE'); ?></td>
			<td>
                <input type="text" name="title_keyword" class="inputbox" size="21" /> <input type="submit" value="<?php echo JText::_('ADS_SEARCH'); ?>" value="<?php echo (isset($val))? $val : '';?>" />
			</td>
		</tr>
        <tr>
          <td><?php echo JText::_('ADS_PRICE'); ?></td>
          <td>
              <input type='textbox' name='price_min' id='price_min' size='7' /> &#45;
              <input type='textbox' name='price_max' id='price_max' size='7' />
              <?php echo (isset($currencies))? $currencies : '';?>
          </td>
        </tr>
	</table>
	</div>
	<input type="hidden" name="view"   value="adsman" />
	<input type="hidden" name="task"   value="listadds" />
	<input type="hidden" name="option" value="com_adsman" />
	<input type="hidden" name="Itemid" value="2" />
</form>