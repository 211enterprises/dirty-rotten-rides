<?php
/**------------------------------------------------------------------------
mod_adsman -  Ads Factory 3.4.0
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/
// no direct access
defined('_JEXEC') or die('Restricted access');

class modAdsmanHelper
{

	function getList(&$params){
		$db			= JFactory::getDBO();
		$user		= JFactory::getUser();
		$userId		= (int) $user->get('id');

		$count		= (int) $params->get('nr_ads_displayed', 5);
		$featured_display	= trim($params->get('featured_display', ''));
		$t_display	= trim( $params->get('type_display') );
		$secid		= trim( $params->get('secid') );
		$show_front	= $params->get('show_front', 1);
		$aid		= $user->get('aid', 0);

		/**
		 * type display:
		 * 0 Latest Ads; 1 Popular Ads; 2 Most Valuable Ads
		 * 3 Random Ads; 4 Featured Ads
		 */

		$_Query = "";
		switch ($t_display){
			case 0:{
				// Latest Ads
				$_Query = "SELECT a.* FROM #__ads AS a \r\n ".
				" LEFT JOIN #__ads_currency AS c ON c.id = a.currency  \r\n".
				" LEFT JOIN #__ads_categories AS cat ON  a.category=cat.id ".
				" WHERE a.status = 1 AND (cat.status=1 OR cat.status IS NULL ) AND a.close_by_admin = 0 AND closed =0  \r\n".
				" ORDER BY a.start_date DESC, a.id DESC ".
				" LIMIT $count ";
				break;
			}
			case 1:{
				// Popular Ads
				$_Query = "SELECT a.* FROM #__ads AS a \r\n ".
				" LEFT JOIN #__ads_currency AS c ON c.id = a.currency  \r\n".
				" LEFT JOIN #__ads_categories AS cat ON  a.category=cat.id ".
				" WHERE a.status = 1 AND (cat.status=1 OR cat.status IS NULL ) AND a.close_by_admin = 0 AND closed =0  \r\n".
				" ORDER BY a.hits DESC ".
				" LIMIT $count ";
				break;
			}
			case 2:{
				// Most Valuable Ads
				$_Query = "SELECT a.* FROM #__ads AS a \r\n ".
				" LEFT JOIN #__ads_currency AS c ON c.id = a.currency  \r\n".
				" LEFT JOIN #__ads_categories AS cat ON  a.category=cat.id ".
				" WHERE a.status = 1 AND (cat.status=1 OR cat.status IS NULL ) AND a.close_by_admin = 0 AND closed =0  \r\n".
				" ORDER BY a.askprice DESC ".
				" LIMIT $count ";
				break;
			}
			case 3:{
				// Random Ads
				$_Query = "SELECT a.* FROM #__ads AS a \r\n ".
				" LEFT JOIN #__ads_currency AS c ON c.id = a.currency  \r\n".
				" LEFT JOIN #__ads_categories AS cat ON  a.category=cat.id ".
				" WHERE a.status = 1 AND (cat.status=1 OR cat.status IS NULL ) AND a.close_by_admin = 0 AND closed =0  \r\n".
				" ORDER BY rand() ".
				" LIMIT $count ";
				break;
			}
			case 4:{
				// Featured Ads
						
				$featured_display_filter = "";
				
				if($featured_display!==""){
					
					$featured_display_filter = " AND a.featured IN ({$featured_display}) ";
					
				}

				$_Query = "SELECT a.* FROM #__ads AS a \r\n ".
				" LEFT JOIN #__ads_currency AS c ON c.id = a.currency  \r\n".
				" LEFT JOIN #__ads_categories AS cat ON  a.category=cat.id ".
				" WHERE a.featured <> 'none' {$featured_display_filter}  AND a.status = 1 ".
				" AND (cat.status=1 OR cat.status IS NULL ) AND a.close_by_admin = 0 AND closed =0  \r\n".
				" ORDER BY rand() ".
				" LIMIT $count ";
				break;
			}
			default:break;
		}
		
		$db->setQuery($_Query);
		return $db->loadAssocList();
	}
}	
?>