<?php
/**------------------------------------------------------------------------
mod_adsman -  Ads Factory Factory 3.4.0
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// Include the syndicate functions only once
require_once (dirname(__FILE__).DS.'helper.php');

$lang=JFactory::getLanguage();
$lang->load('mod_adsman');

$list = modAdsmanHelper::getList($params);
$showimages=$params->get('display_image');
$image_width=$params->get('image_width');
$image_height=$params->get('image_height');
$view = $params->get('view_template','default');

require(JModuleHelper::getLayoutPath('mod_adsman',$view));
