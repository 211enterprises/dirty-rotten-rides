<?php
/**------------------------------------------------------------------------
mod_adsman -  Ads Factory 3.4.0
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

// no direct access
defined('_JEXEC') or die('Restricted access'); ?>
<style type="text/css">
	.cell1{ background:#DAD9D3 !important; }
	.cell2{ background:#E2E6E7 !important; }
</style>
<?php if(count($list)>0){ ?>
<table>
<?php
$i=1; 

require_once (JPATH_ROOT.DS.'components'.DS.'com_adsman'.DS.'helpers'.DS.'route.php');
$Itemid = AdsmanHelperRoute::getMenuItemByTaskName("listadds");

foreach ($list as $key => $item ) {
	$_urlid = $item["id"];
	$url = JRoute::_("index.php?option=com_adsman&task=details&view=adsman&id=$_urlid&Itemid=$Itemid");
	if ($image_height) $w="height=\"$image_height\"";
	else if ($image_width) $w="width=\"$image_width\"";
	?>
	<tr>
		<td class="cell<?php echo $i ?>">&nbsp;<?php echo $key+1; ?>&nbsp;</td>
		<?php if ($showimages) {
		    ?>
		    <td class="cell<?php echo $i ?>">
		    <?php
		    if ($item["picture"]){
	       	    ?>
    		  <img src="components/com_adsman/images/resize_<?php echo $item["picture"]; ?>" border="0" <?php echo $w;?> >
		<?php }else { ?>
    		  <img src="components/com_adsman/images/no_image.png" border="0" <?php echo $w;?> >
		<?php }
		  ?></td><?php
		} ?>
		<td class="cell<?php echo $i ?>" valign="top"><a href="<?php echo $url;?>"><?php echo $item["title"];?></a></td>
	</tr>
<?php
if($i==1) $i++; else $i=1;
} ?>
</table>
<?php } else {
         echo JText::_( 'ADS_NO_ADS');
} ?>