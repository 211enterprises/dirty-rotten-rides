<?php
/**------------------------------------------------------------------------
mod_adsfulltree -  Ads Factory 3.4.0
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

global $allow_counter, $display_subcategories_nr;

$allow_counter				= (int) $params->get('category_counter', 0);
$display_subcategories_nr	= (int) $params->get('display_subcategories_nr', 0);
$collapsed					= $params->get('collapsed', true);
$fold_speed					= (int) $params->get('fold_speed', 250);
$theming					= $params->get('theming', "default");

$jdoc = JFactory::getDocument();

/**
 * JQuery Loading
 */
$j_loaded = false;
$jf_loaded = false;

foreach ($jdoc->_scripts as $j => $jj) {
	if (strstr($j, "jquery.js") !== false) {
		$j_loaded = true;
		break;
	}

    if (strstr($j, "jquery-factory.js") !== false) {
        $jf_loaded = true;
        break;
    }
}

if ($j_loaded == false) {
    $jdoc->addScript(JURI::root().'components/com_adsman/gallery/js/jquery.js');
}
if ($jf_loaded == false) {
    $jdoc->addScript(JURI::root().'components/com_adsman/js/jquery/jquery-factory.js');
}
$jdoc->addScript(JURI::root().'components/com_adsman/js/jquery/jquery.cookie.js');
?>
<script src="<?php echo JURI::root(); ?>components/com_adsman/js/jquery/tree/jquery.treeview.js" type="text/javascript"></script>

<?php
$theme_css = "";
switch ($theming) {
	case "default":
		$theme_css = JURI::root()."components/com_adsman/js/jquery/tree/jquery.treeview.css";
		break;

	case "azuro":
		$theme_css = JURI::root()."components/com_adsman/js/jquery/tree/themes/azuro/jquery.treeview.css";
		break;
		
	case "green":
		$theme_css = JURI::root()."components/com_adsman/js/jquery/tree/themes/green/jquery.treeview.css";
		break;
	default:
		$theme_css = JURI::root()."components/com_adsman/js/jquery/tree/jquery.treeview.css";
		break;
}
$jdoc->addStyleSheet($theme_css);
//jQueryFactory(document).ready(function($) {
$mod_ads_js = "
		jQuery(document).ready(function($) {
			$(\"#ads_treecontainer_{$module->id}\").treeview( { collapsed: {$collapsed}, animated: {$fold_speed},	persist: \"location\" });
		    document.getElementById('ads_treecontainer_{$module->id}').style.display='block';
		});
";


$cookies = $_COOKIE;

$jdoc->addScriptDeclaration( $mod_ads_js );

require_once(JPATH_ROOT.DS."components".DS."com_adsman".DS."options.php");

if(!class_exists("modAdsCategoryTreeHelper")){
	
	class modAdsCategoryTreeHelper{
		
		function getMenuItemId( $needles ){
			
			jimport( 'joomla.application.component.helper' );
			$component = JComponentHelper::getComponent('com_adsman');

			
			$menus	= JApplication::getMenu('site', array());
			$items	= $menus->getItems('component_id', $component->id);
			
			$match = null;
			if ($items) {
				foreach($items as $item)
				{
					$ok = true;
					foreach($needles as $needle => $id)
					{
						if ( @$item->query[$needle] != $id ) {
							$ok = false;
							break;
						}
					}
					if($ok==true){
						$match=$item;
						break;
					}
				}
			}
			
			if (isset($match)) {
				
				return $match->id;
				
			}else 
				return null;
		}
		
        function getItemCount(){
            
			$database = &JFactory::getDbo();
			$database->setQuery("SELECT category, count(*) as nr FROM #__ads WHERE close_by_admin = 0 AND closed =0  GROUP BY category");
			
			return $database->loadObjectList("category");
        }
        
		function getCategories($categoryID=0){
			
			global $category_data;
			$database = &JFactory::getDbo();
			$database->setQuery(" 
			SELECT c . *    
			\r\n".
			" FROM #__ads_categories AS c  \r\n".
			" order by c.id,c.ordering,catname asc
			");
			
			$dbrows = $database->loadObjectList();
			//echo nl2br($database->_sql);exit;
			
			$cats = array();
			
			if($dbrows)
			foreach ($dbrows as $k =>$rw){
			     
			   $rw->nr_a = isset($category_data[$rw->id]->nr)?$category_data[$rw->id]->nr:0;
				
				$cats[$rw->id]["items_no"] = $rw->nr_a;

				
				if ( isset($cats[$rw->parent]["items_no"] ) )
				  $cats[$rw->parent]["items_no"] += $cats[$rw->id]["items_no"];
				else
				  $cats[$rw->parent]["items_no"] = $cats[$rw->id]["items_no"];
				  
				$cats[$rw->parent]["kids"][] = $rw;
			}
			
			return $cats;
			
		}
		
		function catHTML($catId=0, $allCategories, $level){
			
			global $allow_counter,$display_subcategories_nr, $modItemid; 
            			
			if(isset($allCategories[$catId]["kids"])){
				
				foreach ($allCategories[$catId]["kids"] as $c => $item){
					
					$ciu = $item->id." - ".$item->catname;
					
					$link_cat = JRoute::_("index.php?option=com_adsman&task=listadds&cat=".$item->id."&parent=".$catId."&Itemid={$modItemid}");
					
					$class_name = isset($allCategories[$item->id]["kids"])?"folder":"file";
					
					$cat_counter = "";
					$counter = "";

			/**
			 * Ads Counter HTML
			 * 
			 */
					if($allow_counter==1){
						/*
						if (isset($allCategories[$item->id]["items_no"])) {
							
							$counter = $allCategories[$item->id]["items_no"];
							
						} else */ 
						
							$counter = "{$item->nr_a}";
						
						$counter = " ( {$counter}  ) ";
					}
			/**
			 * Subcategories Counter HTML
			 * 
			 */
					if($display_subcategories_nr==1){
						
						if (isset($allCategories[$item->id])) {
							$cat_counter = " ( ".count($allCategories[$item->id]). " ) ";
						}else 
							$cat_counter = "(0)";
					}
	
					
					echo "<li  style='list-style:none'>\r\n
					<span class='{$class_name} category_css'>\r\n
						<a href='$link_cat'>".$item->catname." ".$counter.$cat_counter."</a>\r\n
					</span>\r\n";
					
					if(isset($allCategories[$item->id]["kids"])){
						echo "<ul id='ads_treecontainer' class='filetree closed' style='display:none'>\r\n";
						modAdsCategoryTreeHelper::catHTML( $item->id, $allCategories, $level+1 );
						echo "</ul>\r\n";
					}
					echo "</li>\r\n";
				}
			}else
				return;
		}
	}
	
}
global $modItemid;
$modItemid = modAdsCategoryTreeHelper::getMenuItemId(array("view"=>"ads","layout"=>""));
if (!$modItemid) {
	$modItemid = modAdsCategoryTreeHelper::getMenuItemId(array("task"=>"listadds"));
}

global $category_data;
$category_data = modAdsCategoryTreeHelper::getItemCount();
$cats = modAdsCategoryTreeHelper::getCategories();

echo "<ul id='ads_treecontainer_{$module->id}' class='filetree closed' style='display:none'>\r\n";
modAdsCategoryTreeHelper::catHTML(0,$cats,1);
echo "</ul>\r\n";

?>
