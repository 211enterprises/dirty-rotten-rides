<?php
 /**------------------------------------------------------------------------
mod_adscloud -  Ads Factory 3.4.0
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

// no direct access
defined('_JEXEC') or die('Restricted access');
?>
<style type="text/css">
	.adsCloudModule{ }
	.fontadsCloudModule{ }
</style>
<div class="adsCloudModule">
<?php
require_once (JPATH_ROOT.DS.'components'.DS.'com_adsman'.DS.'helpers'.DS.'route.php');

$Itemid = AdsmanHelperRoute::getMenuItemByTaskName("listadds");
if (count($ordered_tag_list)>0)
	foreach($ordered_tag_list as $k => $v) {
	if($v){
		$vrate='';
		$size = $min_font+($rank_font * ( ($v - $minimum_count) / $rank_freq) );
		$href=JRoute::_("index.php?option=com_adsman&task=listadds&Itemid=$Itemid&tag=$k");
		echo "<a href=\"".$href."\" style=\"font-size: ".$size."px;\" class=\"fontadsCloudModule\" title=\"".$k."\">".$k."</a>";
		{echo "&nbsp;\n";}
	 }
	}
?>
</div>