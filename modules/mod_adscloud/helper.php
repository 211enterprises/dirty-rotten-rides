<?php
/**------------------------------------------------------------------------
mod_adscloud -  Ads Factory 3.4.0
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class mod_adscloudHelper
{
	function getTags() {
		$db	= JFactory::getDBO();
		$result	= null;
		$query  = "SELECT `tagname` , `parent_id`  FROM #__ads_tags LEFT JOIN #__ads AS a ON `parent_id` = a.id LEFT JOIN #__ads_categories AS cat ON  a.category=cat.id WHERE a.status = 1 AND (cat.status=1 OR cat.status IS NULL ) AND a.close_by_admin = 0 AND closed =0  "; 
		
		$db->setQuery($query);
		$result = $db->loadObjectList();
		
		return $result;	  
	} 

	function shuffle_assoc(&$array) {
		if (count($array)>1) {
		$keys = array_rand($array, count($array));
		
		foreach($keys as $key)
		$new[$key] = $array[$key];
		
		$array = $new;
		}
		return true;
	}
}

?>
