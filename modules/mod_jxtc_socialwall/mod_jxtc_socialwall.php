<?php
/*
	JoomlaXTC Social Wall

	version 1.16.0

	Copyright (C) 2008,2009,2010,2011,2013 Monev Software LLC.	All Rights Reserved.

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

	THIS LICENSE IS NOT EXTENSIVE TO ACCOMPANYING FILES UNLESS NOTED.

	See COPYRIGHT.txt for more information.
	See LICENSE.txt for more information.

	Monev Software LLC
	www.joomlaxtc.com
*/

defined( '_JEXEC' ) or die;

jimport( 'joomla.html.parameter' );


$live_site = JURI::base();
$user = JFactory::getUser();
$db = JFactory::getDBO();
$doc = JFactory::getDocument();
$moduleDir = 'mod_jxtc_socialwall';

require_once (JPATH_SITE.'/components/com_content/helpers/route.php');
include_once( JPATH_ROOT.'/components/com_community/defines.community.php' );
require_once( JPATH_ROOT.'/components/com_community/libraries/core.php' );
require_once( JPATH_ROOT.'/components/com_community/libraries/userpoints.php' );

CFactory::load('libraries', 'error');

$groupFilter = (array) $params->get('groupFilter', 0);
$profileFilter = (array) $params->get('profileFilter', 0);
$userid = trim($params->get('user_id', ''));

$currentonly = $params->get('currentonly', 0);
$featuredonly = $params->get('featuredonly', 0);

$updated_avatar_only = $params->get('updated_avatar_only', 0);
$onlineonly = $params->get('onlineonly', 0);
$avatarw = $params->get('avatarw', '120');
$avatarw = empty($avatarw) ? '' : ' width="'.$avatarw.'"';
$avatarh = $params->get('avatarh', '90');
$avatarh = empty($avatarh) ? '' : ' height="'.$avatarh.'"';
$sortfield = $params->get('sortfield', 0);
$sortorder = $params->get('sortorder', 1);
$displayshowall = $params->get('displayshowall', 0);
$columns = $params->get('columns', 3);
$rows = $params->get('rows', 3);
$pages = $params->get('pages', 1);
$dateformat = trim($params->get('dateformat', 'Y-m-d'));

$videow = $params->get('videow', 425);
$videoh = $params->get('videoh', 344);

	$moreclone = $params->get('moreclone', 0);
	$moreqty = $params->get('moreqty', 0);
	$morecols	= trim( $params->get('morecols',1));
	$morelegend	= trim($params->get('moretext', ''));
	$morelegendcolor	= $params->get('morergb','cccccc');
	$moretemplate	= $params->get('moretemplate', '');

$template = $params->get('template', '');
$moduletemplate = trim($params->get('moduletemplate', '{mainarea}'));
$itemtemplate = trim($params->get('itemtemplate', '{username}'));
if ($template && $template != -1) {
    $moduletemplate = file_get_contents(JPATH_ROOT.'/modules/'.$moduleDir.'/templates/'.$template.'/module.html');
    $itemtemplate = file_get_contents(JPATH_ROOT.'/modules/'.$moduleDir.'/templates/'.$template.'/element.html');
	$moretemplate=file_get_contents(JPATH_ROOT.'/modules/'.$moduleDir.'/templates/'.$template.'/more.html');
    if (file_exists(JPATH_ROOT.'/modules/'.$moduleDir.'/templates/'.$template.'/template.css')) {
        $doc->addStyleSheet($live_site.'modules/'.$moduleDir.'/templates/'.$template.'/template.css', 'text/css');
    }
}

$uid = $user->id;

$query = 'SELECT a.id, a.name, a.username, a.email, a.registerDate, a.lastvisitDate, b.posted_on, b.avatar,'.
        ' b.thumb, b.friendcount, b.points, b.status, b.view, b.params FROM #__users AS a INNER JOIN #__community_users AS b ON a.id = b.userid';

if (!$userid) {
if ($groupFilter[0] != '0') {
	$query .= ' INNER JOIN #__community_groups_members AS gm ON a.id = gm.memberid';
}
}

if ($featuredonly)
    $query .= ', #__community_featured AS cf';

$query .= ' WHERE a.block = 0';

if ($featuredonly)
    $query .= ' AND cf.cid = a.id';

if ($userid) {
    $userid = explode(',', $userid);
    JArrayHelper::toInteger($userid);
    $query .= ' AND a.id in ('.join(',', $userid).') ';
} else {
if ($groupFilter[0] != '0') {
        $query .= ' AND gm.approved = 1 AND gm.groupid IN ('.implode(',', $groupFilter).')';
}

    if ($profileFilter[0] != '0') {
        $query .= ' AND b.profile_id IN ('.implode(',', $profileFilter).')';
}

    if ($currentonly && $uid) {
        $query .= ' AND b.userid = '.$uid;
    } else {
    if ($onlineonly) {
        $query .= ' AND a.id in (SELECT DISTINCT(uu.id) FROM #__users AS uu INNER JOIN #__session AS ss ON uu.id=ss.userid WHERE uu.block=0 and ss.client_id != 1)';
    }
    if ($updated_avatar_only) {
        $query .= " AND b.avatar != '' AND b.avatar IS NOT NULL AND b.avatar != 'components/com_community/assets/default.jpg' ";
    }
}
}
$query .= ' ORDER BY ';

switch ($sortfield) {
    case '0': // Register date
        $query .= 'a.registerDate';
        break;
    case '1': // Last visit
        $query .= 'a.lastvisitDate';
        break;
    case '2': // Last post
        $query .= 'b.posted_on';
        break;
    case '3': // ID
        $query .= 'a.id';
        break;
    case '4': // Name
        $query .= 'a.name';
        break;
    case '5': // User name
        $query .= 'a.username';
        break;
    case '6': // Friend count
        $query .= 'b.friendcount';
        break;
    case '7': //Random
        $query .= 'RAND()';
        break;
    case '8': //Popularity
        $query .= 'b.view';
        break;
}

$query .= ($sortorder == '0') ? ' ASC ' : ' DESC ';
$mainqty = $columns*$rows*$pages;
$db->setQuery($query, 0, $mainqty+$moreqty);
$items = $db->loadObjectList();

if (count($items) == 0) {
    echo JText::_('No members yet');
    return;	
}

require JModuleHelper::getLayoutPath($module->module, $params->get('layout', 'default'));


$displayall = '<a style="float:right" href="'.CRoute::_("index.php?option=com_community&view=search&task=browse&sort=latest").'">'.JText::_("Show All").'</a>';
$modulehtml = str_replace('{displayallurl}', $displayall, $modulehtml);

$items = $params->get('moreclone', 0)
	? array_slice($items,0,$moreqty)
	: array_slice($items,($columns * $rows * $pages),$moreqty);

require JModuleHelper::getLayoutPath($module->module, $params->get('layout', 'default').'_more');

JPluginHelper::importPlugin('content');
$contentconfig = JComponentHelper::getParams('com_content');
$dispatcher = JDispatcher::getInstance();
$item = new stdClass();
$item->text = $modulehtml;
$results = $dispatcher->trigger('onContentPrepare', array('com_content.article', &$item, &$contentconfig, 0));
$modulehtml = $item->text;

echo '<div id="'.$jxtc.'">'.$modulehtml.'</div>';
