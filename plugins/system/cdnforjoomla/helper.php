<?php
/**
 * Plugin Helper File
 *
 * @package         CDN for Joomla!
 * @version         3.2.11
 *
 * @author          Peter van Westen <peter@nonumber.nl>
 * @link            http://www.nonumber.nl
 * @copyright       Copyright © 2014 NoNumber All Rights Reserved
 * @license         http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

defined('_JEXEC') or die;

// Load common functions
require_once JPATH_PLUGINS . '/system/nnframework/helpers/functions.php';
require_once JPATH_PLUGINS . '/system/nnframework/helpers/protect.php';

/**
 * Plugin that replaces media urls with CDN urls
 */
class plgSystemCDNforJoomlaHelper
{
	function __construct(&$params)
	{
		$this->params = $params;
		$this->params->pass = 0;

		$hascdn = preg_replace(array('#^.*\://#', '#/$#'), '', $this->params->cdn);

		// return if cdn field has no value
		if (!$hascdn)
		{
			return;
		}

		$this->domain = 0;
		$this->params->https = (
			(!empty($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) != 'off')
			|| (isset($_SERVER['SSL_PROTOCOL']))
			|| (isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == 443)
			|| (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && strtolower($_SERVER['HTTP_X_FORWARDED_PROTO']) == 'https')
		);

		$this->params->sets = array();

		$nr_of_sets = 5;
		for ($i = 1; $i <= $nr_of_sets; $i++)
		{
			$this->params->sets[] = $this->initParams($i);
		}

		foreach ($this->params->sets as $i => $set)
		{
			if (empty($set) || empty($set->searches))
			{
				unset($this->params->sets[$i]);
			}
		}

		if (empty($this->params->sets))
		{
			return;
		}

		$this->params->pass = 1;
	}

	function initParams($set = '')
	{
		if ($set > 1)
		{
			$set = '_' . (int) $set;
		}
		else
		{
			$set = '';
		}
		$p = new stdClass;

		if ($set && (!isset($this->params->{'use_extra' . $set}) || !$this->params->{'use_extra' . $set}))
		{
			return $p;
		}

		$p->enable_in_scripts = $this->params->{'enable_in_scripts' . $set};

		$p->enable_https = $this->params->{'enable_https' . $set};
		if ($this->params->https && !$p->enable_https)
		{
			return $p;
		}
		$p->keep_https = $p->enable_https ? $this->params->{'keep_https' . $set} : 0;
		$p->enable_versioning = $this->params->{'enable_versioning' . $set};

		$p->cdn = preg_replace('#/$#', '', $this->params->{'cdn' . $set});
		$p->ignorefiles = explode(',', str_replace(array('\n', ' '), array(',', ''), $this->params->{'ignorefiles' . $set}));

		$root = preg_replace(array('#^/#', '#/$#'), '', $this->params->{'root' . $set}) . '/';
		$root = preg_quote(preg_replace('#^/#', '', $root), '#');

		$filetypes = str_replace('-', ',', implode(',', $this->params->{'filetypes' . $set}));
		$filetypes = explode(',', $filetypes);
		$extratypes = preg_replace('#\s#', '', $this->params->{'extratypes' . $set});

		if ($extratypes)
		{
			$filetypes = array_merge($filetypes, explode(',', $extratypes));
		}

		$filetypes = array_unique(array_diff($filetypes, array('', 'x')));

		$p->searches = array();
		$p->js_searches = array();
		if (!empty($filetypes))
		{
			$filetypes = implode('|', $filetypes);
			$attribs = 'href=|src=|longdesc=|@import|name="movie" value=|property="og:image" content=|rel="{\'link\':';
			$attribs = str_replace(array('"', '=', ' '), array('["\']?', '\s*=\s*', '\s+'), $attribs);

			// Domain url or root path
			$url = preg_quote(str_replace('https://', 'http://', JURI::root()), '#');
			$url .= '|' . preg_quote(str_replace('http://', '//', JURI::root()), '#');
			if ($p->enable_https)
			{
				$url .= '|' . preg_quote(str_replace('http://', 'https://', JURI::root()), '#');
			}
			if (JURI::root(1))
			{
				$url .= '|' . preg_quote(JURI::root(1) . '/', '#');
			}

			$urls = array();

			// Absolute path
			$urls[] = '(?:' . $url . ')' . $root . '([^ \?QUOTES]+\.(?:' . $filetypes . ')(?:\?[^QUOTES]*)?)';
			// Relative path
			$urls[] = 'LSLASH' . $root . '([a-z0-9-_]+/[^ \?QUOTES]+\.(?:' . $filetypes . ')(?:\?[^QUOTES]*)?)';
			// Relative path - file in root
			$urls[] = 'LSLASH' . $root . '([a-z0-9-_]+[^ \?\/QUOTES]+\.(?:' . $filetypes . ')(?:\?[^QUOTES]*)?)';

			self::getSearches($p, $attribs, $urls);
		}

		$p->cdns = explode(',', $p->cdn);
		foreach ($p->cdns as $i => $cdn)
		{
			$cdn = preg_replace('#^.*\://#', '', trim($cdn));
			if (strpos($cdn, '{') !== false && preg_match_all('#(\.?)\{([^\}]*)\}(\.?)#', $cdn, $matches, PREG_SET_ORDER) > 0)
			{
				$domain = self::getDomain();
				foreach ($matches as $match)
				{
					$r = '';
					if (isset($domain->{$match['2']}) && $domain->{$match['2']})
					{
						$r = $match['1'] . $domain->{$match['2']} . $match['3'];
					}
					else if ($match['1'] && $match['3'])
					{
						$r = '.';
					}
					$cdn = str_replace($match['0'], $r, $cdn);
				}
			}
			$p->cdns[$i] = $cdn;
		}

		return $p;
	}

	function onContentPrepare(&$article, $context = '')
	{
		if (!$this->params->pass)
		{
			return;
		}

		// FEED
		if (JFactory::getDocument()->getType() != 'feed' && JFactory::getApplication()->input->get('option') != 'com_acymailing')
		{
			return;
		}

		if (isset($article->text)
			&& !(
				$context == 'com_content.category'
				&& JFactory::getApplication()->input->get('view') == 'category'
				&& !JFactory::getApplication()->input->get('layout')
			)
		)
		{
			$this->replace($article->text);
		}
		if (isset($article->description))
		{
			$this->replace($article->description);
		}
		if (isset($article->title))
		{
			$this->replace($article->title);
		}
		if (isset($article->author))
		{
			if (isset($article->author->name))
			{
				$this->replace($article->author->name);
			}
			else if (is_string($article->author))
			{
				$this->replace($article->author);
			}
		}
	}

	function onAfterDispatch()
	{
		if (!$this->params->pass)
		{
			return;
		}

		if (JFactory::getDocument()->getType() != 'feed' && JFactory::getApplication()->input->get('option') != 'com_acymailing')
		{
			return;
		}

		// FEED
		if (isset(JFactory::getDocument()->items))
		{
			$context = 'feed';
			$items = JFactory::getDocument()->items;
			foreach ($items as $item)
			{
				$this->onContentPrepare($item, $context);
			}
		}
	}

	function onAfterRender()
	{
		if (!$this->params->pass)
		{
			return;
		}

		// not in pdf's
		if (JFactory::getDocument()->getType() == 'pdf')
		{
			return;
		}

		$html = JResponse::getBody();
		$this->replace($html);
		$this->cleanLeftoverJunk($html);
		JResponse::setBody($html);
	}

	function replace(&$str)
	{
		if (is_array($str))
		{
			foreach ($str as $key => $val)
			{
				$str[$key] = $this->replaceReturn($val);
			}
		}
		else
		{
			$string_array = $this->protectString($str);
			foreach ($string_array as $i => $string)
			{
				if ($i % 2)
				{
					continue;
				}

				foreach ($this->params->sets as $set)
				{
					$this->replaceBySet($string_array[$i], $set);
				}
			}
			$str = implode('', $string_array);
		}
	}

	function replaceReturn($str)
	{
		$this->replace($str);

		return $str;
	}

	function replaceBySet(&$str, &$params)
	{
		if ($this->params->https && !$params->enable_https)
		{
			return;
		}

		$http = 'http://';
		if ($this->params->https && $params->keep_https)
		{
			$http = 'https://';
		}
		$this->replaceBySearches($str, $params->searches, $params, $http);

		if (!empty($params->enable_in_scripts) && strpos($str, '<script') !== false)
		{
			$regex = '#<script(?:\s+type\s*=[^>]*)?>.*?</script>#si';
			if (preg_match_all($regex, $str, $strparts, PREG_SET_ORDER) > 0)
			{
				foreach ($strparts as $strpart)
				{
					$newstr = $strpart['0'];
					if ($this->replaceBySearches($newstr, $params->js_searches, $params, $http))
					{
						$str = str_replace($strpart['0'], $newstr, $str);
					}
				}
			}
		}
	}

	function replaceBySearches(&$str, &$searches, &$params, $http)
	{
		$changed = 0;
		foreach ($searches as $search)
		{
			if (preg_match_all($search, $str, $matches, PREG_SET_ORDER) < 1)
			{
				continue;
			}

			foreach ($matches as $match)
			{
				$pass = 1;
				$file = trim($match['3']);

				if (!$file)
				{
					continue;
				}

				foreach ($params->ignorefiles as $ignore)
				{
					if ($ignore && (strpos($file, $ignore) !== false || strpos(htmlentities($file), $ignore) !== false))
					{
						$pass = 0;
						break;
					}
				}

				if (!$pass)
				{
					continue;
				}

				if ($params->enable_versioning && file_exists($file))
				{
					$file .= ((strpos($file, '?') === false) ? '?' : '&') . filemtime($file);
				}
				$cdn = $this->getCDN($file, $params->cdns, $http);
				$replace = $match['1'] . $cdn . '/' . $file . $match['4'];
				$str = str_replace($match['0'], $replace, $str);
				$changed = 1;
			}
		}

		return $changed;
	}

	/*
	 * Searches are replaced by:
	 * '\1http(s)://' . $this->params->cdn . '/\3\4'
	 * \2 is used to reference the possible starting quote
	 */
	function getSearches(&$p, $attribs, $urls)
	{
		foreach ($urls as $url)
		{
			$r = '\s*' . str_replace('QUOTES', '"\'', $url) . '\s*';

			if ($p->enable_in_scripts)
			{
				$jsr = str_replace('LSLASH', '', $r);
				$p->js_searches[] = '#((["\']))' . $jsr . '(["\'])#i'; // "..."
			}

			$r = str_replace('LSLASH', '/?', $r);

			$p->searches[] = '#((?:' . $attribs . ')\s*(["\']))' . $r . '(\2)#i'; // attrib="..."
			$p->searches[] = '#((?:' . $attribs . ')())' . $r . '([\s|>])#i'; // attrib=...
			$p->searches[] = '#(url\(\s*(["\']))' . $r . '(\2\s*\))#i'; // url("...")
			// add ')' to the no quote checks
			$r = '\s*' . str_replace('QUOTES', '"\'\)', $url) . '\s*';
			$p->searches[] = '#(url\(\s*())' . $r . '(\s*\))#i'; // url(...)
		}
	}

	function getCDN($file, $cdns = array(''), $http)
	{
		if (count($cdns) > 1)
		{
			$cdns = array($cdns[hexdec(substr(hash('md2', $file), -4)) % count($cdns)]);
		}

		return $http . $cdns['0'];
	}

	var $slds = array('a', 'ab', 'abo', 'ac', 'ad', 'adm', 'adv', 'aero', 'agr', 'ah', 'alt', 'am', 'amur', 'arq', 'art', 'arts', 'asn', 'assn', 'asso', 'ato', 'av', 'b', 'bbs', 'bc', 'bd', 'bel', 'bio', 'bir', 'biz', 'bj', 'bl', 'blog', 'bmd', 'c', 'cat', 'cbg', 'cc', 'chel', 'cim', 'city', 'ck', 'club', 'cn', 'cng', 'cnt', 'co', 'com', 'conf', 'coop', 'cq', 'cr', 'cri', 'cv', 'cym', 'd', 'db', 'de', 'dn', 'dni', 'dp', 'dr', 'du', 'e', 'ebiz', 'ecn', 'ed', 'edu', 'eng', 'ens', 'es', 'esp', 'est', 'et', 'etc', 'eti', 'eun', 'f', 'fam', 'far', 'fed', 'fi', 'fin', 'firm', 'fj', 'flog', 'fm', 'fnd', 'fot', 'fr', 'fs', 'fst', 'g', 'g12', 'game', 'gd', 'gda', 'geek', 'gen', 'ggf', 'go', 'gob', 'gok', 'gon', 'gop', 'gos', 'gouv', 'gov', 'govt', 'gp', 'gr', 'grp', 'gs', 'gub', 'gv', 'gx', 'gz', 'h', 'ha', 'hb', 'he', 'hi', 'hl', 'hn', 'hs', 'i', 'id', 'idf', 'idn', 'idv', 'if', 'imb', 'imt', 'in', 'inca', 'ind', 'inf', 'info', 'ing', 'int', 'intl', 'iq', 'ir', 'isa', 'isla', 'it', 'its', 'iwi', 'jar', 'jeju', 'jet', 'jl', 'jobs', 'jor', 'js', 'jus', 'jx', 'k', 'k12', 'kchr', 'kg', 'kh', 'khv', 'kids', 'kiev', 'km', 'komi', 'kr', 'ks', 'kv', 'kzn', 'l', 'law', 'lea', 'lel', 'lg', 'ln', 'lodz', 'lp', 'ltd', 'lviv', 'm', 'ma', 'mari', 'mat', 'mb', 'me', 'med', 'mi', 'mil', 'mk', 'mob', 'mobi', 'mod', 'mpm', 'ms', 'msk', 'muni', 'mus', 'n', 'name', 'nat', 'nb', 'ne', 'nel', 'net', 'news', 'nf', 'ngo', 'nhs', 'nic', 'nis', 'nl', 'nls', 'nm', 'nnov', 'nom', 'nome', 'not', 'nov', 'ns', 'nsk', 'nsn', 'nt', 'ntr', 'nu', 'nw', 'nx', 'o', 'od', 'odo', 'og', 'om', 'omsk', 'on', 'or', 'org', 'orgn', 'ov', 'p', 'pb', 'pe', 'per', 'perm', 'pix', 'pl', 'plc', 'plo', 'pol', 'pp', 'ppg', 'prd', 'priv', 'pro', 'prof', 'psc', 'psi', 'ptz', 'pub', 'publ', 'pwr', 'qc', 'qh', 'qsl', 'r', 're', 'rec', 'red', 'res', 'rg', 'rnd', 'rnrt', 'rns', 'rnu', 'rs', 'rv', 's', 'sa', 'sc', 'sch', 'sci', 'scot', 'sd', 'sec', 'sh', 'sk', 'sld', 'slg', 'sn', 'soc', 'spb', 'srv', 'stv', 'sumy', 'sx', 't', 'te', 'tel', 'test', 'tj', 'tm', 'tmp', 'tom', 'trd', 'tsk', 'tula', 'tur', 'tuva', 'tv', 'tver', 'tw', 'u', 'udm', 'unbi', 'univ', 'unmo', 'unsa', 'untz', 'unze', 'vet', 'vlog', 'vn', 'vrn', 'w', 'war', 'waw', 'web', 'wiki', 'wroc', 'www', 'x', 'xj', 'xz', 'y', 'yk', 'yn', 'z', 'zj', 'zlg', 'zp', 'zt');

	function getDomain()
	{
		if ($this->domain)
		{
			return $this->domain;
		}

		$this->domain = new stdClass;
		$this->domain->fulldomain = JURI::getInstance()->getHost();
		$this->domain->subdomain = '';
		$this->domain->domain = $this->domain->fulldomain;
		$this->domain->extension = '';

		$parts = array_reverse(explode('.', $this->domain->fulldomain));

		if (count($parts) > 1)
		{
			$this->domain->extension = array_shift($parts);
			if (in_array($parts['0'], $this->slds))
			{
				$this->domain->extension = array_shift($parts) . '.' . $this->domain->extension;
			}
			$subs = 0;
			while (!empty($parts))
			{
				$this->domain->{str_repeat('sub', $subs++) . 'domain'} = array_shift($parts);
			}
			$this->domain->subdomain = $this->domain->subdomain ? $this->domain->subdomain : 'www';
		}

		return $this->domain;
	}

	/**
	 * Just in case you can't figure the method name out: this cleans the left-over junk
	 */
	function cleanLeftoverJunk(&$str)
	{
		$str = str_replace(array('{nocdn}', '{/nocdn}'), '', $str);
	}

	function protectString($str)
	{
		if (NNProtect::isEditPage())
		{
			$str = preg_replace('#(<' . 'form [^>]*(id|name)="(adminForm|postform)".*?</form>)#si', '{nocdn}\1{/nocdn}', $str);
		}

		if (strpos($str, '{nocdn}') === false || strpos($str, '{/nocdn}') === false)
		{
			$str = str_replace(array('{nocdn}', '{/nocdn}'), '', $str);

			return array($str);
		}

		$str = str_replace(array('{nocdn}', '{/nocdn}'), '[[CDN_SPLIT]]', $str);

		return explode('[[CDN_SPLIT]]', $str);
	}
}
