<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.0
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );


class plgSearchAdsman extends JPlugin
   {
   	/**
   	 * Constructor
   	 *
   	 * @access      protected
   	 * @param       object  $subject The object to observe
   	 * @param       array   $config  An array that holds the plugin configuration
   	 * @since       1.5
   	 */
   	public function __construct(& $subject, $config)
   	{
   		parent::__construct($subject, $config);
   		$this->loadLanguage();
   	}

    //$mainframe->registerEvent( 'onSearch', 'plgSearchAdsman' );
    //$mainframe->registerEvent( 'onSearchAreas', 'plgSearchAdsmanAreas' );

    /**
     * @return array An array of search areas
     */
    function onContentSearchAreas()
    {
        static $areas = array(
            'adsman' => 'Ads'
        );
        return $areas;
    }


    function onContentSearch( $text, $phrase='', $ordering='', $areas=null )
    {
        $db		= JFactory::getDbo();
        $user	= JFactory::getUser();
        $user	= JFactory::getUser();
        $groups	= implode(',', $user->getAuthorisedViewLevels());

        $searchText = $text;

        if (is_array($areas)) {
            if (!array_intersect($areas, array_keys($this->onContentSearchAreas()))) {
                return array();
            }
        }

        $sContent		= $this->params->get('search_content',		1);
        $sArchived		= $this->params->get('search_archived',		1);
        $limit			= $this->params->def('search_limit',		50);

        $state = array();

        if ($sContent) {
            //$state[]=0;
            $state_published = 1;
        } else {
            $state_published = 0;
        }


        if ($sArchived) {
            $state_archived = 1;
            $whereArchived =  ' OR ' .' (closed = '. $state_archived .' ) ';
        } else {
            $state_archived = 0;
            $whereArchived =  ' AND ' .' (closed = '. $state_archived .' ) ';
            //$whereArchived .' (closed = '. $state_archived .' OR close_by_admin = '. $state_archived.' ) ';
        }

        $text = trim( $text );
        if ($text == '') {
            return array();
        }
        $section 	= JText::_( 'ADS_PLG_SEARCH_SECTION' );

        $wheres 	= array();
        switch ($phrase)
        {
            case 'exact':
                $text		= $db->Quote( '%'.$db->escape( $text, true ).'%', false );
                $wheres2 	= array();
                $wheres2[] 	= 'a.description LIKE '.$text;
                $wheres2[] 	= 'a.title LIKE '.$text;
                $where 		= '(' . implode( ') OR (', $wheres2 ) . ')';
                break;

            case 'all':
            case 'any':
            default:
                $words 	= explode( ' ', $text );
                $wheres = array();
                foreach ($words as $word)
                {
                    $word		= $db->Quote( '%'.$db->escape( $word, true ).'%', false );
                    $wheres2 	= array();
                    $wheres2[] 	= 'a.description LIKE '.$word;
                    $wheres2[] 	= 'a.title LIKE '.$word;
                    $wheres[] 	= implode( ' OR ', $wheres2 );
                }
                $where 	= '(' . implode( ($phrase == 'all' ? ') AND (' : ') OR ('), $wheres ) . ')';
                break;
        }

        switch ( $ordering )
        {
            case 'oldest':
                $order = 'a.start_date ASC';
                break;

            case 'popular':
                $order = 'a.hits DESC';
                break;

            case 'alpha':
                $order = 'a.title ASC';
                break;

            case 'category':
                $order = 'b.title ASC, a.title ASC';
                break;

            case 'newest':
            default:
                $order = 'a.start_date DESC';
        }

        $query = 'SELECT a.id, a.title AS title, a.description AS text, a.start_date AS created,  '
        .' CONCAT("'.$section.' / ",b.catname)  as section , \'2\' AS browsernav'
        . ' FROM #__ads AS a '
        . ' INNER JOIN #__ads_categories AS b ON b.id = a.category '
        . ' WHERE ('. $where
        . ' AND a.status = '.$state_published.' '
        . $whereArchived
        . ' and a.close_by_admin = 0 '
           // . ' AND a.status = 1 and a.close_by_admin=0 and a.closed=0 '
        . ' AND b.status = 1'.')'
        . ' ORDER BY '. $order
        ;
        $db->setQuery( $query, 0, $limit );
        $rows = $db->loadObjectList();
        //echo $db->_sql;exit;

        require_once (JPATH_ROOT.DS.'components'.DS.'com_adsman'.DS.'helpers'.DS.'route.php');
        $Itemid = AdsmanHelperRoute::getMenuItemByTaskName("listadds");


        $return = array();

        if ($rows) {

            foreach($rows as $key => $row) {
                $rows[$key]->href = JRoute::_(JURI::root()."index.php?option=com_adsman&task=details&Itemid=$Itemid&id={$row->id}");
            }

            foreach($rows AS $key => $weblink) {
                if(searchHelper::checkNoHTML($weblink, $searchText, array('url', 'text', 'title'))) {
                    $return[] = $weblink;
                }
            }
        }

        return $return;
    }

}