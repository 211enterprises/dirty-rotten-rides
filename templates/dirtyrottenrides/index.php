<?php
/**
 * @package                Joomla.Site
 * @subpackage	Templates.beez_20
 * @copyright        Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license                GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.filesystem.file');

$user	= JFactory::getUser();

// check modules
$showRightColumn	= ($this->countModules('position-3') or $this->countModules('position-6') or $this->countModules('position-8'));
$showbottom			= ($this->countModules('position-9') or $this->countModules('position-10') or $this->countModules('position-11'));
$showleft			= ($this->countModules('position-4') or $this->countModules('position-7') or $this->countModules('position-5'));

if ($showRightColumn==0 and $showleft==0) {
	$showno = 0;
}

JHtml::_('behavior.framework', true);

// get params
$color				= $this->params->get('templatecolor');
$logo				= $this->params->get('logo');
$navposition		= $this->params->get('navposition');
$app				= JFactory::getApplication();
$menu				= $app->getMenu();
$doc				= JFactory::getDocument();
$templateparams		= $app->getTemplate(true)->params;

$doc->addStyleSheet($this->baseurl.'/templates/system/css/system.css');
$doc->addStyleSheet($this->baseurl.'/templates/'.$this->template.'/css/position.css', $type = 'text/css', $media = 'screen,projection');
$doc->addStyleSheet($this->baseurl.'/templates/'.$this->template.'/css/layout.css', $type = 'text/css', $media = 'screen,projection');
$doc->addStyleSheet($this->baseurl.'/templates/'.$this->template.'/css/print.css', $type = 'text/css', $media = 'print');

$files = JHtml::_('stylesheet', 'templates/'.$this->template.'/css/general.css', null, false, true);
if ($files):
	if (!is_array($files)):
		$files = array($files);
	endif;
	foreach($files as $file):
		$doc->addStyleSheet($file);
	endforeach;
endif;

if ($menu->getActive() == $menu->getDefault()) {
	//Layer video slider - Only on home page
	$doc->addStyleSheet($this->baseurl.'/templates/'.$this->template.'/css/layerslider/layerslider-style.css', $type = 'text/css', $media = 'screen');
	$doc->addStyleSheet($this->baseurl.'/templates/'.$this->template.'/css/layerslider/layerslider.css', $type = 'text/css', $media = 'screen');
	$doc->addScript($this->baseurl.'/templates/'.$this->template.'/javascript/layerslider/jquery.js', 'text/javascript');
	$doc->addScript($this->baseurl.'/templates/'.$this->template.'/javascript/layerslider/jquery-easing-1.3.js', 'text/javascript');
	$doc->addScript($this->baseurl.'/templates/'.$this->template.'/javascript/layerslider/jquery-transit-modified.js', 'text/javascript');
	$doc->addScript($this->baseurl.'/templates/'.$this->template.'/javascript/layerslider/layerslider.transitions.js', 'text/javascript');
	$doc->addScript($this->baseurl.'/templates/'.$this->template.'/javascript/layerslider/layerslider.kreaturamedia.jquery.js', 'text/javascript');
}

$doc->addStyleSheet('templates/'.$this->template.'/css/'.htmlspecialchars($color).'.css');
if ($this->direction == 'rtl') {
	$doc->addStyleSheet($this->baseurl.'/templates/'.$this->template.'/css/template_rtl.css');
	if (file_exists(JPATH_SITE . '/templates/' . $this->template . '/css/' . $color . '_rtl.css')) {
		$doc->addStyleSheet($this->baseurl.'/templates/'.$this->template.'/css/'.htmlspecialchars($color).'_rtl.css');
	}
}

$doc->addScript($this->baseurl.'/templates/'.$this->template.'/javascript/md_stylechanger.js', 'text/javascript');
$doc->addScript($this->baseurl.'/templates/'.$this->template.'/javascript/hide.js', 'text/javascript');

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>" >
<head>
<jdoc:include type="head" />

<!--[if lte IE 6]>
<link href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/ieonly.css" rel="stylesheet" type="text/css" />
<?php if ($color=="personal") : ?>
<style type="text/css">
#line {
	width:98% ;
}
.logoheader {
	height:200px;
}
#header ul.menu {
	display:block !important;
	width:98.2% ;
}
</style>
<?php endif; ?>
<![endif]-->

<!--[if IE 7]>
<link href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/ie7only.css" rel="stylesheet" type="text/css" />
<![endif]-->

<script type="text/javascript">
	var big ='<?php echo (int)$this->params->get('wrapperLarge');?>%';
	var small='<?php echo (int)$this->params->get('wrapperSmall'); ?>%';
	var altopen='<?php echo JText::_('TPL_BEEZ2_ALTOPEN', true); ?>';
	var altclose='<?php echo JText::_('TPL_BEEZ2_ALTCLOSE', true); ?>';
	var bildauf='<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/plus.png';
	var bildzu='<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/minus.png';
	var rightopen='<?php echo JText::_('TPL_BEEZ2_TEXTRIGHTOPEN', true); ?>';
	var rightclose='<?php echo JText::_('TPL_BEEZ2_TEXTRIGHTCLOSE', true); ?>';
	var fontSizeTitle='<?php echo JText::_('TPL_BEEZ2_FONTSIZE', true); ?>';
	var bigger='<?php echo JText::_('TPL_BEEZ2_BIGGER', true); ?>';
	var reset='<?php echo JText::_('TPL_BEEZ2_RESET', true); ?>';
	var smaller='<?php echo JText::_('TPL_BEEZ2_SMALLER', true); ?>';
	var biggerTitle='<?php echo JText::_('TPL_BEEZ2_INCREASE_SIZE', true); ?>';
	var resetTitle='<?php echo JText::_('TPL_BEEZ2_REVERT_STYLES_TO_DEFAULT', true); ?>';
	var smallerTitle='<?php echo JText::_('TPL_BEEZ2_DECREASE_SIZE', true); ?>';
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#layerslider').layerSlider({
			skinsPath : '/templates/dirtyrottenrides/layerslider-skins/',
			skin : 'borderlessdark',
			animateFirstLayer: false,
			autoPlayVideos: false
		});
	});		
</script>
</head>

<body>

	<div id="all">
		<div id="back">
			<div id="header">
				<div class="drrBrand"></div>
				<div class="ui-header-model">
					<img src="templates/dirtyrottenrides/images/ui-header-model.png" alt="" />
				</div>
				
				<!-- Social Media Links-->
				<?php include_once('html/social.tmpl.php'); ?>
				
				<ul class="headerSubLinks">
				<?php if($user->authorise('core.login.offline')) { ?>
					<li><a href="/index.php/dirtygirl-submit" title="Dirty Girl Submissions">Be a dirty girl now</a></li>
					<li><a href="javascript:void(0);" title="Logout" onclick="document.communitylogout.submit();">Logout</a></li>
				<?php } ?>
				</ul>
				            		
				<ul class="skiplinks">
	      	<li><a href="#main" class="u2"><?php echo JText::_('TPL_BEEZ2_SKIP_TO_CONTENT'); ?></a></li>
	        <li><a href="#nav" class="u2"><?php echo JText::_('TPL_BEEZ2_JUMP_TO_NAV'); ?></a></li>
				<?php if($showRightColumn ):?>
	        <li><a href="#additional" class="u2"><?php echo JText::_('TPL_BEEZ2_JUMP_TO_INFO'); ?></a></li>
	      <?php endif; ?>
	      </ul>
	      <h2 class="unseen"><?php echo JText::_('TPL_BEEZ2_NAV_VIEW_SEARCH'); ?></h2>
	      <h3 class="unseen"><?php echo JText::_('TPL_BEEZ2_NAVIGATION'); ?></h3>
	      <jdoc:include type="modules" name="position-1" />
	      <!--<div id="line">
	      	<div id="fontsize"></div>
	        <h3 class="unseen"><?php //echo JText::_('TPL_BEEZ2_SEARCH'); ?></h3>
	        <jdoc:include type="modules" name="position-0" />
				</div> end line -->
	
			</div><!-- end header -->
	    <div id="<?php echo $showRightColumn ? 'contentarea2' : 'contentarea'; ?>">
			<?php
			//if homepage, show banner
			if(JRequest::getVar('view') == "frontpage" ) { ?>
	      <div class="logoheader">
	      	<!--<div class="dirtyGirlBanner" style="margin: 0 0 14px 0;"></div>-->
	      	
	        <?php include_once('html/home-media.tmpl.php'); ?>

	        <div class="dirtyGirlBanner">
		        <a href="/index.php/dirty-girls/26?view=dirtygirlpage"><img src="/images/banners/NovemberWinnerBanner1.jpg" alt="" /></a>
	        </div>
				</div><!-- end logoheader -->
			<?php } ?>
	    	<!--<div id="breadcrumbs">
					<jdoc:include type="modules" name="position-2" />
	      </div>-->
				<?php if ($navposition=='left' and $showleft) : ?>
					<div class="left1 <?php if ($showRightColumn==NULL){ echo 'leftbigger';} ?>" id="nav">
		      	<jdoc:include type="modules" name="position-7" style="beezDivision" headerLevel="3" />
		        <jdoc:include type="modules" name="position-4" style="beezHide" headerLevel="3" state="0 " />
		        <jdoc:include type="modules" name="position-5" style="beezTabs" headerLevel="2"  id="3" />
					</div><!-- end navi -->
		      <?php endif; ?>
					<div id="<?php echo $showRightColumn ? 'wrapper' : 'wrapper2'; ?>" <?php if (isset($showno)){echo 'class="shownocolumns"';}?>>
						<div id="main">
						<?php if ($this->countModules('position-12')): ?>
		        <div id="top">
		        	<jdoc:include type="modules" name="position-12" />
		        </div>
		        <?php endif; ?>
						<jdoc:include type="message" />
		        <jdoc:include type="component" />
					</div><!-- end main -->
				</div><!-- end wrapper -->
	
				<?php if ($showRightColumn) : ?>
		    <h2 class="unseen">
		    	<?php echo JText::_('TPL_BEEZ2_ADDITIONAL_INFORMATION'); ?>
		    </h2>
		    <div id="close">
		    	<a href="#" onclick="auf('right')">
		      <span id="bild">
		      	<?php echo JText::_('TPL_BEEZ2_TEXTRIGHTCLOSE'); ?></span></a>
		    </div>
		
				<div id="right">
		    	<a id="additional"></a>
		      <jdoc:include type="modules" name="position-6" style="beezDivision" headerLevel="3"/>
		      <jdoc:include type="modules" name="position-8" style="beezDivision" headerLevel="3"  />
		      <jdoc:include type="modules" name="position-3" style="beezDivision" headerLevel="3"  />
		    </div><!-- end right -->
		    <?php endif; ?>
		
		    <?php if ($navposition=='center' and $showleft) : ?>
		
		    <div class="left <?php if ($showRightColumn==NULL){ echo 'leftbigger';} ?>" id="nav" >
					<jdoc:include type="modules" name="position-7"  style="beezDivision" headerLevel="3" />
		      <jdoc:include type="modules" name="position-4" style="beezHide" headerLevel="3" state="0 " />
		      <jdoc:include type="modules" name="position-5" style="beezTabs" headerLevel="2"  id="3" />
				</div><!-- end navi -->
	    	<?php endif; ?>
				<div class="wrap"></div>
			</div> <!-- end contentarea -->
	
			</div><!-- back -->
	
		</div><!-- all -->
	
		<div id="footer-outer">
		<?php if ($showbottom) : ?>
	  	<div id="footer-inner">
				<div class="copyright">
					&copy; <?php echo date("Y"); ?> Dirty Rotten Rides. All Rights Reserved.
				</div>
				<div class="visitStore">
					<a href="http://www.dirtyrottenridesapparel.com"><img src="/templates/system/images/DRR_swag.jpg" alt="Dirty Rotten Rides Swag Store" /></a>
				</div>
	  	</div>
		<?php endif ; ?>
		</div>
		<jdoc:include type="modules" name="debug" />
	</body>
</html>
