<?php

// Get DB connection
$db = JFactory::getDBO();
// Create a new query object
$db->setQuery("SELECT * FROM #__home_media");

$results = $db->loadObjectList();
?>
<?php if(count($results) > 0) { ?>
<div id="layerslider-container">
	<div id="layerslider" style="width: 960px; height: 540px;">
<?php foreach($results as $i => $value) { ?>
  <?php if($value->type == 'image') { ?>
    <div class="ls-layer"  style="slidedirection: top; slidedelay: 4000; durationin: 1500; durationout: 1500; easingin: easeInOutQuint; easingout: easeInOutQuint; delayin: 0; delayout: 0;">
      <div class="ls-s0" style="durationin : 1500; durationout : 1500; easingin : easeInOutQuint; easingout : easeInOutQuint; delayin : 0; delayout : 0;">
        <img src="<?php echo '/administrator/components/com_home_media/uploads/' . $value->image_url; ?>" alt="" />
      </div>
    </div>
  <?php } elseif($value->type == 'youtube') { ?>
    <iframe width="960" height="540" src="<?php echo $value->url; ?>" frameborder="0" allowfullscreen></iframe>
  <?php } elseif($value->type == 'vimeo') { ?>
    <iframe width="960" height="540" src="<?php echo $value->url; ?>" frameborder="0" allowfullscreen></iframe>
  <?php } ?>
<?php } ?>
  </div>
</div>
<?php } ?>