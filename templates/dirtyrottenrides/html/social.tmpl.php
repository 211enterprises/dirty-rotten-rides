<?php $config =& JFactory::getConfig(); ?>

<ul class="social">
	<li>
		<a href="<?php echo $config->getValue('facebook_link'); ?>" target="_blank">
		<img src="<?php echo '/templates/'.$this->template.'/images/social-icons/facebook.png'; ?>" alt="Facebook" />
		</a>
	</li>
	<li>
		<a href="<?php echo $config->getValue('twitter_link'); ?>" target="_blank">
			<img src="<?php echo '/templates/'.$this->template.'/images/social-icons/twitter.png'; ?>" alt="Twitter" />
		</a>
	</li>
	<li>
		<a href="<?php echo $config->getValue('instagram_link'); ?>" target="_blank">
			<img src="<?php echo '/templates/'.$this->template.'/images/social-icons/instagram.png'; ?>" alt="Instagram" />
		</a>
	</li>
	<li>
		<a href="<?php echo $config->getValue('youtube_link'); ?>" target="_blank">
			<img src="<?php echo '/templates/'.$this->template.'/images/social-icons/youtube.png'; ?>" alt="YouTube" />
		</a>
	</li>
</ul>