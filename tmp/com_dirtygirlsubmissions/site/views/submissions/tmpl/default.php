<?php
/**
 * @version     1.0.0
 * @package     com_dirtygirlsubmissions
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Allan Cutler <allan@211enterprises.com> - http://www.211enterprises.com
 */
// no direct access
defined('_JEXEC') or die;
?>
<script type="text/javascript">
    function deleteItem(item_id){
        if(confirm("<?php echo JText::_('COM_DIRTYGIRLSUBMISSIONS_DELETE_MESSAGE'); ?>")){
            document.getElementById('form-dirtygirlsubmission-delete-' + item_id).submit();
        }
    }
</script>



