<?php
/**
 * @version     1.0.0
 * @package     com_dirtygirlsubmissions
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Allan Cutler <allan@211enterprises.com> - http://www.211enterprises.com
 */
// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');
// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_dirtygirlsubmissions/assets/css/dirtygirlsubmissions.css');
?>
<script type="text/javascript">
    function getScript(url,success) {
        var script = document.createElement('script');
        script.src = url;
        var head = document.getElementsByTagName('head')[0],
        done = false;
        // Attach handlers for all browsers
        script.onload = script.onreadystatechange = function() {
            if (!done && (!this.readyState
                || this.readyState == 'loaded'
                || this.readyState == 'complete')) {
                done = true;
                success();
                script.onload = script.onreadystatechange = null;
                head.removeChild(script);
            }
        };
        head.appendChild(script);
    }
    getScript('//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',function() {
        js = jQuery.noConflict();
        js(document).ready(function(){
            

            Joomla.submitbutton = function(task)
            {
                if (task == 'dirtygirlsubmission.cancel') {
                    Joomla.submitform(task, document.getElementById('dirtygirlsubmission-form'));
                }
                else{
                    
                    if (task != 'dirtygirlsubmission.cancel' && document.formvalidator.isValid(document.id('dirtygirlsubmission-form'))) {
                        
                        Joomla.submitform(task, document.getElementById('dirtygirlsubmission-form'));
                    }
                    else {
                        alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
                    }
                }
            }
        });
    });
</script>

<form action="<?php echo JRoute::_('index.php?option=com_dirtygirlsubmissions&layout=edit&id=' . (int) $this->item->id); ?>" method="post" enctype="multipart/form-data" name="adminForm" id="dirtygirlsubmission-form" class="form-validate">
    <div class="width-60 fltlft">
        <fieldset class="adminform">
            <legend><?php echo JText::_('COM_DIRTYGIRLSUBMISSIONS_LEGEND_DIRTYGIRLSUBMISSION'); ?></legend>
            <ul class="adminformlist">

                				<li><?php echo $this->form->getLabel('id'); ?>
				<?php echo $this->form->getInput('id'); ?></li>
				<li><?php echo $this->form->getLabel('first_name'); ?>
				<?php echo $this->form->getInput('first_name'); ?></li>
				<li><?php echo $this->form->getLabel('last_name'); ?>
				<?php echo $this->form->getInput('last_name'); ?></li>
				<li><?php echo $this->form->getLabel('email_address'); ?>
				<?php echo $this->form->getInput('email_address'); ?></li>
				<li><?php echo $this->form->getLabel('phone'); ?>
				<?php echo $this->form->getInput('phone'); ?></li>
				<li><?php echo $this->form->getLabel('age'); ?>
				<?php echo $this->form->getInput('age'); ?></li>
				<li><?php echo $this->form->getLabel('where_are_you_from'); ?>
				<?php echo $this->form->getInput('where_are_you_from'); ?></li>
				<li><?php echo $this->form->getLabel('previous_pinup'); ?>
				<?php echo $this->form->getInput('previous_pinup'); ?></li>
				<li><?php echo $this->form->getLabel('favorite_car'); ?>
				<?php echo $this->form->getInput('favorite_car'); ?></li>
				<li><?php echo $this->form->getLabel('favorite_pinup'); ?>
				<?php echo $this->form->getInput('favorite_pinup'); ?></li>
				<li><?php echo $this->form->getLabel('special_talents'); ?>
				<?php echo $this->form->getInput('special_talents'); ?></li>
				<li><?php echo $this->form->getLabel('why_you'); ?>
				<?php echo $this->form->getInput('why_you'); ?></li>
				<li><?php echo $this->form->getLabel('biggest_turn_on'); ?>
				<?php echo $this->form->getInput('biggest_turn_on'); ?></li>
				<li><?php echo $this->form->getLabel('biggest_turn_off'); ?>
				<?php echo $this->form->getInput('biggest_turn_off'); ?></li>
				<li><?php echo $this->form->getLabel('favorite_quote'); ?>
				<?php echo $this->form->getInput('favorite_quote'); ?></li>
				<li><?php echo $this->form->getLabel('created_by'); ?>
				<?php echo $this->form->getInput('created_by'); ?></li>
				<li><?php echo $this->form->getLabel('created_at'); ?>
				<?php echo $this->form->getInput('created_at'); ?></li>


            </ul>
        </fieldset>
    </div>

    

    <input type="hidden" name="task" value="" />
    <?php echo JHtml::_('form.token'); ?>
    <div class="clr"></div>

    <style type="text/css">
        /* Temporary fix for drifting editor fields */
        .adminformlist li {
            clear: both;
        }
    </style>
</form>