<?php
/**
 * @version     1.0.0
 * @package     com_dirtygirlsubmissions
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Allan Cutler <allan@211enterprises.com> - http://www.211enterprises.com
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Dirtygirlsubmission controller class.
 */
class DirtygirlsubmissionsControllerDirtygirlsubmission extends JControllerForm
{

    function __construct() {
        $this->view_list = 'dirtygirlsubmissions';
        parent::__construct();
    }

}