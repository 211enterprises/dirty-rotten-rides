<?php
/**
 * @version     1.0.0
 * @package     com_dirtygirlsubmissions
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Allan Cutler <allan@211enterprises.com> - http://www.211enterprises.com
 */

// No direct access
defined('_JEXEC') or die;

/**
 * Dirtygirlsubmissions helper.
 */
class DirtygirlsubmissionsHelper
{
	/**
	 * Configure the Linkbar.
	 */
	public static function addSubmenu($vName = '')
	{
		JSubMenuHelper::addEntry(
			JText::_('COM_DIRTYGIRLSUBMISSIONS_TITLE_DIRTYGIRLSUBMISSIONS'),
			'index.php?option=com_dirtygirlsubmissions&view=dirtygirlsubmissions',
			$vName == 'dirtygirlsubmissions'
		);
		JSubMenuHelper::addEntry(
			JText::_('COM_DIRTYGIRLSUBMISSIONS_TITLE_SUBMISSIONS'),
			'index.php?option=com_dirtygirlsubmissions&view=submissions',
			$vName == 'submissions'
		);

	}

	/**
	 * Gets a list of the actions that can be performed.
	 *
	 * @return	JObject
	 * @since	1.6
	 */
	public static function getActions()
	{
		$user	= JFactory::getUser();
		$result	= new JObject;

		$assetName = 'com_dirtygirlsubmissions';

		$actions = array(
			'core.admin', 'core.manage', 'core.create', 'core.edit', 'core.edit.own', 'core.edit.state', 'core.delete'
		);

		foreach ($actions as $action) {
			$result->set($action, $user->authorise($action, $assetName));
		}

		return $result;
	}
}
