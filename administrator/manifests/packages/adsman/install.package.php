<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/


class pkg_adsmanInstallerScript {

    function postflight($route,$adapter) {

        $session = JFactory::getSession();
        echo $session->get('com_adsman_install_msg');
        $session->set('com_adsman_install_msg',null);

    }
}