CREATE TABLE IF NOT EXISTS `#__dirtygirlsubmissions_` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`first_name` VARCHAR(255)  NOT NULL ,
`last_name` VARCHAR(255)  NOT NULL ,
`email_address` VARCHAR(255)  NOT NULL ,
`phone` VARCHAR(255)  NOT NULL ,
`age` VARCHAR(3)  NOT NULL ,
`where_are_you_from` VARCHAR(255)  NOT NULL ,
`previous_pinup` VARCHAR(255)  NOT NULL ,
`favorite_car` VARCHAR(255)  NOT NULL ,
`favorite_pinup` VARCHAR(255)  NOT NULL ,
`special_talents` TEXT NOT NULL ,
`why_you` TEXT NOT NULL ,
`biggest_turn_on` VARCHAR(255)  NOT NULL ,
`biggest_turn_off` VARCHAR(255)  NOT NULL ,
`favorite_quote` VARCHAR(255)  NOT NULL ,
`image_1` VARCHAR(255)  NOT NULL ,
`image_2` VARCHAR(255)  NOT NULL ,
`image_3` VARCHAR(255)  NOT NULL ,
`image_4` VARCHAR(255)  NOT NULL ,
`image_5` VARCHAR(255)  NOT NULL ,
`archive` INT(11) DEFAULT 0 , 
`created_by` INT(11)  NOT NULL ,
`created_at` DATETIME NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8_general_ci;

