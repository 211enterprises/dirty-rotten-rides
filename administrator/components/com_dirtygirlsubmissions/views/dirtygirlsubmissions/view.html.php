<?php
/**
 * @version     1.0.0
 * @package     com_dirtygirlsubmissions
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Allan Cutler <allan@211enterprises.com> - http://www.211enterprises.com
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Dirtygirlsubmissions.
 */
class DirtygirlsubmissionsViewDirtygirlsubmissions extends JView
{
	protected $items;
	protected $pagination;
	protected $state;

	/**
	 * Display the view
	 */
	public function display($tpl = null)
	{
		$this->state		= $this->get('State');
		$this->items		= $this->get('Items');
		$this->pagination	= $this->get('Pagination');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			throw new Exception(implode("\n", $errors));
		}
        
		$this->addToolbar();
        
        $input = JFactory::getApplication()->input;
        $view = $input->getCmd('view', '');
        DirtygirlsubmissionsHelper::addSubmenu($view);
        
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since	1.6
	 */
	protected function addToolbar()
	{
		require_once JPATH_COMPONENT.'/helpers/dirtygirlsubmissions.php';

		$state	= $this->get('State');
		$canDo	= DirtygirlsubmissionsHelper::getActions($state->get('filter.category_id'));

		JToolBarHelper::title(JText::_('COM_DIRTYGIRLSUBMISSIONS_TITLE_DIRTYGIRLSUBMISSIONS'), 'dirtygirlsubmissions.png');

        //Check if the form exists before showing the add/edit buttons
        $formPath = JPATH_COMPONENT_ADMINISTRATOR.'/views/dirtygirlsubmission';
        if (file_exists($formPath)) {

          if ($canDo->get('core.create')) {
				    JToolBarHelper::addNew('dirtygirlsubmission.add','JTOOLBAR_NEW');
			    }
	
			    if ($canDo->get('core.edit') && isset($this->items[0])) {
				    JToolBarHelper::editList('dirtygirlsubmission.edit','JTOOLBAR_EDIT');
			    }
			    
			    //Archives Button
			    JToolBarHelper::custom('dirtygirlsubmissions.showSubmissionArchives', 'archive', '', 'Archives', false, false );

        }

		if ($canDo->get('core.edit.state')) {

            if (isset($this->items[0]->state)) {
			    JToolBarHelper::divider();
			    JToolBarHelper::custom('dirtygirlsubmissions.publish', 'publish.png', 'publish_f2.png','JTOOLBAR_PUBLISH', true);
			    JToolBarHelper::custom('dirtygirlsubmissions.unpublish', 'unpublish.png', 'unpublish_f2.png', 'JTOOLBAR_UNPUBLISH', true);
            } else if (isset($this->items[0])) {
                //If this component does not use state then show a direct delete button as we can not trash
                JToolBarHelper::deleteList('', 'dirtygirlsubmissions.delete','JTOOLBAR_DELETE');
            }

            if (isset($this->items[0]->state)) {
			    JToolBarHelper::divider();
			    JToolBarHelper::archiveList('dirtygirlsubmissions.archive','JTOOLBAR_ARCHIVE');
            }
            if (isset($this->items[0]->checked_out)) {
            	JToolBarHelper::custom('dirtygirlsubmissions.checkin', 'checkin.png', 'checkin_f2.png', 'JTOOLBAR_CHECKIN', true);
            }
		}
        
        //Show trash and delete for components that uses the state field
        if (isset($this->items[0]->state)) {
		    if ($state->get('filter.state') == -2 && $canDo->get('core.delete')) {
			    JToolBarHelper::deleteList('', 'dirtygirlsubmissions.delete','JTOOLBAR_EMPTY_TRASH');
			    JToolBarHelper::divider();
		    } else if ($canDo->get('core.edit.state')) {
			    JToolBarHelper::trash('dirtygirlsubmissions.trash','JTOOLBAR_TRASH');
			    JToolBarHelper::divider();
		    }
        }

		if ($canDo->get('core.admin')) {
			JToolBarHelper::preferences('com_dirtygirlsubmissions');
		}


	}
}
