<?php
/**
 * @version     1.0.0
 * @package     com_dirtygirlsubmissions
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Allan Cutler <allan@211enterprises.com> - http://www.211enterprises.com
 */
// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');
// Import CSS
$document = JFactory::getDocument();
$config =& JFactory::getConfig();
$document->addStyleSheet('components/com_dirtygirlsubmissions/assets/css/dirtygirlsubmissions.css');
$document->addStyleSheet('/templates/system/css/lightbox.css');

$document->addScript('/templates/system/js/jquery-1.10.2.min.js');
$document->addScript('/templates/system/js/lightbox-2.6.min.js');

$uploadPath = 'components' . DIRECTORY_SEPARATOR . 'com_dirtygirlsubmissions' . DIRECTORY_SEPARATOR . 'uploads/submissions' .DIRECTORY_SEPARATOR;

// Check if local file exists, if not check CDN
function checkLocalUploadFile($config, $file) {
  if(file_exists(JRoute::_(JUri::base() . $file, false)))
  {
    return JRoute::_(JUri::base() . $file, false);
  }
  else {
    return $config->getValue('cdn_host') . '/administrator/'. $file;
  }
}
?>
<style type="text/css">
	.clearfix {
		clear: both;
	}
</style>
<script type="text/javascript">
    function getScript(url,success) {
        var script = document.createElement('script');
        script.src = url;
        var head = document.getElementsByTagName('head')[0],
        done = false;
        // Attach handlers for all browsers
        script.onload = script.onreadystatechange = function() {
            if (!done && (!this.readyState
                || this.readyState == 'loaded'
                || this.readyState == 'complete')) {
                done = true;
                success();
                script.onload = script.onreadystatechange = null;
                head.removeChild(script);
            }
        };
        head.appendChild(script);
    }
    getScript('//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',function() {
        js = jQuery.noConflict();
        js(document).ready(function(){
            

            Joomla.submitbutton = function(task)
            {
                if (task == 'dirtygirlsubmission.cancel') {
                    Joomla.submitform(task, document.getElementById('dirtygirlsubmission-form'));
                }
                else{
                    
				js = jQuery.noConflict();
				if(js('#jform_image_1').val() != ''){
					js('#jform_image_1_hidden').val(js('#jform_image_1').val());
				}
				if (js('#jform_image_1').val() == '' && js('#jform_image_1_hidden').val() == '') {
					alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
					return;
				}
				js = jQuery.noConflict();
				if(js('#jform_image_2').val() != ''){
					js('#jform_image_2_hidden').val(js('#jform_image_2').val());
				}
				if (js('#jform_image_2').val() == '' && js('#jform_image_2_hidden').val() == '') {
					alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
					return;
				}
				js = jQuery.noConflict();
				if(js('#jform_image_3').val() != ''){
					js('#jform_image_3_hidden').val(js('#jform_image_3').val());
				}
				if (js('#jform_image_3').val() == '' && js('#jform_image_3_hidden').val() == '') {
					alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
					return;
				}
				js = jQuery.noConflict();
				if(js('#jform_image_4').val() != ''){
					js('#jform_image_4_hidden').val(js('#jform_image_4').val());
				}
				if (js('#jform_image_4').val() == '' && js('#jform_image_4_hidden').val() == '') {
					alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
					return;
				}
				js = jQuery.noConflict();
				if(js('#jform_image_5').val() != ''){
					js('#jform_image_5_hidden').val(js('#jform_image_5').val());
				}
				if (js('#jform_image_5').val() == '' && js('#jform_image_5_hidden').val() == '') {
					alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
					return;
				}
                    if (task != 'dirtygirlsubmission.cancel' && document.formvalidator.isValid(document.id('dirtygirlsubmission-form'))) {
                        
                        Joomla.submitform(task, document.getElementById('dirtygirlsubmission-form'));
                    }
                    else {
                        alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
                    }
                }
            }
        });
    });
</script>

<form action="<?php echo JRoute::_('index.php?option=com_dirtygirlsubmissions&layout=edit&id=' . (int) $this->item->id); ?>" method="post" enctype="multipart/form-data" name="adminForm" id="dirtygirlsubmission-form" class="form-validate">
    <div class="width-60 fltlft">
        <fieldset class="adminform">
            <legend><?php echo JText::_('COM_DIRTYGIRLSUBMISSIONS_LEGEND_DIRTYGIRLSUBMISSION'); ?></legend>
            <ul class="adminformlist">

							<li>
								<?php echo $this->form->getLabel('id'); ?>
								<?php echo $this->form->getInput('id'); ?>
							</li>
							<li>
								<?php echo $this->form->getLabel('first_name'); ?>
								<?php echo $this->form->getInput('first_name'); ?>
							</li>
							<li>
								<?php echo $this->form->getLabel('last_name'); ?>
								<?php echo $this->form->getInput('last_name'); ?>
							</li>
							<li>
								<?php echo $this->form->getLabel('email_address'); ?>
								<?php echo $this->form->getInput('email_address'); ?>
							</li>
							<li>
								<?php echo $this->form->getLabel('phone'); ?>
								<?php echo $this->form->getInput('phone'); ?>
							</li>
							<li>
								<?php echo $this->form->getLabel('age'); ?>
								<?php echo $this->form->getInput('age'); ?>
							</li>
							<li>
								<?php echo $this->form->getLabel('where_are_you_from'); ?>
								<?php echo $this->form->getInput('where_are_you_from'); ?>
							</li>
							<li>
								<?php echo $this->form->getLabel('previous_pinup'); ?>
								<?php echo $this->form->getInput('previous_pinup'); ?>
							</li>
							<li>
								<?php echo $this->form->getLabel('favorite_car'); ?>
								<?php echo $this->form->getInput('favorite_car'); ?>
							</li>
							<li>
								<?php echo $this->form->getLabel('favorite_pinup'); ?>
								<?php echo $this->form->getInput('favorite_pinup'); ?>
							</li>
							<li>
								<?php echo $this->form->getLabel('special_talents'); ?>
								<?php echo $this->form->getInput('special_talents'); ?>
							</li>
							<li>
								<?php echo $this->form->getLabel('why_you'); ?>
								<?php echo $this->form->getInput('why_you'); ?>
							</li>
							<li>
								<?php echo $this->form->getLabel('biggest_turn_on'); ?>
								<?php echo $this->form->getInput('biggest_turn_on'); ?>
							</li>
							<li>
								<?php echo $this->form->getLabel('biggest_turn_off'); ?>
								<?php echo $this->form->getInput('biggest_turn_off'); ?>
							</li>
							<li>
								<?php echo $this->form->getLabel('favorite_quote'); ?>
								<?php echo $this->form->getInput('favorite_quote'); ?>
							</li>
							<li>
								<?php echo $this->form->getLabel('image_1'); ?>
								<?php echo $this->form->getInput('image_1'); ?>
							</li>

							<input type="hidden" name="jform[image_1]" id="jform_image_1_hidden" value="<?php echo $this->item->image_1 ?>" />
							<li>
								<?php echo $this->form->getLabel('image_2'); ?>
								<?php echo $this->form->getInput('image_2'); ?>
							</li>
							<input type="hidden" name="jform[image_2]" id="jform_image_2_hidden" value="<?php echo $this->item->image_2 ?>" />
							<li>
								<?php echo $this->form->getLabel('image_3'); ?>
								<?php echo $this->form->getInput('image_3'); ?>
							</li>
							<input type="hidden" name="jform[image_3]" id="jform_image_3_hidden" value="<?php echo $this->item->image_3 ?>" />
							<li>
								<?php echo $this->form->getLabel('image_4'); ?>
								<?php echo $this->form->getInput('image_4'); ?>
							</li>
							<input type="hidden" name="jform[image_4]" id="jform_image_4_hidden" value="<?php echo $this->item->image_4 ?>" />
							<li>
								<?php echo $this->form->getLabel('image_5'); ?>
								<?php echo $this->form->getInput('image_5'); ?>
							</li>
							<input type="hidden" name="jform[image_5]" id="jform_image_5_hidden" value="<?php echo $this->item->image_5 ?>" />				
							<li>
								<?php echo $this->form->getLabel('archive'); ?>
								<select name="jform[archive]" id="jform_archive">
									<option value="0">No</option>
									<option value="1" <?php if($this->item->archive == 1) echo 'selected="selected"' ?>>Yes</option>
								</select>
							</li>
							<li>
								<?php echo $this->form->getLabel('created_by'); ?>
								<?php echo $this->form->getInput('created_by'); ?>
							</li>
							<li>
								<?php echo $this->form->getLabel('created_at'); ?>
								<?php echo $this->form->getInput('created_at'); ?>
							</li>
					</ul>
				</fieldset>
    </div>

    

    <input type="hidden" name="task" value="" />
    <?php echo JHtml::_('form.token'); ?>
    <div class="clr"></div>

    <style type="text/css">
        /* Temporary fix for drifting editor fields */
        .adminformlist li {
            clear: both;
        }
    </style>
</form>

<div class="width-60 fltlft">
	<fieldset class="adminform">
		<legend>Current Uploaded Images</legend>
    <ul class="adminformlist uploadedImgs">
    	<li>
				<?php if (!empty($this->item->image_1)) : ?>
				<a href="<?php echo checkLocalUploadFile($config, $uploadPath . $this->item->image_1); ?>" data-lightbox="images">
          <img src="<?php echo checkLocalUploadFile($config, $uploadPath . $this->item->image_1); ?>" alt="" />
        </a>
				<?php endif; ?>
    	</li>
    	<li>
				<?php if (!empty($this->item->image_2)) : ?>
				<a href="<?php echo checkLocalUploadFile($config, $uploadPath . $this->item->image_2); ?>" data-lightbox="images">
          <img src="<?php echo checkLocalUploadFile($config, $uploadPath . $this->item->image_2); ?>" alt="" />
        </a>
				<?php endif; ?>
    	</li>
    	<li>
				<?php if (!empty($this->item->image_3)) : ?>
				<a href="<?php echo checkLocalUploadFile($config, $uploadPath . $this->item->image_3); ?>" data-lightbox="images">
          <img src="<?php echo checkLocalUploadFile($config, $uploadPath . $this->item->image_3); ?>" alt="" />
        </a>
				<?php endif; ?>
    	</li>
    	<li>
				<?php if (!empty($this->item->image_4)) : ?>
				<a href="<?php echo checkLocalUploadFile($config, $uploadPath . $this->item->image_4); ?>" data-lightbox="images">
          <img src="<?php echo checkLocalUploadFile($config, $uploadPath . $this->item->image_4); ?>" alt="" />
        </a>
				<?php endif; ?>
    	</li>
    	<li>
				<?php if (!empty($this->item->image_5)) : ?>
				<a href="<?php echo checkLocalUploadFile($config, $uploadPath . $this->item->image_5); ?>" data-lightbox="images">
          <img src="<?php echo checkLocalUploadFile($config, $uploadPath . $this->item->image_5); ?>" alt="" />
        </a>
				<?php endif; ?>
    	</li>
    </ul>        
</div>