CREATE TABLE IF NOT EXISTS `#__votingimages_` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`poll_id` INT(11)  NOT NULL ,
`dirty_girl_name` VARCHAR(255)  NOT NULL ,
`thumbnail_image` VARCHAR(255)  NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8_general_ci;

