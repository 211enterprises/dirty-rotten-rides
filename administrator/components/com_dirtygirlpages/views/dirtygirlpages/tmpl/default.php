<?php
/**
 * @version     1.0.0
 * @package     com_dirtygirlpages
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Allan Cutler <allan@211enterprises.com> - http://www.211enterprises.com
 */


// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.tooltip');
JHTML::_('script','system/multiselect.js',false,true);
// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_dirtygirlpages/assets/css/dirtygirlpages.css');

$user	= JFactory::getUser();
$userId	= $user->get('id');
$listOrder	= $this->state->get('list.ordering');
$listDirn	= $this->state->get('list.direction');
$canOrder	= $user->authorise('core.edit.state', 'com_dirtygirlpages');
$saveOrder	= $listOrder == 'a.ordering';
?>

<form action="<?php echo JRoute::_('index.php?option=com_dirtygirlpages&view=dirtygirlpages'); ?>" method="post" name="adminForm" id="adminForm">
	<fieldset id="filter-bar">
		<div class="filter-search fltlft">
			<label class="filter-search-lbl" for="filter_search"><?php echo JText::_('JSEARCH_FILTER_LABEL'); ?></label>
			<input type="text" name="filter_search" id="filter_search" value="<?php echo $this->escape($this->state->get('filter.search')); ?>" title="<?php echo JText::_('Search'); ?>" />
			<button type="submit"><?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?></button>
			<button type="button" onclick="document.id('filter_search').value='';this.form.submit();"><?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?></button>
		</div>
		
        

	</fieldset>
	<div class="clr"> </div>

	<table class="adminlist">
		<thead>
			<tr>
				<th width="1%">
					<input type="checkbox" name="checkall-toggle" value="" onclick="checkAll(this)" />
				</th>

				<th class='left'>
				<?php echo JHtml::_('grid.sort',  'COM_DIRTYGIRLPAGES_DIRTYGIRLPAGES_CAMPAIGN_MONTH', 'a.campaign_month', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('grid.sort',  'COM_DIRTYGIRLPAGES_DIRTYGIRLPAGES_CAMPAIGN_YEAR', 'a.campaign_year', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('grid.sort',  'COM_DIRTYGIRLPAGES_DIRTYGIRLPAGES_DIRTY_GIRL_NAME', 'a.dirty_girl_name', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('grid.sort',  'COM_DIRTYGIRLPAGES_DIRTYGIRLPAGES_DIRTY_GIRL_BIO', 'a.dirty_girl_bio', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('grid.sort',  'COM_DIRTYGIRLPAGES_DIRTYGIRLPAGES_THUMBNAIL_IMAGE', 'a.thumbnail_image', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('grid.sort',  'COM_DIRTYGIRLPAGES_DIRTYGIRLPAGES_IMAGE_1', 'a.image_1', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('grid.sort',  'COM_DIRTYGIRLPAGES_DIRTYGIRLPAGES_IMAGE_2', 'a.image_2', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('grid.sort',  'COM_DIRTYGIRLPAGES_DIRTYGIRLPAGES_IMAGE_3', 'a.image_3', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('grid.sort',  'COM_DIRTYGIRLPAGES_DIRTYGIRLPAGES_IMAGE_4', 'a.image_4', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('grid.sort',  'COM_DIRTYGIRLPAGES_DIRTYGIRLPAGES_IMAGE_5', 'a.image_5', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('grid.sort',  'COM_DIRTYGIRLPAGES_DIRTYGIRLPAGES_CREATED_AT', 'a.created_at', $listDirn, $listOrder); ?>
				</th>


                <?php if (isset($this->items[0]->state)) { ?>
				<th width="5%">
					<?php echo JHtml::_('grid.sort',  'JPUBLISHED', 'a.state', $listDirn, $listOrder); ?>
				</th>
                <?php } ?>
                <?php if (isset($this->items[0]->ordering)) { ?>
				<th width="10%">
					<?php echo JHtml::_('grid.sort',  'JGRID_HEADING_ORDERING', 'a.ordering', $listDirn, $listOrder); ?>
					<?php if ($canOrder && $saveOrder) :?>
						<?php echo JHtml::_('grid.order',  $this->items, 'filesave.png', 'dirtygirlpages.saveorder'); ?>
					<?php endif; ?>
				</th>
                <?php } ?>
                <?php if (isset($this->items[0]->id)) { ?>
                <th width="1%" class="nowrap">
                    <?php echo JHtml::_('grid.sort',  'JGRID_HEADING_ID', 'a.id', $listDirn, $listOrder); ?>
                </th>
                <?php } ?>
			</tr>
		</thead>
		<tfoot>
			<?php 
                if(isset($this->items[0])){
                    $colspan = count(get_object_vars($this->items[0]));
                }
                else{
                    $colspan = 10;
                }
            ?>
			<tr>
				<td colspan="<?php echo $colspan ?>">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
		</tfoot>
		<tbody>
		<?php foreach ($this->items as $i => $item) :
			$ordering	= ($listOrder == 'a.ordering');
			$canCreate	= $user->authorise('core.create',		'com_dirtygirlpages');
			$canEdit	= $user->authorise('core.edit',			'com_dirtygirlpages');
			$canCheckin	= $user->authorise('core.manage',		'com_dirtygirlpages');
			$canChange	= $user->authorise('core.edit.state',	'com_dirtygirlpages');
			?>
			<tr class="row<?php echo $i % 2; ?>">
				<td class="center">
					<?php echo JHtml::_('grid.id', $i, $item->id); ?>
				</td>

				<td>
				<?php if (isset($item->checked_out) && $item->checked_out) : ?>
					<?php echo JHtml::_('jgrid.checkedout', $i, $item->editor, $item->checked_out_time, 'dirtygirlpages.', $canCheckin); ?>
				<?php endif; ?>
				<?php if ($canEdit) : ?>
					<a href="<?php echo JRoute::_('index.php?option=com_dirtygirlpages&task=dirtygirlpage.edit&id='.(int) $item->id); ?>">
					<?php echo $this->escape($item->campaign_month); ?></a>
				<?php else : ?>
					<?php echo $this->escape($item->campaign_month); ?>
				<?php endif; ?>
				</td>
				<td>
					<?php echo $item->campaign_year; ?>
				</td>
				<td>
					<?php echo $item->dirty_girl_name; ?>
				</td>
				<td>
					<?php echo $item->dirty_girl_bio; ?>
				</td>
				<td>

					<?php
						if (!empty($item->thumbnail_image)):
							$uploadPath = 'components' . DIRECTORY_SEPARATOR . 'com_dirtygirlpages' . DIRECTORY_SEPARATOR . 'uploads' .DIRECTORY_SEPARATOR . $item->thumbnail_image;
							echo '<a href="' . JRoute::_(JUri::base() . $uploadPath, false) . '" target="_blank" title="See the thumbnail_image">' . $item->thumbnail_image . '</a>';
						else:
							echo $item->thumbnail_image;
						endif; ?>				</td>
				<td>

					<?php
						if (!empty($item->image_1)):
							$uploadPath = 'components' . DIRECTORY_SEPARATOR . 'com_dirtygirlpages' . DIRECTORY_SEPARATOR . 'uploads' .DIRECTORY_SEPARATOR . $item->image_1;
							echo '<a href="' . JRoute::_(JUri::base() . $uploadPath, false) . '" target="_blank" title="See the image_1">' . $item->image_1 . '</a>';
						else:
							echo $item->image_1;
						endif; ?>				</td>
				<td>

					<?php
						if (!empty($item->image_2)):
							$uploadPath = 'components' . DIRECTORY_SEPARATOR . 'com_dirtygirlpages' . DIRECTORY_SEPARATOR . 'uploads' .DIRECTORY_SEPARATOR . $item->image_2;
							echo '<a href="' . JRoute::_(JUri::base() . $uploadPath, false) . '" target="_blank" title="See the image_2">' . $item->image_2 . '</a>';
						else:
							echo $item->image_2;
						endif; ?>				</td>
				<td>

					<?php
						if (!empty($item->image_3)):
							$uploadPath = 'components' . DIRECTORY_SEPARATOR . 'com_dirtygirlpages' . DIRECTORY_SEPARATOR . 'uploads' .DIRECTORY_SEPARATOR . $item->image_3;
							echo '<a href="' . JRoute::_(JUri::base() . $uploadPath, false) . '" target="_blank" title="See the image_3">' . $item->image_3 . '</a>';
						else:
							echo $item->image_3;
						endif; ?>				</td>
				<td>

					<?php
						if (!empty($item->image_4)):
							$uploadPath = 'components' . DIRECTORY_SEPARATOR . 'com_dirtygirlpages' . DIRECTORY_SEPARATOR . 'uploads' .DIRECTORY_SEPARATOR . $item->image_4;
							echo '<a href="' . JRoute::_(JUri::base() . $uploadPath, false) . '" target="_blank" title="See the image_4">' . $item->image_4 . '</a>';
						else:
							echo $item->image_4;
						endif; ?>				</td>
				<td>

					<?php
						if (!empty($item->image_5)):
							$uploadPath = 'components' . DIRECTORY_SEPARATOR . 'com_dirtygirlpages' . DIRECTORY_SEPARATOR . 'uploads' .DIRECTORY_SEPARATOR . $item->image_5;
							echo '<a href="' . JRoute::_(JUri::base() . $uploadPath, false) . '" target="_blank" title="See the image_5">' . $item->image_5 . '</a>';
						else:
							echo $item->image_5;
						endif; ?>				</td>
				<td>
					<?php echo $item->created_at; ?>
				</td>


                <?php if (isset($this->items[0]->state)) { ?>
				    <td class="center">
					    <?php echo JHtml::_('jgrid.published', $item->state, $i, 'dirtygirlpages.', $canChange, 'cb'); ?>
				    </td>
                <?php } ?>
                <?php if (isset($this->items[0]->ordering)) { ?>
				    <td class="order">
					    <?php if ($canChange) : ?>
						    <?php if ($saveOrder) :?>
							    <?php if ($listDirn == 'asc') : ?>
								    <span><?php echo $this->pagination->orderUpIcon($i, true, 'dirtygirlpages.orderup', 'JLIB_HTML_MOVE_UP', $ordering); ?></span>
								    <span><?php echo $this->pagination->orderDownIcon($i, $this->pagination->total, true, 'dirtygirlpages.orderdown', 'JLIB_HTML_MOVE_DOWN', $ordering); ?></span>
							    <?php elseif ($listDirn == 'desc') : ?>
								    <span><?php echo $this->pagination->orderUpIcon($i, true, 'dirtygirlpages.orderdown', 'JLIB_HTML_MOVE_UP', $ordering); ?></span>
								    <span><?php echo $this->pagination->orderDownIcon($i, $this->pagination->total, true, 'dirtygirlpages.orderup', 'JLIB_HTML_MOVE_DOWN', $ordering); ?></span>
							    <?php endif; ?>
						    <?php endif; ?>
						    <?php $disabled = $saveOrder ?  '' : 'disabled="disabled"'; ?>
						    <input type="text" name="order[]" size="5" value="<?php echo $item->ordering;?>" <?php echo $disabled ?> class="text-area-order" />
					    <?php else : ?>
						    <?php echo $item->ordering; ?>
					    <?php endif; ?>
				    </td>
                <?php } ?>
                <?php if (isset($this->items[0]->id)) { ?>
				<td class="center">
					<?php echo (int) $item->id; ?>
				</td>
                <?php } ?>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>

	<div>
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>