<?php
/**
 * @version     1.0.0
 * @package     com_dirtygirlpages
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Allan Cutler <allan@211enterprises.com> - http://www.211enterprises.com
 */
// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');
// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_dirtygirlpages/assets/css/dirtygirlpages.css');
?>
<script type="text/javascript">
    function getScript(url,success) {
        var script = document.createElement('script');
        script.src = url;
        var head = document.getElementsByTagName('head')[0],
        done = false;
        // Attach handlers for all browsers
        script.onload = script.onreadystatechange = function() {
            if (!done && (!this.readyState
                || this.readyState == 'loaded'
                || this.readyState == 'complete')) {
                done = true;
                success();
                script.onload = script.onreadystatechange = null;
                head.removeChild(script);
            }
        };
        head.appendChild(script);
    }
    getScript('//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',function() {
        js = jQuery.noConflict();
        js(document).ready(function(){
            

            Joomla.submitbutton = function(task)
            {
                if (task == 'dirtygirlpage.cancel') {
                    Joomla.submitform(task, document.getElementById('dirtygirlpage-form'));
                }
                else{
                    
				js = jQuery.noConflict();
				if(js('#jform_thumbnail_image').val() != ''){
					js('#jform_thumbnail_image_hidden').val(js('#jform_thumbnail_image').val());
				}
				if (js('#jform_thumbnail_image').val() == '' && js('#jform_thumbnail_image_hidden').val() == '') {
					alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
					return;
				}
				js = jQuery.noConflict();
				if(js('#jform_image_1').val() != ''){
					js('#jform_image_1_hidden').val(js('#jform_image_1').val());
				}
				js = jQuery.noConflict();
				if(js('#jform_image_2').val() != ''){
					js('#jform_image_2_hidden').val(js('#jform_image_2').val());
				}
				js = jQuery.noConflict();
				if(js('#jform_image_3').val() != ''){
					js('#jform_image_3_hidden').val(js('#jform_image_3').val());
				}
				js = jQuery.noConflict();
				if(js('#jform_image_4').val() != ''){
					js('#jform_image_4_hidden').val(js('#jform_image_4').val());
				}
				js = jQuery.noConflict();
				if(js('#jform_image_5').val() != ''){
					js('#jform_image_5_hidden').val(js('#jform_image_5').val());
				}
                    if (task != 'dirtygirlpage.cancel' && document.formvalidator.isValid(document.id('dirtygirlpage-form'))) {
                        
                        Joomla.submitform(task, document.getElementById('dirtygirlpage-form'));
                    }
                    else {
                        alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
                    }
                }
            }
        });
    });
</script>

<form action="<?php echo JRoute::_('index.php?option=com_dirtygirlpages&layout=edit&id=' . (int) $this->item->id); ?>" method="post" enctype="multipart/form-data" name="adminForm" id="dirtygirlpage-form" class="form-validate">
    <div class="width-60 fltlft">
        <fieldset class="adminform">
            <legend><?php echo JText::_('COM_DIRTYGIRLPAGES_LEGEND_DIRTYGIRLPAGE'); ?></legend>
            <ul class="adminformlist">

                				<li><?php echo $this->form->getLabel('id'); ?>
				<?php echo $this->form->getInput('id'); ?></li>
				<li><?php echo $this->form->getLabel('campaign_month'); ?>
				<?php echo $this->form->getInput('campaign_month'); ?></li>
				<li><?php echo $this->form->getLabel('campaign_year'); ?>
				<?php echo $this->form->getInput('campaign_year'); ?></li>
				<li><?php echo $this->form->getLabel('dirty_girl_name'); ?>
				<?php echo $this->form->getInput('dirty_girl_name'); ?></li>
				<li><?php echo $this->form->getLabel('dirty_girl_bio'); ?>
				<?php echo $this->form->getInput('dirty_girl_bio'); ?></li>
				<li>
					<?php echo $this->form->getLabel('dirty_type'); ?>
					<select name="jform[dirty_type]" id="jform_dirty_type" aria-invalid="false">
						<option value="0">No</option>
						<option value="1" <?php if($this->item->dirty_type == 1) echo 'selected="selected"'; ?>>Yes</option>
					</select>
				</li>
				
				<li><?php echo $this->form->getLabel('thumbnail_image'); ?>
				<?php echo $this->form->getInput('thumbnail_image'); ?></li>

				<?php if (!empty($this->item->thumbnail_image)) : ?>
						<a href="<?php echo JRoute::_(JUri::base() . 'components' . DIRECTORY_SEPARATOR . 'com_dirtygirlpages' . DIRECTORY_SEPARATOR . 'uploads' .DIRECTORY_SEPARATOR . $this->item->thumbnail_image, false);?>">[View File]</a>
				<?php endif; ?>
				<input type="hidden" name="jform[thumbnail_image]" id="jform_thumbnail_image_hidden" value="<?php echo $this->item->thumbnail_image ?>" />				<li><?php echo $this->form->getLabel('image_1'); ?>
				<?php echo $this->form->getInput('image_1'); ?></li>

				<?php if (!empty($this->item->image_1)) : ?>
						<a href="<?php echo JRoute::_(JUri::base() . 'components' . DIRECTORY_SEPARATOR . 'com_dirtygirlpages' . DIRECTORY_SEPARATOR . 'uploads' .DIRECTORY_SEPARATOR . $this->item->image_1, false);?>">[View File]</a>
				<?php endif; ?>
				<input type="hidden" name="jform[image_1]" id="jform_image_1_hidden" value="<?php echo $this->item->image_1 ?>" />				<li><?php echo $this->form->getLabel('image_2'); ?>
				<?php echo $this->form->getInput('image_2'); ?></li>

				<?php if (!empty($this->item->image_2)) : ?>
						<a href="<?php echo JRoute::_(JUri::base() . 'components' . DIRECTORY_SEPARATOR . 'com_dirtygirlpages' . DIRECTORY_SEPARATOR . 'uploads' .DIRECTORY_SEPARATOR . $this->item->image_2, false);?>">[View File]</a>
				<?php endif; ?>
				<input type="hidden" name="jform[image_2]" id="jform_image_2_hidden" value="<?php echo $this->item->image_2 ?>" />				<li><?php echo $this->form->getLabel('image_3'); ?>
				<?php echo $this->form->getInput('image_3'); ?></li>

				<?php if (!empty($this->item->image_3)) : ?>
						<a href="<?php echo JRoute::_(JUri::base() . 'components' . DIRECTORY_SEPARATOR . 'com_dirtygirlpages' . DIRECTORY_SEPARATOR . 'uploads' .DIRECTORY_SEPARATOR . $this->item->image_3, false);?>">[View File]</a>
				<?php endif; ?>
				<input type="hidden" name="jform[image_3]" id="jform_image_3_hidden" value="<?php echo $this->item->image_3 ?>" />				<li><?php echo $this->form->getLabel('image_4'); ?>
				<?php echo $this->form->getInput('image_4'); ?></li>

				<?php if (!empty($this->item->image_4)) : ?>
						<a href="<?php echo JRoute::_(JUri::base() . 'components' . DIRECTORY_SEPARATOR . 'com_dirtygirlpages' . DIRECTORY_SEPARATOR . 'uploads' .DIRECTORY_SEPARATOR . $this->item->image_4, false);?>">[View File]</a>
				<?php endif; ?>
				<input type="hidden" name="jform[image_4]" id="jform_image_4_hidden" value="<?php echo $this->item->image_4 ?>" />				<li><?php echo $this->form->getLabel('image_5'); ?>
				<?php echo $this->form->getInput('image_5'); ?></li>

				<?php if (!empty($this->item->image_5)) : ?>
						<a href="<?php echo JRoute::_(JUri::base() . 'components' . DIRECTORY_SEPARATOR . 'com_dirtygirlpages' . DIRECTORY_SEPARATOR . 'uploads' .DIRECTORY_SEPARATOR . $this->item->image_5, false);?>">[View File]</a>
				<?php endif; ?>
				<input type="hidden" name="jform[image_5]" id="jform_image_5_hidden" value="<?php echo $this->item->image_5 ?>" />				<li><?php echo $this->form->getLabel('created_at'); ?>
				<?php echo $this->form->getInput('created_at'); ?></li>


            </ul>
        </fieldset>
    </div>

    <div class="clr"></div>

<?php if (JFactory::getUser()->authorise('core.admin','dirtygirlpages')): ?>
	<div class="width-100 fltlft">
		<?php echo JHtml::_('sliders.start', 'permissions-sliders-'.$this->item->id, array('useCookie'=>1)); ?>
		<?php echo JHtml::_('sliders.panel', JText::_('ACL Configuration'), 'access-rules'); ?>
		<fieldset class="panelform">
			<?php echo $this->form->getLabel('rules'); ?>
			<?php echo $this->form->getInput('rules'); ?>
		</fieldset>
		<?php echo JHtml::_('sliders.end'); ?>
	</div>
<?php endif; ?>

    <input type="hidden" name="task" value="" />
    <?php echo JHtml::_('form.token'); ?>
    <div class="clr"></div>

    <style type="text/css">
        /* Temporary fix for drifting editor fields */
        .adminformlist li {
            clear: both;
        }
    </style>
</form>