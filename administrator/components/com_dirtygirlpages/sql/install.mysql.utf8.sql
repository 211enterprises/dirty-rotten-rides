CREATE TABLE IF NOT EXISTS `#__dirtygirlpages_` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`asset_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',

`campaign_month` VARCHAR(255)  NOT NULL ,
`campaign_year` YEAR NOT NULL ,
`dirty_girl_name` VARCHAR(255)  NOT NULL ,
`dirty_girl_bio` TEXT NOT NULL ,
`thumbnail_image` VARCHAR(255)  NOT NULL ,
`image_1` VARCHAR(255)  NOT NULL ,
`image_2` VARCHAR(255)  NOT NULL ,
`image_3` VARCHAR(255)  NOT NULL ,
`image_4` VARCHAR(255)  NOT NULL ,
`image_5` VARCHAR(255)  NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`created_at` DATETIME NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8_general_ci;

