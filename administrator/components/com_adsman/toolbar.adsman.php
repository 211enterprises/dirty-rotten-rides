<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

$page_titles = array(
	"settings" => "General Settings",
	"mailsettings" => "",
	"showadsconfig" => "Import / Export",
	"show_customfields_config" => "Custom Fields Config",
	"integration" => "User Profile Integration",
	"countries" => "",
	"showpaymentconfig" => "Payment Gateways Config",
	"paymentitems" => "Payment Pricing Items",
	"paymentitemsconfig" => "Payment Pricing Items",
	"plugins_administrator" => "Plugins Management",
	"field_administrator" => "Custom Fields & Page Positions",
	"fposition_panel" => "Custom Fields & Page Positions",
	"fposition_fields" => "Custom Fields & Page Positions",
	"fposition_templates" => "Custom Fields & Page Positions",
	"countries" => "Country Manager",
	"themes_administrator" => "Themes Administrator",
	"gallery_administrator" => "Gallery Plugins",
	"gateway_administrator" => "Payment Gateway Plugins"

);

require_once( JApplicationHelper::getPath( 'toolbar_html' ) );

$TOOLBAR_adsman = new TOOLBAR_adsman();

switch ($task)
{

	case 'settings'  :
        $TOOLBAR_adsman->_SETTINGS();
        //$TOOLBAR_adsman->_MENUHelper();
		break;
		
	case 'showadsconfig'  :
	case 'show_customfields_config' :	
	case 'integration'  :
	case 'countries'  :
	case 'plugins_administrator'  :
	case 'field_administrator'  :
	case 'fposition_panel' :
	case 'fposition_fields' :
	case 'fposition_templates' :
	case 'countries'  :
//	case 'themes_administrator'  :
//	case 'gallery_administrator':
//	case 'gateway_administrator':
        $TOOLBAR_adsman->_NONE($page_titles[$task]);
        $TOOLBAR_adsman->_MENUHelper();
		break;

    case 'showpaymentconfig' :
    case 'paymentitems' :
    case 'paymentitemsconfig'  :
        $TOOLBAR_adsman->_PAYMENTITEMS($page_titles[$task],'paymentitems.png');
    	break;

    case 'gateway_administrator':
        $TOOLBAR_adsman->_PAYMENTITEMS($page_titles[$task],'gateway.png');
        break;

    case 'themes_administrator'  :
        $TOOLBAR_adsman->_PAYMENTITEMS($page_titles[$task],'themes.png');
        break;

    case 'gallery_administrator':

        $TOOLBAR_adsman->_PAYMENTITEMS($page_titles[$task],'gallery.png');
        break;

	case 'mailsettings'  :
        $TOOLBAR_adsman->_SETTINGS("savemailsettings");
		//TOOLBAR_adsman::_SETTINGS("savemailsettings");
		break;
		
	case 'showadmimportform'  :
        $TOOLBAR_adsman->_IMPORT();
		$TOOLBAR_adsman->_MENUHelper();
		break;
		
	case 'listadds'  :
        $TOOLBAR_adsman->_LIST(false);
        $TOOLBAR_adsman->_MENUHelper();
		break;
	
	case 'users' :
        $TOOLBAR_adsman->_USERS(false);
        $TOOLBAR_adsman->_MENUHelper();
		break;
	
	case 'detailUser' :
        $TOOLBAR_adsman->_SIMPLE(false);
		//TOOLBAR_adsman::_SIMPLE(false);
		break;
	
	case 'edit' :
	case 'editA':
    case 'newAd':
        $TOOLBAR_adsman->_EDIT();
		//TOOLBAR_adsman::_EDIT();
		break;

	case 'cb_integration':
        $TOOLBAR_adsman->_SETTINGS("savecbintegration");
		//TOOLBAR_adsman::_SETTINGS("savecbintegration");
		break;

	case 'payments':
        $TOOLBAR_adsman->_PAYMENTS(true);
        $TOOLBAR_adsman->_MENUHelper();
		break;
		
	case 'new_payment':
        $TOOLBAR_adsman->_NEWPAYMENT();
		//TOOLBAR_adsman::_NEWPAYMENT();
		break;
	
	case 'categories'  :
        $TOOLBAR_adsman->_CATEGORIES();
		//TOOLBAR_adsman::_CATEGORIES();
		break;

	case 'reported_adds'  :
        $TOOLBAR_adsman->_REPORTS();
        $TOOLBAR_adsman->_MENUHelper();
		break;
		
	case 'editCat'  :
	case 'editcat'  :
        $TOOLBAR_adsman->_CATEGORIES_EDIT();
		//TOOLBAR_adsman::_CATEGORIES_EDIT();
		break;

    case 'settingsmanager'  :
        $TOOLBAR_adsman->_NONE("ADS_SETTINGS_CENTRAL");
        $TOOLBAR_adsman->_MENUHelper();
		break;

    default:
    case 'dashboard'  :
        $TOOLBAR_adsman->_NONE("ADS_DASHBOARD");
        break;
	
	case 'aboutComponent'  :
        $TOOLBAR_adsman->_SIMPLE("ADS_ABOUT", "info.png");
        $TOOLBAR_adsman->_MENUHelper();
		break;
		
	case 'statistics'  :
        $TOOLBAR_adsman->_SIMPLE("ADS_STATISTICS","generic.png");
        $TOOLBAR_adsman->_MENUHelper();
		break;
		
	case 'write_admin_message'  :
        $TOOLBAR_adsman->_MESSAGE();
		//TOOLBAR_adsman::_MESSAGE();
		break;
		
}