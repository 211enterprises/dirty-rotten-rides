<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

// no direct access
defined('_JEXEC') or die( 'Restricted access' );

class JAdsUsers extends FactoryFieldsTbl {
	var $userid				= null;
	var $name				= null;
	var $surname			= null;
	var $address			= null;
	var $city				= null;
	var $state				= null;
	var $country			= null;
	var $phone				= null;
	var $modified			= null;
	var $email		    	= null;
	var $paypalemail      	= null;
	var $YM        			= null;
	var $googleX	        = null;
	var $googleY	        = null;
	var $status		        = null;
	
	function JAdsUsers( &$db ) {
		$this->__construct( '#__ads_users', 'userid', $db );
		
		$db->setQuery("select * from #__ads_fields where own_table='#__ads_users' order by ordering");
		$rows=$db->LoadObjectList();
		
		for($i=0; $i<count($rows); $i++)
		{
			$fieldname 			= $rows[$i]->db_name;
			$this->$fieldname 	= null;
		}
		
	}
	
	function getUserDetails($uid=null)
	{
		if ($uid) $this->userid=$uid;
		
		$u = JTheFactoryUserProfile::getInstance();
		$this->userid = $uid;
		
		if(defined("ads_opt_profile_mode") && ads_opt_profile_mode!= "")
			$profile_mode = ads_opt_profile_mode;
		else
			$profile_mode = '';
		$u->getUserProfile($this,$profile_mode);
		
	    return $this;
	}
	
	/*function loadAndMembership($user_id)
	{
	  $this->load($user_id);

	  if (!$this->user_id)
	  {
	    $this->user_id = JFactory::getUser()->id;
	  }

	  $membership =& JTable::getInstance('membershipsold', 'Table');
	  $membership->load($this->membership_sold_id);

	  $this->membership_sold = $membership;
	}*/
	
	function findOneByUserId($user_id)
	  {
	    $query = ' SELECT u.* '
	           . ' FROM #__users u'
	           . ' WHERE u.userid = ' . $user_id;
	    $this->_db->setQuery($query);
	
	    $this->bind($this->_db->loadObject());
	  }
	
	function getCountry() {
		$active_only = 1;
		

		$active="";
		if($active_only){
			$active = " AND active=1";
		}
		$this->_db->setQuery("SELECT * FROM #__ads_country WHERE id = '".$this->country."' $active ");
		return $this->_db->loadObject();
	}
	
}

?>