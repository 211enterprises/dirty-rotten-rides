<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die( 'Restricted access' );

class TableUrls extends JTable{
	
    var $id 		= null;
    var $user_id 	= null;
    var $ad_id		= null;
    var $video_link = null; 
    var $video_title = null;
    var $video_description = null;
    var $video_thumbnail = null;
    var $video_sitename = null;
    var $video_sourceThumb = null;
    var $date_added = null;
	    
	function TableUrls( &$db ) {
		
		$this->__construct( '#__ads_urls', 'id', $db );

	}
    
    
}
?>