<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die( 'Restricted access' );

class TablePriceimage extends JTable{
	
    var $id = null;
    var $currency = null; 
    var $credits_img_0 = null;
    var $credits_img_1 = null;
    var $credits_img_2 = null;
    var $credits_img_3 = null;
    var $credits_img_4 = null;
	var $credits_img_5 = null;
	var $credits_img_6 = null;
	var $credits_img_7 = null;
	    
	function TablePriceimage( &$db ) {
		
		$this->__construct( '#__ads_images_pricing', 'id', $db );

	}
   
}

?>