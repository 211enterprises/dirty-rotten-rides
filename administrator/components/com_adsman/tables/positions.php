 <?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die( 'Restricted access' );

class TablePositions extends JTable {
       var $id                                 = null;
       var $title                              = null;
       var $name                               = null;
       var $description                = null;
       var $html                               = null;
       var $tpl                                = null;
       var $verified                   = null;


       function TablePositions ( &$db )
       {
               parent::__construct('#__ads_positions', 'id', $db);
       }

}