<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die( 'Restricted access' );

class JAdsAddsman extends FactoryFieldsTbl {
	var $id					= null;
	var $title				= null;
    var $category	        = null;
	var $short_description	= null;
	var $description		= null;
	var $start_date			= null;
	var $end_date			= null;
	var $addtype			= null;
	var $userid				= null;
	var $askprice			= null;
	var $currency			= null;
	var $picture			= null;
	var $hits				= null;
	var $status				= null;
	var $close_by_admin		= null;
	var $closed				= null;
	var $closed_date		= null;
	var $newmessages		= null;
	var $featured			= null;
	var $MapX				= null;
	var $MapY				= null;
	var $atachment			= null;
	var $extend				= null;
	var $token_id			= null;
	var $guest_username		= null;
	var $guest_email		= null;
	
	function JAdsAddsman(&$db) {
	
		$this->__construct( '#__ads', 'id', $db );
	}
	
	function delete($oid = null){
		if($oid)
	        $this->load($oid);

	    $this->_db->setQuery("SELECT * FROM #__ads_pictures WHERE id_ad='$this->id'");
	    $images	= $this->_db->loadObjectList();
	    
	    if(!defined("ADDSMAN_IMAGE_PATH"))
			define('ADDSMAN_IMAGE_PATH',JPATH_COMPONENT_SITE.DS."images".DS );
	    
	    if (count($images)){
	        foreach ($images as $image){
	            if (file_exists(ADDSMAN_IMAGE_PATH.$image->picture)){
        	        @unlink(ADDSMAN_IMAGE_PATH.$image->picture);
        	        @unlink(ADDSMAN_IMAGE_PATH."middle_".$image->picture);
        	        @unlink(ADDSMAN_IMAGE_PATH."resize_".$image->picture);
	            }
	        }
	    }

	    if (file_exists(ADDSMAN_IMAGE_PATH.$this->picture)){
	    	@unlink(ADDSMAN_IMAGE_PATH.$this->picture);
	    	@unlink(ADDSMAN_IMAGE_PATH."middle_".$this->picture);
			@unlink(ADDSMAN_IMAGE_PATH."resize_".$this->picture);

	    }
        if (file_exists(ADDSMAN_IMAGE_PATH."attach_{$oid}.fil"))
			@unlink(ADDSMAN_IMAGE_PATH."attach_{$oid}.fil");

		$this->_db->setQuery("DELETE FROM #__ads_tags WHERE parent_id='$this->id'"); //remove tags
		$this->_db->query();
		$this->_db->setQuery("DELETE FROM #__ads_pictures WHERE id_ad='$this->id'"); //remove pictures
		$this->_db->query();
		$this->_db->setQuery("DELETE FROM #__ads_report WHERE adid='$this->id'"); //remove reports
		$this->_db->query();
		$this->_db->setQuery("DELETE FROM #__ads_messages WHERE adid='$this->id'"); //remove messages
		$this->_db->query();
		$this->_db->setQuery("DELETE FROM #__ads WHERE id='$this->id'"); //remove the add
		$this->_db->query();
	}
	
	function loadData($id){
		
		$my = & JFactory::getUser();
		$SelectCols = array();
		$JoinList = array();
		
		// Currency table Join
		$SelectCols[] 	= " cur_table.id AS cur_id , cur_table.name AS currency_name ";
		$JoinList[] = " LEFT JOIN `#__ads_currency`  AS cur_table ON  currency=cur_table.id ";
		
		// Category table join
		$SelectCols[] 	= "  cat_table.id AS cati,  cat_table.catname AS catname, cat_table.parent AS catparent_id  ";
		$JoinList[] = " LEFT JOIN `#__ads_categories`  AS cat_table ON  category=cat_table.id ";
		
		$SelectCols[] 	= "   `catsef`.`categories` as catslug ";
		$JoinList[] = " LEFT JOIN `#__ads_categories_sef` AS `catsef` ON  `cat_table`.`id`=`catsef`.`catid` ";
		
		// Tags join
		$SelectCols[] 	= "  GROUP_CONCAT(DISTINCT `t`.`tagname`) AS tags ";
		$JoinList[] = " LEFT JOIN `#__ads_tags`  AS t ON ptable.`id`=`t`.`parent_id` ";
		
		// User join
		$JoinList[] = " LEFT JOIN `#__ads_users`  AS usr ON ptable.`userid`=`usr`.`userid` ";
		
		// Pictures table join
		$SelectCols[] 	= "  IF ( `pic_table`.`id` >0, 1, 0 )  AS more_pictures ";
		$JoinList[] = " LEFT JOIN `#__ads_pictures`  AS pic_table ON `ptable`.`id`=`pic_table`.`id_ad` ";
		
		$my = &JFactory::getUser();
		
		if( $my->id ){
			// Is Favourite join
			$SelectCols[] 	= "  IF ( `fav_table`.`id` >0, 1, 0 )  AS favorite ";
			$JoinList[] = " LEFT JOIN `#__ads_favorites`  AS fav_table ON `fav_table`.`adid`=`ptable`.`id` AND `fav_table`.`userid` = '".$my->id."' ";
		}
		
		$this->catslug = null; $this->catparent_id = null;
		$this->catname = null; $this->currency_name = null; $this->tags = null; $this->more_pictures = null; 
		$this->favorite = null;
		
		parent::loadData( $id, $SelectCols, $JoinList ,array("#__ads" => "ptable","#__ads_users"=>"usr"));
		
		return $this;
	}

    function getCategory() {
    	$database 	=& JFactory::getDBO();	
		$database->setQuery("SELECT * FROM #__ads_categories WHERE id = '".$this->category."'");
		return $database->loadObject();
	}

	function getCurrency() {
		$database 	=& JFactory::getDBO();	
		$database->setQuery("SELECT * FROM #__ads_currency WHERE id = '".$this->currency."'");
		return $database->loadObject();
	}

	function SetCategory($category_name) {
		$database 	=& JFactory::getDBO();	
        $database->setQuery("select id from #__ads_categories where catname='".$category_name."'");
        $this->category=$database->LoadResult();
        return $this->category;
    }


    function SetCurrency($currency_name) {
    	$database 	=& JFactory::getDBO();	
        $database->setQuery("select id from #__ads_currency where name='".$currency_name."'");
        $this->currency=$database->LoadResult();
        return $this->currency;
    }

	function isMyAdd() {
        $my = &JFactory::getUser();
        if ($my->id && $my->id==$this->userid) return true;
        else return false;
    }

    function haveAccess() {

        $my = &JFactory::getUser();
    	if($this->close_by_admin || $this->close_by_admin || ($this->status==0 && $my->id != $this->userid ) )
    		return false;
    	return true;
    }

    function isFavorite() {
        $my = &JFactory::getUser();
        if (!$my->id || $my->id==$this->userid)
			return null;

		$this->_db->setQuery("SELECT * FROM #__ads_favorites WHERE adid = '".$this->id."' AND userid = '".$my->id."'");
		$this->_db->query();
		if($this->_db->getNumRows()=="1")
			return 1;
		else
			return 0;
	}

    function getMessages($userid="") {

    	$user_sql = "";

		if ($userid!="") {
    	    if ($this->addtype == 0)
    			$user_sql = " AND ( (from_user = '".$userid."' OR to_user = '".$userid."')  AND ( from_user != 0 OR to_user != 0 ) ) ";
            else
                $user_sql = " AND ( (from_user = '".$userid."' OR to_user = '".$userid."' )  OR ( from_user =0  OR to_user =0 ) )";
        }

        if ($userid == "" && $this->addtype == 1) {
            $user_sql = " AND ( from_user = 0 OR to_user = 0 )";
        }

		$sql = "SELECT m.*, u.username AS fromusername , p.username AS tousername FROM #__ads_messages AS m".
		" LEFT JOIN #__users AS u ON m.from_user = u.id ".
		" LEFT JOIN #__users AS p ON m.to_user = p.id ".
		" WHERE adid = '".$this->id."' ".$user_sql. " ORDER BY m.datesend DESC";

		$this->_db->setQuery($sql);
		return $this->_db->loadObjectList();
	}

	function sendNewMessage($id,$id_msg,$message){

		$my = & JFactory::getUser();

		$m = new JAdsMessages($this->_db);

		if (!$id_msg) {
        	$m->adid		=$this->id;
        	$m->id_message	=0;
        	$m->comment		=$message;
        	$m->datesend	=date('Y-m-d H:i:s',time());
			$m->from_user	=$my->id;
			$m->to_user		=$this->userid;

			$usr=JTable::getInstance("user");
			$usr->load($this->userid);
		
			$this->SendMails(array($usr),"new_message",$message);

        } else {
            $replytom 		= new JAdsMessages($this->_db);
            $replytom->load($id_msg);
            
            $m->adid 		= $this->id;
            $m->id_message 	= $id_msg;
            $m->from_user 	= $my->id;
            $m->to_user 	= $replytom->from_user;
            $m->datesend 	= date('Y-m-d H:i:s',time());
            $m->comment 	= $message;
            
            $replytom->wasread = 1;
            $replytom->store();

			$usr			=JTable::getInstance("user");
            $usr->load($replytom->from_user);
           
            $this->SendMails(array($usr),"new_message",$message);
        }
        
        $m->store();
    }

	function isAllowedImage($ext) {
		if(strpos(strtolower(add_opt_allowed_ext) , strtolower($ext))!=false)
			return true;
		else return false;
	}

	function getImages() {
	
	$database 	= JFactory::getDbo();
	if ($this->id != null) {
		$query = "SELECT * FROM #__ads_pictures WHERE id_ad = '".$this->id."' AND `published` = 1 ";
		
		$database->setQuery( $query );
		$this->images	= $database->loadObjectList();
		
		if ($this->images != null)
			return $this->images;
			
	} 

	return null;
		
	}

    function getCountImages() {

        $database 	= JFactory::getDbo();
        if ($this->id != null) {
            $query = "SELECT count(*) FROM #__ads_pictures WHERE id_ad = '".$this->id."' AND `published` = 1 ";

            $database->setQuery( $query );
            $images_count	= $database->loadResult();
            if ($images_count != null)
                return $images_count;
        }
        return 0;
	}

    function getCountDeletedImages() {
        $database 	= JFactory::getDbo();
        if ($this->id != null) {
            $query = "SELECT count(*) FROM #__ads_pictures WHERE id_ad = '".$this->id."' AND `published` = 0 ";

            $database->setQuery( $query );
            $images_count	= $database->loadResult();
            if ($images_count != null)
                return $images_count;
        }
        return 0;
    }

	function SendMails($userlist,$mailtype,$param = null) {
		
		$app        = JFactory::getApplication();
		$my         = JFactory::getUser();
		$database 	= JFactory::getDBO();
		
		$logged_user = "";
		if ( $my->id ) {
			$logged_user = $my->name;
		}

		$mail_from = htmlspecialchars_decode($app->getCfg('mailfrom'));
		$site_name = htmlspecialchars_decode($app->getCfg('sitename'));
		
// If mailfrom is not defined A super administrator is notified
		if ( ! $mail_from  || ! $site_name ) {
			
			//get all super administrator
			$query = 'SELECT name, email, sendEmail' .
					' FROM #__users' .
					' WHERE LOWER( usertype ) = "super administrator"';
			$database->setQuery( $query );
			$rows = $database->loadObjectList();

			$site_name = $rows[0]->name;
			$mail_from = $rows[0]->email;
			
		}
		
		// category Fix
		if(!isset($this->catname) && $this->category){
			$co = $this->getCategory();
			$this->catname = $co->catname;
			
		}

		set_time_limit(0);
		ignore_user_abort();
		
		$mail_body=new JAdsMails($this->_db);
		
		if (!$mail_body->load($mailtype) ) return;
		
		if (!$mail_body->enabled) return;
		
		if (count($userlist)<=0) return;
        
		foreach($userlist as $can){

			if(isset($can->name)) 
				$name = $can->name;
			else  
				$name = "";
			if(isset($can->surname)) 
				$surname = $can->surname;
			else  
				$surname = "";
			
			/**
			if ($this->addtype==ADDSMAN_TYPE_PRIVATE) {
			    $name=adsman_add_type_private_ad;
			    $surname=adsman_add_type_private_ad;

			}
			*/
			
			//get sender id
			if ( $my->id ){
				$from_id = $my->id;
			} else {
				$from_id = 0;
			}

			if ($mailtype = 'new_message') {
				
				$message_text = $param;
			}	

            $uri = JURI::getInstance();

            $base = $uri->base();

            $replace = '#components/com_adsman/#';
            $basepath = preg_replace($replace,'',$base);
            $basepath = preg_replace('#administrator/#','',$basepath);

			$taskItemID = AdsmanHelperRoute::getMenuItemByTaskName("listadds");
			
			$mess =str_replace("%NAME%",			$name,			$mail_body->content);
			$mess =str_replace("%SURNAME%",			$surname,		$mess);
			$mess =str_replace("%ADDTITLE%",		$this->title,	$mess);

            $mess =str_replace("%ADD_EDITLINK%", 	$basepath."index.php?option=com_adsman&task=edit&id=$this->id&Itemid=$taskItemID&tk=$this->token_id",$mess);
   			$mess =str_replace("%ADDLINK%", 		$basepath."index.php?option=com_adsman&task=details&id=$this->id&Itemid=$taskItemID",$mess);

			$mess =str_replace("%CATEGORY%", 		@$this->catname,$mess);
			$mess =str_replace("%SHORTDESC%", 		$this->short_description,$mess);
			$mess =str_replace("%START_DATE%", 		$this->start_date,		$mess);
			$mess =str_replace("%END_DATE%",		$this->end_date,		$mess);
			$mess =str_replace("%LOGGED_USERNAME%", $logged_user, 			$mess);
			$mess =str_replace("%MESSAGE_BODY%", 	$message_text, 			$mess);
			
			$nlmess = str_replace('\\\r\\\n', "<br>", $mess);
			$mess = str_replace('\\', "", $nlmess);
			
			// Your custom Fields replacements

			$subj =str_replace("%NAME%",			$name, 			$mail_body->subject);
			$subj =str_replace("%SURNAME%",			$surname,		$subj);
			$subj =str_replace("%ADDTITLE%",		$this->title,	$subj);

            $subj =str_replace("%ADD_EDITLINK%", 	$basepath."index.php?option=com_adsman&task=edit&id=$this->id&Itemid=$taskItemID&token=$this->token_id",$subj);
           	$subj =str_replace("%ADDLINK%",			$basepath."index.php?option=com_adsman&task=details&id=$this->id&Itemid=$taskItemID",$subj);
            $subj =str_replace("%CATEGORY%",		@$this->catname,$subj);
			$subj =str_replace("%SHORTDESC%",		$this->short_description,$subj);
			$subj =str_replace("%LOGGED_USERNAME%", $logged_user, 	$subj);
			
			if($can->email){
			    JUTility::sendMail($mail_from, $site_name,$can->email, $subj, $mess, true);
			}
			
		}

	}

}

class JAdsCategories extends FactoryFieldsTbl {
	var $id                = null;
	var $catname           = null;
	var $description	   = null;
	var $parent            = null;
	var $hash              = null;
	var $ordering          = null;
	var $status	 	       = null;

	function JAdsCategories( &$db ){
		$this->__construct( '#__ads_categories', 'id', $db );
	}
	
	/**
	 *
	 * Deprecated
	 * 
	 * Allready in Thefactory
	 *  
	 **/
	function string_cat_path($id=null,$task="listadds"){
		return AdsmanHelperRoute::getSEFCatString($id);
	}
	
}

class JAdsCountry extends JTable {
	var $id                = null;
	var $name   	       = null;
	var $symbol            = null;
	var $active            = null;

	function JAdsCountry( &$db ){
		$this->__construct('#__ads_country','id',$db);

	}
}


class JAdsPicture extends JTable {
	var $id 				= null;
	var $id_ad				= null;
	var $userid				= null;
	var $picture			= null;
	var $modified			= null;
    var $published			= null;

	function JAdsPicture ( &$db ) {
		$this->__construct( '#__ads_pictures', 'id', $db );
	}
}

class JAdsMails extends JTable {
	var $id 				= null;
	var $mail_type			= null;
	var $content			= null;
	var $subject			= null;
	var $enabled            = null;

	function JAdsMails( &$db ) {
		$this->__construct( '#__ads_mails', 'mail_type', $db );
	}
	
	function store( $updateNulls=false ) {
		$k = $this->_tbl_key;

		if ($this->$k) {
			$ret = $this->_db->updateObject($this->_tbl, $this, $this->_tbl_key, $updateNulls);
		} else {
			$ret = $this->_db->insertObject($this->_tbl, $this, $this->_tbl_key);
		}

		if (!$ret) {
			$this->_error = strtolower(get_class($this))."::store failed <br />" . $this->_db->getErrorMsg();
			return false;
		} else {
			return true;
		}
	}

}

class JAdsMessages extends JTable {
	var $id					= null;
	var $adid				= null;
	var $from_user			= null;
	var $to_user			= null;
	var $id_message			= null;
	var $comment			= null;
	var $datesend			= null;
	var $wasread            = null;

/**
* @param database A database connector object
*/
	function JAdsMessages( &$db ) {
		$this->__construct( '#__ads_messages', 'id', $db );
	}
}

