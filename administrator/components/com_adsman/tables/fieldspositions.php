 <?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die( 'Restricted access' );

class TableFieldsPositions extends JTable {
       var $id                               = null;
       var $fid                              = null;
       var $position                         = null;

       function TableFieldsPositions ( &$db )
       {
               parent::__construct('#__ads_fields_positions', 'id', $db);
       }

}