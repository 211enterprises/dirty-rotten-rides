 <?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die( 'Restricted access' );

class TablePaySystems extends JTable {
       var $id                  = null;
       var $paysystem           = null;
       var $classname           = null;
       var $enabled				= null;
	   var $isdefault			= null;
	   var $params				= null;
	   var $ordering			= null;

       function TablePaySystems ( &$db )
       {
               parent::__construct('#__ads_paysystems', 'id', $db);
       }
       
}