<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die( 'Restricted access' );

class TablePackages extends JTable{
	
    var $id = null;
    var $name = null;
    var $price = null;
    var $currency = null; 
    var $credits = null;
    var $status = null;
    var $ordering = null;
	    
	function TablePackages( &$db ) {
		
		$this->__construct( '#__ads_packages', 'id', $db );

	}
    
    
}
?>