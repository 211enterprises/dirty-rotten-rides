<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('joomla.application.component.controller');
jimport('behaviour.mootools');

/**
 * Ads Component Controller
 *
 * @package	Adsman
 * @since 1.6.0
 */
class JAdsAdminController extends JController
{
	
	function publish(){
		AdsUtilities::changeContent(1);
	}

	function unpublish(){
		AdsUtilities::changeContent(0);
	}

	function publish_cat(){
		AdsUtilities::changeCat(1);
	}

	function unpublish_cat(){
		AdsUtilities::changeCat(0);
	}

	function block(){
		AdsUtilities::block_add(1);
	}

	function unblock(){
		AdsUtilities::block_add(0);
	}

    function processing(){
        AdsUtilities::processing_report(1);
    }

    function solved(){
        AdsUtilities::solved_report(1);
    }

	function listadds(){
		$app = JFactory::getApplication();
		$db	 = JFactory::getDBO();
		$where = array();

		$context			= 'com_adsman.jadsadminview.listadds';
		$filter_order		= $app->getUserStateFromRequest( $context.'filter_order',		'filter_order',		'',	'cmd' );
		$filter_order_Dir	= $app->getUserStateFromRequest( $context.'filter_order_Dir',	'filter_order_Dir',	'',	'word' );
		$filter_authorid	= $app->getUserStateFromRequest( $context.'filter_authorid',	'filter_authorid',	0,	'int' );
		$search				= $app->getUserStateFromRequest( $context.'search',			'search',			'',	'string' );
		$search				= JString::strtolower($search);

		$limit		= $app->getUserStateFromRequest('global.list.limit', 'limit', $app->getCfg('list_limit'), 'int');
		$limitstart	= $app->getUserStateFromRequest($context.'limitstart', 'limitstart', 0, 'int');

		// In case limit has been changed, adjust limitstart accordingly
		$limitstart = ( $limit != 0 ? (floor($limitstart / $limit) * $limit) : 0 );

		$status 	= $app->getUserStateFromRequest($context.'status', 	 'status', 	 2, 'int');
		$ads_type 	= $app->getUserStateFromRequest($context.'ads_type', 'ads_type', 2, 'int');
        $ads_published = $app->getUserStateFromRequest($context.'ads_published', 'ads_published', 2, 'int');

		if (!$filter_order) {
			$filter_order = 'a.title';
		}
		$order = ' ORDER BY '. $filter_order .' '. $filter_order_Dir .'';

		if($search)
			$where[] = " a.title LIKE '%$search%' ";
			
		if( $status == 0 )
			$where[] = " (a.closed = $status AND a.close_by_admin = $status ) ";	
		
		if ( $status == 1 )	
			$where[] = " (a.closed = $status OR a.close_by_admin = $status ) ";	

		if($ads_type == 0 || $ads_type == 1)
			$where[] = " a.addtype = $ads_type ";

        if($ads_published == 0 || $ads_published == 1)
        	$where[] = " a.status = $ads_published ";

		// Build the where clause of the content record query
		$where = (count($where) ? ' WHERE '.implode(' AND ', $where) : '');

		// Get the total number of records
		$query = 'SELECT COUNT(*)' .
				' FROM #__ads AS a' .
				' LEFT JOIN #__ads_categories AS cc ON cc.id = a.category' .
				$where;
				
		$db->setQuery($query);
		$total = $db->loadResult();

		// Create the pagination object
		jimport('joomla.html.pagination');
		$pagination = new JPagination($total, $limitstart, $limit);

		// Get the articles
		$query = 'SELECT a.*, cc.catname AS name, u.name AS editor ' .
				' FROM #__ads AS a' .
				' LEFT JOIN #__ads_categories AS cc ON cc.id = a.category' .
				' LEFT JOIN #__users AS u ON u.id = a.userid' .
				$where.$order;
		$db->setQuery($query, $pagination->limitstart, $pagination->limit);
		$rows = $db->loadObjectList();

		// table ordering
		$lists['order_Dir']	= $filter_order_Dir;
		$lists['order']		= $filter_order;

		// search filter
		$lists['search'] = $search;
		
		// status closed/open
		$lists['status'] = $status;
		
		// ads_type public/private
		$lists['ads_type'] = $ads_type;

        // ads_type published/unpublished
        $lists['ads_published'] = $ads_published;
		
		JAdsAdminView::listadds($rows, $pagination, $lists);
	}

    function showLatestads(){
        $app = JFactory::getApplication();
        $db	 = JFactory::getDBO();
        $where = array();
        $latestAds = array();

        $context			= 'com_adsman.jadsadminview.listLatestads';
        $filter_order		= $app->getUserStateFromRequest( $context.'filter_order',		'filter_order',		'',	'cmd' );
        $filter_order_Dir	= $app->getUserStateFromRequest( $context.'filter_order_Dir',	'filter_order_Dir',	'',	'word' );

        if (!$filter_order) {
            $filter_order = 'a.title';
        }

        // Get the total number of records
        $query = 'SELECT COUNT(*)' .
                ' FROM #__ads AS a' .
                ' LEFT JOIN #__ads_categories AS cc ON cc.id = a.category'
                . ' LIMIT 0,10';

        $db->setQuery($query);
        $total = $db->loadResult();

        // Create the pagination object
        //jimport('joomla.html.pagination');
        //$paginationAds = new JPagination($total, $limitstart, $limit);

        // Get the articles
        $query = 'SELECT a.*, cc.catname AS name, u.name AS editor ' .
                ' FROM #__ads AS a' .
                ' LEFT JOIN #__ads_categories AS cc ON cc.id = a.category' .
                ' LEFT JOIN #__users AS u ON u.id = a.userid' .
                ' ORDER BY a.id DESC '
                . ' LIMIT 0,9';
        $db->setQuery($query);
        $rowsAds = $db->loadObjectList();

        // table ordering
        $listsAds['order_Dir']	= $filter_order_Dir;
        $listsAds['order']		= $filter_order;

        $latestAds['rows'] = $rowsAds;
        $latestAds['lists'] = $listsAds;

        return $latestAds;
        //JAdsAdminView::listLatestads($rowsAds, $paginationAds, $listsAds);
    }

    function showLatestpayments(){
        $app	= JFactory::getApplication();

        $database	= JFactory::getDBO();
        $query = "SELECT COUNT(*)"
           . "\n FROM ".PAYTABLE_PAYLOG." p
           LEFT JOIN #__users u ON p.userid=u.id LIMIT 0,5 ";

        $database->setQuery( $query );
        $total = $database->loadResult();

      	$database->setQuery("SELECT p.*,u.username, a.title as object_item
               FROM ".PAYTABLE_PAYLOG." AS p
               LEFT JOIN #__users u ON p.userid=u.id
               LEFT JOIN ".PAYTABLE_PAYITEMS_TABLE." as a ON p.object_id = a.id
               ORDER BY date DESC LIMIT 0,9 " );

            $pay_list=$database->loadObjectList();

            $filter[] = JHTML::_('select.option', '', JText::_('ADS_ALL'), 'text', 'value' );
            $filter[] = JHTML::_('select.option', 'ok', JText::_('ADS_PAYMENT_OK'), 'text', 'value' );
            $filter[] = JHTML::_('select.option', 'manual_check', JText::_('ADS_MANUAL_CHECK'), 'text', 'value' );
            $filter[] = JHTML::_('select.option', 'cancelled', JText::_('ADS_CANCELLED'), 'text', 'value' );
            $filter[] = JHTML::_('select.option', 'refunded', JText::_('ADS_REFUNDED'), 'text', 'value' );
            $filter[] = JHTML::_('select.option', 'error',JText::_('ADS_ERROR'), 'text', 'value' );

            $rows = array();

            $i = 0;
            if (count($pay_list))
                foreach ($pay_list as $row)
                {
                    $rows[$i][]	= $i+1;
                    //$rows[$i][]	= JHTML::_('grid.id', $i+1, $row->id);
                    $rows[$i][]	= $row->username;
                    $rows[$i][]	= $row->date;

                    if ($row->itemname!="price_contact")
                        $rows[$i][]	= $row->object_item;
                    else
                        $rows[$i][]	= "";

                    $pricing_title = $row->itemname;
                    if ($row->itemname == "packages") {

                        if ( $row->var_name1=="package_id" ) {

                            $database->setQuery("SELECT name FROM ".PAYTABLE_PACKAGES." WHERE id = {$row->var_value1}");
                            $pname = $database->loadResult();
                            $pricing_title .= " ( {$pname} )";
                        }
                    }
                    $rows[$i][]	= $pricing_title;

                    $rows[$i][]	= number_format($row->amount,2,'.','');;
                    $rows[$i][]	= $row->currency;
                    $rows[$i][]	= $row->refnumber;
                    $rows[$i][]	= $row->invoice;
                    $rows[$i][]	= $row->ipn_ip;
                    $rows[$i][]	= $row->status;

                    $i++;
                }

        		$filter_controlls	=	"<button onclick=\"this.form.submit();\">".JText::_( 'ADS_GO' )."</button>";
        		$filter_controlls	.=	"<button onclick=\"document.getElementById('search').value='';this.form.submit();\">".JText::_( 'ADS_RESET' )."</button>";

        return $rows;

        //JTheFactoryAdminHelper::showAdminTableList($header_row,$rows,$pageNav,null, $lists['$filter_keyword'].'&nbsp;&nbsp;'.$lists['filter_state'].'&nbsp'.$filter_controlls);

    }

	function remove(){
		$db		= JFactory::getDBO();
		$cid	= JRequest::getVar( 'cid', array(0), '', 'array' );
		require_once (JPATH_ADMINISTRATOR.DS.'components'.DS.'com_adsman'.DS.'tables'.DS.'addsman.php');
		require_once (JPATH_BASE.DS."..".DS.'components'.DS.'com_adsman'.DS.'helpers'.DS.'helper.php');
		$add=new JAdsAddsman($db);
		foreach ($cid as $id)
		{
		    $add->load($id);
    	    $add->delete($id);
		}
		$this->setRedirect( "index.php?option=com_adsman&task=listadds",JText::_("ADD_DELETED") );
	}

    function newAd(){
        $db		= JFactory::getDBO();

    	require_once (JPATH_ADMINISTRATOR.DS.'components'.DS.'com_adsman'.DS.'tables'.DS.'addsman.php');
    	require_once (JPATH_BASE.DS."..".DS.'components'.DS.'com_adsman'.DS.'helpers'.DS.'helper.php');
    	require_once (JPATH_ADMINISTRATOR.DS.'components'.DS.'com_adsman'.DS.'tables'.DS.'adsuser.php');

    	$row = new JAdsAddsman($db);

        if($row->category)
             JRequest::setVar('custom_fields_category',$row->category );

        $category = (isset($row->category)) ? $row->category : 0 ;

        $include_root_params = array();
        if (defined("ads_opt_category_root_post") && ads_opt_category_root_post==0) {
          $include_root_params = array("include_root"=>0);
        }

        if( isset($category) && $category!="" ) {
            $categoryHTML = Ads_HTML::selectCategory("category", $include_root_params + array("select"=>$category,"script"=>"onchange='aiai(this.value);'"));
        }
        else
        {
            $categoryHTML = Ads_HTML::selectCategory("category", $include_root_params + array("script"=>"onchange='aiai(this.value);'"));
        }

        $Fi = & FactoryFieldsFront::getInstance();
        $lists["fields"] = $Fi->getHTMLFields("ads", $row->id, $row,1);

        $user = new JAdsUsers($db);
    	$lists["user_fields"] = null;


        $feat[]=JHTML::_('select.option', 'none', JText::_("ADS_NONE"));
        $feat[]=JHTML::_('select.option', 'gold', JText::_("ADSMAN_PAYMENT_GOLD"));
        $feat[]=JHTML::_('select.option', 'silver', JText::_("ADSMAN_PAYMENT_SILVER"));
        $feat[]=JHTML::_('select.option', 'bronze', JText::_("ADSMAN_PAYMENT_BRONZE"));

    	$lists['featured']=JHTML::_('select.genericlist', $feat,'featured','class="inputbox" id="featured" style="width:120px;"','value', 'text','none');
        $lists['category']= $categoryHTML;
    	//$lists['category'] 	= AdsUtilities::makeCatTree($row->category,"category");
    	$lists['currency'] 	= AdsUtilities::makeCurrencySelect($row->currency);

    	JHTML::_('behavior.calendar');
     	JAdsAdminView::edit( $row, $lists);
    }

    function ajax_form_customfields(){

        require_once (JPATH_ADMINISTRATOR.DS.'components'.DS.'com_adsman'.DS.'tables'.DS.'addsman.php');
        $Fi = & FactoryFieldsFront::getInstance();
        $id = JRequest::getInt( "catid", 0 );

        JRequest::setVar("custom_fields_category",$id);
        $session	= JFactory::getSession();
        $session->set('custom_fields_category',$id);

        $db = JFactory::getDbo();

        $adid = JRequest::getInt( "adid", 0 );
        $add = new JAdsAddsman($db);
        $add->loadData( $adid );

        $fields = $Fi->getHTMLFields("ads", $add->id, $add,1 );
        $doc = JFactory::getDocument();

        $root = JUri::root();
        $doc->addScriptDeclaration(' var root = "'.$root.'";');

        ob_end_clean();
        if(count($fields)){
            JAdsAdminView::ajaxCustomFields($fields);
        }else
            echo "";
        jexit();
    }

    function ajax_selectcategories(){
        $include_root_params = array();
        if (defined("ads_opt_category_root_post") && ads_opt_category_root_post==0) {
            $include_root_params = array("include_root"=>0);
        }

        $doc = JFactory::getDocument();

        $root = JUri::root();
        $doc->addScriptDeclaration(' var root = "'.$root.'";');

        $category = JRequest::getInt( "catid", 0 );

        if( isset($category) && $category!="" ) {
            $categoryHTML = Ads_HTML::selectCategory("category", $include_root_params + array("select"=>$category,"script"=>"onchange='aiai(this.value);'"));
        }
        else
        {
            $categoryHTML = Ads_HTML::selectCategory("category", $include_root_params + array("script"=>"onchange='aiai(this.value);'"));
        }
        ob_end_clean();
        echo $categoryHTML;
        jexit();
    }

	function edit(){
		$db		= JFactory::getDBO();
		$cid	= JRequest::getVar( 'cid', array(0), '', 'array' );
        $category = JRequest::getVar('category', 0 );

        $session	= JFactory::getSession();
        $session->clear("custom_fields_category");

		JArrayHelper::toInteger($cid, array(0));
		
		require_once (JPATH_ADMINISTRATOR.DS.'components'.DS.'com_adsman'.DS.'tables'.DS.'addsman.php');
		require_once (JPATH_BASE.DS."..".DS.'components'.DS.'com_adsman'.DS.'helpers'.DS.'helper.php');
		require_once (JPATH_ADMINISTRATOR.DS.'components'.DS.'com_adsman'.DS.'tables'.DS.'adsuser.php');


		$row = new JAdsAddsman($db);
		$row->load($cid[0]);

		if($row->category) 
			JRequest::setVar('custom_fields_category',$row->category );

        $jdoc = JFactory::getDocument();

        if (!defined("ads_opt_category_ajax_load") || ads_opt_category_ajax_load==0) {
            $include_root_params = array();
            if (defined("ads_opt_category_root_post") && ads_opt_category_root_post==0) {
                $include_root_params = array("include_root"=>0);
            }
            if( isset($category) && $category!="" )
                $lists["category"] = Ads_HTML::selectCategory("category", $include_root_params + array("select"=>$category,"script"=>"onchange='aiai(this.value);'"));
            else
                $lists["category"] = Ads_HTML::selectCategory("category", $include_root_params + array("select"=>$row->category,"script"=>"onchange='aiai(this.value);'"));
        } else {
            if ( isset($category) && $category!="" )
                $catid = $category;
            else
                $catid = $row->category;
            $js = ' window.addEvent("domready", function() {	categorySelectPopulate("ajax_selectcategories",'.(int)$catid.'); })';
            $page = 'var page = "edit";';
            $jdoc->addScriptDeclaration($js);
            $jdoc->addScriptDeclaration($page);

        }

        if ($row->id) {
            if ( defined("ads_opt_component_offset") && ads_opt_component_offset!="" )
                     $offset = ads_opt_component_offset;

            if ($offset != 'UTC') {
                $corelate = 0;
            } else {
                $corelate = 1;
            }

            $startdate = Ads_Time::getTimeStamp($row->start_date,$corelate);
            $expiredate = Ads_Time::getTimeStamp($row->end_date,$corelate);

            $diff = abs($expiredate - $startdate);

            //$db_no_of_days = floor($diff / 86400) + 1;
            $db_no_of_days = floor($diff / 86400) ;
            $lists['db_no_of_days'] = $db_no_of_days;
            $lists['db_key_valab'] = $db_no_of_days;
        }

        $timestamp = strtotime(gmdate("M d Y H:i:s", time()));
        $config = JFactory::getConfig();
        $offset = $config->get("offset");
        $add_days = $timestamp + 3600*$offset + 86400*3; //added 3 days

        $now = gmdate( "Y-m-d H:i:s", $timestamp + 3600*$offset);
        $end = gmdate( "Y-m-d H:i:s", $add_days);

        $lists['valab_now'] = $now;
        $lists['valab_end'] = $end;

        $payOb = JTheFactoryPaymentLib::getInstance();
        $payOb->loadAvailabeItems();
        $pricings = $payOb->price_plugins;  // array['price_featured_bronze'], ['price_packages'], ['price_pay_image'],['price_listing']

        $lists['time_limited'] = 0;

        if (isset($pricings['price_listing']) && $pricings['price_listing']->enabled == 1) {

        	if ( isset($pricings['price_listing']->time_valability) && $pricings['price_listing']->time_valability == 1 ) {  // Preconfigured Availabilities Ranges ==1

        		$params_arr = array();

        	  	  if ($pricings['price_listing']->time_valability_prices  && trim( strip_tags($pricings['price_listing']->time_valability_prices) ) !=""  ) {
        			$params_arr = explode("|", trim( strip_tags($pricings['price_listing']->time_valability_prices) ) );

                    $lists['valabs'] = array();

        			if (count($params_arr) > 0) {
        				foreach ($params_arr as $p){
        					if ($p) {
        						$pi = explode(":",$p);
        						$lists['valabs'][] = $pi[0]; //days
                                $vprices[$pi[0]] = $pi[1];
        					}
        				}
        				sort($lists['valabs']);

        				reset($lists['valabs']);
        		    	$first_key = key($lists['valabs']);
        				$add_days = $timestamp + 3600*$offset + 86400 * $lists['valabs'][$first_key] ; //added $valabs[$first_key] days

                        //require_once(  JPATH_SITE . DS . 'components' . DS . 'com_adsman'.DS.'helpers'.DS.'datetime.php' );
                        //$lists['valab_end']  = AdsHelperDateTime::isoDateToUTC($add_days);
        				$lists['valab_end']  = date( "Y-m-d H:i:s", $add_days);

                        $lists['time_limited'] = 1;
        						//$smarty->assign("time_limited", 1 );
        						/*$valabs
        						array
        						  0 => string '2' (length=1) days
        						  1 => string '3' (length=1)
        						  2 => string '10' (length=2)
        						*/
        					}
        				}

        	} else {
                if ( isset($pricings['price_listing']->preferential_categories) && $pricings['price_listing']->preferential_categories == 1) {

                    if ($pricings['price_listing']->default_price_categories && $pricings['price_listing']->prices_categories == 0) {
                        $lists['time_limited'] = 2;
                    } else {
                        $lists['time_limited'] = 0;
                    }
                }
            }

            $listing_enabled = 1;
            $lists["listing_enabled"] = $listing_enabled;
        }

        $jDoc = JFactory::getDocument();
        $js_block = "";
        $js_block .= "
				var VList = new Array();
				var VPriceList = new Array();";

        if (isset($lists['valabs'])) {
          if (count($lists['valabs']) ) {
            foreach ($lists['valabs'] as $k => $v){

            $rtime = Ads_Time::apendDays(Ads_Time::getNow(),$v);
            $rtime = gmdate("Y-m-d H:i:s", $rtime);

            $js_block .= "
            \r\n
                VList[{$k}] = '{$rtime}';\r\n
                VPriceList[{$k}] = '".$vprices[$v]."';\r\n
            ";
            }

            $jDoc->addScriptDeclaration($js_block);
          }
        }

        if ($row->id) {
		    $Fi = & FactoryFieldsFront::getInstance();
		    $lists["fields"] = $Fi->getHTMLFields("ads", $row->id, $row,0);
        } else {
            $Fi = & FactoryFieldsFront::getInstance();
            $lists["fields"] = $Fi->getHTMLFields("ads", $row->id, $row,1);
        }

        if ($row->id) {
            $js = ' var AFIDid = "'.$row->id.'";';
        } else {
            $js = ' var AFIDid = "";';
        }
        $jDoc->addScriptDeclaration($js);

	    $user = new JAdsUsers($db);
	    $row->userdetails 			= $user->getUserDetails($row->userid);
	    $row->userdetails->country 	= $user->getCountry();
	    
	    $lists["user_fields"] = null;
	    
	    if( !CB_DETECT )
	    	$lists["user_fields"] = $Fi->getCustomFields("user_profile", $row->userid);
	    
	    if ( isset($row->userdetails->country->name) )
			$row->userdetails->country = $row->userdetails->country->name;
		else 
			$row->userdetails->country = "";
			
	    $Juser = & JTable::getInstance("user");
	    $Juser->load($row->userid);
	    $row->userdetails->username = $Juser->username;
	    $row->messages = $row->getMessages();

		$feat[]=JHTML::_('select.option', 'none', JText::_("ADS_NONE"));
		$feat[]=JHTML::_('select.option', 'gold', JText::_("ADSMAN_PAYMENT_GOLD"));
		$feat[]=JHTML::_('select.option', 'silver', JText::_("ADSMAN_PAYMENT_SILVER"));
		$feat[]=JHTML::_('select.option', 'bronze', JText::_("ADSMAN_PAYMENT_BRONZE"));

		$lists['featured']=JHTML::_('select.genericlist', $feat,'featured','class="inputbox" id="featured" style="width:120px;"','value', 'text',$row->featured);

	    //$lists['category'] 	= AdsUtilities::makeCatTree($row->category,"category");
	    $lists['currency'] 	= AdsUtilities::makeCurrencySelect($row->currency);
	    
		JHTML::_('behavior.calendar');
 		JAdsAdminView::edit( $row, $lists);
	}

	function set_featured(){
		$mainframe = JFactory::getApplication();
		$db		= JFactory::getDBO();
		$cid	= JRequest::getVar( 'cid', array(0), '', 'array' );

	    $feat = JRequest::getVar('featured','');

	    if (!$id) $id = $cid[0];
	    if ($feat){
	        $db->setQuery("UPDATE #__ads SET featured='$feat' where id='$id'");
	        $db->query();
	    }
		$mainframe->redirect( "index.php?option=com_adsman&task=edit&cid[]=$id",JText::_("ADS_SAVED") );
	}

	function save(){
		$Itemid = JRequest::getVar('Itemid', 0, 'REQUEST', 'integer');
		$mainframe = JFactory::getApplication();
		
		$db		= JFactory::getDBO();
		$cid	= JRequest::getVar( 'cid', array(0), '', 'array' );
        $video_url = JRequest::getVar('video-link', '');
        $delete_video = JRequest::getVar('delete_video', '');

		if(!defined("ADDSMAN_IMAGE_PATH"))
			define('ADDSMAN_IMAGE_PATH',JPATH_COMPONENT_SITE.DS."images".DS );

		require_once (JPATH_ADMINISTRATOR.DS.'components'.DS.'com_adsman'.DS.'tables'.DS.'addsman.php');
		require_once (JPATH_BASE.DS."..".DS.'components'.DS.'com_adsman'.DS.'helpers'.DS.'helper.php');
		
		$row = new JAdsAddsman($db);
		$row->load($cid[0]);
		
		$posted_pm_values = JRequest::get('POST');

		$row->bind($posted_pm_values);
		$row->short_description = stripcslashes(JRequest::getVar('short_desc','','POST','string'));
		$row->description = stripcslashes(JRequest::getVar('description','','POST','string',JREQUEST_ALLOWRAW));

        // TAGS
        $tags 	= $posted_pm_values["tags"];
        $tag_obj = new adsTags($db, "#__ads");
        $tag_obj->setTags($row->id, $tags);

		$store = $row->store();

		if ($store !== true) {
			JError::raiseWarning(0,$store);
			$mainframe->redirect( "index.php?option=com_adsman&task=listadds",JText::_("ADS_NOT_SAVED") );
		}

		require_once(JPATH_ROOT.DS."components".DS."com_adsman".DS."models".DS."adsman.php");
        require_once(JPATH_ROOT.DS."components".DS."com_adsman".DS."plugins".DS."pricing".DS."price_pay_image.php");
		$model = new adsModelAdsman();

        if ($row->id) {
            // after ad is saved, save video url too
            if (isset($video_url) && $video_url != '' && !$delete_video) {
                $saved_url = $model->saveUrls($row->id,$video_url);

                if( $saved_url !== true ) {
                    JError::raiseWarning(1,JText::_("ADS_VIDEO_NOT_SAVED"));
                }
            }
        }

		$txt_msg = "";
		// UPLOAD FILES
		$m1 = $model->uploadFiles($row);
		if( $m1 )
			JError::raiseNotice(1,$m1);
			
    	if (isset($_FILES["atachment"])){
    		if (is_uploaded_file(@$_FILES["atachment"]['tmp_name'])){
    			$model->attachment = $_FILES["atachment"];
    			$model->attachment_uploaded = 1;
    		}
    	}
			
	   $m2 = $model->uploadAtachment($row);
		if($m2 )
			JError::raiseWarning(1,$m2);
			
      $m3 = $model->deleteFiles($row);
		if($m3 )
			JError::raiseWarning(1,$m3);

		if ($store) {
            if ($this->getTask() == 'edit.apply') {
            	// Redirect to the detail page.
                $mainframe->redirect( "index.php?option=com_adsman&task=edit&cid[]=$cid[0]",JText::_("ADS_SAVED") );
            }

			$mainframe->redirect( "index.php?option=com_adsman&task=listadds",JText::_("ADS_SAVED") );
		} else {
            if ($this->getTask() == 'edit.apply') {
                // Redirect to the detail page.
                $mainframe->redirect( "index.php?option=com_adsman&task=edit&cid[]=$cid[0]",JText::_("ADS_NOT_SAVED") );
            }

			$mainframe->redirect( "index.php?option=com_adsman&task=listadds",JText::_("ADS_NOT_SAVED") );
		}
	}

	function saveterms(){
		require_once (JPATH_ADMINISTRATOR.DS.'components'.DS.'com_adsman'.DS.'tables'.DS.'addsman.php');
		$database = & JFactory::getDBO();
		$term = new JAdsMails($database);

        $term->load('terms_and_conditions');
    	$term->content=JRequest::getVar('terms','','POST','string');

    	$term->store();
    	$msg=JText::_("ADS_SETTINGS_SAVED");
		$this->setRedirect( "index.php?option=com_adsman&task=terms",$msg );
	}
	
	function savesettings() {
		require_once (JPATH_ADMINISTRATOR.DS.'components'.DS.'com_adsman'.DS.'tables'.DS.'addsman.php');
		$database = JFactory::getDBO();

        $returnURL = JRequest::getVar('profile_integration',0, 'post','int');

        if (isset($returnURL) && $returnURL == 1 ) {

            $opts = new Ads_Options($database);

            $profileRequired = JRequest::getVar('ads_opt_profile_required',0);
            $profileMode = JRequest::getVar('ads_opt_profile_mode','ads');

            $requiredProfile = $opts->changeSetting('ads_opt_profile_required',$profileRequired);
            $integrationProfile = $opts->changeSetting('ads_opt_profile_mode',$profileMode);

            if ($integrationProfile) {
                $msg=JText::_("ADS_SETTINGS_SAVED");
            }
            else
                $msg= JText::_("ADS_FILE_WRITE_FAILED");



        } else {

            $opts = new Ads_Options($database);

            if ($opts->saveOptions()) {
                $msg=JText::_("ADS_SETTINGS_SAVED");
            }
            else
                $msg= JText::_("ADS_FILE_WRITE_FAILED");

            // New limit per user groups
            $_limitGroup = JRequest::getVar("limit_group_size","",'post');

            if ($_limitGroup) {

                $_userGroupLimits = JRequest::getVar('limit_group',array(),'POST','array');
                $opts->saveLimitsArray($_limitGroup,$_userGroupLimits,"group");
            }

            $_limitUser = JRequest::getVar("limit_user_size","",'post');

            if($_limitUser) {

                $_userUserLimits = JRequest::getVar('limit_user',array(),'POST','array');
                $opts->saveLimitsArray($_limitUser,$_userUserLimits,"user");
            }

            $term = new JAdsMails($database);

            $term->load('terms_and_conditions');
            $term->content=JRequest::getVar('terms_conditions','','POST','string',JREQUEST_ALLOWRAW);

            $term->store();

            $returnURL = JRequest::getVar('profile_integration',0, 'post','int');

        }
    	
    	if (isset($returnURL) && $returnURL == 1 )
    		$this->setRedirect("index.php?option=com_adsman&task=integration" , 'Profile '.$msg );
    	else
			$this->setRedirect( "index.php?option=com_adsman&task=settings",$msg );
	}

	function terms(){
		
		$database = JFactory::getDBO();
		$database->setQuery("select content from #__ads_mails where mail_type='terms_and_conditions' ");
		$lists['terms']= $database->loadResult();
		JAdsAdminView::showTerms($lists);
	}
	
	function settings() {
		
		global $ads_opt_gallery_objects;
		
		$database 	= JFactory::getDBO();
		$acl	  	= JFactory::getACL();
		$opts 		= new Ads_Options($database);


        $date_format[] = JHTML::_("select.option", 'Y-m-d', 'Y-m-d');
        $date_format[] = JHTML::_("select.option", 'm/d/Y', 'm/d/Y');
        $date_format[] = JHTML::_("select.option", 'd/m/Y', 'd/m/Y');
        $date_format[] = JHTML::_("select.option", 'd.m.Y', 'd.m.Y');
        $date_format[] = JHTML::_("select.option", 'D, F d Y', 'D, F d Y');

		$lists['date_format'] = JHTML::_("select.genericlist",$date_format,"ads_opt_date_format"," onChange='showDateInFormat(this.value);' " ,'value', 'text',ads_opt_date_format, "Change date format");

		$time_format[] = JHTML::_("select.option", 'H:i', 'H:i');
		$time_format[] = JHTML::_("select.option", 'h:iA', 'h:iA');
		$lists['date_time_format'] = JHTML::_("select.genericlist",$time_format,"ads_opt_date_time_format","onChange='showTimeInFormat(this.value);'",'value', 'text',ads_opt_date_time_format,"Change time format");

		$database->setQuery("SELECT content FROM #__ads_mails WHERE mail_type='terms_and_conditions' ");
		$lists['terms']		= $database->loadResult();

		$database->setQuery( "SELECT * FROM #__ads_currency" );
		$lists['currency']	= $database->loadObjectList();
		
		$groups = array();
		
		$lists["users_group_limits"] = $opts->getLimitGroups();
		
		if ($lists["users_group_limits"]) {
			
			foreach ($lists["users_group_limits"] as $k => $g)
				$groups[] = $g->id;
		
			$groups = implode(",",$groups);
		}

	    $usergroup = $this->getGroups();
		$lists["users_groups"] = $usergroup;
	
		$users_l = array();
		$lists["users_user_limits"] = $opts->getLimitUsers();
		foreach ($lists["users_user_limits"] as $k => $g)
			$users_l[] = $g->id;	
		
		$users_l = implode(",",$users_l);
		$filteruser = "";
		if($users_l!="")
			$filteruser = " AND id NOT IN ($users_l) ";
		
		$users = array();
		$query = 'SELECT id AS value, name AS text'
		. ' FROM #__users'
		. ' WHERE block = 0' . $filteruser;
		
		$database->setQuery( $query );
		
		if ( isset($nouser) ) {
			$users[] = JHTML::_('select.option',  '0', '- '. JText::_( 'ADSMAN_NO_USER' ) .' -' );
			$users = array_merge( $users, $database->loadObjectList() );
		} else {
			$users = $database->loadObjectList();
		}
		$lists["users"] = $users;

		$lists['ads_opt_allow_guest_messaging'] = JHTML::_("select.booleanlist",'ads_opt_allow_guest_messaging','',ads_opt_allow_guest_messaging, JText::_('ADS_YES'),JText::_("ADS_NO"));
		$lists['ads_opt_inner_categories'] 		= JHTML::_("select.booleanlist",'ads_opt_inner_categories','',ads_opt_inner_categories,JText::_('ADS_YES'),JText::_("ADS_NO"));
		$lists['ads_opt_enable_captcha'] 		= JHTML::_("select.booleanlist",'ads_opt_enable_captcha','',ads_opt_enable_captcha,JText::_('ADS_YES'),JText::_("ADS_NO"));
		$lists['ads_opt_category_root_post'] 	= JHTML::_("select.booleanlist",'ads_opt_category_root_post','',defined("ads_opt_category_root_post")?ads_opt_category_root_post:1,JText::_('ADS_YES'),JText::_("ADS_NO"));
				
        $galleries_plugins = array();
        foreach($ads_opt_gallery_objects as $key=>$value)
	    {
	    	$galleries_plugins[] = JHTML::_('select.option', $key, $value);
	    }
		$lists['gallery_plugin'] = JHTML::_('select.genericlist',$galleries_plugins,'gallery_plugin',"" ,'value', 'text',ads_opt_gallery);
		
		
		$row = new JConfig();
		$errors 	= array ( 'default' => JText::_('ADS_SYSYTEM_DEFAULT'), 0=> JText::_('ADS_NONE'),  E_ERROR | E_WARNING | E_PARSE => JText::_('ADS_SIMPLE'), 'maximum'=> JText::_('ADS_MAXIMUM'));
		
		$lists["error_reporting"] = $errors[$row->error_reporting];
		
		$lists["local_time"] = JHTML::_('date',  'now', JText::_('DATE_FORMAT_LC2'));
		$lists["gmt_time"] = gmdate('l, d F Y H:i',time());

        $list_sort_date_options = array();
        $list_sort_date_options[] = JHTML::_('select.option', 'start', 'Published date');
        $list_sort_date_options[] = JHTML::_('select.option', 'end', 'End date');

        $selected_sort_date = defined("ads_opt_sort_date") ? ads_opt_sort_date : 'end';
//        $lists["ads_opt_sort_date"] 	= JHTML::_("select.genericlist", $list_sort_date_options,'ads_opt_sort_date',"" ,'value', 'text', $selected_sort_date);
        $lists['ads_opt_sort_date'] = JHTML::_("select.radiolist",$list_sort_date_options,'ads_opt_sort_date','style="float: left; "' ,'value', 'text', $selected_sort_date);

    	$checkout_options = array();
		$checkout_options[] = JHTML::_('select.option', 'credits', 'Buy credits directly on checkout');
		$checkout_options[] = JHTML::_('select.option', 'packages', 'Show package selection on checkout');
		
		$selected_checkout = defined("ads_opt_choose_checkout") ? ads_opt_choose_checkout : 'credits';
		$lists['ads_opt_choose_checkout'] 	= JHTML::_("select.radiolist",$checkout_options,'ads_opt_choose_checkout','style="float: left; clear: left !important; "' ,'value', 'text', $selected_checkout);
		
		
		jimport('joomla.filesystem.folder');
		$cache_files = JFolder::files(JPATH_ROOT.DS."components".DS."com_adsman".DS."templates".DS."cache".DS);
		
		$lists["cache_stats"]["cache_size"] 	= 0;
		$lists["cache_stats"]["no_files"] 		= 0;
		$lists["cache_stats"]["last_cached"] 	= $lists["cache_stats"]["first_cached"] = "nothing cached";
		
		if($cache_files){
			foreach ($cache_files as $file){
				$lists["cache_stats"]["cache_size"] += filesize(JPATH_ROOT.DS."components".DS."com_adsman".DS."templates".DS."cache".DS.$file);
				$lists["cache_files"][] 			= date("d M Y H:i:s", filemtime(JPATH_ROOT.DS."components".DS."com_adsman".DS."templates".DS."cache".DS.$file));
			}
			
			$lists["cache_stats"]["no_files"] 		= count($lists["cache_files"]);
			$lists["cache_stats"]["last_cached"] 	= max($lists["cache_files"]);
			$lists["cache_stats"]["first_cached"] 	= min($lists["cache_files"]);
		}
		
		$lists["recaptcha"]["public_key"]="";
		if(defined('ads_opt_recaptcha_public_key'))
			$lists["recaptcha"]["public_key"]= ads_opt_recaptcha_public_key;

			
		$lists["recaptcha"]["private_key"]="";
		if(defined('ads_opt_recaptcha_private_key'))
			$lists["recaptcha"]["private_key"]=ads_opt_recaptcha_private_key;
		
		$lists["mailcaptcha"]["public_key"]="";
		if(defined('ads_opt_mailcaptcha_public_key'))
			$lists["mailcaptcha"]["public_key"]= ads_opt_mailcaptcha_public_key;

			
		$lists["mailcaptcha"]["private_key"]="";
		if(defined('ads_opt_mailcaptcha_private_key'))
			$lists["mailcaptcha"]["private_key"]=ads_opt_mailcaptcha_private_key;
		
		$captcha_themes = array();
		$captcha_themes[] = JHTML::_('select.option', 'red', 'Red');
		$captcha_themes[] = JHTML::_('select.option', 'white', 'White');
		$captcha_themes[] = JHTML::_('select.option', 'blackglass', 'Blackglass');
		$captcha_themes[] = JHTML::_('select.option', 'clean', 'Clean');
		
		$lists["recaptcha_themes"] 				= JHTML::_("select.genericlist", $captcha_themes,'ads_opt_recaptcha_theme',"" ,'value', 'text', defined("ads_opt_recaptcha_theme")?ads_opt_recaptcha_theme:"red");
		$lists['ads_opt_enable_antispam_bot'] 	= JHTML::_("select.booleanlist",'ads_opt_enable_antispam_bot','',defined("ads_opt_enable_antispam_bot")?ads_opt_enable_antispam_bot:1,JText::_('ADS_YES'),JText::_("ADS_NO"));
		
		$antispam_bots = array();
		$antispam_bots[] = JHTML::_('select.option', 'joomla', 'Joomla');
		$antispam_bots[] = JHTML::_('select.option', 'smarty', 'Smarty');
		$antispam_bots[] = JHTML::_('select.option', 'recaptcha', 'reCaptcha');
		
		$lists['ads_opt_choose_antispam_bot'] 	= JHTML::_("select.radiolist",$antispam_bots,'ads_opt_choose_antispam_bot',' onchange="toggleMailCaptcha(this.value);"' ,'value', 'text',defined("ads_opt_choose_antispam_bot")?ads_opt_choose_antispam_bot:"joomla");
		$lists['ads_opt_logged_posting'] 		= JHTML::_("select.booleanlist",'ads_opt_logged_posting','',ads_opt_logged_posting,JText::_('ADS_YES'),JText::_("ADS_NO"));
		
		$database->setQuery("select content from #__ads_mails where mail_type='terms_and_conditions' ");
		$lists['terms']= $database->loadResult();
		
		$lists['ads_opt_category_ajax_load'] = JHTML::_("select.booleanlist",'ads_opt_category_ajax_load','',defined("ads_opt_category_ajax_load") ? ads_opt_category_ajax_load:0,JText::_('ADS_YES'),JText::_("ADS_NO"));
		$lists['ads_opt_post_editor_buttons'] = JHTML::_("select.booleanlist",'ads_opt_post_editor_buttons','',defined("ads_opt_post_editor_buttons") ? ads_opt_post_editor_buttons:0,JText::_('ADS_YES'),JText::_("ADS_NO"));

        $lists['ads_opt_transliterate'] = JHTML::_("select.booleanlist",'ads_opt_transliterate','',defined("ads_opt_transliterate") ? ads_opt_transliterate:0,JText::_('ADS_YES'),JText::_("ADS_NO"));
		
		$config = JFactory::getConfig();
		
		$offset = $config->getValue("config.offset"); //UTC default //JFactory::getConfig()->get('offset');
						
		// Get the list of time zones from the server.
		/*$zones = DateTimeZone::listIdentifiers();*/
		// If the timezone is not set use the server setting.			
		if(defined("ads_opt_component_offset") && ads_opt_component_offset !="" )
			$sel_offset = ads_opt_component_offset;
		else
			$sel_offset = $offset;
		
		// LOCALE SETTINGS
		$timezones = array (
			JHtml::_('select.option', -12, JText::_('UTC__12_00__INTERNATIONAL_DATE_LINE_WEST')),
			JHtml::_('select.option', -11, JText::_('UTC__11_00__MIDWAY_ISLAND__SAMOA')),
			JHtml::_('select.option', -10, JText::_('UTC__10_00__HAWAII')),
			JHtml::_('select.option', -9.5, JText::_('UTC__09_30__TAIOHAE__MARQUESAS_ISLANDS')),
			JHtml::_('select.option', -9, JText::_('UTC__09_00__ALASKA')),
			JHtml::_('select.option', -8, JText::_('UTC__08_00__PACIFIC_TIME__US__AMP__CANADA_')),
			JHtml::_('select.option', -7, JText::_('UTC__07_00__MOUNTAIN_TIME__US__AMP__CANADA_')),
			JHtml::_('select.option', -6, JText::_('UTC__06_00__CENTRAL_TIME__US__AMP__CANADA___MEXICO_CITY')),
			JHtml::_('select.option', -5, JText::_('UTC__05_00__EASTERN_TIME__US__AMP__CANADA___BOGOTA__LIMA')),
			JHtml::_('select.option', -4, JText::_('UTC__04_00__ATLANTIC_TIME__CANADA___CARACAS__LA_PAZ')),
			JHtml::_('select.option', -4.5, JText::_('UTC__04_30__VENEZUELA')),
			JHtml::_('select.option', -3.5, JText::_('UTC__03_30__ST__JOHN_S__NEWFOUNDLAND__LABRADOR')),
			JHtml::_('select.option', -3, JText::_('UTC__03_00__BRAZIL__BUENOS_AIRES__GEORGETOWN')),
			JHtml::_('select.option', -2, JText::_('UTC__02_00__MID_ATLANTIC')),
			JHtml::_('select.option', -1, JText::_('UTC__01_00__AZORES__CAPE_VERDE_ISLANDS')),
			JHtml::_('select.option', 0, JText::_('UTC_00_00__WESTERN_EUROPE_TIME__LONDON__LISBON__CASABLANCA')),
			JHtml::_('select.option', 1, JText::_('UTC__01_00__AMSTERDAM__BERLIN__BRUSSELS__COPENHAGEN__MADRID__PARIS')),
			JHtml::_('select.option', 2, JText::_('UTC__02_00__ISTANBUL__JERUSALEM__KALININGRAD__SOUTH_AFRICA')),
			JHtml::_('select.option', 3, JText::_('UTC__03_00__BAGHDAD__RIYADH__MOSCOW__ST__PETERSBURG')),
			JHtml::_('select.option', 3.5, JText::_('UTC__03_30__TEHRAN')),
			JHtml::_('select.option', 4, JText::_('UTC__04_00__ABU_DHABI__MUSCAT__BAKU__TBILISI')),
			JHtml::_('select.option', 4.5, JText::_('UTC__04_30__KABUL')),
			JHtml::_('select.option', 5, JText::_('UTC__05_00__EKATERINBURG__ISLAMABAD__KARACHI__TASHKENT')),
			JHtml::_('select.option', 5.5, JText::_('UTC__05_30__BOMBAY__CALCUTTA__MADRAS__NEW_DELHI__COLOMBO')),
			JHtml::_('select.option', 5.75, JText::_('UTC__05_45__KATHMANDU')),
			JHtml::_('select.option', 6, JText::_('UTC__06_00__ALMATY__DHAKA')),
			JHtml::_('select.option', 6.5, JText::_('UTC__06_30__YAGOON')),
			JHtml::_('select.option', 7, JText::_('UTC__07_00__BANGKOK__HANOI__JAKARTA__PHNOM_PENH')),
			JHtml::_('select.option', 8, JText::_('UTC__08_00__BEIJING__PERTH__SINGAPORE__HONG_KONG')),
			JHtml::_('select.option', 8.75, JText::_('UTC__08_00__WESTERN_AUSTRALIA')),
			JHtml::_('select.option', 9, JText::_('UTC__09_00__TOKYO__SEOUL__OSAKA__SAPPORO__YAKUTSK')),
			JHtml::_('select.option', 9.5, JText::_('UTC__09_30__ADELAIDE__DARWIN__YAKUTSK')),
			JHtml::_('select.option', 10, JText::_('UTC__10_00__EASTERN_AUSTRALIA__GUAM__VLADIVOSTOK')),
			JHtml::_('select.option', 10.5, JText::_('UTC__10_30__LORD_HOWE_ISLAND__AUSTRALIA_')),
			JHtml::_('select.option', 11, JText::_('UTC__11_00__MAGADAN__SOLOMON_ISLANDS__NEW_CALEDONIA')),
			JHtml::_('select.option', 11.5, JText::_('UTC__11_30__NORFOLK_ISLAND')),
			JHtml::_('select.option', 12, JText::_('UTC__12_00__AUCKLAND__WELLINGTON__FIJI__KAMCHATKA')),
			JHtml::_('select.option', 12.75, JText::_('UTC__12_45__CHATHAM_ISLAND')),
			JHtml::_('select.option', 13, JText::_('UTC__13_00__TONGA')),
			JHtml::_('select.option', 14, JText::_('UTC__14_00__KIRIBATI')),);
		
		$lists['ads_opt_component_offset'] 	= JHTML::_('select.genericlist',  $timezones, 'ads_opt_component_offset','class="inputbox" size="1"', 'value', 'text', $sel_offset);
		$lists["component_time"] 			= JHTML::_('date',  'now', JText::_('DATE_FORMAT_LC2'),$offset);
				
		JAdsAdminView::showSettings($lists);
	}
	
	static function getGroups()
	{
		$user = JFactory::getUser();
		
		$db = JFactory::getDbo();
		$db->setQuery(
			'SELECT a.id, a.title, COUNT(DISTINCT b.id) AS level' .
			' FROM #__usergroups AS a' .
			' LEFT JOIN `#__usergroups` AS b ON a.lft > b.lft AND a.rgt < b.rgt' .
			' GROUP BY a.id' .
			' ORDER BY a.lft ASC'
		);
		$options = $db->loadObjectList();

		// Check for a database error.
		if ($db->getErrorNum()) {
			JError::raiseNotice(500, $db->getErrorMsg());
			return null;
		}

		return $options;
	}

	function showadsconfig(){
		JAdsAdminView::showAdsManager();
	}
	
	function show_customfields_config(){
		JAdsAdminView::showCustomFieldsManager();
	}

	function settingsmanager() {
		JAdsAdminView::showSettingsManager();
	}

    function dashboard() {
        $mainframe = JFactory::getApplication();
        $db = JFactory::getDBO();
        $where = array();

        $nowSql = Ads_Time::getNowSql();

        $query = "SELECT COUNT(*) FROM #__ads WHERE status = 1 AND close_by_admin = 0 AND closed = 0 AND '".Ads_Time::getNowSql()."' <= end_date  ";
        $db->setQuery($query);
        $nr_active = $db->loadResult();

        $query="SELECT COUNT(*) FROM #__ads ";
        $db->setQuery($query);
        $nr_total = $db->loadResult();

        $query="SELECT COUNT(*) FROM #__ads WHERE status = 1 ";
        $db->setQuery($query);
        $nr_published = $db->loadResult();

        $query="SELECT COUNT(*) FROM #__ads WHERE status = 0  OR close_by_admin = 1 OR (closed = 1 AND status = 1 AND close_by_admin = 0 AND closed_date <= end_date) ";
        $db->setQuery($query);
        $nr_unpublished = $db->loadResult();

        $query="SELECT COUNT(*) FROM #__ads WHERE close_by_admin = 1";
        $db->setQuery($query);
        $nr_blocked = $db->loadResult();

        $query="SELECT COUNT(*) FROM #__ads WHERE  closed = 0 and status = 1 AND close_by_admin = 0 AND '".Ads_Time::getNowSql()."' >= end_date";
        $db->setQuery($query);
        $nr_expired = $db->loadResult();

        $query="SELECT COUNT(*) FROM #__ads_messages";
        $db->setQuery($query);
        $nr_messages = $db->loadResult();

        $query="SELECT COUNT(distinct userid) FROM #__ads";
        $db->setQuery($query);
        $nr_a_users = $db->loadResult();

        $query="SELECT COUNT(*) FROM #__users";
        $db->setQuery($query);
        $nr_r_users = $db->loadResult();

        $query="SELECT COUNT(*) FROM #__ads_report";
        $db->setQuery($query);
        $nr_ads_reported = $db->loadResult();

        $query="SELECT COUNT(*) FROM #__ads WHERE status = 1 AND closed = 1 AND closed_date > end_date ";
        $db->setQuery($query);
        $nr_ads_archived = $db->loadResult();

        $query="SELECT COUNT(*) FROM #__ads_users WHERE status = 0";
        $db->setQuery($query);
        $nr_blocked_users = $db->loadResult();

        $query = 'SELECT count(id) FROM #__ads_users ';
        $db->setQuery($query);
        $nr_users_profile = $db->loadResult();
        $nr_users_unfilled_profile = $nr_r_users - 	$nr_users_profile;


        $query="SELECT payment_method, currency, SUM(amount) as currency_amount, COUNT(*) as currency_quant FROM #__ads_paylog WHERE status = 'ok' GROUP BY payment_method, currency";
        $db->setQuery($query);
        $received_payments = $db->loadAssocList();

        /*$query="SELECT currency, SUM(amount) as currency_amount FROM #__ads_paylog WHERE status = 'ok' GROUP BY payment_method,currency";
        $db->setQuery($query);
        $received_amount = $db->loadAssocList();*/

        $context			= 'com_adsman.jadsadminview.statistics';
        $lists['order']		= $mainframe->getUserStateFromRequest( $context.'filter_order',		'filter_order',		'',	'cmd' );
        $lists['order_Dir']	= $mainframe->getUserStateFromRequest( $context.'filter_order_Dir',	'filter_order_Dir',	'',	'word' );

        if (!$lists['order']) {
            $lists['order'] = 'username';
        }
        $order = ' ORDER BY '. $lists['order'] .' '. $lists['order_Dir'] .'';

        $limit		= $mainframe->getUserStateFromRequest('global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
        $limitstart	= $mainframe->getUserStateFromRequest('global.limitstart', 'limitstart', 0, 'int');
        // In case limit has been changed, adjust limitstart accordingly
        $limitstart = ( $limit != 0 ? (floor($limitstart / $limit) * $limit) : 0 );

        //$where[] = " username!='admin' ";

        // Build the where clause of the content record query
        $where = (count($where) ? ' WHERE '.implode(' AND ', $where) : '');

        // Get the total number of records
        $query = "SELECT COUNT(*) ".
            "FROM #__users".$where.$order;

        $db->setQuery($query);
        $total = $db->loadResult();

        // Create the pagination object
        jimport('joomla.html.pagination');
        $pagination = new JPagination($total, $limitstart, $limit);

        // Get the articles
        $query = " SELECT u.id, u.username, count( a.userid ) as nr_ads ".
                 " FROM #__users AS u ".
                 " LEFT JOIN #__ads AS a ON a.userid = u.id ".
                 " GROUP BY u.username ".
                 $where;
        $db->setQuery($query, $pagination->limitstart, $pagination->limit);
        $rows = $db->loadObjectList();
        foreach ($rows as $k => $v){

            $query = "SELECT COUNT(*) ".
                "FROM #__ads WHERE userid = $v->id AND featured = 'gold' ";
            $db->setQuery($query);
            $v->gold = $db->loadResult();

            $query = "SELECT COUNT(*) ".
                "FROM #__ads WHERE userid = $v->id AND featured = 'bronze' ";
            $db->setQuery($query);
            $v->bronze = $db->loadResult();

            $query = "SELECT COUNT(*) ".
                "FROM #__ads WHERE userid = $v->id AND featured = 'silver' ";
            $db->setQuery($query);
            $v->silver = $db->loadResult();
        }

        // table ordering
        $lists['nr_r_users']		= $nr_r_users;
        $lists['nr_a_users']		= $nr_a_users;
        $lists['nr_messages']		= $nr_messages;
        $lists['nr_total']			= $nr_total;
        $lists['nr_active']			= $nr_active;
        $lists['nr_published']		= $nr_published;
        $lists['nr_unpublished']	= $nr_unpublished;
        $lists['nr_blocked']		= $nr_blocked;
        $lists['nr_expired']		= $nr_expired;
        $lists['nr_ads_reported']	= $nr_ads_reported;
        $lists['nr_ads_archived']	= $nr_ads_archived;
        $lists['nr_blocked_users']	= $nr_blocked_users;
        $lists['nr_users_unfilled_profile'] = $nr_users_unfilled_profile;
        $lists['received_payments'] = $received_payments;
       // $lists['received_amount'] 	= $received_amount;

        $latestAds = $this->showLatestads();
        $latestPayments = $this->showLatestpayments();

        JAdsAdminView::showDashboard($rows, $pagination, $lists,$latestAds,$latestPayments);
    }

	function delete_limit(){
		$mainframe = JFactory::getApplication();
		
		$db     = JFactory::getDBO();
		$id     = JRequest::getVar("owner",$_GET);
		$type   = JRequest::getVar("type",$_GET);
		$sql    = "DELETE FROM #__ads_limits WHERE owner = '$id' AND limit_type='{$type}'";
		$db->setQuery($sql);
		$db->query();
		$mainframe->redirect("index.php?option=com_adsman&task=settings","Limit deleted");
	}

	function offerFreeCredits() {
		$mainframe = JFactory::getApplication();
		$db = JFactory::getDBO();
	
		$user_id = (int)JRequest::getVar('user_id', 0, 'POST','string');
		$amount = (int)JRequest::getVar('free_credits_amount',0, 'POST','string');		
		 
		$db->setQuery("SELECT credits_no FROM #__ads_user_credits WHERE userid='$user_id' ");
		$val = $db->LoadResult();
    	
        if ($val != null && $val >= 0 ){
            $db->setQuery("UPDATE #__ads_user_credits SET credits_no=credits_no+$amount, package_free=1 WHERE userid='$user_id' ");
        } 
        else 
        {
            $db->setQuery("INSERT INTO #__ads_user_credits SET credits_no=$amount , userid='$user_id', package_free=1 ");
        }
        
        $db->query();
		
		$mainframe->redirect("index.php?option=com_adsman&task=paymentitemsconfig&itemname=packages","Free Credits offered to selected user");
		
	}
	
	// IMPORT / EXPORTs	TASKS
	/**
	 * Custom Fields Selection Panel
	 *
	 * @return
	 */
	function ExportToXls(){
		$db = & JFactory::getDBO();
		
		$lists = null;
		
		$db->setQuery("SELECT * FROM #__".APP_CFIELDS_PREFIX."_fields ORDER BY ordering ASC, page ASC ");
		$lists["fields"] = $db->loadObjectList();

        JTheFactoryAdminHelper::beginAdminForm("do_ExportToXls",null,'post',true,JText::_('ADS_EXPORT_TO_XLS'));
 		JAdsAdminView::ExportToXls( $lists );
        JTheFactoryAdminHelper::endAdminForm();
		return true;
	}

	function do_ExportToXls() {

		$db = JFactory::getDBO();

		$JoinList = array();
		$SelectCols = array();
		FactoryFieldsFront::getCustomFieldsJoin($JoinList, $SelectCols, null, array("#__ads_users"=>"prof","#__ads"=>"a"));
		
		$cid = JRequest::getVar("cid",array());
		
		$cfields_sql = "";
		if( count($SelectCols)>0 ){
			$cfields_sql = ",".implode(",",$SelectCols);
		}

		$sql = "
		SELECT *, 	
			 b.name as currency_name, d.catname,  IF (a.userid != 0, e.username , 'Guest') as username {$cfields_sql} ".PHP_EOL.
		" FROM #__ads a  ".PHP_EOL.
        " LEFT JOIN #__ads_users prof ON a.userid= prof.userid  ".PHP_EOL.
		 implode(" \r\n ",$JoinList).PHP_EOL."
		LEFT JOIN #__ads_currency b ON a.currency = b.id  ".PHP_EOL." 
		LEFT JOIN #__ads_categories d ON a.category = d.id ".PHP_EOL." 
		LEFT JOIN #__users e ON a.userid = e.id ".PHP_EOL."
		GROUP BY a.id 
		";
		$db->setQuery($sql);
		$result = $db->loadObjectList();
			
		
		## Empty data vars
        $data = "" ;
        ## We need tabbed data
        $sep = "\t"; 
      
        $header = array('User','Title','Short description','Description','Picture','Ask price','Currency','Start date','End date','Published','Category');
     
        if( $cid ) {
		  $db->setQuery("SELECT name, db_name FROM #__".APP_CFIELDS_PREFIX."_fields WHERE status = 1 ORDER BY ordering");
		  $cflabels = $db->loadObjectList("db_name");
		  $custom_headers = '';
						
		  foreach ($cid as $c => $field ){
			array_push($header,$cflabels[$field]->name );
			//$fields .= '"' . $cflabels[$field]->name . '"' . " \t ";
   		  }
        }
      
 	   	if (isset($result)) {
      		$fields = (array_values($header));
 	   	}

 	  	## Count all fields(will be the collumns
      	$columns = count($fields);
      	
      	## Put the name of all fields to $out.  
      	for ($i = 0; $i < $columns; $i++) {
        	$data .= $fields[$i]."\t";
      	}
  
      	$data .= "\n";
      
      	## Counting rows and push them into a for loop
     	for($k=0; $k < count( $result ); $k++) {
         $row = $result[$k];
         
         $line = '';
         $line .= "$row->username" . "\t";
         $line .= "$row->title" . "\t";
         $line .= "$row->short_description" . "\t";
         $line .= "$row->description" . "\t";
         $line .= "$row->picture" . "\t";
         $line .= "$row->askprice" . "\t";
         $line .= "$row->currency_name" . "\t";
         $line .= "$row->start_date" . "\t";
         $line .= "$row->end_date" . "\t";
         
         switch ($row->addtype){ 
	 			case '0':
					$line .= "public" . "\t";
				break;
				case '1':
					$line .= "private" . "\t";
				break;
	 		}

	 	 $line .= "$row->catname" . "\t";
         
         if( $cid ) {
			foreach ($cid as $c => $field ){
				$line .= '"' . $row->$field . '"' . "\t";
			}
         }
 
         $data .= trim($line)."\n";
      }
       
      $data = str_replace("\r\n","",$data);
           
      if (count( $result ) == 0) {
        $data .= "\n(0) Records Found!\n";
      }
      
		
		$filename = "ExportXLS";
		header("Content-type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=\"".$filename.".xls\"");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
		header("Pragma: public");
		
		print $data;
	//	ob_clean();
		ob_end_flush();
		exit;
	}

	function showadmimportform() {
		JAdsAdminView::bulkadmimport(null);
	}

	function importcsv(){
	    $err=AdsUtilities::ImportFromCSV();
    
	    if (count($err)<=0)
			$this->setRedirect( "index.php?option=com_adsman",JText::_("ADS_IMPORTED_SUCCESS"));
	    else
	    {
		 	JAdsAdminView::bulkadmimport($err);
	    }

	}


// USERS TASKS
	function blockuser(){
		AdsUtilities::block_user(1);
	}

	function unblockuser(){
		AdsUtilities::block_user(0);
	}

	function users() {
		
		$db		= JFactory::getDBO();
		$app	= JFactory::getApplication();
		$where = array();
        $whereAds = '';

		$context			= 'com_adsman.adsadminview.users';
		$filter_order		= $app->getUserStateFromRequest( $context.'filter_order',		'filter_order',		'',	'cmd' );
		$filter_order_Dir	= $app->getUserStateFromRequest( $context.'filter_order_Dir',	'filter_order_Dir',	'',	'word' );
		
		$search				= $app->getUserStateFromRequest( $context.'search',			'search',			'',	'string' );
		$search				= JString::strtolower($search);

        $profile_state 	    = $app->getUserStateFromRequest($context.'profile_state', 	 'profile_state', 	 2, 'int');
        $status 	        = $app->getUserStateFromRequest($context.'status', 	 'status', 	 2, 'int');

		$limit				= $app->getUserStateFromRequest('global.list.limit', 'limit', $app->getCfg('list_limit'), 'int');
		$limitstart			= $app->getUserStateFromRequest($context.'limitstart', 'limitstart', 0, 'int');
		// In case limit has been changed, adjust limitstart accordingly
		$limitstart 		= ( $limit != 0 ? (floor($limitstart / $limit) * $limit) : 0 );

		if (!$filter_order) {
			$filter_order = 'name';
		}
		$order = ' ORDER BY '. $filter_order .' '. $filter_order_Dir .'';

		if($search)
			$where[] = " username LIKE '%$search%' ";

        if($status == 0 || $status == 1)
            $where[] = " (block = $status ) ";

		// Build the where clause of the content record query
		$where = (count($where) ? ' WHERE '.implode(' AND ', $where) : '');

        if($profile_state == 1)
            $whereAds = ($where != '') ? ' AND b.id = '.$profile_state  : ' WHERE b.id = '.$profile_state;

        if ($profile_state == 0)
            $whereAds = ($where != '') ? " AND b.id IS NULL " : " WHERE b.id IS NULL  ";

		// Get the total number of records
		$query = "SELECT COUNT(*) ".
			"FROM #__users".$where;
		$db->setQuery($query);
		$total = $db->loadResult();

		// Create the pagination object
		jimport('joomla.html.pagination');
		$pagination = new JPagination($total, $limitstart, $limit);

		// Get the users
    	$query = "SELECT b.id as profid, u.id as userid1, u.username as username,u.name as name ".
    			", u.email AS email , u.block as blocked, b.id as usrid,c.name as country_name, 
    			 COUNT(DISTINCT aads.id) as nr_total_ads,
    			 COUNT(DISTINCT aactive.id) as nr_open_ads,
    			 COUNT(DISTINCT aclosed.id) as nr_closed_ads, b.status".
    			" FROM #__users AS u ".
    			" LEFT JOIN #__ads_users AS b ON u.id=b.userid ".
    			" LEFT JOIN #__ads_country AS c ON b.country=c.id ".
    			
    			" LEFT JOIN #__ads AS aads ON u.id= aads.userid  ".
    			" LEFT JOIN #__ads AS aactive ON u.id= aactive.userid and aactive.closed <> 1  ".
    			" LEFT JOIN #__ads AS aclosed ON u.id= aclosed.userid and aclosed.closed = 1  ".
				$where.
                $whereAds.
				" GROUP BY u.id ".$order;

		$db->setQuery($query, $limitstart, $limit);
		$rows = $db->loadObjectList();

        foreach ($rows as $k => $v){
            $query = "SELECT COUNT(*) ".
                "FROM #__ads WHERE userid = $v->userid1 AND featured = 'gold' ";
            $db->setQuery($query);
            $v->gold = $db->loadResult();

            $query = "SELECT COUNT(*) ".
                "FROM #__ads WHERE userid = $v->userid1 AND featured = 'bronze' ";
            $db->setQuery($query);
            $v->bronze = $db->loadResult();

            $query = "SELECT COUNT(*) ".
                "FROM #__ads WHERE userid = $v->userid1 AND featured = 'silver' ";
            $db->setQuery($query);
            $v->silver = $db->loadResult();

            $query = "SELECT b.id as profid ".
              " FROM #__users AS u ".
              " LEFT JOIN #__ads_users AS b ON u.id=b.userid ".
              " WHERE userid = $v->userid1 ";
            $db->setQuery($query);

        }

		$lists["order"] = $filter_order;
		$lists["order_Dir"] = $filter_order_Dir;
        // search filter
        $lists['search'] = $search;

        // status active/archived
        $lists['status'] = $status;

        $lists["profile_state"] = $profile_state;

		JAdsAdminView::listusers($rows, $pagination, $lists);
	}
	
	function detailuser(){
		
		$app	= JFactory::getApplication();
		$db = & JFactory::getDBO();
		
		$cid = JRequest::getVar("cid",array());
		$id = JRequest::getVar("id","int");

		if(!$id){
			if (!$id) $id=$cid[0];			
		}

		require_once (JPATH_ADMINISTRATOR.DS.'components'.DS.'com_adsman'.DS.'tables'.DS.'adsuser.php');
	
		$user = new JAdsUsers($db);
		if (!$user->getUserDetails($id) && (CB_DETECT!=1)){
			echo "<script> alert('".JText::_("ADS_ERR_NO_SELECTED")."'); window.history.go(-1);</script>\n";
			echo "<script>history.go(-1);</script>";
			return;
		}
		$user->userid=$id;
		$lists=array();
		
		
   		$user_sql = " (from_user = '".$id."' OR to_user = '".$id."' ) ";

		$sql = "SELECT m.*, m.comment as message,m.datesend as modified, u.username AS fromuser, p.username AS touser FROM #__ads_messages AS m".
		" LEFT JOIN #__users AS u ON m.from_user = u.id ".
		" LEFT JOIN #__users AS p ON m.to_user = p.id ".
		" WHERE  ".$user_sql. " ORDER BY m.datesend DESC";
		
		$db->setQuery($sql);
		$lists['messages'] = $db->loadObjectList();
	
		$query="SELECT count(*) AS nr_ads,max(start_date) AS last_ad_date,min(start_date) AS first_ad_date  FROM #__ads WHERE userid=$id";
		$db->setQuery($query);
		$res=$db->loadAssocList();

		$lists['nr_ads']=$res[0]['nr_ads'];
		$lists['last_ads_placed']=$res[0]['last_ad_date'];
		$lists['first_ads_placed']=$res[0]['first_ad_date'];
	
		$query="select * from #__ads_user_credits as c
					where userid = $id";
		$db->setQuery($query);
		$lists['credits'] = $db->loadObjectList();
		
		$Fi = & FactoryFieldsFront::getInstance();
    	$lists["user_fields"] = $Fi->getCustomFields("user_profile", $id);
    	
		JAdsAdminView::detailUser($user, $lists);
	}


// REPORT ADDS
	function reported_adds() {
		$db		    = JFactory::getDBO();
		$mainframe  = JFactory::getApplication();
		$where = array();

		$context			= 'com_adsman.jadsadminview.reported_adds';
		$filter_order		= $mainframe->getUserStateFromRequest( $context.'filter_order',		'filter_order',		'',	'cmd' );
		$filter_order_Dir	= $mainframe->getUserStateFromRequest( $context.'filter_order_Dir',	'filter_order_Dir',	'',	'word' );

        $filter_progress	= $mainframe->getUserStateFromRequest( $context.'filter_progress',	'filter_progress',	'',	'string' );
		/*$filter_solved		= $mainframe->getUserStateFromRequest( $context.'filter_solved',	'filter_solved',	'0',	'string' );*/
        $filter_status		= $mainframe->getUserStateFromRequest( $context.'filter_status',	'filter_status',	'',	'string' );

        $search				= $mainframe->getUserStateFromRequest( $context.'search',			'search',			'',	'string' );
		$search				= JString::strtolower($search);
		$limit				= $mainframe->getUserStateFromRequest('global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
		$limitstart			= $mainframe->getUserStateFromRequest($context.'limitstart', 'limitstart', 0, 'int');
		// In case limit has been changed, adjust limitstart accordingly
		$limitstart 		= ( $limit != 0 ? (floor($limitstart / $limit) * $limit) : 0 );

		$filter_order = 'b.title';
		$order = ' ORDER BY '. $filter_order .' '. $filter_order_Dir .'';

		if($search)
			$where[] = "b.title LIKE '%$search%' OR a.message LIKE '%$search%'";

        if(isset($filter_status) && $filter_status!=""){
			$where[] = "a.status = '$filter_status' ";
		}

        /*if (isset($filter_progress) && $filter_progress!=""){
            $where[] = "a.processing = '$filter_progress' ";
        }

        if(isset($filter_solved) && $filter_solved!=""){
			$where[] = "a.solved = '$filter_solved' ";
		}*/

		// Build the where clause of the content record query
		$where = (count($where) ? ' WHERE '.implode(' AND ', $where) : '');

		// Get the total number of records
		$query = "SELECT a.*, b.title, u.username ".
		         " FROM #__ads_report a ".
	    		 "LEFT JOIN #__ads AS b ON a.adid=b.id ".
	             "LEFT JOIN #__users AS u ON a.userid=u.id ".
				 $where;
		$db->setQuery($query);
		$total = $db->loadResult();

		// Create the pagination object
		jimport('joomla.html.pagination');
		$pagination = new JPagination($total, $limitstart, $limit);

		// Get the articles
		$query = "SELECT a.*, b.title, b.close_by_admin, u.username ".
		            " FROM #__ads_report a ".
	    		    "LEFT JOIN #__ads AS b ON a.adid=b.id ".
	                "LEFT JOIN #__users AS u ON a.userid=u.id ".
				    $where.$order;
		$db->setQuery($query, $pagination->limitstart, $pagination->limit);
		$rows = $db->loadObjectList();

		// table ordering
		$lists['order_Dir']	= $filter_order_Dir;
		$lists['order']		= $filter_order;

		// search filter
		$lists['search'] = $search;
		//$lists['progress'] = $filter_progress;
		//$lists['solved'] = $filter_solved;
        $lists['status'] = $filter_status;

		JAdsAdminView::report_ads($rows, $pagination, $lists);
	}

	function change_reported_status() {
		
		$mainframe = JFactory::getApplication();

		$cid = JRequest::getVar("cid",array());
		$database = JFactory::getDBO();

		if (count( $cid ) < 1) {
			echo "<script> alert('".JText::_("ADS_ERR_NO_SELECTED")."'); window.history.go(-1);</script>\n";
			exit;
		}
		$status=JRequest::getVar('status','');

        switch ($status){
            default:
            case "solved":
                $set=" status='solved' ";
                break;
            case "unsolved":
                $set=" status='unprocessed' ";
                //$set=" solved=0 ";
                break;
            case "processing":
                $set=" status='processing' ";
                //$set=" solved=0, processing=1 ";
                break;
            case "unprocessing":
                $set=" status='unprocessed' ";
                //$set=" processing=0 ";
                break;
            case "received":
                $set=" status='processing' ";
                //$set=" waiting=0 ";
                break;
            case "waiting":
                $set=" status='waiting' ";
                break;
        }

		/*switch ($status){
			default:
			case "solved":
			$set=" solved=1, processing=0, waiting=0 ";
			break;
			case "unsolved":
			$set=" solved=0 ";
			break;
			case "processing":
			$set=" solved=0, processing=1 ";
			break;
			case "unprocessing":
			$set=" processing=0 ";
			break;
            case "received":
            $set=" waiting=0 ";
            break;
            case "waiting":
            $set=" waiting=1 ";
            break;
		}*/

		$cids = implode( ',', $cid );

		$query = "UPDATE #__ads_report"
		. "\n SET $set "
		. "\n WHERE adid IN ( $cids )"
		;
		$database->setQuery( $query );
		if (!$database->query()) {
			echo "<script> alert('".$database->getErrorMsg()."'); window.history.go(-1); </script>\n";
			exit();
		}
		$mainframe->redirect( JURI::root().'administrator/index.php?option=com_adsman&task=reported_adds',JText::_("ADS_STATUS_CHANGED") );
	}

// MESSAGING
	function write_admin_message(){
		require_once (JPATH_ADMINISTRATOR.DS.'components'.DS.'com_adsman'.DS.'tables'.DS.'addsman.php');
		$db = JFactory::getDBO();

		$id = JRequest::getVar('adid',0);
		$id_reported = JRequest::getVar('id_reported',0);

		if ($id){
			$obj = new JAdsAddsman($db);
			if (!$obj->load($id)) return;
		} elseif($id_reported){
			$obj = new JAdsAddsman($db);
			if (!$obj->load($id_reported)) return;
		} else {
			return;
		}
		$usr = JTable::getInstance("user");
		$usr->load($obj->userid);
		$obj->username = $usr->username;

		JAdsAdminView::writeAddMessage($obj);
	}

	function sendaddmessage(){

		$mainframe = JFactory::getApplication();
		$database =  JFactory::getDBO();
		$my =  JFactory::getUser();

		require_once (JPATH_ADMINISTRATOR.DS.'components'.DS.'com_adsman'.DS.'tables'.DS.'addsman.php');
		$id = JRequest::getVar('adid',0);
		$message = JRequest::getVar('message','');

		if (!$message){
			echo JText::_("ADS_ERR_MESSAGE_EMPTY");
			return;
		}

		$add = new JAdsAddsman($database);
		if (!$add->load($id)) return;

		$add->newmessages = 1;
		$add->store();
		$add->sendNewMessage($id,null,$message);

		$owner_user=JTable::getInstance("user");
		$owner_user->load($add->userid);

		$add->SendMails(array($owner_user),'ads_admin_message');

        //set status waiting
        $db		= JFactory::getDbo();
        $query = 'UPDATE #__ads_report' .
                ' SET status = "waiting" '.
                ' WHERE adid ='. $id .' ';
        $db->setQuery($query);

        if (!$db->query()) {
            JError::raiseError( 500, $db->getErrorMsg() );
            return false;
        }

        $msg = JText::sprintf('ADS_ITEMS_SET_WAITING');


        $mainframe->redirect('index.php?option=com_adsman&task=edit&cid[]='.$id,JText::_("ADS_MESSAGE_SENT"). ' '. $msg);
	}


	function delmessage(){
		$db		= JFactory::getDBO();
		$cid	= JRequest::getVar( 'id', null );
		$mid	= JRequest::getVar( 'mid', null );
		require_once (JPATH_ADMINISTRATOR.DS.'components'.DS.'com_adsman'.DS.'tables'.DS.'addsman.php');
		require_once (JPATH_BASE.DS."..".DS.'components'.DS.'com_adsman'.DS.'helpers'.DS.'helper.php');
		$m = new JAdsMessages($db);
	    $m->load($mid);
	    $m->delete($mid);
		$this->setRedirect( "index.php?option=com_adsman&task=edit&cid[]={$cid}",JText::_("ADD_DELETED") );
	}
	

    function assignAdUser(){
        $db		= JFactory::getDBO();
        $app	= JFactory::getApplication();
        $where = array();

        $context			= 'com_adsman.adsadminview.assignAdUser';
        $filter_order		= $app->getUserStateFromRequest( $context.'filter_order',		'filter_order',		'',	'cmd' );
        $filter_order_Dir	= $app->getUserStateFromRequest( $context.'filter_order_Dir',	'filter_order_Dir',	'ASC',	'word' );
        $search				= $app->getUserStateFromRequest( $context.'search',			'search',			'',	'string' );
        $search				= JString::strtolower($search);

        $limit				= $app->getUserStateFromRequest('global.list.limit', 'limit', $app->getCfg('list_limit'), 'int');
        $limitstart			= $app->getUserStateFromRequest($context.'limitstart', 'limitstart', 0, 'int');
        // In case limit has been changed, adjust limitstart accordingly
        $limitstart 		= ( $limit != 0 ? (floor($limitstart / $limit) * $limit) : 0 );

        if (!$filter_order) {
            $filter_order = 'name';
        }
        $order = ' ORDER BY '. $filter_order .' '. $filter_order_Dir .'';

        if($search)
            $where[] = " username LIKE '%$search%' ";

        // Build the where clause of the content record query
        $where = (count($where) ? ' WHERE '.implode(' AND ', $where) : '');

        // Get the total number of records
        $query = "SELECT COUNT(*) ".
            "FROM #__users"
            .$where;
        $db->setQuery($query);
        $total = $db->loadResult();

        // Create the pagination object
        jimport('joomla.html.pagination');
        $pagination = new JPagination($total, $limitstart, $limit);

        // Get the users
        $query = "SELECT b.id as profid, u.id as userid1, u.username as username,u.name as name, ".
                " b.id as usrid, b.status".
                " FROM #__users AS u ".
                " LEFT JOIN #__ads_users AS b ON u.id=b.userid ".
                " LEFT JOIN #__ads AS aads ON u.id= aads.userid  ".
                $where.
                "GROUP BY u.id ".$order;

        $db->setQuery($query, $limitstart, $limit);
        $rows = $db->loadObjectList();

        $lists["order"] = $filter_order;
        $lists["order_Dir"] = $filter_order_Dir;
        $lists['search'] = $search;

        JAdsAdminView::assignAdUser($rows, $pagination, $lists);
    }
	
	
/**
 * BETA
 * dumps categories in a quick addable form
 *
 * @return true
 */
	function exportCategories_PEAR(){
		
	    $cat=JTheFactoryCategory::getInstance(APP_CATEGORY_TABLE,APP_CATEGORY_DEPTH);
	    $cats = $cat->build_child(0,false);
        
	    $rows = array();
	    
	    $jj = 0;
		if($cats)
        foreach($cats as $i => $cat)
        {
       		$offset = "";
        	if($cat["depth"]>0 ) {
				$offset = str_repeat("  ",$cat["depth"]);
        	}
			$rows[$jj][0] = $offset.$cat["catname"];
			if ( $cat["description"]!="" )
				$rows[$jj][1] = $cat["description"];
			else 
				$rows[$jj][1] = "-";
			$jj++;
        }
		
        $filename = "ExportXLS_Categories";
		header("Content-type: application/vnd.ms-excel");
	    header("Content-Disposition: attachment; filename=\"".$filename.".xls\"");
	    header("Expires: 0");
	    header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
		header("Pragma: public");
		ob_clean();
		
	    echo JTheFactoryXLSCreator::createXLS($rows,null,"Categories");
	    ob_end_flush();
	    exit;
		return true;
	}
	
	   ###############################################################
## This function will generate a file in XLS Format
	function exportCategories(){
     
      $mainframe = JFactory::getApplication();
      $db    = JFactory::getDBO();
   
   	  $cat=JTheFactoryCategory::getInstance(APP_CATEGORY_TABLE,APP_CATEGORY_DEPTH);
	  $cats = $cat->build_child(0,false);
        
	  $rows = array();
	   
	  $jj = 0;
	  if($cats)
        foreach($cats as $i => $cat)
        {
       		$offset = "";
        	if($cat["depth"]>0 ) {
				$offset = str_repeat("  ",$cat["depth"]);
        	}
			$rows[$jj][0] = $offset.$cat["catname"];
			if ( $cat["description"]!="" )
				$rows[$jj][1] = $cat["description"];
			else 
				$rows[$jj][1] = "-";
			$jj++;
        }
 
      ## Empty data vars
      $data = "" ;
      ## We need tabbed data
      $sep = "\t"; 
      
      ## Count all fields(will be the collumns
      $columns = 2;

      ## Put the name of all fields to $out.  
      $data .= 'Categories';
      
      $data .= "\n";
     
      ## Counting rows and push them into a for loop
      for($k=0; $k < count( $rows ); $k++) {
         $row = $rows[$k];
         $line = '';
         
         ## Now replace several things for MS Excel
         $value = str_replace('"', '""', $row[0]);
         $line .= '"' . $row[0] . '"' . $sep;
         
         $data .= trim($line)."\n";
   	  }

      $data = str_replace("\r","",$data);

      ## If count rows is nothing show o records.
      if (count( $rows ) == 0) {
        $data .= "\n(0) Records Found!\n";
      }
      
      $filename = "ExportXLS_Categories";
      header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=\"".$filename.".xls\"");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Pragma: public");

        print $data ;
		//ob_clean();
      
        ob_end_flush();
	    exit;
		return true;   

   }

	function ajaxPositionSelect() {
		
		global $database;
		ob_end_clean();
		
		$fid = &FactoryLayer::getRequest($_REQUEST,"fid",0);
			
		$database->setQuery(
		"
		SELECT a. * , fp.fid 
		FROM #__".APP_CFIELDS_PREFIX."_positions a
		LEFT JOIN #__".APP_CFIELDS_PREFIX."_fields_positions fp ON fp.position = a.id 
		AND fp.fid = '{$fid}'
		WHERE fp.fid IS NULL
		");
		
		$positions = $database->loadAssocList();
		
		$positionsOpts = array();
		
		if (count($positions)>0)
			foreach ($positions as $o => $pos ) 
			{
				$positionsOpts[$pos["id"]] = $pos["title"]."  (".$pos["tpl"].")";
			}
    	
    	echo FactoryHTML::selectGenericList($positionsOpts, "position", null);
    	
	}
    function installtemplates()
    {
        jimport('joomla.filesystem.folder');
        if (JFolder::exists(JPATH_SITE . DS . 'components' . DS . 'com_adsman' . DS . 'templates-dist')) {
            JFolder::copy(JPATH_SITE . DS . 'components' . DS . 'com_adsman' . DS . 'templates-dist', JPATH_SITE . DS . 'components' . DS . 'com_adsman' . DS . 'templates', '', true);
            $message = JText::_('ADS_TEMPLATES_OVERWRITTEN');
        } else {
            $message = JText::_('ADS_NEW_TEMPLATES_NOT_FOUND_PLEASE_CHECK_INSTALLATION_KIT');
        }
        $this->setRedirect('index.php?option=com_adsman&task=settingsmanager', $message);
        
    }

}

/**
 * To Factorize
 *
 */
class Ads_Options {
    var $_options_array=array(
        'ads_opt_max_picture_size'=>'ads_opt_max_picture_size',
        'ads_opt_date_format'=>'ads_opt_date_format',
        'ads_opt_date_time_format'=>'ads_opt_date_time_format',
        'thumb_resize_width'=>'ads_opt_thumb_width',
        'thumb_resize_height'=>'ads_opt_thumb_height',
        'medium_resize_width'=>'ads_opt_medium_width',
        'medium_resize_height'=>'ads_opt_medium_height',
        'max_nr_months'=>'ads_opt_availability',
        'max_nr_img'=>'ads_opt_maxnr_images',
        'allow_messages'=>'ads_opt_allow_messages',
        'arh'=>'ads_opt_archive',
        'items_per_page'=>'ads_opt_nr_items_per_page',
        'enable_hour'=>'ads_opt_enable_hour',
        'enable_countdown'=>'ads_opt_enable_countdown',
        'rss_title'=>'ads_opt_RSS_title',
        'rss_description'=>'ads_opt_RSS_description',
        'rss_nritems'=>'ads_opt_RSS_nritems',
        'rss_feedtype'=>'ads_opt_RSS_feedtype',
        'max_nr_tags'=>'ads_opt_max_nr_tags',
        'allowpaypal'=>'ads_opt_allowpaypal',
        'require_image'=>'ads_opt_require_picture',
        'ads_opt_resize_if_larger'=>'ads_opt_resize_if_larger',
        'ads_price_compulsory'=>'ads_price_compulsory',
        'googlemapkey'=>'ads_opt_google_key',
        'ads_map_in_ads_details'=>'ads_map_in_ads_details',
        'ads_allow_atachment'=>'ads_allow_atachment',
        'ads_require_atachment'=>'ads_require_atachment',
        'ads_short_desc_long'=>'ads_short_desc_long',
        'allow_messaging'=>'ads_opt_allow_messaging',
        'global_limit'=>'ads_opt_global_limit',
        'extend_days'=>'ads_opt_extend_days',
        'enable_category_page'=>'ads_opt_category_page',
        'ads_opt_allow_guest_messaging'=>'ads_opt_allow_guest_messaging',
        'ads_opt_enable_captcha'=>'ads_opt_enable_captcha',
        'ads_opt_inner_categories'=>'ads_opt_inner_categories',
        'ads_opt_transliterate'=>'ads_opt_transliterate',

			'gallery_plugin'=>'ads_opt_gallery',
			'ads_opt_googlemap_defx'=>'ads_opt_googlemap_defx',
			'ads_opt_googlemap_defy'=>'ads_opt_googlemap_defy',
			'ads_opt_googlemap_allowed_maps'=>'ads_opt_googlemap_allowed_maps',
			'ads_opt_googlemap_default_zoom'=>'ads_opt_googlemap_default_zoom',
			'ads_opt_googlemap_type'=>'ads_opt_googlemap_type',
			'ads_opt_googlemap_gx'=>'ads_opt_googlemap_gx',
			'ads_opt_googlemap_gy'=>'ads_opt_googlemap_gy',
			'ads_opt_recaptcha_public_key'=>'ads_opt_recaptcha_public_key',
			'ads_opt_recaptcha_private_key'=>'ads_opt_recaptcha_private_key',
			'ads_opt_mailcaptcha_public_key'=>'ads_opt_mailcaptcha_public_key',
			'ads_opt_mailcaptcha_private_key'=>'ads_opt_mailcaptcha_private_key',
			'ads_opt_enable_antispam_bot'=>'ads_opt_enable_antispam_bot',
			'ads_opt_recaptcha_theme'=>'ads_opt_recaptcha_theme',
			'ads_opt_choose_antispam_bot'=>'ads_opt_choose_antispam_bot',
			'ads_opt_price_enable'=>'ads_opt_price_enable',
			'ads_opt_adtype_enable'=>'ads_opt_adtype_enable',
			'ads_opt_adtype_val'=>'ads_opt_adtype_val',
			'ads_opt_adpublish_enable'=>'ads_opt_adpublish_enable',
			'ads_opt_adpublish_val'=>'ads_opt_adpublish_val',
			'ADDSMAN_TPL_THEME'=>'ADDSMAN_TPL_THEME',
			'ads_opt_logged_posting' => 'ads_opt_logged_posting',
			'ads_opt_googlemap_distance' => 'ads_opt_googlemap_distance',
			'ads_opt_googlemap_unit_available' => 'ads_opt_googlemap_unit_available',
			'ads_opt_profile_mode' => 'ads_opt_profile_mode',
			'ads_opt_profile_required' => 'ads_opt_profile_required',
			'ads_opt_messages_public' => 'ads_opt_messages_public',
			'ads_opt_category_root_post' => 'ads_opt_category_root_post',
			'ads_opt_category_ajax_load' => 'ads_opt_category_ajax_load',
			'ads_opt_post_editor_buttons' => 'ads_opt_post_editor_buttons',
			'ads_opt_component_offset' => 'ads_opt_component_offset',
			'ads_opt_choose_checkout' => 'ads_opt_choose_checkout',
            'ads_opt_sort_date' => 'ads_opt_sort_date'
        );
        
	var $_raw_options=array('ads_opt_googlemap_allowed_maps');
	var $_checkboxes=array(
							'ads_opt_resize_if_larger','ads_opt_require_picture','ads_opt_enable_countdown',
							'ads_opt_enable_hour','ads_price_compulsory',
							'ads_map_in_ads_details','ads_allow_atachment',
							'ads_opt_allow_messaging','ads_opt_category_page','ads_require_atachment',
							'ads_opt_profile_required'
							);
	var $_db=null;
	
	
	function Ads_Options(&$database){
		$this->_db=$database;
	}

	function saveOptions() {
		$out_file = ADS_COMPONENT_PATH.DS.'options.php';
		
		$f = fopen($out_file,'w');
		if (!$f) return false;
				
		fputs($f,"<?php\n");
		  foreach ($this->_options_array as $var => $optstring) {
		  	
			$defaultval=defined($optstring) ? constant($optstring) : '';
			
            if (in_array($optstring,$this->_checkboxes))  $defaultval='0';

            if (in_array($var,$this->_raw_options)){
            	
            	$v = JRequest::getVar($var, $defaultval, 'default', 'array');
            	if(count($v))
            		$v = implode(",",$v);
            	else
            		$v = "";
            }
            else {
                if ($var != 'gallery_plugin') {
                   $v = JRequest::getVar($var,$defaultval);
                } else {
                    $v = $defaultval;
                }
            }
            $v=addslashes($v);
            fputs($f,"define('$optstring','$v');\n");

		  }
        fputs($f,"\n?>");
       	fclose($f);
    	
 		$c = JRequest::getVar('currency','EUR','post','string');
    	$this->SaveArrayInTable('#__ads_currency','name',$c);

		return true;
    }

	function changeSetting($option,$value){
		
		$out_file = ADS_COMPONENT_PATH.DS.'options.php';
		$f = fopen($out_file,'w');

		if (!$f) return false;
		
		fputs($f,"<?php\n");
		foreach ($this->_options_array as $var => $optstring){

            if($optstring == $option){
                $v = $value;
            } else {

                if ($option == 'ads_opt_profile_mode' && $optstring == 'ads_opt_profile_required') {
                    $v = JRequest::getVar('ads_opt_profile_required',0);
                }
                 else {
                    $v = constant($optstring);
                }
            }

                fputs($f,"define('$optstring','$v');\n");
            }

        fputs($f,"\n?>");
		fclose($f);
		
		return true;
	}
    
   function SaveArrayInTable($tablename,$fieldname,$values) {
   	
       $values = rtrim($values,'~');
       $valarr=explode('~',$values);


    	if (count($valarr)>0)
    	{
    		$this->_db->setQuery("select * from $tablename");
    		$oldvals=$this->_db->LoadObjectList();

    		$this->_db->setQuery("delete from $tablename");
    		$this->_db->query();

    		foreach ($valarr as $curr){
    			
    			if (!empty($curr)){
    			    
    				$oldid='null';
    			    for ($i=0; $i<count($oldvals); $i++)
    			     if ( strtoupper($oldvals[$i]->$fieldname) == strtoupper($curr) ) {
    			         $oldid="'".$oldvals[$i]->id."'";
    			         break;
    			     }

    				$this->_db->setQuery("insert into $tablename  set id=$oldid, $fieldname='$curr'");
    				$this->_db->query();
    			}
    		}
    	} else {
    		$this->_db->setQuery("delete from $tablename");
    		$this->_db->query();
    	}

    }
    
   function saveLimitsArray($limit,$limit_owners,$type) {
   		
	$app = JFactory::getApplication();
	
	  if (!empty($limit_owners)) {
	
    	$db = & JFactory::getDBO();
    	$_sql = " INSERT INTO #__ads_limits (owner, limit_size, limit_type) VALUES  ";
    	
    	$rows = array();
    	foreach ($limit_owners as $k => $o){
    		$own = $o;
    		$rows[] = "($own,$limit, '$type')";
    	}
    	$_sql.= implode(",",$rows);
    	$db->setQuery($_sql);
    	$db->query();
    	//echo $_sql;exit;
    
	  } else {
			JError::raiseWarning(0,JText::_("ADS_USERS_LIMIT").$limit);
			$app->redirect( "index.php?option=com_adsman&task=settings",JText::_("ADS_LIMIT_NO_USERS_ADDED") );
	  }
	}
	
    
   function getLimitGroups(){
    	$db = & JFactory::getDBO();
    	$_sql = "SELECT ag.id, ag.title, li.limit_size FROM #__ads_limits as li LEFT JOIN #__usergroups AS ag ON li.owner = ag.id WHERE limit_type='group' ";
    	
    	$db->setQuery($_sql);
    	$list = $db->loadObjectList();
    	//echo $db->_sql;exit;
    	
    	return $list;
    }
    
   function getLimitUsers(){
    	$db = & JFactory::getDBO();
    	$_sql = "
    	SELECT ag.id,ag.name,li.limit_size 
    	FROM #__ads_limits as li 
    	LEFT JOIN #__users AS ag ON li.owner = ag.id 
    	WHERE limit_type = 'user'
    	";
    	$db->setQuery($_sql);
    	$list = $db->loadObjectList();
    	//echo $db->_sql;exit;
    	return $list;
    }
}

?>
