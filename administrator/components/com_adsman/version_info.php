<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class JTheFactoryAbout extends JObject{
	var $latestversion=null;
	var $newdownloadlink=null;
	var $newsletter=null;
	var $announcements=null;
	var $releasenotes=null;
	var $_componentname=null;

	function __construct()
    {
 		$this->_componentname=COMPONENT_NAME;
		return $this->GetInfo();

    }
	function &getInstance()
	{
		static $instances;

		if (!isset( $instances ))
			$instances = new JTheFactoryAbout();

		return $instances;
	}
 	function remote_read_url( $uri ) {

        if ( function_exists('curl_init') ){
            $handle = curl_init();

            curl_setopt ($handle, CURLOPT_URL, $uri);
            curl_setopt ($handle, CURLOPT_MAXREDIRS,5);
            curl_setopt ($handle, CURLOPT_AUTOREFERER, 1);
            curl_setopt ($handle, CURLOPT_CONNECTTIMEOUT, 10);
            curl_setopt ($handle, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt ($handle, CURLOPT_TIMEOUT, 100);
            $buffer = curl_exec($handle);

            curl_close($handle);
            if ( $buffer || !ini_get('allow_url_fopen') ){
		return $buffer;
            }
        }
	if ( ini_get('allow_url_fopen') ) {
            $fp = @fopen( $uri, 'r' );
            if ( !$fp )
                return false;
         	stream_set_timeout($fp, 20);
            $linea = '';
            while( $remote_read = fread($fp, 4096) )
                $linea .= $remote_read;
       		$info = stream_get_meta_data($fp);
            fclose($fp);
        	if ($info['timed_out'])
        	   return false;
            return $linea;
        } else {
           return false;
        }
    }

	function GetInfo()
	{
		jimport('joomla.utilities.simplexml');
		@set_time_limit(60);


		$fileURL = COMPONENT_HOME_PAGE.'versions/com_adsman.xml';

		$fileContents = $this->remote_read_url($fileURL);
		
		$xmlparser = new JSimpleXML;

		if (!$xmlparser->loadString($fileContents))
		{
			return false;
		}
		$xmldoc = $xmlparser->document;

		$element = $xmldoc->getElementByPath('/latestversion');
		$this->latestversion = $element ? $element->data() : '';
		$element = $xmldoc->getElementByPath('/newdownloadlink');
		$this->newdownloadlink =$element ? $element->data() : '';
		$element = $xmldoc->getElementByPath('/newsletter');
		$this->newsletter = $element ? $element->data() : '';
		$element = $xmldoc->getElementByPath('/announcements');
		$this->announcements = $element ? $element->data() : '';
		$element = $xmldoc->getElementByPath('/notes');
		$this->releasenotes = $element ? $element->data() : '';

		return true;

	}
	function showAbout()
	{
    	$ver1=explode('.',COMPONENT_VERSION);
    	$ver2=explode('.',$this->latestversion);

    	$isNew=false;
    	$ver_info= JText::_('ADS_VERSION_IS_UP_TO_DATE');

    	$n = count($ver1);
    	for($i=0; $i < $n; $i++)
		{
    	    if (intval($ver1[$i])<intval($ver2[$i]))
			{				
		        $isNew=true;
	    	    $ver_info= JText::_('ADS_NEW_VERSION_AVAILABLE');
		        break;
	    	}
    	    if (intval($ver1[$i])>intval($ver2[$i]))
			{
    	        $isNew=false;
          	    break;
    	    }
    	}
    	
    	switch ($isNew) {
    		case true: ?>
    			<div id="info_ver">
		    	    <span style="color:red"><?php echo $ver_info; ?> </span><br/>
		    	</div>
		    	<?php break;
		    case false: ?>
		    	<div id="info_ver">
		    	    <span style="color:green"><?php echo $ver_info; ?> </span><br/>
		    	</div>	
    	<?php } ?>

    	<div id="info_div" style="border:1px solid black;width:350px;margin-top:20px;">
            <?php echo JText::_('ADS_INSTALLED_VERSION'). COMPONENT_VERSION; ?><br/>
            <?php echo JText::_('ADS_LATEST_VERSION_AVAILABLE'). $this->latestversion; ?><br/>
    	</div>
    	<?php
    	if ($isNew)
    	{?>
    	    <div id="download_div" style="border:1px solid black;width:350px;margin-top:20px;">
    	    <?php echo $this->newdownloadlink; ?>
    	    </div>
    	<?php
    	}
    	if ($this->newsletter)
    	{?>
    	    <div style="margin-top:20px;">
                <?php echo JText::_('ADS_NEWSLETTER_SUBSCRIBE');?>
    	        <a href="<?php echo $this->newsletter; ?>" target="_blank"><?php echo JText::_('ADS_LINK');?></a>
    	    </div>
    	<?php
    	}
    	if ($this->announcements)
    	{?>
    	    <div style="margin-top:20px;">
                <?php echo JText::_('ADS_LATEST_ANNOUNCEMENTS');?>
    	        <?php echo $this->announcements; ?>
    	    </div>
    	<?php
    	}
    	if ($this->releasenotes)
    	{?>
    		<div style="margin-top:20px;">
                <?php echo JText::_('ADS_LATEST_RELEASE_NOTES');?>
    	        <?php echo $this->releasenotes; ?>
    	    </div>
    	<?php
    	}
    	?>
    	<form action="index.php" method="post" name="adminForm">
    	<input type="hidden" name="option" value="com_adsman" />
    	<input type="hidden" name="task" value="" />
    	</form>
    	<?php

	}

}
?>