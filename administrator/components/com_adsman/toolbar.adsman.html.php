<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/



/**
* @package		Joomla
* @subpackage	Categories
*/
class TOOLBAR_adsman {
	
	function _NONE($title="",$img='cpanel.png' ){
		$task = &JRequest::getCmd("task");

		/*if ($title) {
			//$title = ': <small><small>[ '. $title.' ]</small></small>';
			$title =  "ADSMAN_ADS $title";
		}*/
		
		JToolBarHelper::title( JText::_('ADSMAN_ADS') . JText::_($title), 'cpanel.png' );
        //JToolBarHelper::title( JText::_($title), 'adsdashboard.jpg' );
		//JToolBarHelper::divider();
		//if( $task!="" && $task!="settingsmanager" )
		//	TOOLBAR_adsman::_SETTINGS_SUBMENU();
	}

    function _PAYMENTITEMS($title="",$img='cpanel.png' ){
        $task = &JRequest::getCmd("task");

        if ($title) {
            //$title = ': <small><small>[ '. $title.' ]</small></small>';
            $title =  'Ads '.$title;
        }

        //JToolBarHelper::title( JText::_($title), 'cpanel.png' );
        JToolBarHelper::title( JText::_($title), $img);
        JToolBarHelper::divider();
        TOOLBAR_adsman::_MENUHelper();
        //if( $task!="" && $task!="settingsmanager" )
          //  TOOLBAR_adsman::_SETTINGS_SUBMENU();
    }

	/**
	* Draws the menu for Editing an existing add
	* @param int The published state (to display the inverse button)
	*/
	function _EDIT()
	{
		$cid = JRequest::getVar( 'cid', array(0), '', 'array' );
        $id = $cid[0];
        $text = ($id != 0) ? JText::_("ADS_EDIT") : JText::_("ADS_NEW_AD");

		//JToolBarHelper::title( JText::_( 'Ads ' ) .': <small><small>[ '. $text.' ]</small></small>', 'adscategories.png' );
        JToolBarHelper::title( JText::_('ADSMAN_ADS') . $text, 'adscategories.png' );
        ($id != 0) ? JToolBarHelper::apply('edit.apply') : '';
		JToolBarHelper::save();

		JToolBarHelper::custom( 'unblock', 'apply.png', 'apply_f2.png', 'Unblock', false );
		JToolBarHelper::custom( 'block', 'cancel.png', 'cancel_f2.png', 'Block', false );
		JToolBarHelper::back();
		
	}
	
	function _PAYMENTS()
	{
		$cid = JRequest::getVar( 'cid', array(0), '', 'array' );
		$text = JText::_('ADS_RECEIVED_PAYMENTS_MANAGER');

        JToolBarHelper::title( JText::_('ADSMAN_ADS') . $text, 'payments.png' );

        JToolBarHelper::custom('new_payment','save.png','f_save.png','New Payment',false);
        JToolBarHelper::custom( 'approve_payment', 'apply.png', 'apply_f2.png', 'Approve payment', true );
        JToolBarHelper::deleteList(JText::_("ADS_DELETE"),'deletePaylog');
        JToolBarHelper::divider();

        $bar = JToolBar::getInstance('toolbar');
        $bar->appendButton( 'Link', 'preview', JTF_PAYMENT_PRICINGS_TITLE, "index.php?option=".APP_EXTENSION."&task=paymentitems",false );
        $bar->appendButton( 'Link', 'menus', JTF_PAYMENT_GATEWAYS_TITLE, "index.php?option=".APP_EXTENSION."&task=gateway_administrator",false );

	}
	
    function _NEWPAYMENT()
    {
		$cid = JRequest::getVar( 'cid', array(0), '', 'array' );
		$text = JText::_('ADS_PAYMENT');
		JToolBarHelper::title( JText::_( 'ADSMAN_ADS' ) . $text, 'adscategories.png' );
		JToolBarHelper::custom('save_payment','save.png','f_save.png','Save Payment',false);
		JToolBarHelper::custom('paymentitems','back.png','f_back.png','Back',false);
    }

	/**
	* Menu for settings
	*/
	function _SETTINGS($save_task = 'savesettings') {

		$task = &JRequest::getCmd("task");
		$title = "";
		switch ($task){
			case "settings":
				$title = JText::_( "ADS_MISC_SETTINGS");
				break;
			case "mailsettings":
				$title = JText::_( "MAIL_SETTINGS");
               // $img = "mail_manager.png";
				break;
		}
		//if($title)
		//	$title = ": <small><small>[ ".$title." ]</small></small>";
		
		JToolBarHelper::title( JText::_('ADSMAN_ADS').$title  , 'cpanel.png' );
		TOOLBAR_adsman::_customBUTTON("Standard","save.png","Save", $save_task , null,null );
		JToolBarHelper::divider();
        TOOLBAR_adsman::_MENUHelper();
		//TOOLBAR_adsman::_SETTINGS_SUBMENU();
		
	}
	
	function _IMPORT() {
		
		TOOLBAR_adsman::_customBUTTON("Standard","save.png","Upload", "importcsv" , null,null );
		JToolBarHelper::title( JText::_( 'ADS_IMPORT_SETTINGS' ) , 'cpanel.png' );
		JToolBarHelper::divider();
		//TOOLBAR_adsman::_SETTINGS_SUBMENU();
	}
	/**
	 * Menu with only one Back item
	*/
	function _SIMPLE($title="",$img='cpanel.png') {

		JToolBarHelper::title( JText::_( $title ) , $img );
		$bar = & JToolBar::getInstance('toolbar');
		// Add an apply button
		$bar->appendButton( 'link', 'menus', "Ads List", "index.php?option=com_adsman&task=listadds", false, false );
		//TOOLBAR_adsman::_SETTINGS_SUBMENU();
	}

	/**
	 * Menu with categories list
	*/
	function _CATEGORIES() {

		JToolBarHelper::title(  JText::_( 'ADS_TOOLBAR_CATEGORIES' ) , 'adscategories.png' );

		JToolBarHelper::custom( 'editCat', 'new.png', 'new_f2.png', 'New category', false );
		JToolBarHelper::custom( 'editCat', 'edit.png', 'edit_f2.png', 'Edit categories', true );
		JToolBarHelper::custom( 'showMoveCategories', 'move.png', 'move_f2.png', 'Move categories', true );
		JToolBarHelper::deleteList("","delcategories");
        JToolBarHelper::divider();
        TOOLBAR_adsman::_MENUHelper();
        //TOOLBAR_adsman::_SETTINGS_SUBMENU();
	}
	
	/**
	 * Menu with categories list
	*/
	function _CATEGORIES_EDIT() {

		JToolBarHelper::title(  JText::_( 'ADS_TOOLBAR_CATEGORY_EDIT' ) , 'adscategories.png' );
		JToolBarHelper::save( "savecat" );
		JToolBarHelper::back();
	}
	
	function _REPORTS()
	{
		$cid = JRequest::getVar( 'cid', array(0), '', 'array' );
		$text = "Reported ";
        JToolBarHelper::title( JText::_( 'ADSMAN_ADS' ) . $text, 'reported.png' );

		//JToolBarHelper::custom( 'processing', 'preview.png', 'preview_f2.png', 'Mark in progress', true );
		//JToolBarHelper::custom( 'solved', 'apply.png', 'apply_f2.png', 'Mark as solved', true );
        JToolBarHelper::custom( 'block', 'cancel.png', 'cancel_f2.png', 'Block', true );
        //JToolBarHelper::custom( 'write_admin_message', 'send.png', 'send.png', 'Write a message', true );
	}
	
	function _LIST()
	{
		
		JToolBarHelper::title( JText::_( 'ADS_LIST' ), 'listads.png' );

        JToolBarHelper::addNewX('newAd', 'JTOOLBAR_NEW');
		JToolBarHelper::editListX();
		JToolBarHelper::publishList();
		JToolBarHelper::unpublishList();
		JToolBarHelper::custom( 'unblock', 'apply.png', 'apply_f2.png', 'Unblock', true );
        JToolBarHelper::custom( 'block', 'cancel.png', 'cancel_f2.png', 'Block', true );
		JToolBarHelper::deleteList();
		
	}
	
	function _USERS()
	{
		JToolBarHelper::title( JText::_( 'ADS_USERS' ), 'user.png' );
		JToolBarHelper::custom( 'unblockuser', 'apply.png', 'apply_f2.png', 'Unblock', true );
		JToolBarHelper::custom( 'blockuser', 'cancel.png', 'cancel_f2.png', 'Block', true );
	}
	
	function _MESSAGE() {

		JToolBarHelper::title( JText::_( 'ADS_SEND_MESSAGE' ), 'inbox.png' );
		JToolBarHelper::custom( 'sendaddmessage', 'send.png', 'send_f2.png', 'Send Message', false );
		JToolBarHelper::back();
	}
	
	function _customBUTTON($type="Standard",$icon,$alt,$task, $listSelect, $x){
		$bar = & JToolBar::getInstance('toolbar');

		//strip extension
		$icon	= preg_replace('#\.[^.]*$#', '', $icon);

		// Add a chosend type button
		$bar->appendButton( $type, $icon, $alt, $task, $listSelect, $x );
	}
		
	
	function _SETTINGS_SUBMENU(){
		
		$bar = & JToolBar::getInstance('toolbar');
		// Add an apply button
		$bar->appendButton( 'link', 'options', "Settings", "index.php?option=com_adsman&task=settingsmanager", false, false );
        //JToolBarHelper::divider();
        //$bar->appendButton( 'link', 'adsdashboard', "Dashboard", "index.php?option=com_adsman", false, false );
		TOOLBAR_adsman::_SUBMENUHelper();

	}
	
	function _SUBMENUHelper(){
		$task = &JRequest::getCmd("task");
		$menu = &JToolBar::getInstance('submenu');

		$menu->appendButton(JText::_("ADS_MISC_SETTINGS"), "index.php?option=com_adsman&amp;task=settings",($task=="settings")?1:0);
		$menu->appendButton(JText::_("ADS_PROFILE_INTEGRATION"), "index.php?option=com_adsman&amp;task=integration",($task=="integration")?1:0);
        $menu->appendButton(JText::_("ADS_CUSTOM_FIELDS"), "index.php?option=com_adsman&amp;task=show_customfields_config",(($task == 'show_customfields_config')||($task=="field_administrator")||($task=="fposition_panel") || ($task=="fposition_fields")|| ($task=="fposition_templates"))?1:0);
        $menu->appendButton(JText::_("ADS_MENU_CATEGORIES"), "index.php?option=com_adsman&amp;task=categories",($task=="categories")?1:0);
        $menu->appendButton(JText::_("PAYMENT_ITEMS"), "index.php?option=com_adsman&amp;task=paymentitems",($task=="paymentitems" || $task=="paymentitemsconfig")?1:0);
		$menu->appendButton(JText::_("ADS_RECEIVED_PAYMENTS_GATEWAYS"), "index.php?option=com_adsman&amp;task=gateway_administrator",($task=="gateway_administrator")?1:0);
        $menu->appendButton(JText::_("ADS_THEMES"), "index.php?option=com_adsman&amp;task=themes_administrator",($task=="themes_administrator")?1:0);
   		$menu->appendButton(JText::_("ADS_GALLERY"), "index.php?option=com_adsman&amp;task=gallery_administrator",( $task=="gallery_administrator")?1:0);
		$menu->appendButton(JText::_("MAIL_SETTINGS"), "index.php?option=com_adsman&amp;task=mailsettings",($task=="mailsettings")?1:0);
		$menu->appendButton(JText::_("ADS_COUNTRY_MANAGER"), "index.php?option=com_adsman&amp;task=countries",($task=="countries")?1:0);
		$menu->appendButton(JText::_("ADS_IMPORT_EXPORT"), "index.php?option=com_adsman&amp;task=showadsconfig",($task=="showadsconfig")?1:0);
	}

    function _MENUHelper(){
        $task = &JRequest::getCmd("task");
        $menu = &JToolBar::getInstance('submenu');

        $menu->appendButton("Ads List", "index.php?option=com_adsman&amp;task=listadds",($task=="listadds")?1:0);
        $menu->appendButton("Payments", "index.php?option=com_adsman&amp;task=payments",($task=="payments")?1:0);
        $menu->appendButton("Reported", "index.php?option=com_adsman&amp;task=reported_adds",($task == 'reported_adds')?1:0);
        $menu->appendButton("Users", "index.php?option=com_adsman&amp;task=users",($task=="users")?1:0);
        $menu->appendButton("Settings", "index.php?option=com_adsman&amp;task=settingsmanager",
            (($task=="settingsmanager")||($task=="integration")||($task=="show_customfields_config") || ($task=="categories")|| ($task=="paymentitems")
                ||($task=="gateway_administrator")|| ($task=="themes_administrator") || ($task=="gallery_administrator")
                ||($task=="mailsettings")|| ($task=="countries") || ($task=="showadsconfig") )?1:0);
        $menu->appendButton("About", "index.php?option=com_adsman&amp;task=aboutComponent",($task=="aboutComponent")?1:0);
    }

}