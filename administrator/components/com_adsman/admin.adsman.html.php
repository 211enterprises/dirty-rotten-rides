<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

$document = JFactory::getDocument();

$url = JURI::root().'administrator/components/com_adsman/tmpl/adsman.css'; 
$document->addStyleSheet($url);

$urlcss = JURI::root().'administrator/components/com_adsman/css/adsmenu.css'; 
$document->addStyleSheet($urlcss);
/**
 * HTML View class for the ADS component
 *
 * @static
 * @package	Ads Factory
 * @since 1.5.5
*/
class JAdsAdminView
{
	function edit(&$row, &$lists) {
		$db		= JFactory::getDBO();
		$user	= JFactory::getUser();
		$editor = JFactory::getEditor();
		if ($row) {
            JFilterOutput::objectHTMLSafe( $row, ENT_QUOTES );
        }

		jimport('joomla.filesystem.file');
		jimport('joomla.html.pane');
		$pane	= &JPane::getInstance('sliders', array('allowAllClose' => true));
		$tabs 	= &JPane::getInstance('Tabs');
        $database = JFactory::getDBO();

        $aduser = new JAdsUsers($database);
        $aduser->getUserDetails($row->userid);

    	JHTML::_('behavior.tooltip');
		JHTML::_('behavior.modal');
        JHtml::_('behavior.framework');
         //JHTML::_('behavior.mootools');
		?>
		<form action="index.php?option=com_adsman&view=edit" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data" >
		<table class="adminlist">
			<tr>
				<td width="50%" valign="top">
				<?php 
				echo $tabs->startPane("ad");
				echo $tabs->startPanel(JText::_("ADS_AD_INFO"),"ad1");
				?>

				<table class="adminform" style="width:auto;">
                  <tr>
                    <td width="130">
                        <label class="adslbl" id="useridlbl" for="userid" width="130"><?php echo JText::_( 'ADS_SELECT_AD_AUTHOR' ); ?>: </label>
                    </td>
                    <td>
                        <span class="tooltip_right"><input type="hidden" id="userid" name="userid" style="width:120px;" value="<?php echo $row->userid; ?>" /></span>
                        <span class="" id="userName" name="userName"><?php echo $aduser->username;?></span>
                        <span style="margin-left:3px; padding: 2px; border: 1px solid #999999; border-radius: 5px 5px 5px 5px"><a rel="{handler: 'iframe', size: {x:520, y:490}}" href="index.php?option=com_adsman&task=assignAdUser&tmpl=component" class="modal"><?php echo JText::_( 'ADS_ASSIGN_AD_USER' ); ?></a></span>
                    </td>
                  </tr>

				  <tr>
					<td>
					    <label for="title" width="130"><?php echo JText::_( 'ADS_TITLE' ); ?>: </label>
				    </td> <!--end td width = 50% >-->
                    <td>
                        <input class="text_area" type="text" name="title" id="title" value="<?php echo $row->title; ?>" size="50" maxlength="50" title="<?php echo JText::_( 'ADS_NAME_DISPLAY_IN_HEADINGS' ); ?>" />
                    </td>
			      </tr>
			      <tr>
				    <td>
					    <label for="category" width="130"><?php echo JText::_( 'ADS_PLG_CATEGORY' ); ?>: </label>
				    </td>
				    <td>
                        <?php if ( !isset($lists['category'])) { ?>
                                <span id="category_axj_space"><img src="<?php echo JURI::root().'/components/com_adsman/img/ajax-loader.gif'; ?>" /></span>
                        <?php }
                            else echo $lists['category'];
                        ?>
                    </td>
			      </tr>
                  <tr>
                      <td>
                        <label for="featured" width="130"><?php echo JText::_( 'ADSMAN_FREATURED' ); ?>: </label>
                      </td>
                      <td><?php echo $lists['featured']; ?></td>
                  </tr>
                  <tr>
                      <td>
                          <label for="ask_price" width="130"><?php echo JText::_( 'ADSMAN_ASK_PRICE' ); ?>: </label>
                      </td>
                      <td>
                          <input type="text" name="askprice" value="<?php echo $row->askprice; ?>" />
                          <?php echo $lists["currency"]; ?>
                      </td>
                  </tr>
			      <tr>
				    <td>
					    <label for="short_desc" width="130"><?php echo JText::_( 'ADSMAN_SHORT_DESCRIPTION' ); ?>:	</label>
				    </td>
				    <td>
					    <textarea name="short_desc" style="width:200px;"><?php echo $row->short_description; ?></textarea>
    				</td>
	        	  </tr>
                  <tr>
                    <td valign="top"><?php echo JText::_( 'ADS_TAGS');?></td>
                    <td><?php $row->tags = '';
                        if ($row->id) {
                			$tag_obj=new adsTags($database, "#__ads");
                			$tg = $tag_obj->getTags($row->id);
                			if($tg){
                				$row->tags = $tg;
                				$row->tags = implode(",", $row->tags);
                			}
                		}
                        ?>
                        <input class="inputbox" type="text" id="tags" name="tags" value="<?php echo $row->tags;?>" size="50" />
                    </td>
                  </tr>
			      <tr>
				    <td>
					    <label for="description"><?php echo JText::_( 'ADSMAN_DESCRIPTION' ); ?>:</label>
				    </td>
				    <td>
                        <?php echo $editor->display( 'description',  $row->description, '500', '300', '10', '10', array('pagebreak', 'readmore') ) ; ?>
				    </td>
			      </tr>
                <tr>
                    <td colspan="2">
                        <div id="custom_fields_container">
                            <table>
                            <?php //Custom fields will pop here:
                              if(isset($lists["fields"]))
                                foreach ($lists["fields"] as $key => $item) { ?>
                                <tr>
                                    <td>
                                        <?php echo $item["field_name"];?>
                                    </td>
                                    <td>
                                        <?php echo $item["field_html"];?>
                                    </td>
                                </tr>
                                <?php } ?>
                            </table>
                        </div>
                    </td>
                </tr>

		    </table>
                <script type="text/javascript">
                function selectUserId(user_id,userName){
                	document.getElementById("userid").value = user_id;
                    document.getElementById("userName").innerHTML = userName;
                }
                </script>

		<?php 
		echo $tabs->endPanel();
		echo $tabs->startPanel(JText::_("ADS_PHOTOS_ATTACHMENTS"),"ad2");
		$photos=array();
		if($row->picture){
			$main_pic = new stdClass();
			$main_pic->picture = $row->picture;
			$photos[] = $main_pic;
		}

		$database->setQuery("select * from #__ads_pictures where id_ad='$row->id'");
		$extra_photos = $database->loadObjectList();
		$photos = array_merge($photos,$extra_photos);

		?>
		<script language="javascript" type="text/javascript" src="<?php echo JURI::root();?>components/com_adsman/js/multifile.js"></script>
        <script language="javascript" type="text/javascript" src="<?php echo JURI::root();?>administrator/components/com_adsman/js/adsman.js"></script>
		<script type="text/javascript">
			var ckSel=true;
			function toggleRemoveAll(){
				var mainPic = 0;
				var extraPic = 0;
				<?php if ($row->picture) { ?>
					mainPic = 1;
				<?php } ?>
				<?php if (count($extra_photos)>0) { ?>
					extraPic = <?php echo count($extra_photos);?>
				<?php } ?>
				
				if(mainPic)
					document.getElementById("ck_delete_main_picture").checked = ckSel;
				for(var p=0;p<extraPic;p++)
						document.getElementById("ck_delete_picture_"+p).checked = ckSel;
				ckSel=!ckSel;
			}
		</script>
			<strong><?php echo JText::_('ADS_PHOTOS'); ?></strong>
			<table class="adminlist">
				<?php if (count($photos)>0 ) { ?>
					<thead>
						<tr>
							<th><?php echo JText::_('ADS_PREVIEW'); ?></th>
							<th><?php echo JText::_('ADS_INFO'); ?></th>
							<th width="100" style="color: red;">
                <?php echo JText::_('ADS_DELETE'); ?> <br />	<a href="#" onclick="toggleRemoveAll();"><span id="files_checkbox"><?php echo JText::_('ADS_CHECK_UNCHECK');?></span></a>
							</th>
						</tr>
					</thead>
				<?php }?>
				<?php if ($row->picture) { ?>
				<tr>
					<td width="200">
						<a href="<?php echo JURI::root()."/media/com_adsman/images/";?>/<?php echo "middle_$row->picture";?>" class="modal">
							<img src="<?php echo JURI::root()."/media/com_adsman/images/";?>/<?php echo "resize_$row->picture";?>" />
						</a><br />
						<a href="<?php echo JURI::root()."/media/com_adsman/images/";?>/<?php echo "$row->picture";?>" class="modal">
							<?php echo JText::_('ADS_ORIGINAL_SIZE'); ?>
						</a>
					</td>
					<td class="paramlist key">
                        <?php echo JText::_('ADS_EXTENSION'); ?>
                        <?php echo JFile::getExt(JPATH_ROOT."/media/com_adsman/images"."/$row->picture");?>
                        <br />
                        <?php echo JText::_('ADS_SIZE'); ?>
                        <?php echo filesize(JPATH_ROOT."/media/com_adsman/images"."/$row->picture");?>
					</td>
					<td align="center">
						<input type="checkbox" name="delete_main_picture" id="ck_delete_main_picture" value="1" />
					</td>
				</tr>
				<?php } else { ?>
			 	<tr>
			 		<td><?php echo JText::_('ADS_MAIN_PICTURE');?></td>
			 		<td>&nbsp;</td>
			 		<td><div class="fileinputs"><input type="file" name="picture" /></div></td>
			 	</tr>
				<?php }?>
				<?php 
				$ck =0;
				if(count($extra_photos) > 0 ) 
				 foreach ($extra_photos as $photo) {
					?>
				<tr>
					<td width="200">
						<a href="<?php echo JURI::root()."/media/com_adsman/images/";?>/<?php echo "middle_$photo->picture";?>" class="modal">
							<img src="<?php echo JURI::root()."/media/com_adsman/images/";?>/<?php echo "resize_$photo->picture";?>" />
						</a><br />
						<a href="<?php echo JURI::root()."/media/com_adsman/images/";?>/<?php echo "$photo->picture";?>" class="modal">
                            <?php echo JText::_('ADS_ORIGINAL_SIZE'); ?>
						</a>
					</td>
					<td class="paramlist key">
                     <?php echo JText::_('ADS_EXTENSION'); ?>
					<?php echo JFile::getExt(JPATH_ROOT."/media/com_adsman/images"."/$photo->picture");?>
					<br />
					<?php echo JText::_('ADS_SIZE'); ?>
					<?php echo filesize(JPATH_ROOT."/media/com_adsman/images"."/$photo->picture");?>
					</td>
					<td align="center">
						<input type="checkbox" name="delete_pictures[]" id="ck_delete_picture_<?php echo $ck;?>" value="<?php echo $photo->id;?>" />
					</td>
				</tr>
				<?php $ck++;
				 } ?>
                <tr><td colspan="3"><br /><hr /></td></tr>
                <!--</table>
            <hr />
                <strong><?php echo JText::_('ADS_IMAGES');?></strong>
                <table class="adminlist">-->
                    <tr>
                        <td width="200" style="background:#EDEDED;"><strong><?php echo JText::_('ADS_ADD_IMAGES');?><strong></td>
                        <td colspan="2" style="background:#EDEDED;">
                            <div id="files">
                                <input class="inputbox" id="my_file_element" type="file" name="pictures_1" />
                                <div id="files_list"></div>
                                <script>
                                var multi_selector = new MultiSelector( document.getElementById('files_list'),<?php echo ads_opt_maxnr_images - count($photos);?> );
                                multi_selector.addElement( document.getElementById( 'my_file_element' ) );
                                </script>
                            </div>
                        </td>
                    </tr>


                </table>
                <script type="text/javascript">
                	/*Joomla.submitbutton = function(task)
                	{
                		if (task == 'edit.apply') {
                			Joomla.submitform(task, document.getElementById('profile-form'));
                		}
                	}*/
                </script>

	 	    <?php if (ads_allow_atachment=="1" ) { ?>
            <table class="adminlist">
                <tr>
                  <td width="200" style="background:#EDEDED;"><strong><?php echo JText::_('ADS_ATTACHMENT');?><strong></td>
                  <td style="background:#EDEDED;" colspan="2">
                    <?php if ($row->atachment) { ?>
                        <a target="_blank" href="<?php echo JURI::root().'index.php?option=com_adsman&amp;task=downloadfile&id='.$row->id;?>"><?php echo JText::_('ADS_DOWNLOAD'); ?></a>
                        <?php if (ads_require_atachment=="1") { ?>
                            <?php echo JText::_("ADS_REPLACE_ATTACHMENT"); ?>
                            <input type="file" name="atachment" />
                        <?php } else { ?>
                            <input type="checkbox" name="delete_atachment" value="1" /><span style="float: left;"><?php echo JText::_('ADS_DELETE_ATTACHMENT'); ?>
                        <?php } ?>
                    <?php } else { ?>
                        <input type="file" name="atachment" />
                        <?php } ?>
                    <?php } ?>
                  </td>
                </tr>
            </table>
			<br />

            <!-- Embedded Video -->
            <?php
                $video_url = '';
                $video_id = null;
                if ( isset($row->id) && ($row->id != '') ) {

                    $sql = "SELECT id, video_link FROM #__ads_urls WHERE ad_id= ".$row->id;

                    $database->setQuery( $sql );
                    $resultv = $database->loadAssoc() ;
                    $video_url = $resultv['video_link'];
                    $video_id = $resultv['id'];
                }
            ?>
            <strong><?php echo JText::_('ADS_DETAILS_VIDEO'); ?></strong>
            <table class="adminlist">
                <tr>
                    <td style="background:#EDEDED;" colspan="2"><input id="video-link" type="text" size="95" maxlength="255" class="input_border" name="video-link" value="<?php echo $video_url; ?>" >
                        <input type="checkbox" name="delete_video" value="<?php echo $video_id; ?>" />&nbsp;<?php echo JText::_('ADS_DELETE_VIDEO');?>
                    </td>
                </tr>
            </table>

			<?php
			echo $tabs->endPanel();

            echo $tabs->startPanel(JText::_("ADS_COORDINATES_AND_LOCATION"),"ad2");

            if (ads_opt_google_key!="") {
                if ($row->MapX && $row->MapY){
                    $mapmarker="{pointTox:$row->MapX,pointToy:$row->MapY}";
                }else{
                    $mapmarker="{pointTox:".ads_opt_googlemap_defx.",pointToy:".ads_opt_googlemap_defy."}";
                }
                JFactory::getDocument()->addScriptDeclaration("
                    var language=Array();
                    language['ads_current_position']= '".JText::_("ADS_CURRENT_POSITION")."';
                ");
                JHTML::_("adsmanmap.js","map_canvas","listmap","gmap_centermap($mapmarker,'listmap',true);");
                ?>
              <table width="100%" style="border: 1px dashed silver; padding: 5px; margin-bottom: 10px;">
                <tr>
                    <td colspan="2">
                        <div id="map_canvas"></div>
                    </td>
                </tr>
                <tr>
                    <td width="150"><?php echo JText::_( 'ADS_GOOGLE_X_COORDINATE' ); ?> </td>
                    <td><input class="inputbox" type="text" id="googleX" name="MapX" value="<?php echo $row->MapX;?>" size="50" /></td>
                </tr>
                <tr>
                    <td width="150"><?php echo JText::_( 'ADS_GOOGLE_Y_COORDINATE' ); ?></td>
                    <td><input class="inputbox" type="text" id="googleY" name="MapY" value="<?php echo $row->MapY;?>" size="50"></td>
                </tr>

                <tr>
                    <td valign="top"><?php echo JText::_( 'ADS_AD_CITY' );?></td>
                    <td><input class="inputbox" type="text" id="ad_city" name="ad_city" value="<?php echo $row->ad_city;?>" size="50" /></td>
                </tr>
                <tr>
                    <td valign="top"><?php echo JText::_( 'ADS_AD_POSTCODE' );?></td>
                    <td><input class="inputbox validate" type="text" id="ad_postcode" name="ad_postcode" value="<?php echo $row->ad_postcode;?>" size="20" /></td>
                </tr>
              </table>
            <?php }

            echo $tabs->endPanel();

			echo $tabs->endPane();
			?>
		
			</td>
	<td valign="top">
			<?php
				echo $pane->startPane( "ads-pane" );
				$title = JText::_( 'ADSMAN_AD_DETAILS' );
				echo $pane->startPanel( $title, "detail-page" );
			?>
		<table width="100%" style="border: 1px dashed silver; padding: 5px; margin-bottom: 10px;">
			<tr>
				<td valign="top" rowspan="2">
					<label for="published" width="100">
						<?php echo JText::_( 'ADS_STATUS' ); ?>:
					</label>
				</td>
				<td width="100" valign="middle"><?php echo JText::_( 'ADS_PUBLISHED');?></td>
                <td><fieldset id="status" class="radio" style="border:0 none !important;">
                        <?php echo JHTML::_('select.booleanlist','status','',$row->status);?>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td width="100" valign="top"><?php echo JText::_( 'ADS_CLOSED_BY_ADMIN'); ?></td>
                <td valign="top">
                    <fieldset id="close_by_admin" class="radio" style="border:0 none !important;">
                        <?php echo JHTML::_('select.booleanlist','close_by_admin','',$row->close_by_admin); ?>
                    </fieldset>
                    <?php if ($row->closed==1) echo JText::_( 'ADS_CLOSED_ON').$row->closed_date; ?>
                    <br />
				</td>
			</tr>
			<tr>
				<td>
					<label for="add_type" width="100">
						<?php echo JText::_( 'ADS_AD_TYPE' ); ?>:
					</label>
				</td>
				<td colspan="2">
					<?php
					$jopts=array();
					$jopts[]=JHTML::_('select.option', ADDSMAN_TYPE_PUBLIC, JText::_("ADS_ADTYPE_PUBLIC"));
					$jopts[]=JHTML::_('select.option', ADDSMAN_TYPE_PRIVATE, JText::_("ADS_ADTYPE_PRIVATE"));
					
					echo JHTML::_("select.genericlist",$jopts,"addtype",'','value', 'text', $row->addtype);
					?>
				</td>
			</tr>

            <?php
            if ($lists['time_limited'] == 0) { ?>
                <tr>
                    <td valign="top"><label for="start_date"><?php echo JText::_( 'ADS_START_DATE' ); ?>:</label></td>
                    <td colspan="2">
                       <?php echo JHTML::_('calendar', $row->start_date, 'start_date', 'start_date', '%Y-%m-%d %H:%M:%S', array('class'=>'inputbox', 'size'=>'25',  'maxlength'=>'19')); ?>
                    </td>
                </tr>
                <tr>
                    <td valign="top"><label for="end_date"><?php echo JText::_( 'ADS_END_DATE' ); ?>:</label></td>
                    <td colspan="2">
                        <?php echo JHTML::_('calendar', $row->end_date, 'end_date', 'end_date', '%Y-%m-%d %H:%M:%S', array('class'=>'inputbox', 'size'=>'25',  'maxlength'=>'19')); ?>
                    </td>
                </tr>
            <?php } else { ?>
            	 <tr>
            	 	<td valign="top"><label for="end_date"><?php echo JText::_("ADS_AVAILABILITY"); ?></label></td>
            	 	<td valign="top" colspan="2">

                         <?php if ($row->id) { ?>

                            <input class="text_area" readonly type="hidden" name="start_date" id="start_date" size="15" maxlength="19" value="<?php echo $row->start_date; ?>" alt="start_date"/>
                            <input class="text_area" type="hidden" name="end_date" id="end_date" size="15" maxlength="19" value="<?php echo $row->end_date; ?>" alt="end_date"/>

                            <select id="ad_valability" name="ad_valability" onchange="document.getElementById('end_date').value=VList[this.value]; showValabilityPrice(VPriceList[this.value]);">
            	    			<?php foreach ($lists['valabs'] as $key=>$item) {
                                    if ($row->status) {
                                        //jtext text="valab_period_not editable"
                                        if ($item == $lists['db_no_of_days']) { ?>
                                            <option value="<?php echo $key; ?>" selected="selected" title="<?php echo $item; ?>"><?php echo $item; ?> days</option>
                                        <?php }
                                    } else {
                                        //"display selected period but editable"*
                                        if ($item == $lists['db_no_of_days']) { ?>
                                            <option value="<?php echo $key; ?>" selected="selected" title="<?php echo $item; ?>"><?php echo $item; ?> days</option>
                                        <?php } else {

                                        }
                                    }
                                } ?>
            				</select>
                            <?php echo $row->start_date.' - '. $row->end_date;
                         } else { ?>

                            <input class="text_area" readonly type="hidden" name="start_date" id="start_date" size="15" maxlength="19" value="<?php echo $lists['valab_now']; ?>" alt="start_date"/>
                            <input class="text_area" type="hidden" name="end_date" id="end_date" size="15" maxlength="19" value="<?php echo $lists['valab_end']; ?>" alt="end_date"/>


            				<select id="ad_valability" name="ad_valability" onchange="document.getElementById('end_date').value=VList[this.value]; showValabilityPrice(VPriceList[this.value]);">
            					<?php foreach ($lists['valabs'] as $key=>$item) { ?>
            						<option value="<?php echo $key; ?>" title="<?php echo $item;?>"><?php echo $item; ?> days</option>
            					<?php } ?>
            				</select>
                         <?php } ?>
            	 	</td>
            	 </tr>
            <?php } ?>

		</table>
			<?php
				$title = JText::_( 'ADS_USERDETAILS' );
				echo $pane->endPanel();
				echo $pane->startPanel( $title, "detail-page" );
			?>
		<table width="100%" style="border: 1px dashed silver; padding: 5px; margin-bottom: 10px;">
				<tr>
					<td><?php echo JText::_( 'ADS_USERNAME');?></td>
					<td><?php echo $row->userdetails->username;?></td>
				</tr>
				<tr>
					<td><?php echo JText::_( 'ADS_NAME');?></td>
					<td><?php echo $row->userdetails->name, " ", $row->userdetails->surname;?></td>
				</tr>
				<?php if(isset($row->userdetails->phone)) { ?>
				<tr>
					<td><?php echo JText::_( 'ADS_PHONE');?></td>
					<td><?php echo $row->userdetails->phone; ?></td>
				</tr>
				<?php } ?>
				<?php if(isset($row->userdetails->email)) { ?>
				<tr>
					<td><?php echo JText::_( 'ADS_EMAIL');?></td>
					<td><?php echo $row->userdetails->email; ?></td>
				</tr>
				<?php } ?>
				<?php if(isset($row->userdetails->address)) { ?>
				<tr>
					<td><?php echo JText::_( 'ADS_ADDRESS');?></td>
					<td><?php echo $row->userdetails->address; ?></td>
				</tr>
				<?php } ?>
				<tr>
					<td><?php echo JText::_( 'ADS_CITY');?></td>
					<td><?php echo $row->userdetails->city; ?></td>
				</tr>
				<tr>
					<td><?php echo JText::_( 'ADS_STATE');?></td>
					<td><?php echo $row->userdetails->state; ?></td>
				</tr>
				<tr>
					<td><?php echo JText::_( 'ADS_COUNTRY');?></td>
					<td><?php echo $row->userdetails->country; ?></td>
				</tr>
				<tr>
					<td><?php echo JText::_( 'YM');?></td>
					<td><?php echo $row->userdetails->YM; ?></td>
				</tr>
				<?php if ( isset($lists["user_fields"]) ) 
				foreach($lists["user_fields"] as $li => $field )
				{ ?>
				<tr>
					<td><?php echo $field['name']; ?></td>
					<td><?php echo $field['value']; ?></td>
				</tr>	
				<?php } ?>
		</table>
		<?php
			echo $pane->endPanel();
			$title = JText::_('ADS_MESSAGES');
			echo $pane->startPanel( $title, "messages-page" );
		?>
		<table width="100%" style="border: 1px dashed silver; padding: 5px; margin-bottom: 10px;">
            <tr>
                <th colspan="4"><?php echo JText::_( 'ADS_MESSAGES');?></th>
            </tr>
            <tr>
                <th><?php echo JText::_( 'ADS_FROM');?></th>
                <th><?php echo JText::_( 'ADS_TO');?></th>
                <th><?php echo JText::_( 'ADS_MESSAGE');?></th>
                <th><?php echo JText::_( 'ADS_DATE');?></th>
                <th>&nbsp;</th>
            </tr>
            <?php foreach ($row->messages as $k => $m) {  ?>
            <tr>
                <td><?php echo $m->fromusername; ?></td>
                <td><?php echo $m->tousername; ?></td>
                <td><span class="editlinktip hasTip" title="<?php echo $m->comment,".."; ?>"><?php echo substr($m->comment,0, 20); ?></span></td>
                <td><?php echo $m->datesend; ?></td>
                <td><a href="index.php?option=com_adsman&task=delmessage&mid=<?php echo $m->id; ?>&id=<?php echo $row->id; ?>">Delete</a></td>
            </tr>
            <?php } ?>
            <tr>
                <td colspan="4" align="center"><input type="button" class='button' onclick="location.href='index.php?option=com_adsman&task=write_admin_message&adid=<?php echo $row->id; ?>'" value="&nbsp;&nbsp;&nbsp;<?php echo JText::_("ADS_SEND"); ?>&nbsp;&nbsp;&nbsp;"></td>
            </tr>
        </table>
        <?php
            echo $pane->endPanel();
            echo $pane->endPane();
        ?>
	</td>
	</tr>
	</table>
			<input type="hidden" name="option" value="com_adsman" />
			<input type="hidden" name="id" value="<?php echo $row->id; ?>" />
			<input type="hidden" name="cid" value="<?php echo $row->id; ?>" />
			<input type="hidden" name="task" value="save" />
		</form>
		<?php
	}

    function ajaxCustomFields($fields) {
       echo '<table>';
        //Custom fields will pop here:
          if(isset($fields))
            foreach ($fields as $key => $item) { ?>
            <tr>
                <td>
                <?php echo $item["field_name"];?>
                </td>
                <td>
                <?php echo $item["field_html"];?>
                </td>
            </tr>
            <?php }
        echo '</table>';
    }

	function listadds(&$rows, &$page, &$lists) {
		// Initialize variables
		$db		= JFactory::getDBO();
		$user	= JFactory::getUser();
		$config	= JFactory::getConfig();

		//Ordering allowed ?
		JHTML::_('behavior.tooltip');
		JHTML::_('behavior.caption');
		JHtml::_('behavior.framework');
		?>
		<form action="index.php?option=com_adsman&view=listadds" method="post" name="adminForm" id="adminForm">
			<table>
				<tr>
					<td width="100%">
						<?php echo JText::_( 'ADS_FILTER' ); ?>:
						<input type="text" size="50" name="search" id="search" value="<?php echo $lists['search'];?>" class="text_area" onchange="document.adminForm.submit();" title="<?php echo JText::_( 'ADS_FILTER_BY' );?>" />
						<button onclick="this.form.submit();"><?php echo JText::_( 'ADS_GO' ); ?></button>
						<button onclick="document.getElementById('search').value='';this.form.getElementById('filter_sectionid').value='-1';this.form.getElementById('catid').value='0';this.form.getElementById('filter_authorid').value='0';this.form.getElementById('filter_state').value='';this.form.submit();"><?php echo JText::_( 'ADS_RESET' ); ?></button>
					</td>
					<td nowrap="nowrap">
						<?php
						?>
					</td>
					
					<td align="left" width="100%"></td>
                    <td>
                      <select id="ads_type" name="ads_type" onchange="submitform();">
                          <option value="2" <?php echo $lists['ads_type'] == 2 ? 'selected="selected"' : ''; ?>><?php echo JText::_('ADS_SELECT_AD_TYPE'); ?></option>
                          <option value="0" <?php echo $lists['ads_type'] == 0 ? 'selected="selected"' : ''; ?>><?php echo JText::_('ADS_ADTYPE_PUBLIC'); ?></option>
                          <option value="1" <?php echo $lists['ads_type'] == 1 ? 'selected="selected"' : ''; ?>><?php echo JText::_('ADS_ADTYPE_PRIVATE'); ?></option>
                      </select>
                    </td>
                    <td>
                      <select id="ads_published" name="ads_published" onchange="submitform();">
                          <option value="2" <?php echo $lists['ads_published'] == 2 ? 'selected="selected"' : ''; ?>><?php echo JText::_('ADS_SELECT_AD_PUBLISHED'); ?></option>
                          <option value="1" <?php echo $lists['ads_published'] == 1 ? 'selected="selected"' : ''; ?>><?php echo JText::_('ADS_PUBLISHED'); ?></option>
                          <option value="0" <?php echo $lists['ads_published'] == 0 ? 'selected="selected"' : ''; ?>><?php echo JText::_('ADS_UNPUBLISHED'); ?></option>
                      </select>
                    </td>
					<td>
					  <select id="status" name="status" onchange="submitform();">
			  			  <option value="2" <?php echo $lists['status'] == 2 ? 'selected="selected"' : ''; ?>><?php echo JText::_('ADS_SELECT_STATUS'); ?></option>
                          <option value="0" <?php echo $lists['status'] == 0 ? 'selected="selected"' : ''; ?>><?php echo JText::_('ADS_ACTIVE'); ?></option>
                          <option value="1" <?php echo $lists['status'] == 1 ? 'selected="selected"' : ''; ?>><?php echo JText::_('ADS_ARCHIVE'); ?></option>
					  </select>
					</td>
				</tr>
			</table>

			<table class="adminlist" cellspacing="1">
			<thead>
				<tr>
					<th width="5"><input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $rows ); ?>);" /></th>
					<th class="title ads_align_left" width="1%" nowrap="nowrap">
						<?php echo JHTML::_('grid.sort',   'Published', 'a.status', @$lists['order_Dir'], @$lists['order'] ); ?>
					</th>
					<th class="title ads_align_left" width="20"><?php echo JHTML::_('grid.sort',   'Featuring', 'a.featured', @$lists['order_Dir'], @$lists['order'] ); ?></th>
					<th class="title ads_align_left"><?php echo JHTML::_('grid.sort',   'Title', 'a.title', @$lists['order_Dir'], @$lists['order'] ); ?></th>
					<th class="title ads_align_left" width="25%" nowrap="nowrap"><?php echo JHTML::_('grid.sort',   'Category', 'cc.catname', @$lists['order_Dir'], @$lists['order'] ); ?></th>
					<th class="title ads_align_left" width="10%" nowrap="nowrap"><?php echo JHTML::_('grid.sort',   'Author', 'u.name', @$lists['order_Dir'], @$lists['order'] ); ?></th>
					<th class="title ads_align_left" width="20"><?php echo JHTML::_('grid.sort',   'Start Date', 'a.start_date', @$lists['order_Dir'], @$lists['order'] ); ?></th>
                    <th class="title ads_align_left" width="20"><?php echo JHTML::_('grid.sort',   'End Date', 'a.end_date', @$lists['order_Dir'], @$lists['order'] ); ?></th>
                    <th width="1%" nowrap="nowrap">
                        <?php echo JHTML::_('grid.sort',   'Blocked', 'a.close_by_admin', @$lists['order_Dir'], @$lists['order'] ); ?>
                    </th>
                    <th align="center" width="10"><?php echo JHTML::_('grid.sort',   'Hits', 'a.hits', @$lists['order_Dir'], @$lists['order'] ); ?></th>
					<th width="3%" class="title ads_align_right"><?php echo JHTML::_('grid.sort',   'ID', 'a.id', @$lists['order_Dir'], @$lists['order'] ); ?></th>
				</tr>
			</thead>
			<tfoot>
			<tr>
				<td colspan="11">
					<?php echo $page->getListFooter(); ?>
				</td>
			</tr>
			</tfoot>
			<tbody>
			<?php
			$k = 0;
			$nullDate = $db->getNullDate();
			for ($i=0, $n=count( $rows ); $i < $n; $i++) {
				$row = &$rows[$i];
				$link 	= 'index.php?option=com_adsman&task=edit&cid[]='. $row->id;
				if ( $row->status == 1 ) {
					$img = 'tick.png';
					$alt = JText::_( 'ADS_PUBLISHED' );
				} else {
					$img = 'publish_x.png';
					$alt = JText::_( 'ADS_UNPUBLISHED' );
				}
				if ( $row->close_by_admin == 1 ) {
					$img_block = 'publish_x.png';
					$alt_block = JText::_( 'ADS_BLOCKED' );
				} else {
					$img_block = 'tick.png';
					$alt_block = JText::_( 'ADS_OPENED' );
				}
				?>
				<tr class="<?php echo "row$k"; ?>">

					<td class="center">
						<?php echo JHTML::_('grid.id', $i, $row->id );?>
					</td>
					<td class="center">
						<span class="editlinktip hasTip" title="<?php echo JText::_( $alt );?>"><a href="javascript:void(0);" onclick="return listItemTask('cb<?php echo $i;?>','<?php echo $row->status ? 'unpublish' : 'publish' ?>')">
						<img src="<?php echo JURI::root()."administrator/components/com_adsman/img/".$img; ?>" width="16" height="16" border="0" alt=<?php echo $alt; ?> /></a></span>	
					</td>
					<td class="ads_align_left">
						<?php if($row->featured!="none") echo "<strong style='color:#CCC000;'>".ucfirst($row->featured)."</strong>";?>
					</td>
					<td>
						<a href="<?php echo JRoute::_( $link ); ?>"><?php echo htmlspecialchars($row->title, ENT_QUOTES); ?></a>
					</td>
					<td class="ads_align_left">
						<?php if($row->name) echo htmlspecialchars($row->name, ENT_QUOTES); else echo "---"; ?>
					</td>
					<td class="ads_align_left">
						<?php if($row->editor) echo htmlspecialchars($row->editor, ENT_QUOTES); else echo "---"; ?>
					</td>
					<td nowrap="nowrap">
						<?php if(isset($row->start_date)) echo JHTML::_('date',  $row->start_date, JText::_('DATE_FORMAT_LC4') ); ?>
					</td>
                    <td nowrap="nowrap">
                        <?php if(isset($row->end_date)) echo JHTML::_('date',  $row->end_date, JText::_('DATE_FORMAT_LC4') ); ?>
                    </td>
                    <td class="center">
                        <span class="editlinktip hasTip" title="<?php echo JText::_( $alt_block );?>"><a href="javascript:void(0);" onclick="return listItemTask('cb<?php echo $i;?>','<?php echo $row->close_by_admin ? 'unblock' : 'block' ?>')">
                            <img src="<?php echo JURI::root()."administrator/components/com_adsman/img/".$img_block;?>" width="16" height="16" border="0" alt="<?php echo $alt_block; ?>" /></a>
                        </span>
                    </td>
					<td nowrap="nowrap" class="center">
						<?php echo $row->hits ?>
					</td>
					<td class="ads_align_right">
						<?php echo $row->id; ?>
					</td>
				</tr>
				<?php
				$k = 1 - $k;
			}
			?>
			</tbody>
			</table>

		<input type="hidden" name="option" value="com_adsman" />
		<input type="hidden" name="task" value="listadds" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="filter_order" value="<?php echo $lists['order']; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $lists['order_Dir']; ?>" />
		<?php echo JHTML::_( 'form.token' ); ?>
		</form>
		<?php
	}

    function assignAdUser($rows,&$page,$lists){

        JHtml::_('behavior.framework');
        JHTML::_('behavior.tooltip');
        require_once(JPATH_COMPONENT_ADMINISTRATOR.DS."tmpl".DS."user".DS."assign_aduser.php");
    }

	function listUsers($rows,&$page,$lists){

		JHTML::_('behavior.tooltip');
		require_once(JPATH_COMPONENT_ADMINISTRATOR.DS."tmpl".DS."user".DS."users.php");
    }

	function detailUser( &$user, &$lists ){

		$u=&JTable::getInstance("user");
		$u->load($user->userid);
		require_once(JPATH_COMPONENT_ADMINISTRATOR.DS."tmpl".DS."user".DS."user.php");
    }

	function report_ads(&$rows, &$page, &$lists) {
		// Initialize variables
		$db		= JFactory::getDBO();
		$user	= JFactory::getUser();
		$config	= JFactory::getConfig();

		//Ordering allowed ?
		JHTML::_('behavior.tooltip');
		?>
		<script type="text/javascript">
    	function submitbutton(action){
    		frm=document.adminForm;
    	    frm.status.value=action;
    	    if(action=="block")
    	    	frm.task.value='block';
            else if (action=="unblock")
                frm.task.value='unblock';
    	   	else
    	    	frm.task.value='change_reported_status';
    		submitform(frm.task.value);
    	}
		</script>
		<form action="index.php?option=com_adsman" method="post" name="adminForm" id="adminForm">
			<table>
				<tr>
					<td width="100%">
						<?php echo JText::_( 'ADS_FILTER' ); ?>:
						<input type="text" size="50" name="search" id="search" value="<?php echo $lists['search'];?>" class="text_area" onchange="document.adminForm.submit();" title="<?php echo JText::_( 'ADS_FILTER_BY' );?>"/>


                        <select name="filter_status" onchange="this.form.submit();">
                            <option value=""  <?php if($lists['status']=='') echo "selected"; ?> ><?php echo JText::_("ADS_ALL");?></option>
                            <option value="solved" <?php if($lists['status']=='solved') echo "selected"; ?> ><?php echo JText::_("ADS_FILTER_SOLVED_1");?></option>
                            <option value="processing" <?php if($lists['status']=='processing') echo "selected"; ?> ><?php echo JText::_("ADS_FILTER_PROGRESS_1");?></option>
                            <option value="waiting" <?php if($lists['status']=='waiting') echo "selected"; ?> ><?php echo JText::_("ADS_FILTER_WAITING");?></option>
                            <option value="unprocessed" <?php if($lists['status']=='unprocessed') echo "selected"; ?> ><?php echo JText::_("ADS_FILTER_UNPROCESSED");?></option>
                        </select>
						<button onclick="this.form.submit();"><?php echo JText::_( 'ADS_GO' ); ?></button>
						<button onclick="document.getElementById('search').value='';this.form.submit();"><?php echo JText::_( 'ADS_RESET' ); ?></button>
					</td>
					<td nowrap="nowrap">
						<?php
						?>
					</td>
				</tr>
			</table>

			<table class="adminlist" cellspacing="1">
			<thead>
				<tr>
					<th width="5"><input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $rows ); ?>);" /></th>
					<th class="title ads_align_left" width="10%" nowrap="nowrap"><?php echo JHTML::_('grid.sort',   JText::_('ADS_REPORTED_ON'), 'modified', @$lists['order_Dir'], @$lists['order'] ); ?></th>
					<th class="title ads_align_left" width="25%" nowrap="nowrap"><?php echo JHTML::_('grid.sort',   JText::_('ADS_REPORTED_AD'), 'title', @$lists['order_Dir'], @$lists['order'] ); ?></th>
                    <th class="title" width="20" nowrap="nowrap"><?php echo JHTML::_('grid.sort',   JText::_('ADS_BLOCKED', 'a.close_by_admin'), @$lists['order_Dir'], @$lists['order'] ); ?></th>
					<th class="title ads_align_left" width="12%" nowrap="nowrap"><?php echo JHTML::_('grid.sort',   JText::_('ADS_REPORTED_BY'), 'username', @$lists['order_Dir'], @$lists['order'] ); ?></th>
					<th class="title ads_align_left"><?php echo JHTML::_('grid.sort',  JText::_('ADS_MESSAGE'), 'message', @$lists['order_Dir'], @$lists['order'] ); ?></th>
                    <th class="title ads_align_left" width="12%"><?php echo JText::_('ADS_WRITE_MESSAGE');?></th>
                    <th class="title" width="210"><?php echo JHTML::_('grid.sort',  JText::_('ADS_STATUS'), 'processing ', @$lists['order_Dir'], @$lists['order'] ); ?></th>
        		</tr>
			</thead>
			<tfoot>
			<tr>
				<td colspan="9">
					<?php echo $page->getListFooter(); ?>
				</td>
			</tr>
			</tfoot>
			<tbody>
			<?php
			$k = 0;
			$nullDate = $db->getNullDate();
			for ($i=0, $n=count( $rows ); $i < $n; $i++) {
				$row = &$rows[$i];
				$link 	= 'index.php?option=com_adsman&task=edit&cid[]='. $row->adid;

	    		$img1 	= ($row->status == 'solved') ? 'tick.png' : 'publish_x.png';
	    		$img2 	= ($row->status == 'processing') ? 'tick.png' : 'publish_x.png';

                switch ($row->status) {
                    case 'solved':
                        $alt 	= JText::_('ADS_FILTER_SOLVED_1');
                        $task = 'unprocessing';
                        $actiontitle = JText::_('ADS_SET_AS_NOT_PROCESSED');
                        $img = 'tick.png';
                        break;
                    case 'processing':
                        $alt 	= JText::_('ADS_FILTER_PROGRESS_1');
                        $task = 'solved';
                        $actiontitle = JText::_('ADS_SET_AS_SOLVED');
                        $img = 'processing.png';
                        break;
                    case 'waiting':
                        $alt 	= JText::_('ADS_FILTER_WAITING');
                        $task = 'processing';
                        $actiontitle = JText::_('ADS_SET_IN_PROGRESS');
                        $img = 'waiting.png';
                        break;
                    case 'unprocessed':
                        $alt 	= JText::_('ADS_FILTER_UNPROCESSED');
                        $task = 'waiting';
                        $actiontitle = JText::_('ADS_SET_AS_WAITING');
                        $img = 'publish_x.png';
                        break;
                    default:
                        $alt 	= JText::_('ADS_FILTER_UNPROCESSED');
                        $task = 'waiting';
                        $actiontitle = JText::_('ADS_SET_AS_WAITING');
                        $img = 'publish_x.png';
                        break;
                }

	    		$message = (strlen($row->message)>30) ? substr($row->message,0,30).".." : $row->message;
	    		$title   = (strlen($row->title)>30) ? substr($row->title,0,30).".." : $row->title;

                if($row->close_by_admin)
                    $title.="( Closed by admin )";

                if ( $row->close_by_admin == 1 ) {
                    $img_block = 'publish_x.png';
                    $alt_block = JText::_( 'ADS_BLOCKED' );
                } else {
                    $img_block = 'tick.png';
                    $alt_block = JText::_( 'ADS_OPENED' );
                }

				?>
				<tr class="<?php echo "row$k"; ?>">
					<td class="ads_align_center">
						<?php echo JHTML::_('grid.id', $i, $row->adid ); ?>
					</td>
					<td class="ads_align_left">
						<?php echo htmlspecialchars($row->modified, ENT_QUOTES); ?>
					</td>
					<td>
						<a href="<?php echo JRoute::_( $link ); ?>"><?php echo htmlspecialchars($title, ENT_QUOTES); ?></a>
					</td>
                    <td class="ads_align_center">
                        <span class="editlinktip hasTip" title="<?php echo JText::_( $alt_block );?>"><a href="javascript:void(0);" onclick="return listItemTask('cb<?php echo $i;?>','<?php echo $row->close_by_admin ? 'unblock' : 'block' ?>')">
                            <img src="<?php echo JURI::root()."administrator/components/com_adsman/img/".$img_block;?>" width="16" height="16" border="0" alt="<?php echo $alt_block; ?>" /></a>
                        </span>
                    </td>
					<td>
						<?php if($row->username) echo htmlspecialchars($row->username, ENT_QUOTES); else echo "---"; ?>
					</td>
					<td>
						<?php echo htmlspecialchars($message, ENT_QUOTES);; ?>
					</td>
                    <td>
                        <a href="index.php?option=com_adsman&task=write_admin_message&id_reported=<?php echo $row->adid; ?>"><?php echo  AdsUtilities::getUserNameReported($row->adid); //JText::_("ADS_WRITE_MESSAGE_TO_AUTHOR");  //if($row->username) echo htmlspecialchars($row->username, ENT_QUOTES); ?></a>
                    </td>
                    <td class="center">
                        <a href="javascript: void(0);" onClick="return listItemTask('cb<?php echo $i;?>','<?php echo $task;?>')" >
                            <img src="<?php echo JURI::root()."administrator/components/com_adsman/img/".$img;?>" width="16" height="16" border="0" alt="<?php echo $alt; ?>" title="<?php echo $alt .' &#187; '. $actiontitle; ?>" />
                        </a>
                    </td>
				</tr>
				<?php
				$k = 1 - $k;
			}
			?>
			</tbody>
			</table>

		<input type="hidden" name="option" value="com_adsman" />
		<input type="hidden" name="task" value="reported_adds" />
		<input type="hidden" name="status" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<!--<input type="hidden" name="redirect" value="<?php //echo $redirect;?>" />-->
		<input type="hidden" name="filter_order" value="<?php echo $lists['order']; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $lists['order_Dir']; ?>" />
		<?php echo JHTML::_( 'form.token' ); ?>
		</form>
		<?php
	}

	function showAdsManager()
    {
        ?>
    <fieldset class="ads">
      <legend><span style="float:left;"><?php echo JText::_( 'ADS_IMPORT_EXPORT' ); ?></span></legend>
    	<div id="cpanel">
		<?php
          $link = 'index.php?option=com_adsman&amp;task=showadmimportform';
   		  JAdsAdminView::quickiconButton( $link, 'importads.png', "Import Ads" );

          $link = 'index.php?option=com_adsman&amp;task=exportToXls';
		  JAdsAdminView::quickiconButton( $link, 'xlsexport.png', "Export XLS" );
		  
		  $link = 'index.php?option=com_adsman&amp;task=exportCategories';
		  JAdsAdminView::quickiconButton( $link, 'xlsexport.png', "Export Categories" );


		  ?>
    	</div>
    	<div style="height:100px">&nbsp;</div>
    	<div style="clear:both;"> </div>
    </fieldset>
        <?php
    }
    
    function showCustomFieldsManager()
    {
        ?>
    <fieldset class="ads">
        <legend><span style="float:left;"><?php echo JText::_( 'ADS_CUSTOM_FIELDS' ); ?></span></legend>
    	<div id="cpanel">
    	    <?php
              $link = 'index.php?option=com_adsman&amp;task=field_administrator';
              JAdsAdminView::quickiconButton( $link, 'cfields.png', "Fields Manager", "Step 1" );

              $link = 'index.php?option=com_adsman&amp;task=fposition_panel&act=list';
              JAdsAdminView::quickiconButton( $link, 'positions_big.png', "Positions / Template Syntax ", "Step 2" );

              $link = 'index.php?option=com_adsman&amp;task=fposition_fields';
              JAdsAdminView::quickiconButton( $link, 'cfields_assign.png', "Positions / Fields display ", "Step 3" );

            ?>
        </div>
        <div style="height:100px">&nbsp;</div>
        <div style="clear:both;"> </div>
    </fieldset>
        <?php
    }

	function statistics(&$rows, &$page, &$lists) {
		// Initialize variables
		$db		= JFactory::getDBO();
		$user	= JFactory::getUser();
		$config	= JFactory::getConfig();
		JHTML::_('behavior.tooltip');
		?>
    <fieldset class="adminform">
        <legend><?php echo JText::_( 'ADS_STATISTICS' ); ?></legend>
			<table class="adminlist" cellspacing="0" border="1">
				<tr class="grayRow">
					<td colspan="4" width="25" class="red" valign="top"><?php echo JText::_("ADS_STATISTICS"); ?></td>
                    <td rowspan="5" width="70" style="background-color: #f2f2f2;" valign="top">&nbsp;</td>
                    <td colspan="2" width="25" class="red" valign="top"><?php echo JText::_("ADS_STATISTICS_USERS"); ?></td>
                    <td rowspan="5" width="70" style="background-color: #f2f2f2;" valign="top">&nbsp;</td>
                    <td width="100" colspan="4" class="red"><?php echo JText::_("ADS_RECEIVED_PAYMENTS_GATEWAYS"); ?></td>
                </tr>

                <tr class="row0">
                    <td width="5%" class="right"><strong><?php  echo $lists["nr_total"];?></strong></td>
                    <td width="80" valign="top"><strong><?php echo JText::_("ADS_TOTAL"); ?></strong></td>

                    <td width="5%" class="right"><strong><?php  echo $lists["nr_ads_reported"];?></strong></td>
                    <td width="100"><?php echo JText::_("ADS_REPORTED"); ?></td>

                    <td width="5%" valign="top" class="right"><strong><?php  echo $lists["nr_r_users"];?></strong></td>
                    <td width="100" valign="top"><strong><?php echo JText::_("ADS_REGISTERED_USERS"); ?></strong></td>

                    <!--<td width="50" colspan="2" class="red"><?php echo JText::_("ADS_RECEIVED_AMOUNT"); ?></td>
                    <td width="100" colspan="4" class="red"><?php echo JText::_("ADS_RECEIVED_PAYMENTS_GATEWAYS"); ?></td>-->

                    <td width="50" rowspan="4" colspan="2">
                        <table cellpadding="0" cellspacing="0" width="400">
                        <?php
                            foreach ($lists["received_payments"] as $k => $row): ?>
                                <?php $w = 1; ?>
                                <tr class="<?php echo "row$w"; ?>">
                                    <td><?php echo ucfirst($row['payment_method']); ?></td>
                                    <td class="right" width="60"><strong><?php echo $row['currency_amount']; ?></strong></td>

                                    <td width="45"><strong><?php echo $row['currency']; ?></strong></td>
                                    <td width="100"><strong><?php echo $row['currency_quant']; ?><?php echo JText::_('ADS_PAYMENTS');?></strong></td>
                                </tr>
                                <?php $w = 1 - $w; ?>
                        <?php endforeach; ?>
                        </table>
                    </td>
                   <!-- <td width="50" rowspan="4" colspan="2">
                        <table cellpadding="0" cellspacing="0" width="100%">
                        <?php /*foreach ($lists["received_amount"] as $k => $row): ?>
                            <?php $w = 1; ?>
                            <tr class="<?php echo "row$w"; ?>">
                                <td class="right" width="50"><strong><?php echo $row['currency_amount']; ?></strong></td>
                                <td><strong><?php echo $row['currency']; ?></strong></td>
                            </tr>
                            <?php $w = 1 - $w; ?>
                        <?php endforeach; */ ?>
                        </table>
                    </td>-->

                </tr>

                <tr class="row0">
                    <td width="5%" class="right"><strong><?php  echo $lists["nr_active"];?></strong></td>
                    <td width="50"><?php echo JText::_("ADS_ACTIVE"); ?></td>

                    <td width="5%" class="right"><strong><?php  echo $lists["nr_blocked"];?></strong></td>
                    <td width="50"><?php echo JText::_("ADS_BLOCKED"); ?></td>

                    <td width="5%" class="right"><strong><?php  echo $lists["nr_a_users"];?></strong></td>
                    <td width="50"><?php echo JText::_("ADS_ACTIVE_USERS"); ?></td>
                </tr>

                <tr class="row0">
                    <td width="5%" class="right"><strong><?php  echo $lists["nr_published"];?></strong></td>
                    <td width="40"><?php echo JText::_("ADS_PUBLISHED"); ?></td>

                    <td width="5%" class="right"><strong><?php echo $lists["nr_expired"];?></strong></td>
                    <td width="40"><?php echo JText::_("ADS_EXPIRED"); ?></td>

                    <td width="5%" class="right"><strong><?php  echo $lists["nr_blocked_users"];?></strong></td>
                    <td width="40"><?php echo JText::_("ADS_BLOCKED_USERS"); ?></td>
                </tr>
                <tr class="row0">
                    <td width="5%" class="right"><strong><?php  echo $lists["nr_unpublished"];?></strong></td>
                    <td width="40"><?php echo JText::_("ADS_UNPUBLISHED"); ?></td>

                    <td width="5%" class="right"><strong><?php  echo $lists["nr_ads_archived"];?></strong></td>
                    <td width="40"><?php echo JText::_("ADS_ARCHIVE"); ?></td>

                    <td width="5%" class="right"><strong><?php  echo $lists["nr_users_unfilled_profile"];?></strong></td>
                    <td width="40"><?php echo JText::_("ADS_UNFILLED_PROFILES"); ?></td>
                </tr>

			</table>
    </fieldset>
		<?php
	}


	function showTerms(&$lists) {
	  $editor = JFactory::getEditor();
?>

<form action="index.php" method="post" name="adminForm">
	<input type="hidden" name="option" value="com_adsman" />
	<input type="hidden" name="task" value="saveterms" />
 <div style="width:90%;">
	<table width="100%">
    <tr>
    	<th class="title" colspan="2"><?php echo JText::_("ADS_TERMS"); ?></th>
    </tr>
	<tr>
		<td colspan="2">
			<?php echo $editor->display( 'terms',  $lists["terms"] , '100%', '550', '75', '20' ) ;?>
		</td>
	</tr>
	</table>
 </div>
</form>
<?php 
	}
	
	function showSettings($lists) {
				
		jimport('joomla.html.pane');
		JHTML::_("behavior.tooltip");
		
		$editor = &JFactory::getEditor();
		$tabs = &JPane::getInstance('Tabs');
		JLoader::register('JHtmlSettings', JPATH_COMPONENT_ADMINISTRATOR.DS.'thefactory'.DS.'lib'.DS.'settings.php');
        
        $mapmarker="{pointTox:".(ads_opt_googlemap_defx?ads_opt_googlemap_defx:"0").",pointToy:".(ads_opt_googlemap_defy?ads_opt_googlemap_defy:"0")."}";
        JFactory::getDocument()->addScriptDeclaration("
            var language=Array();
            language['ads_current_position']= '".JText::_("ADS_CURRENT_POSITION")."';
        ");
        JHTML::_("adsmanmap.js","map_canvas","listmap","gmap_centermap($mapmarker,'listmap',true);");
		?>
		<script type="text/javascript" src="<?php echo JURI::root();?>administrator/components/com_adsman/js/adsman.js"></script>
		<script type="text/javascript">
			// GUI for AJAX
			function showProgress(tab) {
				document.getElementById("tab_"+tab).style.display='block';document.getElementById("tab_"+tab+"_content").style.display='none';
				setTimeout("GUIAjax_showContent("+tab+")", 500);
			}
			function GUIAjax_showContent(tab) {
				document.getElementById("tab_"+tab).style.display='none';document.getElementById("tab_"+tab+"_content").style.display='block';
			}
		</script>
		<form action="index.php" method="post" name="adminForm" id="adminForm">
			<input type="hidden" name="option" value="com_adsman">
			<input type="hidden" name="task" value="savesettings">
			<input type="hidden" name="currency" value="">
			<input type="hidden" name="payment" value="">
			<input type="hidden" name="returnurl" value="<?php echo JURI::root().'/administrator/index.php?option=com_adsman&task=settings'; ?>">
			<?php
			$tabList = array(
				array(JText::_("ADS_MISC_SETTINGS") , 'miscsettings.php'),
				array(JText::_("CHECKOUT_SETTINGS") , 'checkout.php'),
				array(JText::_("ADS_IMAGE_SETTINGS"), 'imagesettings.php'),
				array(JText::_("ADS_GOOGLE_MAPS"), 'googlemaps.php'),
				array(JText::_("ADS_ANTI_SPAM"), 'privacy.php'),
				array(JText::_("ADS_TERMS_CONDITIONS"), 'terms.php'),
				array(JText::_("ADS_RESTRICTIONS"), 'userlimits.php'),
				array(JText::_("CACHE_MANAGEMENT"), 'cachemanager.php'),
				array(JText::_("ADS_SYSTEM_INFO"), 'systeminfo.php')
			);
			
			echo $tabs->startPane("configPane");
			foreach ($tabList as $tk => $tabItem){
				
				echo $tabs->startPanel("<div onclick=\"showProgress($tk);\">{$tabItem[0]}</div>","tab$tk");
									
				echo '<img id="tab_'.$tk.'" src="'.JURI::root().'/components/com_adsman/img/ajax-loader.gif" style="display:none;" border = "0" /><div id="tab_'.$tk.'_content" style="width:100%;">';
					require_once(JPATH_COMPONENT.DS.'tmpl'.DS.$tabItem[1]);
    						
				echo "</div>";
				echo $tabs->endPanel();
			}
			echo $tabs->endPane();
			?>
		</form>
		<?php
	}

	function showSettingsManager() {
        ?>
        <table width="100%" border="0">
          <tr>
        	<td width="60%" valign="top">

		    	<div id="cpanel">
				<?php
				  $link = 'index.php?option=com_adsman&amp;task=settings';
				  JAdsAdminView::quickiconButton( $link, 'adm/settings.png', JText::_("ADS_MISC_SETTINGS") );
		
				  $link = 'index.php?option=com_adsman&amp;task=integration';
				  JAdsAdminView::quickiconButton( $link, 'cb_integration.png', JText::_("ADS_PROFILE_INTEGRATION") );
		  
                  $link = 'index.php?option=com_adsman&amp;task=show_customfields_config';
                  JAdsAdminView::quickiconButton( $link, 'adm/positions.png', JText::_("ADS_CUSTOM_FIELDS") );

                  $link = 'index.php?option=com_adsman&amp;task=categories';
                  JAdsAdminView::quickiconButton( $link, 'adm/categories.png', JText::_("ADS_MENU_CATEGORIES") );
			    ?>
		    	</div>
		    	<div style="height:100px">&nbsp;</div>
		    	<div style="clear:both;"> </div>

		    	<div id="cpanel">
		    	<?php
				  $link = 'index.php?option=com_adsman&amp;task=paymentitems';
				  JAdsAdminView::quickiconButton( $link, 'paymentitems.png', JText::_("PAYMENT_ITEMS") );
		
				  $link = 'index.php?option=com_adsman&amp;task=gateway_administrator';
				  JAdsAdminView::quickiconButton( $link, 'gateway.png', JText::_("ADS_RECEIVED_PAYMENTS_GATEWAYS") );
		    	?>
		    	</div>
		    	<div style="height:100px">&nbsp;</div>
		    	<div style="clear:both;"> </div>

                <div id="cpanel">
                <?php
                    $link = 'index.php?option=com_adsman&amp;task=themes_administrator';
                    JAdsAdminView::quickiconButton( $link, 'adm/themes.png', JText::_("ADS_THEMES") );

                    $link = 'index.php?option=com_adsman&amp;task=gallery_administrator';
                    JAdsAdminView::quickiconButton( $link, 'adm/gallery.png', JText::_("ADS_GALLERY") );
                ?>
                </div>
                <div style="height:100px">&nbsp;</div>
                <div style="clear:both;"> </div>

		    	<div id="cpanel">
		    	<?php
		    	   $link = 'index.php?option=com_adsman&amp;task=mailsettings';
				   JAdsAdminView::quickiconButton( $link, 'adm/mail_manager.png', JText::_("MAIL_SETTINGS") );

    			   $link = 'index.php?option=com_adsman&amp;task=countries';
				   JAdsAdminView::quickiconButton( $link, 'adm/country_manager.png', JText::_("ADS_COUNTRIES_MANAGER") );
		
				   $link = 'index.php?option=com_adsman&amp;task=showadsconfig';
				   JAdsAdminView::quickiconButton( $link, 'xlsexport.png', JText::_("ADS_IMPORT_EXPORT") );
		    	?>

		    	<div style="height:150px">&nbsp;</div>
		    	<div style="clear:both;"> </div>
        	</td>
        	<td width="200" valign="middle">
		    	<table class="adminform">
                  <tr>
                      <td style="text-align:center !important;">
                      <a  href="http://wiki.thefactory.ro" title="Home">
                          <img src="http://wiki.thefactory.ro/lib/tpl/default/images/logo_wiki.png" alt="Logo" width="550" height="80" />
                      </a>
                      </td>
                  </tr>
		    	  <tr>
		    		<td style="text-align:center !important;">
					<strong> <?php echo JText::_('ADS_DOCUMENTATION_UNDER');?> </strong><br />
                    <a href="http://wiki.thefactory.ro/doku.php#ads_factory" target="_blank">http://wiki.thefactory.ro/doku.php#ads_factory</a>

                    <br /><br />
                    <strong> <?php echo JText::_('ADS_DOCUMENTATION_FAQ');?></strong><br />
                        <a href="http://www.thefactory.ro/joomla-forum/ads-factory.html" target="_blank">http://www.thefactory.ro/joomla-forum/ads-factory.html</a>

					<br /><br />

                    <strong><?php echo JText::_('ADS_DOCUMENTATION_UPDATES');?></strong><br />
                        <span><?php echo JText::_('ADS_DOCUMENTATION_VISIT_PAGE');?></span> <br />
                        <a href="http://www.thefactory.ro" target="_blank">http://www.thefactory.ro</a> <br />

					<br /> <?php echo JText::_('ADS_DOCUMENTATION_PLEASE');?> <a href="http://extensions.joomla.org/extensions/ads-a-affiliates/classified-ads/8766?qh=YTozOntpOjA7czozOiJhZHMiO2k6MTtzOjc6ImZhY3RvcnkiO2k6MjtzOjExOiJhZHMgZmFjdG9yeSI7fQ%3D%3D" target="_blank"><?php echo JText::_('ADS_DOCUMENTATION_VOTE_US');?></a><br>
		    		</td>
		    	  </tr>
		    	
				</table>
				<div style="height:50px">&nbsp;</div>
        	</td>
          </tr>
        </table>

        <?php
	}

    function showDashboard(&$rows, &$page, &$lists,&$latestAds,&$latestPayments) {
    ?>
        <table width="100%" border="0" cellpadding="2">
          <tr>
              <td rowspan="3" style=" vertical-align: top; padding: 10px 10px; text-align: center; ">
                  <div id="cpanel" style="width: 120px; position: relative; margin-top: 19px; ">
                <?php
                    $link = 'index.php?option=com_adsman&amp;task=listadds';
                    JAdsAdminView::quickiconButton( $link, 'adm/list_ads.png', JText::_("Ads List") );

                    echo '<br />';
                    $link = 'index.php?option=com_adsman&amp;task=payments';
                    JAdsAdminView::quickiconButton( $link, 'adm/payments.png', JText::_("Payments") );

                    echo '<br />';
                    $link = 'index.php?option=com_adsman&amp;task=reported_adds';
                    JAdsAdminView::quickiconButton( $link, 'adm/reported.png', JText::_("Reported") );

                    echo '<br />';
                    $link = 'index.php?option=com_adsman&amp;task=users';
                    JAdsAdminView::quickiconButton( $link, 'cb_integration.png', JText::_("Users") );

                    echo '<br />';
                    $link = 'index.php?option=com_adsman&amp;task=settingsmanager';
                    JAdsAdminView::quickiconButton( $link, 'adm/settings.png', JText::_("Settings") );

                    echo '<br />';
                    $link = 'index.php?option=com_adsman&amp;task=aboutComponent';
                    JAdsAdminView::quickiconButton( $link, 'adm/info.png', JText::_("About") );
                ?>
                  </div>
              </td>

              <td style="width: 97%;vertical-align: top;">
                  <?php JAdsAdminView::statistics($rows, $page, $lists); ?>
              </td>
          </tr>
          <tr>
               <td style="vertical-align: top; width:33%;">
                   <div id="latest_ads">
                    <fieldset class="adminform">
                        <legend><?php echo JText::_( 'ADSMAN_LATEST_ADS' ); ?></legend>
                            <?php  JHtml::_('behavior.framework');
                                    JHTML::_('behavior.tooltip');
                                require_once(JPATH_COMPONENT_ADMINISTRATOR.DS."tmpl".DS."showLatestads.php");
                            ?>
                    </fieldset>
                  </div>
               </td>
          </tr>
          <tr>
              <td style="vertical-align: top;">
                  <div id="latest_payments">
                    <fieldset class="adminform">
                        <legend><?php echo JText::_( 'ADSMAN_LATEST_PAYMENTS' ); ?></legend>
                            <?php  JHtml::_('behavior.framework');
                                   JHTML::_('behavior.tooltip');
                               require_once(JPATH_COMPONENT_ADMINISTRATOR.DS."tmpl".DS."showLatestpayments.php");
                            ?>
                    </fieldset>
                  </div>
              </td>
          </tr>

        </table>
    <?php
    }

	function bulkadmimport($errors){
		$database = JFactory::getDBO();
		?>
		<script language="javascript" type="text/javascript">
			function validateForm(){
				var csv = document.getElementById('csv');

				if(!csv.value){
					alert('<?php echo JText::_("ADSMAN_ERR_NO_CSV_FILE");?>');
					return false;
				}
			}
		</script>
		  <form action="index.php" method="post" name="adminForm" onsubmit="return validateForm();" enctype="multipart/form-data">
		  <input type="hidden" name="option" value="com_adsman">
		  <input type="hidden" name="task" value="importcsv">

		  <table width="100%" class="adminlist">
		  	<tr>
		  		<td>
		  			<?php echo JText::_("ADSMAN_CSV_FILE"); echo JHTML::_('tooltip', JText::_("ADSMAN_CSV_FILE_HELP")); ?>
		  		</td>
		  		<td>
		  			<input type="file" name="csv" id="csv" size="100" >
		  		</td>
		  	</tr>
		  	<tr>
		  		<td>
		  			<?php echo JText::_("ADSMAN_CSV_IMG_ARC"); echo JHTML::_('tooltip', JText::_("ADSMAN_CSV_IMG_ARC_HELP")); ?>
		  		</td>
		  		<td>
		  			<input type="file" name="arch" id="arch" style="width:450px !important;" size="100" >
		  		</td>
		  	</tr>

		  </table>
		  </form>
		<div align="left">
		<?php
		if(count($errors)>0){
			for ($i=1;$i<=count($errors);$i++){
				if($errors[$i]){
						echo ads_line." ".$errors[$i];
						echo '<br>';
				}
			}
		}
		?>
		</div>
		<?php
	}
	
	function quickiconButton( $link, $image, $text, $step = '' ) {
	?>
		<div style="float:left; font-weight: bold;"><span><?php echo $step; ?></span>
			<div class="icon">
				<a href="<?php echo $link; ?>">
					<?php
						echo JHTML::_('image.administrator', $image, '../components/com_adsman/img/', NULL, NULL, $text );
					?>
					<span><?php echo $text; ?></span>
				</a>
			</div>
		</div>
		<?php
	}


	function newPayment(&$lists){
		JHTML::_('behavior.tooltip');
    ?>
     <table width="100%">
     <form action="index.php" method="post" name="adminForm">
      <tr>
       <td colspan="2">
    	<table class="adminheading">
    	 <tr>
    	  <th width="40%"><?php echo JText::_('ADS_NEW_PAYMENT');?></th>
    	  </tr>
    	 </table>
    	</td>
       </tr>
       <tr>
    	<td colspan="2">
    	 <table class="adminlist">
    	  <tr>
        	  <td width="10%"><?php echo JText::_('ADS_PAYMENT_ITEM');?></td>
        	  <td><?php echo $lists['itemname'];?></td>
    	  </tr>
    	  <tr>
        	  <td><?php echo JText::_('ADS_NR_ITEMS');?></td>
        	  <td><input name="nr_items" value="1" size="2"></td>
    	  </tr>
    	  <tr>
        	  <td><?php echo JText::_('ADS_USERNAME');?></td>
        	  <td><?php echo $lists['usernames'];?></td>
    	  </tr>
    	  <tr>
        	  <td><?php echo JText::_('ADS_NOTIFY_USER');?></td>
        	  <td><input type="checkbox" name="notify_user" value="1"></td>
    	  </tr>
    	 </table>
    	</td>
       </tr>
      <input type="hidden" name="option" value="com_adsman" />
      <input type="hidden" name="task" value="savepayment" />
      <input type="hidden" name="boxchecked" value="0" />
      <input type="hidden" name="hidemainmenu" value="0">
     </form>
     </table>
    <?php
    }

    function writeAddMessage($add){
     ?>
     <script language="JavaScript" type="text/javascript">
    	<!--
    	function submitbutton(action){
    		if (document.adminForm.message.value || action==''){
    		    submitform(action);
    			return true;
    		}
    		alert('<?php echo JText::_("ADS_ERR_MESSAGE_EMPTY"); ?>');
    	}
    	//-->
     </script>

     <form method="POST" action="index.php"  name="adminForm">
      <table width="100%">
       <tr>
    	<td><?php echo JText::_('ADS_TO');?><?php echo $add->username;?></td>
       </tr>
       <tr>
    	<td><?php echo JText::_('ADS_REGARDING_AD');?><?php echo $add->title;?></td>
       </tr>
       <tr>
    	<td><?php echo JText::_('ADS_ID_AD');?><?php echo $add->id;?></td>
       </tr>
       <tr>
        <td>
        <textarea name="message" cols="80" rows="10"></textarea>
        </td>
       </tr>
      </table>
    	 <input type="hidden" name="adid" value="<?php echo $add->id;?>">
    	 <input type="hidden" name="option" value="com_adsman">
         <input type="hidden" name="task" value="sendaddmessage">

    	 <!--<input type="hidden" name="task" value="send_message_ads">-->
     </form>
    <?php
    }
    
    function ExportToXls($lists){
    	global $JAds_pages;
    ?>

	<h2><?php echo JText::_('ADS_EXPORT_SELECT_CUSTOM_FIELDS');?></h2>
    <table class="adminlist">
    	<tr>
    		<th width="25"><?php echo JText::_('ADS_CHECK');?></th>
    		<th><?php echo JText::_('ADS_FIELD_NAME');?></th>
    		<th><?php echo JText::_('ADS_TYPE');?></th>
    		<th><?php echo JText::_('ADS_SECTION');?></th>
    	</tr>
    <?php foreach ($lists["fields"] as $f => $field ){ ?>
    	<tr>
    		<td>
    			<input type="checkbox" name="cid[]" value="<?php echo $field->db_name;?>" /> 
    		</td>
    		<td>
    			<?php echo $field->name;?> <?php echo $field->db_name;?>
    		</td>
    		<td>
    			<?php echo $field->ftype;?>
    		</td>
    		<td>	
    			<?php echo $JAds_pages[$field->page];?>
    		</td>
    	</tr>
    <?php } ?>	
    <tr>
    	<td colspan="4" align="center">
    	<a href="#" onclick="document.adminForm.submit();">
    		<img src="<?php echo JURI::root();?>/components/com_adsman/img/xlsexport.png" style="vertical-align:middle;" /><?php echo JText::_('ADS_DO_EXPORT');?>
		</a>
    	</td>
    </tr>
    </table>
    
    <?php	
    }
    
}
?>