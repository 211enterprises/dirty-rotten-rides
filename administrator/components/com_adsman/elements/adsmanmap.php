<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
 * @build: 01/04/2012
 * @package: Jobs
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

abstract class JHTMLAdsmanMap
{
    static function js($canvasid="map_canvas",$mapname='listmap',$startup_js='')
    {
        if (!ads_opt_google_key) return;
  
        $jdoc=JFactory::getDocument();
        $jdoc->addScript('http://maps.googleapis.com/maps/api/js?key='.ads_opt_google_key.'&amp;sensor=false');
        $jdoc->addScript(JURI::root()."components/com_adsman/js/maps.js");
        $jdoc->addScriptDeclaration("
                    window.addEvent('domready', function(){
                            load_gmaps(google.maps.MapTypeId.".ads_opt_googlemap_type.
                            ",{pointTox:".ads_opt_googlemap_defx.",pointToy:".ads_opt_googlemap_defy."},".
                            ads_opt_googlemap_default_zoom.",'".$canvasid.
                        "',".ads_opt_googlemap_gx.",".ads_opt_googlemap_gy.",'".$mapname."');{$startup_js} } );	         
        ");
        
        return;
    }
    static function radius()
    {
		$radius_units = explode(",",ads_opt_googlemap_unit_available);
        if (!count($radius_units)) $radius_units = array("10","20","50","100");
        
        $opts=array();
        foreach($radius_units as $radius)
            $opts[]=JHtml::_("select.option",$radius,$radius);
            
        return JText::_("ADS_RADIUS").":&nbsp;".JHtml::_("select.genericlist",$opts,"radiusSelect","id='radiusSelect'").
            "&nbsp;<span>".((ads_opt_googlemap_distance==1)?"km":"miles");
        
    }
    static function limitbox()
    {
        $opts[]=JHtml::_("select.option","10","10");
        $opts[]=JHtml::_("select.option","20","20");
        $opts[]=JHtml::_("select.option","50","50");
        $opts[]=JHtml::_("select.option","0",JText::_("ADS_ALL"));
        return JText::_("ADS_SHOW").":&nbsp;".JHtml::_("select.genericlist",$opts,"ads_limitbox",
            "id='ads_limitbox' onchange='gmap_refreshAds();'");
        
    }
    
}
