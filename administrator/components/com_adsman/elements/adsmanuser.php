<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

abstract class JElementAdsmanUser
//class JElementAdsmanUser extends JElement
{
	static function fetchElement($name, $value, &$node, $control_name)
	{

		$database = & JFactory::getDBO();
		$tmp = array();
		$sql = "SELECT DISTINCT usr.id, CONCAT(usr.username, '".PHP_EOL."' ) as username FROM #__ads AS a RIGHT JOIN #__users AS usr ON usr.id = a.userid ORDER BY usr.username";
		$database->setQuery($sql);
		$tmp[] = JHTML::_('select.option', '0',"Choose user",'id','username');
		$tmp = array_merge( $tmp, $database->loadObjectList() );
		
		return JHTML::_('select.genericlist', $tmp, $control_name.'['.$name.']', 'class="inputbox"  ', 'id', 'username', $value);
	}
}
?>