<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

abstract class JElementAdsmanFilter
//class JElementAdsmanFilter extends JElement
{
	static function fetchElement($name, $value, &$node, $control_name)
	{

		$tmp[] = JHTML::_('select.option', 'start_date',"Start date" );
		$tmp[] = JHTML::_('select.option', 'title',"Title");
		$tmp[] = JHTML::_('select.option', 'askprice',"Price");
		
		return JHTML::_('select.genericlist', $tmp, $control_name.'['.$name.']', 'class="inputbox"  ', 'value', 'text', $value);
	}
}
?>