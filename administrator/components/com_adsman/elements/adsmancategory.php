<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

abstract class JElementAdsmanCategory extends JElement
//class JElementAdsmanCategory extends JElement
{
	static function fetchElement($name, $value, &$node, $control_name)
	{

		jimport('joomla.application.component.helper');
		if(!defined('APP_CATEGORY_TABLE')){
			define('APP_CATEGORY_TABLE','#__ads_categories');
			define('APP_CATEGORY_DEPTH','2');
			JLoader::register('JTheFactoryCategoryTable',JPATH_ADMINISTRATOR.DS.'components'.DS.'com_adsman'.DS.'thefactory'.DS.'category'.DS.'table.category.php');
			JLoader::register('JTheFactoryCategoryObj',JPATH_ADMINISTRATOR.DS.'components'.DS.'com_adsman'.DS.'thefactory'.DS.'category'.DS.'category.core.php');
			JLoader::register('JTheFactoryHelper',JPATH_ROOT.DS.'components'.DS.'com_adsman'.DS.'thefactory'.DS.'front.helper.php');
		}
		require_once(JPATH_SITE.DS."components".DS."com_adsman".DS.'helpers'.DS.'helper.php');
		$c = AdsUtilities::makeCatTree($value,$control_name.'['.$name.']', 200, false,"All");
		return $c;
	}
}
?>