<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2011 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
 * @build: 01/04/2012
 * @package: Jobs
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

abstract class JHTMLAdsDate
{
    static function calendar($isodate, $name)
    {
        require_once (JPATH_SITE . DS . 'components' . DS . 'com_adsman'.DS.'options.php');
        $result = JHTML::_('calendar', $isodate, $name, $name, AdsHelperDateTime::dateFormatConversion(ads_opt_date_format), array('class' => 'inputbox validate-date', 'size' => '16', 'maxlength' => '19'));

        if ($isodate) //ISODATES and JHtml::_('calendar') doesn't take kindly all formats       
            $result = str_replace(' value="' . htmlspecialchars($isodate, ENT_COMPAT, 'UTF-8') . '"',
                ' value="' . htmlspecialchars(JHtml::date($isodate, ads_opt_date_format, false), ENT_COMPAT, 'UTF-8') . '"',
                $result
            );

        return $result;
    }

}
