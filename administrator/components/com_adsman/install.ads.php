<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/


class com_adsmanInstallerScript{
	/**
	 * method to install the component
	 *
	 * @return void
	 */
    var $_version = null;
    var $_versionprevious = null;
    var $_new_install = null;


	function install($parent) 
	{
		//$parent->getParent()->setRedirectURL('index.php?option=com_adsman');
        $this->_version = $this->getCurrentVersion();
        $this->_versionprevious = $this->getPreviousVersion();
        $this->_new_install = $this->checkNewInstall();

        if ($this->_new_install){

            //Install SQL
            $sqlfile_create = JPATH_BASE."/components/com_adsman/install.adsman.mysql.utf8.sql";
                    //JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_adsman'.'install.adsman.mysql.utf8';
            $errors = com_adsmanInstallerScript::injectSQL($sqlfile_create);

            if($errors){
                echo "<pre style='color:red;'>";
                print_r($errors);
                echo "</pre>";
            }

            $sqlfile_insert = JPATH_SITE."/components/com_adsman/installer/install.adsman.inserts.sql";
            $errors = com_adsmanInstallerScript::injectSQL($sqlfile_insert); 

            if($errors){
                echo "<pre style='color:red;'>";
                print_r($errors);
                echo "</pre>";
            }

            com_adsmanInstallerScript::InstallPlugin('adsman','Search - Ads','search','search_limit=50');

            //Install CB Plugin
            com_adsmanInstallerScript::install_cbplugin('adsman_my_ads');
            com_adsmanInstallerScript::install_cbplugin('adsman_googlemap');
            com_adsmanInstallerScript::install_cbplugin('adsman_my_credits');
        }

	}
	/**
	 * method to uninstall the component
	 *
	 * @return void
	 */
	function uninstall($parent) 
	{
		// $parent is the class calling this method
        //delete SQL
        $sqlfile_delete = JPATH_BASE."/components/com_adsman/uninstall.adsman.mysql.utf8.sql";

        if (JFile::exists($sqlfile_delete)) {
            // $this->AddSQLFromFile($update_sql);
            $errors = com_adsmanInstallerScript::injectSQL($sqlfile_delete);

            if($errors){
                echo "<pre style='color:red;'>";
                print_r($errors);
                echo "</pre>";
            }
        }
	}

    function update($parent)
	{
		//$parent->getParent()->setRedirectURL('index.php?option=com_adsman');

        $this->_version = $this->getCurrentVersion();
        $this->_versionprevious = $this->getPreviousVersion();
        $this->_new_install = $this->checkNewInstall();

        if (!$this->_new_install){

            $update_class = JPATH_SITE.DS.'components'.DS.'com_adsman'.DS.'installer'.DS.'upgrade'.DS.'upgrade.php';

            if (JFile::exists($update_class))
            {
                require_once(JPATH_SITE.DS.'components'.DS.'com_adsman'.DS.'installer'.DS.'upgrade'.DS.'upgrade.php');
                JTheFactoryUpgrade::upgrade($this->_versionprevious);
            }

            //update SQL
            $update_sql = JPATH_SITE.DS.'components'.DS.'com_adsman'.DS.'installer'.DS.'upgrade'.DS.'upgrade_'.$this->_versionprevious.'.sql';
            if (JFile::exists($update_sql)) {

                $errors = com_adsmanInstallerScript::injectSQL($this->_new_install); 

                if($errors){
                    echo "<pre style='color:red;'>";
                    print_r($errors);
                    echo "</pre>";
                }
            }
        }

	}

	/**
	 * method to run before an install/update/uninstall method
	 *
	 * @return void
	 */
	function preflight($type, $parent) 
	{
		// $parent is the class calling this method
		// $type is the type of change (install, update or discover_install)
		echo '<p>' . JText::_('COM_ADSMAN_PREFLIGHT_' . $type . '_TEXT') . '</p>';
	}
	
	/**
	 * method to run after an install/update/uninstall method
	 *
	 * @return void
	 */
	function postflight($type, $parent) 
	{
		// $parent is the class calling this method
		// $type is the type of change (install, update or discover_install)
		$database =& JFactory::getDBO();
        ob_start();

        if ($this->_new_install){

            $database->setQuery("SELECT extension_id FROM `#__extensions` WHERE `element`= 'com_adsman' ");
            $component_id = $database->LoadResult();

            $MenuInstaller = new TheFactoryMenuHelper();
            $MenuInstaller->title = "AdsMan Menu";
            $MenuInstaller->componentid = $component_id;

            $MenuInstaller->AddMenuItem( 'User profile', 'user-profile','index.php?option=com_adsman&view=user&task=myuserprofile',9,2);
            $MenuInstaller->AddMenuItem( 'New Ad', 'new-ad','index.php?option=com_adsman&view=adsman&task=new',8,2);
            $MenuInstaller->AddMenuItem( 'My Ads', 'my-ads','index.php?option=com_adsman&view=adsman&task=myadds',7,2);
            $MenuInstaller->AddMenuItem( 'Favorite Ads', 'favourite-ads','index.php?option=com_adsman&view=adsman&task=favorites',6,2);

            $MenuInstaller->AddMenuItem( 'Ads on Map', 'list-ads-map','index.php?option=com_adsman&view=maps',5,1);
            $MenuInstaller->AddMenuItem( 'Search Ads', 'search-ads','index.php?option=com_adsman&view=adsman&task=show_search',3,1);
            $MenuInstaller->AddMenuItem( 'Categories', 'ads-categories','index.php?option=com_adsman&view=adsman&task=listcats',2,1);
            $MenuInstaller->AddMenuItem( 'Ads List', 'ads-list','index.php?option=com_adsman&view=adsman&task=listadds',1,1);
            
            $MenuInstaller->storeMenu();

            // JOOMFISH contentelements
            if (file_exists(JPATH_ROOT.DS. 'administrator'.DS.'components'.DS.'com_joomfish'.DS.'joomfish.php'))
            {
                copy(JPATH_ROOT.DS.'administrator/components/com_adsman/joomfish/ads_categories.xml',JPATH_ROOT.DS. 'administrator'.DS.'components'.DS.'com_joomfish'.DS.'contentelements'.DS.'ads_categories.xml');
                copy(JPATH_ROOT.DS.'administrator/components/com_adsman/joomfish/ads.xml',JPATH_ROOT.DS. 'administrator'.DS.'components'.DS.'com_joomfish'.DS.'contentelements'.DS.'ads.xml');
                copy(JPATH_ROOT.DS.'administrator/components/com_adsman/joomfish/ads_fields.xml',JPATH_ROOT.DS. 'administrator'.DS.'components'.DS.'com_joomfish'.DS.'contentelements'.DS.'ads_fields.xml');
                copy(JPATH_ROOT.DS.'administrator/components/com_adsman/joomfish/ads_fields_options.xml',JPATH_ROOT.DS. 'administrator'.DS.'components'.DS.'com_joomfish'.DS.'contentelements'.DS.'ads_fields_options.xml');
            }
            JFolder::move(JPATH_SITE . DS . 'components' . DS . 'com_adsman' . DS . 'templates-dist',
					JPATH_SITE . DS . 'components' . DS . 'com_adsman' . DS . 'templates');
            JFile::copy(
					JPATH_SITE . DS . 'components' . DS . 'com_adsman' . DS . 'options-dist.php',
					JPATH_SITE . DS . 'components' . DS . 'com_adsman' . DS . 'options.php'
			);

            if (!file_exists(JPATH_ROOT.DS. 'media'.DS.'com_adsman')) {
                @mkdir(JPATH_ROOT. DS. 'media'. DS. 'com_adsman');
                if (!file_exists(JPATH_ROOT.DS. 'media'.DS.'com_adsman'.DS.'images')) {
                    @mkdir(JPATH_ROOT. DS. 'media'. DS. 'com_adsman'.DS.'images');

                    if (!file_exists(JPATH_ROOT . DS . 'media' . DS . 'com_adsman' . DS . 'images' .DS. 'index.html')) {
                        JFile::copy(
                            JPATH_ROOT . DS . 'components' . DS . 'com_adsman' . DS . 'images' .DS. 'index.html',
                            JPATH_ROOT . DS . 'media' . DS . 'com_adsman' . DS . 'images' .DS. 'index.html'
                        );
                    }

                    if (!file_exists(JPATH_ROOT . DS . 'media' . DS . 'com_adsman' . DS . 'images' .DS. 'hdot.gif')) {
                        JFile::copy(
                            JPATH_ROOT . DS . 'components' . DS . 'com_adsman' . DS . 'images' .DS. 'hdot.gif',
                            JPATH_ROOT . DS . 'media' . DS . 'com_adsman' . DS . 'images' .DS. 'hdot.gif'
                        );
                    }
                    if (!file_exists(JPATH_ROOT . DS . 'media' . DS . 'com_adsman' . DS . 'images' .DS. 'no_image.png')) {
                        JFile::copy(
                            JPATH_ROOT . DS . 'components' . DS . 'com_adsman' . DS . 'images' .DS. 'no_image.png',
                            JPATH_ROOT . DS . 'media' . DS . 'com_adsman' . DS . 'images' .DS. 'no_image.png'
                        );
                    }
                }
            }
            
        } else {

            if (!file_exists(JPATH_ROOT.DS. 'media'.DS.'com_adsman')) {
                @mkdir(JPATH_ROOT. DS. 'media'. DS. 'com_adsman');
                if (!file_exists(JPATH_ROOT.DS. 'media'.DS.'com_adsman'.DS.'images')) {

                    @mkdir(JPATH_ROOT. DS. 'media'. DS. 'com_adsman'.DS.'images');

                    if (!file_exists(JPATH_ROOT . DS . 'media' . DS . 'com_adsman' . DS . 'images' .DS. 'index.html')) {
                        JFile::copy(
                            JPATH_ROOT . DS . 'components' . DS . 'com_adsman' . DS . 'images' .DS. 'index.html',
                            JPATH_ROOT . DS . 'media' . DS . 'com_adsman' . DS . 'images' .DS. 'index.html'
                        );
                    }

                    if (!file_exists(JPATH_ROOT . DS . 'media' . DS . 'com_adsman' . DS . 'images' .DS. 'hdot.gif')) {
                        JFile::copy(
                            JPATH_ROOT . DS . 'components' . DS . 'com_adsman' . DS . 'images' .DS. 'hdot.gif',
                            JPATH_ROOT . DS . 'media' . DS . 'com_adsman' . DS . 'images' .DS. 'hdot.gif'
                        );
                    }
                    if (!file_exists(JPATH_ROOT . DS . 'media' . DS . 'com_adsman' . DS . 'images' .DS. 'no_image.png')) {
                        JFile::copy(
                            JPATH_ROOT . DS . 'components' . DS . 'com_adsman' . DS . 'images' .DS. 'no_image.png',
                            JPATH_ROOT . DS . 'media' . DS . 'com_adsman' . DS . 'images' .DS. 'no_image.png'
                        );
                    }
                }
            }
            ?>
            <table width="100%">
            <tr>
                <td>
                    <h1>
                    The installation detected that you already had a previous installed version of Ads Factory.
                    </h1>
                </td>
            </tr>
            <tr>
                <td>
                    <h2>
                    The previously existing Ads Template folder WAS NOT overwritten in order to preserve any changes you might have done. If you like to overwrite the contents of the template folder please click the button below
                    </h2>            
                </td>        
            </tr>
            <tr>
                <td>
                    <button style="background-color:red;color:black;" onclick="if(confirm('Are you sure that you want to overwrite your existing Ads Factory templates?')) window.location='index.php?option=com_adsman&task=installtemplates'">
                    Overwrite Templates now!
                    </button> 
                </td>        
            </tr>
            </table>
            <?php
        }

		?>
		<table class="adminform">
			<tr>
				<td>
				    <?php com_adsmanInstallerScript::printfile("install.notes");?>
				</td>
			</tr>
			<tr>
				<td>
					<p>
					Please set up your <strong>Ads Factory</strong> in the <a href='<?php echo JURI::root();?>/administrator/index.php?option=com_adsman&task=settings'>admin panel according to your site requirements.</a></p>
					<br/><br/>
					<p> Release Notes:<br/><br/>
					<?php echo @$this->_version->releasenotes;?></p>
					<p><strong>Ads Factory</strong> Component <em>for Joomla CMS</em> <br />
					Visit us at <a target="_blank" href="http://www.thefactory.ro">thefactory.ro</a> to learn  about new versions
					and/or to give us feedback<br/>
					(c) 2006-2012 thefactory.ro
				</td>
			</tr>
		</table>
	<?php
        
        $message=ob_get_contents();
        $session = JFactory::getSession();
        $session->set('com_adsman_install_msg',$message);        
	}

    protected function checkNewInstall()
    {
        if (!$this->_versionprevious)
            return true;

        return false;
    }

    protected function getPreviousVersion()
    {
    	$db = & JFactory::getDBO();
        $db->setQuery("SELECT manifest_cache FROM #__extensions where element ='com_adsman' AND type='component' ");
        $manifest_cache = $db->LoadResult();

        $manifest_cache_params = new JRegistry($manifest_cache);

        return $manifest_cache_params->get('version');

    }

    protected function getCurrentVersion()
    {
   	    $configfile = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_adsman'.DS.'thefactory'.DS.'application.ini';

        if ( !file_exists( $configfile ) )
        {
			$error = JError::raiseError(500, 'Unable to load application configfile: '.$configfile);
			return $error;
        }

		if(function_exists('parse_ini_file'))
		{
			$ini = parse_ini_file($configfile,true);
		}
		else
		{
			jimport('joomla.registry.registry');
			jimport('joomla.filesystem.file');

			$handler =& JRegistryFormat::getInstance('INI');
			$data = $handler->stringToObject( JFile::read($configfile) ,true);

			$ini = JArrayHelper::fromObject($data);
		}

        return $ini['extension']['version'];
    }

	function injectSQL($sqlfile) {
		$database = JFactory::getDBO();
		
		$errors = array();

        $mqr = @get_magic_quotes_runtime();
		@set_magic_quotes_runtime(0);
		$query = fread( fopen(  $sqlfile, 'r' ), filesize($sqlfile ) );
		@set_magic_quotes_runtime($mqr);
		$pieces  =  com_adsmanInstallerScript::split_sql($query);
	
		for ($i=0; $i<count($pieces); $i++) {
			$pieces[$i] = trim($pieces[$i]);
			if(!empty($pieces[$i]) && $pieces[$i] != "#") {
				$database->setQuery( $pieces[$i] );
				if (!$database->query()) {
					$errors[] = array ( $database->getErrorMsg(), $pieces[$i] );
				}
			}
		}
		
	   return $errors;
	}
	
	function split_sql($sql) {
		$sql = trim($sql);
		$sql = @ereg_replace("\n#[^\n]*\n", "\n", $sql);
	
		$buffer = array();
		$ret = array();
		$in_string = false;
	
		for($i=0; $i<strlen($sql)-1; $i++) {
			if($sql[$i] == ";" && !$in_string) {
				$ret[] = substr($sql, 0, $i);
				$sql = substr($sql, $i + 1);
				$i = 0;
			}
	
			if($in_string && ($sql[$i] == $in_string) && $buffer[1] != "\\") {
				$in_string = false;
			}
			elseif(!$in_string && ($sql[$i] == '"' || $sql[$i] == "'") && (!isset($buffer[0]) || $buffer[0] != "\\")) {
				$in_string = $sql[$i];
			}
			if(isset($buffer[1])) {
				$buffer[0] = $buffer[1];
			}
			$buffer[1] = $sql[$i];
		}
	
		if(!empty($sql)) {
			$ret[] = $sql;
		}
		return($ret);
	}
	
	
	function printfile($filename) {
	    $filename = JPATH_ROOT . DS . "administrator" . DS . "components" . DS . "com_adsman" . DS . "$filename";
	    if (!file_exists($filename)) return;
	    $contents = fread( fopen(  $filename, 'r' ), filesize($filename ) );
	    echo $contents;
	
	}
	
	function install_cbplugin($pluginname){
		
		$database = &JFactory::getDBO();
	
		$database->setQuery("SELECT * FROM `#__extensions` WHERE `element`='com_comprofiler'");
		$comprofiler = $database->loadResult();
		
		if(count( $comprofiler ) <= 0){
			return false;
		}
		
		
	   $query = "SELECT id FROM #__comprofiler_plugin where element='$pluginname.plugin'";
	   $database->setQuery($query);
	   $plugid = $database->loadResult();

	   if (!$plugid) {

           switch ($pluginname) {
               case 'adsman_my_ads':
                   com_adsmanInstallerScript::InstallCBPlugin('Ads Factory - My Ads','My Ads','adsman_my_ads','plug_adsman_my_ads','getmyadsTab');
                   break;
               case 'adsman_googlemap':
                   com_adsmanInstallerScript::InstallCBPlugin('Ads Factory - GoogleMap','GoogleMap','adsman_googlemap','plug_adsman_googlemap','getmymap');
                   break;
               case 'adsman_my_credits':
                   com_adsmanInstallerScript::InstallCBPlugin('Ads Factory - My Credits','My Credits','adsman_my_credits','plug_adsman_my_credits','getmycreditsTab');
                   break;
           }
       }
		return true;
	}
	
		
	function InstallCBPlugin($plugintitle,$tabtitle,$pluginname,$folder,$class)
	{
		$database = &JFactory::getDBO();
		$query = "INSERT INTO #__comprofiler_plugin set
				`name`='$plugintitle',
				`element`='$pluginname.plugin',
				`type`='user',
				`folder`='$folder',
				`ordering`=99,
				`published`=1,
				`iscore`=0
			";
			$database->setQuery( $query );
			$database->query();
		
			$plugid = $database->insertid();
	  
		$query = "INSERT INTO #__comprofiler_tabs set
			`title`='$tabtitle',
			`ordering`=999,
			`enabled`=1,
			`pluginclass`='$class',
			`pluginid`='{$plugid}',
			`fields`=0,
			`displaytype`='tab',
			`position`='cb_tabmain',
			`viewaccesslevel`=2
			";
		$database->setQuery( $query );
		$database->query();
	
		@mkdir(JPATH_ROOT.'/components/com_comprofiler/plugin/user/'.$folder);
		$source_file = JPATH_ROOT."/components/com_adsman/cb_plug/$pluginname.plugin.php";
		$destination_file = JPATH_ROOT."/components/com_comprofiler/plugin/user/$folder/$pluginname.plugin.php";
		copy ($source_file, $destination_file);
	
		$source_file = JPATH_ROOT."/components/com_adsman/cb_plug/$pluginname.plugin.xml";
		$destination_file = JPATH_ROOT."/components/com_comprofiler/plugin/user/$folder/$pluginname.plugin.xml";
		copy ($source_file, $destination_file);
	}

	function InstallPlugin($plugin_element,$plugin_title,$plugin_folder,$params='')
	{
	   $database = &JFactory::getDBO();

        @mkdir(JPATH_ROOT."/plugins/$plugin_folder/adsman");
		$source_file = JPATH_ROOT."/components/com_adsman/module/$plugin_folder/$plugin_element.php";
		$destination_file =  JPATH_ROOT."/plugins/$plugin_folder/adsman/$plugin_element.php";
		if (!file_exists($destination_file)) copy ($source_file, $destination_file);
	
		$source_file = JPATH_ROOT."/components/com_adsman/module/$plugin_folder/$plugin_element.xml";
		$destination_file =  JPATH_ROOT."/plugins/$plugin_folder/adsman/$plugin_element.xml";
		if (!file_exists($destination_file)) copy ($source_file, $destination_file);

        $source_file = JPATH_ROOT."/components/com_adsman/module/$plugin_folder/en-GB/en-GB.plg_search_adsman.ini";
        $destination_file =  JPATH_ADMINISTRATOR."/language/en-GB/en-GB.plg_search_adsman.ini";
        copy ($source_file, $destination_file);

		$database->setQuery("select count(*) from #__extensions where name='plg_search_adsman' ");
		if ($database->LoadResult()) return; //not unique
	
		// Insert in db
	  	$extension = JTable::getInstance('extension');

		  $extension->name      = 'plg_search_adsman';
		  $extension->type      = 'plugin';
		  $extension->element   = $plugin_element;
		  $extension->folder    = $plugin_folder;
		  $extension->enabled   = 1;
		  $extension->access    = 1;
		  $extension->client_id = 0;
		  $extension->protected = 0;
		  $extension->params 	= '{"search_limit":"50","search_content":"1","search_archived":"1"}';
		  
		  $extension->store();
		
	}
	
	
}

class TheFactoryMenuHelper{
	
	var $id = null;
	var $module_id = null;
	var $menutype = null;
	var $componentid = null;
	var $title = null;
	var $menu_list = array();
	
	function _getMenuType_ID(){
		
		$menuType = JTable::getInstance('menutype');
		$menuType->load(array("title"=>$this->title));
		$this->id=$menuType->id;
		$this->menutype=$menuType->menutype;
	}
	
	function _getMenuModule_ID(){
		
		$module = JTable::getInstance( 'module');
		
		$module->load(array("title"=>$this->title));
		$this->module_id = $module->id;
	}
	
	function storeMenu(){
		
		$this->_getMenuType_ID();
		
		$menuType = JTable::getInstance('menutype');
		$menuType->id 		= $this->id;
		$this->menutype = $menuType->menutype = JFilterOutput::stringURLSafe($this->title);
		$menuType->title 	= $this->title;
		$menuType->store();
		$this->storeMenuModule();
	}
	
	function storeMenuModule(){

		$db =& JFactory::getDBO();

		$this->_getMenuModule_ID();
		$module = JTable::getInstance( 'module');
		
		$module->id 		= $this->module_id;
		$module->title 		= $this->title;
		$module->position 	= 'position-7';
		$module->module 	= 'mod_menu';
		$module->access 	= 1;
		$module->showtitle 	= 1;
		$module->published	= 1;
		$module->client_id 	= 0;
		$module->language 	= '*';
		$module->params 		= '{"menutype":"'.$this->menutype.'","startLevel":"1","endLevel":"0","showAllChildren":"0","tag_id":"","class_sfx":"","window_open":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"itemid"}';
		$module->store();
		
		// module assigned to show on All pages by default
		// Clean up possible garbage first
		$query = 'DELETE FROM #__modules_menu WHERE moduleid = '.(int) $module->id;
		$db->setQuery( $query );
		if (!$db->query()) {
			return JError::raiseWarning( 500, $db->getError() );
		}

		// ToDO: Changed to become a Joomla! db-object
		$query = 'INSERT INTO #__modules_menu VALUES ( '.(int) $module->id.', 0 )';
		$db->setQuery( $query );
		if (!$db->query()) {
			return JError::raiseWarning( 500, $db->getError() );
		}

		$query = 'DELETE FROM #__menu WHERE menutype = "'.$this->menutype.'"';
		$db->setQuery( $query );
		if (!$db->query()) {
			return JError::raiseWarning( 500, $db->getError() );
		}
		
		// Add Menu Items
		if(count($this->menu_list)){
			//remove all menu items
			
			foreach ($this->menu_list as $k => $menuitem){
				
				$menuitem->component_id = $this->componentid;
				$menuitem->menutype = $this->menutype;
				
				$menuitem->store();
				//first store -> level=0, parent_id = 0 <-> bad
				$menuitem->level = 1;
				$menuitem->parent_id = 1;
				$menuitem->store();
			}
		}
		
	}

	
	function AddMenuItem($title, $alias, $link, $ordering, $access=0,$params=null){
		
		$menu = JTable::getInstance( 'menu');
		$database =& JFactory::getDBO();
		
		$query = ' SELECT extension_id'
         . ' FROM #__extensions'
         . ' WHERE element="com_adsman"';
  		$database->setQuery($query);
  		$component_id = $database->loadResult();

		$menu->title		= $title;
		$menu->alias 		= $alias;
		$menu->link 		= $link;
		$menu->type			= "component";
		$menu->access		= $access;
		$menu->published	= 1;
		$menu->parent_id	= 1;
		$menu->component_id	= $component_id;
		$menu->ordering		= $ordering;
		$menu->access		= $access;
        $menu->params       = '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}';
        $menu->language 	= '*';
		
		$this->menu_list[$title] = $menu;
	
	}
	
}

?>
