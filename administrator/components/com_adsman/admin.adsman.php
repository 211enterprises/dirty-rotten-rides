<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

require_once( JPATH_COMPONENT.DS.'controller.php' );
require_once (JApplicationHelper::getPath('admin_html'));


// Set the helper directory
JHTML::addIncludePath( JPATH_ROOT.DS.'components'.DS.'com_adsman'.DS.'helpers'.DS );

require_once (JPATH_ROOT.DS.'components'.DS.'com_adsman'.DS.'helpers'.DS."helper.php");
require_once (JPATH_ROOT.DS.'components'.DS.'com_adsman'.DS.'helpers'.DS."route.php");
require_once (JPATH_ROOT.DS.'components'.DS.'com_adsman'.DS.'config.php');
require_once (JPATH_ROOT.DS.'components'.DS.'com_adsman'.DS.'options.php');

JHtml::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR . DS . 'elements');

$AdsUtilities = new AdsUtilities();
$AdsUtilities->loadLanguage();

$admintask = JRequest::getCmd('task',"dashboard");
$view   = JRequest::getCmd('view');
$option = JRequest::getCmd('option');
$sub = JRequest::getCmd('sub',"");

if($sub)
	$admintask = $sub;

if ('' == $admintask)
{
    $admintask = '' == $view ? 'dashboard' : $view;
}

require_once(JPATH_COMPONENT.DS.'thefactory'.DS.'admin.application.php');

$JTheFactoryApp = new JTheFactoryApp();
$Tapp = $JTheFactoryApp->getInstance();

if ($Tapp->checktask($admintask)) return; 

global $cb_fieldmap;
$controller = new JAdsAdminController();
// Register Extra tasks
$controller->registerTask( 'newAd' , 'edit' );
$controller->registerTask( 'edit.apply', 'save' );

$controller->execute( $admintask );
$controller->redirect();

?>
