<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

$db		= JFactory::getDBO();

JHTML::_('behavior.tooltip');
JHTML::_('behavior.caption');
JHtml::_('behavior.framework');

?>
<form action="index.php?option=com_adsman&view=showLatestads" method="post" name="adminForm" id="adminForm">
<table class="adminlist" cellspacing="1">
<thead>
<tr>
	<th width="1%" nowrap="nowrap">
		<?php echo JText::_('ADS_PUBLISHED'); ?>
	</th>
	<th class="title ads_align_left" width="20"><?php echo JText::_('ADS_FEATURING'); ?></th>
	<th class="title ads_align_left"><?php echo JText::_('ADS_TITLE'); ?></th>
	<th class="title ads_align_left" width="25%" nowrap="nowrap"><?php echo JText::_('ADS_PLG_CATEGORY'); ?></th>
	<th class="title ads_align_left" width="10%" nowrap="nowrap"><?php echo JText::_('ADS_AUTHOR'); ?></th>
	<th class="title ads_align_left" width="20"><?php echo JText::_('ADS_START_DATE'); ?></th>
    <th class="title ads_align_left" width="20"><?php echo JText::_('ADS_END_DATE'); ?></th>
    <th width="1%" nowrap="nowrap">
        <?php echo JText::_('ADS_BLOCKED'); ?>
    </th>
	<th align="center" width="10"><?php echo JText::_('ADS_HITS'); ?></th>
	<th width="3%" class="title ads_align_right"><?php echo JText::_('ID'); ?></th>
	</tr>
</thead>
<tbody>
<?php
$k = 0;
$nullDate = $db->getNullDate();
for ($i=0, $n=count( $latestAds['rows'] ); $i < $n; $i++) {
$row = &$latestAds['rows'][$i];
$link 	= 'index.php?option=com_adsman&task=edit&cid[]='. $row->id;
if ( $row->status == 1 ) {
	$img = 'tick.png';
	$alt = JText::_( 'ADS_PUBLISHED' );
} else {
	$img = 'publish_x.png';
	$alt = JText::_( 'ADS_UNPUBLISHED' );
}
if ( $row->close_by_admin == 1 ) {
	$img_block = 'publish_x.png';
	$alt_block = JText::_( 'ADS_BLOCKED' );
} else {
	$img_block = 'tick.png';
	$alt_block = JText::_( 'ADS_OPENED' );
}
?>
<tr class="<?php echo "row$k"; ?>">
	<td class="center">
		<span class="editlinktip hasTip" title="<?php echo JText::_( $alt );?>"><a href="javascript:void(0);" onclick="return listItemTask('cb<?php echo $i;?>','<?php echo $row->status ? 'unpublish' : 'publish' ?>')">
		<img src="<?php echo JURI::root()."administrator/components/com_adsman/img/".$img; ?>" width="16" height="16" border="0" alt=<?php echo $alt; ?> /></a></span>
	</td>
	<td>
		<?php if($row->featured!="none") echo "<strong style='color:#CCC000;'>".ucfirst($row->featured)."</strong>";?>
	</td>
	<td>
		<a href="<?php echo JRoute::_( $link ); ?>"><?php echo htmlspecialchars($row->title, ENT_QUOTES); ?></a>
	</td>
	<td>
		<?php if($row->name) echo htmlspecialchars($row->name, ENT_QUOTES); else echo "---"; ?>
	</td>
	<td>
		<?php if($row->editor) echo htmlspecialchars($row->editor, ENT_QUOTES); else echo "---"; ?>
	</td>
	<td nowrap="nowrap">
		<?php if(isset($row->start_date)) echo JHTML::_('date',  $row->start_date, JText::_('DATE_FORMAT_LC4') ); ?>
	</td>
    <td nowrap="nowrap">
        <?php if(isset($row->end_date)) echo JHTML::_('date',  $row->end_date, JText::_('DATE_FORMAT_LC4') ); ?>
    </td>
    <td class="center">
        <span class="editlinktip hasTip" title="<?php echo JText::_( $alt_block );?>"><a href="javascript:void(0);" onclick="return listItemTask('cb<?php echo $i;?>','<?php echo $row->close_by_admin ? 'unblock' : 'block' ?>')">
            <img src="<?php echo JURI::root()."administrator/components/com_adsman/img/".$img_block;?>" width="16" height="16" border="0" alt="<?php echo $alt_block; ?>" /></a>
        </span>
    </td>
	<td nowrap="nowrap" class="center">
		<?php echo $row->hits ?>
	</td>
	<td class="ads_align_right">
		<?php echo $row->id; ?>
	</td>
</tr>
<?php
$k = 1 - $k;
}
?>
</tbody>
</table>

<input type="hidden" name="option" value="com_adsman" />
<input type="hidden" name="task" value="showLatestads" />
<input type="hidden" name="boxchecked" value="0" />
<input type="hidden" name="filter_order" value="<?php echo $lists['order']; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $lists['order_Dir']; ?>" />
<?php echo JHTML::_( 'form.token' ); ?>
</form>


