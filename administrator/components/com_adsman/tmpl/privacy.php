<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/
?>

<fieldset class="adminform">
	<legend><?php echo JText::_( 'ADS_SPAM_SETTINGS' ); ?></legend>
	
	<?php if(defined("ads_opt_enable_captcha") && ads_opt_enable_captcha!=1) {  ?>
		<img src="<?php echo JURI::root();?>administrator/components/com_adsman/img/warning.png" style='vertical-align:middle;' />	   <strong style="color:#FF0000 !important;"><?php echo JText::_("ADS_CAPTCHA_IS_DISABLED");?></strong>
	<?php }	 ?>
	<h3><?php echo JText::_("ADS_RECAPTCHA_SETTINGS"); ?></h3>
	<p><?php echo JText::_('ADS_USES_THE'); ?> <a href="http://recaptcha.net/">ReCaptcha</a> <?php echo JText::_('ADS_ANTI_BOT_SERVICE'); ?></p>
	<p>
	  <?php echo JText::_('ADS_REGISTER_CAPTCHA'); ?> <a href="https://admin.recaptcha.net/accounts/signup/?next=%2Frecaptcha%2Fcreatesite%2F" target="_blank"><?php echo JText::_('ADS_ACCOUNT'); ?></a>
	  <?php echo JText::_('ADS_ACCOUNT_FILL_IN'); ?>.
	</p>
	<table class="adminlist">
	  <!-- recaptcha_public_key -->
	  <tr class="hasTip" title="<?php echo JText::_('ADS_RECAPTCHA_PUBLIC_KEY'); ?>::<?php echo JText::_('ADS_RECAPTCHA_PUBLIC_KEY'); ?>">
	    <td width="20%" class="paramlist_key">
	      <span class="editlinktip">
	        <label for="recaptcha_public_key"><?php echo JText::_('ADS_RECAPTCHA_PUBLIC_KEY'); ?></label>
	      </span>
	    </td>
	    <td class="paramlist_value">
		    <input type="text" name="ads_opt_recaptcha_public_key" id="ads_opt_recaptcha_public_key" value="<?php echo $lists["recaptcha"]["public_key"]; ?>" style="width:280px;" />
	    </td>
	  </tr>
	
	  <!-- recaptcha_private_key -->
	  <tr class="hasTip" title="<?php echo JText::_('ADS_RECAPTCHA_PRIVATE_KEY'); ?>::<?php echo JText::_('ADS_RECAPTCHA_PRIVATE_KEY'); ?>">
	    <td width="20%" class="paramlist_key">
	      <span class="editlinktip">
	        <label for="recaptcha_private_key"><?php echo JText::_('ADS_RECAPTCHA_PRIVATE_KEY'); ?></label>
	      </span>
	    </td>
	    <td class="paramlist_value">
		    <input type="text" name="ads_opt_recaptcha_private_key" id="ads_opt_recaptcha_private_key" value="<?php echo $lists["recaptcha"]["private_key"]; ?>" style="width:280px;" />
	    </td>
	  </tr>
		<tr>
			<td width="20%" class="paramlist_key"><label for="recaptcha_themes"><?php echo JText::_("ADS_THEME");?></label></td>
			<td>
				<?php echo $lists["recaptcha_themes"];?>
			</td>
		</tr>
	</table>
	<br />
	<h3><?php echo JText::_("ADS_EMAIL_SPAM_PROTECTION"); ?></h3>
	<table class="adminlist">
		<tr>
			<td class="paramlist_key">
				<label for="ads_opt_enable_antispam_bot"><?php echo JText::_("ADS_PLG_ENABLE");?></label>
			</td>
			<td>
				<fieldset id="ads_opt_enable_antispam_bot" class="radio" style="border:0 none !important;">
					<?php echo $lists['ads_opt_enable_antispam_bot'];?>
				</fieldset>	
			</td>
		</tr>
		<tr>
			<td width="20%" class="paramlist_key">
				<label for="ads_opt_choose_antispam_bot"><?php echo JText::_("ADS_CHOOSE_PLG");?></label>
			</td>
			<td>
				<fieldset id="ads_opt_choose_antispam_bot" class="radio" style="border:0 none !important;">
					<?php echo $lists['ads_opt_choose_antispam_bot']; ?>
				</fieldset>	
			</td>
		</tr>
	</table>
	<table class="adminlist" id="tab_joomla" width="100%">
		<tr>
			<td width="100%" class="paramlist_description">
                <?php echo JText::_('ADS_JOOMLA_STANDARD_EMAIL_CLOACKING');?>
			</td>
		</tr>
	</table>
	<table class="adminlist" id="tab_smarty" width="100%">
		<tr>
			<td width="100%" class="paramlist_description">
                <?php echo JText::_('ADS_SMARTY_PLUGIN_CLOACKING');?>
			</td>
		</tr>
	</table>
	<table class="adminlist" id="tab_recaptcha">
		<tr>
			<td colspan="2" class="paramlist_description">
                <?php echo JText::_('ADS_GET_KEYS_FOR_MAILCAPTCHA_FROM');?><a target="_blank" href="http://www.google.com/recaptcha/mailhide/apikey">www.google.com/recaptcha/mailhide/apikey</a>
			</td>
		</tr>
		<tr class="hasTip" title="<?php echo JText::_('ADS_MAILCAPTCHA_PUBLIC_KEY'); ?>::<?php echo JText::_('ADS_MAILCAPTCHA_PUBLIC_KEY'); ?>">
			<td width="20%" class="paramlist_key">
			  <span class="editlinktip">
			    <label for="mailcaptcha_public_key"><?php echo JText::_('ADS_MAILCAPTCHA_PUBLIC_KEY'); ?></label>
			  </span>
			</td>
			<td class="paramlist_value">
			    <input type="text" name="ads_opt_mailcaptcha_public_key" id="ads_opt_mailcaptcha_public_key" value="<?php echo $lists["mailcaptcha"]["public_key"]; ?>" style="width:280px;" />
			</td>
		</tr>
		<tr class="hasTip" title="<?php echo JText::_('ADS_MAILCAPTCHA_PRIVATE_KEY'); ?>::<?php echo JText::_('ADS_MAILCAPTCHA_PRIVATE_KEY'); ?>">
			<td width="20%" class="paramlist_key">
				<span class="editlinktip">
					<label for="mailcaptcha_private_key"><?php echo JText::_('ADS_MAILCAPTCHA_PRIVATE_KEY'); ?></label>
				</span>
			</td>
			<td class="paramlist_value">
			    <input type="text" name="ads_opt_mailcaptcha_private_key" id="ads_opt_mailcaptcha_private_key" value="<?php echo $lists["mailcaptcha"]["private_key"]; ?>" style="width:280px;" />
			</td>
		</tr>
	</table>
	
</fieldset>

<script>
toggleMailCaptcha("<?php echo defined("ads_opt_choose_antispam_bot")?ads_opt_choose_antispam_bot:"joomla";?>");
</script>

