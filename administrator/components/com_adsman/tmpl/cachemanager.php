<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/
?>

<fieldset class="adminform">
	<legend><span style="vertical-align:middle;"><?php echo JText::_( 'ADS_SMARTY_CACHE' ); ?></span></legend>
	<table class="paramlist admintable">
		<?php JTheFactoryAdminHelper::writableCell("components/com_adsman/templates/cache",1,"Smarty cache Dir ",0,"index.php?option=com_adsman&task=dirchmode&dir=components/com_adsman/templates/cache");?>
		<tr>
			<td></td>
		</tr>
		<tr>
			<td class="paramlist_key" width="150"><?php echo JText::_( 'ADS_CACHED_FILES_NO');?></td>
			<td><?php echo $lists["cache_stats"]["no_files"];?></td>
		</tr>
		<tr>
			<td class="paramlist_key" width="150"><?php echo JText::_( 'ADS_CACHED_SIZE');?></td>
			<td><?php printf ("%.2f", $lists["cache_stats"]["cache_size"]/1024);?> kb</td>
		</tr>
		<tr>
			<td class="paramlist_key" width="150"><?php echo JText::_( 'ADS_LAST_CACHED_FILE');?></td>
			<td><?php echo $lists["cache_stats"]["last_cached"];?></td>
		</tr>
		<tr>
			<td class="paramlist_key" width="150"><?php echo JText::_( 'ADS_FIRST_CACHED_FILE');?></td>
			<td><?php echo $lists["cache_stats"]["first_cached"];?></td>
		</tr>
		<tr>
			<td colspan="2"><hr /></td>
		</tr>
		<tr>
			<td colspan="2">
			<img src="<?php echo JURI::root();?>administrator/components/com_adsman/img/cpanel.png" style='width:20px !important; vertical-align:middle;' />	
			<a href='index.php?option=com_adsman&task=purgecache'><?php echo JText::_( 'ADS_PURGE_ALL_CACHE');?></a>
			</td>
		</tr>
	</table>
</fieldset>