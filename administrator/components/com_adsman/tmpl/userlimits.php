<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/
?>

<fieldset class="adminform">
	<legend><!--<img src="<?php echo JURI::root();?>/administrator/components/com_adsman/img/settings.png" style="vertical-align:middle;" />-->
		<?php echo JText::_( 'ADS_USER_RESTRICTIONS' ); ?></legend>
<table class="adminlist">
  <tr>
	<td valign="top">
		<fieldset class="batch">
			<legend><img src="<?php echo JURI::root();?>administrator/components/com_adsman/img/addedit.png" style='height:25px; vertical-align:middle;' /><?php echo JText::_( 'ADS_USER_LIMITS' ); ?></legend>
		<table class="adminlist" width="100%">
		<tr>
			<td width="30">
			<span style="float: left;">
				<span class="tooltip_right"><?php echo JText::_("ADS_GLOBAL_LIMIT"); echo "&nbsp;"; ?></span>
			 	<span class="tooltip_right"><?php echo JHTML::_('tooltip', JText::_("ADS_HELP_GLOBAL_LIMIT")); ?></span>
			 </span>	  
			 <input type="text" name="global_limit" value="<?php if(defined('ads_opt_global_limit')) echo ads_opt_global_limit;?>" />
			</td>
			<td></td>
		</tr>
		<tr>
			<th colspan="2"><hr />
				<span class="tooltip_right"><?php echo JText::_("ADS_LIMITS_GROUP"); echo "&nbsp;"; ?></span>
				<span class="tooltip_right"><?php echo JHTML::_('tooltip', JText::_("ADS_HELP_LIMITS_GROUP"));?></span>
			</th>
		</tr>
		<tr>
			<td width="20%">
				
			  <table>
					<?php foreach ($lists["users_group_limits"] as $k => $g){  ?>
					<tr>
						<td><a href="index.php?option=com_adsman&task=delete_limit&owner=<?php echo $g->id;?>&type=group"><?php echo JText::_('ADS_X_DELETE');?></a></td>
						<td width="100"><?php echo $g->title;?></td>
						<td width="30"><?php echo $g->limit_size; ?></td>
					</tr>
					<?php } ?>
			  </table>
			</td>
			<td>
				<a href="#" onclick="javascript:Joomla.submitbutton('savesettings');"><?php echo JText::_("ADS_ADD_LIMIT"); ?></a>
				<table class="adminlist" style="width:50%;">
					<thead>
						<tr>
							<th width="1%">
								<input type="checkbox" name="checkall-toggle" value="" onclick="checkAll(this)" />
							</th>
							<th class="left" width="100">
								<?php echo JText::_('ADSMAN_HEADING_GROUP_TITLE'); ?>
							</th>
							<th width="5%">
								<?php echo JText::_('JGRID_HEADING_ID'); ?>
							</th>
						</tr>
					</thead>
				<tbody>
				<?php $user    =& JFactory::getUser();
				 foreach ($lists["users_groups"] as $i => $item) :
					$canCreate	= $user->authorise('core.create',		'com_users');
					$canEdit	= $user->authorise('core.edit',			'com_users');
					//$allowed_groups = $this->wallSettings->allowed_wallowners_groups;
					// If this group is super admin and this user is not super admin, $canEdit is false
					if (!$user->authorise('core.admin') && (JAccess::checkGroup($item->id, 'core.admin'))) {
						$canEdit = false;
					}
					$canChange	= $user->authorise('core.edit.state',	'com_users');
				?>
			<tr class="row<?php echo $i % 2; ?>">
				<td class="center">
					<?php //$state = (in_array($item->id, $allowed_groups)) ? ('checked="checked"') : (''); ?>
						<input type="checkbox" name="limit_group[]" value="<?php echo $item->id;?>" <?php //echo $state; ?> title="<?php echo JText::_( 'ADS_GROUP' ); ?>" />
				</td>
				<td>
					<?php echo str_repeat('<span class="gi">|&mdash;</span>', $item->level) ?>
					<?php echo $item->title; ?>
				</td>
				<td class="center">
					<?php echo (int) $item->id; ?>
				</td>
			</tr>
			<?php endforeach; ?>
			<tr>
				<td><?php echo JText::_("ADS_LIMIT"); ?>:</td>
				<td colspan="2"><input type="text" name="limit_group_size" /></td>
			</tr>
 		</tbody>
	</table>
  </td>
 </tr>
 
<tr>
	<th colspan="2"><hr />
			<span class="tooltip_right"><?php echo JText::_("ADS_LIMITS_USER"); echo "&nbsp;"; ?></span>
			<span class="tooltip_right"><?php echo JHTML::_('tooltip', JText::_("ADS_HELP_LIMITS_USER"));?></span>
	</th>
	</tr>
	<tr>
		<td width="20%">
			<table>
				<?php foreach ($lists["users_user_limits"] as $k => $g){  ?>
				<tr>
					<td><a href="index.php?option=com_adsman&task=delete_limit&owner=<?php echo $g->id;?>&type=user"><?php echo JText::_('ADS_X_DELETE');?></a></td>
					<td width="100"><?php echo $g->name;?></td><td width="30"><?php echo $g->limit_size; ?></td>
				</tr>
				<?php } ?>
			</table>
		</td>
		<td>
			<a href="#" onclick="javascript:Joomla.submitbutton('savesettings');"><?php echo JText::_("ADS_ADD_LIMIT"); ?></a>
			<table>
				<tr>
					<td><?php echo JText::_("ADS_USER"); ?>:</td>
					<td>
					<select name="limit_user[]" multiple>
						<?php foreach ($lists["users"] as $k => $item) { ?>
						<option value="<?php echo $item->value; ?>"><?php echo $item->text; ?></option>
						<?php } ?>
					</select>
					</td>
				</tr>
				<tr>
					<td><?php echo JText::_("ADS_LIMIT"); ?>:</td>
					<td><input type="text" name="limit_user_size" /></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</fieldset>

</td>
</tr>
</table>
</fieldset>

