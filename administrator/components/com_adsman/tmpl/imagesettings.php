<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/
?>

<fieldset>
  <legend><?php echo JText::_( 'ADS_IMAGE_SETTINGS' ); ?></legend>
	<table class="adminlist">
	<tr>
	<td valign="top">
	<fieldset>
			<legend><img src="<?php echo JURI::root();?>administrator/components/com_adsman/img/image_32.png" />
				<span><?php echo JText::_( 'ADS_IMAGE_SIZE_OPTIONS' ); ?></span>
				</legend>
		<table class="adminlist" width="50%">
	    <tr class="hasTip" title="<?php echo JText::_('ADS_GD_NEEDS_INSTALL');?>">
	    	<td width="25%" class="paramlist_key">
	    		<strong><?php echo JText::_("ADS_GD_VERSION");?>:</strong>
	    	</td>
	    	<td class="paramlist_value">
	    		<?php 
	    		$gdVersion = JTHEF_get_gd_version();
	    		if($gdVersion!=false) echo $gdVersion; else echo JText::_("ADS_GD_NOT_INSTALLED");
	    		?>
	    	</td>
	    </tr>
	    <tr>
	    	<th colspan="2" align="left">
	    		<span class="tooltip_right"><?php echo JText::_("ADS_IMAGE_THUMB_SETTINGS"); echo "&nbsp;"; ?></span>
	    		<span class="tooltip_right"><?php echo JHTML::_('tooltip', JText::_("ADS_HELP_RESIZE_THUMB")); ?></span>
	    	</th>
	    </tr>
	    <tr>
			<td class="paramlist_key">
				<span style="margin-left:10px">&nbsp;</span><?php echo JText::_("ADS_IMAGE_THUMB_WIDTH_RESIZE"); ?>:&nbsp;
			</td>
			<td class="paramlist_value">
				<input name="thumb_resize_width" value="<?php echo ads_opt_thumb_width;?>" size="5"/> (px)
			</td>
	    </tr>
	    <tr>
	    	<td class="paramlist_key">
				<span style="margin-left:10px">&nbsp;</span><?php echo JText::_("ADS_IMAGE_THUMB_HEIGHT_RESIZE"); ?>:&nbsp;
	    	</td>
	    	<td class="paramlist_value">
	    		<input name="thumb_resize_height" value="<?php echo ads_opt_thumb_height;?>"  size="5"/> (px)
	    	</td>
	    </tr>
	    <tr>
			<th colspan="2"  align="left">
				<span class="tooltip_right"><?php  echo JText::_("ADS_IMAGE_MEDIUM_SETTINGS"); echo "&nbsp;"; ?></span>
				<span class="tooltip_right"><?php echo JHTML::_('tooltip',JText::_("ADS_HELP_RESIZE_MEDIUM")); ?></span>
			</th>
		</tr>
		<tr>
			<td class="paramlist_key">
				<span style="margin-left:10px">&nbsp;</span><?php echo JText::_("ADS_IMAGE_MEDIUM_WIDTH_RESIZE"); ?>:&nbsp;
			</td>
			<td class="parmlist_value">
				<input name="medium_resize_width" value="<?php echo ads_opt_medium_width;?>" size="5" /> (px)
			</td>
		</tr>
		<tr>
			<td class="paramlist_key">
				<span style="margin-left:10px">&nbsp;</span><?php echo JText::_("ADS_IMAGE_MEDIUM_HEIGHT_RESIZE"); ?>:&nbsp;
			</td>
			<td class="parmlist_value">
				<input name="medium_resize_height" value="<?php echo ads_opt_medium_height;?>" size="5" /> (px)
			</td>
		</tr>
		<tr>
			<td class="paramlist_key">
				<?php echo JText::_("ADS_MAX_PICTURE_SIZE"); ?>:&nbsp;
			</td>
			<td class="paramlist_value">
				<input name="ads_opt_max_picture_size" value="<?php echo ads_opt_max_picture_size?>" size="12" onkeyup="document.getElementById('tf_mega_size').innerHTML=this.value/1024;"> (Kb)
				 - <span id="tf_mega_size"><?php echo ads_opt_max_picture_size/1024;?></span> (MB)<br />
				&nbsp;
				<p>
				<input name="ads_opt_resize_if_larger" type="checkbox" value="1" <?php if (ads_opt_resize_if_larger) echo "checked";?>> 
					<span style="float:left;">&nbsp;<?php echo JText::_("ADS_RESIZE_IF_EXCEEDS");  ?>&nbsp;</span>
					<span style="float:left;">&nbsp;
					<?php echo JHTML::_('tooltip', JText::_("ADS_RESIZE_IF_EXCEEDS_HELP")); ?>
					</span>	  
					
				</p>	
			</td>
		</tr>
	</table>	
	</fieldset>
	
	</td>
	<td valign="top">
	  <fieldset>
				<legend><img src="<?php echo JURI::root();?>administrator/components/com_adsman/img/app_48.png" style='height:32px; vertical-align:middle;' />
				<?php echo JText::_( 'ADS_LISTING_SETTINGS' ); ?></legend>
		<table class="adminlist" width="50%">
			<tr>
			<th colspan="2" align="left">
				<?php  echo JText::_("ADS_IMAGE_SETTINGS"); ?>
			</th>
			</tr>
		    <tr class="hasTip" title="<?php echo JText::_('ADS_IMAGES_UPLOAD_NO');?>">
		    	<td class="paramlist_key" width="25%">
		        	<?php echo JText::_("ADS_IMAGE_NR_MAX");?>:&nbsp;
		        </td>	
		        <td class="paramlist_value">
		        	<input name="max_nr_img" value="<?php echo ads_opt_maxnr_images ?>" size="3">
		    	</td>
		    </tr>
		    <tr>
				<td class="paramlist_key">
					<?php echo JText::_("ADS_REQUIRE_IMAGE"); ?>:&nbsp;
				</td>
				<td class="paramlist_value">
					<input name="require_image" <?php echo ads_opt_require_picture?"checked":"" ?> type="checkbox" value="1">
				</td>
		     </tr>
			<tr>
				<td class="paramlist_key"><?php  echo JText::_("ADS_IMAGE_GALLERY");  ?></td>
				<td class="paramlist_value">
				<?php  echo $lists["gallery_plugin"];?></td>
			</tr>
		</table>
	  </fieldset>
	</td>
	</tr>
  </table>
  
  
		
    
</fieldset>	
<?php
function JTHEF_get_gd_version(){
	$gd = array();
	ob_start();
	@phpinfo(INFO_MODULES);
	$output=ob_get_contents();
	ob_end_clean();
	
	if(preg_match("/GD Version[ \t]*(<[^>]+>[ \t]*)+([^<>]+)/s",$output,$matches)){
		return $matches[2];
	}
	
	return false;
}

?>