<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/
?>

<fieldset class="adminform">
	<legend><?php echo JText::_("ADS_TERMS"); ?></legend>
 <div style="width:90%;">
	<table width="100%">
	<tr>
		<td colspan="2">
			<?php 
			 echo $editor->display( 'terms_conditions',  $lists["terms"] , '100%', '550', '75', '20' ) ;
			?>
		</td>
	</tr>
	</table>
 </div>
</fieldset>