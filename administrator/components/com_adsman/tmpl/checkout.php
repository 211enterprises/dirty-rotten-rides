<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/
?>
<fieldset>
    <legend><?php echo JText::_( 'CHECKOUT_SETTINGS' ); ?></legend>

    <table class="adminlist">
        <tr>
            <td width="15%" class="paramlist_key">
                <?php echo JText::_("CHECKOUT_SETUP");?>
            </td>
            <td>
                <fieldset id="ads_opt_choose_checkout" class="radio" style="border:0 none !important;">
                    <?php echo $lists['ads_opt_choose_checkout']; ?>
                </fieldset>
            </td>
        </tr>
    </table>

</fieldset>
