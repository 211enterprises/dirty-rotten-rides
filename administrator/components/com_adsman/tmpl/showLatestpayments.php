<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

$db		= JFactory::getDBO();

JHTML::_('behavior.tooltip');
JHTML::_('behavior.caption');
JHtml::_('behavior.framework');

echo '<table class="adminlist" cellspacing="1">';
        //$header_row[] = array("width"=>"10","th"=>"#");
        $header_row[] = array("width"=>"15%","th"=>JText::_("ADS_USER"),"class"=>"ads_align_left");
        $header_row[] = array("width"=>"9%","th"=>JText::_("ADS_DATE"),"class"=>"ads_align_left");
        $header_row[] = array("width"=>"*%","th"=>JText::_("ADS_ITEM"),"class"=>"ads_align_left");
        $header_row[] = array("width"=>"5%","th"=>JText::_("ADS_PAYMENT_ITEM"),"class"=>"ads_align_left");
        $header_row[] = array("width"=>"7%","th"=>JText::_("ADS_PAYMENT_ITEM_AMOUNT"),"class"=>"ads_align_left");
        $header_row[] = array("width"=>"5%","th"=>JText::_("ADS_PAYMENT_ITEM_CURRENCY"),"class"=>"ads_align_left");
        $header_row[] = array("width"=>"5%","th"=>JText::_("ADS_PAYMENT_ITEM_REFNO"),"class"=>"ads_align_left");
        $header_row[] = array("width"=>"5%","th"=>JText::_("ADS_PAYMENT_ITEM_INVOICE_NO"),"class"=>"ads_align_left");
        $header_row[] = array("width"=>"5%","th"=>JText::_("IPN"),"class"=>"ads_align_left");
        $header_row[] = array("width"=>"5%","th"=>JText::_("ADS_STATUS"));

    if (count($header_row)){
      echo "<thead>";
        echo "<tr>";
    	  foreach ($header_row as $header)
    	  {
    	      if (is_array($header)){
    	          $attribs='';

    	          if (isset($header["width"]))
					$attribs.=' width="'.$header["width"].'"';

				  if(isset($header["align"]))
				  	$attribs.=' align="'.$header["align"].'"';

				  if (isset($header["class"]))
				  	$attribs.=' class="'.$header["class"].'"';

				  if(isset($header["attributes"]))
				  	$attribs.=' '.$header["attributes"].' ';

    	          echo "<th $attribs>".JText::_($header["th"])."</th>";
    	      } else {
    	          echo "<th>".JText::_($header)."</th>";
    	      }
    	  }
    	  echo "</tr>";
    	  echo "</thead>";
    }

    $k=0;
    for ($i=0;$i<count($latestPayments);$i++){

	   $row = $latestPayments[$i];
       ?>
       	 <tr class="<?php echo 'row',$k; ?>">
       	<?php
            for($j=1;$j<count($row);$j++){

                $r = $row[$j];

                if (is_array($r)) {
    	          $attribs='';

    	          if ($r["width"])
    	          	$attribs.=' width="'.$r["width"].'"';

    	          if ($r["align"])
    	          	$attribs.=' align="'.$r["align"].'"';

    	          if ($r["class"])
    	          	$attribs.=' class="'.$r["class"].'"';

    	          $attribs.=' '.$r["attributes"]." ";
                }
                else
                {
                    if ($j == 5)
                        echo "<td class='ads_align_right'>$r</td>";
                    elseif ($j == 10)
                        echo "<td class='center'>$r</td>";
                    else
                        echo "<td>$r</td>";
                }
            }
         echo "</tr>";
         $k = 1-$k;
       }
    echo "</table>";
?>
