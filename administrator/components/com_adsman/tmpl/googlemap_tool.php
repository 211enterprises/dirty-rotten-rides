<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<body onload="initialize()" onunload="GUnload()">

<h3 style="color:#FAA000; text-decoration:underline;"><?php echo JText::_('ADS_PAGE_GOOGLEMAPTOOL'); ?></h3>
<div>
<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

    if (ads_opt_google_key!="") { ?>

	<div id="map_canvas" ></div>
	<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=<?php echo ads_opt_google_key;?>" type="text/javascript"></script>
	<script type="text/javascript">
	var pointTox=this.parent.document.getElementById("googleX").value;
	var pointToy=this.parent.document.getElementById("googleY").value;
    var zoom = this.parent.document.getElementById("zoom_optic").value;

	function getSliderValue(val_param,index){
    		var val = String(val_param);
    		var vals = Array[1];
    		vals = val.split(",");
    		return vals[index];
    }
    
    function initialize() {
		if (GBrowserIsCompatible()) {

			//var map = new GMap2(document.getElementById("map_canvas"));
			var map = new GMap2(document.getElementById("map_canvas"),
				{ size: new GSize(420,320) } );
			
			var point = new GLatLng(pointTox, pointToy);
			var centerpoint = new GLatLng(pointTox, pointToy);
			var mapControl = new GMapTypeControl();



	        GEvent.addListener(map,"click", function(overlay,latlng) {
	          if (latlng) {
	
	          	var myHtml = latlng;
	            var str = new String(myHtml);
	
	            myHtml = str.substring(1,str.length-1);
	
	            document.getElementById("txt1").value = getSliderValue(myHtml,0);
	            document.getElementById("txt2").value = getSliderValue(myHtml,1);
	          }
	        });
	        
	        GEvent.addListener(map,"zoomend", function() {
				document.getElementById("txt_zoom").value = this.getZoom();
	        });
			map.addControl(mapControl);
			map.addControl(new GLargeMapControl());
			map.setCenter(centerpoint, zoom);
			map.addOverlay(new GMarker(point));
	  }
	}

    </script>
<?php } else echo JText::_('ADS_ERR_NO_MAP'); ?>
</div>


	<div style="text-align:center;width: 500px; ">
		<input type="text" readonly="readonly" id="txt_zoom" size="3" /><a href="#" style="color:#FAA000; font-weight:bold;" onClick="submitPopupZoom()"><?php echo "Select zoom";?></a>
		<hr />
		<input type="text" readonly="readonly" id="txt1" /><input type="text" readonly="readonly" id="txt2" /><a href="#" style="color:#FAA000; font-weight:bold;" onClick="submitPopup()"><?php echo JText::_('ADS_SELECT'); ?></a>
	</div>
<script type="text/javascript">
function submitPopup(){
	this.parent.selectGoogleMap_Choords(document.getElementById("txt1").value,document.getElementById("txt2").value);
	//this.parent.content.SqueezeBox.close();
		
}
function submitPopupZoom(){
	this.parent.selectGoogleMap_zoom(document.getElementById("txt_zoom").value);
	//this.parent.content.SqueezeBox.close();
		
}
document.getElementById("txt1").value=pointTox;
document.getElementById("txt2").value=pointToy;

document.getElementById("txt_zoom").value=zoom;
</script>
  </body>
</html>
