<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/
?>
<fieldset>
    <legend><span style="vertical-align: middle;"><?php echo JText::_( 'ADS_MISC_SETTINGS' ); ?></span></legend>
	<table class="adminlist">
	<tr>
		<td valign="top">
		
		<fieldset>
			<legend><img src="<?php echo JURI::root();?>administrator/components/com_adsman/img/month_f2.png" style='height:25px; vertical-align:middle;' />
				<span><?php echo JText::_( 'ADS_DATE_TIME_OPTIONS' ); ?></span>
				</legend>
			<table class="adminlist" width="100%">
			<tr>
				<td class="paramlist_key"><?php echo JText::_("ADS_TIME_ZONE"); ?></td>
				<td class="paramlist_value">
					<?php echo $lists["ads_opt_component_offset"];?>
				</td>
			</tr>
			<tr>
				<td class="paramlist_key"><?php  echo JText::_("ADS_DATE_FORMAT"); ?></td>
				<td class="paramlist_value"><?php echo $lists["date_format"];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span id="datef"><?php echo date(ads_opt_date_format);?></span></td>
			</tr>
			<tr>
				<td class="paramlist_key"><?php echo JText::_("ADS_TIME_FORMAT"); ?></td>
				<td class="paramlist_value">
					<?php echo $lists["date_time_format"];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<span id="timef" class="small"><?php echo date(ads_opt_date_time_format);?></span>
				</td>
			</tr>
    	  </table>
		</fieldset>
			
		<fieldset>
			<legend><img src="<?php echo JURI::root();?>administrator/components/com_adsman/img/tool_f2.png" style='height:25px; vertical-align:middle;' />
				<?php echo JText::_( 'ADS_LISTING_SETTINGS' ); ?></legend>
				<table class="adminlist" width="100%">
			      <tr>
			      	<td class="paramlist_key">
			      		<span class="tooltip_right"><?php echo JText::_("ADS_ITEMSPERPAGE"); echo "&nbsp;"; ?></span>
			      		<span class="tooltip_right"><?php echo JHTML::_('tooltip', JText::_("ADS_ITEMSPERPAGE_HELP"));?></span>:</td>
			      	<td class="paramlist_value"><input type="text" name="items_per_page" style="width:30px;" class="inputbox" value="<?php echo ads_opt_nr_items_per_page; ?>" />
			      	</td>
			      </tr>
			      <tr>
			        <td class="paramlist_key">
			        	<span class="tooltip_right"><?php echo JText::_("ADS_ENABLE_PRICE_FIELD"); echo "&nbsp;"; ?></span>
			        	<span class="tooltip_right"><?php echo JHTML::_('tooltip', JText::_("ADS_ENABLE_PRICE_FIELD_HELP"));?></span>:
			        </td>
			        <td class="paramlist_value">
			        	<fieldset id="ads_opt_price_enable" class="radio" style="border:0 none !important;">
			            <?php
			            $ads_price = 1;
			            if ( defined('ads_opt_price_enable') && ads_opt_price_enable!='' )
			            	$ads_price = ads_opt_price_enable;
			            echo JHTML::_('select.booleanlist','ads_opt_price_enable','',$ads_price);
			            ?>
			            </fieldset>
					</td>
					<tr>
						<td class="paramlist_key">
						<span class="tooltip_right"><?php echo JText::_("ADS_POST_EDITOR_BUTTONS"); echo "&nbsp;"; ?></span>
						<span class="tooltip_right"><?php echo JHTML::_('tooltip', JText::_("ADS_POST_EDITOR_BUTTONS_HELP"));?></span>:</td>
						<td class="paramlist_value">
						  <fieldset id="ads_opt_post_editor_buttons" class="radio" style="border:0 none !important;">	
							<?php echo $lists['ads_opt_post_editor_buttons']; ?>
						  </fieldset>
						</td>
					</tr>
			      </tr>
					<tr>
						<td class="paramlist_key"><?php echo JText::_("ADS_PRICE_COMPULSORY");?>:</td>
						<td class="paramlist_value"><input name="ads_price_compulsory" type="checkbox" value="1" <?php echo (ads_price_compulsory)?"checked":""; ?>></td>
					</tr>
			      <tr>
			        <td class="paramlist_key">
			        <span class="tooltip_right"><?php echo JText::_("ADS_ENABLE_AD_TYPE_FIELD");  echo "&nbsp;"; ?></span>
			        <span class="tooltip_right"><?php echo JHTML::_('tooltip', JText::_("ADS_ENABLE_AD_TYPE_FIELD_HELP"));?></span>:</td>
			        <td class="paramlist_value">
			         <fieldset id="ads_opt_adtype_enable" class="radio" style="border:0 none !important;">
			            <?php
			            $ads_adtype = 1;
			            if ( defined('ads_opt_adtype_enable') && ads_opt_adtype_enable!='' )
			            	$ads_adtype = ads_opt_adtype_enable;
			            echo JHTML::_('select.booleanlist','ads_opt_adtype_enable',' onchange="if(this.value==\'1\') document.getElementById(\'ads_opt_adtype_val\').style.display=\'none\'; else  document.getElementById(\'ads_opt_adtype_val\').style.display=\'\';" ',$ads_adtype);
			            ?>
			           </fieldset> 
					</td>
			      </tr>
			      <tr>
			        <td class="paramlist_key">
			        <span class="tooltip_right"><?php echo JText::_("ADS_MESSAGES_LIST_PRIVATE");  echo "&nbsp;"; ?></span>
			        <span class="tooltip_right"><?php echo JHTML::_('tooltip', JText::_("ADS_MESSAGES_LIST_PRIVATE_HELP"));?></span>:</td>
			        <td class="paramlist_value">
			          <fieldset id="ads_opt_messages_public" class="radio" style="border:0 none !important;">
			            <?php
			            
			            if ( defined('ads_opt_messages_public') && ads_opt_messages_public!='' )
			            	$public_messages = ads_opt_messages_public;
			            else 
			            	$public_messages = 0;	
			            echo JHTML::_('select.booleanlist','ads_opt_messages_public','',$public_messages);
			            ?>
			          </fieldset>
					</td>
			      <tr>
			        <td class="paramlist_key">
			        	<span class="tooltip_right"><?php echo JText::_("ADS_AD_TYPE_DEFAULT_VALUE"); echo "&nbsp;"; ?></span>
			        	<span class="tooltip_right"><?php echo JHTML::_('tooltip', JText::_("ADS_AD_TYPE_DEFAULT_VALUE_HELP"));?></span>:</td>
			        <td class="paramlist_value">
			          			            
			            <?php
			            $style="";
			            if($ads_adtype == 1){
			            	$style="style='display:none;'";
			            }
			            $ads_adtype_default = "";
			            if ( defined('ads_opt_adtype_val') && ads_opt_adtype_val!='' )
			            	$ads_adtype_default = ads_opt_adtype_val;
			            	
						$opts = array();
						$opts[] = JHTML::_('select.option', ADDSMAN_TYPE_PUBLIC, JText::_('ADS_ADTYPE_PUBLIC'));
						$opts[] = JHTML::_('select.option', ADDSMAN_TYPE_PRIVATE,JText::_('ADS_ADTYPE_PRIVATE'));
						echo JHTML::_('select.genericlist',  $opts, 'ads_opt_adtype_val', $style.'class="inputbox" id="ads_opt_adtype_val"',  'value', 'text',$ads_adtype_default);
			            ?>
			           
					</td>
			      </tr>
			      <tr>
			        <td class="paramlist_key" style="width:220px !important;">
			        	<span class="tooltip_right"><?php echo JText::_("ADS_ENABLE_PUBLISHED_STATUS_FIELD"); echo "&nbsp;"; ?></span>
			        	<span class="tooltip_right"><?php echo JHTML::_('tooltip', JText::_("ADS_ENABLE_PUBLISHED_STATUS_FIELD_HELP"));?></span>:</td>
			        <td class="paramlist_value">
			          <fieldset id="ads_opt_adpublish_enable" class="radio" style="border:0 none !important;">
			            <?php
			            $ads_adpublish = 1;
			            if ( defined('ads_opt_adpublish_enable') && ads_opt_adpublish_enable!='' )
			            	$ads_adpublish = ads_opt_adpublish_enable;
			            echo JHTML::_('select.booleanlist','ads_opt_adpublish_enable',' onchange="if(this.value==\'1\') document.getElementById(\'ads_opt_adpublish_val\').style.display=\'none\'; else  document.getElementById(\'ads_opt_adpublish_val\').style.display=\'\';" ',$ads_adpublish);
			            ?>
			          </fieldset>  
					</td>
			      </tr>
			      <tr>
			        <td class="paramlist_key">
			        	<span class="tooltip_right"><?php echo JText::_("ADS_STATUS_DEFAULT_VALUE"); echo "&nbsp;"; ?></span>
			        	<span class="tooltip_right"><?php echo JHTML::_('tooltip', JText::_("ADS_STATUS_DEFAULT_VALUE_HELP"));?></span>:</td>
			        <td class="paramlist_value">
			            <?php
			            $style="";
			            
			            if($ads_adpublish == 1){
			            	$style="style='display:none;'";
			            }
			            $ads_adstatus_default = "";
			            if ( defined('ads_opt_adpublish_val') && ads_opt_adpublish_val!='' )
			            	$ads_adstatus_default = ads_opt_adpublish_val;
			            	
						$opts = array();
						$opts[] = JHTML::_('select.option', 1, JText::_('ADS_PUBLISHED'));
						$opts[] = JHTML::_('select.option', 0, JText::_('ADS_UNPUBLISHED'));
						echo JHTML::_('select.genericlist',  $opts, 'ads_opt_adpublish_val', $style.'class="inputbox" id="ads_opt_adpublish_val"',  'value', 'text',$ads_adstatus_default);
			            ?>
					</td>
			      </tr>
			      <tr>
					<td class="paramlist_key"><?php echo JText::_("ADS_ENABLE_EXPIRATION_HOUR");?>:</td>
					<td class="paramlist_value"><input name="enable_hour" type="checkbox" value="1" <?php echo (ads_opt_enable_hour)?"checked":""; ?>></td>
				  </tr>
				  <tr>
					<td class="paramlist_key"><?php echo JText::_("ADS_ENABLE_COUNTDOWN");?>:</td>
					<td class="paramlist_value"><input name="enable_countdown" type="checkbox" value="1" <?php echo (ads_opt_enable_countdown)?"checked":""; ?>></td>
				  </tr>
				  
				  <tr>
					<td class="paramlist_key"><?php echo JText::_("ADS_ALLOW_FILE_ATACHMENTS");?>:</td>
					<td class="paramlist_value">
						<span style="float: left;"><input onclick="if(this.checked) document.getElementById('ads_require_attachment_container').style.display=''; else  document.getElementById('ads_require_attachment_container').style.display='none';" name="ads_allow_atachment" type="checkbox" value="1" <?php echo (defined("ads_allow_atachment") && ads_allow_atachment==1 )?"checked":""; ?>> 
							<?php echo JText::_("ADS_ENABLED"); ?>&nbsp;&nbsp;
						 </span>
						<span style="float: left;" id='ads_require_attachment_container' <?php if ( !defined("ads_allow_atachment") || ads_allow_atachment==0 ) { ?> style='display:none;' <?php } ?> >
						
								<input style="float: left;" name="ads_require_atachment" type="checkbox" value="1" <?php echo (defined("ads_require_atachment") && ads_require_atachment==1 )?"checked":""; ?>> 
								<?php echo JText::_("ADS_REQUIRED"); ?>
						</span>
					</td>
				</tr>
				<tr>
					<td class="paramlist_key">
						<span class="tooltip_right"><?php echo JText::_("ADS_SHORT_DESC_LONG"); echo "&nbsp;";?></span>
						<span class="tooltip_right"><?php echo JHTML::_('tooltip', JText::_("ADS_HELP_SHORT_DESC_LONG")); ?></span>: 
					</td>
					<td class="paramlist_value">
						<input class="hasTip" type="text" name="ads_short_desc_long" value="<?php echo ads_short_desc_long; ?>" title="<?php echo JText::_('ADS_CHARACTER_NUMBERS');?>">
					</td>
				</tr>
				<tr>
					<td class="paramlist_key"><?php echo JText::_("ADS_MAX_NR_TAGS_TITLE");?>: </td>
					<td class="paramlist_value"><input type="text" name="max_nr_tags" value="<?php echo ads_opt_max_nr_tags; ?>"></td>
				</tr>
                <tr>
                    <td class="paramlist_key">
                        <span class="tooltip_right"><?php echo JText::_("ADS_LISTS_ORDERED_BY");  echo "&nbsp;"; ?></span>
                        <span class="tooltip_right"><?php echo JHTML::_('tooltip', JText::_("ADS_LISTS_ORDERED_BY_HELP"));?></span>:
                    </td>
                    <td class="paramlist_value">

                      <fieldset id="ads_opt_sort_date" class="radio" style="border:0 none !important;">
                         <?php echo $lists['ads_opt_sort_date']; ?>
                       </fieldset>
                    </td>
                  </tr>

			  </table>
			</fieldset>
			
			</td>
			<td valign="top">
			<fieldset>
				<legend><img src="<?php echo JURI::root();?>administrator/components/com_adsman/img/app_48.png" style='height:25px; vertical-align:middle;' />
				<?php echo JText::_( 'ADS_GLOBAL_SETTINGS' ); ?></legend>
				<table class="adminlist" width="100%">
				<tr>
					<td class="paramlist_key" style="width:220px !important;"> <?php echo JText::_("ADS_ALLOW_MESSAGING"); ?>:&nbsp;</td>
					<td class="paramlist_value" colspan="4"><input name="allow_messaging" <?php echo ads_opt_allow_messaging?"checked":"" ?> type="checkbox" value="1"></td>
				</tr>
				<tr>
					<td class="paramlist_key" style="width:200px !important;">
						<span class="tooltip_right"><?php echo JText::_("ADS_EXTEND_DAYS"); echo "&nbsp;"; ?></span>
						<span class="tooltip_right"><?php echo JHTML::_('tooltip', JText::_("ADS_HELP_EXTEND_DAYS")); ?></span>: 
					</td>
					<td class="paramlist_value" colspan="4">
					   <input type="text" name="extend_days" style="width:30px;" value="<?php if(defined("ads_opt_extend_days")) echo ads_opt_extend_days; ?>">
					</td>
				</tr>
				
				<tr>
					<td class="paramlist_key" >
					<span class="tooltip_right"><?php echo JText::_("ADS_MAX_MONTHS_OFFER"); echo "&nbsp;"; ?></span>
					<span class="tooltip_right"><?php echo JHTML::_('tooltip', JText::_("ADS_HELP_MAX_MONTHS"));?></span>:</td>
					<td class="paramlist_value" colspan="4">
						<input type="text" name="max_nr_months" style="width:30px;" class="inputbox" value="<?php echo ads_opt_availability; ?>">
						<p class="smallsub"> <?php echo JText::_("ADS_MONTHS"); ?></p>
					</td>
				</tr>
				
				<tr align="left">
					<td><span style="float:left;"><?php echo JText::_( 'ADS_CURRENCIES' ); ?></span></td>
					<td valign="top" width="40">
					 	<input name="temp_currency" id="temp_currency" value="">
					 	<br/><input type="button" class="button" value="<?php echo JText::_("ADS_ADD"); ?>" onclick="addCurreny();">
					</td>
				    <td width="50">
						<select name="currency_list" id="currency_list" size="5" multiple>
						<?php
							for ($i=0; $i<count($lists['currency']); $i++){
								echo "<option value='".$lists['currency'][$i]->name."'>".$lists['currency'][$i]->name."</option>";
							}
						?>
						</select>
				  	</td>
				    <td valign="top" width="40">
						<input type="button" class="button" value="<?php echo JText::_("ADS_DELETE");?>" onclick="delCurrency();">
				    </td>
				    <td>&nbsp;</td>
				 </tr>
				</table>
			</fieldset>
			<fieldset>
				<legend><img src="<?php echo JURI::root();?>administrator/components/com_adsman/img/categories.png" style='height:25px; vertical-align:middle;' /><?php echo JText::_( 'ADS_CATEGORY_SETTINGS' ); ?></legend>
				<table class="adminlist" width="100%">
			    <tr>
					<td class="paramlist_key" style="width:320px !important;">
						<span class="tooltip_right"><?php echo JText::_("ADS_CATEG_SEF_TABLE"); echo "&nbsp;"; ?></span>
						<span class="tooltip_right"><?php echo JHTML::_('tooltip', JText::_("ADS_CATEG_SEF_TABLE_HELP")); ?></span>:&nbsp;</td>
					<td class="paramlist_value">
						<a href="index.php?option=com_adsman&task=sef_category">Create</a>
					</td>
			     </tr>
			    <tr>
					<td class="paramlist_key">
						<span class="tooltip_right"><?php echo JText::_("ADS_CATEGORY_PAGE"); echo "&nbsp;"; ?></span>
						<span class="tooltip_right"><?php echo JHTML::_('tooltip', JText::_("ADS_CATEGORY_PAGE_HELP")); ?></span>:&nbsp;</td>
					<td class="paramlist_value"><input name="enable_category_page" <?php echo ads_opt_category_page?"checked":"" ?> type="checkbox" value="1" /></td>
			     </tr>
				<tr>
					<td class="paramlist_key">
						<span class="tooltip_right"><?php echo JText::_("ADS_INNER_CATEGORIES"); echo "&nbsp;"; ?></span>
						<span class="tooltip_right"><?php echo JHTML::_('tooltip', JText::_("ADS_INNER_CATEGORIES_HELP"));?></span>:</td>
					<td class="paramlist_value">
					  <fieldset id="ads_opt_inner_categories" class="radio" style="border:0 none !important;">	
						<?php echo $lists['ads_opt_inner_categories'];  ?>
					  </fieldset>	
					</td>
				</tr>
				<tr>
					<td class="paramlist_key">
						<span class="tooltip_right"><?php echo JText::_("ADS_CATEGORY_ROOT_POST"); echo "&nbsp;"; ?></span>
						<span class="tooltip_right"><?php echo JHTML::_('tooltip', JText::_("ADS_CATEGORY_ROOT_POST_HELP"));?></span>:</td>
					<td class="paramlist_value">
					  <fieldset id="ads_opt_category_root_post" class="radio" style="border:0 none !important;">	
						<?php echo $lists['ads_opt_category_root_post'];  ?>
					  </fieldset>	
					</td>
				</tr>
				<tr>
					<td class="paramlist_key">
						<span class="tooltip_right"><?php echo JText::_("ADS_CATEGORY_AJAX_ENABLE"); echo "&nbsp;"; ?></span>
						<span class="tooltip_right"><?php echo JHTML::_('tooltip', JText::_("ADS_CATEGORY_AJAX_ENABLE_HELP"));?></span>:</td>
					<td class="paramlist_value">
					  <fieldset id="ads_opt_category_ajax_load" class="radio" style="border:0 none !important;">	
						<?php echo $lists['ads_opt_category_ajax_load']; ?>
					  </fieldset>	
					</td>
				</tr>
                <tr>
                    <td class="paramlist_key">
                        <span class="tooltip_right"><?php echo JText::_("ADS_CATEGORY_TRANSLITERATE"); echo "&nbsp;"; ?></span>
                        <span class="tooltip_right"><?php echo JHTML::_('tooltip', JText::_("ADS_CATEGORY_TRANSLITERATE_HELP"));?></span>:</td>
                    </td>
                    <td class="paramlist_value">
                        <fieldset id="ads_opt_transliterate" class="radio" style="border:0 none !important;">
                            <?php echo $lists['ads_opt_transliterate'];  ?>
                        </fieldset>
                    </td>
                </tr>

				</table>	
			</fieldset>
			<fieldset>
				<legend><img src="<?php echo JURI::root();?>administrator/components/com_adsman/img/user.png" style='height:25px; vertical-align:middle;' /><?php echo JText::_( 'ADS_GUEST_POSTING' ); ?></legend>
				<table class="adminlist" width="100%">
			      <tr>
			        <td class="paramlist_key" style="width:320px !important;">
			        	<span class="tooltip_right"><?php echo JText::_("ADS_ENABLE_GUEST_POSTING"); echo "&nbsp;"; ?></span>
			        	<span class="tooltip_right"><?php echo JHTML::_('tooltip', JText::_("ADS_ENABLE_GUEST_POSTING_HELP"));?></span>:</td>
			        <td class="paramlist_value">
			          <fieldset id="ads_opt_logged_posting" class="radio" style="border:0 none !important;">
			            <?php echo $lists['ads_opt_logged_posting'];  ?>
			          </fieldset>  
					</td>
			      </tr>
			      <tr>
			        <td class="paramlist_key">
			        	<span class="tooltip_right"><?php echo JText::_("ADS_ALLOW_GUEST_MESSAGING"); echo "&nbsp;"; ?></span>
			        	<span class="tooltip_right"><?php echo JHTML::_('tooltip', JText::_("ADS_ALLOW_GUEST_MESSAGING_HELP"));?></span>:</td>
			        <td class="paramlist_value">
			          <fieldset id="ads_opt_allow_guest_messaging" class="radio" style="border:0 none !important;">
			            <?php echo $lists['ads_opt_allow_guest_messaging'];  ?>
			          </fieldset>  
					</td>
			      </tr>
			      <tr>
			        <td class="paramlist_key">
			        	<span class="tooltip_right"><?php echo JText::_("ADS_ALLOW_CAPTCHA"); echo "&nbsp;"; ?></span>
			       		<span class="tooltip_right"><?php echo JHTML::_('tooltip', JText::_("ADS_ALLOW_CAPTCHA_HELP"));?></span>:</td>
			        <td class="paramlist_value">
			          <fieldset id="ads_opt_enable_captcha" class="radio" style="border:0 none !important;">
			            <?php echo $lists['ads_opt_enable_captcha'];  ?>
			            &nbsp;
			            <a href="#" onclick="$$('.tab4').fireEvent('click');"><?php echo JText::_("ADS_CONFIGURE_CAPTCHA_HERE")?></a>
			          </fieldset>  
					</td>
			      </tr>
				</table>	
			</fieldset>
			</td>
		</tr>
	</table>	
</fieldset>