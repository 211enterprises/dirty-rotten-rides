<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

jimport('joomla.html.pane');
	$pane	= &JPane::getInstance('sliders', array('allowAllClose' => true));
	JHTML::_('behavior.tooltip');
?>
<form action="index.php" method="post" name="adminForm">

<table width="100%" >
<tr>
	<td valign="top">
	<fieldset class="adminform">
		<legend>
		<img src="<?php echo JURI::root();?>administrator/images/month.png" style='height:25px; vertical-align:middle;' />
		<?php echo JText::_('ADS_USERDETAILS'); ?>
		</legend>
		<table class="paramlist admintable">
			<tr>
				<td class="paramlist_key" width="20%"><?php echo JText::_("ADS_USERNAME"); ?></td>
				<td><?php echo $u->username; ?></td>
			</tr>
			<tr>
				<td class="paramlist_key"><?php echo JText::_("ADS_NAME"); ?></td>
				<td><?php echo $user->name; ?></td>
			</tr>
			<tr>
				<td class="paramlist_key"><?php echo JText::_("ADS_SURNAME"); ?></td>
				<td><?php echo $user->surname; ?></td>
			</tr>
			<tr>
				<td class="paramlist_key"><?php echo JText::_("ADS_ADDRESS"); ?></td>
				<td><?php echo $user->address; ?></td>
			</tr>
			<tr>
				<td class="paramlist_key"><?php echo JText::_("ADS_PHONE"); ?></td>
				<td><?php echo $user->phone; ?></td>
			</tr>
			<tr>
				<td class="paramlist_key"><?php echo JText::_("ADS_EMAIL"); ?></td>
				<td><a href="mailto:<?php echo $u->email; ?>"><?php echo $u->email;?></a></td>
			</tr>
			<?php if ( isset($lists["user_fields"]) ) 
			foreach($lists["user_fields"] as $li => $field )
			{ ?>
			<tr>
				<td class="paramlist_key"><?php echo $field['name']; ?></td>
				<td><?php echo $field['value']; ?></td>
			</tr>	
			<?php } ?>
		</table>	
		</fieldset>
		<fieldset class="adminform">
			<legend>
			<img src="<?php echo JURI::root();?>administrator/images/month.png" style='height:25px; vertical-align:middle;' />
			<?php echo JText::_("ADS_STATISTICS"); ?>
			</legend>
			<table class="adminform">	
				<tr>
					<td valign="top" colspan="2">
						<table class="adminlist paramlist">
							<tr>
								<td class="paramlist key"><?php echo JText::_("ADS_NUMBER_OF_ADS"); ?>:</td>
								<td class="paramlist value"><?php echo $lists['nr_ads'];?></td>
							</tr>
							<tr>
								<td class="paramlist key"><?php echo JText::_("ADS_LATEST_AD"); ?>:</td>
								<td class="paramlist value"><?php echo $lists['last_ads_placed'];?></td>
							</tr>
							<tr>
								<td class="paramlist key"><?php echo JText::_("ADS_FIRST_AD"); ?>:</td>
								<td class="paramlist value"><?php echo $lists['first_ads_placed'];?></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>	
		</fieldset>
	</td>
	<td valign="top">
		<?php
	echo $pane->startPane( "ads-pane" );
	$title = JText::_( 'ADS_DETAILS' );
	echo $pane->startPanel( $title, "detail-page" );
		?>
		<fieldset class="adminform">
			<legend>
				<img src="<?php echo JURI::root();?>administrator/components/com_bids/img/payments.png" style='height:30px; vertical-align:middle;' />
				<?php echo JText::_("ADS_CREDITS"); ?>
			</legend>
			<table class="adminlist" width="30%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<th width="30%" class="title"><?php echo JText::_("ADS_PAYMENT_ITEM"); ?></th>
					<th width="70%" align="left"><?php echo JText::_("ADS_PAYMENT_AMOUNT"); ?></th>
				</tr>
				<?php 
				    if(count($lists["credits"])==0) {
				?>
				<tr>
					<td colspan="2"><?php echo JText::_('ADS_NO_CREDITS_BOUGHT');?></td>
				</tr>
				<?php 
				  } else {
				    for($i=0;$i<count($lists["credits"]);$i++){
                    ?>
                        <tr>
                            <td><?php echo $lists["credits"][$i]->credittype;?></td>
                            <td><?php echo $lists["credits"][$i]->amount;?></td>
                        </tr>
				    <?php }
				  } ?>
			</table>
		</fieldset>
		<?php
	echo $pane->endPanel();
	$title = JText::_( 'ADS_MESSAGES' );
	echo $pane->startPanel( $title, "detail-page" );
		?>
		<fieldset class="adminform">
			<legend>
			<img src="<?php echo JURI::root();?>administrator/images/task_f2.png" style='height:25px; vertical-align:middle;' />
			<?php echo JText::_("ADS_MESSAGES"); ?>
			</legend>
			<table class="adminlist" style="border: 1px dashed silver; padding: 5px; margin-bottom: 10px;">
				<tr>
					<th align="center">#</th>
					<th><?php echo JText::_('ADS_FROM');?></th>
					<th><?php echo JText::_('ADS_TO');?></th>
					<th><?php echo JText::_('ADS_MESSAGE');?></th>
					<th><?php echo JText::_('ADS_DATE');?></th>
				</tr>
				<?php 
				if(isset($lists['messages']) && count($lists['messages']))
				foreach ($lists['messages'] as $k => $m) { ?>
				<tr>
					<td align="center">
					<?php
					echo $m->id;
					/*
						<a href="index.php?option=com_adsman&task=comments_administrator&act=toggle_comment&id=<?php echo $m->id; ?>"><img src="<?php echo F_ROOT_URI;?>/administrator/images/<?php if($m->published==1) echo 'apply_f2.png';else echo 'publish_f2.png';?>" style="width:12px;" border="0" /></a>
						<a href="#" onclick="if(confirm('Are you sure you want to remove coment?')=='1') location.href='index.php?option=com_adsman&task=comments_administrator&act=del_comment&id=<?php echo $m->id; ?>';"><img src="<?php echo F_ROOT_URI;?>/administrator/images/cancel_f2.png" style="width:12px;" border="0" /></a>
						*/
					?>
					</td>
					<td><?php echo $m->fromuser; ?></td>
					<td><?php echo $m->touser; ?></td>
					<td><span class="editlinktip hasTip" title="<?php echo $m->message; ?>"><?php echo substr($m->message,0, 20); ?></span></td>
					<td><?php echo $m->modified; ?></td>
				</tr>
				<?php } ?>
			</table>
		</fieldset>
		<?php
			echo $pane->endPanel();
			echo $pane->endPane();
		?>
	</td>
</tr>
</table>
 <input type="hidden" name="option" value="com_adsman">
 <input type="hidden" name="task" value="detailuser">
 <input type="hidden" name="id" value="<?php echo $user->userid; ?>">
 </form>
