<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

JHtml::_('behavior.framework', true);
?>

<script type="text/javascript">
function submitPopup(userId,userName){
    this.parent.selectUserId(userId,userName);
    this.close();
}
</script>

<form action="index.php?option=com_adsman&task=assignAdUser&tmpl=component" method="post" name="adminForm" id="adminForm">
  <table width="100%" class="adminlist">
	<tfoot>
	<tr>
		<td colspan="3" align="right">
            <?php echo $page->getListFooter(); ?></td>
	</tr>
	</tfoot>
	<thead>
    <tr>
        <th colspan="3"><?php echo JText::_("ADS_REGISTERED_USERS"); ?></th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>
            <?php echo JText::_( 'ADS_FILTER' ); ?>:
				<input type="text" size="50" name="search" id="search" value="<?php echo $lists['search'];?>" class="text_area" onchange="document.adminForm.submit();" title="<?php echo JText::_( 'ADS_FILTER_BY' );?>"/>
            <button onclick="document.getElementById('search').value='';this.form.submit();"><?php echo JText::_( 'ADS_RESET' ); ?></button>
        </td>
    </tr>
	<tr>
        <td><div style="padding: 2px;">
             <a href="#" style="color:#FAA000; font-weight:bold;" onClick="submitPopup(0,'Guest')"> <span class="userid" id="0"><?php echo JText::_('ADS_GUEST');;?></span></a>
        </div>
      <?php
	  for ($i=0, $n=count( $rows ); $i < $n; $i++) {
	    $row = &$rows[$i];
      ?>
        <span style="padding: 2px;">
             <a href="#" style="color:#05186e; font-weight:bold;" onClick="submitPopup(<?php echo $row->userid1;?>,'<?php echo $row->name;?>')"> <span class="userid" id="<?php echo $row->userid1;?>"><?php echo $row->name;?></span></a>
        </span><br />
      <?php
      }
	  ?>
    </td></tr>
    </tbody>
   </table>
 
  <input type="hidden" name="option" value="com_adsman" />
  <input type="hidden" name="task" value="assignAdUser" />
  <input type="hidden" name="boxchecked" value="0" />
  <input type="hidden" name="hidemainmenu" value="0">
  <input type="hidden" name="filter_order" value="<?php echo $lists['order']; ?>" />
  <input type="hidden" name="filter_order_Dir" value="<?php echo $lists['order_Dir']; ?>" />
</form>
