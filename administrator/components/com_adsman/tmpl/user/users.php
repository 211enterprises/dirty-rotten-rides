<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/
?>

<form action="index.php?option=com_adsman&task=users" method="post" name="adminForm">
<table width="100%" class="adminlist">
    <thead>
        <tr>
            <td width="100%">
                <?php echo JText::_( 'ADS_FILTER' ); ?>:
                <input type="text" size="50" name="search" id="search" value="<?php echo $lists['search'];?>" class="text_area" onchange="document.adminForm.submit();" title="<?php echo JText::_( 'ADS_FILTER_BY' );?>" />
                <button onclick="this.form.submit();"><?php echo JText::_( 'ADS_GO' ); ?></button>
                <button onclick="document.getElementById('search').value='';this.form.getElementById('filter_sectionid').value='-1';this.form.getElementById('catid').value='0';this.form.getElementById('filter_authorid').value='0';this.form.getElementById('filter_state').value='';this.form.submit();"><?php echo JText::_( 'ADS_RESET' ); ?></button>
            </td>
            <td nowrap="nowrap">
                <?php
                ?>
            </td>
            <td>
              <select id="profile_state" name="profile_state" onchange="submitform();">
                  <option value="2" <?php echo $lists['profile_state'] == 2 ? 'selected="selected"' : ''; ?>><?php echo JText::_('ADS_SELECT_PROFILE_STATE'); ?></option>
                  <option value="1" <?php echo $lists['profile_state'] == 1 ? 'selected="selected"' : ''; ?>><?php echo JText::_('ADS_PROFILE_FILLED'); ?></option>
                  <option value="0" <?php echo $lists['profile_state'] == 0 ? 'selected="selected"' : ''; ?>><?php echo JText::_('ADS_PROFILE_UNFILLED'); ?></option>
              </select>
            </td>
            <td>
              <select id="status" name="status" onchange="submitform();">
                  <option value="2" <?php echo $lists['status'] == 2 ? 'selected="selected"' : ''; ?>><?php echo JText::_('ADS_SELECT_USER_STATUS'); ?></option>
                  <option value="0" <?php echo $lists['status'] == 0 ? 'selected="selected"' : ''; ?>><?php echo JText::_('ADS_ACTIVE'); ?></option>
                  <option value="1" <?php echo $lists['status'] == 1 ? 'selected="selected"' : ''; ?>><?php echo JText::_('ADS_BLOCKED'); ?></option>
              </select>
            </td>
        </tr>
    </thead>
	<tfoot>
        <tr>
            <td colspan="4" align="right">
            <?php echo $page->getListFooter(); ?>
            </td>
        </tr>
	</tfoot>
	<tr>
		<td colspan="4">
			<table class="adminlist" cellpadding="0" cellspacing="0" border="0">
				<thead>
					<tr>
						<th width="15" align="center">#</th>
						<th width="15">
							<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $rows ); ?>);" />
						</th>
						<th width="69" class="title ads_align_left"><?php echo JHTML::_('grid.sort', JText::_("ADS_USERNAME"), 'username', @$lists['order_Dir'], @$lists['order']  ) ; ?></th>
						<th width="69" class="title ads_align_left"><?php echo JHTML::_('grid.sort', JText::_("ADS_NAME"), 'name' , @$lists['order_Dir'], @$lists['order']  ); ?></th>
						<th width="69" class="title ads_align_left"><?php echo JHTML::_('grid.sort', JText::_("ADS_EMAIL"), 'email' , @$lists['order_Dir'], @$lists['order']  ); ?></th>
						<th width="69" align="center"><?php echo JHTML::_('grid.sort', JText::_("ADS_USER_ACTIVE_BLOCKED"), 'u.block' , @$lists['order_Dir'], @$lists['order']  ); ?></th>
						<th width="69" align="center"><?php echo JHTML::_('grid.sort', JText::_("ADS_USER_PROFILE_STATE"), 'b.userid' , @$lists['order_Dir'], @$lists['order']  ); ?></th>
						<th width="73" align="center"><?php echo JHTML::_('grid.sort', JText::_("ADSMAN_TOTAL_ADS"), 'nr_total_ads' , @$lists['order_Dir'], @$lists['order']  ); ?></th>

						<th width="80"><?php echo JHTML::_('grid.sort', JText::_("ADSMAN_ACTIVE_ADS"), 'nr_open_ads' , @$lists['order_Dir'], @$lists['order']  ); ?></th>
                        <th class="title" width="69"><?php echo JText::_("ADS_GOLD"); ?></th>
                        <th class="title" width="69"><?php echo JText::_("ADS_SILVER"); ?></th>
                        <th class="title" width="69"><?php echo JText::_("ADS_BRONZE"); ?></th>
                        <th width="80"><?php echo JHTML::_('grid.sort', JText::_("ADSMAN_CLOSED_ADS"), 'nr_closed_ads' , @$lists['order_Dir'], @$lists['order']  ); ?></th>
					</tr>
				</thead>
				<tbody>
      <?php
	  $k = 0;
	  for ($i=0, $n=count( $rows ); $i < $n; $i++) {
	    $row = &$rows[$i];
	    
		$img_blocked 	= ($row->blocked ==1) ?   'publish_x.png':'publish_g.png';
		$task1_blocked 	= ($row->blocked ==1) ? 'unblockuser' : 'blockuser';
		$alt_blocked 	= ($row->blocked ==1) ? JText::_("ADS_BLOCKED") : JText::_("ADS_ACTIVE");
	  
		$link 	= 'index.php?option=com_adsman&task=detailUser&id='. $row->userid1;
       

      ?>
				<tr class="<?php echo 'row',$k; ?>">
					<td align="center">
						<?php echo $page->getRowOffset( $i ); ?>
					</td>
					<td align="center"><?php echo JHTML::_('grid.id', $i, $row->userid1);?></td>
					<td>
						<?php if($row->profid){ ?>
							<a href="<?php echo $link; ?>" title="Edit"><?php echo $row->username; ?></a>
						<?php } else { ?>
							<?php echo $row->username; ?>
						<?php } ?>
					</td>
					<td><?php echo $row->name;?></td>
					<td><?php echo $row->email; ?></td>
					
					<td width="73" align="center">
					<?php if($row->profid){ ?>	
						<a href="javascript: void(0);" onclick="return listItemTask('cb<?php echo $i;?>','<?php echo $task1_blocked;?>')">
							<img src="<?php echo JURI::root()."administrator/components/com_adsman/img/".$img_blocked;?>" height="16" border="0" alt="<?php echo $alt_blocked; ?>" title="<?php echo $alt_blocked;?>" class="hasTip"/>
						</a>
					<?php } else { ?>
							<?php echo $alt_blocked; ?>
						<?php } ?>	
					</td>
					
					<td width="73" align="center">
						<?php if($row->profid){ ?>
							<?php echo JText::_("ADS_PROFILE_FILLED"); ?>
						<?php } else { ?>
							<?php echo JText::_("ADS_PROFILE_UNFILLED"); ?>
						<?php } ?>
					</td>
					<td align="center"><?php echo $row->nr_total_ads; ?></td>
					<td align="center"><?php echo $row->nr_open_ads;?></td>
                    <td align="center"><?php echo $row->gold;?></td>
                    <td align="center"><?php echo $row->silver;?></td>
                    <td align="center"><?php echo $row->bronze;?></td>
                    <td align="center"><?php echo $row->nr_closed_ads; ?></td>
				</tr>
				</tbody>
      <?php
	  $k = 1 - $k;
	  }
	  ?>
			</table>
		</td>
	</tr>
 </table>
 
  <input type="hidden" name="option" value="com_adsman" />
  <input type="hidden" name="task" value="users" />
  <input type="hidden" name="boxchecked" value="0" />
  <input type="hidden" name="hidemainmenu" value="0">
  <input type="hidden" name="filter_order" value="<?php echo $lists['order']; ?>" />
  <input type="hidden" name="filter_order_Dir" value="<?php echo $lists['order_Dir']; ?>" />
</form>
