<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/
?>
<fieldset class="adminform">
	<legend><?php echo JText::_( 'ADS_SYSTEM_SETTINGS' ); ?> </legend>
	<table class="adminlist">
		<tr>
			<td colspan="2"><h3><?php echo JText::_( 'ADS_FILE_PERMISSIONS' ); ?></h3></td>
		</tr>
		<?php JTheFactoryAdminHelper::writableCell("components/com_adsman/templates/cache/",1,"<b>Smarty cache directory:</b>  ",1,"index.php?option=com_adsman&task=dirchmode&dir=components/com_adsman/templates/cache");?>
		<?php JTheFactoryAdminHelper::writableCell("media/com_adsman/images/",1, "<b>Image Uploading directory:</b>  ",1,"index.php?option=com_adsman&task=dirchmode&dir=components/com_adsman/images");?>
		<?php JTheFactoryAdminHelper::writableCell("components/com_adsman/options.php",1,"<b>Configuration file:</b>  ",1,"index.php?option=com_adsman&task=dirchmode&dir=components/com_adsman/options.php");?>
		<tr>
			<td colspan="2"><h3><?php echo JText::_("ADS_JOOMLA_SETTINGS");?></h3></td>
		</tr>
		<tr>
			<td>
				<?php echo JText::_( 'ADS_ERROR_REPORTING' ); ?>:
			</td>
			<td>
				<?php echo $lists["error_reporting"]; ?>
			</td>
		</tr>
		<tr>
			<td>
				<?php echo JText::_( 'ADS_SITE_LOCALE_TIME' ); ?>:
			</td>
			<td>
				<?php echo $lists["local_time"]; ?>
			</td>
		</tr>
		<tr>
			<td>
				<?php echo JText::_( 'ADS_COMPONENT_LOCALE_TIME' ); ?>:
			</td>
			<td>
				<?php echo $lists["component_time"]; ?>
			</td>
		</tr>
		<tr>
			<td colspan="2"><h3><img src="<?php echo JURI::root();?>components/com_adsman/img/php-icon-white.gif" style="vertical-align:middle;" />  <?php echo JText::_("ADS_PHP_SETTINGS");?></h3></td>
		</tr>
		<tr>
			<td>
				<?php echo JText::_( 'ADS_DISPLAY_ERRORS' ); ?>:
			</td>
			<td>
				<?php echo JTheFactoryAdminHelper::get_php_setting('display_errors'); ?>
			</td>
		</tr>
		<tr>
			<td>
				<?php echo JText::_( 'ADS_FILE_UPLOADS' ); ?>:
			</td>
			<td>
				<?php echo JTheFactoryAdminHelper::get_php_setting('file_uploads'); ?>
			</td>
		</tr>
		<tr>
			<td>
				<?php echo JText::_( 'ADS_MAX_UPLOAD_SIZE' ); ?>:
			</td>
			<td>
				<?php echo ini_get('upload_max_filesize'); ?>
			</td>
		</tr>
		<tr>
			<td>
				<?php echo JText::_( 'ADS_GMT_TIME' ); ?>:
			</td>
			<td>
				<?php echo $lists["gmt_time"]; ?>
			</td>
		</tr>
	</table>
</fieldset>
