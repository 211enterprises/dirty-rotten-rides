<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/
?>

<fieldset class="adminform">
	<legend><?php echo JText::_( 'ADS_GOOGLE_MAPS_CONFIG' ); ?></legend>
<?php JHTML::_('behavior.modal');?>
	<fieldset class="adminform">
		<legend><?php echo JText::_( 'ADS_GOOGLE_MAPS_API_SETUP' ); ?></legend>
		<table class="adminlist">
			<tr>
				<td width="20%" class="paramlist_key">
					<span class="tooltip_right"><?php echo JText::_("ADS_GOOGLEMAPKEY");echo "&nbsp;"; ?></span>
					<span class="tooltip_right"><?php echo JHTML::_('tooltip', JText::_("ADS_HELP_GOOGLEMAPKEY")); ?></span> : 
				</td>
				<td width="80%">
					<a target="_blank" href="http://code.google.com/apis/maps/signup.html">Get Key</a>
					<br />
				   <input type="text" name="googlemapkey" style="width:600px;" value="<?php echo ads_opt_google_key; ?>">
				</td>
			</tr>
		</table>
	</fieldset
	<table class="adminlist">
		<tr>
			<td>
				<fieldset class="adminform">
					<legend><?php echo JText::_( 'ADS_GOOGLE_MAPS_CONFIG' ); ?></legend>
					<table class="adminlist" width="100%">
						<tr>
							<td valign="top" class="paramlist_key" colspan="2">
								<br />
								<span class="tooltip_right"><?php echo JText::_("ADS_GOOGLE_MAP_DEFAULT_POSITION");echo "&nbsp;"; ?></span>
								<span class="tooltip_right"><?php echo FactoryHTML::tooltip(JText::_("ADS_GOOGLE_MAP_DEFAULT_POSITION_HELP"));?></span>
							</td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div id="map_canvas"></div>
                            </td>
                        </tr>
                        <tr>
							<td valign="top" colspan="2">
							     <span style="float: left;"><?php echo JText::_("X:");?>&nbsp;</span>  
							     <span class="tooltip_right"><input type="text" id="googleX" name="ads_opt_googlemap_defx" style="width:120px;" value="<?php if(defined("ads_opt_googlemap_defx")) echo ads_opt_googlemap_defx;?>" /></span>
								 <span style="float: left; clear: left;"><?php echo JText::_("Y:");?>&nbsp;</span>
								 <span class="tooltip_right"><input type="text" id="googleY" name="ads_opt_googlemap_defy" style="width:120px;" value="<?php if(defined("ads_opt_googlemap_defy"))  echo ads_opt_googlemap_defy;?>" /></span>
							</td>
						</tr>
						<tr>
							<td class="paramlist_key"><?php echo JText::_("ADS_GOOGLE_MAPS_DEFAULT_ZOOM");?>:</td>
							<td>
								<input type="text" name="ads_opt_googlemap_default_zoom" id="zoom_optic" value="<?php if(defined("ads_opt_googlemap_default_zoom"))  echo ads_opt_googlemap_default_zoom;?>" size="5"  />
							</td>
						</tr>
						<tr>
							<td valign="top" class="paramlist_key">
								<br />
								<?php echo JText::_("ADS_GOOGLE_ALLOWED_MAP_TYPES");?>:
							</td>
							<td valign="top">
								<br />
								<?php 
								$cuc = array();
								if(defined("ads_opt_googlemap_allowed_maps") && strlen(ads_opt_googlemap_allowed_maps)>0  )
									$cuc = explode(",", ads_opt_googlemap_allowed_maps );
								?>
								<select name="ads_opt_googlemap_type" style="width:200px;">
									<option value="MAP" <?php if(ads_opt_googlemap_type=="MAP") echo "selected"; ?> >Map</option>
									<option value="HYBRID" <?php if(ads_opt_googlemap_type=="HYBRID") echo "selected"; ?>>Hybrid</option>
									<option value="SATELLITE" <?php if(ads_opt_googlemap_type=="SATELLITE") echo "selected"; ?>>Satellite</option>
								</select>
							</td>
						</tr>
					</table>
				</fieldset>
			</td>
			<td valign="top">
				<!--
				<img src="http://maps.google.com/intl/en_ALL/images/logos/maps_logo.gif" />
				<br />
				-->
				<fieldset class="adminform">
					<legend><?php echo JText::_( 'ADS_GOOGLE_MAPS_LAYOUT_SETTINGS' ); ?></legend>
					<table class="adminlist" width="100%">
						<tr>
							<td class="paramlist_key"><?php echo JText::_("ADS_GOOGLE_MAP_LAYOUT_SIZE");?>:</td>
							<td>
								<span style="float: left;"><?php echo JText::_("ADS_GOOGLE_MAP_WIDTH");?>&nbsp;</span>
								<span class="tooltip_right"><input type="text" name="ads_opt_googlemap_gx" value="<?php if(defined("ads_opt_googlemap_gx"))  echo ads_opt_googlemap_gx;?>" /></span>
								
								<span style="float: left; clear: left;"><?php echo JText::_("ADS_GOOGLE_MAP_HEIGHT");?></span>
								<span class="tooltip_right"><input type="text" name="ads_opt_googlemap_gy" value="<?php if(defined("ads_opt_googlemap_gy"))  echo ads_opt_googlemap_gy;?>" /></span>
							</td>
						</tr>
						<tr>
							<td class="paramlist_key"><?php echo JText::_("ADS_GOOGLEMAP_IN_ADSDETAILS");?>:</td>
							<td><input name="ads_map_in_ads_details" type="checkbox" value="1" <?php echo (ads_map_in_ads_details)?"checked":""; ?>></td>
						</tr>
					</table>
				</fieldset>
				<fieldset class="adminform">
					<legend><?php echo JText::_( 'ADS_GOOGLE_MAPS_RADIUS_SEARCH' ); ?></legend>
					<table class="adminlist" width="100%">
						<tr class="hasTip" title="<?php echo JText::_('ADS_GOOGLE_MAP_DISTANCES_UNIT'); ?>::<?php echo JText::_('ADS_GOOGLE_MAP_DISTANCES_UNIT'); ?>">
							<td width="40%" class="paramlist_key">
						      <span class="editlinktip">
						        <!--<label for="ads_opt_googlemap_distance">--><?php echo JText::_('Distances unit'); ?><!--</label>-->
						      </span>
						    </td>
						    <td class="paramlist_value">
						      <select id="ads_opt_googlemap_distance" name="ads_opt_googlemap_distance" style="width:200px;">
						        <option value="0" <?php echo 0 == @ads_opt_googlemap_distance ? 'selected' : ''; ?>><?php echo JText::_('ADS_GOOGLE_MAPS_MILES'); ?></option>
						        <option value="1" <?php echo 1 == @ads_opt_googlemap_distance ? 'selected' : ''; ?>><?php echo JText::_('ADS_GOOGLE_MAPS_KILOMETERS'); ?></option>
						      </select>
						    </td>
						</tr>
						<tr class="hasTip" title="<?php echo JText::_('ADS_GOOGLE_MAPS_DISTANCES_UNITS'); ?>">
							<td width="40%" class="paramlist_key">
						      <span class="editlinktip">
						        <!--<label for="distances_unit">--><?php echo JText::_('Available Distances units'); ?><!--</label>-->
						      </span>
						    </td>
						    <td class="paramlist_value">
								<input type="textbox" style="width:300px;" name="ads_opt_googlemap_unit_available" value="<?php if(defined('ads_opt_googlemap_unit_available') && ads_opt_googlemap_unit_available!='') echo ads_opt_googlemap_unit_available; else echo '5,25,50,100';?>" />
						    </td>
						</tr>
					</table>
				</fieldset>
			</td>
		</tr>
	</table>
<script type="text/javascript">
function selectGoogleMap_Choords(x,y){
	document.getElementById("googleX").value=x;;
	document.getElementById("googleY").value=y;
}
function selectGoogleMap_zoom(z){
	document.getElementById("zoom_optic").value=z;
}
</script>
</fieldset>