// Settings functions
function addCurreny()
{
	el	=document.getElementById('temp_currency');
	el2	=document.getElementById('currency_list');

	var elOptNew = document.createElement('option');
	elOptNew.text = el.value.toUpperCase();
	elOptNew.value = el.value.toUpperCase();
	
	try {
	    el2.add(elOptNew, null); // standards compliant; doesn't work in IE
	}
	catch(ex) {
	    el2.add(elOptNew); // IE only
	}
	el.value='';
}


function delCurrency()
{
	  var elSel = document.getElementById('currency_list');
	  var i;
	  for (i = elSel.length - 1; i>=0; i--) {
	    if (elSel.options[i].selected) {
	      elSel.remove(i);
	    }
	  }
}

var MONTH_NAMES=new Array('January','February','March','April','May','June','July','August','September','October','November','December','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
var DAY_NAMES=new Array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sun','Mon','Tue','Wed','Thu','Fri','Sat');

function LZ(x) {return(x<0||x>9?"":"0")+x}

function formatDate(format) {
	date = new Date()
	format=format+"";
	var result="";
	var i_format=0;
	var c="";
	var token="";
	var y=date.getYear()+"";
	var M=date.getMonth()+1;
	var d=date.getDate();
	var E=date.getDay();
	var H=date.getHours();
	var m=date.getMinutes();
	var s=date.getSeconds();
	var yyyy,yy,MMM,MM,dd,hh,h,mm,ss,ampm,HH,H,KK,K,kk,k;
	// Convert real date parts into formatted versions
	var value=new Object();
	if (y.length < 4) {y=""+(y-0+1900);}
	value["y"]=""+y;
	value["yyyy"]=y;
	value["yy"]=y.substring(2,4);
	value["M"]=M;
	value["MM"]=LZ(M);
	value["MMM"]=MONTH_NAMES[M-1];
	value["NNN"]=MONTH_NAMES[M+11];
	value["d"]=d;
	value["dd"]=LZ(d);
	value["E"]=DAY_NAMES[E+7];
	value["EE"]=DAY_NAMES[E];
	value["H"]=H;
	value["HH"]=LZ(H);
	if (H==0){value["h"]=12;}
	else if (H>12){value["h"]=H-12;}
	else {value["h"]=H;}
	value["hh"]=LZ(value["h"]);
	if (H>11){value["K"]=H-12;} else {value["K"]=H;}
	value["k"]=H+1;
	value["KK"]=LZ(value["K"]);
	value["kk"]=LZ(value["k"]);
	if (H > 11) { value["a"]="PM"; }
	else { value["a"]="AM"; }
	value["m"]=m;
	value["mm"]=LZ(m);
	value["s"]=s;
	value["ss"]=LZ(s);
	while (i_format < format.length) {
		c=format.charAt(i_format);
		token="";
		while ((format.charAt(i_format)==c) && (i_format < format.length)) {

			token += format.charAt(i_format++);
			}
		if (value[token] != null) { result=result + value[token]; }
		else { result=result + token; }
		}
	return result;
}

function showDateInFormat(format){
	var buf_format;
	//var now = new Date();

    switch(format){
        case 'Y-m-d':
            buf_format = "yyyy-MM-dd";
        break;
        case 'Y-d-m':
            buf_format = "yyyy-dd-MM";
        break;
        case 'm/d/Y':
            buf_format = "MM/dd/yyyy";
        break;
        case 'd/m/Y':
            buf_format = "dd/MM/yyyy";
        break;
        case 'd.m.Y':
            buf_format = "dd.MM.yyyy";
        break;
        case 'D, F d Y':
            buf_format = "E, MMM dd yyyy";
        break;
    }


	/*switch(format){
		case'Y-m-d':
			buf_format = "yyyy-MM-dd";
		break;
		case'Y-d-m':
			buf_format = "yyyy-dd-MM";
		break;
		case'm-d-Y':
			buf_format = "MM-dd-yyyy";
		break;
		case'd-m-Y':
			buf_format = "dd-MM-yyyy";
		break;
		case'D, F d Y':
			buf_format = "E, MMM dd yyyy";
		break;
	}*/

	document.getElementById('datef').innerHTML = formatDate(buf_format);
}

function showTimeInFormat(formatt){
	var buf_format;

	switch(formatt){
		case'H:i':
			buf_format = "HH:mm";
		break;
		case'h:iA':
			buf_format = "h:mm a";
		break;
	}
	document.getElementById('timef').innerHTML = formatDate(buf_format);
}

// Joomla  Submitbutton overridden
 Joomla.submitform = function(task, form) {
 	
 	//if (typeof tinyMCE  != 'undefined') tinyMCE.execCommand('mceFocus', false,'terms_conditions');
 	
	if (typeof(form) === 'undefined') {
		form = document.getElementById('adminForm');
			/**
			 * Added to ensure Joomla 1.5 compatibility
			 */
			if(!form){
				form = document.adminForm;
			}
	}
				
	if (typeof(task) !== 'undefined') {
		form.task.value = task;
	}

	
	if (task == 'savesettings') {
				
		var elSel = document.getElementById('currency_list');
		 
		if ( elSel.length <= 0){
			alert('<?php echo ads_err_currency_empty;?>');
			return;
		}
		
		var i;
		document.adminForm.currency.value='';
		
		for (i =0 ; i< elSel.length; i++) {
		  	document.adminForm.currency.value+=elSel.options[i].value+'~';
		}
				
		form.submit();
		
	} else {
		
		// Submit the form.
		if (typeof form.onsubmit == 'function') {
			form.onsubmit();
		}
		if (typeof form.fireEvent == "function") {
			form.fireEvent('submit');
		}
		form.submit();
	}
	
};


function toggleMailCaptcha(val){
	document.getElementById('tab_joomla').style.display='none';
	document.getElementById('tab_smarty').style.display='none';
	document.getElementById('tab_recaptcha').style.display='none';
	document.getElementById("tab_"+val).style.display='';
}

function show_waiting(){
	var ahtml = "<img src=root + 'components/com_adsman/img/components/com_adsman/img/ajax-loader.gif' />";
	document.getElementById("custom_fields_container").innerHTML=ahtml;
}

function theFactoryToggleFields(){

	var ahtml = this.response.text;
	document.getElementById("custom_fields_container").innerHTML=ahtml;
}

function aiai(sel){

	show_waiting();

	var id_q = "";
	if(AFIDid!="")
		id_q = "&adid="+AFIDid;

	var url;

	link = "index.php?option=com_adsman&task=ajax_form_customfields&catid=" + sel + id_q;
	var FieldsAjaxCalls = new Request({
		 method: "get",
		 url: link,

		 onComplete:theFactoryToggleFields
		}).send();

}

function categorySelectPopulateResponse(){
	var ahtml = this.response.text;
	document.getElementById("category_axj_space").innerHTML=ahtml;

	if ( page == 'search') {
		aiai_search(document.getElementById("cat").value);
	}
	else {
		aiai(document.getElementById("category").value);
		if (document.getElementById('category_price_container') != null) {
			document.getElementById('category_price_container').innerHTML = 'Listing costs '+ document.getElementById("category").value + ' credits';
		}
	}
}

function categorySelectPopulate(task,catid){
	var link = "index.php?option=com_adsman&task="+task+"&catid="+catid;

	var CategoryAjaxCall = new Request({
		method: "get",
		url: link,
		onComplete: categorySelectPopulateResponse
	}).send();

}
