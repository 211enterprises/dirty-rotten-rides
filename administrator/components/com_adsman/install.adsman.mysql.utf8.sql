CREATE TABLE IF NOT EXISTS `#__ads` (
  `id` int(11) NOT NULL auto_increment,
  `category` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `short_description` text NOT NULL,
  `description` text NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `addtype` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `userid` int(11) NOT NULL,
  `askprice` varchar(100) NOT NULL,
  `currency` varchar(100) NOT NULL,
  `picture` varchar(50),
  `featured` enum('gold','silver','bronze','none') default 'none',
  `hits` int(11) NOT NULL,
  `close_by_admin` tinyint(1) NOT NULL DEFAULT 0,
  `newmessages` tinyint( 1 ) NOT NULL DEFAULT 0,
  `closed` TINYINT( 1 ) NOT NULL DEFAULT '0',
  `closed_date` DATETIME NOT NULL,
  `atachment` varchar(100) NOT NULL,
  `MapX` varchar(255) NOT NULL,
  `MapY` varchar(255) NOT NULL,
  `ad_city` varchar(200),
  `ad_postcode` varchar(10),
  `extend` tinyint(1) NOT NULL DEFAULT 1,
  `token_id` VARCHAR(100) NOT NULL,
  `guest_email` VARCHAR(100) NOT NULL,
  `guest_username` VARCHAR(100) NOT NULL,
  PRIMARY KEY  (`id`),
  INDEX `category` (`category`),
  INDEX `currency` (`currency`),
  INDEX `start_date` (`start_date`),
  INDEX `end_date` (`end_date`),
  INDEX `addtype` (`addtype`),
  INDEX `status` (`status`),
  INDEX `userid` (`userid`),
  INDEX `close_by_admin` (`close_by_admin`),
  INDEX `featured` (`featured`),
  INDEX `closed` (`closed`)
);

CREATE TABLE IF NOT EXISTS `#__ads_categories` (
  `id` int(11) NOT NULL auto_increment,
  `catname` varchar(100) NOT NULL default '',
  `description` text NOT NULL,
  `parent` int(11) NOT NULL default '0',
  `hash` varchar(32) NOT NULL default '',
  `ordering` int(11) default NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY  (`id`),
  KEY `ihash` (`hash`),
  INDEX `parent` (`parent`),
  INDEX `status` (`status`)
);

CREATE TABLE IF NOT EXISTS `#__ads_categories_sef` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `catid` int(11) NOT NULL,
  `categories` text NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `catid` (`catid`)
);

CREATE TABLE IF NOT EXISTS `#__ads_cbfields` (
  `id` int(11) NOT NULL auto_increment,
  `field` varchar(50) default NULL,
  `cb_field` varchar(50) default NULL,
  `love_field` varchar(50) default NULL,
  PRIMARY KEY  (`id`)
);

CREATE TABLE IF NOT EXISTS `#__ads_contacts_buy` (
  `id` int(11) NOT NULL auto_increment,
  `userid` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
);

CREATE TABLE IF NOT EXISTS `#__ads_country` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(150) NOT NULL default '',
  `simbol` char(3) NOT NULL default '',
  `active` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
);

CREATE TABLE IF NOT EXISTS `#__ads_currency` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(11) default NULL,
  PRIMARY KEY  (`id`)
);

CREATE TABLE IF NOT EXISTS `#__ads_custom_prices` (
  `id` int(11) NOT NULL auto_increment,
  `category` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `price_type` enum('listing') NOT NULL,
  PRIMARY KEY  (`id`)
);

CREATE TABLE IF NOT EXISTS `#__ads_favorites` (
  `id` int(11) NOT NULL auto_increment,
  `userid` int(11) NOT NULL,
  `adid` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  INDEX `userid` (`userid`),
  INDEX `adid` (`adid`)
);

CREATE TABLE IF NOT EXISTS `#__ads_fields` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `db_name` varchar(100) NOT NULL,
  `field_id` varchar(100) NOT NULL,
  `page` varchar(100) NOT NULL,
  `search` tinyint(1) NOT NULL,
  `has_options` tinyint(1) NOT NULL default '0',
  `ftype` varchar(150) NOT NULL,
  `compulsory` tinyint(1) NOT NULL,
  `validate_type` varchar(100) NOT NULL,
  `own_table` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `css_class` varchar(100) NOT NULL,
  `style_attr` text NOT NULL,
  `ordering` int(11) NOT NULL default '0',
  `attributes` text NOT NULL,
  `params` text NOT NULL,
  `help` text NOT NULL,
  PRIMARY KEY  (`id`),
  INDEX `field_id` (`field_id`)
);

CREATE TABLE IF NOT EXISTS `#__ads_fields_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fid` int(11) NOT NULL,
  `cid` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fid` (`fid`),
  KEY `cid` (`cid`)
);

CREATE TABLE IF NOT EXISTS `#__ads_fields_options` (
  `id` int(11) NOT NULL auto_increment,
  `fid` int(11) NOT NULL,
  `option_name` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`),
  INDEX `fid` (`fid`)
);

CREATE TABLE IF NOT EXISTS `#__ads_fields_positions` (
  `id` int(11) NOT NULL auto_increment,
  `fid` int(11) NOT NULL,
  `position` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`),
  INDEX `fid` (`fid`),
  INDEX `position` (`position`)
);

CREATE TABLE IF NOT EXISTS `#__ads_images_pricing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `credits_img_0` double NOT NULL,
  `credits_img_1` double NOT NULL,
  `credits_img_2` double NOT NULL,
  `credits_img_3` double NOT NULL,
  `credits_img_4` double NOT NULL,
  `credits_img_5` double NOT NULL,
  `credits_img_6` double NOT NULL,
  `credits_img_7` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__ads_limits` (
  `id` int(11) NOT NULL auto_increment,
  `owner` int(11) NOT NULL,
  `limit_size` int(11) NOT NULL,
  `limit_type` enum('group','user') NOT NULL,
  PRIMARY KEY  (`id`)
);

CREATE TABLE IF NOT EXISTS `#__ads_mails` (
  `id` int(11) NOT NULL auto_increment,
  `mail_type` varchar(250) default NULL,
  `content` text,
  `subject` varchar(250) default NULL,
  `enabled` int(11) default '1',
  PRIMARY KEY  (`id`),
  KEY `mailtypes` (`mail_type`)
);

CREATE TABLE IF NOT EXISTS `#__ads_messages` (
  `id` int(11) NOT NULL auto_increment,
  `adid` int(11) NOT NULL,
  `from_user` int(11) NOT NULL,
  `to_user` int(11) NOT NULL,
  `comment` text NOT NULL,
  `datesend` datetime NOT NULL,
  `id_message` int( 11 ) NOT NULL DEFAULT '0',
  `wasread` TINYINT( 1 ) NOT NULL DEFAULT '0',
  PRIMARY KEY  (`id`)
);

CREATE TABLE IF NOT EXISTS `#__ads_packages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `price` varchar(100) NOT NULL,
  `currency` varchar(3) NOT NULL,
  `credits` int(11) NOT NULL,
  `status` TINYINT(1) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `#__ads_paylog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `currency` varchar(11) DEFAULT NULL,
  `refnumber` varchar(100) DEFAULT NULL,
  `invoice` varchar(50) DEFAULT NULL,
  `ipn_response` text,
  `ipn_ip` varchar(100) DEFAULT NULL,
  `comission_id` int(11) DEFAULT NULL,
  `status` enum('ok','error','manual_check','cancelled','refunded') DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `itemname` varchar(50) DEFAULT NULL,
  `payment_method` varchar(100) DEFAULT NULL,
  `object_id` int(11) DEFAULT NULL,
  `var_name1` VARCHAR( 255 ) NOT NULL ,
  `var_name2` VARCHAR( 255 ) NOT NULL ,
  `var_name3` VARCHAR( 255 ) NOT NULL ,
  `var_name4` VARCHAR( 255 ) NOT NULL ,
  `var_value1` VARCHAR( 255 ) NOT NULL ,
  `var_value2` VARCHAR( 255 ) NOT NULL ,
  `var_value3` VARCHAR( 255 ) NOT NULL ,
  `var_value4` VARCHAR( 255 ) NOT NULL ,
  `params` mediumtext DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ixdate` (`date`),
  KEY `ixuserid` (`userid`),
  KEY `ixstatus` (`status`),
  KEY `ixref` (`refnumber`),
  KEY `ixinvoice` (`invoice`)
);

CREATE TABLE IF NOT EXISTS `#__ads_payments` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `paydate` datetime NOT NULL,
  `automatic` int(11) default '0' NOT NULL,
  PRIMARY KEY  (`id`)
);

CREATE TABLE IF NOT EXISTS `#__ads_paysystems` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paysystem` varchar(50) DEFAULT NULL,
  `classname` varchar(50) DEFAULT NULL,
  `enabled` int(1) DEFAULT '1',
  `isdefault` int(1) DEFAULT '1',
  `params` text,
  `ordering` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `#__ads_pictures` (
  `id` int(11) NOT NULL auto_increment,
  `id_ad` int(11) NOT NULL default '0',
  `userid` int(11) NOT NULL default '0',
  `picture` varchar(100) NOT NULL default '',
  `modified` date NOT NULL default '0000-00-00',
  `published` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY  (`id`),
  KEY `idad` (`id_ad`),
  KEY `ixuserid` (`userid`)
);

CREATE TABLE IF NOT EXISTS `#__ads_positions` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `html` text NOT NULL,
  `tpl` text NOT NULL,
  `verified` tinyint(1) NOT NULL,
  PRIMARY KEY  (`id`)
);

CREATE TABLE IF NOT EXISTS `#__ads_pricing` (
  `id` int(11) NOT NULL auto_increment,
  `itemname` varchar(50) default NULL,
  `price` decimal(10,2) default NULL,
  `currency` varchar(10) default NULL,
  `enabled` int(1) default NULL,
  `params` text,
  `ordering` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `ixitemname` (`itemname`)
);

CREATE TABLE IF NOT EXISTS `#__ads_report` (
  `id` int(11) NOT NULL auto_increment,
  `adid` int(11) NOT NULL default '0',
  `userid` int(11) NOT NULL default '0',
  `message` varchar(200) NOT NULL default '',
  `status` varchar(150) NOT NULL default 'unprocessed',
  `modified` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`),
  KEY `adid` (`adid`)
);

CREATE TABLE IF NOT EXISTS `#__ads_tags` (
  `id` int(11) NOT NULL auto_increment,
  `parent_id` int(11) NOT NULL,
  `tagname` varchar(255) NOT NULL,
  `tablename` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`),
  INDEX `parent_id` (`parent_id`),
  INDEX `tagname` (`tagname`)
);

CREATE TABLE IF NOT EXISTS `#__ads_urls` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `ad_id` int(11) NOT NULL,
  `video_link` varchar(255) NOT NULL,
  `video_title` text NOT NULL,
  `video_description` text NOT NULL,
  `video_thumbnail` text NOT NULL,
  `video_sitename` varchar(100) NOT NULL,
  `video_sourceThumb` text NOT NULL,
  `date_added` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__ads_users` (
  `id` int(11) NOT NULL auto_increment,
  `userid` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `surname` varchar(100) NOT NULL,
  `address` varchar(150) NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` varchar(150) NOT NULL,
  `phone` text NOT NULL,
  `modified` datetime NOT NULL,
  `email` varchar(255) NOT NULL,
  `paypalemail` varchar(255) NOT NULL,
  `YM` varchar(255) NOT NULL,
  `googleX` varchar(100) NOT NULL,
  `googleY` varchar(100) NOT NULL,
  `status` TINYINT( 1 ) NOT NULL DEFAULT '0',
  PRIMARY KEY  (`id`),
  INDEX `userid` (`userid`)
);

CREATE TABLE IF NOT EXISTS `#__ads_user_credits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `credits_no` int(11) NOT NULL,
  `package_free` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__ads_watchlist` (
  `id` int(11) NOT NULL auto_increment,
  `userid` int(11) NOT NULL default '0',
  `catid` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `iuserid` (`userid`)
);
