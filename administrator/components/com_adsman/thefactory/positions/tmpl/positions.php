<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

require_once(dirname(__FILE__).DS."..".DS."tmpl".DS."position.toolbar.php");?>
<form name="adminForm" method="POST">
	<input type="hidden" name="task" value="fposition_panel" />
	<input type="hidden" name="option" value="<?php echo APP_EXTENSION;?>" />
	<table class="adminlist">
		<tr>
			<th><?php echo JTHEF_POSITION;?></th>
			<th><?php echo JTHEF_TEMPLATE;?></th>
			<th><?php echo JTHEF_DESCRIPTION;?></th>
			<th><?php echo JTHEF_PREVIEW;?></th>
			<th width="10"><?php echo F_DELETE;?></th>
		</tr>
		<?php 
		if($positions)
		for ($i=0; $i<count($positions); $i++){ 
			?>
		<tr>
			<td>
				<a href="index.php?option=<?php echo APP_EXTENSION;?>&task=fposition_panel&act=edit&id=<?php echo $positions[$i]['id'];?>"><?php echo $positions[$i]["title"]."  ( ".$positions[$i]["name"]." )"; ?></a>
			</td>
			<td>
				<?php if($positions[$i]["tpl"]) echo $positions[$i]["tpl"]; else echo "Details Tpl" ?>
			</td>
			<td>
				<?php echo $positions[$i]["description"]; ?>
			</td>
			<td>
				<?php echo htmlentities($positions[$i]["html"]); ?>
			</td>
			<td align="center"><a href="#" onclick="if(confirm('Are you sure?')=='1'){ document.getElementById('act_id').value='remove'; document.getElementById('id_id').value='<?php echo $positions[$i]["id"];?>'; document.adminForm.submit();}"><img src="<?php echo F_ROOT_URI;?>/components/<?php echo APP_EXTENSION;?>/img/delete.png" border="0" /></a></td>
		</tr>
		<?php } ?>
	</table>
	<input type="hidden" name="id" id="id_id" value="" />
	<input type="hidden" name="act" id="act_id" value="save" />
</form>
