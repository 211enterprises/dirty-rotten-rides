<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

require_once(dirname(__FILE__).DS."..".DS."tmpl".DS."position.toolbar.php");
	
?>

<script type="text/javascript">

function getFieldId( id_string ){
	return id_string.replace("custom_field_","");
}

function getPositionId( index ){
	return document.getElementById("position_id_"+index).value;
}

function appendField2Position(elemHTML, field_id, position_id ){
	
	var position  = getPositionId(position_id);
	
	var remove_link = "<a onclick=\"removeFieldFromPosition(" + field_id + "," + position + "); \"><img src='<?php echo JURI::root();?>/administrator/components/<?php echo APP_EXTENSION;?>/thefactory/fields/assets/delete.png' border='0' /></a>";
	var docum = "<div id='aux_" + position + "_" + field_id + "'>" + elemHTML +  remove_link + "</div>";
	
	var fi = document.getElementById("position_" + position).value;
	if( fi.search("#" + field_id + "#") == -1)
		document.getElementById("position_" + position).value += "#" + field_id + "#";
	else
		docum = "";
	
	return docum;
}

function removeFieldFromPosition( field_id, position_id ){
	
	$('aux_' + position_id + '_' + field_id ).remove(); 
	var strtmp = document.getElementById("position_" + position_id).value;
	strtmp = strtmp.replace("#" + field_id + "#","");
	document.getElementById("position_" + position_id).value = strtmp;
	return false;
}



function clearAll( position_id ){
	document.getElementById('drop_' + position_id).innerHTML='';
	document.getElementById("position_" + position_id).value = "";
	return false;
}

</script>
<style type="text/css">
	.item {
		position: relative;
		width: 150px;
		height: 30px;
		border: 1px solid #eee;
		margin: 10px;
		border-right: 2px solid #ddd;
		border-bottom: 2px solid #ddd;
		background-color: #fff;
		background-position: left top;
		background-repeat: no-repeat;
		cursor: pointer;
	}

	.item_clone {
		border: 1px solid #AAA;
		border-right: 2px solid #ddd;
		border-bottom: 2px solid #ddd;
		background-color: #fff;
		background-position: left top;
		background-repeat: no-repeat;
		cursor: pointer;
	}
	
	.item span
	{
		position: absolute;
		bottom: 0;
		left: 0;
		font-size: 0.8em;
		font-weight: bold;
		width: 100%;
		text-align: center;
	 
	}
	
	
	.pm{
		margin: 10px;
		border:2px solid #DDDDDD; 
		width:250px; 
		height:200px; 
		float:left; 
		margin:2px; padding:2px;	
		overflow:auto;
		background: #DEDEDE;
	}		
	.pm span{
		border: 1px solid #eee;
		border-right: 2px solid #ddd;
		border-bottom: 2px solid #ddd;
	}
	.position_title{
		background:#EDEDED !important; 
		padding:3px;
		width:230px;
		height:40px;
	}
	.drop {
		width:230px; 
		height:150px; 
		background: #DEDEDE;
	}
	
</style>
<form name="adminForm" method="POST">
	<input type="hidden" name="task" value="fposition_fields" />
	<input type="hidden" name="option" value="<?php echo APP_EXTENSION;?>" />
	<input type="hidden" name="act" value="save" />
	<input type="hidden" name="list_type" value="drag" />
	
	<table class="admintable">
		<td valign="top">
				<fieldset>
					<legend><?php echo JText::_('ADS_FIELDS_ON_PAGES');?></legend>
					<table class="adminlist">
					<tr>
					<thead>
						<th><?php echo JText::_('ADS_FIELD');?></th>
					</thead>
					</tr>
					</table>
					<div style="height:50px;">
					<h3 id="tool_info"><?php echo JText::_('ADS_CLICK_FIELD_TO_DRAG');?></h3>
					</div>
					<div id="items">
					<?php 
					if($customFields)
					for ($i=0; $i<count($customFields); $i++){
						$field_id = $customFields[$i]["id"];
						?>
						<div id="custom_field_<?php echo $field_id;?>" class="item">
							<span><?php echo $customFields[$i]["name"]."( ".$customFields[$i]["db_name"]." )";?></span>
						</div>
					<?php } ?>
					</div>
				</fieldset>
		</td>
		<td valign="top">
			<table class="adminlist">
				<thead>
					<tr>
						<td>
						&nbsp;
						<div style="color:#FF0000;" id="log_fields_events"></div>
						</td>
					</tr>
				</thead>
			</table>
			<div id="droppables">
				<?php 
				foreach($positions as $o => $pos ){	
					$id = $pos["id"];
					$title = $pos["title"]."  (".$pos["tpl"].")";
					?>
					<div class="pm">
						<input type="hidden" id="position_id_<?php echo $o;?>" value="<?php echo $id;?>" />
						<div class="position_title">
						<?php echo $title;?>
						<a href="#" onclick="clearAll(<?php echo $id;?>);return false;" class="clear_fields"><img src="<?php echo JURI::root();?>/administrator/images/cancel_f2.png" width="20" border="0" align="right" /></a>
						</div>
						<div id="drop_<?php echo $id;?>" class="drop">
						<?php $fisharp = ""; ?>
						<?php for ($ll=0; $ll<count($pos["fields"]); $ll++ ){ ?>
							<div id='aux_<?php echo $id;?>_<?php echo $pos["fields"][$ll]->id;?>'> 
							<span><?php echo $pos["fields"][$ll]->name."( ".$pos["fields"][$ll]->db_name." )";?></span>
							<a onclick="removeFieldFromPosition(<?php echo $pos["fields"][$ll]->id;?>,<?php echo $id;?>);" ><img src='<?php echo JURI::root();?>/administrator/components/<?php echo APP_EXTENSION;?>/thefactory/fields/assets/delete.png' border='0' /></a>
							</div>
							<?php $fisharp .= "#".$pos["fields"][$ll]->id."#";?>
						<?php } ?>
						</div>
						<input type="hidden" name="position_<?php echo $id;?>" id="position_<?php echo $id;?>" value="<?php echo $fisharp;?>" />
					</div>
					<?php
				}
				?>
			</div>
		</td>
	</table>
</form>

<script>
var fx = [];
var myTransition = new Fx.Transition(Fx.Transitions.Elastic, 3);


$$('.item').each(function(item){
	item.addEvent('mousedown', function(e) {
		document.getElementById('tool_info').innerHTML='Start Dragging';
		e = new Event(e).stop();
 
		var clone = this.clone()
			.setStyles(this.getCoordinates()) // this returns an object with left/top/bottom/right, so its perfect
			.addClass('item_clone')
			.setStyles({'opacity': 0.7, 'position': 'absolute'})
			.addEvent('emptydrop', function() {
				this.remove();
			}).inject(document.body);
		clone.style.cursor="move";
			
		new Drag.Move(clone, {
			droppables: $$('#droppables div')
		});
	 
		clone.addEvent('emptydrop', function(){
			document.getElementById('tool_info').innerHTML='Click field to drag';
			this.setStyle('background-color', '#faec8f');
		});

	});
	
});

$$('#droppables .drop').each(function(drop, index){
	fx[index] = drop.effects({transition:Fx.Transitions.Back.easeOut});
	drop.addEvents({
		'over': function(el, obj){
					this.setStyle('background-color', '#ACACAC');
				},
		'leave': function(el, obj){
			this.setStyle('background-color', '#DEDEDE');
		},
		'drop': function(el, obj){
			var dd = appendField2Position( el.innerHTML , getFieldId(el.id) , index );
			if(dd==""){
				document.getElementById('log_fields_events').innerHTML = "Field already exists in position!";
				el.remove();
			document.getElementById('tool_info').innerHTML='Click field to drag';
				return;
			}
			document.getElementById('log_fields_events').innerHTML = "Field added in position!";
			this.innerHTML += dd;
			el.remove();
			document.getElementById('tool_info').innerHTML='Click field to drag';
			fx[index].start({
				'height': this.getStyle('height').toInt() + 10
			});
		}
	});
});

</script>