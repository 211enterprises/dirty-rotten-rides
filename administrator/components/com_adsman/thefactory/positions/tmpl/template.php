<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

require_once(dirname(__FILE__).DS."..".DS."tmpl".DS."position.toolbar.php");
?>
<form name="adminForm" method="POST">
	<input type="hidden" name="task" value="fposition_panel" />
	<input type="hidden" name="option" value="<?php echo APP_EXTENSION;?>" />
<?php		
	if ($content !== false)
		{
			// Set FTP credentials, if given
			jimport('joomla.client.helper');
			$ftp =& JClientHelper::setCredentialsFromRequest('ftp');

			$content = htmlspecialchars($content, ENT_COMPAT, 'UTF-8');
			// move to HTML Class
			?>
			<form action="index.php" method="post" name="adminForm">
			<input type="submit" value="Save" />
	
			<?php if($ftp): ?>
			<fieldset title="<?php echo JText::_('DESCFTPTITLE'); ?>">
				<legend><?php echo JText::_('DESCFTPTITLE'); ?></legend>
	
				<?php echo JText::_('DESCFTP'); ?>
	
				<?php if(JError::isError($ftp)): ?>
					<p><?php echo JText::_($ftp->message); ?></p>
				<?php endif; ?>
	
				<table class="adminform nospace">
				<tbody>
				<tr>
					<td width="120">
						<label for="username"><?php echo JText::_('ADS_USERNAME'); ?>:</label>
					</td>
					<td>
						<input type="text" id="username" name="username" class="input_box" size="70" value="" />
					</td>
				</tr>
				<tr>
					<td width="120">
						<label for="password"><?php echo JText::_('ADS_PASSWORD'); ?>:</label>
					</td>
					<td>
						<input type="password" id="password" name="password" class="input_box" size="70" value="" />
					</td>
				</tr>
				</tbody>
				</table>
			</fieldset>
			<?php endif; ?>
	
			<script type="text/javascript">
				
				function setSel(start,end){
				  var t=document.getElementById('filecontent');
				  if(t.setSelectionRange){
				  	t.setSelectionRange(start,end); t.focus();
				  }
				}
				
				function focus_tpl(code){
				  var pval=document.getElementById('filecontent').value; var t = new String(pval);
				  var s = t.indexOf(code);
				  if(s>0){ var e=code.length; setSel(s,s+e);}
				}
				
				function insertAtCursor(myField, myValue) {
				//IE support
				if (document.selection) {
				myField.focus();
				sel = document.selection.createRange();
				sel.text = myValue;
				}
				//MOZILLA/NETSCAPE support
				else if (myField.selectionStart || myField.selectionStart == '0') {
				var startPos = myField.selectionStart;
				var endPos = myField.selectionEnd;
				myField.value = myField.value.substring(0, startPos)
				+ myValue
				+ myField.value.substring(endPos, myField.value.length);
				} else {
				myField.value += myValue;
				}
				}
				
				function append_tpl(code){
					var t=document.getElementById('filecontent');
					insertAtCursor(t,code);
					focus_tpl(code);
				}
				
			</script>
			<table class="adminlist">
			<tr>
				<th>
					<?php echo $file; ?>
				</th>
				<th>
				</th>
			</tr>
			<tr>
				<td width="80%">
					<textarea style="width:80%;height:500px; cursor:text;" cols="110" rows="25" id="filecontent" name="filecontent" class="inputbox"><?php echo $content; ?></textarea>
				</td>
				<td valign="top" align="left">
                    <?php echo JText::_('ADS_POSITIONS');?>
				<table class="adminlist" style="width:300px !important;"  >
				<?php 
				if ( count( $positions ) )
					foreach ( $positions as $p => $pi){
						?>
						<tr><td align="left">
						<?php
						$pname = $pi["name"];
						$tpl = $pi["tpl"];
						if(strstr($tpl,"list") )
							$code = "{include file='elements/display_fields.tpl' position='$pname' this_add=".htmlentities("$")."item->id}";
						else
							$code = "{include file='elements/display_fields.tpl' position='$pname'}";
						$code = addslashes($code);
						?>	
						<?php if($pi["verified"]) { ?>
						<font color="Green"><?php echo JText::_('ADS_VERIFIED');?></font>
						<a href="javascript:focus_tpl('<?php echo $code;?>');">
							<img src="<?php echo F_ROOT_URI;?>components/<?php echo APP_EXTENSION;?>/img/go_back.png">
                            <?php echo JText::_('ADS_GO_AT_POSITION');?>
						</a>
						<?php }else{ ?>
						<font color="red"><?php echo JText::_('ADS_NOT_VERIFIED');?></font>
						<a href="javascript:append_tpl('<?php echo $code;?>');">
							<img src="<?php echo F_ROOT_URI;?>components/<?php echo APP_EXTENSION;?>/img/go_back.png">
                            <?php echo JText::_('ADS_INSERT_AT_CURSOR');?>
						</a>
						<?php } ?>
						<?php echo $pi["title"];?>
						</td></tr>
						<?php
					}
				?>
				</table>
				</td>
			</tr>
			</table>
	
			<div class="clr"></div>
	
			<input type="hidden" name="tpl" value="<?php echo $template; ?>" />
			<input type="hidden" name="option" value="<?php echo $option;?>" />
			<input type="hidden" name="task" value="fposition_templates" />
			<input type="hidden" name="act" value="save" />
			<input type="hidden" name="client" value="<?php echo $client->id;?>" />
			<?php echo JHTML::_( 'form.token' ); ?>
			</form>
			<?php
			
		} else {
			echo $msg = JText::sprintf('Operation Failed Could not open', $file);
			//$mainframe->redirect('index.php?option='.$option.'&client='.$client->id, $msg);
		}
