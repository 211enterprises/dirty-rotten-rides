<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

$bar = & JToolBar::getInstance('toolbar');
$act = & JRequest::getCmd("act","list");
$task = & JRequest::getVar("task");

if($task =="field_administrator")
	$bar->appendButton( 'Custom', '<span class="icon-32-step1"></span><strong>'. JText::_("ADS_FIELDS_STEP1").'</strong>' );
else
	$bar->appendButton( 'Link', 'step1', JText::_("ADS_FIELDS_STEP1"), "index.php?option=".APP_EXTENSION."&task=field_administrator",false);

if($task =="fposition_panel")
	$bar->appendButton( 'Custom', '<span class="icon-32-step2"></span><strong>'. JText::_("ADS_FIELDS_STEP2").'</strong>' );
	
else	
	$bar->appendButton( 'Link', 'step2', JText::_("ADS_FIELDS_STEP2"), "index.php?option=".APP_EXTENSION."&task=fposition_panel&act=list",false);
	
if($task =="fposition_fields")
	$bar->appendButton( 'Custom', '<span class="icon-32-step3"></span><strong>'. JText::_("ADS_FIELDS_STEP3").'</strong>' );
	
else	
	$bar->appendButton( 'Link', 'step3', JText::_("ADS_FIELDS_STEP3"), "index.php?option=".APP_EXTENSION."&task=fposition_fields",false);

$list_type = & JRequest::getCmd("list_type","fields_positions");
JHTML::_('behavior.modal'); 

if($task=="fposition_fields") { 

?>

   	<table class="adminlist">
   		<thead>
   		
   		<tr>
			<td style="text-align:left;">
				<h2><img src="<?php echo F_ROOT_URI;?>administrator/components/com_adsman/thefactory/fields/assets/cfields_assign.png" border="0" style="vertical-align:middle;" />
				<?php echo JTHEF_POSITIONS_CPANEL." / ".JTHEF_POSFIELDS_TITLE;?></h2>
				<h3><?php echo JTHEF_POSFIELDS_SUBTITLE;?></h3>
			</td>
			<td align="right" width="100" valign="bottom">
			<table style="border: 1px solid #FFFFFF; padding: 2px;">
				<tr>
				<?php	if($list_type=="fields_positions") { ?>
						<td width="50" style="text-align: center; vertical-align: bottom;">
						<a href="#" onclick="javascript:document.adminForm.submit();">
						<strong><span style="text-align: center;"><?php echo F_SAVE;?></span></strong><br />
							<img src="<?php echo F_ROOT_URI;?>/administrator/components/<?php echo F_COMPONENT_NAME;?>/thefactory/fields/assets/filesave.png" border="0" />
						</a>
					</td>
				<?php } elseif ($list_type=="drag"){ ?>
					<td width="50" style="text-align: center; vertical-align: bottom;">
						<a href="#" onclick="javascript:document.adminForm.submit();">
						<strong><span style="text-align: center;"><?php echo F_SAVE;?></span></strong><br />
							<img src="<?php echo F_ROOT_URI;?>/administrator/components/<?php echo F_COMPONENT_NAME;?>/thefactory/fields/assets/filesave.png" border="0" />
						</a>
					</td>						
					
				<?php } ?>
	
		   			<td width="50" style="text-align: center; vertical-align: bottom;">
							<a class="modal" rel="{handler: 'iframe', size: {x: 640, y: 480}, onClose: function() {}}" href="index.php?option=<?php echo APP_EXTENSION;?>&task=fposition_panel&amp;act=help">
								<strong><?php echo F_HELP_POSITIONS;?></strong><br />	
								<img src="<?php echo F_ROOT_URI;?>/administrator/components/<?php echo F_COMPONENT_NAME;?>/thefactory/fields/assets/help.png" border="0" alt="Help" />
							</a>
						</td>
   				</tr>
			
				<tr>
					<td colspan="2">
			
				<?php if($act =="list" ) { ?>
				<div class="button2-left" style="width:150px;">
					<a href="index.php?option=<?php echo APP_EXTENSION;?>&task=fposition_fields&list_type=drag" <?php if($list_type=="drag") { ?> style="font-size:18px;" <?php } ?> >
						<strong><?php echo JText::_('ADS_FIELDS_DRAGGABLE_VIEW');?></strong>
					</a>
				</div>
				<div class="button2-left" style="width:150px;">
					<a href="index.php?option=<?php echo APP_EXTENSION;?>&task=fposition_fields" <?php if($list_type=="fields_positions") { ?> style="font-size:18px;" <?php } ?> >
						<strong><?php echo JText::_('ADS_FIELDS_DETAILED_VIEW');?></strong>
					</a>
				</div>
				<div class="button2-left" style="width:150px;">
					<a href="index.php?option=<?php echo APP_EXTENSION;?>&task=fposition_fields&list_type=fields" <?php if($list_type=="fields") { ?> style="font-size:18px;" <?php } ?>>
						<strong><?php echo JText::_('ADS_FIELDS_LIST_VIEW');?></strong>
					</a>
				</div>
				<?php } ?>
					</td>
				</tr>
			</table>
			</td>
		</tr>
		</thead>
	</table>
	
<?php } elseif ($task=="fposition_panel") { ?>
   	<table class="adminlist" width="100%">
   		<thead>
   		<tr>
			<td style="text-align:left !important;">
				<h2><img src="<?php echo F_ROOT_URI;?>administrator/components/com_adsman/thefactory/fields/assets/positions_big.png" border="0"  style="vertical-align:middle;" />
				<?php echo JTHEF_POSITIONS." / ". JTHEF_POSITIONS_TEMPLATE_SYNTAX ;?>
				</h2>
				<h3><?php echo JTHEF_POSITIONS_SUBTITLE;?></h3>
			</td>
			<td align="right"> 
				<table style="border: 1px solid #FFFFFF; padding: 2px;">
					<tr>
						<?php if($act == "edit" || $act =="new") { ?>
						
						<td width="50" style="text-align: center; vertical-align: bottom;">
							<a href="#" onclick="validForm('save');">
							<strong><?php echo F_SAVE;?></strong><img src="<?php echo F_ROOT_URI;?>/administrator/components/<?php echo F_COMPONENT_NAME;?>/thefactory/fields/assets/filesave.png" border="0" />
							</a>
						</td>
						<?php } ?>
					
					
						<td width="60" style="text-align: center; vertical-align: bottom;">
						<?php if($act!="" && $act!="list") { ?>
							<a href="index.php?option=<?php echo APP_EXTENSION;?>&task=<?php echo $task;?>&act=list">
								<strong><?php echo F_BACK;?></strong><br />
								<img src="<?php echo F_ROOT_URI;?>/administrator/components/<?php echo F_COMPONENT_NAME;?>/thefactory/fields/assets/back.png" />
							</a>	
						<?php } else { ?>
							<a href="index.php?option=<?php echo F_COMPONENT_NAME;?>&task=fposition_panel&act=new">
								<strong><span style="float: left; text-align: center;"><?php echo JTHEF_NEW_POSITION;?></span></strong>
								<img src="<?php echo F_ROOT_URI;?>/administrator/components/<?php echo F_COMPONENT_NAME;?>/thefactory/fields/assets/add_position.png" />
							</a>
						<?php } ?>	
						</td>
					
						<td width="50" style="text-align: center; vertical-align: bottom;">
							<a href="index.php?option=<?php echo F_COMPONENT_NAME;?>&task=fposition_fields">
								<strong><?php echo F_STEP3;?></strong><br />
								<img src="<?php echo F_ROOT_URI;?>/administrator/components/<?php echo F_COMPONENT_NAME;?>/thefactory/fields/assets/cfields_assign_template.png" />
							</a>	
						</td>
						<td width="50" style="text-align: center; vertical-align: bottom;">
						
							<a class="modal" rel="{handler: 'iframe', size: {x: 640, y: 480}, onClose: function() {}}" href="index.php?option=<?php echo APP_EXTENSION;?>&task=fposition_panel&amp;act=help">
								<strong><?php echo F_HELP_POSITIONS;?></strong><br />	
								<img src="<?php echo F_ROOT_URI;?>/administrator/components/<?php echo F_COMPONENT_NAME;?>/thefactory/fields/assets/help.png" border="0" alt="Help" />
							</a>
												
						</td>
						
					</tr>
				</table>
			</td>		
		</tr>
		</thead>
	</table>
<?php } elseif ($task=="fposition_templates") { ?>
   	<table class="adminlist" width="100%">
   		<thead>
   		<tr>
			<th style="text-align:left !important;"><?php echo 'v'. F_ROOT_URI;?>
				<h1><img src="<?php echo F_ROOT_URI;?>administrator/components/com_adsman/thefactory/fields/assets/positions_big.png" border="0"><?php echo JTHEF_POSITIONS_CPANEL." / ".JTHEF_TEMPLATES_TITLE;?></h1>
				<h2><?php echo JTHEF_POSITIONS_SUBTITLE;?></h2>
			</th>
		</tr>
	</table>
<?php } 

$bar->appendButton( "separator", "divider");

?>