<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

require_once(dirname(__FILE__).DS."..".DS."tmpl".DS."position.toolbar.php");?>

<form name="adminForm" method="POST">
	<input type="hidden" name="task" value="fposition_panel" />
	<input type="hidden" name="option" value="<?php echo APP_EXTENSION;?>" />
		<script type="text/javascript">
		  
		  function prep4SQL(o){
			if(o.value!='') {
				var cbsqloldvalue, cbsqlnewvalue;
				cbsqloldvalue = o.value; o.value=o.value.replace(/[^a-zA-Z0-9]+/g,''); cbsqlnewvalue = o.value;
				if (cbsqloldvalue != cbsqlnewvalue) { alert("Warning: SQL name of field has been changed to fit SQL constraints") }
			}
		  }
		  
		  function validForm(){
		  	var isNew = <?php echo $isNew;?>;
		  	var errors = "";
		  	
			if(!isNew){
				var PositionTitle = "-";
			  	var PositionName = "-";
			}else{
				var PositionTitle = document.getElementById("ptitle").value;
			  	var PositionName = document.getElementById("pname").value;
				prep4SQL(document.getElementById("pname"));
			}
			
		  	var PositionHTML = document.getElementById("phtml").value;
		  	var testHTML  = checkHTML(PositionHTML);
		  	
			
		  	if( PositionTitle!="" && PositionName!="" && testHTML==true ){
		  		document.adminForm.submit();
		  	}else{
		  		if( PositionName=="" ){ errors += "The unique name is compulsory!\r\n"; }
		  		if( PositionTitle=="" ){ errors += "The title is compulsory!\r\n"; }
		  		if( testHTML != "" ){ errors += testHTML + "\r\n"; }
		  		alert(errors);
		  	}
		  }
		  
		  function checkHTML(p){
		  	if(p.indexOf("%VALUE%")==-1) return " %VALUE% must be in the layout of the item!";
		  	return true;
		  }
		  
		</script>
		
		<table class="adminform" border="1">
			<tr>
				<td colspan="3">
				<?php if(!$isNew){ ?>
					Use this tool to get the smarty snippet to include in the template:<br />
					<a href="#" onclick="var j=document.getElementById('nn');j.focus();j.select();">
						<strong>Select smarty</strong><img src="<?php echo F_ROOT_URI;?>/components/<?php echo APP_EXTENSION;?>/img/go_back.png" />
					</a>
					<br />
					<?php 
					if(strstr($position->tpl,"list") )
						$code = "{include file='elements/display_fields.tpl' position='$position->name' this_add=".htmlentities("$")."item->id}";
					else
						$code = "{include file='elements/display_fields.tpl' position='$position->name'}";
					?>
					<input size="100" type="text" id="nn" name="nn" value="<?php echo $code?>" style=";" readonly />
					<br /><br />
				<?php } ?>	
				</td>
			</tr>
			<tr>
				<td><?php echo JTHEF_POSITION;?></td>
		<?php if($isNew){ ?>
				<td><input type="text" id="ptitle" name="title" size="100" /></td>
		<?php }else{ ?>
				<td><?php echo $position->title;?></td>
		<?php } ?>
				<td><?php echo JTHEF_PMAN_TITLE_DESCR;?></td>
			</tr>
			<tr>
				<td><?php echo JTHEF_PMAN_NAME;?><?php echo FactoryHTML::tooltip(JTHEF_PMAN_NAME_HELP);?></td>
		<?php if($isNew){ ?>
				<td><input type="text" id="pname" onchange="prep4SQL(this)" name="name" size="100" /></td>
		<?php }else{ ?>
				<td><?php echo $position->name;?></td>
		<?php } ?>
				<td><?php echo JTHEF_PMAN_NAME_DESCR;?></td>
			</tr>
			<tr>
				<td><?php echo JTHEF_PMAN_DESCR;?><?php echo FactoryHTML::tooltip(JTHEF_PMAN_DESCR_HELP);?></td>
		<?php if($isNew){ ?>
				<td><textarea name="description" rows="5" cols="50"></textarea></td>
		<?php }else{ ?>
				<td><?php echo $position->description;?></td>
		<?php } ?>
				<td><?php echo JTHEF_PMAN_DESCR_DESCR;?></td>
			</tr>
			<tr>
				<td><?php echo JTHEF_PMAN_TPL;?><?php echo FactoryHTML::tooltip(JTHEF_PMAN_TPL_HELP);?></td>
		<?php if($isNew){ 
			$tpls  = explode(",",APP_POSITION_TEMPLATES);
			?>
				<td><select name="tpl"><?php foreach ($tpls as $tpl) echo '<option value="'.$tpl.'">'.$tpl.'</option>';?></select></td>
		<?php }else{ ?>
				<td><?php echo $position->tpl;?></td>
		<?php } ?>
				<td><?php echo JTHEF_PMAN_TPL_DESCR;?></td>
			</tr>
			<tr>
				<td><?php echo JTHEF_PMAN_ITEMHTML;?><?php echo FactoryHTML::tooltip(JTHEF_PMAN_ITEMHTML_HELP);?></td>
				<td valign="top"><textarea id="phtml" name="html" rows="5" cols="50"><?php if( isset($position->html) ) echo $position->html;?></textarea></td>
				<td><?php echo JTHEF_PMAN_ITEMHTML_DESCR;?></td>
			</tr>
		<?php
        /*
		if(!$isNew){ ?>
			<tr>
				<td><?php echo JTHEF_VERIFIED;?><?php echo FactoryHTML::tooltip(JTHEF_VERIFIED_HELP);?></td>
				<td><?php if( $position->verified ) echo "Yes"; else echo "No"; ?></td>
				<td><?php echo JTHEF_VERIFIED_DESC;?></td>
			</tr>
		<?php } */
		?>
			<tr>
				<td colspan="3">
					<input type="button" value="Save" onclick="validForm()" />
				</td>
			</tr>
		</table>
	<?php if($isNew){ ?>
		<input type="hidden" name="act" value="add" />
	<?php }else{ ?>
		<input type="hidden" name="act" value="save" />
		<input type="hidden" name="id" value="<?php echo $id;?>" />
	<?php } ?>
</form>
