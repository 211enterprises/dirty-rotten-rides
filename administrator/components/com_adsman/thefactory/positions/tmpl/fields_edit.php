<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

require_once(dirname(__FILE__).DS."..".DS."tmpl".DS."position.toolbar.php");
?>
<form name="adminForm" method="POST">
	<input type="hidden" name="task" value="fposition_fields" />
	<input type="hidden" name="option" value="<?php echo APP_EXTENSION;?>" />
	<input type="hidden" name="list_type" value="fields" />
   	<table class="adminlist">
   		<tr>
			<td colspan="2">
				<h1><img src="<?php echo F_ROOT_URI;?>administrator/images/cfields_assign.png" border="0"><?php echo JTHEF_POSITIONS_CPANEL." / ".JTHEF_POSFIELDS_TITLE;?></h1>
				<h2><?php echo JTHEF_POSFIELDS_SUBTITLE;?></h2>
			</td>
		</tr>
	</table>
<?php 
    	if($customFieldsHTML) {
    		$positionsHTML = FactoryHTML::selectGenericList($positionsOpts,"position", $jtable->position);
		?>
		<table class="adminlist">
		<tr>
			<td><?php echo JText::_('ADS_FIELD');?></td>
			<td><?php echo $customFieldsHTML;?></td>
		</tr>
		<tr>
			<td><?php echo JText::_('ADS_POSITION');?></td>
			<td id="am_putat"><?php echo $positionsHTML;?></td>
		</tr>
		<tr>
			<td colspan="2">
				<input type="submit" value="Save" />
			</td>
		</tr>
		</table>
		
	<input type="hidden" name="id" value="<?php echo $id;?>" />
	<input type="hidden" name="act" value="save" />
</form>
		<?php
    	} else
    		echo JText::_('ADS_NO_MORE_FIELDS_TO_ASSIGN');
?>