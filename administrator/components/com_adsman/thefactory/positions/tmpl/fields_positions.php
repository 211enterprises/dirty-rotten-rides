<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

require_once(dirname(__FILE__).DS."..".DS."tmpl".DS."position.toolbar.php");

jimport('joomla.html.pane');
$pane	= &JPane::getInstance('sliders', array('allowAllClose' => true));
	
?>
<script type="text/javascript">
var pitem;
function resize(){
	document.getElementById(pitem).style.border =  "none";
}

function popup(item){
	document.getElementById(item).style.border =  "1px solid #FF0000";
	pitem = item;
	setTimeout("resize()",1000);
}

function refreshOptions(e_id){
	var e = document.getElementById(e_id);
	
	var display_var = "";
	var i = 0;
	var n = e.options.length;
	for (i = 0; i < n; i++) {
		if(e.options[i].selected)
			display_var += e.options[i].text + ",";
	}
	if(!display_var)
		display_var = "Unassigned";
	document.getElementById("info_"+e_id).innerHTML=display_var;
	popup("info_"+e_id);
}

function allcategories(e_id) {
	var e = document.getElementById(e_id);
	
	var i = 0;
	var n = e.options.length;
	for (i = 0; i < n; i++) {
		e.options[i].selected = true;
	}
}

function nocategory(e_id) {
	var e = document.getElementById(e_id);
	
	var i = 0;
	var n = e.options.length;
	for (i = 0; i < n; i++) {
		e.options[i].selected = false;
	}
}
</script>
<form name="adminForm" method="POST">
	<input type="hidden" name="task" value="fposition_fields" />
	<input type="hidden" name="option" value="<?php echo APP_EXTENSION;?>" />
	<input type="hidden" name="act" value="save" />
	<input type="hidden" name="list_type" value="fields_positions" />
	
	<table class="adminlist">
		<tr>
			<td valign="top" width="40%">
				<fieldset>
					<legend><?php echo JText::_('ADS_FIELDS_ON_PAGES');?></legend>
					<table class="adminlist">
					<tr>
					<thead>
						<th><?php echo JText::_('ADS_FIELD');?></th>
						<th width="200"><?php echo JText::_('ADS_TEMPLATE_POSITION');?></th>
					</thead>
					</tr>
					<?php 
					if($customFields)
					for ($i=0; $i<count($customFields); $i++){
						$field_id = $customFields[$i]["id"];
						?>
					<tr class="row_<?php echo (int)($i%2);?>">
						<td>
						<a href="#" onclick="$$('#fields_pane<?php echo $field_id;?>').fireEvent('click');popup('fields_pane<?php echo $field_id;?>'); return false;">
						<strong>
						<?php echo $customFields[$i]["name"]."( ".$customFields[$i]["db_name"]." )";?>
						</strong>
						</a>
						<br />
						<a href="#" onclick="$$('#fields_pane<?php echo $field_id;?>').fireEvent('click');popup('fields_pane<?php echo $field_id;?>'); return false;"><?php echo JText::_('ADS_ASSIGN_ON_PAGES');?></a>
						</td>
						<td id="info_field<?php echo $field_id;?>">
						<?php 
						if($customFields[$i]["ptitles"]!="")
							echo $customFields[$i]["ptitles"];
						else	
							echo JText::_('ADS_UNASIGNED');
						?>
						</td>
					</tr>
					<?php } ?>
					</table>
				</fieldset>
			</td>
			<td valign="top">
				<fieldset>
					<legend><?php echo JText::_('ADS_ASSIGN_FIELDS_ON_PAGES');?></legend>
				<?php 
				echo $pane->startPane("content-pane");
				if($customFields)
				for ($i=0; $i<count($customFields); $i++){
					
					$field_id = $customFields[$i]["id"];
					$selected_positions = array();
					if($customFields[$i]["pids"])
						$selected_positions = explode(",",$customFields[$i]["pids"]);
						
					echo $pane->startPanel( $customFields[$i]["name"]."( ".$customFields[$i]["db_name"]." )", "fields_pane{$field_id}" );
					?>
					<table>
      				  <tr>
						<td>
						<?php
			    		    echo FactoryHTML::selectGenericList($positionsOpts,"field{$field_id}[]",$selected_positions,' onchange="refreshOptions(this.id);"  multiple="multiple" style="width:350px;"');
						?>
						<input class="fields_input" type="radio" value="1" name="fassign<?php echo $field_id;?>" onclick="allcategories('field<?php echo $field_id;?>');refreshOptions('field<?php echo $field_id;?>');" />
						<span style="float: left; padding-right: 10px;"><?php echo JText::_('ADS_ALL');?></span>
						
						<input class="fields_input" type="radio" value="0" name="fassign<?php echo $field_id;?>" onclick="nocategory('field<?php echo $field_id;?>');refreshOptions('field<?php echo $field_id;?>');" />
						 <span style="float: left"><?php echo JText::_('ADS_NONE');?></span>
						</td>
				      </tr>
					</table>
					<?php echo $pane->endPanel();?> 
				<?php } 
				echo $pane->endPane();
				?>
				</fieldset>
			</td>
		</tr>
	</table>
</form>
