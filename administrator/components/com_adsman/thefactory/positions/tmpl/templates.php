<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

require_once(dirname(__FILE__).DS."..".DS."tmpl".DS."position.toolbar.php");
?>
<form name="adminForm" method="POST">
	<input type="hidden" name="task" value="fposition_panel" />
	<input type="hidden" name="option" value="<?php echo APP_EXTENSION;?>" />
		<table width="100%" class="adminlist">
			<tr>
				<th width="80">Edit</th>
				<th>Template</th>
				<th align="center">Writable</th>
			</tr>
	<?php
	
	foreach ($templates as $k => $tpl){
		// ads is themed;
		// no for ads:
		// $tpl_path = F_ROOT_DIR."/components/".APP_EXTENSION."/templates/".$tpl;
		
		$tpl_path = F_ROOT_DIR."/components/".APP_EXTENSION."/templates/".ADDSMAN_TPL_THEME."/".$tpl;
		if(!file_exists($tpl_path))
			$tpl_path = F_ROOT_DIR."/components/".APP_EXTENSION."/templates/default/".$tpl;
		
		?>
			<tr>
				<td><a href="index.php?option=<?php echo APP_EXTENSION;?>&task=fposition_templates&act=edit&tpl=<?php echo $tpl;?>">ADS_EDIT</a></td>
				<td><?php echo $tpl;?></td>
				<td align="center"><?php 
				
				if( is_writable($tpl_path))
					echo "<font color='green'>" . JText::_('ADS_WRITABLE') . "</font>";
				else 
					echo "<font color='red'>" . JText::_('ADS_NOT_WRITABLE') . "</font>";
				?></td>
			</tr>
		<?php
	}
	?>
		</table>
	<?php
