<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

require_once(dirname(__FILE__).DS."..".DS."tmpl".DS."position.toolbar.php");
?>
<form name="adminForm" method="POST">
	<input type="hidden" name="task" value="fposition_panel" />
	<input type="hidden" name="option" value="<?php echo APP_EXTENSION;?>" />
	<a href="index.php?option=<?php echo APP_EXTENSION;?>&task=fposition_fields&act=new">
		<img src="<?php echo JURI::root();?>administrator/images/module.png" />
		<strong><?php echo JText::_('ADS_FIELDS_NEW_ASSIGN');?></strong>
	</a>
	<table class="adminlist">
		<tr>
			<th><?php echo JText::_('ADS_FIELD');?></th>
			<th><?php echo JText::_('ADS_FIELD_POSITIONS');?></th>
            <th align="center" width="80"><?php echo JText::_('ADS_DELETE');?></th>
		</tr>
		<?php 
		if($customFields)
		for ($i=0; $i<count($customFields); $i++){
			$pos = $customFields[$i]["id"];
			?>
		<tr>
			<td>
				<?php echo $customFields[$i]["name"]."( ".$customFields[$i]["db_name"]." )"; ?>
			</td>
			<td>
				<?php 
				if(!$customFields[$i]["title"]) echo "Unassigned";
				else echo $customFields[$i]["title"]." - ".$customFields[$i]["tpl"];
				?>
			</td>
			<td align="center">
				<?php if($customFields[$i]["title"]){  ?>
				    <a href="#" onclick="if(confirm('Are you sure?')=='1'){ document.location='<?php echo F_ROOT_URI;?>administrator/index.php?option=<?php echo APP_EXTENSION;?>&task=fposition_fields&act=del&id=<?php echo $pos;?>';}"><img src="<?php echo F_ROOT_URI;?>components/<?php echo APP_EXTENSION;?>/img/delete.png" border="0" /></a>
            </td>
			<?php } else echo "---"; ?>
		</tr>
		<?php } ?>
	</table>
</form>
