<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/


class PositionsLib{
	
	var $_db = null;
	
	function PositionsLib($params=null)
	{
		$this->_db = FactoryLayer::getDB();
	}

	function &getInstance()
	{
		static $instances;

		if (!isset( $instances["positionsLib"] ))
			$instances["positionsLib"] = new PositionsLib();

		return $instances["positionsLib"];
	}
	
	
	
	/**
	 * Load defined Positions
	 *
	 */
	function _loadPositions($tpl=null){
		
		$tpl_filter = "";
		if($tpl){
			$tpl_filter = "WHERE tpl='$tpl'";
		}
		$this->_db->setQuery("SELECT * FROM #__".APP_CFIELDS_PREFIX."_positions $tpl_filter");
		return $this->_db->loadAssocList();
	}
	
	
	/**
	 * Load position
	 *
	 * @param position $id
	 * @return position object
	 */
	function _loadPosition($id){
		$db = &FactoryLayer::getDB();
		$db->setQuery("SELECT * FROM #__".APP_CFIELDS_PREFIX."_positions WHERE id = $id");
		return $db->loadObject();
	}
	
	
	/**
	 * Backend
	 * One Page Positions Assign Purpose
	 * 
	 * @return all custom fields and their position id, title, tpl  concatenated
	 **/
	function _renderCustomFields(){
		$db = &JFactory::getDBO();
		$db->setQuery(
		"SELECT GROUP_CONCAT(pf.position) as pids, 
		a.id, a.name,a.db_name,
		GROUP_CONCAT(p.title) as ptitles,GROUP_CONCAT(p.tpl)  as ptpls
		FROM #__".APP_CFIELDS_PREFIX."_fields as a 
		LEFT JOIN #__".APP_CFIELDS_PREFIX."_fields_positions as pf ON fid = a.id 
		LEFT JOIN #__".APP_CFIELDS_PREFIX."_positions p ON p.id = position 
		GROUP BY a.id
		");
		
		return $db->loadAssocList();
	}
	
	/**
	 * Returns all custom fields and their position assigned
	 * 
	 * @param select all true otherwise only unasigned
	 **/
	function _getCustomFields($all=true){
		$db = &JFactory::getDBO();
		if($all==false)
			$db->setQuery("
				SELECT 
					f . * 
				FROM `#__".APP_CFIELDS_PREFIX."_fields` AS f 
				LEFT JOIN `#__".APP_CFIELDS_PREFIX."_fields_positions` AS fp ON f.id = fp.fid 
				GROUP BY f.id
				HAVING count( fp.position ) < 
					(SELECT count( a.id ) FROM #__".APP_CFIELDS_PREFIX."_positions a ) 
 			");
		else
			$db->setQuery("SELECT pf.id, a.name,a.db_name,p.title,p.tpl FROM #__".APP_CFIELDS_PREFIX."_fields a LEFT JOIN #__".APP_CFIELDS_PREFIX."_fields_positions pf ON fid = a.id LEFT JOIN #__".APP_CFIELDS_PREFIX."_positions p ON p.id = position ");
		
		//Ads_Debug::getSQL("Unasigned Custom Field",false);
		return $db->loadAssocList();
	}

	
	/**
	 * Get's unasigned positions of a field
	 *
	 * @param  field id
	 * @return array
	 */
	function getFieldPositions($fid){
		$db = &JFactory::getDBO();
		$db->setQuery(
		"
		SELECT a. * , p.fid 
		FROM `#__".APP_CFIELDS_PREFIX."_positions` a
		LEFT JOIN `#__".APP_CFIELDS_PREFIX."_fields_positions` p ON p.position = a.id 
		AND p.fid = '{$fid}'
		WHERE p.fid IS NULL
		");
		//Ads_Debug::getSQL("Unasigned Positions for $fid ",false);
		return $db->loadAssocList();
	}
	
	
	/**
	 *
	 * Front site Position renderer
	 * 
	 * @param: object
	 * @return: array of subarrays positions with the add->custom fields html's
	 * @deprecated 
	 * 
	 **/
	function loadPositionedFields(&$add, $tpl=null, $owner_table=null, $action="list"){
		
		return $this->renderFields($tpl, $add, $owner_table, $action );
		
	}
	
	
	function renderFields( $template , $item = null, $table = null, $action = "list"){
		
		if(!$template)
			$template = APP_POSITION_DEFAULT_TEMPLATE;
		$positionsOutput = array();
		// loads positions list
		// renders each position
		$positions = $this->loadTemplatePositions( $template );
		if(count($positions)>0){
			foreach ($positions as $positionName => $positionObj ){
				$this->renderPosition($positionObj, $positionsOutput, $template, $item, $table, $action );
			}
		}
		
		return $positionsOutput;
	}
	
	function loadTemplatePositions( $template ){
		
		$db = & JFactory::getDBO();
		
		$db->setQuery("SELECT * FROM #__".APP_CFIELDS_PREFIX."_positions WHERE tpl = '{$template}' ");
		return $db->loadAssocList("name");
		
	}
	
	
	/**
	 * Fills a position
	 *
	 * @param unknown_type $template
	 * @param unknown_type $item
	 * @param unknown_type $table
	 */
	function renderPosition( $positionObj, &$positionsOutput, $template ,  $item = null, $table = null, $action = "list" ){
		
		$allowed_fields_sql = "";
		if(isset($item->category)){
			$this->_db->setQuery("SELECT fid FROM #__".APP_CFIELDS_PREFIX."_fields_categories WHERE cid = '{$item->category}'");
			$allowed_fields = $this->_db->loadResultArray();
			if($allowed_fields){
				$allowed_fields_sql = " AND ( fld.id IN (".implode(",",$allowed_fields).") OR fld.own_table <> '#__ads' ) ";
			}
		}
		
		$position = $positionObj["id"];
		if ($allowed_fields_sql) {
			$db = & JFactory::getDBO();
			
			$statement = "SELECT fld.id, name as ftitle, db_name,ftype,attributes FROM #__".APP_CFIELDS_PREFIX."_fields AS fld LEFT JOIN #__".APP_CFIELDS_PREFIX."_fields_positions as p ON p.fid = fld.id WHERE p.position = '{$position}' {$allowed_fields_sql} ORDER BY ordering ";
			$db->setQuery($statement);
			
			$ftypes = $db->loadAssocList("db_name");
			// add Fields to positionObj
			foreach ($ftypes as $fieldKey => $fieldObj){
				
				// Field Database Name
				$fname = $fieldKey;
				$fieldTypePluginName = "FieldType_".$fieldObj["ftype"];
				
				if(isset($item->$fname) && $item->$fname!=""){
					
					$fieldTypePlugin = new $fieldTypePluginName;
					$fieldTypePlugin->_attributes = $fieldObj["attributes"];
					$fieldTypePlugin->_action = $action;
					$item_value = stripslashes($fieldTypePlugin->getTemplateHTML($fname,$item->$fname));

					$fieldObj["html"] = str_replace("%LABEL%",$fieldObj["ftitle"],$positionObj["html"]);
					$fieldObj["html"] = str_replace("%VALUE%",($item_value )? $item_value :"--",$fieldObj["html"]);
					
				}else{
					$fieldObj["html"] = "";
				}
				
				$positionsOutput[$positionObj["name"]]["title"] = $positionObj["title"];
				$positionsOutput[$positionObj["name"]]["description"] = $positionObj["description"];
				
				$positionsOutput[ $positionObj["name"] ]["fields"][]= $fieldObj;
			}
			
		}
			
	}

	/**
	 *
	 * Front site Position renderer
	 * 
	 * @param: object
	 * @return: array of subarrays positions with the add->custom fields html's
	 * 
	 **/
	function loadPositionedFieldsPreview(&$add, $tpl=null, $owner_table=null, $action="list"){
		
		$db = &JFactory::getDBO();
		
		if(!$tpl)
			$tpl=APP_POSITION_DEFAULT_TEMPLATE;

		static $static_positions;
		
		if ( !isset($add) && !isset($tpl) ){
			if( isset($static_positions["all"]) ){
	
				$positions = $static_positions["all"];
			
			}else{
				
				$db->setQuery("SELECT * FROM #__".APP_CFIELDS_PREFIX."_positions");
				$static_positions["all"] = $positions = $db->loadAssocList("name");
				
			}
		}
		
		
		$add_id = 0;
		if( isset($add->id) ){
			$add_id = $add->id;
		}
		
		$section_sql = "";
		
		if($owner_table!=null){
			
			$section = FactoryFieldsHelper::getPage($owner_table);
			$section_sql = " AND a.page = '$section' ";
		}
		
		
		$db->setQuery("SELECT a.id as fido, db_name, a.name as ftitle , p.* 
		FROM #__".APP_CFIELDS_PREFIX."_fields AS a 
		LEFT JOIN #__".APP_CFIELDS_PREFIX."_fields_positions ON fid = a.id LEFT JOIN #__".APP_CFIELDS_PREFIX."_positions AS p ON position = p.id 
		WHERE p.tpl='{$tpl}' $section_sql  ORDER BY a.ordering ");
		$cei = $db->loadAssocList();
		
		if(!count($cei))	{
			$cei = array();
			$cei[0]->id = 1;
			$cei[0]->db_name = "test1";
			$cei[0]->ftype = "inputbox";
			$cei[0]->ftitle = "Test Field";
			$cei[0]->tpl = $tpl;
			$cei[0]->name = "defdetailspageheader";
				
		}
	
		$positions  = null;

		$ftypes = array();
		$statement = "SELECT id, db_name,ftype,attributes FROM #__".APP_CFIELDS_PREFIX."_fields";
		$db->setQuery($statement);
		$ftypes = $db->loadObjectList("db_name");
		
		if($cei)
		foreach ($cei as $c => $ci) {
			$fido=(int)$ci["fido"];
			$fname = $ci["db_name"];
			
			$fieldTypePluginName = "FieldType_".$ftypes[$fname]->ftype;
			if(isset($add->$fname) && $add->$fname!=""){
				
				$fieldTypePlugin = new $fieldTypePluginName;
				$fieldTypePlugin->_attributes = $ftypes[$fname]->attributes;
				$fieldTypePlugin->_action = $action;
				$item_value = $fieldTypePlugin->getTemplateHTML($fname,$add->$fname);
				$ci["html"] = str_replace("%LABEL%",$ci["ftitle"],$ci["html"]);
				$ci["html"] = str_replace("%VALUE%",($item_value )? $item_value :"--",$ci["html"]);
			}else {
				$ci["html"]	= "";
			}
			
			$positions[ $ci["name"] ]["title"]=$ci["title"];
			$positions[ $ci["name"] ]["description"]=$ci["description"];
			$positions[ $ci["name"] ]["verified"]=$ci["verified"];
			$positions[ $ci["name"] ]["fields"][]=$ci;
		}
		return $positions;
	}
	
		
	
}

?>