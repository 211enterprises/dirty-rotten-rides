<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

class JFactoryPositions{
	
	
	function checkTask( &$act )
	{
		if(!$act)
			$act="list";
		
		$method_name = "fposition_".$act;
		if (method_exists($this,$method_name)){
			return $this->$method_name();
		}
			
		return false;
	}
	
	
	function fposition_list()
	{
		
		$PO = &PositionsLib::getInstance();
		$positions = $PO->_loadPositions();
		require_once(dirname(__FILE__).DS."..".DS."tmpl".DS."positions.php");
		return true;
		
	}

	function fposition_new(){
		
		return $this->fposition_edit(1);
		
	}
	
	function fposition_edit($isNew=0)
	{
		if(!$isNew){
			$id = FactoryLayer::getRequest($_REQUEST,"id",null);
			$PO = &PositionsLib::getInstance();
			$position = $PO->_loadPosition($id);
		}
		require_once(dirname(__FILE__).DS."..".DS."tmpl".DS."position_edit.php");
		return true;
	}
	

	function fposition_save(){
		$db = FactoryLayer::getDB();
		$id = FactoryLayer::getRequest($_REQUEST,"id",null);
		JTable::addIncludePath(JPATH_SITE.DS.'administrator'.DS.'components'.DS.'com_adsman'.DS.'tables');
		$jtable =& JTable::getInstance('Positions','Table');
				
		$jtable->id =$id;
		$jtable->load();
		$jtable->html = JRequest::getVar('html','','POST','string',4);
				
		$jtable->store();
		FactoryLayer::redirect("index.php?option=".APP_EXTENSION."&task=fposition_panel&act=edit&id=".$jtable->id,"Position Saved");
	}
	
	
	function fposition_add(){
		$db = FactoryLayer::getDB();
		JTable::addIncludePath(JPATH_SITE.DS.'administrator'.DS.'components'.DS.'com_adsman'.DS.'tables');
		$jtable =& JTable::getInstance('Positions','Table');
				
		$jtable->id =null;
		$jtable->title =null;
		$jtable->name =null;
		$jtable->name = str_replace(array(" ","-","/",":","."), "_", $jtable->name);
		$jtable->description =null;
		$jtable->html = null;
		$jtable->tpl  = "";
		
		$jtable->bind(JRequest::get('post'));
        
		if($jtable->title!="" && $jtable->name!="")
			$jtable->store();
		else
			FactoryLayer::redirect("index.php?option=".APP_EXTENSION."&task=fposition_panel&act=new",JText::_("ADS_NAME_FIELDS_COMPULSORY"));
				
		FactoryLayer::redirect("index.php?option=".APP_EXTENSION."&task=fposition_panel&act=edit&id=".$jtable->id,JText::_("ADS_FIELDS_POSITION_SAVED"));
	}
	
	
	function fposition_remove(){
		
		$db = FactoryLayer::getDB();
		$id = FactoryLayer::getRequest($_REQUEST,"id",null);
		JTable::addIncludePath(JPATH_SITE.DS.'administrator'.DS.'components'.DS.'com_adsman'.DS.'tables');
		$jtable =& JTable::getInstance('Positions','Table');
				
		$jtable->name =null;
		$jtable->load($id);
		$position = "<strong>{include file='elements/display_fields.tpl' position='$jtable->name'}</strong>";
		$jtable->delete($id);
		
		FactoryLayer::redirect("index.php?option=".APP_EXTENSION."&task=fposition_panel",JText::_("ADS_FIELDS_POSITION_DELETED"));
	}
	
	
	function fposition_help(){
		echo JTHEF_POSITIONS_TEMPLATE_SYNTAX;
		echo "<hr />";
		echo JTHEF_NEW_POSITION_HELP;
		echo "<hr />";
		exit;
	}

	
	
}
