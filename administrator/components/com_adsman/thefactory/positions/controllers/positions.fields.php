<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

class JFactoryPosisionFields{

	function checkTask( &$act )
	{
		if(!$act)
			$act="list";
		
		$method_name = "fposition_".$act;
		if ( method_exists($this,$method_name) ){
			require_once(dirname(__FILE__).DS."..".DS."tmpl".DS."position.toolbar.php");
			return $this->$method_name();
		}

		return false;
	}

	function fposition_list()
	{

		$db = & JFactory::getDBO();
		$PO = &PositionsLib::getInstance();
		$list_type = & JRequest::getCmd("list_type","fields_positions");
		switch ($list_type){
			case "fields_positions":
				$positions = $PO->_loadPositions();
				$positionsOpts = array();
				foreach($positions as $o => $pos ){	
					$positionsOpts[$pos["id"]] = $pos["title"]."  (".$pos["tpl"].")";
				}
				$customFields = $PO->_renderCustomFields();
				require_once(dirname(__FILE__).DS."..".DS."tmpl".DS."fields_positions.php");
				break;
			default:
			case "drag":
				$positions = $PO->_loadPositions();
				for ($oo=0; $oo<count($positions); $oo++){
					$pid = $positions[$oo]["id"];
					
					$db->setQuery("SELECT f.name,f.id,f.db_name 
					FROM `#__".APP_CFIELDS_PREFIX."_fields` AS f 
					LEFT JOIN `#__".APP_CFIELDS_PREFIX."_fields_positions` AS fp ON f.id = fp.fid 
					WHERE fp.position = $pid
					ORDER BY f.name 
					");
					$positions[$oo]["fields"]= $db->loadObjectList();
				}
				$customFields = $PO->_renderCustomFields();
				require_once(dirname(__FILE__).DS."..".DS."tmpl".DS."fields_drag.php");
				break;
			default:
			case "fields":
				$customFields = $PO->_getCustomFields();
				require_once(dirname(__FILE__).DS."..".DS."tmpl".DS."fields.php");
				break;
		}
		
		return true;

	}
	
	function fposition_new(){
		return $this->fposition_edit();
	}
	
	function fposition_edit(){
		
		$db = FactoryLayer::getDB();
		$id = & JRequest::getVar("id",null);
		
		$PO = &PositionsLib::getInstance();
		
		$jtable = null;
			
		if($id){
			
			$positions = $PO->_loadPositions();
			$positionsOpts = array();
			foreach($positions as $o => $pos ){	$positionsOpts[$pos["id"]] = $pos["title"]."  (".$pos["tpl"].")";}
			
			JTable::addIncludePath(JPATH_SITE.DS.'administrator'.DS.'components'.DS.'com_adsman'.DS.'tables');
			$jtable =& JTable::getInstance('FieldsPositions','Table');
			
			$jtable->id =$id;
			$jtable->fid = null;
			$jtable->position = null;
			$jtable->load();
			$db->setQuery("SELECT a.id, name,db_name FROM `#__".APP_CFIELDS_PREFIX."_fields` a LEFT JOIN #__".APP_CFIELDS_PREFIX."_fields_positions p ON p.fid =  a.id WHERE p.id= $id ");
			$customFields = $db->loadAssocList();
			$customFieldsHTML = " <input type='hidden' name='fid' value='".$customFields[0]["id"]."' />".$customFields[0]["name"]."(".$customFields[0]["db_name"].")";
			
		}else{
			
			$jtable->position = null;
			
			$customFields = $PO->_getCustomFields(false);
	    	$customFieldsOpts = array();
	    	
	    	for($i = 0 ; $i<count($customFields); $i++ ){
				$customFieldsOpts[$customFields[$i]["id"]] = $customFields[$i]["name"]." ( ".$customFields[$i]["db_name"].")";
	    	}
	    	
			$positions = $PO->getFieldPositions($customFields[0]["id"]);
			$positionsOpts = array();
			foreach($positions as $o => $pos ){	$positionsOpts[$pos["id"]] = $pos["title"]."  (".$pos["tpl"].")";}
			
			$js= "THEFAjaxCall('am_putat','index.php?option=com_adsman&task=ajaxPositionSelect&fid='+this.value);";

			$customFieldsHTML = FactoryHTML::selectGenericList($customFieldsOpts,"fid", $id,"onchange=\"$js\"");
		}
		
		
		
		require_once(dirname(__FILE__).DS."..".DS."tmpl".DS."fields_edit.php");
		return true;
	}

	function fposition_save(){
		
		$db = FactoryLayer::getDB();
		$list_type = & JRequest::getCmd("list_type","fields_positions");
		switch ($list_type){
			default:
			case "fields":{
				$db = FactoryLayer::getDB();
				JTable::addIncludePath(JPATH_SITE.DS.'administrator'.DS.'components'.DS.'com_adsman'.DS.'tables');
				$jtable =& JTable::getInstance('FieldsPositions','Table');
				
				$jtable->id = null;
				$jtable->fid = null;
				$jtable->position = null;
				$jtable->bind($_POST);
				if(!$jtable->id)
					$jtable->set('_tbl_key', "id");
				$jtable->store();
				break;
			}
			case "fields_positions":{
				
				$req = JRequest::get();
				
				foreach ($req as $k => $r){
					
					
					if( substr($k, 0,7) == "fassign" ){
						$id = str_replace("fassign","",$k);
						if($r=="0"){
							$db->setQuery("DELETE FROM #__".APP_CFIELDS_PREFIX."_fields_positions WHERE fid = $id");
							$db->query();
						}
					}
					
					if( substr($k, 0,5) == "field" ){
						
						$id = str_replace("field","",$k);
						$db->setQuery("DELETE FROM #__".APP_CFIELDS_PREFIX."_fields_positions WHERE fid = $id");
						$db->query();
						
						$r_list = $r;
						if( $r_list ){
							
							$q = "INSERT INTO #__".APP_CFIELDS_PREFIX."_fields_positions (position,fid) VALUES ";
							$vals = array();
							foreach ($r_list as $ik => $ii){
								$vals[] = "('{$ii}', '$id')";
							}
							$q .= implode(",",$vals);
							$db->setQuery($q);
							$db->query();
						}
					}
				}
				break;
			}
			case "drag":{
				$req = JRequest::get();
				foreach ($req as $k => $r){
					if(substr($k, 0,9)=="position_"){
						
						$id = str_replace("position_","",$k);
						$r_list = str_replace("##","#",$r);
						$r_list = trim($r_list,"#");
						$db->setQuery("DELETE FROM #__".APP_CFIELDS_PREFIX."_fields_positions WHERE position = $id");
						$db->query();
						if( $r_list ){
							$r_list = explode("#",$r_list);
							$q = "INSERT INTO #__".APP_CFIELDS_PREFIX."_fields_positions (position,fid) VALUES ";
							$vals = array();
							foreach ($r_list as $ik => $ii){
								$vals[] = "('{$id}', '$ii')";
							}
							$q .= implode(",",$vals);
							$db->setQuery($q);
							$db->query();
						}
					}
				}
				break;
			}
		}
		
		FactoryLayer::redirect("index.php?option=".APP_EXTENSION."&task=fposition_fields&list_type=$list_type","Saved!");
	}

	function fposition_del(){
		$id = & JRequest::getVar("id");
		$db = FactoryLayer::getDB();
		if($id){
			$db->setQuery("DELETE FROM #__".APP_CFIELDS_PREFIX."_fields_positions WHERE id = $id");
			$db->query();
			
		}
		FactoryLayer::redirect("index.php?option=".APP_EXTENSION."&task=fposition_fields");
	}
	
}