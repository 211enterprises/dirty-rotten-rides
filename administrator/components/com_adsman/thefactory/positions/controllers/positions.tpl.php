<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/


class JFactoryPositionTpl{
	
	function checkTask( &$act )
	{
		if(!$act)
			$act="list";
		
		$method_name = "fposition_".$act;
		if (method_exists($this,$method_name)){
			return $this->$method_name();
		}
			
		return false;
	}
	

	function fposition_list(){
		$templates =  explode(",",APP_POSITION_TEMPLATES);
		require_once(dirname(__FILE__).DS."..".DS."tmpl".DS."templates.php");
		return true;
	}


	function fposition_edit(){
		
		$template  = &FactoryLayer::getRequest($_GET,"tpl","");
		if(!$template){
			JFactoryPositions::choose_template();
		}
		global $mainframe;

		// Initialize some variables
		$option		= JRequest::getCmd('option');
		$client		=& JApplicationHelper::getClientInfo(JRequest::getVar('client', '0', '', 'int'));
		
		// ads is themed;
		// no for ads:
		// $tpl_path = F_ROOT_DIR."/components/".APP_EXTENSION."/templates/".$tpl;
		
		$file		= $client->path.DS.'components'.DS.$option.DS.'templates'.DS.ADDSMAN_TPL_THEME.DS.$template;
		if(!file_exists($file))
			$file		= $client->path.DS.'components'.DS.$option.DS.'templates'.DS.'default'.DS.$template;
			

		// Read the source file
		jimport('joomla.filesystem.file');
		$content = JFile::read($file);
		
		$PO = &PositionsLib::getInstance();
		$positions = $PO->_loadPositions($template);

		require_once(dirname(__FILE__).DS."..".DS."tmpl".DS."template.php");
		return true;
	}
	
	
	function fposition_save(){
		
		$template  = &FactoryLayer::getRequest($_POST,"tpl","");
		if(!$template){
			JFactoryPositions::choose_template();
		}
		
		global $mainframe;

		// Initialize some variables
		$option			= JRequest::getCmd('option');
		$client			=& JApplicationHelper::getClientInfo(JRequest::getVar('client', '0', '', 'int'));
		$filecontent	= JRequest::getVar('filecontent', '', 'post', 'string', JREQUEST_ALLOWRAW);

		if (!$template) {
			$mainframe->redirect('index.php?option='.$option.'&client='.$client->id, JText::_('ADS_OPERATION_FAILED').': '.JText::_('ADS_NO_TEMPLATE_SPECIFIED'));
		}

		if (!$filecontent) {
			$mainframe->redirect('index.php?option='.$option.'&client='.$client->id, JText::_('ADS_OPERATION_FAILED').': '.JText::_('ADS_CONTENT_EMPTY'));
		}

		// Set FTP credentials, if given
		jimport('joomla.client.helper');
		JClientHelper::setCredentialsFromRequest('ftp');
		$ftp = JClientHelper::getCredentials('ftp');

		$file		= $client->path.DS.'components'.DS.$option.DS.'templates'.DS.ADS_TPL_THEME.DS.$template;
		if(!file_exists($file))
			$file		= $client->path.DS.'components'.DS.$option.DS.'templates'.DS.'default'.DS.$template;

		
		// Try to make the template file writeable
		if (!$ftp['enabled'] && !JPath::setPermissions($file, '0755')) {
			JError::raiseNotice('SOME_ERROR_CODE', JText::_('ADS_TEMPLATE_WRITABLE_ERR'));
		}

		jimport('joomla.filesystem.file');
		$return = JFile::write($file, $filecontent);

		if ($return)
		{
			$act = JRequest::getCmd('act');
			switch($task)
			{
				case 'apply':
					FactoryLayer::redirect("index.php?option=".APP_EXTENSION."&task=fposition_templates&act=edit&tpl=".$template,JText::_("ADS_TEMPLATE_SAVED"));
					break;

				case 'save':
				default:
					FactoryLayer::redirect("index.php?option=".APP_EXTENSION."&task=fposition_templates&act=edit&tpl=".$template,JText::_("ADS_TEMPLATE_SAVED"));
					break;
			}
		}
		else {
			FactoryLayer::redirect("index.php?option=".APP_EXTENSION."&task=fposition_templates&act=edit&tpl=".$template,JText::sprintf('Failed to open file for writing.', $file));
		}
		
	}


	
/**
 * Previews dinamicaly the Details Template
 * Actually loads the smarty template with smarty param AdsShowPostions=1
 * 	in able to highlight positions 
 * 
 **/	
	function fposition_preview(){
		
		$template  = &FactoryLayer::getRequest($_GET,"tpl","t_details_add.tpl");
		$smarty = AdsUtilities::SmartyLoaderHelper();
		error_reporting(0);
		if($template=="t_details_add.tpl"){
			$add = new stdclass();
			
			$add->title= "Demo Classified ";
			$add->id=a;
			$add->links = AdsUtilities::makeLinks($add);
			
			
			foreach($add->links as $l => $i){
				$add->links[$l]="#";
			}
			
			echo $css = '<style type"text/css">img{border:none !important;} a{color:#AAAAAA !important;}</style>';
			
			$smarty->assign("add", $add);
			$smarty->assign("TPL_DEBUG", 1);
			// Custom Fields Auto Positioning
			$PO = &PositionsLib::getInstance();
			$positions = $PO->loadPositionedFieldsPreview($add);
		}
		
		$smarty->assign("positions",  $positions);
		
		$obj = $smarty->fetch($template);
    	echo $obj;exit;
	}
	

	function fposition_verify(){
		
		$template  = &FactoryLayer::getRequest($_GET,"tpl","");
		$smarty = AdsUtilities::SmartyLoaderHelper();
		$PO = &PositionsLib::getInstance();
		
		// future: more templates! more troubles!
		$templateList = array("t_details_add.tpl","t_listadds.tpl");
		
		$positions = array();
		
		foreach ($templateList as $template){

			$templatePositions=$PO->_loadPositions($template);
			$positions = array_merge($positions,$templatePositions);
			$add->title= "Demo Classified ";
			$add->id="0";
			$p = $PO->loadPositionedFields($add,$template);
			$smarty->assign("positions",  $positions);
			
			$smarty->assign("AdsShowPostions", 1);
			$obj = $smarty->fetch($template);
			
		}
		//Ads_Debug::var_dump($positions);
		
		?>
		<h2>Verify Positions</h2>
		<table width="100%" cellpadding="0" cellspacing="0">
		<?php
		$ki=0;
		if($positions)
			foreach ($positions as $k => $p){
				$ptpl = $p["tpl"];
				$pname = $p["title"]."(".$p["name"].")";
				$puname = $p["name"];
				$help = "";
				
				if ( $p["verified"] )
					$verified = "verified";
				else{
					$verified = "not verified";
					$help = "<div>The position was not found in the {$p['tpl']} template. <br />Search for :{include file='elements/display_fields.tpl' position='$puname'}.</div>";
				}

				if($ki==0)
					$background = "#EEEEEE";
				else	
					$background = "#DDDDDD";
				$ki= 1 - $ki;
				?>
				<tr style=" background:<?php echo $background;?>;">
					<td><?php echo $ptpl;?></td>	
					<td><?php echo $pname;?></td>
					<td width="100" align="center" style='color:#FF0000;' ><?php echo $verified;?></td>
				</tr>
				<?php if($help){ ?>
				<tr>
					<td style="background:<?php echo $background;?>; padding:10px; font-size:13px; border:dashed 1px #000; border-top:none;" colspan="3">
					<?php echo $help;?>
					</td>
				</tr>	
				<?php }
			}
		?>
		</table>			
		<?php
			
		exit;
		
	}

	
}
