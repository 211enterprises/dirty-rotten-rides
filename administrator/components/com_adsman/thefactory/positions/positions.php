<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/



class JTheFactoryPositions
{
    
    function __construct($params=null)
    {
    }

	function &getInstance($params=null)
	{
		static $instances;
		JLoader::register('PositionsLib', dirname(__FILE__).DS.'positions.lib.php');

		if (!isset( $instances ))
			$instances = new JTheFactoryPositions($params);

		return $instances;
	}
	
	
	function route(&$task, &$act){
		
		if (method_exists($this,$task)){
			return $this->$task($act);
		}
        
	}
	
    function fposition_panel(&$act)
    {
    	
		JLoader::register('JFactoryPositions', dirname(__FILE__).DS.'controllers'.DS.'positions.positions.php');
		$JFactoryPositions = new JFactoryPositions();
    	return $JFactoryPositions->checkTask($act);

    }
	
    function fposition_fields(&$act)
    {
		JLoader::register('JFactoryPosisionFields', dirname(__FILE__).DS.'controllers'.DS.'positions.fields.php');
		$JFactoryPosisionFields = new JFactoryPosisionFields();
		return $JFactoryPosisionFields->checkTask($act);
    }

    
	function fposition_templates(&$act){
		
		JLoader::register('JFactoryPositionTpl', dirname(__FILE__).DS.'controllers'.DS.'positions.tpl.php');
		$JFactoryPositionTpl = new JFactoryPositionTpl();
		return $JFactoryPositionTpl->checkTask($act);
	}
    

}
