<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/
?>

<fieldset class="adminform">
	<legend><?php echo JText::_( 'ADS_PAYMENT_GATEWAY_UPLOAD' ); ?></legend>
	<form action="index.php?option=<?php echo F_COMPONENT_NAME;?>" method="post" name="adminForm" enctype="multipart/form-data" >
		<table class="paramlist admintable">
			<tr>
				<td class="paramlist_key" width="150">
					<?php echo JText::_('ADS_PAYMENT_GATEWAY_ZIP');?>
				</td>
				<td>
					<input type="file" name="gateway_file" />
				</td>
				<td>
					<strong style="color:#FF0000;">
					<?php echo JText::_("ADS_PAYMENT_GATEWAY_INFO");?>
					</strong>
				</td>
			</tr>
		</table>
		<input type="hidden" name="act" value="do_upload_gateway" />
		<input type="hidden" name="option" value="<?php echo F_COMPONENT_NAME; ?>" />
		<input type="hidden" name="task" value="gateway_administrator" />
	</form>
</fieldset>