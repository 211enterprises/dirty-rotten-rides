<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/
?>

<div id="cpanel">
		<div style="float:left;">
			<div class="icon">
                <a href="<?php echo JURI::root();?>administrator/index.php?option=<?php echo APP_EXTENSION;?>&task=themes_administrator">
					<?php
						echo JHTML::_('image.administrator', 'themes.png', '../administrator/components/'.APP_EXTENSION.'/thefactory/plugins/assets/', NULL, NULL, "Themes Manager" );
					?>
					<span><?php echo JText::_('ADS_THEMES_MANAGER'); ?></span>
				</a>
			</div>
		</div>
		<div style="float:left;">
			<div class="icon">
				<a href="<?php echo JURI::root();?>administrator/index.php?option=<?php echo APP_EXTENSION;?>&task=gallery_administrator">
					<?php
						echo JHTML::_('image.administrator', 'gallery.png', '../administrator/components/'.APP_EXTENSION.'/thefactory/plugins/assets/', NULL, NULL, "Gallery Manager" );
					?>
					<span><?php echo JText::_('ADS_GALLERY_MANAGER'); ?></span>
				</a>
			</div>
		</div>
		<div style="float:left;">
			<div class="icon">
				<a href="<?php echo JURI::root();?>administrator/index.php?option=<?php echo APP_EXTENSION;?>&task=gateway_administrator">
					<?php
						echo JHTML::_('image.administrator', 'gateway.png', '../administrator/components/'.APP_EXTENSION.'/thefactory/plugins/assets/', NULL, NULL, "Payment Gateway Manager" );
					?>
					<span><?php echo JText::_('ADS_PAYMENT_GATEWAY_MANAGER'); ?></span>
				</a>
			</div>
		</div>
	
</div>