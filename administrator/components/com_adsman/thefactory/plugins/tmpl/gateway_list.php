<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

JHTML::_("behavior.modal");
JHtml::_("behavior.mootools");
?>
<fieldset class="adminform">
	<legend><?php echo JText::_( 'ADS_PAYMENT_GATEWAYS_PLG' ); ?></legend>
<form action="index.php?option=<?php echo F_COMPONENT_NAME;?>" method="post" name="adminForm" id="adminForm">

<table class="adminlist">
<thead>
	<tr>
		<!--<th align="center" width="5">#</th>-->
		<th colspan="2" class="left"><?php echo JText::_( 'ADS_GATEWAY');?></th>
		<th class="left"><?php echo JText::_( 'ADSMAN_DESCRIPTION');?></th>
		<th width="50"><?php echo JText::_( 'ADS_ENABLED');?></th>
		<th width="50"><?php echo JText::_( 'ADS_DEFAULT');?></th>
		<th width="70"><?php echo JText::_( 'ADS_LOGO');?></th>
		<th><?php echo JText::_( 'ADS_DELETE');?></th>
	</tr>
</thead>
<?php 
$i = 0;
foreach ($list["pay_list"] as  $gl){ $i++; 
?>
<tr>
	<!--<td align="center">
		<?php echo $i;?>
	</td>-->
	<td class="center" width="10">
		<input type="radio" id="cb<?php echo $i;?>" name="cid[]" value="<?php echo $gl->classname; ?>" onclick="Joomla.isChecked(this.checked);" />
	</td>
	<td>
		<?php $link=JURI::root()."administrator/index.php?option=".APP_EXTENSION."&task=showpaymentconfig&paymenttype=".$gl->classname;
			  echo "<a href='".$link."' title='".JText::_('ADS_PAYMENT_METHOD_CONFIG')."'>".$gl->classname."</a>";
		?>
	</td>
	<td>
		<?php echo $gl->classdescription;?>
	</td>
	<td class="center">
	<?php 
	    $img_enabled=($gl->enabled) ? "tick.png" : "publish_x.png";
	    echo "<a href=\"javascript: void(0);\" onClick=\"return listItemTask('cb".($i)."','togglepayment')\">
        			".JHTML::_('image','admin/'.$img_enabled, "Toggle", array('border' => 0), true)."
        		</a>";
		?>
	</td>
    <td class="center">
	<?php if ($gl->isdefault) { ?>
		<img src="<?php echo JURI::root();?>administrator/templates/bluestork/images/menu/icon-16-default.png" />

    <?php } ?>
	</td>
	<td width="50" align="center">
		<img src="<?php echo $gl->getLogo();?>" style="max-width:150px;" />
	</td>
	<td width="50" class="center">
		<?php if( !in_array($gl->classname, $lists["core_pay_gateways"]) ){ ?>
		<a href="javascript:if(confirm('Are you sure you want to remove <?php echo $gl->classname;?> gateway plugin? ')==1) document.location='index.php?option=<?php echo APP_EXTENSION;?>&task=gateway_administrator&act=remove_gateway&gateway=<?php echo $gl->classname;?>'"><?php echo JText::_( 'ADS_DELETE' ); ?></a>
		<?php } else { ?>
            <?php echo JText::_( 'ADS_CORE');?>
		<?php } ?>
	</td>
</tr>
<?php } ?> 
</table>
	<input type="hidden" name="act" value="setdefault" />
	<input type="hidden" name="option" value="<?php echo F_COMPONENT_NAME; ?>" />
	<input type="hidden" name="task" value="gateway_administrator" />
	<input type="hidden" name="boxchecked" value="0" />
</form>

</fieldset>