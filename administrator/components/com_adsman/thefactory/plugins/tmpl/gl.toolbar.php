<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

$act = FactoryLayer::getRequest($_REQUEST,"act","");
$bar = & JToolBar::getInstance('toolbar');

$bar->appendButton( 'Link', 'back', 'List Gallery', "index.php?option=".APP_EXTENSION."&task=gallery_administrator&act=list_gallery" );

if($act == "upload_gallery") {
	$bar->appendButton( 'Link', 'save', 'Save', "javascript:document.adminForm.submit();" );
}
if ($act=="" || $act=="list_gallery") {
	$bar->appendButton( 'Standard', 'default', 'Set As Default', 'gallery_administrator', true, false );
}

$bar->appendButton( 'Link', 'upload', 'Upload Gallery', "index.php?option=".APP_EXTENSION."&task=gallery_administrator&act=upload_gallery" );

JToolBarHelper::title(JText::_('ADS_GALLERY'), 'mediamanager.png');

?>
<!--<table border="0" class="adminlist" width="100%" >
	<thead>
	<tr>
		<th align="left">
		<h2 style="text-align:left;">
			<img src="<?php echo F_ROOT_URI;?>administrator/components/com_adsman/img/toolbar/gallery.png" alt="Gallery" border="0" style="vertical-align:middle;" />&nbsp;&nbsp;<?php //echo JText::_('ADS_GALLERY');?></h2>
		</th>
	</tr>
	</thead>
</table>-->