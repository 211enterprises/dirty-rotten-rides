<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

$act = FactoryLayer::getRequest($_REQUEST,"act","");
$bar = & JToolBar::getInstance('toolbar');

if($act == "upload_gateway") {
	$bar->appendButton( 'Link', 'save', 'Save', "javascript:document.adminForm.submit();" );
}
if ($act=="" || $act=="list_gateway") {
	JToolBarHelper::makeDefault("setdefaultpayment");
}

$bar->appendButton( 'Link', 'upload', 'Upload Payment Gateway', "index.php?option=".APP_EXTENSION."&task=gateway_administrator&act=upload_gateway" );
?>
<!--
<table border="0" class="adminlist" width="100%" >
	<thead>
	<tr>
		<th align="left">
		<h2 style="text-align:left;">
			<img src="<?php echo F_ROOT_URI;?>administrator/components/com_adsman/img/toolbar/gateway.png" alt="Payment Gateway" border="0" style="vertical-align:middle;" />&nbsp;&nbsp;<?php //echo JText::_('Payment Gateways');?></h2>
		</th>
	</tr>
	</thead>
</table>
-->