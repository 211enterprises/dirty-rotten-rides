<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

$act = FactoryLayer::getRequest($_REQUEST,"act","");
$bar = & JToolBar::getInstance('toolbar');

$bar->appendButton( 'Link', 'back', 'List Themes', "index.php?option=".APP_EXTENSION."&task=themes_administrator&act=list_themes" );

if ($act=="" || $act=="list_themes") {
	$bar->appendButton( 'Standard', 'default', 'Set As Default', 'themes_administrator', true, false );
}

if($act == "upload_theme") {
		$bar->appendButton( 'Link', 'save', 'Save', "javascript:document.adminForm.submit();" );
}

$bar->appendButton( 'Link', 'upload', 'Upload Theme', "index.php?option=".APP_EXTENSION."&task=themes_administrator&act=upload_theme" );

?>
		
<!--<table border="0" class="adminlist" width="100%" >
	<thead>
	<tr>
		<th align="left">
		<h2 style="text-align:left;">
			<img src="<?php echo F_ROOT_URI;?>administrator/components/com_adsman/img/toolbar/themes.png" alt="Themes" border="0" style="vertical-align:middle;" />&nbsp;&nbsp;<?php //echo JText::_('Themes');?></h2>
		</th>
	</tr>
	</thead>
</table>-->
