<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

JHTML::_("behavior.modal");
JHtml::_("behavior.mootools");
?>
<fieldset class="adminform">
	<legend><?php echo JText::_( 'ADS_GALLERY_PLUGINS' ); ?></legend>
<form action="index.php?option=<?php echo F_COMPONENT_NAME;?>" method="post" name="adminForm">

<table class="adminlist">
<thead>
	<tr>
		<th class="left" colspan="2"><?php echo JText::_( 'ADS_GALLERY');?></th>
		<th width="50"><?php echo JText::_( 'ADS_DEFAULT');?></th>
		<th width="50"><?php echo JText::_( 'ADS_PREVIEW');?></th>
		<th width="200"><?php echo JText::_( 'ADS_LAST_MODIFIED');?></th>
		<th><?php echo JText::_( 'ADS_DELETE');?></th>
	</tr>
</thead>
<?php 
$i = 0;
foreach ($list["gl_list"] as  $gl) { 
	$i++; 
?>
<tr>
	<td class="center" width="10">
		<input type="radio" id="cb<?php echo $i;?>" name="cid[]" value="<?php echo $gl["name"]; ?>" onclick="Joomla.isChecked(this.checked);" />
	</td>
	<td>
		<?php echo $gl["name"];?>
	</td>
	<td class="center">
	<?php if ($gl["default"]) {
		echo JHTML::_('image','admin/tick.png', "Default", array('border' => 0), true);
	}?>
	</td>
	<td width="50" class="center">
		<a href="index.php?option=<?php echo APP_EXTENSION;?>&task=gallery_administrator&act=preview_gl&gl=<?php echo $gl["name"];?>"><?php echo JText::_( 'ADS_PREVIEW' ); ?></a>
	</td>
	<td align="center">
		<?php echo $gl["time"];?>
	</td>
	<td width="50" align="center">
		<?php if( !in_array($gl["name"], $list["core_galleries"]) ){ ?>
		<a href="javascript:if(confirm('Are you sure you want to remove  <?php echo $gl['name'];?> gallery plugin? ')==1) document.location='index.php?option=<?php echo APP_EXTENSION;?>&task=gallery_administrator&act=remove_gallery&gl=<?php echo $gl["name"];?>'"><?php echo JText::_( 'ADS_DELETE' ); ?></a>
		<?php } else { ?>
            <?php echo JText::_( 'ADS_CORE');?>
		<?php } ?>
	</td>
</tr>
<?php } ?> 
</table>
	<input type="hidden" name="act" value="setdefault" />
	<input type="hidden" name="option" value="<?php echo F_COMPONENT_NAME; ?>" />
	<input type="hidden" name="task" value="gallery_administrator" />
	<input type="hidden" name="boxchecked" value="0" />
</form>

</fieldset>