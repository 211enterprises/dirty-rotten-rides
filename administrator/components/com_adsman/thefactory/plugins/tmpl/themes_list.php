<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

JHTML::_("behavior.modal");
JHtml::_("behavior.mootools");
?>
<fieldset class="adminform">
	<legend><?php echo JText::_( 'ADS_THEMES' ); ?></legend>
	<form action="index.php?option=<?php echo F_COMPONENT_NAME;?>" method="post" name="adminForm" id="adminForm">
<table class="adminlist">
<thead>
	<tr>
		<th class="left" colspan="2"><?php echo JText::_( 'ADS_TEMPLATE_NAME');?></th>
		<th width="50"><?php echo JText::_( 'ADS_DEFAULT');?></th>
		<th width="100"><?php echo JText::_( 'ADS_THUMBNAIL');?></th>
		<th class="left" width="200"><?php echo JText::_( 'ADS_LAST_MODIFIED');?></th>
		<th><?php echo JText::_( 'ADS_DELETE');?></th>
	</tr>
</thead>
<?php 
$i = 0;
foreach ($list["themes"] as  $theme){ $i++; ?>
<tr>
	<td align="center" width="10">
		<input type="radio" id="cb<?php echo $i;?>" name="cid[]" value="<?php echo $theme["name"]; ?>" onclick="Joomla.isChecked(this.checked);" />
	</td>
	<td>
		<?php echo $theme["name"];?>
	</td>
	<td class="center">
	<?php if ($theme["default"]) {
		echo JHTML::_('image','admin/tick.png', "Default", array('border' => 0), true);
	}
	?>
	</td>
	<td class="center">
		<?php
		$tpl = JPATH_ROOT.DS."components".DS.APP_EXTENSION.DS."templates".DS.$theme["name"].DS.$theme["name"].".jpg";
		if(file_exists($tpl)){ ?>
		<a class="modal" href="<?php echo JURI::root();?>/components/<?php echo APP_EXTENSION;?>/templates/<?php echo $theme["name"];?>/<?php echo $theme["name"];?>.jpg">
			<img src="<?php echo JURI::root();?>/components/<?php echo APP_EXTENSION;?>/templates/<?php echo $theme["name"];?>/<?php echo $theme["name"];?>.jpg" style="width:90px !important;" />
		</a>
		<?php } ?>
	</td>
	<td align="left">
		<?php echo $theme["time"];?>
	</td>
	<td width="50" class="center">
		<a href="javascript:if(confirm('Are you sure you want to remove  <?php echo $theme['name'];?> theme? ')==1) document.location='index.php?option=<?php echo APP_EXTENSION;?>&task=themes_administrator&act=remove_theme&theme=<?php echo $theme["name"];?>' "><?php echo JText::_( 'ADS_DELETE' ); ?></a>
	</td>
</tr>
<?php } ?> 
</table>
	<input type="hidden" name="act" value="setdefault" />
	<input type="hidden" name="option" value="<?php echo F_COMPONENT_NAME; ?>" />
	<input type="hidden" name="task" value="themes_administrator" />
	<input type="hidden" name="boxchecked" value="0" />
	</form>
</fieldset>