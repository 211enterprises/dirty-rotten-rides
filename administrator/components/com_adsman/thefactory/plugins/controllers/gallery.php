<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/


jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.archive');
jimport('joomla.filesystem.path');


class FactoryGalleryController{
	
	var $_db 	= null;
	
	
	function checkTask( $act )
	{
		$this->_db = &FactoryLayer::getDB();
		if(!$act)
			$act="list_gallery";
		$method_name = $act;
		
		if ( method_exists($this,$method_name) ){
			require_once(dirname(__FILE__).DS."..".DS."tmpl".DS."gl.toolbar.php");
			$this->$method_name();
			return true;
		}

		return false;
	}
	
	function list_gallery(){
		
		$component = APP_EXTENSION;
		$gl_path = JPATH_ROOT.DS."components".DS.$component.DS."gallery".DS;
		$gl_list = JFolder::files($gl_path,'^gl_([^\.]*)\.php');
		
		if( $gl_list){
			
			$list["gl_list"] = array();
			foreach ($gl_list as $file){
				
				$gallery_class_name = str_replace(array("gl_",".php"),"",$file);
				$list["gl_list"][$file]["default"] = 0;
				if( ads_opt_gallery==$gallery_class_name )
					$list["gl_list"][$file]["default"] = 1;
				$list["gl_list"][$file]["name"] = $file;
				$list["gl_list"][$file]["time"] =  gmdate ("F d Y H:i:s.", filemtime($gl_path.$file));
			}
		}
		$list["core_galleries"] = array("gl_lytebox.php");
		require_once(dirname(__FILE__).DS."..".DS."tmpl".DS."gl_list.php");
		
	}
	
	function preview_gl(){
		
		$component = APP_EXTENSION;
		$gl_path = JPATH_ROOT.DS."components".DS.$component.DS."gallery".DS;
		
		$gallery_name = &JRequest::getCmd("gl","");
		$database = & JFactory::getDBO();
		require_once($gl_path.$gallery_name);
		$gallery_class_name = str_replace(".php","",$gallery_name);
		$gallery = new $gallery_class_name($database,JURI::root()."administrator/components/".APP_EXTENSION."/thefactory/plugins/assets/photos/");
		$gallery->addImage("photo0.jpg");
		$gallery->addImage("photo1.gif");
		$gallery->addImage("photo2.gif");
		$gallery->writeJS();
		echo $gallery->getGallery();

		return true;
	}
	
	function upload_gallery(){
		
		require_once(dirname(__FILE__).DS."..".DS."tmpl".DS."gallery_upload.php");
		
	}
	
	function do_upload_gallery(){
		
		$msg = "";
		
		// Move uploaded file
		jimport('joomla.filesystem.file');

		
        if (!isset($_FILES["gallery_file"]['name'])){ return false; }
		
        $upl = $_FILES["gallery_file"]['tmp_name'];
        
        $gallery_file_upl = $_FILES["gallery_file"]["name"];
        $gallery_name = JFile::stripExt($_FILES["gallery_file"]["name"]);
        if(file_exists(JPATH_ROOT.DS."components".DS."com_adsman".DS."gallery".DS.$gallery_name.".php")){
				FactoryLayer::redirect("index.php?option=".F_COMPONENT_NAME."&task=gallery_administrator","Gallery named '{$gallery_name}' allready exists!");
        }
        
		$file = JPATH_ROOT.DS."tmp".DS.$gallery_file_upl;
        
        move_uploaded_file($upl,$file);
        
		$p_filename = $file;
		$archivename = $p_filename;

		$tmpdir = JPATH_ROOT.DS."components".DS."com_adsman".DS."gallery".DS;

		// Clean the paths to use for archive extraction
		$extractdir = JPath::clean($tmpdir);
		$archivename = JPath::clean($archivename);

		// do the unpacking of the archive
		$result = JArchive::extract( $archivename, $extractdir);

		if ( $result === false ) {
			return false;
		}

		FactoryLayer::redirect("index.php?option=".F_COMPONENT_NAME."&task=gallery_administrator",$msg);
	}
	
	function remove_gallery(){
		
		// Move uploaded file
		jimport('joomla.filesystem.file');
		$name = &JRequest::getCmd("gl","");
		if(!$name){
			FactoryLayer::redirect("index.php?option=".F_COMPONENT_NAME."&task=gallery_administrator","No Galery selected to remove!");
		}
		
		$gallery_path = JPATH_ROOT.DS."components".DS.APP_EXTENSION.DS."gallery".DS. str_replace(array("gl_",".php"),"",$name);
		
		$s = false;
		if(file_exists($gallery_path))
			$s = JFolder::delete(JFolder::makeSafe($gallery_path.DS));
		$s += JFile::delete(JPATH_ROOT.DS."components".DS.APP_EXTENSION.DS."gallery".DS.$name);
		$msg = "";
		if($s)
			$msg = "Gallery removed!";
			
		FactoryLayer::redirect("index.php?option=".F_COMPONENT_NAME."&task=gallery_administrator",$msg);
	}
	
	function setdefault(){
		$cid	= JRequest::getVar('cid', array(), 'method', 'array');
		if($cid[0]){
			$id = $cid[0];
		}
        $database = & JFactory::getDBO();
		$gallery_class_name = str_replace(array("gl_",".php"),"",$id);
		$opts = new Ads_Options($database);
		$opts->changeSetting("ads_opt_gallery",$gallery_class_name);
		FactoryLayer::redirect("index.php?option=".F_COMPONENT_NAME."&task=gallery_administrator"," $gallery_class_name setted default!");
		
	}
}