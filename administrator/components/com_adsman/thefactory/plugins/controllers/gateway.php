<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/


jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.archive');
jimport('joomla.filesystem.path');


class FactoryGatewayController{
	
	var $_db 	= null;
	
	
	function checkTask( $act )
	{
		$this->_db = &FactoryLayer::getDB();
		if(!$act)
			$act="list_gateway";
		$method_name = $act;
		
		if ( method_exists($this,$method_name) ){
			require_once(dirname(__FILE__).DS."..".DS."tmpl".DS."gateway.toolbar.php");
			$this->$method_name();
			return true;
		}

		return false;
	}
	
	function list_gateway(){
		
		$PO = &JTheFactoryPaymentLib::getInstance();
		$PO->loadAvailabeGateways();
		$list["pay_list"] = $PO->pay_plugins;
		$lists["core_pay_gateways"] = array("pay_2checkout","pay_paypal","pay_moneybookers");
		require_once(dirname(__FILE__).DS."..".DS."tmpl".DS."gateway_list.php");
		
	}
	
	function upload_gateway(){
		
		require_once(dirname(__FILE__).DS."..".DS."tmpl".DS."gateway_upload.php");
		
	}
	
	function do_upload_gateway(){
		
		$msg = "";
		
		// Move uploaded file
		jimport('joomla.filesystem.file');

		
		if (!isset($_FILES["gateway_file"]['name'])){ return false; }
		
		$upl = $_FILES["gateway_file"]['tmp_name'];
		$gateway_file_upl = $_FILES["gateway_file"]["name"];
		
		$gateway_name = JFile::stripExt($gateway_file_upl);
		
		if(file_exists(JPATH_ROOT.DS."components".DS.F_COMPONENT_NAME.DS."plugins".DS."payment".DS.$gateway_name.".php")){
			FactoryLayer::redirect("index.php?option=".F_COMPONENT_NAME."&task=gateway_administrator","Payment Gateway named '{$gateway_name}' allready exists!");
		}
        
		$file = JPATH_ROOT.DS."tmp".DS.$gateway_file_upl;
        
      move_uploaded_file($upl,$file);
        
		$p_filename = $file;
		$archivename = $p_filename;

		// Clean the paths to use for archive extraction
		$extractdir = JPath::clean(JPATH_ROOT.DS."components".DS.F_COMPONENT_NAME.DS."plugins".DS."payment".DS);
		$archivename = JPath::clean($archivename);

		// do the unpacking of the archive
		$result = JArchive::extract( $archivename, $extractdir);

		if ( $result === false ) {
			return false;
		}
		
		$db = & JFactory::getDBO();
		$paysystem = ucfirst($gateway_name);
		
		$db->setQuery("INSERT INTO `#__ads_paysystems` SET paysystem = '{$paysystem}', classname='{$gateway_name}', isdefault=0;");
		$db->query();

		FactoryLayer::redirect("index.php?option=".F_COMPONENT_NAME."&task=gateway_administrator",$msg);
	}
	
	function remove_gateway(){
		
		// Move uploaded file
		jimport('joomla.filesystem.file');
		$name = &JRequest::getCmd("gateway","");
		if(!$name){
			FactoryLayer::redirect("index.php?option=".F_COMPONENT_NAME."&task=gateway_administrator","No Gateway selected to remove!");
		}
		$dir = JPath::clean(JPATH_ROOT.DS."components".DS.F_COMPONENT_NAME.DS."plugins".DS."payment".DS);
		$files = JFolder::files($dir,$name);
		$files_list = array();
		if (count($files)) {
			foreach ($files as $file)
				$files_list[] = $dir.DS.$file;
		}
		$s = JFile::delete($files_list);
		$msg = "";
		if($s){
			$msg = "Gateway removed!";
			$db = & JFactory::getDBO();
			
			$db->setQuery("DELETE FROM `#__ads_paysystems` WHERE classname='{$name}';");
			$db->query();
		}
		FactoryLayer::redirect("index.php?option=".F_COMPONENT_NAME."&task=gateway_administrator",$msg);
	}
	
}