<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/


class JTheFactoryPlugins_controller{
	
    /**
     * Added INIT Method to load all dependencies both on front & backend
     *
     * @since 13/01/2010 (dd/mm/YYYY)
     */
	function init(&$application){
		
		if($application){
			$Tapp   = $application;
		}
		else {
		    $JTheFactoryAppClass = new JTheFactoryApp();
            $Tapp   = $JTheFactoryAppClass->getInstance(null,null);
			//$Tapp=&JTheFactoryApp::getInstance(null,null);
        }
	}

	/**
	 * Backend Application Router
	 *
	 * @param $task
	 * @param $act
	 * @return bool
	 */
	function route(){
		
		$task   = JRequest::getVar("task","");
		$act    = JRequest::getVar("act","");
		if (method_exists($this,$task)){
			return $this->$task($act);
		}

	}
	
	function plugins_administrator(&$act){
		require_once(dirname(__FILE__).DS."tmpl".DS."cpanel.php");
		return true;
	}
	
	function themes_administrator(&$act)
	{

		JLoader::register('FactoryThemesController', dirname(__FILE__).DS.'controllers'.DS.'themes.php');
		$FC = new FactoryThemesController();
		return $FC->checkTask($act);

	}

	function gallery_administrator(&$act)
	{

		JLoader::register('FactoryGalleryController', dirname(__FILE__).DS.'controllers'.DS.'gallery.php');
		$FC = new FactoryGalleryController();
		return $FC->checkTask($act);

	}
	
	function gateway_administrator(&$act)
	{

		JLoader::register('FactoryGatewayController', dirname(__FILE__).DS.'controllers'.DS.'gateway.php');
		$FC = new FactoryGatewayController();
		return $FC->checkTask($act);

	}
	
}

?>