<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

class JTheFactoryAdminToolbar extends JObject
{
    /* The Class methods should be the same Case as the TASK! in_array is case sensitive*/
    function settings()
    {
		JToolBarHelper::title( JText::_( 'ADS_SETTINGS' ) , 'cpanel.png' );
		JToolBarHelper::save( 'savesettings' );
		JToolBarHelper::back();

    }
    
    function paymentitemsconfig()
    {
		JToolBarHelper::title( JText::_( 'ADS_CONFIG_PAYMENT_ITEM' ) , 'cpanel.png' );
        JToolBarHelper::apply( 'paymentitemsconfigapply' );
        JToolBarHelper::save( 'savepaymentitemsconfig' );
		JToolBarHelper::back();
    }
    function  mailsettings()
    {
		JToolBarHelper::title( JText::_( 'ADS_CONFIG_EMAILS' ) , 'cpanel.png' );
		JToolBarHelper::save( 'savemailsettings' );
		JToolBarHelper::back();
    }
    
    function showpaymentconfig()
    {
		JToolBarHelper::title( JText::_( 'ADS_CONFIG_PAYMENT_GATEWAY' ) , 'cpanel.png' );
		JToolBarHelper::save( 'savepaymentconfig' );
		JToolBarHelper::back();
    }
    
    function payments()
    {
		JToolBarHelper::title( JText::_( 'ADSMAN_PAYMENTS_LIST' ), 'categories.png' );
		JToolBarHelper::custom( 'approve_payment', 'apply.png', 'apply_f2.png', JText::_('ADS_APPROVE_PAYMENT'), true );
		JToolBarHelper::back();
    }
    
    function categories()
    {
		JToolBarHelper::title(  JText::_( 'ADS_CATEGORY_MANAGEMENT' ) , 'categories.png' );
		JToolBarHelper::custom( 'newcat', 'new.png', 'new_f2.png', JText::_('ADS_NEW_CATEGORY'), false );
		JToolBarHelper::custom( 'editcat', 'edit.png', 'edit_f2.png', JText::_('ADS_EDIT_CATEGORIES'), true );
		JToolBarHelper::deleteList("","delcategories");
		JToolBarHelper::back();
    }
    
    function newcat($text='New Category')
    {
		JToolBarHelper::title(  JText::_( 'ADS_CATEGORY_MANAGEMENT' ) . ': <small><small>['.JText::_( $text ). ']</small></small>' , 'categories.png' );
		JToolBarHelper::save( "savecat" );
		JToolBarHelper::back();
    }
    
    function editcat()
    {
        JTheFactoryAdminToolbar::newcat('ADS_CATEGORY_EDIT');
    }
    
    function aboutComponent()
    {
        $Tapp=&JTheFactoryApp::getInstance();
        $text="<small><small>{$Tapp->description}</small></small>";
		JToolBarHelper::title( JText::_( 'ADS_ABOUT_EXTENSION' ) .  $text, "frontpage.png" );
    }
       
    function paymentitems()
    {
		JToolBarHelper::title( JText::_( 'PAYMENT_ITEMS' ), "frontpage.png" );

    }
    
    function cb_integration()
    {
		JToolBarHelper::title( JText::_( 'ADSMAN_CB_INTEGRATION' ), 'install.png' );
		JToolBarHelper::custom( 'savecbintegration', 'save.png', 'save_f2.png', JText::_('ADSMAN_SAVE_FIELDS'), false );
    	
    }

}

?>