<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/


//error_reporting(E_ALL);

class JTheFactoryApplication extends JObject{

	/* the Application Instance Name */
	
	var $appname = 'default';
	
	/* the Application Component prefix */
	
	var $prefix = null;
	
	/* the Application Instance Configuration */
	
	var $ini = null;
	
	/* the Application Framework version */
	
	var $_version = '1.2.0';
	
	/* the Application loaded addons */
	
	var $_addons = array();
	
	
	var $frontpage = 0;
	
	var $app_path_admin = '';
	var $app_path_front = '';

	/**
	 * Enhances a Instance of an Application in an static 
	 * array of applications
	 *
	 * @param $configfile path
	 * @param $name
	 * @return JTheFactoryApplication
	 */
	function &getInstance( $configfile=null, $front=0 , $name='default' )
	{	
		static $instances;
		
		if (!isset( $instances[$name] ) )
			$instances[$name] = new JTheFactoryApp( $configfile, $front );

		return $instances[$name];
	}
	
	function __construct( $configfile=null, $runfront=null )
	{
        
        if ($runfront!==null)
            $this->frontpage=$runfront;
            
        if ( !$configfile )
        	$configfile = dirname(__FILE__).DS.'..'.DS.'application.ini';
        	
        if ( !file_exists( $configfile ) )
        {
			$error = JError::raiseError(500, 'Unable to load application: '.$configfile);
			return $error;
        }
		
        if(function_exists('parse_ini_file'))
		{
			$ini = parse_ini_file($configfile,true);
		}
		else
		{
			jimport('joomla.registry.registry');
			jimport('joomla.filesystem.file');
		
			$handler =& JRegistryFormat::getInstance('INI');
			$data = $handler->stringToObject( JFile::read($configfile) ,true);
		
			$ini = JArrayHelper::fromObject($data);
		}
        
		$this->ini = $ini;
        $this->appname		= $this->getIniValue('name');
        $this->prefix		= $this->getIniValue('prefix');
        $this->description	= $this->getIniValue('description');
        $this->app_path_admin = JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_'.$this->appname.DS.'thefactory'.DS;
        $this->app_path_front = JPATH_ROOT.DS.'components'.DS.'com_'.$this->appname.DS.'thefactory'.DS;
        
        if (is_int($this->getIniValue('error_reporting'))) error_reporting($this->getIniValue('error_reporting'));
        
        /**
         *  Unsafe : Two applications on same page: APP_PREFIX has the latest prefix loaded
        */
        if (!defined('APP_EXTENSION')) {
            define('APP_EXTENSION', 'com_'.$this->appname );
        }
        if (!defined('APP_PREFIX')) {
            define('APP_PREFIX', $this->prefix );
        }
        if (!defined('COMPONENT_HOME_PAGE')) {
            define('COMPONENT_HOME_PAGE', $this->getIniValue('extension_home') );
        }
        if (!defined('COMPONENT_VERSION')) {
            define('COMPONENT_VERSION', $this->getIniValue('version') );
        }
        $defines=$this->getSection('defines');
        if (count($defines))
            foreach ($defines as $const=>$val)
                if ( !defined($const) ) define($const,$val);
                
        
        // LIBRARIES, HELPERS &

        jimport('joomla.application.component.model');
        jimport('joomla.filesystem.folder');

		  require_once($this->app_path_admin.'admin.layer.php');
		  require_once($this->app_path_admin.'thefactory.tables.php');
          require_once($this->app_path_admin.'admin.helper.php');
          require_once($this->app_path_front.'front.helper.php');
		
			// Custom Fields
			if ($this->getIniValue('use_custom_fields')){
				
				$eo_file = "admin.controller.php";
				$eo_classname = "JTheFactoryFields_controller";
				JLoader::register($eo_classname,$this->app_path_admin."fields".DS.$eo_file);
				$jc = new $eo_classname;
				$jc->init($this);
				
			}
			// LOADS Application Language Definitions
			require_once($this->app_path_admin.DS.'admin.language.php');
			
			if ($this->getIniValue('use_category_management')){
				@define('APP_CATEGORY_TABLE',$this->getIniValue('table','categories'));
				@define('APP_CATEGORY_TABLE_SEF', APP_CATEGORY_TABLE."_sef");
				@define('APP_CATEGORY_DEPTH',$this->getIniValue('table','depth'));
				require_once($this->app_path_admin.'category/table.category.php');
				
				require_once($this->app_path_admin.'category/category.core.php');
				if (!$this->frontpage){
					require_once($this->app_path_admin.'category/admin.category.php');
				}
      	}
		
        if ($this->getIniValue('use_integration')){
        	
            JLoader::register('JTheFactoryIntegration',$this->app_path_admin.'integration/admin.controller.php');
				$jc = new JTheFactoryIntegration();
				$jc->init($this);
				
        }

        if ($this->getIniValue('use_extended_profile'))
        {
			$constname = "APP_".APP_PREFIX."_PROFILE";
			@define($constname,$this->getIniValue('table','extended-profile'));
			JLoader::register('JTheFactoryUserProfile', JPATH_SITE . DS . 'components' . DS . 'com_adsman'.DS.'thefactory'.DS.'front.userprofile.php');
	        //JLoader::register('JTheFactoryUserProfile',JPATH_COMPONENT_SITE.DS.'thefactory'.DS.'front.userprofile.php');
        }
        
        if ($this->getIniValue('use_default_positions')){
			@define('APP_POSITION_TEMPLATES',$this->getIniValue('templates','positions-manager'));
			@define('APP_POSITION_DEFAULT_TEMPLATE',$this->getIniValue('default_template','positions-manager'));
			JLoader::register('JTheFactoryPositions', $this->app_path_admin.'positions'.DS.'positions.php');
        }

        
        if ($this->getIniValue('use_payment_gateways'))
        {
            JLoader::register('JTheFactoryPayment_controller',$this->app_path_admin.'payments/admin.controller.php');
            if (!defined('APP_PLUGIN_DIR')) {
                define('APP_PLUGIN_DIR',JPATH_SITE . DS . 'components' . DS . 'com_adsman'.DS.'plugins');
            }
            //define('APP_PLUGIN_DIR',JPATH_COMPONENT_SITE.DS.'plugins');
            $defines = $this->getSection('payment-tables');
            
            if (count($defines))
                foreach ($defines as $const=>$val)
                    if (!defined("PAYTABLE_".strtoupper($const))) define("PAYTABLE_".strtoupper($const),$val);


			if ( $this->frontpage )
				require_once($this->app_path_admin.'admin.payment.php');
	            JLoader::register('JTheFactoryPaymentController',JPATH_SITE . DS . 'components' . DS . 'com_adsman'.DS.'thefactory'.DS.'front.payment.php');
        }
        
        if ($this->getIniValue('use_currencies')){
        	if (!defined("APP_CURRENCY_TABLE_".strtoupper(APP_PREFIX))) {
                define("APP_CURRENCY_TABLE_".strtoupper(APP_PREFIX),$this->getIniValue('table_name','currency'));
            }
        }
        
		$eo_file = "admin.controller.php";
		$eo_classname = "JTheFactoryPlugins_controller";
		JLoader::register($eo_classname,$this->app_path_admin."plugins".DS.$eo_file);
		$jc = new $eo_classname;
		$jc->init($this);

		$eo_file = "admin.controller.php";
		$eo_classname = "JTheFactoryCUpgrade_controller";
		JLoader::register($eo_classname,$this->app_path_admin."cupgrade".DS.$eo_file);
	}
	
	function checktask( $task ){
		
		$db = JFactory::getDbo();
        
        if ($this->frontpage)
            $methoded='front_'.$task;
        else
			$methoded='admin_'.$task;
		
		if ($this->getIniValue('use_payment_gateways')){
			
            if($this->frontpage){
            	
	            $payment	= new JTheFactoryPaymentController($db, $_REQUEST);
            	$ret 		= $payment->front_checktask($task,$_REQUEST);
     	
            }
            else {
	            $payment	= new JTheFactoryPayment_Controller();
            	$ret 		= $payment->route();
            }
            if ($ret) return true;
        }
        
		if ($this->getIniValue('use_category_management'))
		{
			if ( !$this->frontpage ){
				$act = JRequest::getVar("act","");
                $JTheFactoryCategoryClass = new JTheFactoryCategory();
                $JTheFactoryCategories =$JTheFactoryCategoryClass->getInstance();
				//$JTheFactoryCategories =& JTheFactoryCategory::getInstance();
				$ret = $JTheFactoryCategories->route($methoded,$act);
	            if ($ret) return true;
			}
        }
        
		if ($this->getIniValue('use_custom_fields')){
            if($task=="field_administrator"){
           	
				$act = JRequest::getVar("act","list_fields");
				$jc = new JTheFactoryFields_controller();
				if($jc->route()) return true;
			}
        }
        
		$act = JRequest::getVar("act","list_themes");
		$jc = new JTheFactoryPlugins_controller();
		if($jc->route()) return true;
		
       if($task=="com_upgrade"){
			$act = JRequest::getVar("act","check");
			$jc = new JTheFactoryCUpgrade_controller();
			if($jc->route()) return true;
		}
		if( $task=="integration" ){

			$jc = new JTheFactoryIntegration();
			if($jc->route()) return true;

		}
		
        $JTheFactoryPositionsClass = new JTheFactoryPositions();
        $FactoryPositionsController = $JTheFactoryPositionsClass->getInstance();
		//$FactoryPositionsController =& JTheFactoryPositions::getInstance();
		$act = JRequest::getVar("act","list");
		$routed = $FactoryPositionsController->route($task,$act);
		if($routed==true) 
			return true;
		return false;
       
	}
	
	
	function getSection($section)
	{
	    return $this->ini[$section];
	}
	
	function getIniValue($key,$section='extension')
	{
	    if (isset($this->ini[$section][$key]))
	       return $this->ini[$section][$key];
	    else
	       return "";
	}

}