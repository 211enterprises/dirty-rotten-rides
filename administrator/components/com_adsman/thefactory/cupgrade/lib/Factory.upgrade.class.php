<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/


// ######################################################################
// ######################### Upgrade Installation class #################
// ###############################  configurable   ######################
// ######################################################################
 
class FactoryUpgrade{
	
	var $_db 			= null;

	var $_level 		= 0;
	
	var $_unzipped 		= 0;
	
	var $_installed_version = null;
	
	var $_message		= null;
	
	var $_filesToCopy = array();
	
	var $_installDir = null;
	
	var $_exceptions = array();
	
	
    function &getInstance($db=null)
    {
        static $instance;

        if (!isset ($instance))
        {
            global $database;
            if (!$db) $db=$database;

            $instance=new FactoryUpgrade($db);
        }
        return $instance;
    }
	
	/**
	 * Constructor 
	 */
	function FactoryUpgrade(&$db){
		
		$this->_db = $db;
		$this->getInstalledVersion();
		$this->isUpgradable();
		$this->updateLevel();
		
		if(isset($_SESSION["auction_upgrade"]["lang"]) && $_SESSION["auction_upgrade"]["lang"]!=null)
			$this->_lang = $_SESSION["auction_upgrade"]["lang"];
		else 
			$this->_lang = "en";
			
		if(isset($_SESSION["auction_upgrade"]["unzipped"]) && $_SESSION["auction_upgrade"]["unzipped"]!=null ){
			if(isset($_SESSION["auction_upgrade"]["install_dir"])&& $_SESSION["auction_upgrade"]["install_dir"]!=null){
				if(file_exists($_SESSION["auction_upgrade"]["install_dir"]))
				{
					$this->_installDir = $_SESSION["auction_upgrade"]["install_dir"];
					$this->_unzipped = 1;
				}
			}
		}
			
	}
	
	function upgradeDB(){
		$versions = explode(',', THEFACTORY_COMPONENT_DB_UPDATES);
		$sql_errorrs = array();
		
		
		foreach($versions as $i => $v){
			if($v > $this->_installed_version){
				$f = str_replace(".","",$v);
				$sql_file = THEFACTORY_ROOT_PATH.DIR_SEP.THEFACTORY_COMPONENT."_$f.sql";
				
				if(file_exists($sql_file)){
					//echo "---<strong>".$sql_file."</strong>";
					$e = SQLUtils::injectSQL($sql_file);
					if(is_array($e)){
						$sql_errorrs[] = "Error message in {$sql_file}<br />";
						$sql_errorrs = array_merge($sql_errorrs, $e);
					}
				}
			}
		}
		
		if( count($sql_errorrs) )
			return implode("<br />",$sql_errorrs);
		else
			return true;
	}
	
	function setMessage($msg){
		$_SESSION["auction_upgrade"]["msg"] = $msg;
		$this->_message = $msg;
	}
	
	function displayMessage(){
		if(isset($_SESSION["auction_upgrade"]["msg"]) && $_SESSION["auction_upgrade"]["msg"]!=null){
			echo $_SESSION["auction_upgrade"]["msg"];
			unset($_SESSION["auction_upgrade"]["msg"]);
		}
	}
	
	function chooseLanguage($lng){
		
		$_SESSION["auction_upgrade"]["lang"] = $lng;
		
	}
	
	function updateUnzippedStatus($dir){
		$_SESSION["auction_upgrade"]["unzipped"] = 1;
		$_SESSION["auction_upgrade"]["install_dir"] = $dir;
		$this->_installDir = $dir;
		$this->_unzipped = 1;
	}
	
	function copyXMLFile(){
		$xmlName = THEFACTORY_COMPONENT.".xml";
		copy($this->_installDir.$xmlName, THEFACTORY_ROOT_PATH.DIR_SEP."administrator".DIR_SEP."components".DIR_SEP."com_".THEFACTORY_COMPONENT.DIR_SEP.$xmlName);
	}
	
	function increaseLevel($new){
		
		$this->_level = $new;
		$_SESSION["auction_upgrade"]["level"] = $this->_level;
		 
	}
	
	function updateLevel(){
		if(isset($_SESSION["auction_upgrade"]["level"]) && $_SESSION["auction_upgrade"]["level"]!=null)
			$this->_level = $_SESSION["auction_upgrade"]["level"];
		else 
			$this->_level = 0;
	}
	
	function isUpgradable(){
		if($this->_installed_version == THEFACTORY_COMPONENT_VERSION)
			die("You are up to date!");
		if ( !in_array($this->_installed_version, explode(",",THEFACTORY_COMPONENT_UPGRADABLE) )  ){
			die("One of the following ".THEFACTORY_COMPONENT_NAME." versions must be installed: ".THEFACTORY_COMPONENT_UPGRADABLE);
		}
		// Upgradable
		//$this->increaseLevel(0);
	}
	
	function getInstalledVersion(){
		
		if (!file_exists(THEFACTORY_ROOT_PATH.'/administrator/components/com_'.THEFACTORY_COMPONENT.'/'.THEFACTORY_COMPONENT.'.xml'))
			die("Ads Factory does not seem to be installed!!");
			
		$f = fopen(THEFACTORY_ROOT_PATH.'/administrator/components/com_'.THEFACTORY_COMPONENT.'/'.THEFACTORY_COMPONENT.'.xml',"r");
		$xml="";
		while ($s=fread($f,4096)) {
			$xml.=$s;
		}
		
		preg_match_all("/<version>(.*?)<\/version>/",$xml,$m);
		$this->_installed_version = strip_tags($m[0][0]);
	}
	
	/**
	 * Add excepted directory names from overwriting 
	 */
	function addExceptions($Xceptions){
		$this->_exceptions = $Xceptions;
	}
	
	function copyFiles(){
		
		$TEMP_unziped = mosPathName($this->_installDir);
		
		if(count($this->_filesToCopy)>0){
			foreach ($this->_filesToCopy as $src => $fileName ){
				$src = mosPathName($src,false);
				$fileRealPath = str_replace($TEMP_unziped,"",$src);
				$dst = mosPathName(THEFACTORY_ROOT_PATH."/").$fileRealPath;
				copy_overdir($src,$dst);
			}
		}
	}
	
	function setFilesToCopy(){
		$dir_handle = @opendir($this->_installDir) or die("Unable to open $this->_installDir"); 
		$this->_filesToCopy = $this->list_dir($dir_handle,$this->_installDir);
	}
	
	function list_dir($dir_handle,$path){
		$ds = trim(" / ");
		static $list;
	    while (false !== ($file = readdir($dir_handle))) {
	    	if(!in_array($file,$this->_exceptions)){
		        $dir = $path.$file.$ds;
		        if(is_dir($dir) && $file != '.' && $file !='..' )
		        {
		        	$handle = @opendir($dir) or die("undable to open file $file");
		        	$this->list_dir($handle, $dir);
		        }elseif($file != '.' && $file !='..'){
		        	$list[$path.$file]=$file;
		        }
	    	}
	    }
	    closedir($dir_handle);
	    return $list;
	}
	 
	function getFilesInDir($prefix="",$dir)
	{
	    $results = array();
	    if (!is_dir($dir)) return false;
	    $handler = opendir($dir);
	    if (!$handler) return false;
	
	    while ($file = readdir($handler)) {
	        if ($file != '.' && $file != '..')
	        	if(!is_dir($file))
                	$results[] = $file;
	    }
	
	    // tidy up: close the handler
	    closedir($handler);
	
	    // done!
	    return $results;
	}
	
	// FUTURE FEATURE
	function saveBackup(){
		// save SQL logs
		// zip files 
	}
	
}

/**
 * HTML Drawing class 
 **/
class HTMLUtils{

	/**
	 * 
	 * DRAWS LEFT BAR status list 
	 *
	 */
	function displayInstallStatus($level){
		$lMessages = array(
			"Choose Language",
			"Archive unzipping..",
			"Files upgrade..",
			"Database upgrade..",
			"Finish.."
		);
		$lMessagesFinished = array(
			"Language chosen",
			"Archive unzipped to <strong>".$extractdir.$_SESSION["auction_upgrade"]["unziped"]."</strong>",
			"Files upgraded",
			"Database upgraded",
			"Finished Upgrading"
		);
		
		for($i=0; $i<count($lMessages); $i++){
			$p = $i+1;
			if($i==$level)
				echo "<div style='color:#FF0000;'>&nbsp;<strong style='background:#00DDDE; color:#000000;'>&nbsp;&nbsp; Step {$p}.&nbsp;&nbsp;</strong>&nbsp;".$lMessages[$i]."</div>";
			elseif($i<$level)
				echo "<div style='color:#000;'>&nbsp;<strong style='background:#008DDE;'>&nbsp;&nbsp;Step {$p}.&nbsp;&nbsp;</strong>&nbsp;<strong>".$lMessagesFinished[$i]."</strong></div>";
			else
				echo "<div style='color:#FFF;'>&nbsp;<strong style='background:#AAAAAA;'>&nbsp;&nbsp;Step {$p}.&nbsp;&nbsp;</strong>&nbsp;".$lMessages[$i]."</div>";
		}
	}
	
	function displayLevelForm($level){
		$Upgrade = & FactoryUpgrade::getInstance($database,THEFACTORY_EXTRACT_PATH);
		global $FLanguage;
		switch($level){
			case "-1":{
				?>
					<table style="font-size:20px; ">
						<tr>
							<td colspan="2" align="left">
								<p style="color:#FF0000;">
								<?php echo $Upgrade->error; ?>
								</p>
								<br />
								<input type="submit" value="Retry" style="margin-left:150px;" />
							</td>
						</tr>
					</table>
				<?php
				break;
			}
			case "0":{
				?>
					<strong style="color:#FF0000;"><?php echo $FLanguage["en"]["msg_backup"]; ?></strong>
					<br /><br />
					<table style="font-size:20px; ">
						<tr>
							<td>Choose language</td>
							<td align="left">
								<select name="language">
									<option value="en">English</option>
									<option value="de">Deutsch</option>
								</select>
							</td>
						</tr>
						<tr>
							<td colspan="2" align="left">
								<br />
								<input type="submit" value="Continue" style="margin-left:150px;" onclick="document.getElementById('lang_en').style.display='';" />
							</td>
						</tr>
						<tr>
							<td></td>
							<td id="lang_en" style="display:none;">
							<?php echo $FLanguage["en"]["unzip_msg"]; ?>
							</td>
						</tr>
					</table>
				<?php
				break;
			}
			case "1":{
				?>
					<table>
						<tr>
							<td>
							<?php echo $FLanguage[$Upgrade->_lang]["unzip_msg"]; ?>
							</td>
						</tr>
					</table>
				<?php
				break;
			}
			case "2":{
				?>
				<table>
					<tr>
						<td>
							<?php echo $FLanguage[$Upgrade->_lang]["overwrite_templates"]; ?>
						</td>
						<td>
							<input name="o_templates" type="radio" value="1" checked="checked" /> <?php echo $FLanguage[$Upgrade->_lang]["overwrite_yes"]; ?>
							<input name="o_templates" type="radio" value="0" /> <?php echo $FLanguage[$Upgrade->_lang]["overwrite_no"]; ?>
						</td>
					</tr>
					<tr>
						<td>
							<?php echo $FLanguage[$Upgrade->_lang]["overwrite_lang"]; ?>
						</td>
						<td>
							<input name="o_lang" type="radio" value="1" checked="checked" /> <?php echo $FLanguage[$Upgrade->_lang]["overwrite_yes"]; ?>
							<input name="o_lang" type="radio" value="0" /> <?php echo $FLanguage[$Upgrade->_lang]["overwrite_no"]; ?>
						</td>
					</tr>
					<tr>
						<td>
							<?php echo $FLanguage[$Upgrade->_lang]["overwrite_images"]; ?>
						</td>
						<td>
							<input name="o_images" type="radio" value="1" checked="checked" /> <?php echo $FLanguage[$Upgrade->_lang]["overwrite_yes"]; ?>
							<input name="o_images" type="radio" value="0" /> <?php echo $FLanguage[$Upgrade->_lang]["overwrite_no"]; ?>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<input type="submit" value="Continue.." />
						</td>
					</tr>
				</table>
				<?php
				break;
			}
			case "3":{
				?>
				<table>
					<tr>
						<td>
							<?php
								$Upgrade = & FactoryUpgrade::getInstance($database,THEFACTORY_EXTRACT_PATH);
							?>
							<?php  $Upgrade->displayMessage();?>
						</td>
					</tr>
					<tr>
						<td>
							<input type="submit" value="Continue.." />
						</td>
					</tr>
				</table>
				<?php
				break;
			}
		}
	}
	
	function displayLevelRoute($level){
		global $install_levels;
		switch($level){
			case "0":{
				?><input type="hidden" name="action" value="choose_language" /><?php
				break;
			}
			case "2":{
				?><input type="hidden" name="action" value="upgrade" /><?php
				break;
			}
			case $install_levels["db_update"]:{
				?><input type="hidden" name="action" value="finish" /><?php
				break;
			}
		}
	}
	
	function showUpgradeForm($level){
		global $install_levels;
		?>
		<html>
			<head>
				<title><?php echo THEFACTORY_COMPONENT_NAME." ".THEFACTORY_COMPONENT_VERSION; ?> Upgrade Script</title>
			</head>
		<?php
			HTMLUtils::showHeader();
			$Upgrade = & FactoryUpgrade::getInstance($database,THEFACTORY_EXTRACT_PATH);
			
		?>
		<?php if($Upgrade->_level != $install_levels["db_update"]) { ?>
		<div style="color:#FF0000; width:80%; background:#AAAAAA;"><?php  $Upgrade->displayMessage();?></div>
		<?php } ?>
		<form name="upgrade_form" method="post" action="<?php echo THEFACTORY_ROOT_SITE."/Factory.upgrade.index.php";?>">
			<table border="0" width="100%">
				<tr>
					<td style="background:#DEDEDE; width:250px;">
						Upgrade status:
						<?php HTMLUtils::displayInstallStatus($level);?>
					</td>
					<td valign="top" style="background:#EFEFEF;  padding:10px;">
						<?php HTMLUtils::displayLevelForm($level);?>
					</td>
				</tr>
			</table>
		<?php HTMLUtils::displayLevelRoute($level);?>
		</form>
		<?php
	}
	
	
	
	function showHeader()
	{
		
		$Upgrade = & FactoryUpgrade::getInstance($database,THEFACTORY_EXTRACT_PATH);
		echo "<img src='http://www.thefactory.ro/images/vm/auction.jpg' /> <h2>Auction Factory Upgrade Package </h2><br /><br />";
		echo "You have installed <strong>".THEFACTORY_COMPONENT_NAME." ".$Upgrade->_installed_version."</strong> ... Upgrading to <strong>".THEFACTORY_COMPONENT_NAME." ".THEFACTORY_COMPONENT_VERSION."</strong> <br />";
		
	}
	
}







function copy_overdir($src,$dst){
	$Upgrade = & FactoryUpgrade::getInstance($database,THEFACTORY_EXTRACT_PATH);
	$ds = DIR_SEP;
	$caca = explode($ds, $dst);
	$fil = array_pop($caca);
	$real_dir = str_replace($fil,"",$dst);
	if(!file_exists($real_dir))
		$success = mkdir_r($real_dir,0777);
	else
		$success = true;
	
	if(!$success){
		
		$Upgrade->setMessage($Upgrade->_message."<br />".$real_dir." could not be made <br /> ");
	}
	
	$success = copy($src,$dst);
	if(!$success){
		
		$Upgrade->setMessage($Upgrade->_message."<br />".$src." could not be copied to $dst <br /> ");

	}
}
 
function mkdir_r($dirName, $rights=0777){
    $success = false;
	$dirs = explode(DIR_SEP, $dirName);
    $dir='';
    foreach ($dirs as $part) {
        $dir.=$part.DIR_SEP;
        if (!is_dir($dir) && strlen($dir)>0)
            $success = mkdir($dir, $rights);
    }
    return $success;
}



// BRIDGING TO DO FOR SA 
function saRedirect($url){
	mosRedirect($url);
}

function saGetParam($SRC_ARRAY,$key,$def =null, $type_mask=null){
	return mosGetParam($SRC_ARRAY,$key,$def,$mask);
}

class SQLUtils{
	
	function injectSQL($sqlfile)
	{
	    global $database;
		$mqr = @get_magic_quotes_runtime();
		@set_magic_quotes_runtime(0);
		$query = fread( fopen(  $sqlfile, 'r' ), filesize($sqlfile ) );
		@set_magic_quotes_runtime($mqr);
		$pieces  = SQLUtils::split_sql($query);
	
		for ($i=0; $i<count($pieces); $i++) {
			$pieces[$i] = trim($pieces[$i]);
			if(!empty($pieces[$i]) && $pieces[$i] != "#") {
				$database->setQuery( $pieces[$i] );
				if (!$database->query()) {
					$errors[] = $database->getErrorMsg().":<br />". $pieces[$i] ." <br />";
				}
			}
		}
	   return $errors;
	}
	function split_sql($sql) {
		$sql = trim($sql);
		$sql = ereg_replace("\n#[^\n]*\n", "\n", $sql);
	
		$buffer = array();
		$ret = array();
		$in_string = false;
	
		for($i=0; $i<strlen($sql)-1; $i++) {
			if($sql[$i] == ";" && !$in_string) {
				$ret[] = substr($sql, 0, $i);
				$sql = substr($sql, $i + 1);
				$i = 0;
			}
	
			if($in_string && ($sql[$i] == $in_string) && $buffer[1] != "\\") {
				$in_string = false;
			}
			elseif(!$in_string && ($sql[$i] == '"' || $sql[$i] == "'") && (!isset($buffer[0]) || $buffer[0] != "\\")) {
				$in_string = $sql[$i];
			}
			if(isset($buffer[1])) {
				$buffer[0] = $buffer[1];
			}
			$buffer[1] = $sql[$i];
		}
	
		if(!empty($sql)) {
			$ret[] = $sql;
		}
		return($ret);
	}
	
}

?>