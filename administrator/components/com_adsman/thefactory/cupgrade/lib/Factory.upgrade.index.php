<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/



// REQUIRE ALL ONCE
require_once("Factory.upgrade.init.php");
require_once("Factory.upgrade.class.php");

global $database, $install_levels, $FLanguage;

session_name( md5( THEFACTORY_ROOT_SITE ) );
session_start();

if(!extension_loaded('zlib')) {
	die("The installer can't continue before zlib is installed");
}

$Upgrade = & FactoryUpgrade::getInstance($database);

//unset($_SESSION);
//$Upgrade->increaseLevel(0);
//echo "<PRE>";var_dump($Upgrade->_level);
//exit;


if(!is_writable(THEFACTORY_ROOT_PATH.DIR_SEP."components".DIR_SEP."com_".THEFACTORY_COMPONENT)){
	$c = chmod(THEFACTORY_ROOT_PATH.DIR_SEP."components".DIR_SEP."com_".THEFACTORY_COMPONENT,0777);
	if(!$c){
		$Upgrade->error = $FLanguage["en"]["error_permission_front"];
		HTMLUtils::showUpgradeForm(-1);
		exit;
	}
		
}

if(!is_writable(THEFACTORY_ROOT_PATH.DIR_SEP."administrator".DIR_SEP."components".DIR_SEP."com_".THEFACTORY_COMPONENT)){
	$Upgrade->error = $FLanguage["en"]["error_permission_admin"];
	HTMLUtils::showUpgradeForm(-1);
	exit;
}

if(!is_writable(THEFACTORY_TMP_DIR)){
	$c = chmod(THEFACTORY_TMP_DIR,0777);
	if(!$c){
		$Upgrade->error = $FLanguage["en"]["error_permission_tmp"];
		HTMLUtils::showUpgradeForm(-1);
		exit;
	}
		
}

if(!file_exists(THEFACTORY_ARCHIVE_PATH)){
	$Upgrade->error = $FLanguage["en"]["error_file"];
	HTMLUtils::showUpgradeForm(-1);
	exit;
}


// ==> CONTROLER
$action = mosGetParam(array_merge($_POST,$_GET), "action");


if(!$action){
	if($Upgrade->_level==1){
		$action = "do_unzip";
	}elseif( $Upgrade->_level==2 ){
		$action = "upgrade_config";
	}
}

switch($action){
	case "choose_language":{
		$Upgrade->increaseLevel( $install_levels["unzip"] );
		HTMLUtils::showUpgradeForm($Upgrade->_level);
		$mlang = mosGetParam($_POST, "language");
		$Upgrade->chooseLanguage( $mlang );
		saRedirect( THEFACTORY_ROOT_SITE."/".'Factory.upgrade.index.php?action=do_unzip' );
		break;
	}
	case "do_unzip":{
		// ================> UNZIP TO /tmp/
		if($Upgrade->_unzipped!=1){
			$udir =  uniqid( 'install_' );
			$EXTRACT_PATH = mosPathName(THEFACTORY_TMP_DIR.'/'.$udir);
			require_once( THEFACTORY_ROOT_PATH. '/administrator/includes/pcl/pclzip.lib.php' );
			require_once( THEFACTORY_ROOT_PATH. '/administrator/includes/pcl/pclerror.lib.php' );
			
			$zipfile = new PclZip( THEFACTORY_ARCHIVE_PATH );
			if(substr(PHP_OS, 0, 3) == 'WIN') {
				define('OS_WINDOWS',1);
			} else {
				define('OS_WINDOWS',0);
			}
			$ret = $zipfile->extract( PCLZIP_OPT_PATH, $EXTRACT_PATH );
			if($ret == 0) {
				die( 'Unrecoverable error '.$zipfile->errorName(true).'"' );
			}
			
			$Upgrade->updateUnzippedStatus($EXTRACT_PATH);
			$Upgrade->setMessage("Succesfully unzipped!");
		}else{
			if(file_exists($Upgrade->_installDir))
				$Upgrade->setMessage("Allready unzipped!");
		}
		$Upgrade->increaseLevel($install_levels["upgrade"]);
		
		saRedirect(THEFACTORY_ROOT_SITE."/".'Factory.upgrade.index.php?action=upgrade_config');
		// <================ UNZIP TO /tmp/
		break; 
	}
	case "upgrade_config":{
		if( !file_exists($Upgrade->_installDir) ){
			$Upgrade->setMessage("Archive not unzipped!");
			$Upgrade->increaseLevel( $install_levels["unzip"] );
			saRedirect(THEFACTORY_ROOT_SITE."/".'Factory.upgrade.index.php?action=choose_language');
		}
		HTMLUtils::showUpgradeForm( $Upgrade->_level );
		break;
	}
	case "finish":{
		
		if( !file_exists($Upgrade->_installDir) ){
			$Upgrade->setMessage("Archive not unzipped!");
			$Upgrade->increaseLevel(0);
			saRedirect(THEFACTORY_ROOT_SITE."/".'Factory.upgrade.index.php?action=choose_language');
		}
		
		$Upgrade->increaseLevel($install_levels["finish"]);
		$Upgrade->copyXMLFile();
		$Upgrade->setMessage("Success!");
		HTMLUtils::showUpgradeForm($Upgrade->_level);
		break;
		
	}
	case "db_update":{
		
		$s = $Upgrade->upgradeDB();
		if($s!==true){
			$Upgrade->setMessage("<strong style='color:#FF0000;'>Errors found in SQL Updating!</strong><br />".$s);
			HTMLUtils::showUpgradeForm( $Upgrade->_level );
		}
		else{
			$Upgrade->setMessage("Success!");
			HTMLUtils::showUpgradeForm( $Upgrade->_level );
			$Upgrade->increaseLevel($install_levels["finish"]);
			//saRedirect(THEFACTORY_ROOT_SITE."/".THEFACTORY_COMPONENT_NAME.'.upgrade.index.php?action=finish');
		}
		
		break;
	}
	case "upgrade":{
		
		if( !file_exists($Upgrade->_installDir) ){
			$Upgrade->setMessage("Archive not unzipped!");
			$Upgrade->increaseLevel( $install_levels["unzip"] );
			saRedirect(THEFACTORY_ROOT_SITE."/".'Factory.upgrade.index.php?action=choose_language');
		}
		
		$extractdir = $Upgrade->_installDir;
		
		// CUSTOM FOR AUCTION for the moment bicoz itz stupid archived
		$Eexceptions = array(
			"options.php",
			"bids.xml",
			"smarty",
			"cache"
		);
		
		$ds = trim(" / ");
		$ADMIN_files = array(
		);
		
		$o_lang = saGetParam($_POST,"o_lang");
		$o_templates = saGetParam($_POST,"o_templates");
		$o_images = saGetParam($_POST,"o_images");

		if($o_lang==0)
			array_push($Eexceptions,"lang");
		
		if($o_templates==0)
			array_push($Eexceptions,"templates");
			
		if($o_images==0)
			array_push($Eexceptions,"images");
		
		$Upgrade->addExceptions( array_merge($ADMIN_files, $Eexceptions) ) ;
		$Upgrade->setFilesToCopy();
		$Upgrade->copyFiles();
		
		/*
		 foreach ($ADMIN_files as $s => $fileName ){
			$src = mosPathName($Upgrade->_installDir.DIR_SEP).$fileName;
			$dst = mosPathName(THEFACTORY_ROOT_PATH.DIR_SEP."administrator".DIR_SEP."components/com_".THEFACTORY_COMPONENT."/").$fileName;
			copy_overdir($src,$dst);
		 }
		*/
		
		
		$Upgrade->increaseLevel( $install_levels["db_update"] );
		saRedirect(THEFACTORY_ROOT_SITE."/".'Factory.upgrade.index.php?action=db_update');
		break;
	}
	default :{
		HTMLUtils::showUpgradeForm($Upgrade->_level);
		break;
	}
	
}
// <== CONTROLER

?>