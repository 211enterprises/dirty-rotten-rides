<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

require_once("Factory.upgrade.config.php");

/*** access Joomla's configuration file ***/
    $my_path = dirname(__FILE__);

    if( file_exists($my_path."/../../../../configuration.php")) {
        require_once($my_path."/../../../../configuration.php");
		define( 'JPATH_BASE', dirname( $my_path."/../../../../configuration.php") );
    }elseif( file_exists($my_path."/../../../configuration.php")) {
        require_once($my_path."/../../../configuration.php");
		define( 'JPATH_BASE', dirname( $my_path."/../../../configuration.php") );
    }
    elseif( file_exists($my_path."/../../configuration.php")){
        require_once($my_path."/../../configuration.php");
		define( 'JPATH_BASE', dirname( $my_path."/../../configuration.php") );
    }
    elseif( file_exists($my_path."/configuration.php")){
        require_once( $my_path."/configuration.php" );
		define( 'JPATH_BASE', dirname( $my_path."/configuration.php") );
    }
    else
        die( "Joomla Configuration File not found!" );


    if( class_exists( 'jconfig' ) ) {
			define( '_JEXEC', 1 );
			define( 'DS', DIRECTORY_SEPARATOR );

			define( 'JPATH_CONFIGURATION', constant('JPATH_BASE') );

			// Load the framework
			require_once ( JPATH_BASE . DS . 'includes' . DS . 'defines.php' );
			require_once ( JPATH_BASE . DS . 'includes' . DS . 'framework.php' );

			// create the mainframe object
			$mainframe = & JFactory::getApplication( 'site' );

			// Initialize the framework
			$mainframe->initialise();

			//create the database object
			$database = &JFactory::getDBO();

            JPluginHelper::importPlugin('system');
    }else{
    	require_once($mosConfig_absolute_path. '/includes/database.php');
    	require_once($mosConfig_absolute_path. '/includes/joomla.php');


        // load Joomla Language File
        if (file_exists( $mosConfig_absolute_path. '/language/'.$mosConfig_lang.'.php' )) {
            require_once( $mosConfig_absolute_path. '/language/'.$mosConfig_lang.'.php' );
        }
        elseif (file_exists( $mosConfig_absolute_path. '/language/english.php' )) {
            require_once( $mosConfig_absolute_path. '/language/english.php' );
        }
    }
    
    
// BRIDGING
   
    // SA Defines, globalize
    define('THEFACTORY_ROOT_SITE', $mosConfig_live_site);
    define('THEFACTORY_ROOT_PATH', $mosConfig_absolute_path);
    define('DIR_SEP', DIRECTORY_SEPARATOR);
	
	define('THEFACTORY_TMP_DIR', mosPathName(THEFACTORY_ROOT_PATH.'/media/') );
	define('THEFACTORY_ARCHIVE_NAME', THEFACTORY_COMPONENT_NAME.'_'.THEFACTORY_COMPONENT_VERSION.'.zip');
	define('THEFACTORY_ARCHIVE_PATH', mosPathName(THEFACTORY_ROOT_PATH).THEFACTORY_ARCHIVE_NAME);
    
    
	$FLanguage = array(
		"en"=>array(
			"unzip_msg" => "Unzipping files to temporary directory. Wait a few seconds pls. ",
			"overwrite_yes" => "Yes",
			"overwrite_no" => "No",
			"overwrite_templates" => "Overwrite templates?",
			"overwrite_images" => "Overwrite images?",
			"overwrite_lang" => "Overwrite language files?",
			"error_file" => "The file ".THEFACTORY_COMPONENT_NAME."_".THEFACTORY_COMPONENT_VERSION.".zip is not found or not uploaded! Please upload the new Kit in the root of the site",
			"error_permission_tmp" => "tmp/ component directory is not writable!",
			"error_permission_front" => "Front component directory is not writable!",
			"error_permission_admin" => "Backend component directory is not writable!",
			"msg_backup" => "BACKUP FIRST THE FILES AND THE DATABASE!"
		),
		"de"=>array(
			"unzip_msg" => "Entpacken von Dateien auf tempor�ren Verzeichnis. Warten Sie einige Sekunden, pls.",
			"overwrite_yes" => "Ja",
			"overwrite_no" => "Nein",
			"overwrite_templates" => "Overwrite templates?",
			"overwrite_images" => "Overwrite images?",
			"overwrite_lang" => "Overwrite language files?",
			"error_file" => "The file ".THEFACTORY_COMPONENT_NAME."_".THEFACTORY_COMPONENT_VERSION.".zip is not found or not uploaded! Please upload the new Kit in the root of the site",
			"error_permission_tmp" => "tmp/ component directory is not writable!",
			"error_permission_front" => "Front component directory is not writable!",
			"error_permission_admin" => "Backend component directory is not writable!",
			"msg_backup" => "BACKUP FIRST THE FILES AND THE DATABASE!"
		)
	);
	
	$install_levels = array(
		"error_file" => -1,  
	 	"choose_lang"=> 0,  
	 	"unzip"=> 1,
	 	"upgrade"=> 2,  
	 	"db_update"=> 3,   
	 	"finish"=> 4  
	);
	
	$_GLOBALS['FLanguage'] = $FLanguage;
	$_GLOBALS['install_levels'] = $install_levels;
	$_GLOBALS['database'] = $database;

?>