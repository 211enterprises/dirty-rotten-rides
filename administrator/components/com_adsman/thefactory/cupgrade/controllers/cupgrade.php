<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/


class ComUpgradeController{
	
	var $_db 	= null;
	
	
	function checkTask( &$act )
	{
		$this->_db = &FactoryLayer::getDB();
		if(!$act)
			$act="check_upgrade";
			
		$method_name = $act;
		
		if ( method_exists($this,$method_name) ){
			$this->$method_name();
			return true;
		}

		return false;
	}

	function check_upgrade(){
		
        $Tapp=&JTheFactoryApp::getInstance();

		$filename=$Tapp->getIniValue('version_root').'/com_adsman.xml';
		
		$fileContents =@JTheFactoryHelper::remote_read_url($filename,10);
		
		if (!$fileContents){
			require_once(DOMIT_INCLUDE_PATH . 'php_file_utilities.php');
		
			$fileContents =& php_file_utilities::getDataFromFile($filename, 'r');
		}
		if (!$fileContents) return ;
		
		$xmlparser = new JSimpleXML;
		
		if (!$xmlparser->loadString($fileContents))
		{
			return false;
		}
		$xmldoc = $xmlparser->document;
		
		
		$element = $xmldoc->getElementByPath('/latestversion');
		$latestversion = $element ? $element->toString() : '';
		$element = $xmldoc->getElementByPath('/downloadlink');
		$downloadlink =$element ? $element->toString() : '';
		
		$lists = array();
		$lists["installed_version"] = COMPONENT_VERSION;
		$lists["latest_version"] = $latestversion;
		$lists["download_link"] = $downloadlink;
		
		require_once(dirname(__FILE__).DS."..".DS."tmpl".DS."check_upgrade.php");
		return true;
	}
	
	
}


