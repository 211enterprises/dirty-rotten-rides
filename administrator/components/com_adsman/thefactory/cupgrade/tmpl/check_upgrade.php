<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/
?>

<fieldset class="adminform">
	<legend><?php echo JText::_( 'Upgrade Ads Factory!' ); ?></legend>
	
<table class="paramlist admintable" width="100%">
	<tr>
		<td class="paramlist_key">
			Your installed version
		</td>
		<td>
			<?php echo $lists["installed_version"];?>
		</td>
	</tr>
	<tr>
		<td class="paramlist_key">
			Latest version
		</td>
		<td>
			<?php echo $lists["latest_version"];?>
		</td>
	</tr>
	<?php
    	$ver1=explode('.',COMPONENT_VERSION);
    	$ver2=explode('.',$lists["latest_version"]);
    	
    	$isNew=false;
        	for($i=0;$i<count($ver1);$i++){
    	    if (intval($ver1[$i])<intval($ver2[$i])){
    	        $isNew=true;
        	    $ver_info= JText::_('New Version Available!');
    	        break;
    	    }
    	    if (intval($ver1[$i])>=intval($ver2[$i])){
    	        $isNew=false;
          	    $ver_info= JText::_('Your Version is Up to Date!');
    	    }
    	}
    	
// dev shit;
    	$isNew = true;
	?>
	<tr>
		<td class="paramlist_key" colspan="2" style="text-align:center !important;">
			<span style="color:red"><?php echo $ver_info; ?></span>
		</td>
	</tr>
<?php if ($isNew) { ?>
	<tr>
		<td class="paramlist_key" colspan="2" style="text-align:center !important;">
			<br /><br />
			<h3>Upgrade <?php echo $lists["installed_version"];?> to <?php echo $lists["latest_version"];?></h3>
		</td>
	</tr>
	<tr>
		<td class="paramlist_key">
			Upload Ads Factory <?php echo $lists["latest_version"];?> new package:
		</td>
		<td>
			<input type="file" name="ads_kit" />
		</td>
	</tr>
	<tr>
		<td class="paramlist_key">
			 <?php echo $lists["latest_version"];?> upgrade patch:
		</td>
		<td>
			<table class="adminform">
				<tbody><tr>
					<th colspan="2">Upload Package File</th>
				</tr>
				<tr>
					<td width="120">
						<label for="install_package">Package File:</label>
					</td>
					<td>
						<input type="file" size="57" name="install_package" id="install_package" class="input_box">
					</td>
				</tr>
				</tbody>
			</table>
			<table class="adminform">
				<tbody><tr>
					<th colspan="2">Install from URL</th>
				</tr>
				<tr>
					<td width="120">
						<label for="install_url">Install URL:</label>
					</td>
					<td>
						<input type="text" value="http://" size="70" class="input_box" name="install_url" id="install_url">
					</td>
				</tr>
				</tbody>
			</table>
	</td>
	</tr>
	<tr>
		<td class="paramlist_key">Upgrade settings:</td>
		<td>
			<table class="adminform">
				<tbody>
				<tr>
					<td width="120">
						<label>Overwrite templates:</label>
					</td>
					<td>
						<?php echo JHTML::_("select.booleanlist","overwrite_templates");?>					
					</td>
				</tr>
				</tbody>
			</table>
			<table class="adminform">
				<tbody>
				<tr>
					<td width="120">
						<label>Overwrite language files:</label>
					</td>
					<td>
						<?php echo JHTML::_("select.booleanlist","overwrite_lang_files");?>
					</td>
				</tr>
				</tbody>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2" align="center"><br /></td>
	</tr>
	<tr>
		<td colspan="2" align="center">Upgrade <a href="#"><img src="<?php echo JURI::root();?>/administrator/images/next_f2.png" /></a></td>
	</tr>
<?php } ?>
</table>
</fieldset>