<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/


class JTheFactoryCUpgrade_controller{
	
    /**
     * Added INIT Method to load all dependencies both on front & backend
     *
     * @since 13/01/2010 (dd/mm/YYYY)
     */
	function init(&$application){
		
		if($application){
			$Tapp=$application;
		}
		else	
			$Tapp=&JTheFactoryApp::getInstance(null,null);

		
	}

	/**
	 * Backend Application Router
	 *
	 * @param $task
	 * @param $act
	 * @return bool
	 */
	function route(){
		
		$task = & JRequest::getVar("task","");
		$act = & JRequest::getVar("act","");
		
		if (method_exists($this,$task)){
			return $this->$task($act);
		}
        
	}
    
	/**
	 * Backend Main Controller task
	 *
	 * @param $act
	 * @return bool
	*/
    function com_upgrade(&$act)
    {

    	
		JLoader::register('ComUpgradeController', dirname(__FILE__).DS.'controllers'.DS.'cupgrade.php');
		$ComUpgradeController = new ComUpgradeController();
		return $ComUpgradeController->checkTask($act);
	}
	
}

?>