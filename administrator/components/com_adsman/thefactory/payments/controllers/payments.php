<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/


class FactoryPaymentsController{
	
	var $_db 	= null;
	
	
	function checkTask( $act )
	{
		$this->_db = &FactoryLayer::getDB();
		
		$method_name = $act;

		if ( method_exists($this,$method_name) ){
		//	require_once(dirname(__FILE__).DS."..".DS."tmpl".DS."payments.toolbar.php");
			$this->$method_name();
			return true;
		}

		return false;
	}
	
	function payments(){
		
		$payment=&JTheFactoryPaymentLib::getInstance();
        $payment->showPaymentLog();
        return true;
		
	}
	
    function new_payment()
    {
		$payment=&JTheFactoryPaymentLib::getInstance();
        $payment->showAddNewPayment();
        return true;
    }
    
	function Save_Payment() {
        
		$payment=&JTheFactoryPaymentLib::getInstance();
        $payment->saveAddNewPayment();
        $japp=&JFactory::getApplication();
        $japp->redirect('index.php?option='.APP_EXTENSION.'&task=payments',JText::_('ADS_NEW_PAYMENT_SAVED'));
        return true;
        
	}
	
    function Approve_Payment() {
    	
		$payment=&JTheFactoryPaymentLib::getInstance();
        $approved = $payment->approvePayment();
        $japp=&JFactory::getApplication();
        $msg = ($approved == true) ? JText::_('ADS_PAYMENT_APPROVED') : JText::_('ADS_NOT_ENOUGH_CREDITS');

        $japp->redirect('index.php?option='.APP_EXTENSION.'&task=payments',$msg);
        return true;
        
	}
	
	function togglePayment()
	{
		$payment=&JTheFactoryPaymentLib::getInstance();
        $payment->enablePaymentGateway();
        $japp=&JFactory::getApplication();
        $japp->redirect('index.php?option='.APP_EXTENSION.'&task=gateway_administrator',JText::_('ADS_ENABLE_TOGGLED'));
    	return true;
		
	}
	
	function deletePaylog(){
		$payment=&JTheFactoryPaymentLib::getInstance();
        $payment->deletePaylog();
        $japp=&JFactory::getApplication();
        $japp->redirect('index.php?option='.APP_EXTENSION.'&task=payments',JText::_('ADSMAN_PAYMENT_DELETED'));
		
	}
	
}