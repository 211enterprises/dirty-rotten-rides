<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/


class FactoryPricingsController{
	
	var $_db 	= null;
	
	
	function checkTask( $act )
	{
		$this->_db = &FactoryLayer::getDB();
		
		$method_name = $act;
	
		if ( method_exists($this,$method_name) ){
			require_once(dirname(__FILE__).DS."..".DS."tmpl".DS."pricings.toolbar.php");
			$this->$method_name();
			return true;
		}

		return false;
	}
	
    function PaymentItems()
    {
        $payment=&JTheFactoryPaymentLib::getInstance();
        $payment->showAvailablePaymentItems();
        return true;
    }
    function PaymentItemsConfig()
    {
		$payment=&JTheFactoryPaymentLib::getInstance();
    	$classname = 'price_'.JRequest::getVar('itemname','');

    	$payment->showPaymentItemConfig($classname);
    	return true;

    }

    function PaymentItemsConfigApply()
    {
        $payment=&JTheFactoryPaymentLib::getInstance();
        $classname = 'price_'.JRequest::getVar('itemname','');
        $payment->savePaymentItemConfig($classname);

        $japp = JFactory::getApplication();
        $japp->redirect('index.php?option='.APP_EXTENSION.'&task=paymentitems',JText::_('ADS_SETTINGS_SAVED'));
        return true;

    }

    function SavePaymentItemsConfig()
    {
		$payment=&JTheFactoryPaymentLib::getInstance();
    	$classname = 'price_'.JRequest::getVar('itemname','');
    	$payment->savePaymentItemConfig($classname);

        $japp = JFactory::getApplication();
        $japp->redirect('index.php?option='.APP_EXTENSION.'&task=paymentitems',JText::_('ADS_SETTINGS_SAVED'));
    	return true;
    }
    function Save_Payment_Item_Order()
    {
		$payment=&JTheFactoryPaymentLib::getInstance();
        $payment->savePaymentItemOrdering();
        $japp=&JFactory::getApplication();
        $japp->redirect('index.php?option='.APP_EXTENSION.'&task=paymentitems',JText::_('ADS_ORDER_SAVED'));
    	return true;

    }
    
    function Save_Packages_Order()
    {
		$payment=&JTheFactoryPaymentLib::getInstance();
        $payment->savePackagesOrdering();
        $japp=&JFactory::getApplication();
        $japp->redirect('index.php?option='.APP_EXTENSION.'&task=paymentitemsconfig&itemname=packages',JText::_('ADS_PACKAGES_ORDER_SAVED'));
    	return true;

    }
    function EnablePaymentItem()
    {
		$payment=&JTheFactoryPaymentLib::getInstance();
        $payment->enablePaymentItem();
        $japp=&JFactory::getApplication();
        $japp->redirect('index.php?option='.APP_EXTENSION.'&task=paymentitems',JText::_('ADS_ENABLE_TOGGLED'));
    	return true;

    }
	
    function freecredits() {
    	$payment=&JTheFactoryPaymentLib::getInstance();
    	$classname = 'price_'.JRequest::getVar('itemname','');

    	$payment->showFreeCreditsItem($classname);
    	return true;
    }
}