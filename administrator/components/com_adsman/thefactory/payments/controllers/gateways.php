<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/




class FactoryGatewaysController{
	
	var $_db 	= null;
	
	
	function checkTask( $task )
	{
		$this->_db = &FactoryLayer::getDB();
		
		$method_name = $task;
		
		if ( method_exists($this,$method_name) ){
			require_once(dirname(__FILE__).DS."..".DS."tmpl".DS."gateways.toolbar.php");
			$this->$method_name();
			return true;
		}

		return false;
	}
		
	function showpaymentconfig(){
		
		$payment=&JTheFactoryPaymentLib::getInstance();
    	$classname = JRequest::getVar('paymenttype','');
    	$pay_title = ucfirst(str_replace("_"," ",str_replace(".php","",$classname)));
		?>
				<fieldset class="adminform">
					<legend><?php echo JText::_( 'Payment Gateway '.$pay_title.' Configuration' ); ?></legend>
					<br />
					<br />
		<?php
    	$payment->showPaymentGatewayConfig($classname);
    	?>
    	</fieldset>
    	<?php	
        
	}
	
    function savepaymentconfig()
    {
		$payment    =&JTheFactoryPaymentLib::getInstance();
    	$classname  = JRequest::getVar('paymenttype','');
        $payment->savePaymentGatewayConfig($classname);
        $japp = JFactory::getApplication();
        $japp->redirect('index.php?option='.APP_EXTENSION.'&task=gateway_administrator',JText::_('ADS_SETTINGS_SAVED'));
    }
    
    function togglepayment()
    {
		$payment    =&JTheFactoryPaymentLib::getInstance();
        $payment->enablePaymentGateway();
        $japp = JFactory::getApplication();
        $japp->redirect('index.php?option='.APP_EXTENSION.'&task=gateway_administrator',JText::_('ADS_ENABLE_TOGGLED'));
    	return true;

    }
    
	function setdefaultpayment()
	{
		$payment=&JTheFactoryPaymentLib::getInstance();
    	$cid	= JRequest::getVar('cid', array(0), '', 'array');
    	$database = JFactory::getDbo();

        $database->setQuery("update ".PAYTABLE_PAYSYSTEMS." set isdefault=0 ");
        $database->query();
        
    	if ($cid[0]){
	        $database->setQuery("update ".PAYTABLE_PAYSYSTEMS." set isdefault=1 where classname='".$cid[0]."'");
	        $database->query();
	    }
	
        $japp = JFactory::getApplication();
        $japp->redirect('index.php?option='.APP_EXTENSION.'&task=gateway_administrator',JText::_('ADS_SETTINGS_SAVED'));
	}
}