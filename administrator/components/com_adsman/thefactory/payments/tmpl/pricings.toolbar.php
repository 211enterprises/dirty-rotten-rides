<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

$itemname = JRequest::getCmd("itemname",'','get');
?>

<table border="0" class="adminlist" >
	<thead>
	<tr>
		<!--<th align="left">
			<h2 style="text-align:left;">
				<img src="<?php echo F_ROOT_URI;?>administrator/components/com_adsman/img/toolbar/paymentitems.png" alt="Compulsory" border="0" style="vertical-align:middle;" />&nbsp;&nbsp;<?php echo JTF_PAYMENT_PRICINGS_TITLE;?>
			</h2>
		</th>
		<th valign="middle" align="right">-->
		<?php
		if($itemname!=""){
	
			$payment=&JTheFactoryPaymentLib::getInstance();
			$payment->loadAvailabeItems();
			$plugins = $payment->price_plugins;

            $checkArray = array('listing','featured_gold','featured_silver','featured_bronze','contact');
            if ( in_array($itemname,$checkArray ) ) {
            //if($itemname == 'listing') {
                JToolBarHelper::apply("paymentitemsconfigapply");
            }
			JToolBarHelper::save("savepaymentitemsconfig");
            JToolBarHelper::back();
			//JToolBarHelper::cancel("paymentitems");

			echo JText::_("ADSMAN_SELECT_PRICING");
		?>
				<select name="itemname" onchange="document.location='index.php?option=<?php echo APP_EXTENSION;?>&task=paymentitemsconfig&itemname='+this.value;">
    			<?php foreach($plugins as $pi =>$pitem){ ?>
					<option <?php if($itemname==$pitem->itemname){ ?> selected="selected" <?php } ?>" value="<?php echo $pitem->itemname;?>"><?php echo ucfirst($pitem->itemname);?></option>
				<?php } ?>
				</select>
		<?php 
		}
		?>
		</th>
	</tr>
	</thead>
</table>
