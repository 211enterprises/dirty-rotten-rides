<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

$task = JRequest::getCmd("task",'');

if($task=="gateway_administrator"){

	JToolBarHelper::makeDefault("setdefaultpayment");
} elseif ($task=="showpaymentconfig"){
	
	JToolBarHelper::save("savepaymentconfig");
	JToolBarHelper::cancel("gateway_administrator");
}

?>
<table border="0" class="adminlist">
	<thead>
	<tr>
		<th align="left">
		<h2 style="text-align:left;">
			<img src="<?php echo F_ROOT_URI;?>administrator/components/com_adsman/img/sections.png" alt="Compulsory" border="0" style="vertical-align:middle;" />&nbsp;&nbsp;<?php echo JTF_PAYMENT_GATEWAYS_TITLE;?></h2>
		</th>
	</tr>
	</thead>
</table>
