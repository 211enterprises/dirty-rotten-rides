<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

    $bar = JToolBar::getInstance('toolbar');
    $bar->appendButton( 'Link', 'preview', JTF_PAYMENT_PRICINGS_TITLE, "index.php?option=".APP_EXTENSION."&task=paymentitems",false );
	$bar->appendButton( 'Link', 'menus', JTF_PAYMENT_GATEWAYS_TITLE, "index.php?option=".APP_EXTENSION."&task=gateway_administrator",false );
?>

