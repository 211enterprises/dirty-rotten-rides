<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/


class JTheFactoryPayment_controller{
	
    /**
     * Added INIT Method to load all dependencies both on front & backend
     *
     * @since 13/01/2010 (dd/mm/YYYY)
     */
	function init(&$application){
		
		if($application){
			$Tapp=$application;
		}
		else	
			$Tapp=&JTheFactoryApp::getInstance(null,null);

		require_once( dirname(__FILE__).DS.'payments.lib.php');
		require_once( dirname(__FILE__).DS.'lang/default.php');
		
	}

	/**
	 * Backend Application Router
	 *
	 * @param $task
	 * @param $act
	 * @return bool
	 */
	function route(){
		
		require_once( dirname(__FILE__).DS.'payments.lib.php');
		require_once( dirname(__FILE__).DS.'lang/default.php');
		
		$task = JRequest::getVar("task","");
		//$act = & JRequest::getVar("act","");
		
		$gateways_tasks = array( "showpaymentconfig", "savepaymentconfig" ,"togglepayment" ,"setdefaultpayment" );
		$pricings_tasks = array( "paymentitems", "paymentitemsconfig" , "savepaymentitemsconfig" ,"paymentitemsconfigapply" , "save_payment_item_order" , "save_packages_order", "enablepaymentitem", "freecredits");
		$payments_tasks = array( "payments" , "new_payment" , "save_payment" ,"approve_payment" , "togglePayment" , "deletePaylog" );
		
		if(in_array($task, $gateways_tasks)){
			
			JLoader::register('FactoryGatewaysController', dirname(__FILE__).DS.'controllers'.DS.'gateways.php');
			$FC = new FactoryGatewaysController();
			return $FC->checkTask($task);
			
		}
		
		if(in_array($task, $pricings_tasks)){
			
			JLoader::register('FactoryPricingsController', dirname(__FILE__).DS.'controllers'.DS.'pricings.php');
			$FC = new FactoryPricingsController();
			return $FC->checkTask($task);
			
		}

		if(in_array($task, $payments_tasks)){
			JLoader::register('FactoryPaymentsController', dirname(__FILE__).DS.'controllers'.DS.'payments.php');
			$FC = new FactoryPaymentsController();
			return $FC->checkTask($task);
			
		}
		
	}
	
}

?>