<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/


JHtml::_('behavior.framework', true);
		

class JTheFactoryPaymentLib extends JObject
{
    var $gateways		= array();
    var $paymentitems	= array();
    var $plugin_dir		= null;
    var $pay_plugins	= array();
    var $price_plugins	= array();

    function __construct()
    {
		//$this->plugin_dir	= JPATH_COMPONENT_SITE.DS.'plugins';
        $this->plugin_dir	= JPATH_ROOT.DS.'components'.DS.'com_adsman'.DS.'plugins';
		$this->getAvailableGateways();
		$this->getAvailablePaymentItems();
    }
    
	function &getInstance()
	{
		static $instances;

		if (!isset( $instances ))
			$instances = new JTheFactoryPaymentLib();

		return $instances;
	}

	function getAvailableGateways()
	{
    	jimport('joomla.filesystem.folder');

    	$this->gateways	= JFolder::files($this->plugin_dir.DS.'payment','^pay_([^\.]*)\.php');
	}
	
	function getAvailablePaymentItems()
	{
    	jimport('joomla.filesystem.folder');
    	$this->paymentitems	= JFolder::files($this->plugin_dir.DS.'pricing','^price_([^\.]*)\.php');
	}
	
	function loadAvailabeGateways()
	{
    	jimport('joomla.filesystem.file');
    	$db	= JFactory::getDbo();
        for ($i=0; $i<count($this->gateways); $i++) {
        	
            $plugin	= JFile::stripExt($this->gateways[$i]);

    		JLoader::register($plugin,$this->plugin_dir.DS."payment".DS.$this->gateways[$i]);
            $this->pay_plugins[$plugin]	= new $plugin($db);
        }
	}
	
	function listEnabledGateways()
	{
    	jimport('joomla.filesystem.file');
    	$db		= JFactory::getDbo();
    	$arr	= array();
    	
        for	($i=0;$i<count($this->gateways);$i++) {
        	
            $plugin	= JFile::stripExt($this->gateways[$i]);
    		
            JLoader::register($plugin,$this->plugin_dir.DS."payment".DS.$this->gateways[$i]);
            $p	= new $plugin($db);
            if ($p->enabled) 
            	$arr[]	= $p;
        }

        return $arr;
	}
	
	function loadAvailabeItems()
	{
    	jimport('joomla.filesystem.file');
    	$db	= JFactory::getDbo();
    	
        for ($i=0; $i<count($this->paymentitems); $i++) {
        	
			$plugin	= JFile::stripExt($this->paymentitems[$i]);
			JLoader::register($plugin,$this->plugin_dir.DS."pricing".DS.$this->paymentitems[$i]);
    		
			$this->price_plugins[$plugin]	= new $plugin($db );
        }
	}
	
	function beginForm($task)
	{
	    JTheFactoryAdminHelper::beginAdminForm($task,null,'post',true,null);
	}
	
	function endForm()
	{
	    JTheFactoryAdminHelper::endAdminForm();
	}
	
	function showPaymentGatewayConfig($pluginname)
    {
        if (isset($this->pay_plugins[$pluginname])) {
            $obj	= $this->pay_plugins[$pluginname];
        } else {
            $db 	= JFactory::getDbo();
    		JLoader::register($pluginname,$this->plugin_dir.DS."payment".DS.$pluginname.".php");
            $obj	= new $pluginname($db);
        }

        $obj->show_admin_config();
    }
    
    function savePaymentGatewayConfig($pluginname)
    {
        if (isset($this->pay_plugins[$pluginname])) {
            $obj	= $this->pay_plugins[$pluginname];
        } else {
            $db 	= JFactory::getDbo();
    		JLoader::register($pluginname,$this->plugin_dir.DS."payment".DS.$pluginname.".php");
            $obj	= new $pluginname($db);
        }

        $obj->save_admin_config();
    }
	
    function showAvailablePaymentItems()
	{
       
		if (!count($this->price_plugins)) 
        	$this->loadAvailabeItems();

        $this->beginForm('paymentitems');
        $rows	= array();
        
        $count_plugins = count( $this->price_plugins ) - 1;

        $header_row[]	= array("width"=>"5%","class"=>"left","th"=>'<input type="checkbox" name="toggle" value="" onclick="checkAll('.$count_plugins.');" disabled />');
        $th		= JText::_("Ordering").'&nbsp;<a href="javascript: void(0);" onclick="submitbutton(\'save_payment_item_order\');">Save</a>';

        $header_row[]	= array("width"=>"5%", "class"=>"left", "th"=>JText::_("ADS_ENABLED"));
        $header_row[]	= array("width"=>"10%", "class"=>"left","th"=>JText::_("ADS_PAYMENT_ITEM"));
        $header_row[]	= array("width"=>"*%", "class"=>"left", "th"=>JText::_("ADSMAN_DESCRIPTION"));
        $header_row[]	= array("width"=>"5%","th"=>JText::_("ADS_PAYMENT_ITEM_PRICE"));
        $header_row[]	= array("width"=>"5%", "class"=>"left", "th"=>JText::_("ADS_PAYMENT_ITEM_CURRENCY"));
        
        $i = 0;
        if (count($this->price_plugins))
            foreach ($this->price_plugins as $pay_list)
            {

                $link	= JURI::root()."administrator/index.php?option=".APP_EXTENSION."&task=paymentitemsconfig&itemname=".$pay_list->itemname;
                $img_enabled	= ($pay_list->enabled) ? "tick.png" : "publish_x.png";

   				
   				if ($pay_list->itemname == 'maincredit' || $pay_list->itemname == 'packages') {
                       //array("class"=>"center","td"=>"JText::_('basic item')");
    				$rows[$i][]	= array("class"=>"left","td"=>JText::_('ADS_BASIC_ITEM'));
                } else {
    				$rows[$i][]	= JHTML::_('grid.id', $i+1, $pay_list->itemname);
                       //array("class"=>"center","td"=>"JHTML::_('grid.id', $i+1, $pay_list->itemname)");
                }
    			
    			if ($pay_list->itemname == 'packages' || $pay_list->itemname == 'maincredit') {
    				$rows[$i][] = array("class"=>"center","td"=>JText::_('ADS_ENABLED'));
    			} else {
    				$rows[$i][]	= array("class"=>"center","td"=>"<a href=\"javascript: void(0);\" onClick=\"return listItemTask('cb".($i+1)."','enablepaymentitem')\">".
	    				JHTML::_('image','admin/'.$img_enabled, "Toggle", array('border' => 0), true).
        			"</a>");
    			}
    			$rows[$i][]	= array("class"=>"left","td"=>"<a href='".$link."' title='".JText::_('ADS_CONFIGURE_PAYMENT_ITEM')."'>".JText::_($pay_list->itemname)."</a>");
    			$rows[$i][]	= array("class"=>"left","td"=>"$pay_list->classdescription");
    			$rows[$i][]	= array("class"=>"right","td"=>"$pay_list->price");
    			$rows[$i][]	= array("class"=>"left","td"=>"$pay_list->currency");

    			$i++;
            }

        JTheFactoryAdminHelper::showAdminTableList($header_row,$rows,null,JText::_('ADS_PAYMENT_ITEMS_CONFIG'));
        $this->endForm();

	}
	
	function showPaymentItemConfig($itemname)
	{
        if ( isset($this->price_plugins[$itemname] ) ){
            $obj	= $this->price_plugins[$itemname];
        }	else {
            $db 	=& JFactory::getDBO();
    		JLoader::register($itemname,$this->plugin_dir.DS."pricing".DS.$itemname.".php");
            $obj	= new $itemname($db);
        }

        $obj->show_admin_config();

	}
	
	function showFreeCreditsItem($itemname) {
		if ( isset($this->price_plugins[$itemname] ) ){
            $obj	= $this->price_plugins[$itemname];
        }	else {
            $db 	=& JFactory::getDBO();
    		JLoader::register($itemname,$this->plugin_dir.DS."pricing".DS.$itemname.".php");
            $obj	= new $itemname($db);
        }

        $obj->show_free_credits();

	}
	
	function savePaymentItemConfig($itemname)
	{
		if (isset($this->price_plugins[$itemname])){
            $obj	= $this->price_plugins[$itemname];
        } else {
            $db 	= JFactory::getDbo();
    		JLoader::register($itemname,$this->plugin_dir.DS."pricing".DS.$itemname.".php");
            $obj	= new $itemname($db);
        }

        $obj->save_admin_config();
	}
    
	function savePaymentItemOrdering() {

        $database =& JFactory::getDBO();
        $varnames	= array_keys($_REQUEST);
        
        for ($i=0;$i<count($varnames);$i++) {
            if (substr($varnames[$i],0,6) == 'order-') {
                $neworder	= $_REQUEST[$varnames[$i]];
                $itemname	= substr($varnames[$i],6);
                
                $database->setQuery("UPDATE ".PAYTABLE_PRICING." SET ordering='$neworder' WHERE itemname='$itemname'");
                $database->query();
            }
        }
    }
    
    function savePackagesOrdering() {
    	$database =& JFactory::getDBO();
        $varnames	= array_keys($_REQUEST);
        
        for ($i=0; $i<count($varnames); $i++) {
            if (substr($varnames[$i],0,6) == 'order-') {
            	
                $neworder	= $_REQUEST[$varnames[$i]];
                $package_id	= substr($varnames[$i],6);
                 
                $database->setQuery("UPDATE `#__ads_packages` SET ordering='$neworder' WHERE id='$package_id'");
                $database->query();
            }
        }
    }

    function enablePaymentItem() {

    	$cid	= JRequest::getVar('cid', array(0), '', 'array');
    	$database =& JFactory::getDBO();
        for ($i=0; $i<count($cid); $i++) {
    		$database->setQuery("UPDATE ".PAYTABLE_PRICING." SET enabled=1-enabled WHERE itemname='".$cid[$i]."'");
    		$database->query();
        }
    }
    
    function enablePaymentGateway() {

    	$cid	= JRequest::getVar('cid', array(0), '', 'array');
    	$database = JFactory::getDbo();

        for ($i=0;$i<count($cid);$i++) {
    		$database->setQuery("UPDATE ".PAYTABLE_PAYSYSTEMS." SET enabled=1-enabled WHERE classname='".$cid[$i]."'");
    		$database->query();
        }
    }
    
    function showPaymentLog()
    {
		$app	= JFactory::getApplication();

		$limit				= $app->getUserStateFromRequest('global.list.limit','limit',$app->getCfg('list_limit'), 'int');
		$limitstart			= $app->getUserStateFromRequest(APP_EXTENSION.".paylog.limitstart",'limitstart',0,'int');
		
		// In case limit has been changed, adjust limitstart accordingly
		$limitstart = ( $limit != 0 ? (floor($limitstart / $limit) * $limit) : 0 );
		
		$filter_keyword   	= $app->getUserStateFromRequest(APP_EXTENSION.".paylog.filter_keyword",'filter_keyword','' ,'string');
		$filter_state     	= $app->getUserStateFromRequest(APP_EXTENSION.".paylog.filter_state",'filter_state','','string');

		$database	=& JFactory::getDBO();

    	$where	= "";
    	$w = array();

        if ($filter_state) 
        	$w[]	= "  p.status='$filter_state' ";
        	
        if ($filter_keyword) 
        	$w[]	= " (u.username like '%$filter_keyword%' OR p.refnumber LIKE '%$filter_keyword%' OR invoice LIKE '%$filter_keyword%')";
        	
        if (count($w)) 
        	$where	= " WHERE ".implode(" AND ",$w);

    	$query = "SELECT COUNT(*)"
    	   . "\n FROM ".PAYTABLE_PAYLOG." p
           LEFT JOIN #__users u ON p.userid=u.id
    	   $where ";
    	   
    	$database->setQuery( $query );
    	$total = $database->loadResult();


    	$database->setQuery("SELECT p.*,u.username, a.title as object_item 
    	   FROM ".PAYTABLE_PAYLOG." AS p
    	   LEFT JOIN #__users u ON p.userid=u.id 
    	   LEFT JOIN ".PAYTABLE_PAYITEMS_TABLE." as a ON p.object_id = a.id 
    	   $where 
    	   ORDER BY date DESC",$limitstart,$limit);
        
    	$pay_list=$database->loadObjectList();

    	$filter[] = JHTML::_('select.option', '', JText::_('ADS_ALL'), 'text', 'value' );
    	$filter[] = JHTML::_('select.option', 'ok', JText::_('ADS_PAYMENT_OK'), 'text', 'value' );
    	$filter[] = JHTML::_('select.option', 'manual_check', JText::_('ADS_MANUAL_CHECK'), 'text', 'value' );
    	$filter[] = JHTML::_('select.option', 'cancelled', JText::_('ADS_CANCELLED'), 'text', 'value' );
    	$filter[] = JHTML::_('select.option', 'refunded', JText::_('ADS_REFUNDED'), 'text', 'value' );
    	$filter[] = JHTML::_('select.option', 'error',JText::_('ADS_ERROR'), 'text', 'value' );

    	//ok,error,manual_check,cancelled,refunded
    	jimport('joomla.html.pagination');
    	$pageNav = new JPagination( $total, $limitstart, $limit );

		$lists['filter_state']		= JHTML::_('select.genericlist', $filter, 'filter_state', 'class="inputbox" size="1" onchange="document.adminForm.submit();"', 'text', 'value', $filter_state );
		$lists['$filter_keyword']	= ' <input name="filter_keyword" size="80" style="float:auto;" id="search" type="text" value="'.$filter_keyword.'">';

        $rows = array();
        //$header_row[] = array("width"=>"10","th"=>"#");
        $header_row[] = array("width"=>"20","align"=>"left","th"=>'<input type="checkbox" name="toggle" value="" onclick="checkAll('.count( $pay_list ).');" />');
        $header_row[] = array("width"=>"15%","th"=>JText::_("ADS_USER"));
        $header_row[] = array("width"=>"10%","th"=>JText::_("ADS_DATE"));
        $header_row[] = array("width"=>"*%","th"=>JText::_("ADS_ITEM"));
        $header_row[] = array("width"=>"5%","th"=>JText::_("ADS_PAYMENT_ITEM"));
        $header_row[] = array("width"=>"7%","th"=>JText::_("ADS_PAYMENT_ITEM_AMOUNT"));
        $header_row[] = array("width"=>"5%","th"=>JText::_("ADS_PAYMENT_ITEM_CURRENCY"));
        $header_row[] = array("width"=>"5%","th"=>JText::_("ADS_PAYMENT_ITEM_REFNO"));
        $header_row[] = array("width"=>"5%","th"=>JText::_("ADS_PAYMENT_ITEM_INVOICE_NO"));
        $header_row[] = array("width"=>"5%","th"=>JText::_("IPN"));
        $header_row[] = array("width"=>"5%","th"=>JText::_("ADS_STATUS"));

        $i = 0;
        if (count($pay_list))
            foreach ($pay_list as $row)
            {
    			//$rows[$i][]	= $i+1;
    			$rows[$i][]	= JHTML::_('grid.id', $i+1, $row->id);
    			$rows[$i][]	= $row->username;
    			$rows[$i][]	= $row->date;
    			
    			if ($row->itemname!="price_contact")
    				$rows[$i][]	= $row->object_item;
    			else	
    				$rows[$i][]	= "";
    			
    			$pricing_title = $row->itemname;
    			if ($row->itemname == "packages") {
    				
    				if ( $row->var_name1=="package_id" ) {
    					
    					$database->setQuery("SELECT name FROM ".PAYTABLE_PACKAGES." WHERE id = {$row->var_value1}");
    					$pname = $database->loadResult();
	    				$pricing_title .= " ( {$pname} )";
    				}
    				
    			}
    			$rows[$i][]	= $pricing_title;
    			   			    				
    			$rows[$i][]	= number_format($row->amount,2,'.','');;
    			$rows[$i][]	= $row->currency;
    			$rows[$i][]	= $row->refnumber;
    			$rows[$i][]	= $row->invoice;
    			$rows[$i][]	= $row->ipn_ip;
    			$rows[$i][]	= $row->status;

    			$i++;
            }

        //$this->beginForm('payments');
        ?>
        <form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
            <input type="hidden" name="option" value="<?php echo APP_EXTENSION;?>" />
            <input type="hidden" name="task" value="payments" />
            <input type="hidden" name="boxchecked" value="0" />
        <?php
		$filter_controlls	=	"<button onclick=\"this.form.submit();\">".JText::_( 'ADS_GO' )."</button>";
		$filter_controlls	.=	"<button onclick=\"document.getElementById('search').value='';this.form.submit();\">".JText::_( 'ADS_RESET' )."</button>";

		JTheFactoryAdminHelper::showAdminTableList($header_row,$rows,$pageNav,null, $lists['$filter_keyword'].'&nbsp;&nbsp;'.$lists['filter_state'].'&nbsp'.$filter_controlls);

        echo '</form>';
        //$this->endForm();
    }

    function showAddNewPayment()
    {
    	$database = JFactory::getDBO();

        $filter = array('packages' => 'packages');
        $lists['itemname']	= JHTML::_('select.genericlist', $filter, 'itemname', 'class="inputbox" size="1"', 'text', 'value' );
				
		$database->setQuery("SELECT * FROM ".PAYTABLE_PACKAGES." WHERE status=1 AND price >= 0 ORDER BY ordering ASC ");
		$packages  = $database->loadObjectList();
		
		if ($packages) {
			$first_index = key($packages);
			$first_package_id = $packages[$first_index]->id;

			$options = array();
	        
	        foreach ($packages as $k => $package ) {
				$options[] = JHTML::_('select.option',$package->id,$package->name);
	        }
			$lists['packages'] = JHTML::_('select.genericlist', $options, "package_id",'class="inputbox" ','value', 'text', $first_package_id);
				         
	        $database->setQuery("SELECT username as text,id as value FROM #__users WHERE block=0");
	        $filter	= $database->loadObjectList();
	        $lists['usernames']	= JHTML::_('select.genericlist', $filter, 'usernames', 'class="inputbox" size="1"', 'value', 'text' );
	
	        $this->htmlAddNewPayment($lists);
		}
    }

    function htmlAddNewPayment(&$lists)
    {
       ?>
         <table width="100%">
         <form action="index.php" method="post" name="adminForm">
          <tr>
           <td colspan="2">
        	<table class="adminheading">
        	 <tr>
        	  <th width="40%"><?php echo JText::_('ADS_NEW_PAYMENT_USER'); ?></th>
        	  </tr>
        	 </table>
        	</td>
           </tr>
           <tr>
        	<td colspan="2">
        	 <table class="adminlist">
        	  <tr>
            	  <td width="10%"><?php echo JText::_('ADS_PAYMENT_ITEM');?></td>
            	  <td><?php echo $lists['itemname'];?></td>
        	  </tr>
        	 <tr>
            	  <td><?php echo JText::_('ADS_PACKAGES');?></td>
            	  <td><?php echo $lists['packages'];?></td>
        	  </tr>
        	  <tr>
            	  <td><?php echo JText::_('ADS_USERNAME');?></td>
            	  <td><?php echo $lists['usernames'];?></td>
        	  </tr>
        	  <tr>
            	  <td><?php echo JText::_("ADS_NOTIFY_USER");?></td>
            	  <td><input type="checkbox" name="notify_user" value="1"></td>
        	  </tr>
        	 </table>
        	</td>
           </tr>
          <input type="hidden" name="option" value="<?php echo APP_EXTENSION;?>" />
          <input type="hidden" name="task" value="savepayment" />
         </form>
         </table>
        <?php
    }
    
    function saveAddNewPayment()
    {
		$app	= JFactory::getApplication();
		require_once($this->plugin_dir.DS.'payment'.DS."payment_log.php");     
			
    	$db = JFactory::getDBO();

        $itemname	= JRequest::getVar( 'itemname', '' );
        $packageid	= JRequest::getVar( 'package_id', 0 );
        $user		= JRequest::getVar( 'usernames', 0 );
        $notify_user= JRequest::getVar( 'notify_user', 0 );
       
        $paylog	= new JTheFactoryPaymentLog($db);
        $paylog->id			= null;
               
        $price_classname	= "price_$itemname";
                
        if (file_exists(ADS_COMPONENT_PATH."/plugins/pricing/$price_classname.php"))
        {
            require_once(ADS_COMPONENT_PATH."/plugins/pricing/$price_classname.php");
            
            $price_class	= new $price_classname($db);
            if ($itemname == 'packages') 
            {
            	$package_id = isset($paylog->var_value1) ? $paylog->var_value1 : $packageid;
				$package = price_packages_DBHelper::getPackage((int)$package_id);
							
				$credits = $package->credits;
        
		        $paylog->amount 	= $package->price;
		        $paylog->currency	= $package->currency;
		        $paylog->date		= date('y-m-d H:i');
		        $paylog->ipn_ip		= $_SERVER['REMOTE_ADDR'];
		        $paylog->refnumber	= 'admin';
		        $paylog->userid		= $user;
		        $paylog->itemname	= $itemname;
		        $paylog->status		= 'ok';
		       		 
		        $paylog->var_name1 = 'package_id';
		        $paylog->var_value1 = $package_id;
		        	               
		        $paylog->store();
	            
		         // how can you identify a payment with a user!!!?!?!
		        $price_class->acceptOrder($paylog, $credits);
		            
            }
        }
        
        /**
         * Mail sending
         */
        if ($notify_user == 1) {
	        $juser = new JUser();
	        $juser->load($user);
	        
			$email 	= $juser->email;
			$subj 	= PAYTABLE_PAYMENT_SUBJECT;
			$html_mail = PAYTABLE_PAYMENT_CONTENT;
			$html_mail = str_replace("%USERNAME%", $juser->username, $html_mail);
			$html_mail = str_replace("%AMOUNT%", $paylog->amount, $html_mail);
			$html_mail = str_replace("%CURRENCY%", $paylog->currency, $html_mail);
			$html_mail = str_replace("%CREDITS%", $credits, $html_mail);
			
			$mail_from = $app->getCfg('mailfrom');
			$site_name = $app->getCfg('sitename');
			
			JUTility::sendMail($mail_from, $site_name , $email, $subj, $html_mail, true);
        }
    }
    
    
    function approvePayment()
    {
		require_once($this->plugin_dir.DS.'payment'.DS."payment_log.php");     
    	
		$database = & JFactory::getDBO();
        $id = intval( JRequest::getVar( 'id', 0 ) );

        if (!$id){
            $cid = JRequest::getVar('cid', array() ) ;
        }

        if(!$id && (!isset($cid) || count($cid) == 0 )){
        	echo "Select a pay log!";
        	exit;
        }

        $paylog	= new JTheFactoryPaymentLog($database);

        if (isset($id) && $id > 0 ) {
	        if (!$paylog->load($id)) 
	        	return false;
	        	
	        if ($paylog->status == 'ok') 
	        	return true;
	
	        $price_classname	= "price_$paylog->itemname";

            $database->setQuery("SELECT credits_no FROM #__ads_user_credits WHERE userid='$paylog->userid' ");
            $aduser_credits = $database->LoadResult();


	        if (file_exists(ADS_COMPONENT_PATH."/plugins/pricing/$price_classname.php"))
            {
                require_once(ADS_COMPONENT_PATH."/plugins/pricing/$price_classname.php");
                $db =& JFactory::getDBO();
                
                $price_class	= new $price_classname($db);
                                              
                if ($paylog->itemname == 'packages') 
                {
                	
                	$package_id = $paylog->var_value1;
					$package = price_packages_DBHelper::getPackage((int)$package_id);
										
					$credits = $package->credits;
	         		$packages_no = $paylog->amount / $package->price;
					
			        $quantity = number_format($packages_no * $credits,2);
			        
			        if ($paylog->var_value2 != '')
						$total_price_ad = $paylog->var_value2;  
					else 
						$total_price_ad = $quantity; 
				
					if ($paylog->var_value3 != '')
						$featured_ad	= $paylog->var_value3;
					else 
						$featured_ad	= 'none';	

                    $total_credits = $aduser_credits + $quantity;

                    if ($total_credits >= $total_price_ad) {
					    $price_class->acceptOrder($paylog, $quantity,$total_price_ad,$featured_ad);
	    		        $paylog->status	= 'ok';
            	        $paylog->store();
                    } else {
                         return false;
                    }
                } else {
                	$db =& JFactory::getDBO();
                	$db->setQuery("SELECT * FROM #__ads_pricing WHERE itemname='maincredit' ");
                	
					$row = $db->LoadObject();
					
                	$maincredit_price = $row->price;
                	//$currency = $row->currency;
                	if ($paylog->var_value3 != '')
						$featured_ad	= $paylog->var_value3;
					else 
						$featured_ad	= 'none';	
                	
                	$quantity = number_format($paylog->amount/$maincredit_price,2);
					
					if ($paylog->var_value2 != '')
						$total_price_ad = $paylog->var_value2;  
					else 
						$total_price_ad = $quantity;

                    $total_credits = $aduser_credits + $quantity;

                    if ($total_credits >= $total_price_ad) {
                	    $price_class->acceptOrder($paylog, $quantity,$total_price_ad,$featured_ad);

			            $paylog->status	= 'ok';
			            $paylog->store();
                    } else {
                        return false;
                    }
                }
             
            }
        }	elseif (isset($cid) && count($cid)>0 ) {
        		foreach ($cid as $id) 
        		{
		        	$paylog->load($id);
			        $price_classname	= "price_$paylog->itemname";

                    $database->setQuery("SELECT credits_no FROM #__ads_user_credits WHERE userid='$paylog->userid' ");
        	        $aduser_credits = $database->LoadResult();

			        if (file_exists(ADS_COMPONENT_PATH."/plugins/pricing/$price_classname.php"))
		            {
		                require_once(ADS_COMPONENT_PATH."/plugins/pricing/$price_classname.php");
		                $db =& JFactory::getDBO();
		                
		                $price_class	= new $price_classname($db);
		                if ($paylog->itemname == 'packages') {
	                	
		                	$package_id = $paylog->var_value1;
							$package = price_packages_DBHelper::getPackage((int)$package_id);
												
							$credits = $package->credits;
			         		$packages_no = $paylog->amount / $package->price;
							
					        $quantity = number_format($packages_no * $credits,2);
					        
					        if ($paylog->var_value2 != '')
								$total_price_ad = $paylog->var_value2;  
							else 
								$total_price_ad = $quantity;
						
							if ($paylog->var_value3 != '')
								$featured_ad	= $paylog->var_value3;
							else 
								$featured_ad	= 'none';

                            $total_credits = $aduser_credits + $quantity;

                            if ($total_credits >= $total_price_ad) {
    							$price_class->acceptOrder($paylog, $quantity,$total_price_ad,$featured_ad);

					            $paylog->status	= 'ok';
					            $paylog->store();
                            } else {
                                return false;
                            }
		                } else {
                	
		                	$db =& JFactory::getDBO();
		                	$db->setQuery("SELECT * FROM #__ads_pricing WHERE itemname='maincredit' ");
		                	
							$row = $db->LoadObject();
							
		                	$maincredit_price = $row->price;
		                	//$currency = $row->currency;
							
		                	if ($paylog->var_value3 != '')
								$featured_ad	= $paylog->var_value3;
							else 
								$featured_ad	= 'none';

							$quantity = number_format($paylog->amount/$maincredit_price,2);	

							if ($paylog->var_value2 != '')
								$total_price_ad = $paylog->var_value2;  
							else 
								$total_price_ad = $quantity;

                            $total_credits = $aduser_credits + $quantity;

                            if ($total_credits >= $total_price_ad) {
                                $price_class->acceptOrder($paylog, $quantity,$total_price_ad,$featured_ad);

                                $paylog->status	= 'ok';
                                $paylog->store();
                            } else {
                                return false;
                            }
				          
		                }
		            }
        		}
        }
        
        return true;
    }
    
    function deletePaylog(){
		$app	= JFactory::getApplication();
    	
		require_once($this->plugin_dir.DS.'payment'.DS."payment_log.php");     
    	
		$database 	= & JFactory::getDBO();
		$cid 		= JRequest::getVar('cid', array() ) ;
		$paylog		= new JTheFactoryPaymentLog($database);
        
		if ( is_array($cid) && count($cid)>0 )
        {
        	foreach ($cid as $ci) {
				if ($paylog->load($ci)) 
						$paylog->delete();
        	}
        }
        
        return true;
    }

}

?>