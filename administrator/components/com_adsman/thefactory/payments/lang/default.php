<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

define("JTF_PAYMENT_GATEWAYS_TITLE","Payment Gateways");
define("JTF_PAYMENT_PAYMENTS_TITLE","Payment History");
define("JTF_PAYMENT_PRICINGS_TITLE","Payment Items");


