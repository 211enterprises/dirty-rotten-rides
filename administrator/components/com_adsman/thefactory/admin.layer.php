<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/


// @hash
define("F_DS", DIRECTORY_SEPARATOR);

// TO DO replace with Aplication Constants
define("F_ROOT_DIR", JPATH_ROOT);
define("F_ROOT_URI", JURI::root());


class FactoryLayer{
	
	/**
	 * Returns database curent Object   
	 *
	 */
	function &getDB(){

		$db = JFactory::getDbo();
	
		return $db;
	}
	
	/**
	 * Returns current user logged 
	 *
	 */
	function &getUser(){
		$my = &JFactory::getUser(); 
		return $my;
	}
	
	
	/**
	 * Requires, instantiates and returns refference to pagination object 
	 * 
	 **/
	 function initPagination($total, $limitstart, $limit){
		jimport('joomla.html.pagination');
		return new JPagination($total, $limitstart, $limit);
	 }
	 
	 function getRequest(&$arrName, $name, $default=null){
	 	
	 	$tmp = &JRequest::getVar($name,$default);
	
	 	return $tmp;  
	 }
	 
	 function redirect($uri, $msg=null){
	 	
		$mainframe = & JFactory::getApplication();
	 	$mainframe->redirect($uri,$msg);

	 }

}

/**
 * 
 * Factory Framework HTML Methods
 * Joomla 1.6.x
 * 
*/
class FactoryHTML{
	/**
	 * 
	 * Calls Current Framework select list 
	 * TO DO switch $attributes with $filter_value
	 * 
	*/
	function selectGenericList($options, $control_name, $filter_value, $attributes="", $value="value", $text="text" )
	{
		$opts = array();
		
		if (count($options))
			foreach($options as $i => $o){
				$opts[]=JHTML::_('select.option', $i, $o);
			}
				
		if($opts)	
			return JHTML::_( 'select.genericlist', $opts, $control_name, $attributes, $value, $text,$filter_value );
		else 
			return "";
		
	}
	
	function selectBooleanList($tag_name, $tag_attribs, $selected, $yes=null, $no=null){
		return JHTML::_("select.booleanlist",$tag_name, $tag_attribs, $selected, $yes=JText::_("ADS_YES"), $no=JText::_("ADS_NO") );
	}
	
	/**
	 * Calls current Framework backend tooltip method 
	 *
	 */
	function tooltip($t){
		JHTML::_('behavior.tooltip'); //load the tooltip behavior
		return JHTML::tooltip($t);
	}
	
	
}

?>