<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/


define("JTHEF_POSITION", "Position"); 
define("JTHEF_DESCRIPTION", "Description"); 
define("JTHEF_PREVIEW", "Preview"); 
define("JTHEF_TEMPLATE", "Template"); 
 


/**
 * Positions Manager 
 * 
**/
define("JTHEF_POSITIONS_CPANEL", " <a href='index.php?option=com_adsman&task=fposition_panel'>Positions</a>");
define("JTHEF_POSITIONS", " Positions");

define("JTHEF_POSITIONS_TITLE", " Position Definitions Manager");
define("JTHEF_POSITIONS_SUBTITLE", "Custom Fields Step2: Generate Custom Fields Syntax for the Smarty templates");
define("JTHEF_POSITIONS_TEMPLATE_SYNTAX", "Template Syntax");

define("JTHEF_TEMPLATES_TITLE", " Templates Manager");
define("JTHEF_TEMPLATES_SUBTITLE", "Administrate custom fields display positions. ");

define("JTHEF_POSFIELDS_TITLE", " Fields display ");
define("JTHEF_POSFIELDS_SUBTITLE", "Custom Fields Step3: Assign custom fields to enabled template positions in order to display them");

define("JTHEF_FIELD_POSITIONING", "Field Positioning");
define("JTHEF_POSITION_MANAGER", "Positions Manager");

define("JTHEF_NEW_PREVIEW", "Preview Template");
define("JTHEF_NEW_PREVIEW_TITLE", "Preview Template - The positions in database must exist in the t_details_add.tpl file ! Use the Code generarator for each position to get the snippet to put in the template.");

// Position Manager
define("JTHEF_NEW_POSITION", "New Position");
define("JTHEF_NEW_POSITION_TITLE", "Create New Position in Database");

define("JTHEF_NEW_POSITION_HELP", 
" 
 &nbsp;&nbsp;&nbsp;A <strong>'Position'</strong> in a template is a portion where custom fields created in the component<br />
 can be displayed.<br />
 &nbsp;&nbsp;&nbsp;After a position created*, you can assign the custom fields to it in the '".JTHEF_FIELD_POSITIONING."'<br />
 menu in this section. The fields will appear in the position in the order specified in the Custom<br />
 Fields Manager.<br />
 <br /><br /><br />
 * To create a position there are two steps to follow:<br />
 	1) create the position record in the '".JTHEF_POSITION_MANAGER.". by specifing title, unique name, some description and
	the 'looks' of a custom	field item in that position by customizing the way %LABEL% and %VALUE% are displayed.<br />
	<br />
	2) get the code from the new created position details page copy and <strong>paste it</strong><br />
	in the smarty templates where ever you like!<br />
	<br /><br />
	That's it! You can assign fields to appear in that position in the classified details page. <br />
	Hit '".JTHEF_NEW_PREVIEW."' to see a glance of the position! <br />
	<br />
");

define("JTHEF_PMAN_TITLE_DESCR", "Position generic title");
 
define("JTHEF_PMAN_NAME", "Unique Name"); 
define("JTHEF_PMAN_NAME_HELP", "Use alpha-numerical string"); 
define("JTHEF_PMAN_NAME_DESCR", "Position ID in templating ( call positionin template by this name ) "); 
 
define("JTHEF_PMAN_DESCR", "Position description"); 
define("JTHEF_PMAN_DESCR_HELP", "Informative use only"); 
define("JTHEF_PMAN_DESCR_DESCR", "Position Info"); 

define("JTHEF_PMAN_ITEMHTML", "Position item"); 
define("JTHEF_PMAN_ITEMHTML_HELP", "HTML code for positioning the name and value of a cutom field while listing in this position"); 
define("JTHEF_PMAN_ITEMHTML_DESCR", "Position custom layout of a field to be listed in this position.<br />
Use <br /> \"%LABEL%\" (field title)
and \"%VALUE%\"(field html value) <br /> to position the custom fields title and value. "); 
// END POSITIONS Manager

define("JTHEF_PMAN_TPL", "Position Template"); 
define("JTHEF_PMAN_TPL_HELP", "Position Template ( *.tpl file) where it is located  "); 
define("JTHEF_PMAN_TPL_DESCR", "The component template ( *.tpl file) where the position is located."); 

define("JTHEF_VERIFIED", "Verified");
define("JTHEF_VERIFIED_HELP", "Verified if found in template");
define("JTHEF_VERIFIED_DESC", "Verified 'Yes' if found in template. <br />Otherwise 'No' - forgotten to be added in the template? Use the 'Get code' button to get the snippet you must include in the template.");


define("", "");

/**
 * Custom Fields 
 * 
 **/
 
?>