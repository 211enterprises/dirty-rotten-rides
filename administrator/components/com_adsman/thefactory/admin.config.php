<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

class JTheFactoryConfig extends JObject {
	
    var $_options_array	= array();
	var $_raw_options	= array();
	var $_checkboxes	= array();
	var $optionfile		= null;

	function __construct(){
	    /*@var $app JTheFactoryApp*/
        $Tapp = JTheFactoryApp::getInstance();

        $this->_options_array	= $Tapp->getSection('extension-options');
        foreach ($this->_options_array as $option=>$default_val)
        {
            $opt_sett	= $Tapp->getSection($option);
            
            if ( isset($opt_sett['israw']) && $opt_sett['israw'] ) 
            	$this->_raw_options[]=$option;
            if ($opt_sett['type']=='check') 
            	$this->_checkboxes[]=$option;
        }
        $this->optionfile	= JPATH_COMPONENT_SITE.DS.$Tapp->getIniValue('option_file');
	}
	
	function &getInstance()
	{
		static $instances;

		if (!isset( $instances ))
			$instances = new JTheFactoryConfig();

		return $instances;
	}

	function show_Text($option)
	{
	    echo '<td width="350">';
	    echo JText::_($option['description']),'&nbsp';
	    if ($option['help']){
			echo JHTML::_('tooltip',JText::_($option['help']));
		}
	    echo ':';
	    echo '</td>';
	    echo '<td>';
	    $current_value=defined($option['name'])?constant($option['name']):$option['default'];
	    echo '<input name="'.(isset($option['name'])?$option['name']:'').'" type="text" value="'.$current_value.'" size="'.(isset($option['size'])?$option['size']:'').'" '.(isset($option['attributes'])?$option['attributes']:'').'>';
	    echo '</td>';

	}
	function show_Check($option)
	{
	    echo '<td width="350">';
	    echo JText::_($option['description']),'&nbsp';
	    if ($option['help']){
	    	echo JHTML::_('tooltip',JText::_($option['help']));
	    }
	    echo ':';
	    echo '</td>';
	    echo '<td>';
	    $current_value=defined($option['name'])?constant($option['name']):$option['default'];
	    if ($current_value==$option['default']) $checked='checked';

	    echo '<input name="'.(isset($option['name'])?$option['name']:'').'" type="checkbox" value="'.(isset($option['default'])?$option['default']:'').'" size="'.(isset($option['size'])?$option['size']:'').'" '.(isset($option['attributes'])?$option['attributes']:'').' '.(isset($checked)?$checked:'').'>';
	    echo "</td>";

	}
	function show_Radio($option)
	{
	    echo "<td width='350'>";
	    echo JText::_($option['description']),'&nbsp';
	    if ($option['help']) echo JHTML::_('tooltip',JText::_($option['help']));
	    echo ':';
	    echo "</td>";
	    echo "<td>";

	    $current_value=defined($option['name'])?constant($option['name']):$option['default'];
	    $sepp=empty($option['sepparator'])?'%':$option['sepparator'];
	    $select_options_values=explode($sepp,$option['options']);
	    $select_options=explode($sepp,$option['options_description']);

	    for ($i=0;$i<count($select_options_values);$i++){
	        if (empty($select_options[$i])){
				$desc=$select_options_values[$i];
			}
	        else{
	        	$desc=$select_options[$i];
	        }
	        if($select_options_values[$i]==$current_value){
	        	$checked=' checked="" ';
	        }
	        else{
	        	$checked='';
	        }
	        echo "<input type='radio' name='{$option['name']}' value='{$select_options_values[$i]}' size='{$option['size']}' {$option['attributes']} $checked />$desc&nbsp;";
	    }
	    echo "</td>";

	}
	function show_Spacer()
	{
	    echo "<td colspan='2'><hr style='width:450px;'></td>";
	}
	function show_Select($option)
	{
	    echo "<td width='350'>";
	    echo JText::_($option['description']),'&nbsp';
	    if ( isset($option['help']) && $option['help']) echo JHTML::_('tooltip',JText::_($option['help']));
	    echo ':';
	    echo "</td>";
	    echo "<td>";
	    $current_value  = defined($option['name'])?constant($option['name']):$option['default'];
	    $sepp           = empty($option['sepparator'])?'%':$option['sepparator'];
	    $select_options_values = explode($sepp,$option['options']);
	    $select_options = array();
		if(isset($option['options_description'])){
			$select_options = explode($sepp,$option['options_description']);
		}
		if ($option['multiple']){
	    	$multiple="MULTIPLE";
	    }
	    echo '<select name="'.(isset($option['name'])?$option['name']:'').'" size="'.(isset($option['size'])?$option['size']:'').'" '.(isset($multiple)?$multiple:'').' '.(isset($option['attributes'])?$option['attributes']:'').'>';
	    for ($i=0;$i<count($select_options_values);$i++){
	        if (empty($select_options[$i]))
                $desc=$select_options_values[$i];
	        else
                $desc=$select_options[$i];
	        if  ($select_options_values[$i]==$current_value)
                $selected='selected';
	        else
                $selected='';

	        echo "<option value='{$select_options_values[$i]}' $selected>$desc</option>";
	    }
	    echo "</select";
	    echo "</td>";
	}
	function showOption($option)
	{
        $method='show_'.$option['type'];
        if (!method_exists($this,$method)){
			$method='show_text';
		}
        echo "<tr>";
        $this->$method($option);
        echo "</tr>";
	}
	function showTabbedJS()
	{
	    /*@var $app JTheFactoryApp*/
        $Tapp = JTheFactoryApp::getInstance();
        $js = $Tapp->getSection('options-js');
        if (count($js))
        foreach ($js as $f=>$file){
	    ?>
            <script type="text/javascript" src="<?php echo $file?>"></script>
	    <?php
        }
	}
	function beginForm()
	{
	    ?>
	    <form action="index.php" method="post" name="adminForm">
    	   <input type="hidden" name="option" value="<?php echo APP_EXTENSION;?>">
	       <input type="hidden" name="task" value="savesettings">

	    <?php
	}
	function endForm()
	{
	    ?></form><?php
	}
   function showTabbedOptions()
   {
	    /*@var $app JTheFactoryApp*/
        $Tapp = JTheFactoryApp::getInstance();

        $tab_list = explode(',',$Tapp->getIniValue('tabs','options-tabs'));
        $default_tab = $Tapp->getIniValue('default','options-tabs');

		jimport('joomla.html.pane');
		$tabs = &JPane::getInstance('Tabs',array());

		$this->showTabbedJS();
		$this->beginForm();
    	echo $tabs->startPane("configPane");

        for($i=0;$i<count($tab_list);$i++){
            $current_tab=$tab_list[$i];
        	echo $tabs->startPanel($current_tab,"tab_$i");
            echo "<table width='100%'>";
            foreach($this->_options_array as $opt=>$key)
            {
                $current_option=$Tapp->getSection($opt);
                $current_option['name'] = $opt;
                $current_option['default'] = $key;
                if($current_option['tab']==$current_tab || (empty($current_option['tab'])&&($default_tab==$current_tab)))
                    $this->showOption($current_option);

            }
            echo "</table>";
            echo $tabs->endPanel();
        }

        if ($Tapp->getIniValue('use_terms_and_conditions')){
            $editor = &JFactory::getEditor();
            /*@var $termsobj JTheFactoryTermsAndConditions*/
            $termsobj = &JTheFactoryTermsAndConditions::getInstance($Tapp->getSection('terms_and_conditions'));

        	echo $tabs->startPanel(JText::_("ADS_TERMS_CONDITIONS"),"tab_$i");
            echo "<table width='100%'>";
            echo "<tr><th class='title' colspan='2'>".JText::_("ADS_TERMS_CONDITIONS")."</th></tr><tr><td colspan='2'>";
			echo $editor->display( 'terms',  $termsobj->getTerms() , '100%', '550', '75', '20' ) ;
			echo "</td></tr>";
            echo "</table>";
            echo $tabs->endPanel();
        }

        echo $tabs->endPane();
        $this->endForm();
    }
	function saveOptions() {
       	/*@var $app JTheFactoryApp*/
        $Tapp = JTheFactoryApp::getInstance();

        $f = fopen($this->optionfile,'w');
		if (!$f) return false;
		fputs($f,"<?php\n");

		foreach ($this->_options_array as $optstring=>$val){
			
		    if ($Tapp->getIniValue('type',$optstring) == 'spacer') continue;
		    
			$defaultval	= defined($optstring) ? constant($optstring) : $val;
			
            if (in_array($optstring,$this->_checkboxes))  
            	$defaultval='0';
            	
            if (in_array($optstring,$this->_raw_options)) 
            	$v = JRequest::getVar($optstring,$defaultval,'default','none',JREQUEST_ALLOWRAW);

            else 
            	$v = JRequest::getVar($optstring,$defaultval);
            
            $v = addslashes($v);

            fputs($f,"define('$optstring','$v');\n");

        }
        fputs($f,"\n?>");
       	fclose($f);


        if ($Tapp->getIniValue('use_terms_and_conditions')) {
            $v = JRequest::getVar('terms','','default','none',JREQUEST_ALLOWRAW);
            /*@var $termsobj JTheFactoryTermsAndConditions*/
            $termsobj = &JTheFactoryTermsAndConditions::getInstance($Tapp->getSection('terms_and_conditions'));
            $termsobj->saveTerms($v);
       	}

		return true;
    }

}
?>