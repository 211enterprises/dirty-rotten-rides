<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

class JHtmlSettings
{
  public function boolean($id, $title, $selected, $tip = null, $values = array(), $warning = null, $related = null)
  {
    $options = array(
      0 => isset($values[0]) ? JText::_($values[0]) : JText::_('JNO'),
      1 => isset($values[1]) ? JText::_($values[1]) : JText::_('JYES'),
    );

    $select   = JHtml::_('select.genericlist', $options, $id, null, 'value', 'text', $selected);
    $hasTip   = is_null($tip) ? '' : 'hasTip';
    $titleTip = is_null($tip) ? '' : JText::_($title) . ' :: ' . JText::_($tip);

    $warning = is_null($warning) ? '' : '<br style="clear: left;" /><span class="adsman-button adsman-bullet-error adsman-error-field">' . JText::_($warning) . '</span>';
    $related = is_null($related) ? '' : $related . '_related';

    $output = '<tr class="' . $hasTip . ' ' . $related . '" title="' . $titleTip . '">'
            . '  <td width="20%" class="paramlist_key">'
            . '    <span class="editlinktip">'
            . JText::_($title) 
            . '    </span>'
            . '  </td>'
            . '  <td class="paramlist_value">'
            . $select
            . $warning
            . '  </td>'
            . '</tr>';

    return $output;
  }
}