<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/
?>

<fieldset class="adminform">
	<legend><?php echo JText::_( 'ADS_THEMES' ); ?></legend>
<table class="adminlist">
<?php foreach ($list["themes"] as $theme){ ?>
<tr>
	<td>
		<?php echo $theme;?>
	</td>
	<td width="50">
		<?php echo JText::_( 'ADS_DELETE' ); ?>
	</td>
</tr>
<?php } ?> 
</table>
</fieldset>