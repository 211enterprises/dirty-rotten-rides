<?php /**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

$act = FactoryLayer::getRequest($_REQUEST,"act","");
?>
		
<table border="0" class="toolbar" width="100%" >
	<tr>
		<td align="left">
		<h2 style="text-align:left;">
			<img src="<?php echo F_ROOT_URI;?>administrator/components/com_adsman/img/themes.png" alt="Compulsory" border="0" style="vertical-align:middle;" />&nbsp;&nbsp;<?php echo JText::_('ADS_THEMES');?></h2>
		</td>
		<td align="right"> 
		<table border="0" class="adminlist">
			<tr>
				<td></td>
				<?php if($act == "upload_theme") {?>
				<td width="50">
					<a href="#" onclick="document.adminForm.submit()"><strong><?php echo F_SAVE;?></strong><img src="<?php echo F_ROOT_URI;?>/administrator/components/com_adsman/img/filesave.png" style="width:20px;" border="0" /></a>
				</td>
				<?php } ?>
				<td width="50">
					<a href="index.php?option=<?php echo F_COMPONENT_NAME;?>&task=themes_administrator&act=list_themes">
					<strong><?php echo JText::_('ADS_LIST_THEMES');?></strong><img src="<?php echo F_ROOT_URI;?>/administrator/components/<?php echo F_COMPONENT_NAME;?>/thefactory/fields/assets/go_back.png" />
					</a>
				</td>
				<td width="50">
					<a href="index.php?option=<?php echo F_COMPONENT_NAME;?>&task=themes_administrator&act=upload_theme">
					<strong><?php echo JText::_('ADS_UPLOAD_THEME');?></strong><img src="<?php echo F_ROOT_URI;?>/administrator/components/<?php echo F_COMPONENT_NAME;?>/thefactory/fields/assets/add.png" />
					</a>
				</td>
			</tr>
		</table>
		</td>		
	</tr>
</table>
