<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/


jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.archive');
jimport('joomla.filesystem.path');


class FactoryThemesController{
	
	var $_db 	= null;
	
	
	function checkTask( $act )
	{
		$this->_db = &FactoryLayer::getDB();
		if(!$act)
			$act="list_themes";
		$method_name = $act;
		
		if ( method_exists($this,$method_name) ){
			require_once(dirname(__FILE__).DS."..".DS."tmpl".DS."themes.toolbar.php");
			$this->$method_name();
			return true;
		}

		return false;
	}
	
	function list_themes(){
		$component = APP_EXTENSION;
		$themes = JFolder::folders(JPATH_ROOT.DS."components".DS.$component.DS."templates".DS);
		
		if($themes){
			
			$themeOptions = array();
			
			$list["themes"] = array();
			foreach ($themes as $file){
				if($file!="cache")
					$list["themes"][] = $file;
			}
		}
		require_once(dirname(__FILE__).DS."..".DS."tmpl".DS."themes_list.php");
		
	}
	
	function upload_theme(){
		
		require_once(dirname(__FILE__).DS."..".DS."tmpl".DS."themes_upload.php");
	}
	
	function do_upload_theme(){
		
		$msg = "";
		
		// Move uploaded file
		jimport('joomla.filesystem.file');

		
        if (!isset($_FILES["theme_file"]['name'])){ return false; }
		
        $upl=$_FILES["theme_file"]['tmp_name'];
		$file = JPATH_ROOT.DS."tmp".DS."blue.zip";
        
        move_uploaded_file($upl,$file);
        
		$p_filename = $file;
		$archivename = $p_filename;

		$tmpdir = JPATH_ROOT.DS."components".DS."com_adsman".DS."templates".DS."blue";
		mkdir($tmpdir);

		// Clean the paths to use for archive extraction
		$extractdir = JPath::clean($tmpdir);
		$archivename = JPath::clean($archivename);

		// do the unpacking of the archive
		$result = JArchive::extract( $archivename, $extractdir);

		if ( $result === false ) {
			return false;
		}

		FactoryLayer::redirect("index.php?option=".F_COMPONENT_NAME."&task=themes_administrator",$msg);
	}
	
	function remove_theme(){
		
	}
	
}