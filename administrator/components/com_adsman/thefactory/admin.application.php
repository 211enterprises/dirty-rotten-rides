<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

require_once(dirname(__FILE__) . DS ."application" . DS . "application.class.php" );

class JTheFactoryApp extends JTheFactoryApplication
{
    
    /**
     * Does all the initializations for admin backend
     * reads the INI file.
     *
     * @access public
     * @param string $configfile    Filename with application ini
     *
     */
    function __construct($configfile=null,$runfront=null)
    {
		
    	parent::__construct();
    	
    	JLoader::register('JTheFactoryAdminToolbar',dirname(__FILE__).DS.'admin.toolbar.php');
        if ($this->getIniValue('use_admin_config')){
            require_once(JPATH_COMPONENT_SITE.DS.$this->getIniValue('option_file'));
            JLoader::register('JTheFactoryConfig',dirname(__FILE__).DS.'admin.config.php');
        }
        
        if ($this->getIniValue('use_terms_and_conditions')){
            JLoader::register('JTheFactoryTermsAndConditions',dirname(__FILE__).DS.'admin.terms.php');
        }
        if ($this->getIniValue('use_admin_config_mail')){
            JLoader::register('JTheFactoryMailManager',dirname(__FILE__).DS.'admin.mailmanager.php');
        }

        if (!defined('APP_LANGUAGE')) {
            if ($this->getIniValue('use_language_files')){
                define('APP_LANGUAGE','en');
            }else{
                define('APP_LANGUAGE','en');
            }
        }
        
        if ($this->getIniValue('use_extended_profile')){
        	// BETA
			JLoader::register('JTheFactoryUserProfile',JPATH_SITE . DS . 'components' . DS . 'com_adsman'.DS.'thefactory'.DS.'front.userprofile.php');
        	//JLoader::register('JTheFactoryUserProfile',JPATH_COMPONENT_SITE.DS.'thefactory'.DS.'front.userprofile.php');
        }

        if ($this->getIniValue('use_acl')){
        	// BETA
			JLoader::register('JTheFactoryACL',JPATH_COMPONENT_SITE.DS.'thefactory'.DS.'front.acl.php');
        }
    }

    function checktask($task)
    {
    	if( parent::checktask($task)!==false )
    		return true;
    	
    	
		$methoded='admin_'.$task;
            
        if (method_exists($this,$methoded)){
			return $this->$methoded();
        }
        	
        return false;
    }
    
    function import($framework_file)
    {
    	if (file_exists(dirname(__FILE__).DS.'admin.'.$framework_file.'.php'))
    		require_once dirname(__FILE__).DS.'admin.'.$framework_file.'.php';
    	elseif (file_exists(JPATH_COMPONENT_ADMINISTRATOR.DS.'thefactory'.DS.'admin.'.$framework_file.'.php'))
    		require_once file_exists(JPATH_COMPONENT_ADMINISTRATOR.DS.'thefactory'.DS.'admin.'.$framework_file.'.php');
    }
    
    function toolbar($task)
    {
        if (in_array($task,get_class_methods('JTheFactoryAdminToolbar'))){
            call_user_func(array('JTheFactoryAdminToolbar',$task));
            return true;
        }
        return false;
    }

    function admin_Settings()
    {
        if (!$this->getIniValue('use_admin_config')) return false;
        /*@var $config JTheFactoryConfig */
        $config=&JTheFactoryConfig::getInstance();
        $config->showTabbedOptions();

        return true;
    }

    function admin_SaveSettings()
    {
        if (!$this->getIniValue('use_admin_config')) return false;
        /*@var $config JTheFactoryConfig */
        $config=&JTheFactoryConfig::getInstance();
        $config->saveOptions();
        $japp=&JFactory::getApplication();
        $japp->redirect('index.php?option='.APP_EXTENSION.'&task=settings',JText::_('ADS_SETTINGS_SAVED'));
        return true;
	}

    function admin_AboutComponent()
    {
        /*@var $about JTheFactoryAbout */
        require_once(dirname(__FILE__).'/admin.about.php');
        $about=&JTheFactoryAbout::getInstance();
        $about->showAbout();
        return true;
    }
    
    function admin_MailSettings()
    {
        /*@var $mailmanager JTheFactoryMailManager*/
        $mailmanager=&JTheFactoryMailManager::getInstance($this->getSection('mail-settings'));
        $mailmanager->showMailSettings();
        return true;
    }
    
    function admin_MailSettings_Enable()
    {
        /*@var $mailmanager JTheFactoryMailManager*/
        $mailmanager=&JTheFactoryMailManager::getInstance($this->getSection('mail-settings'));
        $mailmanager->saveMailSettingsEnable();
        return true;
    }
    
    function admin_SaveMailSettings()
    {
        /*@var $mailmanager JTheFactoryMailManager*/
        $mailmanager=&JTheFactoryMailManager::getInstance($this->getSection('mail-settings'));
        $mailmanager->saveMailSettings();
        return true;
    }
   
    function admin_CB_Pluginstall()
    {
    	global $option;
    	
    	$mainframe = JFactory::getApplication();
        
    	$japp=&JFactory::getApplication();
    	$integration=&JTheFactoryIntegration::getInstance();
    	if(!constant('CB_DETECT')){
    		$japp->redirect('index.php?option='.$option,'CB not installed!','error');
    	}

    	$integration->adminCBInstall();
        $japp->redirect('index.php?option='.APP_EXTENSION.'&task=cb_integration',JText::_("ADS_PLUGINS_INSTALLED"));
    	return true;
    }

	function admin_showPositions()
    {
    	global $option;
    	$mainframe = JFactory::getApplication();
    	$positions=&JTheFactoryPositions::getInstance(1);
    	$positions->showPositions();
    	return true;
    }
    
	function admin_position2fields()
    {
    	global $option;
    	$mainframe = JFactory::getApplication();
    	$positions=&JTheFactoryPositions::getInstance(1);
    	$positions->position2fields();
    	return true;
    }
    
    function admin_dirchmode(){
    	
    	$dir= JPATH_ROOT.DS.JRequest::getVar( 'dir', '');
    
        $mainframe = JFactory::getApplication();
    	
        $requested_by = $_SERVER['HTTP_REFERER'];
    	if(file_exists($dir)){
    		
    		if (chmod($dir,0755))
    			$mainframe->redirect($requested_by, JText::_("ADS_SUCCESFULLY_CHMODE") );
    		else 
    			$mainframe->redirect($requested_by, JText::_("ADS_CHMODE_FAILED") );
    			
    	}
    }
    
    function admin_purgecache(){
    	
    	
    	$dir= JPATH_ROOT.DS.'components/'.APP_EXTENSION.DS.'templates'.DS.'cache';
    	
        $japp=&JFactory::getApplication();
    	$requested_by = $_SERVER['HTTP_REFERER'];
    	if(file_exists($dir)){
    		if(is_writable($dir)){
    			// TO DO in framework
    			$smarty   = AdsUtilities::SmartyLoaderHelper();
    			$smarty->clear_compiled_tpl();
    			$japp->redirect($requested_by, JText::_("ADS_CACHED_CLEARED") );
    		}else
	    		$japp->redirect($requested_by, JText::_("ADS_PERMISSION_UNAVAILABLE") );
    	}else
	    		$japp->redirect($requested_by, JText::_("ADS_FILE_NOT_EXISTS") );
    }
    
	function admin_countries()
    {
    	global $option,$mainframe;
    	JLoader::register('JTheFactoryCountries_controller',$this->app_path_admin.'countries/admin.controller.php');
    	
    	$countries= new JTheFactoryCountries_controller();
		$ret = $countries->route();
		if ($ret) return true;
		else return false;
    }
    
    
}

?>