<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/




class FCBController{
	
	var $_db 	= null;
	var $_tbl   = null;
	
	function checkTask( $act="show" )
	{
		$this->_tbl = constant("APP_INTEGRATION_".strtoupper(APP_PREFIX));
		$this->_db = &FactoryLayer::getDB();
		
		$method_name = $act;
		
		if ( method_exists($this,$method_name) ){
			require_once(dirname(__FILE__).DS."..".DS."tmpl".DS."integration.toolbar.php");
			$this->$method_name();
			return true;
		}

		return false;
	}
	
	function save()
	{
		$database = & JFactory::getDBO();
		
        $database->setQuery("select * from `$this->_tbl`");
        $ql = $database->loadObjectList("field");
		
		$field_maps = JTheFactoryIntegration::integrationFields();
		
	    for ($i=0;$i<count($field_maps);$i++){
	    	
	        $fld = $field_maps[$i];
	        $cb = JRequest::getVar($fld,'');
	        // && $cb!=""
	        if(isset($cb)){
		        if(isset($ql[$fld]) && $ql[$fld]!=""){
			        $database->setQuery("update `$this->_tbl` set cb_field='$cb' where field='$fld'");
		        } 
				else{
			        $database->setQuery("INSERT INTO `$this->_tbl` set cb_field='$cb' , field='$fld'");
				}   
		        $database->query();
	        }
	    }

		FactoryLayer::redirect("index.php?option=".F_COMPONENT_NAME."&task=integration&act=show&type=cb","Settings Saved!");
		
	}
	
	function show(){
		
		require_once(dirname(__FILE__).DS."..".DS."tmpl".DS."cb.php");
		return true;
	}
	
	function installPlugins(){
		
		if (APP_PREFIX=="adsman") {
			$installer = "ads";
		}else
			$installer = APP_PREFIX;
		
		$install_file = JPATH_COMPONENT_ADMINISTRATOR.DS."install.".$installer.".php";
		require_once($install_file);
		
		com_adsmanInstallerScript::install_cbplugin('adsman_my_ads');
		com_adsmanInstallerScript::install_cbplugin('adsman_googlemap');
        com_adsmanInstallerScript::install_cbplugin('adsman_my_credits');
		return true;
		

	}
	
}


