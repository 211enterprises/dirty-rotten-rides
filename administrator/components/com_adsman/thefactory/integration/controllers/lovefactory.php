<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/




class FLovefactoryController{
	
	var $_db 	= null;
	var $_tbl   = null;
	
	
	function checkTask( $act="show" )
	{
		$this->_tbl = constant("APP_INTEGRATION_".strtoupper(APP_PREFIX));
		$this->_db = &FactoryLayer::getDB();
		
		$method_name = $act;
		
		if ( method_exists($this,$method_name) ){
			require_once(dirname(__FILE__).DS."..".DS."tmpl".DS."integration.toolbar.php");
			$this->$method_name();
			return true;
		}

		return false;
	}

	
	function save()
	{
		$database = & JFactory::getDBO();
        $database->setQuery("select * from `$this->_tbl`");
        $ql = $database->loadObjectList("field");
		
        $Tapp=&JTheFactoryApp::getInstance();
   		$f=$Tapp->getIniValue('fields_list','cb-integration');
   		
   		$field_maps=explode(',',$f);
				
	    
	    for ($i=0;$i<count($field_maps);$i++){
	        $fld=$field_maps[$i];
	        $cb=JRequest::getVar($fld,'');
	        if(isset($cb) && $cb!=""){
		        if(isset($ql[$fld]) && $ql[$fld]!=""){
			        $database->setQuery("update `$this->_tbl` set love_field='$cb' where field='$fld'");
		        }else{
			        $database->setQuery("INSERT INTO `$this->_tbl` set love_field='$cb' , field='$fld'");
		        } 
	        }
	        $database->query();
	    }
		FactoryLayer::redirect("index.php?option=".F_COMPONENT_NAME."&task=integration&act=show&type=lovefactory","Settings Saved!");
	}
	
	function show(){

		require_once(dirname(__FILE__).DS."..".DS."tmpl".DS."lovefactory.php");
		return true;
	}
	
}


