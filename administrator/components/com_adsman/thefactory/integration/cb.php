<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

// CB INTEGRATION LIB

class cb_integration extends JTheFactoryIntegration{
	
	function _detectIntegration($Tapp)
	{
		$database = JFactory::getDbo();
	    $database->setQuery("SELECT count(*) FROM #__extensions WHERE `element`='com_comprofiler'");
	    
	    if($database->loadResult()>0){
	    	if (!defined('CB_DETECT')) {
                define('CB_DETECT',1);
            }
	    	cb_integration::getIntegrationArr($Tapp);
	    }else {
	        if (!defined('CB_DETECT')) {
                define('CB_DETECT',0);
            }
	    }
	}
	
	function getIntegrationArr($Tapp){
		
		static $integration_arr;
		
		if(!isset($integration_arr["cb"])){
			$database =& JFactory::getDBO();
			
		    $integration_arr=array();
		    $cb_fieldmap=array();
		    
	    	$table=$Tapp->getIniValue("field_map_table","cb-integration");
			
	    	$database->setQuery("SELECT field,cb_field FROM `".$table."`");
	    	$r=$database->loadAssocList();
	    	
	    	for($i=0; $i<count($r); $i++) {
	    	    $cb_fieldmap[$r[$i]['field']]=$r[$i]['cb_field'];
	    	}
	    	
	    	$integration_arr["CB"]=$cb_fieldmap;
		}
		return $integration_arr["CB"];
	}
	
	function getProfile(&$user_obj){
		
	   // CB Fields
        require_once(JPATH_ADMINISTRATOR.DS."components".DS."com_comprofiler".DS."plugin.foundation.php");
		cbimport( 'cb.database' );
		cbimport( 'cb.tables' );
	    
		$cbUser	=& CBuser::getInstance( $user_obj->userid );
		
      	$user_obj->has_profile =  0;	
		
      	if ( $cbUser ) {
	    	$user_obj->has_profile =  1;	
			$user =& $cbUser->getUserData();
            $user_obj->username = $user->username;
		}
		
		$cb_fieldmap = JTheFactoryIntegration::getIntegrationArray("CB");
		$integrationFields = JTheFactoryIntegration::integrationFields();
		
      	//$user_obj->username = $user->username;
      
		foreach ($integrationFields as $pm => $fld){
			
            if (isset($cb_fieldmap[$fld]) && $cb_fieldmap[$fld])
            	$user_obj->$fld=$user->$cb_fieldmap[$fld];
		}
	}
	
	function hasProfile(){
		
		$my = JFactory::getUser();
		$user = new stdClass();
		$user->userid = $my->id;

		cb_integration::getProfile($user);

		//if ($user->name) 
		if ($user->has_profile) 
			return true;
		else 
			return false;	
	}
	
	function getListJoins(&$Joins, &$cols, $exclusive ){
		
		$cb_fieldmap = JTheFactoryIntegration::getIntegrationArray("CB");
			
		foreach ($cb_fieldmap as $prof_field => $c ){
			if($c != '') {
			//if($c)
				$cols[] = " prof.{$c} as {$prof_field}";
			}
		}
		
		if($exclusive==1){
			
	    	$Joins[] = " RIGHT JOIN `#__comprofiler` AS `prof` ON `a`.`userid` = `prof`.`user_id` " ;
			
		}else{
			
	    	$Joins[] = " LEFT JOIN `#__comprofiler` AS `prof` ON `a`.`userid` = `prof`.`user_id` " ;	
		    
		}
	}
	
	function getFilterSql($field, $filter){
		
		$cb_fieldmap = JTheFactoryIntegration::getIntegrationArray("CB");
		$sql = " prof." . $cb_fieldmap[$field] . "='" . $filter . "' ";
		
		return $sql;
	}
	
}

?>