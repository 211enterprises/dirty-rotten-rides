<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/
?>

<div id="cpanel">
	
<form action="index.php?option=com_adsman&amp;task=savesettings" method="post" name="adminForm" id="adminForm">
	<div>
		<input type="hidden" name="option" value="com_adsman">
		<input type="hidden" name="task" value="savesettings">
		<input type="hidden" name="profile_integration" value="1">
<!--<img src="<?php //echo JURI::root();?>administrator/components/com_adsman/img/user.png" style='height:25px; vertical-align:middle;' />-->
		<fieldset class="adminform">
                <legend><span style="float:left;"><?php echo JText::_( 'ADS_PROFILE_SETTINGS' ); ?></span></legend>
				<table class="adminlist" width="100%">
				<tr>
					<td class="paramlist_key" width="100"><?php echo JText::_("ADS_USER_PROFILE");?>: </td>
					<td class="paramlist_value">
					<?php $selected = defined("ads_opt_profile_mode") ? ads_opt_profile_mode : "ads"; ?>
					 	<select  name="ads_opt_profile_mode" id="ads_opt_profile_mode">
			  		  	  <option <?php echo $selected == 'ads' ? 'selected="selected"' : ''; ?> value="ads"><?php echo JText::_('ADS_COMPONENT_PROFILE'); ?></option>
			  			  <option <?php echo $selected == 'cb' ? 'selected="selected"' : ''; ?> value="cb"><?php echo JText::_('ADS_COMMUNITY_BUILDER'); ?></option>
			  			  <option <?php echo $selected == 'love' ? 'selected="selected"' : ''; ?> value="love"><?php echo JText::_('ADS_LOVE_FACTORY'); ?></option>
					  </select>
					</td>
				</tr>
			    <tr>
					<td class="paramlist_key"> <?php echo JText::_("ADS_PROFILE_REQUIRED"); ?>:&nbsp;</td>
					<td class="paramlist_value"><input name="ads_opt_profile_required" <?php echo (defined("ads_opt_profile_required") && ads_opt_profile_required)?"checked":"" ?> type="checkbox" value="1"></td>
				</tr>
				<tr>
				<td colspan="2">
					<?php JRoute::_( 'index.php?option=com_adsman&amp;task=savesettings'); ?>
					<input name="save" value="<?php echo JText::_('ADS_SAVE');?>" class="back_button" type="submit" />
				</td>
				</tr>
				</table>
	
		</fieldset>
	</div>	
</form>

<?php
$link = 'index.php?option=com_adsman&amp;task=integration&act=show&type=cb';
JAdsAdminView::quickiconButton( $link, 'cb_integration_48.png', "CB <br /> integration");

$link = 'index.php?option=com_adsman&amp;task=integration&act=show&type=lovefactory';
JAdsAdminView::quickiconButton( $link, 'love_integration48.png', "Love Factory <br /> integration");
?>
</div>