<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/
?>

<table border="0" class="toolbar" width="100%" >
	<tr>
		<td align="left">
		<h2 style="text-align:left;">
            <img src="<?php echo F_ROOT_URI;?>administrator/components/com_adsman/img/integration.png" alt="Compulsory" border="0" style="vertical-align:middle;" />&nbsp;&nbsp;<?php echo JText::_('ADS_COMMUNITY_INTEGRATION');?></h2>
		</td>
		<td align="right"> 
		<table border="0" class="adminlist">
			<tr>
				<td width="90" style="text-align:center;">
					<a href="index.php?option=com_adsman&amp;task=integration&act=show&type=cb"><img src="<?php echo F_ROOT_URI;?>components/com_adsman/img/cb_integration_48.png" border="0" /><br /><strong><?php echo JText::_('ADSMAN_CB_INTEGRATION');?></strong></a>
				</td>
				<td width="90" style="text-align:center;">
					<a href="index.php?option=com_adsman&amp;task=integration&act=show&type=lovefactory"><img src="<?php echo F_ROOT_URI;?>components/com_adsman/img/love_integration48.png" border="0" /><br /><strong><?php echo JText::_('ADS_LOVE_INTEGRATION');?></strong></a>
				</td>
				<td></td>
			
				<?php if($act == "show") {?>
				<td width="50" style="text-align:center;">
					<a href="#" onclick="submitbutton('integration');"><img src="<?php echo F_ROOT_URI;?>administrator/components/com_adsman/img/filesave.png" border="0" /><br /><strong><?php echo F_SAVE;?></strong></a>
				</td>
				<?php } ?>
				<?php if (!JTheFactoryIntegration::detectCBPlugin('adsman_my_ads') || !JTheFactoryIntegration::detectCBPlugin('adsman_googlemap') || !JTheFactoryIntegration::detectCBPlugin('adsman_my_credits')) {	?>
				<td width="10">&nbsp;</td>
				<td width="60" style="text-align:center;">
				<a href="<?php echo JURI::base();?>index.php?option=<?php echo APP_EXTENSION;?>&task=integration&type=cb&act=installPlugins"><img src="<?php echo JURI::base();?>components/com_adsman/img/spanner_48.png" style="vertical-align:middle;"><br /><strong><?php echo JText::_('ADS_INSTALL_CB_PLUGINS');?></strong></a>
				</td>
				<?php } ?>
			</tr>
		</table>
		</td>		
	</tr>
</table>
