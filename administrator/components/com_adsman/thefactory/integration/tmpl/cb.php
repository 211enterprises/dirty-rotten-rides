<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

if ( !CB_DETECT ){ ?>
        <div style="background-color:orange;font-size:150%;"><?php echo JText::_("ADS_CB_NOT_INSTALLED");?></div>
<?php }else{ 
	$integration_arr = JTheFactoryIntegration::getIntegrationArray("CB");
	
	$database =& JFactory::getDBO();
	$query = "SELECT `name` as value,`title` as text FROM #__comprofiler_fields WHERE `table` LIKE '%comprofiler%'  order by `name` ";
	//$query = "SELECT `name` as value,`title` as text FROM #__comprofiler_fields order by `name`";
	$database->setQuery($query);
	
	
	$fields = $database->loadObjectList();
	if ($fields) $fields = array_merge(array(JHTML::_("select.option",'','-'.JText::_("ADS_NONE").'-')),$fields);
	
	$field_maps = JTheFactoryIntegration::integrationFields();
	
    $disabled=(CB_DETECT)?"":"disabled";
?>
<h3><?php echo JText::_('ADSMAN_CB_INTEGRATION');?></h3>

<form action="index.php" method="post" name="adminForm" >
  <input type="hidden" name="option" value="<?php echo APP_EXTENSION;?>">
  <input type="hidden" name="task" value="integration">
  <input type="hidden" name="type" value="cb">
  <input type="hidden" name="act" value="save">
  <table width="100%" class="adminlist">
  <?php for($i=0;$i<count($field_maps);$i++) { 
  		$cb_field_text=APP_PREFIX."_cbfield_".strtolower($field_maps[$i]);

  	?>
  	<tr>
  		<td width="15%" align="left">
  			<?php 
  			
  				if (defined($cb_field_text)) echo constant($cb_field_text);
  				else echo $field_maps[$i];
  			?>:
  		</td>
  		<td width="20%" align="left">
  		  <?php 
	  			  $selected = isset($integration_arr[$field_maps[$i]])?($integration_arr[$field_maps[$i]]) : null;
			  echo JHTML::_("select.genericlist",$fields,$field_maps[$i],"class='inputbox' $disabled",'value','text',@$integration_arr[$field_maps[$i]]);
		  ?>
  		  <?php if (defined($cb_field_text."_help")) echo JHTML::_('behavior.tooltip',constant($cb_field_text."_help")); ?>
  		</td>
  	</tr>
    <?php } ?>
  </table>
</form>
		
		
<?php } ?>