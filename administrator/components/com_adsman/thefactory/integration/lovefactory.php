<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

// LOVEFACTORY INTEGRATION LIB

class lovefactory_integration extends JTheFactoryIntegration{
	
	function _detectIntegration($Tapp)
	{
		$database = JFactory::getDbo();
		
	    $database->setQuery("SELECT count(*) FROM #__extensions WHERE `element`='com_lovefactory'");
	    
	    if($database->loadResult()>0) {
            if (!defined('LOVE_DETECT')) {
	    	    define('LOVE_DETECT',1);
            }
	    	lovefactory_integration::getIntegrationArr($Tapp);
        }else {
            if (!defined('LOVE_DETECT')) {
	            define('LOVE_DETECT',0);
            }
	    }
	}
	
	function getIntegrationArr($Tapp){
		static $integration_arr;
		
		if(!isset($integration_arr["cb"])){
			$database =& JFactory::getDBO();
			
		    $integration_arr=array();
		    $cb_fieldmap=array();
		    
	    	$table=$Tapp->getIniValue("field_map_table","cb-integration");
			
	    	$database->setQuery("SELECT field,love_field FROM `".$table."`");
	    	$r=$database->loadAssocList();
	    	for($i=0;$i<count($r);$i++){
	    	    $cb_fieldmap[$r[$i]['field']]=$r[$i]['love_field'];
	    	}
	    	$integration_arr["LOVE"]=$cb_fieldmap;
		}
		return $integration_arr["LOVE"];
	}
	
	function getProfile(&$user_obj){
		
		$database = JFactory::getDBO();
	    $q = ' SELECT p.*, u.username, m.title, m.title AS testering, m.ordering,'
		. '   u.lastvisitDate, COUNT(se.userid) AS loggedin,'
	    . '   IF (g.lat IS NOT NULL, g.lat, 0) AS googleX,'
	    . '   IF (g.lng IS NOT NULL, g.lng, 0) AS googleY'
        . ' FROM #__lovefactory_profiles p'
		. ' LEFT JOIN #__users u ON u.id = p.user_id'
        . ' LEFT JOIN #__lovefactory_memberships_sold s ON s.id = p.membership_sold_id'
        . ' LEFT JOIN #__lovefactory_memberships m ON m.id = s.membership_id'
        . ' LEFT JOIN #__session se ON se.userid = p.user_id AND se.client_id = 0'
        . ' LEFT JOIN #__lovefactory_geo g ON g.user_id = p.user_id'
        . ' WHERE p.user_id = ' . $user_obj->userid
        . ' GROUP BY p.user_id';
         $database->setQuery($q);
         $user = $database->loadObject();
		
		$cb_fieldmap = JTheFactoryIntegration::getIntegrationArray("LOVE");
		$integrationFields = JTheFactoryIntegration::integrationFields();
		
       	$user_obj->username=$user->username;
		foreach ($integrationFields as $pm => $fld){
			
            if (isset( $cb_fieldmap[$fld] ) && $cb_fieldmap[$fld])
            	$user_obj->$fld = @$user->$cb_fieldmap[$fld];
		}
	}
	
	function hasProfile(){
		
		$my = JFactory::getUser();
		$user = new stdClass();
		$user->userid = $my->id;
		lovefactory_integration::getProfile($user);
		if ($user->name) 
			return true;
		else return false;	
	}
	
	function getListJoins(&$Joins, &$cols, $exclusive ){
		
		
		$cb_fieldmap = JTheFactoryIntegration::getIntegrationArray("LOVE");
		$integrationFields = JTheFactoryIntegration::integrationFields();
		
		unset($cb_fieldmap["googleX"]);
		unset($cb_fieldmap["googleY"]);
       	foreach ($integrationFields as $pm => $fld){
			
            if ( isset($cb_fieldmap[$fld]) &&  $cb_fieldmap[$fld]!="")
            	$cols[] = " prof.{$cb_fieldmap[$fld]} as $fld ";
		}
		
		$cols[] = " g.lat as googleX";
		$cols[] = " g.lat as googleY";
		
		if($exclusive==1){
			
	        $Joins[] = ' RIGHT JOIN #__lovefactory_profiles as prof ON `u`.`id` = `prof`.`user_id` ';
		    $Joins[] = ' LEFT JOIN #__lovefactory_geo as g ON g.user_id = prof.user_id ';
			
		}else{
			
	        $Joins[] = ' LEFT JOIN #__lovefactory_profiles as prof ON `u`.`id` = `prof`.`user_id` ';
		    $Joins[] = ' LEFT JOIN #__lovefactory_geo as g ON g.user_id = prof.user_id ';
		    
		}
	}
	
}

?>