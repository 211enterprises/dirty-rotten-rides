<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/


class JTheFactoryIntegration{
	
    /**
     * Added INIT Method to load all dependencies both on front & backend
     *
     * @since 13/01/2010 (dd/mm/YYYY)
     */
	function init(&$application){

		if($application){
			$Tapp=$application;
		}
		else 
			$Tapp=&JTheFactoryApp::getInstance(null,null);
		
		$this->detectIntegration($application);	

	}

	/**
	 * Backend Application Router
	 *
	 * @param $task
	 * @param $act
	 * @return bool
	 */
	function route(){
		
		$task = JRequest::getVar("task","");
		$act  = JRequest::getVar("act","");
		
		if (method_exists($this,$task)){
			return $this->$task($act);
		}
        
	}
	
	/**
	 * Backend Main Controller task
	 *
	 * @param $act
	 * @return bool
	*/
   function integration( &$act )
    {
    	if( $act=="" ){
			require_once(dirname(__FILE__).DS."tmpl".DS."main.php");
			return true;
    	} else {

			$type = FactoryLayer::getRequest($_REQUEST,"type");
			if( $type=="cb" ){
				
				JLoader::register('FCBController', dirname(__FILE__).DS.'controllers'.DS.'cb.php');
				$FController = new FCBController();
				return $FController->checkTask( $act );
				
			}elseif( $type=="lovefactory" ){
				
				JLoader::register('FLovefactoryController', dirname(__FILE__).DS.'controllers'.DS.'lovefactory.php');
				$FController = new FLovefactoryController();
				return $FController->checkTask( $act );
				
			}

			return false;

    	}

	}
	
	function detectIntegration($application){
		
		$constant_name = "APP_INTEGRATION_".strtoupper(APP_PREFIX);

        @define($constant_name , $application->getIniValue("field_map_table","cb-integration") );

		require_once(dirname(__FILE__).DS."cb.php");
        $cb_integration = new cb_integration();
		$cb_integration->_detectIntegration($application);
        //cb_integration::_detectIntegration($application);
		
		require_once(dirname(__FILE__).DS."lovefactory.php");
        $love_integration = new lovefactory_integration();
        $love_integration->_detectIntegration($application);
		//lovefactory_integration::_detectIntegration($application);
	}
	
	function getIntegrationArray($key){
		$integration_arr = array();
		
		$application = &JTheFactoryApp::getInstance(null,null);
		require_once(dirname(__FILE__).DS."cb.php");
		$integration_arr["CB"] = cb_integration::getIntegrationArr($application);
		
		require_once(dirname(__FILE__).DS."lovefactory.php");
		$integration_arr["LOVE"] = lovefactory_integration::getIntegrationArr($application);
		
		return $integration_arr[$key];
	}
	
	function integrationFields($Tapp=null){
		if(!$Tapp) {
		    $JTheFactoryAppClass = new JTheFactoryApp();
            $Tapp = $JTheFactoryAppClass->getInstance(null,null);
			//$Tapp = JTheFactoryApp::getInstance(null,null);
        }

    	$f	= $Tapp->getIniValue("fields_list","cb-integration");
    	if($f != "")
	    	return explode(",",$f);
	    else 
	    	return null;	
	}

	function getFilterSql($mode,$field, $filter){
		
		$cname = $mode."_integration";
		$cobj = new $cname;
		return $cobj->getFilterSql($field, $filter);

	}
	
	function detectCBPlugin($pluginname) {
		
		$extension = JTable::getInstance('Extension');

    	$cb_plugin_exists = $extension->find(array('type' => 'component', 'element' => 'com_comprofiler'));
		
    	if ( !$cb_plugin_exists ) 
			return false;
		else {	
			$database = &JFactory::getDBO();
	
	   		$query = "SELECT id FROM #__comprofiler_plugin where element='$pluginname.plugin'";
	   		$database->setQuery($query);
	   $plugid = $database->loadResult();

	   
	   if($database->loadResult() > 0){
	    	return true;
	    }else{
	    	return false;
	    }
		}
	}
	
}

?>