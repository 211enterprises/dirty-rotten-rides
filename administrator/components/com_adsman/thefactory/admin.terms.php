<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

class JTheFactoryTermsAndConditions
{
    var $_tablename=null;
    var $_keyfieldname=null;
    var $_fieldname=null;
    var $_filename=null;
    var $usefile=null;
    function __construct($params)
    {

        if ($params['usefile']){
            $this->usefile=1;
            $this->_filename=JPATH_COMPONENT_SITE.DS.$params['filename'];
        }else{
            $this->usefile=0;
            $this->_fieldname=$params['fieldname'];
            $this->_keyfieldname=$params['keyfield'];
            $this->_tablename=$params['table'];
        }

    }

	function &getInstance($params)
	{
		static $instances;

		if (!isset( $instances ))
			$instances = new JTheFactoryTermsAndConditions($params);

		return $instances;
	}
    function getTerms()
    {
        if ($this->usefile){
            jimport('joomla.filesystem.file');
            return JFile::read($this->_filename);

        }else{
            /*@var $db JDatabase*/
            $db=&JFactory::getDBO();
            $db->setQuery('select `'.$this->_fieldname.'` from `'.$this->_tablename.'` where `'.$this->_keyfieldname.'`="terms_and_conditions"',0,1);
            return $db->loadResult();
        }
    }
    function saveTerms($terms)
    {
        if ($this->usefile){
            jimport('joomla.filesystem.file');
            JFile::write($this->_filename,$terms);

        }else{
            /*@var $db JDatabase*/
            $db=&JFactory::getDBO();
            $db->setQuery('update `'.$this->_tablename.'` set `'.$this->_fieldname.'`="'.$terms.'"  where `'.$this->_keyfieldname.'`="terms_and_conditions"');
            $db->query();
        }
    }
}


?>