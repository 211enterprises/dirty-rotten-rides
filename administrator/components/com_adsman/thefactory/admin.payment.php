<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/


class JTheFactoryPayment extends JObject
{
    var $gateways=array();
    var $paymentitems=array();
    var $plugin_dir=null;
    var $pay_plugins=array();
    var $price_plugins=array();

    function __construct()
    {
        $this->plugin_dir=JPATH_COMPONENT_SITE.DS.'plugins';
        $this->getAvailableGateways();
        $this->getAvailablePaymentItems();

    }
    
	function &getInstance()
	{
		static $instances;

		if (!isset( $instances ))
			$instances = new JTheFactoryPayment();

		return $instances;
	}

	function getAvailableGateways()
	{
    	jimport('joomla.filesystem.folder');

    	$this->gateways=JFolder::files($this->plugin_dir.DS.'payment','^pay_([^\.]*)\.php');
	}
	
	function getAvailablePaymentItems()
	{
    	jimport('joomla.filesystem.folder');

    	$this->paymentitems=JFolder::files($this->plugin_dir.DS.'pricing','^price_([^\.]*)\.php');
	}
	
	function loadAvailabeGateways()
	{
    	jimport('joomla.filesystem.file');
    	$db	= &JFactory::getDBO();
    	
        for ($i=0;$i<count($this->gateways);$i++){
            $plugin=JFile::stripExt($this->gateways[$i]);
    		JLoader::register($plugin,$this->plugin_dir.DS."payment".DS.$this->gateways[$i]);
            
    		$this->pay_plugins[$plugin]=new $plugin($db);
        }

	}
	
	function listEnabledGateways()
	{
    	jimport('joomla.filesystem.file');
    	$db	= &JFactory::getDBO();
    	$arr= array();
    	
        for ($i=0;$i<count($this->gateways);$i++){
            $plugin=JFile::stripExt($this->gateways[$i]);
    		JLoader::register($plugin,$this->plugin_dir.DS."payment".DS.$this->gateways[$i]);
            $p=new $plugin($db);
            
            if ($p->enabled) $arr[]=$p;
        }
        return $arr;
		
	}
	
	function loadAvailabeItems()
	{
    	jimport('joomla.filesystem.file');
    	$db	=&JFactory::getDBO();
    	
        for ($i=0; $i<count($this->paymentitems); $i++){
			$plugin	= JFile::stripExt($this->paymentitems[$i]);
			JLoader::register($plugin,$this->plugin_dir.DS."pricing".DS.$this->paymentitems[$i]);
    		
			$this->price_plugins[$plugin] = new $plugin($db );
        }
	}
	
	function beginForm($task)
	{
	    JTheFactoryAdminHelper::beginAdminForm($task,null,'post',true,null);
	}
	
	function endForm()
	{
	    JTheFactoryAdminHelper::endAdminForm();
	}
		
	function showPaymentGatewayConfig($pluginname)
    {
        if (@$this->pay_plugins[$pluginname]){
            $obj	= $this->pay_plugins[$pluginname];
        }else{
            $db	=&JFactory::getDBO();
    		JLoader::register($pluginname,$this->plugin_dir.DS."payment".DS.$pluginname.".php");
            $obj    = new $pluginname($db);

        }
        $obj->show_admin_config();
    }
    
    function savePaymentGatewayConfig($pluginname)
    {
        if ($this->pay_plugins[$pluginname]){
            $obj	= $this->pay_plugins[$pluginname];
        }else{
            $db =&JFactory::getDBO();
    		JLoader::register($pluginname,$this->plugin_dir.DS."payment".DS.$pluginname.".php");
            $obj	= new $pluginname($db);

        }
        $obj->save_admin_config();
    }
	
    function showAvailablePaymentItems()
	{
        //JHtml::_('behavior.mootools');
		JHtml::_('behavior.framework', true);
		if (!count($this->price_plugins)) $this->loadAvailabeItems();

        $this->beginForm('paymentitems');
        $rows	= array();
        $header_row[]	= array("width"=>"3%","class"=>"left","th"=>"#");
        $header_row[]	= array("width"=>"5%","class"=>"left","th"=>'<input type="checkbox" name="toggle" value="" onclick="checkAll('.count( $this->price_plugins ).');" />');
        $th	= JText::_("Ordering").'&nbsp;<a href="javascript: void(0);"  onclick="submitbutton(\'save_payment_item_order\');"><img src="<?php echo F_ROOT_URI;?>components/com_adsman/img/adm/filesave.png" border="0" /></a>';
        $header_row[]	= array("width"=>"5%","align"=>"left","th"=>$th);

        $header_row[]	= array("width"=>"5%", "class"=>"center","th"=>JText::_("ADS_ENABLED"));
        $header_row[]	= array("width"=>"10%","th"=>JText::_("ADS_PAYMENT_ITEM"));
        $header_row[]	= array("width"=>"*%","th"=>JText::_("ADSMAN_DESCRIPTION"));
        $header_row[]	= array("width"=>"5%", "class"=>"right","th"=>JText::_("ADS_PAYMENT_ITEM_PRICE"));
        $header_row[]	= array("width"=>"5%","th"=>JText::_("ADS_PAYMENT_ITEM_CURRENCY"));

        $i	= 0;
        if (count($this->price_plugins))
            foreach ($this->price_plugins as $pay_list)
            {

                $link=JURI::root()."administrator/index.php?option=".APP_EXTENSION."&task=paymentitemsconfig&itemname=".$pay_list->itemname;
                $img_enabled=($pay_list->enabled)?"tick.png":"publish_x.png";

                $rows[$i][]=$i+1;
                $rows[$i][]=JHTML::_('grid.id', $i+1, $pay_list->itemname);
                $rows[$i][]="<input name='order-{$pay_list->itemname}' value='{$pay_list->ordering}' size='1'>";

                if ($pay_list->itemname == 'packages') {
                    $rows[$i][] = JText::_("ADS_PAYMENT_DEFAULT");
                } else {

                    $rows[$i][]="<a href=\"javascript: void(0);\" onClick=\"return listItemTask('cb".($i+1)."','enablepaymentitem')\">
                        <img src='".JURI::root()."administrator/images/".$img_enabled."' height=\"12\" border=\"0\" />
                    </a>";
                }
                $rows[$i][]="<a href='".$link."' title='".JText::_('ADS_CONFIGURE_PAYMENT_ITEM')."'>".$pay_list->itemname."</a>";
                $rows[$i][]=$pay_list->classdescription;
                $rows[$i][]=$pay_list->price;
                $rows[$i][]=JText::_("ADS_CREDITS");

                $i++;
            }


        JTheFactoryAdminHelper::showAdminTableList($header_row,$rows,null,JText::_('ADS_PAYMENT_ITEMS_CONFIG'));
        $this->endForm();

	}
	
	function showPaymentItemConfig($itemname)
	{
        if ( isset($this->price_plugins[$itemname] ) ){
            $obj=$this->price_plugins[$itemname];
        } else {
            $db = & JFactory::getDBO();
    		JLoader::register($itemname,$this->plugin_dir.DS."pricing".DS.$itemname.".php");
            $obj = new $itemname($db);

        }
        $obj->show_admin_config();

	}
	
	function savePaymentItemConfig($itemname)
	{
        if ($this->price_plugins[$itemname]){
            $obj = $this->price_plugins[$itemname];
        }else{
            $db = & JFactory::getDBO();
    		JLoader::register($itemname,$this->plugin_dir.DS."pricing".DS.$itemname.".php");
            $obj = new $itemname($db);

        }

        $obj->save_admin_config();
	}
    
	function savePaymentItemOrdering() {

        $database =& JFactory::getDBO();
        $varnames=array_keys($_REQUEST);
        for($i=0;$i<count($varnames);$i++){
            if(substr($varnames[$i],0,6)=='order-'){
                $neworder=$_REQUEST[$varnames[$i]];
                $itemname=substr($varnames[$i],6);
                $database->setQuery("UPDATE ".PAYTABLE_PRICING." SET ordering='$neworder' WHERE itemname='$itemname'");
                $database->query();
            }
        }

    }

    function enablePaymentItem() {

    	$cid	= JRequest::getVar('cid', array(0), '', 'array');
    	$database =& JFactory::getDBO();
        for($i=0;$i<count($cid);$i++){
    		$database->setQuery("UPDATE ".PAYTABLE_PRICING." SET enabled=1-enabled WHERE itemname='".$cid[$i]."'");
    		$database->query();
        }
    }
    
    function enablePaymentGateway() {

    	$cid	= JRequest::getVar('cid', array(0), '', 'array');
    	$database =& JFactory::getDBO();
        for($i=0;$i<count($cid);$i++){
    		$database->setQuery("UPDATE ".PAYTABLE_PAYSYSTEMS." SET enabled=1-enabled WHERE classname='".$cid[$i]."'");
    		$database->query();
        }
    }
    
    function showPaymentLog()
    {
        $mainframe = JFactory::getApplication();

		$limit					= $mainframe->getUserStateFromRequest('global.list.limit','limit',$mainframe->getCfg('list_limit'), 'int');
		$limitstart				= $mainframe->getUserStateFromRequest(APP_EXTENSION.".paylog.limitstart",'limitstart',0,'int');
		$filter_keyword   		= $mainframe->getUserStateFromRequest(APP_EXTENSION.".paylog.filter_keyword",'filter_keyword','' ,'string');
		$filter_state     		= $mainframe->getUserStateFromRequest(APP_EXTENSION.".paylog.filter_state",'filter_state','','string');

		$database=&JFactory::getDBO();

    	$where="";
    	$w = array();

        if ($filter_state) $w[]="  p.status='$filter_state' ";
        if ($filter_keyword) $w[]=" (u.username LIKE '%$filter_keyword%' OR p.refnumber LIKE '%$filter_keyword%' OR invoice LIKE '%$filter_keyword%')";
        if (count($w)) $where=" WHERE ".implode(" AND ",$w);

    	$query = "SELECT COUNT(*)"
    	   . "\n FROM ".PAYTABLE_PAYLOG." p
           left join #__users u on p.userid=u.id
    	   $where";
    	$database->setQuery( $query );
    	$total = $database->loadResult();


    	$database->setQuery("SELECT p.*,u.username, a.title as object_item 
    	   FROM ".PAYTABLE_PAYLOG." AS p
    	   LEFT JOIN #__users u ON p.userid=u.id 
    	   LEFT JOIN #__ads as a ON p.object_id = a.id 
    	   $where 
    	   ORDER BY date DESC",$limitstart,$limit);
        $pay_list=$database->loadObjectList();

    	$filter[] = JHTML::_('select.option', '', JText::_('ADS_ALL'), 'text', 'value' );
    	$filter[] = JHTML::_('select.option', 'ok', JText::_('ADS_PAYMENT_OK'), 'text', 'value' );
    	$filter[] = JHTML::_('select.option', 'manual_check', JText::_('ADS_MANUAL_CHECK'), 'text', 'value' );
    	$filter[] = JHTML::_('select.option', 'cancelled', JText::_('ADS_CANCELLED'), 'text', 'value' );
    	$filter[] = JHTML::_('select.option', 'refunded', JText::_('ADS_REFUNDED'), 'text', 'value' );
    	$filter[] = JHTML::_('select.option', 'error',JText::_('ADS_ERROR'), 'text', 'value' );

    	//ok,error,manual_check,cancelled,refunded
    	jimport('joomla.html.pagination');
    	$pageNav = new JPagination( $total, $limitstart, $limit );

    	$lists['filter_state']= JHTML::_('select.genericlist', $filter, 'filter_state', 'class="inputbox" size="1" onchange="document.adminForm.submit();"', 'text', 'value', $filter_state );
        $lists['$filter_keyword']=' <input name="filter_keyword" type="text" value="'.$filter_keyword.'">';

        $rows=array();
        $header_row[]=array("width"=>"5%","th"=>"#");
        $header_row[]=array("width"=>"5%","align"=>"left","th"=>'<input type="checkbox" name="toggle" value="" onclick="checkAll('.count( $pay_list ).');" />');
        $header_row[]=array("width"=>"15%","th"=>JText::_("ADS_USER"));
        $header_row[]=array("width"=>"10%","th"=>JText::_("ADS_DATE"));
        $header_row[]=array("width"=>"5%","th"=>JText::_("ADS_ITEM"));
        $header_row[]=array("width"=>"*%","th"=>JText::_("ADS_PAYMENT_ITEM"));
        $header_row[]=array("width"=>"5%","th"=>JText::_("ADS_PAYMENT_ITEM_AMOUNT"));
        $header_row[]=array("width"=>"5%","th"=>JText::_("ADS_PAYMENT_ITEM_CURRENCY"));
        $header_row[]=array("width"=>"5%","th"=>JText::_("ADS_PAYMENT_ITEM_REFNO"));
        $header_row[]=array("width"=>"5%","th"=>JText::_("ADS_PAYMENT_ITEM_INVOICE_NO"));
        $header_row[]=array("width"=>"5%","th"=>JText::_("IPN"));
        $header_row[]=array("width"=>"5%","th"=>JText::_("ADS_STATUS"));

        $i=0;
        if (count($pay_list))
            foreach ($pay_list as $row)
            {
    			$rows[$i][]=$i+1;
    			$rows[$i][]=array("class"=>"center","td"=>JHTML::_('grid.id', $i+1, $row->id));
                    //JHTML::_('grid.id', $i+1, $row->id);
    			$rows[$i][]=array("class"=>"center","td"=>"$row->username");
    			$rows[$i][]=$row->date;
    			
    			if($row->itemname!="price_contact")
    				$rows[$i][]=$row->object_item;
    			else	
    				$rows[$i][]="";
    			$rows[$i][]=$row->itemname;	
    			$rows[$i][]=number_format($row->amount,2,'.','');;
    			$rows[$i][]=$row->currency;

    			$rows[$i][]=$row->refnumber;
    			$rows[$i][]=$row->invoice;
    			$rows[$i][]=$row->ipn_ip;
    			$rows[$i][]=$row->status;

    			$i++;
            }


        $this->beginForm('payments');
        JTheFactoryAdminHelper::showAdminTableList($header_row,$rows,$pageNav,null, $lists['$filter_keyword'].'&nbsp;&nbsp;'.$lists['filter_state'].'&nbsp');
        
        $this->endForm();
    }

    function showAddNewPayment()
    {
    	$database = JFactory::getDBO();

        $database->setQuery("SELECT * FROM ".PAYTABLE_PACKAGES." WHERE status=1 AND price >= 0 ORDER BY ordering ASC ");
		$packages  = $database->loadObjectList();
		
		if ($packages) {
			$first_index = key($packages);
			$first_package_id = $packages[$first_index]->id;

			$options = array();
	        
	        foreach ($packages as $k => $package ) {
				$options[] = JHTML::_('select.option',$package->id,$package->name);
	        }
			$lists['packages'] = JHTML::_('select.genericlist', $options, "package_id",'class="inputbox" ','value', 'text', $first_package_id);
        
			$database->setQuery("SELECT username as text, id as value FROM #__users WHERE block=0");
        	$filter=$database->loadObjectList();
        	$lists['usernames']= JHTML::_('select.genericlist', $filter, 'usernames', 'class="inputbox" size="1"', 'value', 'text' );

        	$this->htmlAddNewPayment($lists);
		}
    }

    function htmlAddNewPayment(&$lists)
    {
    ?>
         <table width="100%">
         <form action="index.php" method="post" name="adminForm">
          <tr>
           <td colspan="2">
        	<table class="adminheading">
        	 <tr>
        	  <th width="40%"><?php echo JText::_('ADS_NEW_PAYMENT_USER'); ?></th>
        	  </tr>
        	 </table>
        	</td>
           </tr>
           <tr>
        	<td colspan="2">
        	 <table class="adminlist">
        	  <tr>
            	  <td width="10%"><?php echo JText::_('ADS_PAYMENT_ITEM');?></td>
            	  <td><?php echo $lists['itemname'];?></td>
        	  </tr>
        	  <tr>
            	  <td><?php echo JText::_('ADS_PACKAGES');?></td>
            	  <td><?php echo $lists['packages'];?></td>
        	  </tr>
        	  <tr>
            	  <td><?php echo JText::_('ADS_USERNAME');?></td>
            	  <td><?php echo $lists['usernames'];?></td>
        	  </tr>
        	  <tr>
            	  <td><?php echo JText::_('ADS_NOTIFY_USER');?></td>
            	  <td><input type="checkbox" name="notify_user" value="1"></td>
        	  </tr>
        	 </table>
        	</td>
           </tr>
          <input type="hidden" name="option" value="<?php echo APP_EXTENSION;?>" />
          <input type="hidden" name="task" value="savepayment" />
         </form>
         </table>
        <?php
    }
    
    function saveAddNewPayment()
    {
		require_once($this->plugin_dir.DS.'payment'.DS."payment_log.php");     
			
    	$db = JFactory::getDBO();

        $itemname=  JRequest::getVar( 'itemname', '' );
        $nr_items=  JRequest::getVar( 'nr_items', 0 );
        $user=  JRequest::getVar( 'usernames', 0 );
        $notify_user=  JRequest::getVar( 'notify_user', 0 );

        $price_classname	= "price_$itemname";
        
        if ($this->price_plugins[$price_classname]){
            $price_class	= $this->price_plugins[$price_classname];
        } else {
            
    		JLoader::register($price_classname,$this->plugin_dir.DS."pricing".DS.$price_classname.".php");
            $price_class	= new $price_classname($db);
        }
        
        $price_class->acceptOrder($user,$nr_items);

        $paylog	= new JTheFactoryPaymentLog($db);
        $paylog->id			= null;
        $paylog->amount		= $nr_items * $price_class->getPrice($_REQUEST);
        $paylog->currency	= $price_class->getCurrency($_REQUEST);
        $paylog->date		= date('y-m-d H:i');
        $paylog->ipn_ip		= $_SERVER['REMOTE_ADDR'];
        $paylog->refnumber	= 'admin';
        $paylog->userid		= $user;
        $paylog->itemname	= $itemname;
        $paylog->status		= 'ok';
        $paylog->store();
        
        // how can you identify a payment with a user!!!?!?!
        $price_classname	= "price_$paylog->itemname";
        
        if ($this->price_plugins[$price_classname]){
            $price_class	= $this->price_plugins[$price_classname];
        } else {
            $db = & JFactory::getDBO();
    		JLoader::register($price_classname,$this->plugin_dir.DS."pricing".DS.$price_classname.".php");
            $price_class	= new $price_classname($db);

        }

        $quantity	= $paylog->amount / $price_class->getPrice($_REQUEST);
        $price_class->acceptOrder($paylog, $quantity);
        $paylog->status = 'ok';
        $paylog->store();

        //TODO: send mail
    }
    
    function approvePayment()
    {
		require_once($this->plugin_dir.DS.'payment'.DS."payment_log.php");     
    	
		$database = & JFactory::getDBO();
        $id = intval( JRequest::getVar( 'id', 0 ) );
        if (!$id){
            $cid = JRequest::getVar('cid', array() ) ;
            $id=$cid[0];
        }
        
        $paylog	= new JTheFactoryPaymentLog($database);

        if (!$paylog->load($id)) return false;

        if ($paylog->status == 'ok') return true;

        $price_classname="price_$paylog->itemname";
        if ($this->price_plugins[$price_classname]){
            $price_class = $this->price_plugins[$price_classname];
        }else{
            $db = & JFactory::getDBO();
    		JLoader::register($price_classname,$this->plugin_dir.DS."pricing".DS.$price_classname.".php");
            $price_class = new $price_classname($db);

        }
		
        $quantity	= $paylog->amount / $price_class->getPrice($_REQUEST);
        
        // htw can you identify a payment with a user!!!?!?!
        $price_class->acceptOrder($paylog, $quantity);
        $paylog->status='ok';
        $paylog->store();
        
        return true;
    }
    
    function deletePaylog(){
    	$mainframe = JFactory::getApplication();
    	
		require_once($this->plugin_dir.DS.'payment'.DS."payment_log.php");     
    	
		$database = & JFactory::getDBO();
		$cid = JRequest::getVar('cid', array() ) ;
		
        $paylog=new JTheFactoryPaymentLog($database);
        if(is_array($cid) && count($cid)>0)
        foreach ($cid as $ci)
			if ($paylog->load($ci)) $paylog->delete();

        return true;
    }

}

?>