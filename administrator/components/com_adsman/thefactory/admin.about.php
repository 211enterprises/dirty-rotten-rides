<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

$document = JFactory::getDocument();
$js = '(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, \'script\', \'facebook - jssdk\'));';
$document->addScriptDeclaration($js);


class JTheFactoryAbout extends JObject{
	
	var $latestversion	= null;
	var $downloadlink	= null;
	var $newsletter		= null;
	var $announcements	= null;
	var $releasenotes	= null;
	var $_componentname	= null;

	function __construct()
    {
        define('DOMIT_INCLUDE_PATH', JPATH_ROOT . DS . 'libraries' . DS . 'domit' . DS );
 		$this->_componentname=APP_EXTENSION;
		return $this->GetInfo();

    }
	function &getInstance()
	{
		static $instances;
		if (!isset( $instances ))
			$instances = new JTheFactoryAbout();

		return $instances;
	}

	function GetInfo(){

		require_once(JPATH_ROOT.DS.'libraries'.DS.'joomla'.DS.'utilities'.DS.'simplexml.php');
		@set_time_limit(60);
		
        $Tapp=&JTheFactoryApp::getInstance();

		$filename=$Tapp->getIniValue('version_root').'/'.$this->_componentname.".xml";

        $doc=JTheFactoryHelper::remote_read_url($filename);
        $xml=&JFactory::getXML($doc,false);

		$this->latestversion = (string)$xml->latestversion;
        $this->downloadlink = (string)$xml->downloadlink;
        $this->versionhistory = (string)$xml->versionhistory;
		$this->aboutfactory = html_entity_decode((string)$xml->aboutfactory);
        $this->otherproducts = html_entity_decode((string)$xml->otherproducts);

		$this->build = (string)$xml->build;

		return true;
	}

	function showAbout()
	{
    	$ver1=explode('.',COMPONENT_VERSION);
    	$ver2=explode('.',$this->latestversion);
    	
		$isNew=false;

        echo '<h2><a href="#" style="text-decoration: none;">Latest Release Notes</a></h2>';

    	$ver_info= JText::_('ADS_VERSION_IS_UP_TO_DATE');

    	$n = count($ver1);
    	for($i=0; $i < $n; $i++)
		{
    	    if (intval($ver1[$i])<intval($ver2[$i]))
			{				
		        $isNew=true;
	    	    $ver_info= JText::_('ADS_NEW_VERSION_AVAILABLE');
		        break;
	    	}
    	    if (intval($ver1[$i])>intval($ver2[$i]))
			{
    	        $isNew=false;
          	    break;
    	    }
    	}
    	
    	switch ($isNew) {
    		case true: ?>
    			<div id="info_ver">
		    	    <span style="color:red; font-size: 13px; font-weight: bold;"><?php echo $ver_info; ?> </span><br />
		    	</div>
		    	<?php break;
		    case false: ?>
		    	<div id="info_ver">
		    	    <span style="color:green; font-size: 13px;font-weight: bold;"><?php echo $ver_info; ?> </span><br />
		    	</div>	
    	<?php } ?>

        <div id="info_div" style="float: left; width: 260px;margin-top:20px;">
            <?php echo JText::_('ADS_INSTALLED_VERSION'). '<span style="font-weight: bold;">'.  COMPONENT_VERSION .'</span>'; ?><br/>
            <?php echo JText::_('ADS_LATEST_VERSION_AVAILABLE'). '<span style="font-weight: bold;">'. $this->latestversion.'</span>'; ?><br/><br/>
            <?php if ($this->versionhistory) echo $this->versionhistory; ?>
        </div>
        <div style="float: left; margin-top:20px;">
            <div class="fb-like" data-href="https://www.facebook.com/theFactoryJoomla" data-send="false" data-layout="box_count" data-width="450" data-show-faces="false"></div>
        </div>
        <div style="clear: both; line-height: 1px;">&nbsp;</div>

        <h2><a href="#" style="text-decoration: none;"><?php echo JText::_('ADS_SUPPORT_AND_UPDATES');?></a></h2>
    	<?php if ($isNew) { ?>
    	    <div id="download_div" style="border:1px solid black;width:350px;margin-top:20px;">
    	    <?php echo $this->newdownloadlink; ?>
    	    </div>
    	<?php
    	} else { ?>
            <div style="margin-top:20px;">
    	        <?php echo $this->downloadlink; ?>
            </div>
    	<?php } ?>

        <h2><a href="#" style="text-decoration: none;"><?php echo JText::_('ADS_OTHER_PRODUCTS');?></a></h2>
        <?php if ($this->otherproducts): ?>
            <div>
                <?php echo $this->otherproducts; ?>
            </div>
        <?php endif; ?>

        <h2><a href="#" style="text-decoration: none;"><?php echo JText::_('ADS_ABOUT_THE_FACTORY');?></a></h2>
        <?php if ($this->aboutfactory): ?>
            <div>
                <?php echo $this->aboutfactory; ?>
            </div>
        <?php endif; ?>

    	<form action="index.php" method="post" name="adminForm">
    	<input type="hidden" name="option" value="com_adsman" />
    	<input type="hidden" name="task" value="" />
    	</form>
    	<?php

	}

}

?>

