<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/


class JTheFactoryCategoryTable extends JFactorySystemTable {
	var $id                = null;
	var $catname           = null;
	var $description       = null;
	var $parent            = null;
	var $hash              = null;
	var $ordering          = null;

	function __construct( $table=APP_CATEGORY_TABLE,$key="id", $db=null )
	{
		if(!isset($db)){
			$db = & FactoryLayer::getDB();
		}
		parent::__construct($table,$key,$db);
	}
}

class JTheFactoryCategorySefTable extends JFactorySystemTable {
	var $id                = null;
	var $categories        = null;
	var $catid		       = null;

	function __construct( $table=APP_CATEGORY_TABLE_SEF,$key="id", $db=null )
	{
		if(!isset($db)){
			$db = & FactoryLayer::getDB();
		}
		parent::__construct($table,$key,$db);
	}
}

?>