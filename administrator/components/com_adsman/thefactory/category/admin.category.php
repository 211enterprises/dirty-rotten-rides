<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/


class JTheFactoryCategory extends JTheFactoryCategoryObj
{
	

    function __construct($cattable=APP_CATEGORY_TABLE,$depth=1)
    {
        $this->category_depth = $depth;
        $this->category_table = $cattable;
        $FactoryLayerClass = new FactoryLayer();
        $this->db =& $FactoryLayerClass->getDB();
        //$this->db = FactoryLayer::getDB();
    }
    
	function &getInstance($cattable=APP_CATEGORY_TABLE,$depth=1)
	{
		static $instances;

		if (!isset( $instances ))
			$instances = new JTheFactoryCategory($cattable,$depth);

		return $instances;
	}
	
	function route(&$task, &$act){
		
		if (method_exists($this,$task)){
			return $this->$task($act);
		}
	}
	
	function admin_Categories(){
	    /*@var $cat JTheFactoryCategory*/
	    $cat=JTheFactoryCategory::getInstance(APP_CATEGORY_TABLE,APP_CATEGORY_DEPTH);
	    $cat->showCategories();
	    return true;
	}
	
	function admin_DelCategories()
	{
	    /*@var $cat JTheFactoryCategory*/
	    $cat=JTheFactoryCategory::getInstance(APP_CATEGORY_TABLE,APP_CATEGORY_DEPTH);
	    $nr=$cat->deleteCategories();
        $japp=&JFactory::getApplication();
        $japp->redirect('index.php?option='.APP_EXTENSION.'&task=categories',$nr." ".JText::_('ADS_CATEGORIES_DELETED'));
	    return true;

	}
	
	function admin_EditCat()
	{
		$cids = JRequest::getVar("cid",array());
		
		if (is_array($cids) & !empty($cids)) $cids=$cids[0];
	    /*@var $cat JTheFactoryCategory*/
	    $cat=JTheFactoryCategory::getInstance(APP_CATEGORY_TABLE,APP_CATEGORY_DEPTH);
		$cat->showEditCategory($cids);
	    return true;
	}
	function admin_SaveCat()
	{
	    /*@var $cat JTheFactoryCategory*/
	    $cat=JTheFactoryCategory::getInstance(APP_CATEGORY_TABLE,APP_CATEGORY_DEPTH);
	    $cat->saveCategory();
        $japp=&JFactory::getApplication();
        $japp->redirect('index.php?option='.APP_EXTENSION.'&task=categories',JText::_('ADS_CATEGORY_SAVED'));
	    return true;
	}
	function admin_NewCat()
	{
	    /*@var $cat JTheFactoryCategory*/
	    $cat=JTheFactoryCategory::getInstance(APP_CATEGORY_TABLE,APP_CATEGORY_DEPTH);
		$cat->showEditCategory(null);
	    return true;

	}
	function admin_SaveCatOrder()
	{
	    /*@var $cat JTheFactoryCategory*/
	    $cat=JTheFactoryCategory::getInstance(APP_CATEGORY_TABLE,APP_CATEGORY_DEPTH);
	    $cat->saveCatOrder();
        $japp=&JFactory::getApplication();
        $japp->redirect('index.php?option='.APP_EXTENSION.'&task=categories',JText::_('ADS_ORDERING_SAVED'));
	    return true;

	}
	function admin_QuickAddCat()
	{
	    /*@var $cat JTheFactoryCategory*/
	    $cat=JTheFactoryCategory::getInstance(APP_CATEGORY_TABLE,APP_CATEGORY_DEPTH);
        $cat->QuickAddCat();

        $japp=&JFactory::getApplication();
        $japp->redirect('index.php?option='.APP_EXTENSION.'&task=categories',JText::_('ADS_CATEGORY_ADDED'));
	    return true;
	}
	
    function admin_sef_category(){
	    /*@var $cat JTheFactoryCategory*/
	    $cat=JTheFactoryCategory::getInstance(APP_CATEGORY_TABLE,APP_CATEGORY_DEPTH);
        $cat->build_sef_table();

        $japp=&JFactory::getApplication();
        $japp->redirect('index.php?option='.APP_EXTENSION.'&task=categories',JText::_('ADS_CATEG_SEF_TABLE_CREATED'));
	    return true;
    	
    }
	
	function showJS()
	{
	    ?>
        	<script language="javascript" type="text/javascript">
        	function delecat(nr,enable){
                el=document.getElementById('cat_new_'+nr);
                el.disabled=enable;
            }
            function showQuickAdd()
            {
                el=document.getElementById('quickadd');
                el.style.display='block';
                el=document.getElementById('quickaddbutton');
                el.style.display='none';
            }
        	</script>
	    <?php
	}
	
	function QuickAddBox()
	{
		?>
			<div id="quickaddbutton">
				<button class="button"  onclick="showQuickAdd();" type="button"><?php echo JText::_('ADS_QUICKADD');?></button>
				</div>
				<div style="display:none;" id="quickadd"><br />
				<span style="font-size:14px;font-weight:bolder"><?php echo JText::_('ADS_QUICKADD_HELP'); ?></span><br /><br />
				<textarea name="quickadd" cols="40" rows="10"></textarea><br />
				<button class="button" onclick="submitform('quickaddcat');"><?php echo JText::_('ADS_QUICKADD');?></button>
			</div>
		<?php
	}
	
	function htmlEditCategory(&$row,&$list)
	{
		?>
			<table class="adminForm" align="left">
				<tr valign="top">
				  <td width="10%"><?php echo JText::_('ADS_NAME');?>:&nbsp;</td>
				  <td>
				   <input type="text" name="catname" value="<?php echo isset($row->catname)?$row->catname:null;?>" />
				  </td>
			 	</tr>
				<tr valign="top">
				  <td width="10%"><?php echo JText::_('ADSMAN_DESCRIPTION');?>:&nbsp;</td>
				  <td>
				   <textarea name="description" cols="30" rows="10"><?php echo isset($row->description)?$row->description:null;?></textarea>
				  </td>
			 	</tr>
			 	<tr valign="top">
				  <td width="10%"><?php echo JText::_('ADS_CAT_PARENT');?>:&nbsp;</td>
				  <td>
				   	<?php echo $list['cats'];?>
				  </td>
			 	</tr>
			</table>
		<?php

	}
	function getCategoryTree()
	{
	    /*@var $db JDatabaseMySQL */
		$cats = $this->build_child(0,true);
		return $cats;
	}
    function showCategories()
    {
    	JTheFactoryCategory::showJS();
        $JTheFactoryAdminHelperClass = new JTheFactoryAdminHelper();
        $JTheFactoryAdminHelperClass->beginAdminForm("",null,'post',true,JText::_('ADS_CATEGORIES_MANAGEMENT'));

		//$cats=$this->getCategoryTree();
		$cats = $this->build_child(0,true);

        $rows=array();
        $rCursor = 0;
        $header_row[]=array("width"=>"3%","class"=>"left","th"=>'<input type="checkbox" name="toggle" value="" onclick="checkAll('.(count( $cats )).');" />');
        $header_row[]=array("width"=>"5%","class"=>"left", "nowrap"=>"nowrap","th"=>JText::_("ADS_ORDERING").'<img src="'.JURI::root().'administrator/components/'.APP_EXTENSION.'/thefactory/category/assets/filesave.png" alt="'.JText::_('ADS_SAVE_ORDERING').'" onclick="submitbutton(\'savecatorder\');" />');
        $header_row[]=array("width"=>"25%","class"=>"left", "th"=>JText::_("ADS_MENU_CATEGORIES"));
        $header_row[]=array("width"=>"*%","class"=>"left", "th"=>JText::_("ADSMAN_DESCRIPTION"));
        $header_row[]=array("width"=>"5%","th"=>JText::_("ADS_STATUS"));
        $header_row[]=array("width"=>"5%","th"=>JText::_("ID"));

        foreach($cats as $i => $cat)
        {
        	$id = $cat["id"];
       		$offset = "";
       		$sep = "";
       		
        	if($cat["depth"]>0 ) {
	       		$offset = "";
        		$sep='<sup>|<span style="text-decoration:underline;">&nbsp;&nbsp;&nbsp;</span></sup>';
				for($j=0;$j<=$cat["depth"];$j++){
					$offset .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
				}
        	}

	        if($id>0){
		        $rows[$rCursor][]=JHTML::_('grid.id',$id,$id,false);
	        	$rows[$rCursor][]=array("class"=>"center","td"=>"<input name='order_{$id}' type='text' class='inputbox' size='1' value='".$cats[$i]['ordering']."' style='float:none !important' />");
		        $link="<a href='index.php?option=".APP_EXTENSION."&amp;task=editcat&amp;cid=".$id."'>".$cats[$i]["catname"]."</a>";
		    	$rows[$rCursor][]=$offset.$sep.$link;
		    	
		        if(isset($cats[$i]["description"]))
			        $rows[$rCursor][]=$cats[$i]["description"];
				else 
			        $rows[$rCursor][]="&nbsp;";
			        
				if ( isset($cats[$i]["status"]) && $cats[$i]["status"]== 1 ) {
					$img = 'tick.png';
					$alt = JText::_( 'ADS_PUBLISH_UNPUBLISH' );
					$task = "unpublish_cat";
				} else {
					$img = 'publish_x.png';
					$alt = JText::_( 'ADS_UNPUBLISHED_PUBLISH' );
					$task = "publish_cat";
				}
				
				$status ="<span class=\"editlinktip hasTip\" title=\"".JText::_( 'ADS_PUBLISH_INFORMATION  '.$alt )."\">";
			 	$status .="<a href=\"index.php?option=".APP_EXTENSION."&task=$task&cid[]=$id\" >";
			 	$status .="<img src=\"".JURI::root()."administrator/components/".APP_EXTENSION."/img/".$img."\" width=\"16\" height=\"16\" border=\"0\" alt=\"".$alt."\" /></a></span>"; 
			 
				$rows[$rCursor][]=array("class"=>"center","td"=>$status);
                $rows[$rCursor][]=array("class"=>"right","td"=>$cats[$i]["id"]);
	        }
        	else{
        		$rows[$rCursor][]="";
        		$rows[$rCursor][]="";
		    	$rows[$rCursor][]=$cats[$i]["catname"];
		        $rows[$rCursor][]=$cats[$i]["description"];
		        $rows[$rCursor][]="";
                $rows[$rCursor][]=array("class"=>"right","td"=>$cats[$i]["id"]);
        	}
        	$rCursor++;
        }

        $JTheFactoryAdminHelperClass->showAdminTableList($header_row,$rows,null,JText::_('ADS_CATEGORIES_MANAGEMENT'));

		JTheFactoryCategory::QuickAddBox();
        $JTheFactoryAdminHelperClass->endAdminForm();
    }
    function deleteCategories()
    {

		$cids = JRequest::getVar("cid",array());
		if (!is_array($cids) && $cids) $cids=array($cids);
		$nr=0;
        for($i=0;$i<count($cids);$i++){
			$this->db->setQuery("delete from `".APP_CATEGORY_TABLE."` where id=".$cids[$i]);
			if($this->db->query()){
				$this->db->setQuery("delete from `".APP_CATEGORY_TABLE."_sef` where catid=".$cids[$i]);
				$this->db->query();
				$this->db->setQuery("update `".APP_CATEGORY_TABLE."` set parent=0 where parent=".$cids[$i]);
				$this->db->query();
				$nr++;
			}
			//$this->db->setQuery("update `".APP_CATEGORY_TABLE."` set category=0 where id=".$cids[$i]);
			//$this->db->query();
		}
		return $nr;
    }
	function showEditCategory($id)
	{
		
		$row = new stdClass();
		$hidden_fields = array();
		
		if ($id) {
      	 	$this->db->setQuery("select * from `".APP_CATEGORY_TABLE."` where `id`='".$id."'");
  	   	 	$row=$this->db->loadObject();
		 	$hidden_fields["id"]=$id;
		
			$list['cats'] =$this->makeCatTree($row->parent,"parent","200");
		
		} else {
			$root = $this->build_child(0, 1);
			$list['cats'] =$this->makeCatTree($root[0]['catname'],"parent","200");
		}

        JTheFactoryAdminHelper::beginAdminForm("savecat",$hidden_fields,'post', JText::_('ADS_CATEGORY_EDIT'));
		JTheFactoryCategory::htmlEditCategory($row,$list);
        JTheFactoryAdminHelper::endAdminForm();
	}
	function saveCategory()
	{
        $catobj=new JTheFactoryCategoryTable();
        $catobj->bind($_REQUEST);

        require_once (JPATH_ROOT.DS.'components'.DS.'com_adsman'.DS.'options.php');

        if (ads_opt_transliterate == 1) {
            $catobj->hash = JTheFactoryLangHelper::transliterate($catobj->catname);
    	    $catobj->hash = md5(strtolower(JFilterOutput::stringURLSafe($catobj->hash)));
        } else {
            $catobj->hash = md5(strtolower(JFilterOutput::stringURLUnicodeSlug($catobj->catname)));
        }

    	if($catobj->store()){
    		
			
			$qu = $this->string_cat_name($catobj->id,0);
			
			if('WIN' == substr(PHP_OS,0,3)){
				$separator = "\r\n";
			}
			else{
				$separator ="\n";
			}
			$catSEF = new JTheFactoryCategorySefTable(APP_CATEGORY_TABLE_SEF );
			$catSEF->id = null;
			$catSEF->load(array("catid"=>$catobj->id));
			
			$catSEF->categories = str_replace("/",$separator,$qu);
			$catSEF->catid = $catobj->id;
			$catSEF->store();
		}
		
	}
	function saveCatOrder()
	{
        JLoader::register('JTheFactoryCategoryTable',dirname(__FILE__).DS.'table.category.php');

	    $cat=new JTheFactoryCategoryTable();

		foreach ($_REQUEST as $k=>$v){
	        if (substr($k,0,6)=='order_'){
	            $id=substr($k,6);
	            $cat->load($id);
	            $cat->ordering=$v;
	            $cat->store();
	        }
	    }
	}
	/**
	 * Quick endless level categories to ROOT
	 *  
	 * @modified: 23/09/09
	 **/
	function QuickAddCat() {

	    //$textcats = JArrayHelper::getValue($_REQUEST,'quickadd','');
	    $textcats = JRequest::getVar('quickadd','','POST','string');
		
		// big moama
		$big_parent_id =  0 ;//JRequest::getVar('parent');

		if('WIN' == substr(PHP_OS,0,3)){
			$separator = "\r\n";
		}
		else{
			$separator ="\n";
		}
		
		$textcats = explode($separator,$textcats);
		$stack = array($big_parent_id);
		
		$first_categ = $textcats[0];
			
		if (array_count_values($textcats) > 1 ) 
		{
			$first_prevcat = $textcats[1];
			$cat_space_initial = $this->getFirstSpaces($first_prevcat);
		} else {
			$cat_space_initial = 0;
		}

		$i=0;
		$last_id = null;
		
		foreach($textcats as $key=>$cat) {
			
			$prevcat = isset($textcats[$i-1]) ? $textcats[$i-1] : '';
			
			$prevcat_spaces = $this->getFirstSpaces($prevcat);
			
			$cat_spaces = $this->getFirstSpaces($cat);

			if($cat_spaces > $prevcat_spaces ) {

				//if that moron user puts too many spaces in front of the child category, here we make things right
				if($cat_spaces - $prevcat_spaces > $cat_space_initial) {
					$new_cat_spaces = '';
					
					for($j=0; $j<$prevcat_spaces+1; $j++){
						$new_cat_spaces .= ' ';
					}
					$textcats[$key] = $new_cat_spaces.trim($cat);
				}
							
				array_push($stack, $last_id );

			}
			if($cat_spaces < $prevcat_spaces) {
		
				$diff_level = $prevcat_spaces-$cat_spaces;
				if ($cat_space_initial!=0)
					$diff_level_back =  $diff_level / $cat_space_initial; // back just 1 level
				else 
					$diff_level_back = 1;
		
				for($j=0;$j<$diff_level_back;$j++){
					array_pop($stack);
				}
			}

		    $cati=new JTheFactoryCategoryTable();
	        $cati->id=null;
	        $cati->catname=trim($cat);
	        $cati->parent=end($stack);
	        $cati->ordering=0;// fo later
	        $cati->hash = $cati->catname;


            //require_once (JPATH_ROOT.DS."components".DS."com_adsman".DS."config.php");
            require_once (JPATH_ROOT.DS.'components'.DS.'com_adsman'.DS.'options.php');

            if (ads_opt_transliterate == 1) {
                $cati->hash = JTheFactoryLangHelper::transliterate($cati->catname);
                $cati->hash = md5(strtolower(JTheFactoryHelper::str_clean($cati->hash)));
            } else {
                $cati->hash = md5(strtolower(JFilterOutput::stringURLUnicodeSlug($cati->catname)));
            }

            if($cati->catname!="")
	    		if($cati->store()) {
	    			
	    			$last_id = $cati->id;
	    			
					$qu = $this->string_cat_name($cati->id,0);
					if('WIN' == substr(PHP_OS,0,3)){
						$separator = "\r\n";
					}
					else{
						$separator ="\n";
					}
					
					$catSEF = new JTheFactoryCategorySefTable(APP_CATEGORY_TABLE_SEF );
					$catSEF->id = null;
					$catSEF->load(array("catid"=>$cati->id));
					//$catSEF->load(array("catid"=>$catobj->id));
					
					$catSEF->categories = str_replace("/",$separator,$qu);
					$catSEF->catid = $cati->id;
					$catSEF->store();
	    		}
			$i++;
		}
//exit();
	}
	
	// move categories in other parent category
	function admin_showMoveCategories(){

		$database = & JFactory::getDBO();
		
		$cid	= JRequest::getVar( 'cid', array(0), '', 'array' );
		$cid_list = implode("," , $cid);
		
		$changed_cats = array();
		$changed_cats = array();
		$database->setQuery("SELECT * FROM ".APP_CATEGORY_TABLE." WHERE id IN ($cid_list);");
		$changed_cats = $database->loadObjectList();
		
		$list = array();
		$list["cats"] = $changed_cats;
		
		$list['parent'] =$this->admin_makeCatTree(0,"parent","200",$cid);

		$hidden_fields = array("cid"=> $cid_list );
        JTheFactoryAdminHelper::beginAdminForm("doMoveCategories",$hidden_fields,JText::_('ADS_MOVE_CATEGORIES'));
		JTheFactoryCategory::htmlMoveCategory($list);
		JTheFactoryAdminHelper::endAdminForm();
		return true;
	}
	
	function htmlMoveCategory($list){
?>
<table class="paramlist admintable" width='100%'>
<tr>
  <td class="paramlist_key" style="width:40% !important;" >Move categories:</td>
  <td>
	  <?php foreach ($list["cats"] as $cat){ 
		echo $cat->catname."<br />";
	  }?>
  </td>
</tr>
<tr>
  <td class="paramlist_key">To category:</td>
  <td>
	<?php echo $list["parent"];?>
  </td>
</tr>
<tr>
  <td colspan="2" align="center" class="paramlist_key">
  &nbsp;
  </td>
</tr>
<tr>
  <td colspan="2" align="center">
	<a href="#" onclick="document.adminForm.submit();"><img src="<?php echo JURI::root();?>/administrator/components/<?php echo APP_EXTENSION;?>/thefactory/category/assets/move_f2.png" style="vertical-align:middle !important;" />Move</a>
  </td>
</tr>
</table>
<?php
	}

	
	function admin_doMoveCategories(){
		$cid	= JRequest::getVar( 'cid',"0", '', 'string' );
		$parent	= JRequest::getVar( 'parent',"0", '', 'string' );
		
		$database = & JFactory::getDBO();
		$database->setQuery("UPDATE ".APP_CATEGORY_TABLE." SET parent = '{$parent}' WHERE id IN ({$cid})");
		$database->query();
		$nr = $database->getAffectedRows();

        //$this->admin_sef_category();
        $cat=JTheFactoryCategory::getInstance(APP_CATEGORY_TABLE,APP_CATEGORY_DEPTH);
        $cat->build_sef_table();


        $japp=&JFactory::getApplication();
        $japp->redirect('index.php?option='.APP_EXTENSION.'&task=categories',$nr." ".JText::_('ADS_CATEGORIES_MOVED'));
	}

	
	function admin_publish_cat(){
		
		$cids = JRequest::getVar("cid",array());
		$nr = count($cids);
		$cids_string = implode(",",$cids);
		
		$this->db->setQuery("UPDATE `".APP_CATEGORY_TABLE."` SET status = 1 WHERE id in ($cids_string)");
		$this->db->query();
		
        $japp=&JFactory::getApplication();
        $japp->redirect('index.php?option='.APP_EXTENSION.'&task=categories',$nr." ".JText::_('ADS_CATEGORIES_PUBLISHED'));
	}

	function admin_unpublish_cat(){
		
		$cids = JRequest::getVar("cid",array());
		$nr = count($cids);
		$cids_string = implode(",",$cids);
		
		$this->db->setQuery("UPDATE `".APP_CATEGORY_TABLE."` SET status = 0 WHERE id in ($cids_string)");
		$this->db->query();
		
		
        $japp=&JFactory::getApplication();
        $japp->redirect('index.php?option='.APP_EXTENSION.'&task=categories',$nr." ".JText::_('ADS_CATEGORIES_UNPUBLISHED'));
	}
	

	/**
	 * Gets all categories and makes links for them
	 *
	*/
	function build_sef_table(){
			
		$cats = array();
		$this->db->setQuery("TRUNCATE TABLE `".APP_CATEGORY_TABLE."_sef`");
		$this->db->query();
		
		$this->db->setQuery("SELECT id FROM `".APP_CATEGORY_TABLE."`");
		$cats = $this->db->loadObjectList();

		foreach ($cats as $cati) {
			
			$qu = $this->string_cat_name($cati->id,0);
			
			if('WIN' == substr(PHP_OS,0,3)){
				$separator = "\r\n";
			}
			else{
				$separator ="\n";
			}
			
			$catSEF = new JTheFactoryCategorySefTable(APP_CATEGORY_TABLE_SEF, "catid", $this->db);
			$catSEF1 = array();
											
			if (!$catSEF->load(array("catid"=>$cati->id))) {
				//no record yet
				$catSEF->set("_tbl_key","id");
				$catSEF->id=null;
			} else {
				$catSEF->set("_tbl_key","catid");
			}
						
			$catSEF1['categories'] = str_replace("/",$separator,$qu);
			$catSEF1['catid'] = $cati->id;
					
		    if (!$catSEF->bind($catSEF1))
		    {
		      $this->setError($this->db->getErrorMsg());
		      return false;
		    }
	  	    
		    if (!$catSEF->check())
		    {
		      $this->setError($this->db->getErrorMsg());
		      return false;
		    }
		
		    if (empty($catSEF->id)) {
		
			    if (!$catSEF->store())
			    {
			      $this->setError($this->db->getErrorMsg());
			      return false;
			    }
		    }
		}
	}
	
	/**
	 * Add a SEF link to a selected category
	 *
	 */
	function add_sef_link(){
		// to do
	}
	
	function admin_makeCatTree($selected_category="", $id_select="cat", $width=200, $disabled=array(), $include_root=1, $all=0, $kids_only=false){
		
		if(! isset($this->category_table) )
			$this->category_table = APP_CATEGORY_TABLE;
		$database = &JFactory::getDBO();
		$html_tree = "";
		
		//$spacer = "&nbsp;&nbsp;&nbsp;";
		$spacer = "&#45;&gt;";
		
		$cat_tree = array();
		if ($all) 
			$cat_tree[]=JHTML::_('select.option','',$all);
				
		$cat = $this;
		if($kids_only)	
			$cats = $cat->getActiveTree($include_root);
		else	
			$cats = $cat->build_child(0, $include_root);
				
		
		$nr_pcats = count($cats);
		if($nr_pcats>0){
			foreach($cats as $cIndex => $category){

			
				$cid = $category["id"];
				
				$leveledSpacer = "";
				for($i=0; $i<$category["depth"];$i++) 
					$leveledSpacer .= $spacer;
				
				$dis = false;
				if( in_array($category["id"],$disabled) )
					$dis = true;

				$category_name = 	stripslashes($leveledSpacer.$category["catname"]).PHP_EOL;		
				$cat_tree[] = JHTML::_('select.option',	$category["id"],$category_name, "value", "text", $dis );
			}
		}
			
		if (count($nr_pcats)>0){
			$html_tree = JHTML::_('select.genericlist', $cat_tree,$id_select,'class="inputbox" style="width:'.$width.'px;" ','value', 'text',$selected_category);
		}else
			$html_tree = '&nbsp;';
		return $html_tree;
	}
	

}

?>