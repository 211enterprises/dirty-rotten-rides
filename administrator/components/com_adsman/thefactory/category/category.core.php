<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/


class JTheFactoryCategoryObj extends JObject {
	var $id                = null;
	var $catname           = null;
	var $description       = null;
	var $parent            = null;
	var $hash              = null;
	var $ordering          = null;
	
    var $category_table=null;
    var $category_depth=null;
    var $category_matrix=null;
    
    /**
     * @since Ads 1.5.4
     */
    var $show_active_only = false;

	function __construct($cattable=APP_CATEGORY_TABLE,$depth=1)
	{
		
        $this->category_depth=$depth;
        $this->category_table=$cattable;
        
		if(!isset($db)){
			$db = JFactory::getDBO();
		}
	}
	
	function getFirstSpaces($string)
	{
		$count = 0;
		$nr=strlen($string);
		if(!$nr){
			return $count;
		}
		for($i=0; $i<=$nr; $i++){
			if(' ' == $string[$i]){
				$count++;
			}
			else{
				break;
			}
		}
		return $count;
	}
	
	function has_children($id, $show_active_only=false){
	
		if( !isset($this->category_table) )
			$this->category_table = APP_CATEGORY_TABLE;
		
		$active_filter = "";
		if($show_active_only==true)
			$active_filter = " AND status = 1 ";
			
		$database = JFactory::getDbo();

		$database->setQuery("select count(*) from $this->category_table where parent='".$id."' $active_filter ");
		$count = $database->loadResult();
		return $count;
	}
    
	function build_child( $id, $include_parent=false ){
		$tree = $this->build_children_tree($id, $include_parent,$flag_reset=false);
		return $tree;
	}

	function build_children_tree($pid,$include_parent=false,$flag_reset=false){
			
		$database = JFactory::getDbo();
		
		$active_filter = "";
		if($this->show_active_only==true)
			$active_filter = " status =1 AND ";
	
		static $treeCat = array(),$depth=0;
		if($flag_reset){
			$treeCat = array();
		}
		if($pid==='' || $pid<0) return;

		//adding the parent element to the tree
		if ($include_parent && $depth==0) {
			if ($pid>0) {
				$q = "SELECT c.* FROM $this->category_table c WHERE $active_filter id='".$pid."' LIMIT 1";

                $database->setQuery($q);
				$base_parent=$database->loadAssocList('id');
				$key = key($base_parent);
				$treeCat[$key] = $base_parent[$key];
				$treeCat[$key]["depth"]= 0;
				$treeCat[0]["parent"] = @$base_parent["parent"];
				$treeCat[$key]["prev"] = null;
				$treeCat[$key]["next"] = null;
				$treeCat[$key]["nr_children"] = $this->has_children($pid);
			}
			//a special case when $pid=0; obviously, there's no category with id=0, so...
			else {
				$treeCat[0]["id"] = 0;
				$treeCat[0]["catname"] = JText::_("ADS_ROOT_CATEGORY");
				$treeCat[0]["parent"] = null;
				$treeCat[0]["ordering"] = null;
				$treeCat[0]["description"] = 'Base level';
				$treeCat[0]["hash"] = null;
				$treeCat[0]["depth"] = 0;
				$treeCat[0]["nr_children"] = $this->has_children(0);
			}
		}
		
		$q = "SELECT c.* FROM $this->category_table c WHERE $active_filter  parent='".$pid."' ORDER BY ordering, catname ASC ";
		$database->setQuery($q);
		$children = $database->loadAssocList('id');

		//in the next 2 "foreach" structures i'm adding to the "tree" the children of $pid
		$prev_key = null;
		$i=0;
		foreach ($children as $key=>$value){
	
			$child = &$children[$key];
	
			//here i add to each category the 'prev' and 'next' keys, in which we store the prevoius and next categories' ids
			if ( $i==0 ){
				$child['prev']=null;
				if(count($children)==1){
					$child['next']=null;
				}
			}
			elseif ( $i>0 ) {
				$child['prev'] = $children[$prev_key]['id'];
				$children[$prev_key]['next'] = $child['id'];
				if( $i==(count($children)-1) ){
					$child['next'] = null;
				}
			}
	
			//also i add the 'depth' key;
			$child['depth'] = $depth;
	
			//and now the number of children for the child category
			$child['nr_children'] = $this->has_children($child['id']);
	
			$prev_key = $key;
			$i++;
		}
	
		foreach($children as $key=>$value){
	
			$child = &$children[$key];
			//keeping the id indexing
			$treeCat[$key]=$child;
	
			if($child['nr_children']){
				$depth++;
					$this->build_children_tree($child['id'],false,$flag_reset);
				$depth--;
			}
		}
	
		return $treeCat;
	}

	function getActiveTree($include_parent=false){

		$db = & JFactory::getDBO();
		// get categories tree
		// parse upside-down the tree and test each category number of items
		$tree = $this->build_child(0, $include_parent);
		
		$fTree = null;
		if (count($tree)>0) {
			foreach ($tree as $category){
				if($category["nr_children"]>=1){
					$id = $category["id"];
					$q = "
					SELECT a.id
					FROM `#__ads_categories` a
					LEFT JOIN `#__ads_categories` b ON b.id = a.parent LEFT JOIN `#__ads_categories` c ON c.id = b.parent LEFT JOIN `#__ads_categories` d ON d.id = c.parent	LEFT JOIN `#__ads_categories` e ON e.id = d.parent	LEFT JOIN `#__ads_categories` f ON f.id = e.parent	LEFT JOIN `#__ads_categories` g ON g.id = f.parent	LEFT JOIN `#__ads_categories` h ON h.id = g.parent	LEFT JOIN `#__ads_categories` i ON i.id = h.parent	LEFT JOIN `#__ads_categories` j ON j.id = i.parent
					WHERE a.parent =$id OR b.parent =$id OR c.parent =$id OR d.parent =$id OR e.parent =$id OR f.parent =$id OR g.parent =$id  OR h.parent =$id  OR i.parent =$id  OR j.parent =$id 
					";
					$db->setQuery($q);
					$idArray = $db->loadResultArray();
					$idArray = array_merge(array($id),$idArray);
					
					$idList = implode(",",$idArray);
					
					$db->setQuery("SELECT count(*) FROM #__ads WHERE category IN ( $idList ) ");
					$c = $db->loadResult();
					if($c>=1){
						// has ads -> ad it!
						$fTree[] = $category;
					}
					
				}else{
					$id = $category["id"];
					$db->setQuery("SELECT count(*) FROM #__ads WHERE category = $id ");
					$c = $db->loadResult();
					if($c>=1){
						// has ads -> ad it!
						$fTree[] = $category;
					}
				}
			}
		}
		return $fTree;
		
	}
	
	/**
	 *
	 * Deprecated since 1.4.0
	 *  
	 **/    
	function getSEFCat(&$cat,&$subcat) {
		
		if(! isset($this->category_table) )
			$this->category_table = APP_CATEGORY_TABLE;
			
		$database = &JFactory::getDBO();
		if (!$cat) return false;

        //require (JPATH_ROOT.DS."components".DS."com_adsman".DS."config.php");
        require_once (JPATH_ROOT.DS.'components'.DS.'com_adsman'.DS.'options.php');
        
        if (ads_opt_transliterate == 1) {
            $cat = JTheFactoryLangHelper::transliterate($cat);
            $cat = JFilterOutput::stringURLSafe($cat);
        } else {
            $cat = JFilterOutput::stringURLUnicodeSlug($cat);
        }

		$parent = (int)JRequest::getVar('parent', 0);
		
		$database->setQuery("SELECT catsef.catid FROM #__ads_categories_sef as catsef
		LEFT JOIN #__ads_categories as cat ON cat.id = catsef.catid AND  cat.parent = $parent
		  WHERE cat.parent = $parent AND (catsef.categories LIKE '".$cat."' OR  catsef.categories LIKE '".$cat."') ;");
        //WHERE cat.parent = $parent AND (catsef.categories LIKE '".JFilterOutput::stringURLSafe($cat)."%' OR  catsef.categories LIKE '".$cat."%') ;");

		$id = $database->loadResult();
		
		if ($subcat){

            if (ads_opt_transliterate == 1) {
                $subcat = JTheFactoryLangHelper::transliterate($subcat);
                $subcat = JFilterOutput::stringURLSafe($subcat);
            } else {
                $subcat = JFilterOutput::stringURLUnicodeSlug($subcat);
            }

			$database->setQuery("SELECT catsef.catid FROM #__ads_categories_sef as catsef
			LEFT JOIN #__ads_categories as cat ON cat.id = catsef.catid AND  cat.parent = $parent
			  WHERE cat.parent = $parent AND (catsef.categories LIKE '%".$cat."%".$subcat."%' OR catsef.categories LIKE '%".$cat."%".$subcat."%') ;");

			$id = $database->loadResult();
		}
	
		return $id;
	}
	
	/**
	 *  
	 * String Category Full Path
	 *  
	 **/
	function string_cat_path($id=null,$task="listadds"){
		$Itemid = JRequest::getVar('Itemid', 0, 'REQUEST', 'integer');
		
		if (!$id ) 
			return;
		
		$cat_path[$id] = JTheFactoryCategoryObj::get_cat_path($id, 1,1);
		
		//if only the root is in path, don't display it
		if(count($cat_path[$id])<=1){
			return;
		}
	
		$string_path = '';
		$i=1;
		$nrcats = count($cat_path[$id]);
		foreach($cat_path[$id] as $cat) {

			if ($cat['id']) {
                $catslug = "";

                if(isset($cat['catslug'])) {
                    $separator = PHP_EOL;
                    $catslug = str_replace($separator,"/",str_replace("/","-",$cat['catslug']));
                    $catslug = "&amp;catslug=$catslug";

                    $catslug_parent_string = $cat['parent'];
                    $catslug_parent = "&amp;parent=$catslug_parent_string";
                }
				$link = JRoute::_( 'index.php?option='.APP_EXTENSION.'&task='.$task.'&cat='.$cat['id'].'&Itemid='.$Itemid.$catslug.$catslug_parent);
				
			} else {
				
				$link = JRoute::_( 'index.php?option='.APP_EXTENSION.'&task='.$task.'&Itemid='.$Itemid);
			}
			$string_path .= '<a href="'.$link.'">'.JFilterOutput::stringURLSafe($cat['catname']).'</a>&gt;';
			$i++;
		}
		
		$string_path = trim($string_path,"&gt;");
	
		return $string_path;
	}
	
	/**
	 * The Full Cat name containgin all parents names separated by /
	 *
	 * @param $id
	 * @return string
	 */
	function string_cat_name($id, $include_root=1,$sef=1){
		
		$cat_path[$id] = JTheFactoryCategoryObj::get_cat_path($id, $include_root,1);

		$all_cats = array();
		if($cat_path[$id])
		foreach($cat_path[$id] as $cat){
			if($sef){

                //require_once (JPATH_ROOT.DS."components".DS."com_adsman".DS."config.php");
                require_once (JPATH_ROOT.DS.'components'.DS.'com_adsman'.DS.'options.php');

                if (ads_opt_transliterate == 1) {
                    $name = JTheFactoryLangHelper::transliterate($cat['catname']);
                    $name = JFilterOutput::stringURLSafe($name);
                } else {
                    $name = JFilterOutput::stringURLUnicodeSlug($cat['catname']);
                }

				$all_cats[] = $name;
			} else
				$all_cats[] = $cat['catname'];
		}

		return implode("/", $all_cats);
	}
	
	/**
	 *  
	 * Array of Category Full Path
	 *  
	 **/
	function get_cat_path($id=null,$include_root=1, $start=0){
	
		if(! isset($this->category_table) ) $this->category_table = APP_CATEGORY_TABLE;
			
		static $cat_pathOBJ=array();
			
		if (!$id) return null;
		if($start==1)
			$cat_pathOBJ=array();

		$database = & JFactory::getDBO();
	
		$q = "SELECT c.id, c.catname, c.parent, catsef.categories as catslug FROM `#__ads_categories` AS c
		  LEFT JOIN `#__ads_categories_sef` AS `catsef` ON  `c`.`id`=`catsef`.`catid` WHERE c.id='".$id."'";
		$database->setQuery($q); 
		// History:
		// USED false param not to translate: JOOMFISH with sef: categories links are not translated this version
		// USED loadAssocList instead of loadAssoc because Joomfish Sucks and in the 
		// .. loadAssoc implementation if not translated it is called loadResult instead of loadAssoc
		$res = $database->loadAssocList('');
		if( count($res)>0 )
			$res = $res[0];
	
		if( isset($res['id']) && is_array($res) )
			array_push( $cat_pathOBJ, $res );
	
		if( @(int)$res['parent'] > 0 ){
			JTheFactoryCategoryObj::get_cat_path($res['parent'],$include_root);
			
		}else{
	
			if($include_root==1){
				$root = array();
				$root['id'] = 0;
				$root['catname'] = JText::_('ADS_ROOT_CATEGORY');
				$root['parent'] = null;
                $root['catslug'] = null;
				array_push($cat_pathOBJ,$root);
			}
			
			$cat_pathOBJ = array_reverse($cat_pathOBJ);
			return $cat_pathOBJ;
			
		}
	
		return $cat_pathOBJ;
		
	}
	
	
	// HTML usefull
	
	/**
	 * one day should only one of the bellow
	 *
	 */
	/**
	 * Make a JHTML select List with indented categories
	 * 
	 * @param: selected category
	 * @param: name of field
	 * @param: select width
	 * @param: include root 
	 *
	 * @modified;24/09/09	
	 */
	function makeCatTree($selected_category="", $id_select="cat", $width=200, $include_root=1, $all=0, $kids_only=false){
		
		
		if(! isset($this->category_table) )
			$this->category_table = APP_CATEGORY_TABLE;
		$database = &JFactory::getDBO();
		$html_tree = "";
		
		$spacer = "...";
		$cat_tree = array();
		if ($all) $cat_tree[]=JHTML::_('select.option','',$all);
				
		$cat = $this;
		if($kids_only)	
			$cats = $cat->getActiveTree($include_root);
		else	
			$cats = $cat->build_child(0, $include_root);
		
		$nr_pcats = count($cats);
		if($nr_pcats>0){
			foreach($cats as $cIndex => $category){
				$cid = $category["id"];
				
				$leveledSpacer = "";
				for($i=0; $i<$category["depth"];$i++)
					$leveledSpacer .= $spacer;
				
					
				$cat_tree[] = JHTML::_('select.option',$category["id"],$leveledSpacer.stripslashes($category["catname"]).PHP_EOL );
			}
		}
		if (count($nr_pcats)>0){
			$html_tree = JHTML::_('select.genericlist', $cat_tree,$id_select,'class="inputbox" style="width:'.$width.'px;" ','value', 'text',$selected_category);
		}else
			$html_tree = '&nbsp;';
		return $html_tree;
	}

	function makeCatTreeVariant($id_select="cat", $attributes="", $width=200, $include_root=0){
		
		if(! isset($this->category_table) )
			$this->category_table = APP_CATEGORY_TABLE;
		$database = &JFactory::getDBO();
		$html_tree = "";
		
		$spacer = "...";
		$cat_tree = array();
		$cat_tree[]=JHTML::_('select.option','',"All");
				
		$cat = $this;
		$cats = $cat->build_child(0, $include_root);
		
		$nr_pcats = count($cats);
		if($nr_pcats>0){
			foreach($cats as $cIndex => $category){
				$cid = $category["id"];
				
				$leveledSpacer = "";
				for($i=0; $i<$category["depth"];$i++)
					$leveledSpacer .= $spacer;
				$cat_tree[] = JHTML::_('select.option',$category["id"],$leveledSpacer.stripslashes($category["catname"]).PHP_EOL );
			}
		}
		if (count($nr_pcats)>0){
			$html_tree = JHTML::_('select.genericlist', $cat_tree,$id_select,' '.$attributes.' class="inputbox" style="width:'.$width.'px;" ','value', 'text');
		}else
			$html_tree = '&nbsp;';
		return $html_tree;
	}
	
	function makeCatSelectMultiple($selected_category="", $id_select="cat", $width=200, $include_root=1, $all=0 ){
		
		if(! isset($this->category_table) )
			$this->category_table = APP_CATEGORY_TABLE;
		$database = &JFactory::getDBO();
		$html_tree = "";
		
		$spacer = "...";
		$cat_tree = array();
		if ($all) $cat_tree[]=JHTML::_('select.option','',$all);
				
		$cat = $this;
		$cats = $cat->build_child(0, $include_root);
		
		$nr_pcats = count($cats);
		if($nr_pcats>0){
			foreach($cats as $cIndex => $category){
				$cid = $category["id"];
				
				$leveledSpacer = "";
				for($i=0; $i<$category["depth"];$i++)
					$leveledSpacer .= $spacer;
				$cat_tree[] = JHTML::_('select.option',$category["id"],$leveledSpacer.stripslashes($category["catname"]).PHP_EOL );
			}
		}
		if (count($nr_pcats)>0){
			$html_tree = JHTML::_('select.genericlist', $cat_tree,$id_select,'multiple class="inputbox" style="width:'.$width.'px;height:250px;"  ','value', 'text', $selected_category);
		}else
			$html_tree = '&nbsp;';
		return $html_tree;
	}
	
	/**
	 * Make a JHTML select List with indented categories
	 * 
	 * @param: selected category
	 * @param: name of field
	 * @param: select width
	 * @param: include root 
	 *
	 * @modified;24/09/09	
	 */
	function HTMLCatTree($name, $selected_category, $params=array() ){

		$id_select = $name;
		if (isset($params["id_select"]) && $params["id_select"]!="") {
			$id_select = $params["id_select"];
		}
		
		$include_root = true;
		if (isset($params["include_root"])) $include_root = $params["include_root"];
		
		$kids_only = false;
		if (isset($params["kids_only"])) $kids_only = $params["kids_only"];
		
		$leaves_enabled = false;
		if (isset($params["leaves_enabled"])) $leaves_enabled = $params["leaves_enabled"];
		
		$width = "200px;";
		if (isset($params["width"]) && $params["width"]!="") {
			$width = $params["width"];
		}
		
		$script = "";
		if (isset($params["script"]) && $params["script"]!="") {
			$script = $params["script"];
		}
		
		if(! isset($this->category_table) )
			$this->category_table = APP_CATEGORY_TABLE;
			
		$database = &JFactory::getDBO();
		$html_tree = "";
		
		$spacer = "...";
		$cat_tree = array();
		
		$all = null;
		if (isset($params["all"]) && $params["all"]!="") {
			$all = $params["all"];
		}
		
		if ($all) $cat_tree[]=JHTML::_('select.option','',$all);
				
		$cat = $this;
		if($kids_only)
			$cats = $cat->getActiveTree($include_root);
		else	
			$cats = $cat->build_child(0, $include_root);
		
		$nr_pcats = count($cats);
		if($nr_pcats>0){
			foreach($cats as $cIndex => $category){
				$cid = $category["id"];
				
				$leveledSpacer = "";
				for($i=0; $i<$category["depth"];$i++)
					$leveledSpacer .= $spacer;
					
				$disabled = false;
				
				if( $leaves_enabled  ){
				 if( $category["nr_children"] > 0 )
						$disabled = true;
				}
				$cat_tree[] = JHTML::_('select.option', $category["id"], $leveledSpacer.stripslashes($category["catname"]).PHP_EOL, 'value', 'text', $disabled );
			}
		}
		if (count($nr_pcats)>0){
			$html_tree = JHTML::_('select.genericlist', $cat_tree, $name,' id="'.$id_select.'" class="inputbox" style="width:'.$width.'" '.$script.' ','value', 'text',$selected_category);
		}else
			$html_tree = '&nbsp;';
		return $html_tree;
		
	}
	
}

?>