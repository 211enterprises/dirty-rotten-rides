<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/


class JTheFactoryCategory_controller{
	
    /**
     * Added INIT Method to load all dependencies both on front & backend
     *
     * @since 13/01/2010 (dd/mm/YYYY)
     */
	function init(&$app){
		if(!$app)
			$Tapp=&JTheFactoryApp::getInstance(null,false);
		
        if ($Tapp->getIniValue('use_category_management')){
		
			define('APP_CATEGORY_TABLE',$Tapp->getIniValue('table','categories'));
			define('APP_CATEGORY_TABLE_SEF', APP_CATEGORY_TABLE."_sef");
			define('APP_CATEGORY_TABLE_FIELD_NAME', "name");
			define('APP_CATEGORY_TABLE_PRIMARY_KEY', "id");
			define('APP_CATEGORY_TABLE_FIELD_DESCRIPTION', "description");
			
			
	        define('APP_CATEGORY_DEPTH',$Tapp->getIniValue('table','depth'));
	        JLoader::register('JTheFactoryCategoryTable', dirname(__FILE__).DS.'table.category.php');
	        
	    	JLoader::register('JTheFactoryCategoryObj', dirname(__FILE__).DS.'category.core.php');
    	
            
           	JLoader::register('JTheFactoryCategory', dirname(__FILE__).DS.'admin.category.php');
        }
	}

	/**
	 * Backend Application Router
	 *
	 * @param $task
	 * @param $act
	 * @return bool
	 */
	function route(){
		
		$task = & JRequest::getVar("task","");
		$task = "admin_".$task;
		$act = & JRequest::getVar("act","");
		
		$JTheFactoryCategories =& JTheFactoryCategory::getInstance(APP_CATEGORY_TABLE);
		
		if (method_exists($JTheFactoryCategories,$task)){
			$JTheFactoryCategories->$task($act);
			return true;
		}
		return false;
	}
	

	
}


?>