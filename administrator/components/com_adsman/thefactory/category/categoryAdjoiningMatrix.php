<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

function categoryAdjoiningMatrix(){

	$database =& JFactory::getDBO();
	$query = "SELECT * FROM `#__rssfactory_categories` ORDER BY `ordering` ASC";
	$database->setQuery($query);
	$categories = $database->loadObjectList('id');
	
	$root_category = new stdClass;
		$root_category->id=0;
		$root_category->title=RFPRO_WORD4ROOT;
		$root_category->parent=null;
	
	array_push($categories,$root_category);

	//initialize adjoining matrix - nu cred ca ajuta prea mult, dar la afisare arata mult mai clar cu taote 0-urile astea
	foreach($categories as $category){
		foreach($categories as $cat){
			$adjoiningMatrix[$category->id][$cat->id]=0;
		}
	}

	foreach($categories as $category){
		if($category->parent && $category->id){
			$adjoiningMatrix[$category->id][$category->parent] = $adjoiningMatrix[$category->parent][$category->id] = 1;
		}
	}

	return $adjoiningMatrix;
	
}

?>