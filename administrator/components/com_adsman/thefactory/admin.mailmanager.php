<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

class JTheFactoryMailManager extends JObject
{
    var $_tablename=null;
    var $_keyfieldname=null;
    var $_fields=array();
    var $shortcuts=array();

    function __construct($params)
    {
        $this->_fields['subject']=$params['subject'];
        $this->_fields['mail']=$params['mail'];
        $this->_fields['enabled']=$params['enabled'];

        $this->_keyfieldname=$params['keyfield'];
        $this->_tablename=$params['table'];

        $short=explode(',',$params['shortcuts']);
        $short_d=explode(',',$params['shortcuts_description']);

        for($i=0;$i<count($short);$i++){
            $this->shortcuts[$short[$i]]=$short_d[$i];
        }

    }
    
	function &getInstance($params=null)
	{
		static $instances;

		if (!isset( $instances ))
			$instances = new JTheFactoryMailManager($params);

		return $instances;
	}
	
    function getMailObject($mailtype)
    {
        /*@var $db JDatabase*/
        $db	= JFactory::getDBO();
        $db->setQuery('select * from `'.$this->_tablename.'` where `'.$this->_keyfieldname.'`="'.$mailtype.'"',0,1);
        $m	= $db->loadAssoc();
        
        $obj=new stdClass();
        $obj->mail_type=$mailtype;
        $obj->subject=$m[$this->_fields['subject']];
        $obj->content=$m[$this->_fields['mail']];
        $obj->enabled=$m[$this->_fields['enabled']];
        
        return $obj;
        
    }
    
    function getTermsAndConditions()
    {
        /*@var $db JDatabase*/
        //$mailtype = JRequest::getVar('mail_type');
        $db	= JFactory::getDBO();
        $db->setQuery('select `'.$this->_fields['mail'].'` from `'.$this->_tablename.'` where `'.$this->_keyfieldname.'`="'.$mailtype.'"',0,1);
        return $db->loadResult();
    	
    }
    
    function saveMailContent($mailtype,$subject,$content,$enabled)
    {
        /*@var $db JDatabase*/
        $db	= JFactory::getDBO();
        $db->setQuery('UPDATE `'.$this->_tablename.'` SET '.
                '`'.$this->_fields['subject'].'`="'.mysql_escape_string($subject).'",'.
                '`'.$this->_fields['mail'].'`="'.mysql_escape_string($content).'",'.
                '`'.$this->_fields['enabled'].'`="'.$enabled.'" '.
                ' WHERE `'.$this->_keyfieldname.'`="'.$mailtype.'"');
        $db->query();
    }
    
	function showJS($current_mail)
	{
	    /*@var $app JTheFactoryApp*/
        $Tapp = JTheFactoryApp::getInstance();
        $js = $Tapp->getSection('mail-settings-js');
        
        if (count($js))
            foreach ($js as $f=>$file){
    	    ?><script type="text/javascript" src="<?php echo $file?>"></script><?php
            }?>

            <script language="JavaScript" type="text/javascript">
        	<!--
        	var is_enabled= <?php if ($current_mail->{$this->_fields['enabled']}) echo "true";else echo "false"; ?>;
        	function submitbutton(action){
        		if (typeof tinyMCE  != 'undefined') tinyMCE.execCommand('mceFocus', false,'mailbody');
        	    frm=document.adminForm;
        		submitform(action);

        	}
        	function toggleMail(){
        	    frm=document.adminForm;
           	    el=document.getElementById("mailbody-tr");
           	    is_enabled=!is_enabled;

                if (is_enabled){
                    document.getElementById("subject").readOnly=false;
                    //frm.subject.disabled =false;
            	    el.style.display="block";
        	    }else{
                    document.getElementById("subject").readOnly=true;
                    //frm.subject.disabled =true;
            	    el.style.display="none";
        	    }

        	}
        	function ClickShortcut(shortcut)
        	{
        	    if (typeof tinyMCE  == 'undefined') return;
        	    if (shortcut.indexOf('LINK%'))
                    shortcut='<a href="'+shortcut+'">'+shortcut+'</a>';
                tinyMCE.execCommand("mceInsertContent",false,shortcut);

        	}
        	//-->
         </script>
	    <?php

	}
	
	function showMailTypeSelect($mailtypes,$current_mail_type)
	{

       echo "<select name='mail_type' onchange=\"submitbutton('mailsettings');\">";
         foreach ($mailtypes as $row){
             $sel = "";
             if ($row->{$this->_keyfieldname}==$current_mail_type) $sel="selected";
                echo "<option value='".$row->{$this->_keyfieldname}."' $sel>";
                   if (defined(APP_PREFIX.'_mail_'.$row->{$this->_keyfieldname}))
                       echo constant(APP_PREFIX.'_mail_'.$row->{$this->_keyfieldname});
                   else
                       echo JText::_(strtoupper($row->{$this->_keyfieldname}));
                echo "</option>";
         }
       echo "</select>";
	}
	
	
	function showMailLegend()
	{
	     echo '<table class="adminlist" style="padding-left:100px;" cellspacing="8">';
	     foreach ($this->shortcuts as $shortcut=>$description)
	     {
	         echo "<tr>";
             echo '<td><a href="javascript:ClickShortcut(\''.$shortcut.'\')">'.$shortcut.'</a>&nbsp;</td>';
             echo "<td>&nbsp; - &nbsp;</td>";
			 echo "<td><b> ".JText::_($description)."</b></td>";
             echo "</tr>";

	     }
	     echo '</table>';
	}
	
    function showMailSettings()
    {
        $current_mail_type = JRequest::getVar('mail_type');
        $editor = JFactory::getEditor();
        /*@var $db JDatabase */
        $db = JFactory::getDBO();

        $db->setQuery("SELECT * FROM `$this->_tablename` WHERE `$this->_keyfieldname`<>'terms_and_conditions'");
        $rows=$db->loadObjectList();

        $where = '';
		  if ($current_mail_type){
        		$where=" AND `$this->_keyfieldname`='$current_mail_type'";
        }
        
        $db->setQuery("SELECT * FROM `$this->_tablename` WHERE `$this->_keyfieldname`<>'terms_and_conditions' $where",0,1);
        $current=$db->loadObject();

        $this->showJS($current);
        JTheFactoryAdminHelper::beginAdminForm('savemailsettings',null,'post',JText::_('MAIL_SETTINGS'));
        ?>
        <table class="adminlist">
        	<tr>
        		<td valign="top" width="300">
        			<table class="adminlist">
        				<thead>
        					<th><?php echo JText::_('ADS_MAIL_TYPE');?></th>
        					<th><?php echo JText::_('ADS_ENABLED');?>
        					<a href="javascript:submitbutton('mailsettings_enable')"><?php echo JText::_('ADS_SAVE_ALL');?></a>
        					</th>
        				</thead>
        				<tbody>
        				<?php foreach ($rows as $row){ ?>
        				<tr>
        					<td>
        					<a href="index.php?option=<?php echo APP_EXTENSION;?>&task=mailsettings&mail_type=<?php echo $row->{$this->_keyfieldname};?>">
        					<?php
             if (defined(APP_PREFIX.'_mail_'.$row->{$this->_keyfieldname})) echo constant(APP_PREFIX.'_mail_'.$row->{$this->_keyfieldname});
             else echo JText::_(strtoupper($row->{$this->_keyfieldname}));
	             ?>
			             </a>
        					</td>
        					<td><input type="checkbox" name="cid[]" value="<?php echo $row->{$this->_keyfieldname};?>" <?php if($row->enabled) echo "checked";?> /></td>
        				</tr>
        				<?php } ?>
        				</tbody>
        			</table>
        		</td>
        		<td valign="top">
        		
        <?php
        $this->showMailTypeSelect($rows,$current->{$this->_keyfieldname});
        echo "<input type='checkbox' name='enabled' value='1' onclick='toggleMail();'";
        if ($current->{$this->_fields['enabled']}) echo " checked >";
        else echo ">";
        echo JText::_("ADS_ENABLED");
        if (!$current->{$this->_fields['enabled']}) echo "<div><h2>".JText::_("ADS_MAILTYPE_NOT_ENABLED")."</h2></div>";

        if (defined(APP_PREFIX.'_mail_'.$current->{$this->_keyfieldname})) $title= constant(APP_PREFIX.'_mail_'.$current->{$this->_keyfieldname});
        else $title= JText::_(strtoupper($current->{$this->_keyfieldname}));
        if (defined(APP_PREFIX.'_mail_'.$current->{$this->_keyfieldname}.'_help')) $help= constant(APP_PREFIX.'_mail_'.$current->{$this->_keyfieldname}.'_help');
        else $help= JText::_(strtoupper($current->{$this->_keyfieldname}.'_help'));
?>
            <table class="adminlist" width="100%">
            	<tr>
            		<td width="50%">
		                <table >
		                <tr>
		                    <th class="title">
		                        <?php echo $title;?>&nbsp;
		                        <?php echo JHTML::_('tooltip',$help);?>
		                    </th>
		                </tr>
		                <tr>
		                    <td>
		                        <input name="subject" id="subject" value="<?php echo  $current->{$this->_fields['subject']};?>"  size="80" <?php if ($current->{$this->_fields['enabled']}) echo "";else echo 'readonly="readonly"'; ?>>
		                    </td>
		                </tr>
		                <tr id="mailbody-tr">
		                    <td>
		                    	<?php echo $editor->display( 'mailbody',  $current->{$this->_fields['mail']}, '100%', '250', '100', '100' ) ;?>
		                   </td>
		                </tr>
		                </table>
		                <?php if (!$current->{$this->_fields['enabled']}) { ?>
                                <script type="text/javascript">
                                    el=document.getElementById("mailbody-tr");
                                    el.style.display="none";

                                    //elSubj = document.getElementById("subject");
                                    //elSubj.style.display="none";
                                </script>
		                <?php } ?>
            		</td>
            		<td align="left" valign="top">
            			<table class="adminlist">
            				<tr>
            					<td align="center"><b><?php echo JText::_("ADS_SHORTCUTS_LEGEND");?></b></td>
            				</tr>
            				<tr><td height="10%"></td></tr>
            				<tr>
								<td>
                                    <?php
                                        echo $this->showMailLegend();
                                    ?>
								</td>
							</tr>
            			</table>
            		</td>
            	</tr>
            </table>
     		</td>
     	</tr>
     </table>
<?php
        JTheFactoryAdminHelper::endAdminForm();
    }
    
    function saveMailSettings()
    {
		$app = JFactory::getApplication();
		
		$this->saveMailSettingsEnable( 0 );
		
		$mailtype = JRequest::getVar('mail_type');
		$sub      = JRequest::getVar('subject','', 'request','Ad %ADDTITLE% '); //
		$mess     = JRequest::getVar('mailbody','','request','none',JREQUEST_ALLOWRAW);
		$ena      = JRequest::getVar('enabled',0);

        $this->saveMailContent($mailtype,$sub,$mess,$ena);

        $app->redirect('index.php?option='.APP_EXTENSION.'&task=mailsettings&mail_type='.$mailtype,JText::_('ADS_SETTINGS_SAVED'));
    }
    
    function saveMailSettingsEnable($redirect=1)
    {
		$app = JFactory::getApplication();
		$db = JFactory::getDbo();
		
    	$cid	= JRequest::getVar('cid', array(), '', 'array');
    	
		$db->setQuery("select * from `$this->_tablename` where `$this->_keyfieldname`<>'terms_and_conditions'");
		$rows=$db->loadObjectList();
		
		foreach ($rows as $row){
			$mailtype = $row->{$this->_keyfieldname};
			$sub = $row->{$this->_fields['subject']};
			$mess = $row->{$this->_fields['mail']};
			$ena = 0;
			if(in_array($mailtype,$cid))
				$ena = 1;
			$this->saveMailContent($mailtype,$sub,$mess,$ena);
			
		}

		if ($redirect) {
	      $app->redirect('index.php?option='.APP_EXTENSION.'&task=mailsettings',JText::_('ADS_MAIL_SETTINGS_SAVED'));
		}
    }

}


?>