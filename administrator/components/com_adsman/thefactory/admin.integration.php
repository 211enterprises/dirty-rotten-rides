<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

class JTheFactoryIntegration extends JObject
{
	var $integration_arr=array();
	var $_table=null;
	
    function __construct($table=null) {
    	$this->_table=$table;
		$this->integration_arr=$this->detectIntegration();
		
    }
	
    function &getInstance($table=null) {
        static $instance;

        if ( !isset ($instance))
        {
            $instance = new JTheFactoryIntegration($table);
        }
        return $instance;
    }
    
	function detectIntegration() {

		global $cb_fieldmap;
	    $database = JFactory::getDBO();
	    
	    if (!$this->_table){
	    	$Tapp=&JTheFactoryApp::getInstance();
	    	$this->_table=$Tapp->getIniValue("field_map_table","cb-integration");
	    }
	    
	    $integration_arr = array();
        //detect cb
	    $cb_fieldmap = array();
	    $database->setQuery("SELECT count(*) FROM #__extensions WHERE `element`='com_comprofiler'");
	    
	    if($database->loadResult()>0) {
	    	
	    	define('CB_DETECT',1);
	    	$database->setQuery("SELECT field,cb_field FROM `".$this->_table."`");
	    	$r = $database->loadAssocList();
	    	
	    	for($i=0; $i<count($r); $i++){
	    	    $cb_fieldmap[$r[$i]['field']] = $r[$i]['cb_field'];
	    	}
	    	
	    	$integration_arr["CB"] = $cb_fieldmap;
	    } else {
	        define('CB_DETECT',0);
	    }
	    //detect cb
	    
	    //detect love factory
	    $database->setQuery("SELECT count(*) FROM #__extensions WHERE `element`='com_lovefactory'");
	    
	    if($database->loadResult()>0){
	    	define('LOVE_DETECT',1);
	    }else{
	    	define('LOVE_DETECT',0);
	    }
	    //detect love factory
	    
        return $integration_arr;
	}
		
	
	function saveBCSettings()
	{
		$database = JFactory::getDbo();
		
        $Tapp = JTheFactoryApp::getInstance();
   		$f = $Tapp->getIniValue('fields_list','cb-integration');
   		$field_maps = explode(',',$f);
				
	    
	    for ($i=0;$i<count($field_maps);$i++){
	    	
	        $fld = $field_maps[$i];
	        $cb = JRequest::getVar($fld,'');
	        $database->setQuery("UPDATE #__ads_cbfields SET cb_field='$cb' WHERE field='$fld'");
	        $database->query();
	    }
		
	}
	
	function adminShowCBDialog()
	{
	    $database = JFactory::getDbo();
		$query = "SELECT `name` as value,`title` as text FROM #__comprofiler_fields ORDER BY `name`";
		$database->setQuery($query);
		$fields = $database->loadObjectList();
		if ($fields)$fields = array_merge(array(JHTML::_("select.option",'','-'.JText::_("ADS_NONE").'-')),$fields);
		
        $Tapp = &JTheFactoryApp::getInstance();
   		$f    = $Tapp->getIniValue('fields_list','cb-integration');
   		$field_maps = explode(',',$f);
		
        $disabled = (CB_DETECT) ? "" : "disabled";
        if ( !CB_DETECT && !LOVE_DETECT ){
          ?>
            <div style="background-color:orange;font-size:150%;"><?php echo JText::_("ADS_CB_NOT_INSTALLED");?></div>
          <?php
        } else {
        	?>
        		<a href="<?php echo JURI::base();?>index.php?option=<?php echo APP_EXTENSION;?>&task=cb_pluginstall"><img src="<?php echo JURI::base();?>/images/config.png" style="vertical-align:middle;"><strong>Install CB Plugins</strong></a>
        	<?php
        }
        ?>
		  <form action="index.php" method="post" name="adminForm" >
		  <input type="hidden" name="option" value="<?php echo APP_EXTENSION;?>">
		  <input type="hidden" name="task" value="savecbsettings">
		  <table width="100%" class="adminlist">
	      <?php for($i=0;$i<count($field_maps);$i++) { 
	      		$cb_field_text=APP_PREFIX."_cbfield_".strtolower($field_maps[$i]);
	      		
	      	?>
		  	<tr>
		  		<td width="15%" align="left">
		  			<?php 
		  			
		  				if (defined($cb_field_text)) echo constant($cb_field_text);
		  				else echo $field_maps[$i];
		  			?>:
		  		</td>
		  		<td width="20%" align="left">
		  		  <?php 
  		  			  $selected = isset($this->integration_arr['CB'][$field_maps[$i]])?($this->integration_arr['CB'][$field_maps[$i]]) : null;
					  echo JHTML::_("select.genericlist",$fields,$field_maps[$i],"class='inputbox' $disabled",'value','text',$this->integration_arr['CB'][$field_maps[$i]]);
				  ?>
    	  		  <?php if (defined($cb_field_text."_help")) echo JHTML::_('behavior.tooltip',constant($cb_field_text."_help")); ?>
		  		</td>
		  	</tr>
            <?php } ?>
		  </table>
		  </form>
        <?php        
        
		
		
	}
	
	function adminCBInstall(){
		
		$install_file = JPATH_COMPONENT_ADMINISTRATOR.DS."install.ads.php";;
		require_once($install_file);
		com_adsmanInstallerScript::install_cbplugin('adsman_my_ads');
		com_adsmanInstallerScript::install_cbplugin('adsman_googlemap');
		return true;
	}

}


?>