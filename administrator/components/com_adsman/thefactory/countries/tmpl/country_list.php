<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

JTheFactoryAdminHelper::beginAdminForm("countries",null,'post',true,JText::_('ADS_COUNTRY_MANAGER'));
?>
<input type="hidden" id="form_act" name="act" value="" />
<table class="adminlist">

	<tr>
		<th colspan="5" align="left">
		<h2>
			<!--<img src="<?php echo F_ROOT_URI;?>administrator/components/com_adsman/img/toolbar/country_manager.png" alt="Countries" border="0" style="vertical-align:middle;" />&nbsp;&nbsp;<?php //echo JText::_('Country Manager');?>-->

			<span style="float: right; text-align:left; font-weight: normal; font-size: 13px;">
				<span style="float:left; padding: 3px; margin-top:6px;"><?php echo JText::_( 'ADS_FILTER' ); ?>&nbsp;</span>
	 			<span style="float:left; margin-top:6px;"><input type="text" size="70" name="searchfilter" id="searchfilter" value="<?php echo $searchfilter;?>" /></span>
				<span style="float:left; padding: 3px; margin-top:3px;"><?php echo JText::_( 'ADS_PUBLISHED' ); ?> <?php echo $active_html;?></span>
				<button onclick="this.form.submit();"><?php echo JText::_( 'ADS_APPLY_FILTER' ); ?></button>
				<button onclick="document.getElementById('searchfilter').value='';this.form.submit();"><?php echo JText::_( 'ADS_RESET' ); ?></button>
			</span>	
	</h2>
	</th>
</tr>
<tr>
	<th width="5"><input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $rows ); ?>);" /></th>
	<th>Name</th>
	<th align="center">Code</th>
	<th align="center" width="35">Published</th>
	<th align="center" width="35">ID</th>
</tr>
<?php 
$odd=0;
foreach ($rows as $k => $country) {?>
<tr class="row<?php echo ($odd=1-$odd);?>">
	<td align="center">
		<?php echo JHTML::_('grid.id', $k, $country->id );?>
	</td>
	<td><?php echo $country->name;?></td>
	<td align="center"><?php echo $country->simbol;?></td>
	<td align="center">
		<?php 
		$link 	= 'index.php?option=com_adsman&task=countries&act=toggle_status&cid[]='. $country->id;
		if ( $country->active == 1 ) {
			$img = 'publish_g.png';
			$alt = JText::_( 'ADS_PUBLISH_UNPUBLISH' );
		} else {
			$img = 'publish_x.png';
			$alt = JText::_( 'ADS_UNPUBLISHED_PUBLISH' );
		}
		?>
		<span class="editlinktip hasTip" title="<?php echo JText::_( 'ADS_PUBLISH_INFORMATION'.'<br />'.$alt );?>">
			<a href="<?php echo $link;?>" >
			
			<img src="<?php echo JURI::root();?>administrator/components/com_adsman/img/<?php echo $img;?>" width="16" height="16" border="0" alt="<?php echo $alt; ?>" />
		</a>
		</span>
	</td>
	<td align="center"><?php echo $country->id;?></td>
</tr>

<?php } ?>
<tr>
	<td colspan="5" align="left">
<?php echo $page->getListFooter();?>
</td>
</tr>
</table>

<?php
	JTheFactoryAdminHelper::endAdminForm();
?>
