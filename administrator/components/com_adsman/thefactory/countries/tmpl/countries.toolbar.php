<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

jimport('behaviour.mootools');
	$bar = & JToolBar::getInstance('toolbar');
	
	$bar->appendButton( 'Link', 'apply', 'Toggle published', "javascript: if (document.adminForm.boxchecked.value==0){alert('Please first make a selection from the list');}else{ document.getElementById('form_act').value='toggle_status'; document.adminForm.submit();}" );
?>
