<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/


class FCountriesController{
	
	var $_db 	= null;
	
	
	function checkTask( &$act )
	{
		$this->_db = &FactoryLayer::getDB();
		if(!$act)
			$act="list_countries";
			
		if($act=="new")
			$act = "edit";
		
		$method_name = $act;
		
		if ( method_exists($this,$method_name) ){
			require_once(dirname(__FILE__).DS."..".DS."tmpl".DS."countries.toolbar.php");
			$this->$method_name();
			return true;
		}

		return false;
	}
	
	
	function toggle_status(){
		$cid	= JRequest::getVar( 'cid', array(), '', 'array' );
		if(count($cid>0)){
			$cici = implode(",",$cid);
			$this->_db->setQuery("UPDATE #__".APP_CFIELDS_PREFIX."_country SET active = 1- active WHERE id IN ($cici)");
			$this->_db->query();
		}
		
		FactoryLayer::redirect("index.php?option=".F_COMPONENT_NAME."&task=countries","Countries publishing toggled");
		
	}

	/** TO DO */
	function del_country(){
		
	}
	
	/** TO DO */
	function saveOrdering(){
		
	    $ftype = new FactoryFields($this->_db);
	    if(count($_REQUEST))
	    foreach ($_REQUEST as $k=>$v){
	        if (substr($k,0,6)=='order_'){
	            $id=substr($k,6);
				$ftype->load($id);
				$ftype->ordering=$v;
				$ftype->store();
			}
	    }
		FactoryLayer::redirect("index.php?option=".F_COMPONENT_NAME."&task=field_administrator");
	}
	
	/** TO VERIFIY */
	function save()
	{
		$id = FactoryLayer::getRequest($_REQUEST,"id");
		
		$f = new JTheFactoryCountry($this->_db);
		
		$f->load($id);
		$f->bind($_POST);
		$f->id=$id;
		
		$msg = $f->store();
		
		if(!$msg)
			FactoryLayer::redirect("index.php?option=".F_COMPONENT_NAME."&task=countries","Error in Category saving!");
		else
			FactoryLayer::redirect("index.php?option=".F_COMPONENT_NAME."&task=countries");
	}
	
	/**
	 * TO DO
	 * 
	 **/
	function edit(){
		
		$id = FactoryLayer::getRequest($_REQUEST,"id");
		$field = new JTheFactoryCountry($this->_db);
		$field->load($id);

		require_once(dirname(__FILE__).DS."..".DS."tmpl".DS."country_edit.php");
		return true;
	}
	
	function list_countries(){
		
		$app	= JFactory::getApplication();
		$context = "rbidscountries";
		$limit		= $app->getUserStateFromRequest('global.list.limit', 'limit', $app->getCfg('list_limit'), 'int');
		$limitstart	= $app->getUserStateFromRequest($context.'limitstart', 'limitstart', 0, 'int');
		
		$activefilter	= $app->getUserStateFromRequest($context.'activefilter', 'activefilter', 2, 'int');
		$searchfilter	= $app->getUserStateFromRequest($context.'searchfilter', 'searchfilter', '', 'string');
		
		
		$options = array();
		$options[] = JHTML::_('select.option',"2"," All ");
		$options[] = JHTML::_('select.option',"1"," Published ");
		$options[] = JHTML::_('select.option',"0"," Unpublished ");
		$active_html = JHTML::_('select.genericlist', $options, "activefilter",'class="inputbox" ','value', 'text', $activefilter);
		
		$where = array();
		if($activefilter!="2"){
			$where[]= " active ='{$activefilter}' ";
		}
		if($searchfilter!=""){
			$where[]= " name LIKE '%{$searchfilter}%' ";
		}
		
		$whereSQL = "";
		if(count($where)>0){
			$whereSQL = "WHERE ".implode("AND",$where);
		}

		// In case limit has been changed, adjust limitstart accordingly
		$limitstart = ( $limit != 0 ? (floor($limitstart / $limit) * $limit) : 0 );

		$this->_db->setQuery("SELECT COUNT(*) FROM #__".APP_CFIELDS_PREFIX."_country $whereSQL ");
		$total = $this->_db->loadResult();
		
		$page = FactoryLayer::initPagination($total, $limitstart, $limit);
		
		$this->_db->setQuery("SELECT * FROM #__".APP_CFIELDS_PREFIX."_country $whereSQL ", $page->limitstart, $page->limit);
		$rows = $this->_db->loadObjectList();
		
		JHTML::_('behavior.tooltip');
		JHTML::_('behavior.caption');
		require_once(dirname(__FILE__).DS."..".DS."tmpl".DS."country_list.php");
		return true;
	}
	
	
}


