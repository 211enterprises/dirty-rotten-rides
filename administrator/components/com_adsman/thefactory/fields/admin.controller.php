<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/


class JTheFactoryFields_controller{
	
    /**
     * Added INIT Method to load all dependencies both on front & backend
     *
     * @since 13/01/2010 (dd/mm/YYYY)
     */
	function init(&$application){
		
		if($application){
			$Tapp=$application;
		}
		else	
			$Tapp=&JTheFactoryApp::getInstance(null,null);

		// Custom Fields
		if ($Tapp->getIniValue('use_custom_fields')) {
			/** added: to include DB, HTML layer + table Object */

            if (!defined('APP_CFIELDS_PREFIX')) {
			    define('APP_CFIELDS_PREFIX',$Tapp->getIniValue('table_prefix', 'custom-fields'));
            }
			$params = array();
			$params["pages"] 		= $Tapp->getIniValue('pages', 'custom-fields');
			$params["page_names"] 	= $Tapp->getIniValue('page_names', 'custom-fields');
			$params["tables"] 		= $Tapp->getIniValue('tables', 'custom-fields');
			$params["pk"] 			= $Tapp->getIniValue('pk', 'custom-fields');
			$params["aliases"] 		= $Tapp->getIniValue('aliases', 'custom-fields');
			
			require_once( JPATH_ROOT.'/components/'.APP_EXTENSION.'/thefactory/front.fields.php');
	    	/**
			 * SA Factory Fields Table
			 * 
			**/
            if (!defined('F_COMPONENT_NAME')) {
			    define("F_COMPONENT_NAME",APP_EXTENSION);
            }
			global $JAds_pages, $JAds_tables, $JAds_pk, $JAds_aliases;
			
			// ===> COMPONENT CONFIG LOAD
			
			if(!isset($params["pages"]) || $params["pages"]=="" )
	        {
				$error = JError::raiseError(500, "[".APP_PREFIX.'] No custom field pages were configured! ');
				return $error;
	        }
			$pages 		= explode(",", $params["pages"]);
			$page_names = explode(",", $params["page_names"]);
			$tables 	= explode(",", $params["tables"]);
			$pk 		= explode(",", $params["pk"]);
			$aliases 	= explode(",", $params["aliases"]);
	
			$JAds_pages 	= array();
			$JAds_tables 	= array();
			$JAds_pk 		= array();
			$JAds_aliases 	= array();
			
			for( $cu=0; $cu<count($pages ) ; $cu++){
				$page = $pages[$cu];
				$JAds_pages[$page] 	= $page_names[$cu];
				$JAds_tables[$page] = $tables[$cu];
				$JAds_pk[$page] = $pk[$cu];
				$JAds_aliases[$tables[$cu]] = $aliases[$cu];
			}
			// <=== COMPONENT CONFIG LOAD
			require_once( dirname(__FILE__).DS."lang".DS."default.php");
			require_once( dirname(__FILE__).DS.'fields.lib.php');
			require_once(F_ROOT_DIR.DS.'components'.DS.APP_EXTENSION.DS.'thefactory'.DS.'fields'.DS.'field_types.php');
		}
		
	}

	/**
	 * Backend Application Router
	 *
	 * @param $task
	 * @param $act
	 * @return bool
	 */
	function route(){
		
		$task = & JRequest::getVar("task","");
		$act = & JRequest::getVar("act","");
		
		if (method_exists($this,$task)){
			return $this->$task($act);
		}
        
	}
    
	/**
	 * Backend Main Controller task
	 *
	 * @param $act
	 * @return bool
	*/
    function field_administrator(&$act)
    {

		JLoader::register('FactoryFieldsController', dirname(__FILE__).DS.'controllers'.DS.'fields.php');
		$FactoryFieldsController = new FactoryFieldsController();
		return $FactoryFieldsController->checkTask($act);

	}
	
}

?>