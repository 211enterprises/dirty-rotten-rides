<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/



class JTheFactoryFields
{
	
    function __construct($params=null)
    {
		
    }

	function &getInstance($params=null)
	{
		static $instances;

		if (!isset( $instances["fields"] ))
			$instances["fields"] = new JTheFactoryFields($params);

		return $instances['fields'];
	}
	

}
