<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/
?>
	<form action="index.php?option=<?php echo F_COMPONENT_NAME;?>" method="post" name="adminForm" id="adminForm">
		<table class="adminlist" cellspacing="1">
			<thead>
				<tr>
					<th width="5" align="center"><?php echo F_NUM; ?></th>
					<th width="80" align="center">
						<?php echo F_ORDERING; ?>
						<a href="#" onclick="document.getElementById('act_id').value = 'saveOrdering'; document.adminForm.submit();"><img src="<?php echo F_ROOT_URI;?>components/com_adsman/img/adm/filesave.png" border="0" /></a>
					</th>
					<th class="title"><?php echo F_FIELDNAME;?></th>
					<th class="title" width="25%" nowrap="nowrap"><?php echo F_FIELDTYPE;?></th>
					<th class="title" width="10%" nowrap="nowrap"><?php echo F_PAGE;?></th>
					<th class="title" width="10"><?php echo F_ENABLED;?></th>
					<th class="title" width="10"><?php echo F_COMPULSORY;?></th>
					<th class="title" width="10"><?php echo F_SEARCH;?></th>
					<th width="3%" class="title"><?php echo F_ID; ?></th>
					<th width="3%" class="title"><?php echo F_DELETE; ?></th>
				</tr>
			</thead>
			<tfoot>
			<tr>
				<td colspan="10">
					<?php  echo $page->getListFooter(); ?>
				</td>
			</tr>
			</tfoot>
			<tbody>
			<?php
			$k = 0;
			$nullDate = $this->_db->getNullDate();


			for ($i=0, $n=count( $rows ); $i < $n; $i++) {
				$row = &$rows[$i];
				$link 	= 'index.php?option='.F_COMPONENT_NAME.'&task=field_administrator&act=edit&id='. $row->id;
				?>
				<tr class="<?php echo "row$k"; ?>">
					<td align="center"><?php echo $i+1; ?></td>
					<td align="center">
					<input type="text"  size="5" name="order_<?php echo $row->id;?>" value="<?php echo $row->ordering;?>" class="text_area" style="text-align: center" />
					</td>
					<td><a href="<?php echo $link; ?>"><?php echo htmlspecialchars($row->name, ENT_QUOTES); ?></a></td>
					<td align="center"><?php if($row->ftype) echo htmlspecialchars($row->ftype, ENT_QUOTES); else echo "---"; ?></td>
					<td align="center"><?php if($row->page) echo htmlspecialchars($row->page, ENT_QUOTES); else echo "---"; ?></td>
					<td align="center"><?php if($row->status) echo JText::_("ADS_YES"); else echo JText::_("ADS_NO"); ?></td>
					<td align="center"><?php if($row->compulsory) echo JText::_("ADS_YES"); else echo JText::_("ADS_NO"); ?></td>
					<td align="center"><?php if($row->search) echo JText::_("ADS_YES"); else echo JText::_("ADS_NO"); ?></td>
					<td align="center"><?php echo $row->id; ?></td>
					<td align="center"><a href="#" onclick="if(confirm('Are you sure?')=='1'){ document.getElementById('f_id').value = '<?php echo $row->id; ?>';  document.getElementById('act_id').value='del_field'; document.adminForm.submit(); } "><img src="<?php echo F_ROOT_URI;?>/administrator/components/<?php echo APP_EXTENSION;?>/thefactory/fields/assets/delete.png" style="width:12px;" border="0" /></a></td>
				</tr>
				<?php
				$k = 1 - $k;
			}
			?>
			</tbody>
			</table>
		<input type="hidden" name="option" value="<?php echo F_COMPONENT_NAME;?>" />
		<input type="hidden" name="id" id="f_id" value="" />
        <input type="hidden" name="act" id="act_id" value="list_fields" />

		<input type="hidden" name="task" value="field_administrator" />
		<input type="hidden" name="boxchecked" value="0" />
		<!--<input type="hidden" name="redirect" value="<?php //echo $redirect;?>" />
		<input type="hidden" name="act" id="act_id" value="fields_list" />
		<input type="hidden" name="filter_order" value="<?php //echo $lists['order']; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php //echo $lists['order_Dir']; ?>" />-->
		<?php // echo JHTML::_( 'form.token' );  ?>
		</form>
