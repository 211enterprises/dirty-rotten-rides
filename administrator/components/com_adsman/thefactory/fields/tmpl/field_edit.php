<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

jimport('joomla.html.pane');
		$pane	= &JPane::getInstance('sliders', array('allowAllClose' => true));
		?>
		<script type="text/javascript">

			function showEditFieldOptionBox(elementId,fid,fname)
			{
			    var newElem = document.getElementById(elementId);
			    document.getElementById("fo_id").value=fid;
			    document.getElementById("fo_name").value=fname;
			    newElem.style.display ='block';
			}
		
			function allcategories() {
				var e = document.getElementById('parent');
				
				var i = 0;
				var n = e.options.length;
				for (i = 0; i < n; i++) {
					e.options[i].selected = true;
				}
			}
			
			function nocategory() {
				var e = document.getElementById('parent');
				
				var i = 0;
				var n = e.options.length;
				for (i = 0; i < n; i++) {
					e.options[i].selected = false;
				}
			}
					
			function ValidCustomField(tsk){
				
				<?php echo $lists["existing_fields"];?>
				
				prep4SQL(document.getElementById('F_db_name'));
				var fTitle = document.getElementById('F_name').value;
				var field = document.getElementById('F_db_name').value;
				var page = document.getElementById('page').value;
				if(tsk=="aply")
					document.getElementById("fields_aply").value = 1;
					
				var edit = '<?php if($field->id)  echo "1"; else echo "0"; ?>';
				
				if( ( edit==0 && JTHEF_Fields[page].in_array(field) ) )
					alert('Field "'+field+'" allready exists in "'+page+'" !!');
				else if(fTitle==""){
					alert("Enter a label for the field!");
				}
				else if(field==""){
					alert("Enter a field db name!");
				}
				else
					document.adminForm.submit();
						
			}
		</script>
		<form action="index.php?option=<?php echo F_COMPONENT_NAME;?>" method="post" name="adminForm" onsubmit="return ValidCustomField();">
		
		<table class="adminform" border="1">
		<tr>
		<td width="50%" valign="top">
			<?php
				echo $pane->startPane("content-pane");
				echo $pane->startPanel( JText::_( 'ADS_FIELD_CONFIGURATION' ), "detail-page" );
			?>
			<div>
			<fieldset class="adminform">
				<legend><?php echo JText::_( 'ADS_MAIN_SETTINGS' ); ?></legend>
			<table class="adminlist" border="0">
				<tr>
					<td class="paramlist_key" align="right" width="100">
						<span class="tooltip_right"><?php echo F_FIELDLABEL ; echo "&nbsp;"; ?></span>
						<span class="tooltip_right"><?php echo FactoryHTML::tooltip(F_FIELDLABEL_HELP); ?></span> 
					</td>
					<td class="paramlist_value">
						<input type="text" id="F_name" name="name" style="width:200px;" value="<?php echo $field->name; ?>" />
						<?php echo JHTML::_('image','admin/publish_y.png', "Compulsory", array('border' => 0), true);?>
					</td>
				</tr>
				<tr>
					<td class="paramlist_key" align="right" width="100">
						<span class="tooltip_right"><?php echo F_FIELDDBNAME ; echo "&nbsp;"; ?></span>
						<span class="tooltip_right"><?php echo FactoryHTML::tooltip(F_FIELDDBNAME_HELP); ?></span> 
					</td>
					<td class="paramlist_value">
						<input type="text" id="F_db_name" onchange="prep4SQL(this)" name="db_name" style="width:200px;" value="<?php echo $field->db_name; ?>" <?php if($field->id){ ?> readonly="readonly" disabled="disabled" <?php } ?> />
						<?php echo JHTML::_('image','admin/publish_y.png', "Compulsory", array('border' => 0), true);?>
					</td>
				</tr>
				<tr>
					<td class="paramlist_key" align="right"><?php echo F_FIELDTYPE;?></td>
					<td class="paramlist_value"><?php echo $lists["field_types"]; ?>
					<?php echo JHTML::_('image','admin/publish_y.png', "Compulsory", array('border' => 0), true);?>
				</td>
				</tr>
				<tr>
					<td class="paramlist_key" align="right"><?php echo F_SECTION;?></td>
					<td class="paramlist_value">
					<?php echo $lists["field_pages"]; ?>
					<?php echo JHTML::_('image','admin/publish_y.png', "Compulsory", array('border' => 0), true);?>
					<br />
					<span style="color:#FFA000;"><?php echo F_CBWARN; ?></span>
					</td>
				</tr>
			</table>	
			</div>
			</fieldset>	
			<?php
				echo $pane->endPanel();
				$title = JText::_( 'ADS_SETTINGS' );
				echo $pane->startPanel( $title, "detail-page" );
			?>	
			<br />
			<fieldset class="adminform">
			<legend><?php echo JText::_( 'ADS_FIELD_BEHAVIOR' ); ?></legend>
			<table class="adminlist" border="0">
				<tr>
					<td class="paramlist_key" align="right">
						<span class="tooltip_right"><?php echo F_COMPULSORY ; echo "&nbsp;"; ?></span>
						<span class="tooltip_right"><?php echo FactoryHTML::tooltip(F_COMPULSORY_HELP); ?></span> 
					</td>
					<td class="paramlist_value"><?php echo $lists["compulsory"]; ?></td>
				</tr>
				<tr>
					<td class="paramlist_key" align="right"><?php echo F_VALIDATE_TYPE; ?></td>
					<td class="paramlist_value"><?php echo $lists["validate_type"]; ?></td>
				</tr>
				<tr>
					<td class="paramlist_key" align="right">
						<span class="tooltip_right"><?php echo F_SEARCH ; echo "&nbsp;"; ?></span>
						<span class="tooltip_right"><?php echo FactoryHTML::tooltip(F_SEARCH_HELP); ?></span> 
					</td>
					<td class="paramlist_value"><?php echo $lists["search"]; ?></td>
				</tr>
				<tr>
					<td class="paramlist_key" align="right">
						<span class="tooltip_right"><?php echo F_ENABLED ; echo "&nbsp;"; ?></span>
						<span class="tooltip_right"><?php echo FactoryHTML::tooltip(F_ENABLED_HELP); ?></span> 
					</td>
					<td class="paramlist_value"><?php echo $lists["status"]; ?></td>
				</tr>
				<tr>
					<td class="paramlist_key" align="right" width="100"><?php echo F_ORDERING;?></td>
					<td class="paramlist_value"><input type="text" name="ordering" style="width:20px;" value="<?php echo $field->ordering; ?>" /></td>
				</tr>
			</table>	
			</fieldset>	
			<?php
				echo $pane->endPanel();
				$title = JText::_( 'ADS_FIELD_PARAMETERS' );
				echo $pane->startPanel( $title, "detail-page" );
			?>	
			<br />
			<fieldset class="adminform">
			<legend><?php echo JText::_( 'ADS_DISPLAY_SETTINGS' ); ?></legend>
			<table class="adminlist" border="0">
				<tr>
					<td class="paramlist_key" align="right" width="100"><?php echo F_CSS_CLASS;?></td>
					<td class="paramlist_value"><input type="text" name="css_class" style="width:200px;" value="<?php echo $field->css_class; ?>" /></td>
				</tr>
				<tr>
					<td class="paramlist_key" align="right" width="100">
						<span class="tooltip_right"><?php echo F_FIELDID ; echo "&nbsp;"; ?></span>
						<span class="tooltip_right"><?php echo FactoryHTML::tooltip(F_FIELDID_HELP); ?></span>
					</td>
					<td class="paramlist_value"><input type="text" name="field_id" style="width:200px;" value="<?php echo $field->field_id; ?>" /></td>
				</tr>
				<tr>
					<td class="paramlist_key" align="right" width="100"><?php echo F_STYLE_ATTR;?></td>
					<td class="paramlist_value"><textarea name="style_attr" style="width:250px;" ><?php echo $field->style_attr; ?></textarea></td>
				</tr>
				<tr>
					<td class="paramlist_key" align="right" width="100"><?php echo F_HELP;?></td>
					<td class="paramlist_value"><textarea name="help" style="width:250px;" ><?php echo $field->help; ?></textarea></td>
				</tr>
			</table>
			
			<span class="tooltip_right"><?php echo JText::_( 'ADS_FIELD_TYPE_ATTRIBUTES' ) ; ?> &nbsp;</span>
			<span class="tooltip_right"><?php echo FactoryHTML::tooltip("Attributes in html meaning ex: &lt;span align='left' "); ?></span>
			<br />
			<?php foreach ($lists["field_types_list"] as $plm => $paramets ){ 
					 $pluginName = $paramets->class_name;
			?>
				<table id="custom_attr_<?php echo $pluginName;?>" <?php if($field->ftype!=$pluginName) { ?> style="display:none;" <?php } ?> >
				<?php 
					if($paramets->_attributes) {
						echo "<tr><td colspan='2'>".ucfirst($pluginName)."</td></tr>";
					
					foreach ( $paramets->_attributes as $name => $pm) {
				?>
					<tr>
						<td><?php echo $name;?></td>
						<td>
							<input type="text" name="<?php echo $pluginName.'_attributes['.$name.']'; ?>" id="<?php echo $pluginName.$name;?>" value="<?php if($pm!="text") echo $pm;?>" />
						</td>
					</tr>
					<?php }
					}
					 ?>
				</table>
				<?php
			}
?>
			<span class="tooltip_left"><?php echo JText::_( 'ADS_FIELD_TYPE_PARAMS' ) ;?> &nbsp;</span>
			<span class="tooltip_right"><?php echo FactoryHTML::tooltip(" Field type specific parameters "); ?></span>
			
			<?php  foreach ($lists["field_types_list"] as $plm => $paramets ){
					 $pluginName = $paramets->class_name;
			?>
				<table id="custom_params_<?php echo $pluginName;?>" <?php if($field->ftype!=$pluginName) { ?> style="display:none;" <?php } ?> >
				<?php 
					if($paramets->_params) {
						echo "<tr><td colspan='2'>".ucfirst($pluginName)."</td></tr>";
					
					foreach ( $paramets->_params as $name => $pm) {
				?>
					<tr>
						<td><?php echo $name;?></td>
						<td>
							<input type="text" name="<?php echo $pluginName.'_params['.$name.']'; ?>" id="<?php echo $pluginName.$name;?>" value="<?php if($pm!="text") echo $pm;?>" />
						</td>
					</tr>
					<?php }
					}
					 ?>
				</table>
				<?php
			}
			echo "</fieldset>";
			echo $pane->endPanel();
			echo $pane->startPanel( JText::_( 'ADS_CATEGORY_ASSIGNMENT' ), "detail-page" );
			?>
			<fieldset class="adminform">
			<legend><?php echo JText::_( 'ADS_CATEGORY_AVAILABILITY' ); ?></legend>
			<table class="adminlist" border="0">
				<tr>
					<td class="paramlist_key" align="right" width="100">Available for categories</td>
					<td>
					    <?php echo $lists["category"]; ?>
					</td>
					<td>
						<a href="#" onclick="allcategories();"><?php echo JText::_( 'ADS_PLG_SELECT_ALL_CATEG');?></a> | <a href="#" onclick="nocategory();"><?php echo JText::_( 'ADS_PLG_SELECT_NONE_CATEG');?></a>
					</td>
				</tr>
			</table>	
			<?php
			echo "</fieldset>";
			echo $pane->endPanel();
			echo $pane->endPane();
			
			?>
			
			<table cellspacing="0" cellpadding="4" border="0" align="center">
			<tr align="center">
				<td>
					<?php echo JHTML::_('image','admin/publish_y.png', "Compulsory", array('border' => 0), true);?>
				</td>
				<td>
					<?php echo JText::_( 'ADS_COMPULSORY_FIELD' ); ?> |
				</td>
			</tr>
			<tr>
				<td colspan="2"><input type="button" value="<?php echo F_SAVE;?>" onclick="ValidCustomField()" /></td>
			</tr>
			</table>	
		</td>
		<td id="parameter_tab" valign="top" <?php if (!( $field->id && $lists["curent_type"]->has_options )) { ?> style="display:none;"  <?php } ?> >
			<?php JHTML::_('behavior.modal'); ?>
            <?php echo JText::_( 'ADS_FIELD_OPTION_HELP');?>
			<textarea  name="new_option" rows="5" cols="40" ></textarea> 
			<?php if (( $field->id && $lists["curent_type"]->has_options )) { ?> <input type="submit" value="Add" onclick="document.getElementById('fields_act').value='add_option';" /> <?php } ?>
			<br /><br />
			<div id="fieldOptionEditBox" style="display:none; position:absolute; margin-left:-100px; background:#DEDEDE;">
			<fieldset class="adminform">
				<legend><?php echo JText::_( 'ADS_EDIT_FIELD_OPTION' ); ?><a href='#' onclick='document.getElementById("fieldOptionEditBox").style.display="none";' style="margin-left:120px;"><?php echo JText::_( 'ADS_CLOSE');?></a></legend>
				<input type="hidden" name="fo_id" id="fo_id" /><br />
				<table class="adminlist" border="0" style="height:100px;">
				<tr>
					<td class="paramlist_key"><?php echo JText::_( 'ADS_FIELD');?></td>
					<td class="paramlist_value"><?php echo $field->name;?></td>
				</tr>
				<tr>
					<td class="paramlist_key"><?php echo JText::_( 'ADS_OPTION');?></td>
					<td class="paramlist_value"><input type="text" name="fo_name" id="fo_name" /></td>
				</tr>
				<tr>
					<td colspan="2"> <br /><br /> </td>
				</tr>
				<tr>
					<td colspan="2" align="center"><input type="button" value="Save" onclick=" document.getElementById('fields_act').value='save_option'; document.adminForm.submit();" /></td>
				</tr>
				</table>
				<br />
			</fieldset>	
			</div>
			<div>
				<div style="height:250px; overflow-y: scroll;">
				<table class="adminlist">
					<tr>
						<th class="title" valign="top">
							<?php echo F_VALUES;?>
						</th>
						<th class="title" width="50">
							<?php echo JText::_( 'ADS_EDIT');?>
						</th>
						<th class="title" width="50">
							<?php echo F_DELETE;?>
						</th>
					</tr>
					<?php
					$odd=1;
					if( isset($lists["curent_type"]->_options) && count($lists["curent_type"]->_options))
						foreach($lists["curent_type"]->_options as $k => $option){
						 ?>
						<tr class="row<?php echo $odd;?>">
							<td><?php echo $option->option_name;?></td>
							<td align="center">
								<a href="javascript:showEditFieldOptionBox('fieldOptionEditBox', '<?php echo $option->id;?>', '<?php echo $option->option_name;?>');">
								<img src="<?php echo F_ROOT_URI;?>/administrator/components/<?php echo APP_EXTENSION;?>/thefactory/fields/assets/edit_f2.png" style="width:15px;" border="0" />	
								
								</a>
							</td>
							<td align="center">
							<a href="#" onclick="if(confirm('Are you sure?')) { document.getElementById('fields_act').value='delete_option'; document.getElementById('id_opt').value='<?php echo $option->id;?>'; document.adminForm.submit(); } "><img src="<?php echo F_ROOT_URI;?>/administrator/components/<?php echo APP_EXTENSION;?>/thefactory/fields/assets/delete.png" border="0" /></a>
							</td>
						</tr>
						<?php $odd=1-$odd; } ?>
				</table>
				</div>
			</div>
			
		</td>
		<td id="parameter_tab_message">
		</td>
		</table>
			<input type="hidden" name="id" value="<?php echo $field->id; ?>" />
			<input type="hidden" id="id_opt" name="id_opt" value="" />
			<input type="hidden" id="fields_aply" name="fields_aply" value="0" />
			<input type="hidden" id="fields_act" name="act" value="save" />
			<input type="hidden" name="option" value="<?php echo F_COMPONENT_NAME; ?>" />
			<input type="hidden" name="task" value="field_administrator" />
		</form>
		<?php
