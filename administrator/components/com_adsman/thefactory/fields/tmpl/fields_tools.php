<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/
?>

<table>
	<tr>
		<td>
			<a href=""><?php echo JText::_('ADS_OPTIMIZE_CUSTOM_FIELDS_SIZE');?></a>
		</td>
		<td>
			<a href=""><?php echo JText::_('ADS_PURGE_ALL_CUSTOM_FIELDS');?></a>
		</td>
		<td>
			<a href=""><?php echo JText::_('ADS_EXPORT_CUSTOM_FIELDS');?></a>
		</td>
	</tr>
</table>
<table>
	<tr>
		<td></td>
	</tr>
</table>