<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

$act = FactoryLayer::getRequest($_REQUEST,"act",""); 

$bar = & JToolBar::getInstance('toolbar');
$task = & JRequest::getVar("task");

if($task =="field_administrator")
	$bar->appendButton( 'Custom', '<span class="icon-32-step1"></span><strong>'. JText::_("ADS_FIELDS_STEP1").'</strong>' );
else
	$bar->appendButton( 'Link', 'step1', JText::_("ADS_FIELDS_STEP1"), "index.php?option=".APP_EXTENSION."&task=field_administrator",false);

if($task =="fposition_panel")
	$bar->appendButton( 'Custom', '<span class="icon-32-step2"></span><strong>'. JText::_("ADS_FIELDS_STEP2").'</strong>' );
else
	$bar->appendButton( 'Link', 'step2', JText::_("ADS_FIELDS_STEP2"), "index.php?option=".APP_EXTENSION."&task=fposition_panel&act=list",false);
	
if($task =="fposition_fields")
	$bar->appendButton( 'Custom', '<span class="icon-32-step3"></span><strong>'.JText::_("ADS_FIELDS_STEP3").'</strong>' );
else
	$bar->appendButton( 'Link', 'step3', JText::_("ADS_FIELDS_STEP3"), "index.php?option=".APP_EXTENSION."&task=fposition_fields",false);


?>

<table border="0" class="toolbar" width="100%">
	<tr>
		<td align="left" width="78%">
		<h2 style="text-align:left; color: #666666;">
			<img src="<?php echo F_ROOT_URI;?>/administrator/components/<?php echo F_COMPONENT_NAME;?>/thefactory/fields/assets/cfields.png" alt="Fields Manager" border="0" style="vertical-align:middle;" />&nbsp;&nbsp;<?php echo F_GENERAL_TITLE;?></h2>
		<div><h3 style="text-align:left; color: #666666;"><?php echo F_STEP1_TITLE; ?></h3></div>
		
			</td>
		<td align="right"> 
		<table class="adminlist" style="border: 1px solid #FFFFFF; padding: 2px;">
			<tr>
				<td>&nbsp;</td>
				<?php if($act == "edit" || $act =="new") {?>
				<td width="60" style="text-align: center; vertical-align: bottom;">
					<a href="#" onclick="ValidCustomField('aply');">
					<strong><?php echo F_APPLY;?></strong><img src="<?php echo F_ROOT_URI;?>/administrator/components/<?php echo F_COMPONENT_NAME;?>/thefactory/fields/assets/tick.png" border="0" />
					</a>
				</td>
				<td width="50" style="text-align: center; vertical-align: bottom;">
					<a href="#" onclick="ValidCustomField('save');">
					<strong><?php echo F_SAVE;?></strong><img src="<?php echo F_ROOT_URI;?>/administrator/components/<?php echo F_COMPONENT_NAME;?>/thefactory/fields/assets/filesave.png" border="0" />
					</a>
				</td>
				
				<td width="60" style="text-align: center; vertical-align: bottom;">
					<a href="index.php?option=<?php echo F_COMPONENT_NAME;?>&task=field_administrator&act=list_fields">
					<strong><span style="float: left; text-align: center;"><?php echo F_LIST;?></span></strong>
							<img src="<?php echo F_ROOT_URI;?>/administrator/components/<?php echo F_COMPONENT_NAME;?>/thefactory/fields/assets/go_back.png" />
					</a>
				</td>
				<?php } ?>
				<?php if($act !="new") {?>
				<td width="60" style="text-align: center; vertical-align: bottom;">
					<a href="index.php?option=<?php echo F_COMPONENT_NAME;?>&task=field_administrator&act=new">
						<strong><span style="float: left; text-align: center;"><?php echo F_NEW;?></span></strong>
						<img src="<?php echo F_ROOT_URI;?>/administrator/components/<?php echo F_COMPONENT_NAME;?>/thefactory/fields/assets/add.png" />
					</a>
				</td>
				<?php } ?>
				<td width="50" style="text-align: center; vertical-align: bottom;">
					<a href="index.php?option=<?php echo F_COMPONENT_NAME;?>&task=fposition_panel&act=list">
					<strong><?php echo F_STEP2;?></strong><br /><img src="<?php echo F_ROOT_URI;?>/administrator/components/<?php echo F_COMPONENT_NAME;?>/thefactory/fields/assets/positions.png" />
					<!--$bar->appendButton( 'Link', 'config', "Positions / <br />Manage Positions", 'index.php?option='.APP_EXTENSION.'&task=fposition_panel&act=list' );-->
				</td>
			</tr>
		</table>
		</td>		
	</tr>
</table>
