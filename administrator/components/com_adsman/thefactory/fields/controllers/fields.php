<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/




class FactoryFieldsController{
	
	var $_db 	= null;
	
	
	function checkTask( &$act )
	{
        $this->_db = &FactoryLayer::getDB();
		if(!$act)
			$act="list_fields";
			
		if($act=="new")	
			$act = "edit";
		
		$method_name = $act;
		
		if ( method_exists($this,$method_name) ){
			require_once(dirname(__FILE__).DS."..".DS."tmpl".DS."fields.toolbar.php");
			$this->$method_name();
			return true;
		}

		return false;
	}
	
	
	function del_field(){
		
	    $ftype = new FactoryFields($this->_db);
	    
		$id = FactoryLayer::getRequest($_REQUEST,"id");
		if($id)
			$ftype->delete($id);
		
		FactoryLayer::redirect("index.php?option=".F_COMPONENT_NAME."&task=field_administrator",F_DELETE_FIELD_OK);
	}
	
	function saveOrdering(){
		
	    $ftype = new FactoryFields($this->_db);
	    if(count($_REQUEST))
	    foreach ($_REQUEST as $k=>$v){
	        if (substr($k,0,6)=='order_'){
	            $id=substr($k,6);
				$ftype->load($id);
				$ftype->ordering=$v;
				$ftype->store();
			}
	    }
		FactoryLayer::redirect("index.php?option=".F_COMPONENT_NAME."&task=field_administrator");
	}
	
	function add_option($id,$redirect=true){
		if(!$id)
			$id = FactoryLayer::getRequest($_REQUEST,"id");
		$f_opt = FactoryLayer::getRequest($_REQUEST,"new_option");
		
		$f = new FactoryFields($this->_db);
		$f->load($id);
		
		$options = explode(",",$f_opt);
		
		foreach($options as $o => $opt){
			if(trim($opt)!="")
				$f->store_option($opt);
		}
		
		if($redirect==true)
			FactoryLayer::redirect("index.php?option=".F_COMPONENT_NAME."&task=field_administrator&act=edit&id=$id");
	}
	
	function delete_option(){
		
		$id 	= FactoryLayer::getRequest($_REQUEST,"id");
		$id_opt  = FactoryLayer::getRequest($_REQUEST,"id_opt");
		
		$f = new FactoryFields($this->_db);
		$f->del_option($id_opt);
		
		FactoryLayer::redirect("index.php?option=".F_COMPONENT_NAME."&task=field_administrator&act=edit&id=$id", "Option deleted!");
	}
	
	function save_option(){
		
		$id 	= FactoryLayer::getRequest($_REQUEST,"id");
		$id_opt  = FactoryLayer::getRequest($_REQUEST,"fo_id");
		$option  = FactoryLayer::getRequest($_REQUEST,"fo_name");
		$f = new FactoryFields($this->_db);
		$f->update_option($id_opt,$option);
		FactoryLayer::redirect("index.php?option=".F_COMPONENT_NAME."&task=field_administrator&act=edit&id=$id", "Option saved!");
		
	}
	
	function save()
	{
		global $JAds_tables;
		
		$id = FactoryLayer::getRequest($_REQUEST,"id");
		$cid = JRequest::getVar( 'parent', array(), '', 'array' );
		$f_apply = (int)JRequest::getVar( 'fields_aply');
		
		$f = new FactoryFields($this->_db);
		
		$f->load($id);
		$f->bind($_POST);
		$f->id=$id;
		
		// some lazy validation
		if(!$f->id)
			if(!$f->db_name || !$f->name){
				FactoryLayer::redirect("index.php?option=".F_COMPONENT_NAME."&task=field_administrator&act=edit",F_SAVE_ERROR);
			}

		$f->own_table = $JAds_tables[$f->page];
		
		$f->has_options =0;
		
		$field_class="FieldType_".$f->ftype;
		if(! class_exists($field_class))
			$field_class="FieldType_inputBox";
		
		$field_class_Obj = new $field_class;
		
		$f->has_options = $field_class_Obj->has_options;
		
		$attr = $_REQUEST[$f->ftype."_attributes"];
		$attr_ini = "";
		if($attr)
		foreach ($attr as $p => $v)
			$attr_ini .= "$p=$v;";
		$f->attributes = $attr_ini;
		
		$params = $_REQUEST[$f->ftype."_params"];
		$params_ini = "";
		if($params)
		foreach ($params as $p => $v)
			$params_ini .= "$p=$v;";
		$f->params = $params_ini;
		
		$msg = $f->store();
		
		if(!$msg)
			FactoryLayer::redirect("index.php?option=".F_COMPONENT_NAME."&task=field_administrator","Error in Field saving!");
		
		$this->add_option($f->id,false);

		if($f->id){
			if(count($cid)>0){
				// delete category assignments
				$sql = "DELETE FROM #__".APP_CFIELDS_PREFIX."_fields_categories WHERE fid = '{$f->id}'";
				$this->_db->setQuery($sql);$this->_db->query();
				// add category assignemtns
				$inserts = array();
				foreach ($cid as $catid){
					$inserts[] = " ('{$f->id}', '{$catid}')";
				}
				$ins = "INSERT INTO #__".APP_CFIELDS_PREFIX."_fields_categories (`fid`,`cid`) VALUES ".implode(",",$inserts);
				$this->_db->setQuery($ins);$this->_db->query();
			}
		}
		
		
		if ($f->id){
			

			if($f_apply==1)
				FactoryLayer::redirect("index.php?option=".F_COMPONENT_NAME."&task=field_administrator&act=edit&id={$f->id}","Field saved");
			else 
				FactoryLayer::redirect("index.php?option=".F_COMPONENT_NAME."&task=field_administrator","Field saved");
		}
		else
			FactoryLayer::redirect("index.php?option=".F_COMPONENT_NAME."&task=field_administrator");
		
	}
	
	/**
	 *
	 * Edit Custom Field task 
	 * 
	 * 
	 **/
	function edit(){
		
		global $JAds_tables;
		$id = FactoryLayer::getRequest($_REQUEST,"id");
		$field = new FactoryFields($this->_db);
		$lists = array();
		$parameters_plugins = null;
		
		if($id){
			$field->load($id);
			$field_class="FieldType_".$field->ftype;
			if(! class_exists($field_class))
				$field_class="FieldType_inputBox";
			$lists["curent_type"] = new $field_class;
			if($lists["curent_type"]->has_options) 
				$lists["curent_type"]->_options = $field->getOptions();
		}else{
			$lists["curent_type"]->has_options=0;
			$field->compulsory=0;
			$field->search=0;
			$field->status=1;
		}

		// to include custom REGEX validation
		$opts = array("" => "None",	'Email' => "Email",	'URL' => 'URL',	'Number'=> "Number"	);

		if( !$lists["curent_type"]->has_options){
			$lists['validate_type'] = FactoryHTML::selectGenericList($opts, 'validate_type', $field->validate_type, 'class="inputbox" id="validate_type" style="width:120px;"' ); 
		}
		else 
			$lists['validate_type'] = " - - - - - - - - - ";
		
		
		
		$pl = FactoryFieldTypes::getAllTypes(true);
		
		$option_plugins = "";
		$plugin_names = "";
		
		if($pl){
			$tmp = array();
			$tmp2 = array();
			foreach ($pl as $plname){
				
				$tmp2[]="'".$plname->class_name."'";
				if($plname->has_options)
					$tmp[]="'".$plname->class_name."'";
			}
			$option_plugins = implode(",",$tmp);
			$plugin_names = implode(",",$tmp2);
			
		}
		
		
		$lists["field_type_parameters"] = $parameters_plugins;
			
		
		$script = "var JTHEF_AllOptionTypes = new Array($plugin_names);";
		$script .= "var JTHEF_HasOptionTypes = new Array($option_plugins);";
		$jdoc = & JFactory::getDocument();
		$jdoc->addScriptDeclaration($script);
		
		if( $lists["curent_type"]->has_options && count($lists["curent_type"]->_options )){
			$lists["field_types"] = $field->getTypes($field->ftype, true );
			$lists["field_types_list"] = $pl;
		}
		else{
			$lists["field_types"] = $field->getTypes($field->ftype);
			$lists["field_types_list"] = $pl;
		}
			
			

		if($id){
			
			$local_attributes = null;
			$ftype = $lists["curent_type"];
			
			$attributes_ini = $ftype->getAttributes();
			$params_ini = $ftype->getParams();
			
			for($m = 0; $m < count($lists["field_types_list"]) ; $m++ ){
				$pua = $lists["field_types_list"][$m];
				if($pua->class_name==$ftype->class_name){
					
					
					$real_attributes = $attributes_ini;
					$cattributes = explode(";",$field->attributes);
					
					foreach ($cattributes as $cua => $tattr)
						if($tattr!=""){
							$c = explode("=", $tattr);
							$real_attributes[$c[0]] = $c[1];
						}
					$pua->_attributes = $real_attributes;	
					
					$real_params = $params_ini;
					$cparams = explode(";",$field->params);
					
					foreach ($cparams as $cua => $tp)
						if($tp!=""){
							$c = explode("=", $tp);
							$real_params[$c[0]] = $c[1];
						}
					$pua->_params = $real_params;
				}
					
			}
			
			$lists["field_pages"] = "<input type='hidden' name='page' id='page' value='{$field->page}' />".FactoryFieldsHelper::getPageName($field->page);
			
		}else{
			
			$lists["field_pages"] = $field->getPages($field->page);
			
		}
		
	    $cat	= JTheFactoryCategory::getInstance(APP_CATEGORY_TABLE,APP_CATEGORY_DEPTH);
	    
	    $this->_db->setQuery("SELECT c.id as value, catname as text FROM #__".APP_CFIELDS_PREFIX."_categories as c RIGHT JOIN #__".APP_CFIELDS_PREFIX."_fields_categories as fc ON c.id = fc.cid WHERE fc.fid = '{$id}'");
	    $assigned = $this->_db->loadObjectList();
		$lists["category"] 	 = $cat->makeCatSelectMultiple($assigned,"parent[]","200");
		
		
		
		$lists["compulsory"] = FactoryHTML::selectBooleanList('compulsory','',$field->compulsory,$yes="Yes",$no="No");
		$lists["search"] 	 = FactoryHTML::selectBooleanList('search','',$field->search,$yes="Yes",$no="No");
		$lists["status"] 	 = FactoryHTML::selectBooleanList('status','',$field->status,$yes="Yes",$no="No");
		
		$existing_fields = array();
		$lists["existing_fields"] = "var JTHEF_Fields = new Array(2);";
		
		foreach($JAds_tables as $k => $i){
			
			$pageName = FactoryFieldsHelper::getPage($i);
			
			if ($i)
				$existing_fields = $this->_db->getTableFields($i);
			$lists["existing_fields"] .= "JTHEF_Fields['$pageName']= new Array(".count($existing_fields[$i])."); \r\n ";
			$p=0;
			if($existing_fields[$i])
			foreach($existing_fields[$i] as $kkc => $iic){
				$lists["existing_fields"] .= "JTHEF_Fields['$pageName'][$p]='$kkc';\r\n";
				$p++;
			}
		}
		
		$jdoc = & JFactory::getDocument();
		$jdoc->addScript(F_ROOT_URI."administrator/components/".F_COMPONENT_NAME."/thefactory/fields/assets/fields.js");
		if($id){
			$jdoc->addScriptDeclaration(" window.addEvent('domready', function(){ toggleParameterTab('$field->ftype'); }) ");
		}
		
		//echo nl2br($lists["existing_fields"]);exit; 
		require_once(dirname(__FILE__).DS."..".DS."tmpl".DS."field_edit.php");
		return true;
	}
	
	function list_fields(){
		
		$app	= JFactory::getApplication();

		$context = "";

		$limit		= $app->getUserStateFromRequest('global.list.limit', 'limit', $app->getCfg('list_limit'), 'int');
		$limitstart	= $app->getUserStateFromRequest($context.'limitstart', 'limitstart', 0, 'int');

		// In case limit has been changed, adjust limitstart accordingly
		$limitstart = ( $limit != 0 ? (floor($limitstart / $limit) * $limit) : 0 );

		$this->_db->setQuery("SELECT COUNT(*) FROM #__".APP_CFIELDS_PREFIX."_fields");
		$total = $this->_db->loadResult();

		$page = FactoryLayer::initPagination($total, $limitstart, $limit);

		$this->_db->setQuery("SELECT * FROM #__".APP_CFIELDS_PREFIX."_fields ORDER BY ordering ASC, page ASC ", $page->limitstart, $page->limit);
		$rows = $this->_db->loadObjectList();

		require_once(dirname(__FILE__).DS."..".DS."tmpl".DS."field_list.php");
        return true;
	}

	
}


