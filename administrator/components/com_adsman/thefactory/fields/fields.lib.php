<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/


class FactoryFields extends JFactorySystemTable {
	
	var $id					= null;
	var $name				= null;
	var $db_name			= null;
	var $field_id			= null;
	var $page				= null;
	var $search				= null;
	var $ftype				= null;
	var $compulsory			= null;
	var $status		        = null;
	var $own_table			= null;
	var $validate_type		= null;
	var $css_class			= null;
	var $style_attr			= null;
	var $attributes			= null;
	var $params				= null;
	var $ordering			= null;
	var $help				= null;
	
	function FactoryFields( &$db ) {
		$this->__construct( "#__".APP_CFIELDS_PREFIX."_fields", 'id', $db );
	}
	
	/**
	 *
	 * Store Custom Field 
	 * 
	 **/
	function store(){
		
		$field_class="FieldType_".$this->ftype;

		$F = new $field_class;
		$d = $this->_db->getTableFields($this->own_table);
		
		$l = ($F->length)?"(".$F->length.")":"";
		
		
		// field exists: edit
		if(isset($this->id) && $this->id>0){
			return parent::store();
			//else create new
		}
		if (!isset($d[$this->own_table][$this->db_name]))
		{
			//echo $this->_db->_sql;exit;
			$this->_db->setQuery("ALTER TABLE $this->own_table ADD $this->db_name $F->sql_type $l;");
			$this->_db->query();
			
			$d = $this->_db->getTableFields($this->own_table);
			if ( isset($d[$this->own_table][$this->db_name]) ){
				return parent::store();
			}else{
				return false;
			}
		}
		
	}
	
	function delete($oid=null){
		
		$this->load($oid);
		
		// Core Test: Some fields can not be deletable!
		
		
		
		
	// DELETE actual field from table
	if( !in_array($this->db_name , array("title","start_date","end_date","id","userid","user_id")) ){
		$this->_db->setQuery("ALTER TABLE $this->own_table DROP `$this->db_name` ;");
		$this->_db->query();
	}
		
	// DELETE all it's options
		$this->_db->setQuery("DELETE FROM #__".APP_CFIELDS_PREFIX."_fields_options WHERE fid = '{$this->id}'");
		$this->_db->query();

	// DELETE all it's assignings to positions
		$this->_db->setQuery("DELETE FROM #__".APP_CFIELDS_PREFIX."_fields_positions WHERE fid = '{$this->id}'");
		$this->_db->query();

	// DELETE all it's assignings to categories
		$this->_db->setQuery("DELETE FROM #__".APP_CFIELDS_PREFIX."_fields_categories WHERE fid = '{$this->id}'");
		$this->_db->query();
		
		parent::delete($oid);

	}
	
	/**
	 *
	 * Custom Field Options methods
	 *  
	 **/
	function store_option($option){
		
		$option = $this->_db->getEscaped($option);
		$this->_db->setQuery("INSERT INTO #__".APP_CFIELDS_PREFIX."_fields_options SET fid = '{$this->id}', option_name ='{$option}'");
		$this->_db->query();
	}
	
	function del_option($id_opt){
		$option = $this->_db->getEscaped($id_opt);
		$this->_db->setQuery("DELETE FROM #__".APP_CFIELDS_PREFIX."_fields_options WHERE id ='{$option}'");
		$this->_db->query();
	}
	
	function update_option($id_opt, $option){
		$id_opt = $this->_db->getEscaped($id_opt);
		$option = $this->_db->getEscaped($option);
		$this->_db->setQuery("UPDATE #__".APP_CFIELDS_PREFIX."_fields_options SET option_name ='{$option}' WHERE id ='{$id_opt}'");
		$this->_db->query();
	}
	
	function getOptions(){
		$this->_db->setQuery("SELECT * FROM #__".APP_CFIELDS_PREFIX."_fields_options WHERE fid = '{$this->id}'");
		return $this->_db->loadObjectList("id"); 
	}
	
	function getValues($section, $owner_id){
		global $JAds_tables, $JAds_pk;

		$this->_db->setQuery("SELECT * FROM $JAds_tables[$section] WHERE $JAds_pk[$section] = '{$owner_id}' ");
		$ret = $this->_db->loadObjectList();
		if ( count($ret) )
			return $ret[0];
		return null;
	}
	
	
	// TO DO
	function JGetAtributes(){
		
	}

	// TO DO
	function JStoreAtributes(){
		
	}
	
	// TO DO pluggins easyer to config
	// CB is not an example here
	function getTypes($selected = null, $OnlyOptions_types=false){

		$plugins = FactoryFieldTypes::getAllTypes($OnlyOptions_types);
		$opts = array();
		foreach ( $plugins as $k => $item ){
			$opts[$item->class_name] = $item->type_name;
		}

		return FactoryHTML::selectGenericList( $opts, 'ftype', $selected, 'class="inputbox" onchange="toggleParameterTab(this.value)" id="ftype" style="width:120px;"' );

	}
	
	
	/**
	 *
	 * TO DO: Move outside in PageClass
	 *  
	 **/
	function getPages($selected = null){
		global $JAds_pages;
    	return FactoryHTML::selectGenericList( $JAds_pages,'page',$selected,'class="inputbox" id="page" style="width:120px;"','value', 'text');
	}
}



class FactoryFieldsTypesHelper{
	function is_valid_Email($email){
		// Split the email into a local and domain
		$atIndex	= strrpos($email, "@");
		$domain		= substr($email, $atIndex+1);
		$local		= substr($email, 0, $atIndex);

		// Check Length of domain
		$domainLen	= strlen($domain);
		if ($domainLen < 1 || $domainLen > 255) {
			return false;
		}

		// Check the local address
		// We're a bit more conservative about what constitutes a "legal" address, that is, A-Za-z0-9!#$%&\'*+/=?^_`{|}~-
		$allowed	= 'A-Za-z0-9!#&*+=?_-';
		$regex		= "/^[$allowed][\.$allowed]{0,63}$/";
		if ( ! preg_match($regex, $local) ) {
			return false;
		}

		// No problem if the domain looks like an IP address, ish
		$regex		= '/^[0-9\.]+$/';
		if ( preg_match($regex, $domain)) {
			return true;
		}

		// Check Lengths
		$localLen	= strlen($local);
		if ($localLen < 1 || $localLen > 64) {
			return false;
		}

		// Check the domain
		$domain_array	= explode(".", $domain);
		$regex		= '/^[A-Za-z0-9-]{0,63}$/';
		foreach ($domain_array as $domain ) {

			// Must be something
			if ( ! $domain ) {
				return false;
			}

			// Check for invalid characters
			if ( ! preg_match($regex, $domain) ) {
				return false;
			}

			// Check for a dash at the beginning of the domain
			if ( strpos($domain, '-' ) === 0 ) {
				return false;
			}

			// Check for a dash at the end of the domain
			$length = strlen($domain) -1;
			if ( strpos($domain, '-', $length ) === $length ) {
				return false;
			}

		}

		return true;
	}
	function is_valid_Number($number){return is_numeric($number);}
	function is_valid_URL($url){
		return preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $url);
	}
}



/**
 * 
 * 
 * Table to extend for other tables that accept cutom fields
 * 
 *  overrides loadFull, load, store of a common DB Table class to proccess
 * 	custom fields simultanious likely with the generic action of the parent item
 *  
 *  
 *  provides html display of fields for a parent item 
 *  
 * 
 **/
class FactoryFieldsTbl extends JFactorySystemTable{
	
	var $_F_section = null;
	
	/**
	 *  Regular loading of a row with custom fields
	 * 
	 **/
	function load($oid=null){
		
		$e = parent::load($oid);
		
		global $JAds_tables;
		$this->_F_section = array_search($this->_tbl, $JAds_tables);
		
		require_once(F_ROOT_DIR."/components/".APP_EXTENSION."/thefactory/front.fields.php");
		$Fi = & FactoryFieldsFront::getInstance();
		$k = $this->_tbl_key;
		
		$fields = $Fi->getCustomFields($this->_F_section, $oid);
		
		for( $i=0; $i< count($fields); $i++ )
		{
			$fieldname = $fields[$i]["db_name"];
			$this->$fieldname=$fields[$i]["value"];
		}
		
		return $e;
	}
	
	/**
	 * @since 1.3.1
	 *
	 * @param $oid
	 */
	function ancLoad($oid){

		global $JAds_tables;
		$this->_F_section = array_search($this->_tbl, $JAds_tables);
		
		require_once(F_ROOT_DIR."/components/".APP_EXTENSION."/thefactory/front.fields.php");
		$Fi = & FactoryFieldsFront::getInstance();
		$k = $this->_tbl_key;
		
		$fields = $Fi->getCustomFields($this->_F_section,null);
		
		
		for( $i=0; $i< count($fields); $i++ )
		{
			$fieldname = $fields[$i]["db_name"];
			$this->$fieldname=null;
		}
		
		$e = parent::load($oid);
		
	}
		
	/**
	 * Database Loads a row from the database and binds the fields to the object properties
	 * 
	 * 
	 *
	 * @access	public
	 * @param	mixed	Optional primary key.  If not specifed, the value of current key is used
	 * @param		
	 * 		array(
	 * 			"join_select" 	=> array("ex.name","ex_property_name"),
	 * 			"join_condition"	=> "ex.id =a.id",
	 * 			"join_table" 	=> "jooomlaaaa AS ex"
	 * 		)
	 * @return	boolean	True if successful
	 */
	function loadData( $oid=null , &$SelectList , &$JoinList , $alias_helper=null )
	{
		$k = $this->_tbl_key;

		if ($oid !== null) {
			$this->$k = $oid;
		}

		$oid = $this->$k;
		$tblname = $this->_tbl;
		$tblkey = $this->_tbl_key;

		if ($oid === null) {
			return false;
		}
		$this->reset();


		$select = "";
		$join = "";
		
		global $JAds_tables;
		$page =  array_search($tblname, $JAds_tables);
		FactoryFieldsFront::appendCustomFields($this);
		FactoryFieldsFront::getCustomFieldsJoin($JoinList, $SelectList, $page, $alias_helper);
		
		if( count($JoinList)>0 )
			$join 	= "\r\n".implode(" \r\n ",$JoinList)."\r\n";
		if(count($SelectList)>0)
			$select = ",".implode(" , ",$SelectList);

		
		$db =& $this->getDBO();

		$query = 'SELECT ptable.*'
		. $select
		. ' FROM '.$tblname.' AS ptable '
		. $join
		. ' WHERE ptable.'.$tblkey.' = '.$db->Quote($oid)
		. ' GROUP BY ptable.'.$tblkey;

		$db->setQuery( $query );

		
		//echo nl2br($db->_sql);exit;
		if ($result = $db->loadAssoc( )) {
			$r =  $this->bind($result);

			//Ads_Debug::var_dump($this,1);
			return $r;
		}
		else
		{
			$this->setError( $db->getErrorMsg() );
			return false;
		}
		
	}
	
	/**
	 * TO DO:
	 * The name of the method quite says what it does
	 *
	 */
	function appendFieldsAsPropertiesToObject(&$obj){
		
	}

	function validate($value2Validate, $plugin)
	{
		$error = null;
		
		
		if(!$value2Validate && $plugin->compulsory)
		{
			$error = JText::_("ADS_FIELD") . $plugin->name . JText::_("ADS_FIELD_IS_COMPULSORY");
		}
		
		if($value2Validate && $plugin->validate_type ){
		
			switch ( strtolower($plugin->validate_type) )
			{
				case "email":{
					if(!FactoryFieldsTypesHelper::is_valid_Email($value2Validate))
						$error = $plugin->name. JText::_("ADS_FIELDS_VALIDATE_EMAIL");
					break;
				}
				case "number":{
					if(!FactoryFieldsTypesHelper::is_valid_Number($value2Validate))
						$error = $plugin->name. JText::_("ADS_FIELDS_VALIDATE_NUMBER");
					break;
				}
				case "url":{
					
					if(!FactoryFieldsTypesHelper::is_valid_URL($value2Validate))
						$error = $plugin->name. JText::_("ADS_FIELDS_VALIDATE_URL");
					break;
				}
			}
		}
		
		return $error;
		
	}
	

	function store(){
		
		global $JAds_tables;
		$this->_F_section = array_search($this->_tbl, $JAds_tables);
		
		$kname = $this->_tbl_key;
		$posted_val = JRequest::get('POST');

		if ( (isset($posted_val['id']) && $posted_val['task'] == 'save') || ($posted_val['task'] == 'quickaddcat') || ($posted_val['task'] == 'savecat') || ($posted_val['task'] == 'saveUserDetails')) {
		
			$s = $this->saveFields($this->_F_section, $this->_F_section, $this->$kname );
		
			if( $s!==true )
				return $s;
		 }
		
		 $s = parent::store();

 		 return $s; 
		
	}
		
	/**
	 * Method to call ancestral store
	 * @since 1.5.4
	 *
	 * @return bool
	 */
	function ancStore(){
		return parent::store();
	}
	
	/**
	 * Validates from REQUEST and Appends db table the custom fields as properties
	 *  after this store will save the fields too;
	 *
	 * @param $section
	 * @param $owner_name
	 * @param $owner_id
	 * @return boolean
	 */
	
	function saveFields($section, $owner_name=null, $owner_id=null){
		
		global $JAds_tables, $JAds_pk;
		$field_table = new FactoryFields($this->_db);
				
		//$category = (int)JRequest::getVar('custom_fields_category');
        $posted_val = JRequest::get('POST');
        $category = (int)JRequest::getVar('category');

      	$allowed_fields_sql = "";
		$allowed_fields = null;
		
		if( $category!=0 ){
			$this->_db->setQuery("SELECT fid FROM #__".APP_CFIELDS_PREFIX."_fields_categories WHERE cid = '{$category}'");
			$allowed_fields = $this->_db->loadResultArray();
		}

		if (count($allowed_fields)) {
			$allowed_fields_sql = " AND id IN (".implode(",",$allowed_fields).")";
		}

		$this->_db->setQuery("SELECT * FROM #__".APP_CFIELDS_PREFIX."_fields WHERE page = '$section' AND status = 1 $allowed_fields_sql order by ordering");
		$l =  $this->_db->loadObjectList();

		if (count($l) && ( count($allowed_fields) || $category==0 || $section=="user_profile" ) ){
		//if (count($l)){
			$msg = array();
			foreach($l as $k => $plugin){
				$field_class="FieldType_".$plugin->ftype;
				
				// Check to see that field type is available 
				if(class_exists($field_class)){
					
					$FObject = new $field_class;
					$FObject->owner_id = $owner_id;
					$f=$plugin->db_name;
					
					$d = $FObject->getVar($plugin->db_name,$this->$f);
					//$d = FactoryLayer::getRequest($_REQUEST, $plugin->db_name);
					$m = $this->validate( $d , $plugin );
					
					if($m!="")
						$msg[] = $m;
					
					// MYSQL ESCAPE STRING SUCKS
					if($d!==null) {
						$this->$f = str_replace('\r\n', '<br />', $this->_db->getEscaped($d, true));
                    }
				}
			}

			if(count($msg)>0)
				return implode("<br />", $msg);
			else return true;
		}else 
			return true;

	}
	
	function getEditHTMLFields(){
		

		global $JAds_tables;
		$this->_F_section = array_search($this->_tbl, $JAds_tables);
		$kname = $this->_tbl_key;
		
		$field_table = new FactoryFields($this->_db);
		$values = $field_table->getValues($this->_F_section, $this->$kname );
		
		$this->_db->setQuery("SELECT * FROM #__".APP_CFIELDS_PREFIX."_fields WHERE page = '$this->_F_section' AND status = 1 order by ordering");
		$l =  $this->_db->loadObjectList();
		
		$outputZ = "";
		$o = 0;
		if (count($l))		
			foreach($l as $k => $plugin){
				$field_class="FieldType_".$plugin->ftype;
				if(class_exists($field_class)){
					$pl = new $field_class;
					$db_name = $plugin->db_name;
					$field_table->load($plugin->id);
					if($pl->has_options){
						$pl->_options = $field_table->getOptions();  
					}
					if(isset($values->$db_name))
						$pl->value  = $values->$db_name;
					$outputZ .= $plugin->db_name." ".$pl->display($plugin->db_name,$plugin->field_id,$plugin->style_attr, $plugin->css_class)." <br />";
					$o++; 
				}
			}
		
		return $outputZ;
	}
	
	
}


class FactoryFieldsHelper{
	
	/**
	 *
	 * Returns the Page name from given table name parameter 
	 * 
	 **/
	function getPage($TableName)
	{
		global $JAds_tables;
		return array_search($TableName, $JAds_tables);
		
	}
	
	function getPageName($PageName){
		global $JAds_pages;
		return $JAds_pages[$PageName];
	}
	
}

