<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/


define("F_GENERAL_TITLE","Fields manager");
define("F_SAVE","Save");
define("F_APPLY","Apply");
define("F_LIST","List fields");
define("F_NEW","New field");
define("F_NUM","Num");
define("F_ORDERING","Ordering");
define("F_FIELDNAME","Name");
define("F_FIELDTYPE","Type");
define("F_ENABLED","Enabled");
define("F_COMPULSORY","Compulsory");
define("F_SEARCH","Search-able");
define("F_ID","ID");
define("F_FIELDLABEL","Field Label");
define("F_FIELDDBNAME","DB Name");
define("F_SECTION","Section");
define("F_VALIDATE_TYPE","Validate Type");
define("F_FIELDLABEL_HELP","Field Label as it will appear on frontend forms");
define("F_FIELDDBNAME_HELP","Mysql Field Name - must be unique");
define("F_CBWARN","If you use CB Profiler, User Profile fields must be created in CB not in this admin area.");
define("F_COMPULSORY_HELP","Required on frontend");
define("F_SEARCH_HELP","Should appear in the frontend search form?");
define("F_ENABLED_HELP","Set to Yes in order to publish it");
define("F_CSS_CLASS","CSS Class Name");
define("F_FIELDID","Field ID");
define("F_FIELDID_HELP","HTML/CSS Field ID");
define("F_STYLE_ATTR","Style Attributes");
define("F_VALUES","Values");
define("F_SAVE4OPTIONS","Save field to access aditional options");
define("F_DELETE","Delete");
define("F_PAGE","Section");
define("F_SAVE_ERROR","Fill out the label and db name fields!");
define("F_DELETE_FIELD_OK","Field deleted!");
define("F_HELP","Info tip");
define("F_MANAGE_POSITIONS","Manage Positions");
define("F_STEP1_TITLE","Custom Fields Step1: Define custom fields");
define("F_STEP1","Step 1");
define("F_STEP2","Step 2");
define("F_STEP3","Step 3");
define("F_BACK","Back");
define("F_HELP_POSITIONS","Help");