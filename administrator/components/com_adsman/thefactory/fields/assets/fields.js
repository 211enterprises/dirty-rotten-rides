Array.prototype.in_array = function(p_val){for(var i=0,l=this.length;i<l;i++){if(this[i] == p_val){return true;}}return false;}

function prep4SQL(o){
	if(o.value!='') {
		var cbsqloldvalue, cbsqlnewvalue;
		cbsqloldvalue = o.value;
		o.value=o.value.replace(/[^a-zA-Z0-9]+/g,'');
		cbsqlnewvalue = o.value;
		if (cbsqloldvalue != cbsqlnewvalue) {
			alert("Warning: SQL name of field has been changed to fit SQL constraints")
		}
	}
}

function toggleParameterTab(tid){
	var b = JTHEF_HasOptionTypes.in_array(tid);
	
	if(b==1){
		document.getElementById("parameter_tab").style.display='';
		document.getElementById("parameter_tab_message").style.display='none';
	}else{
		document.getElementById("parameter_tab").style.display='none';
		document.getElementById("parameter_tab_message").style.display='';
	}
	
	for(var k=0; k < JTHEF_AllOptionTypes.length; k++)
		document.getElementById("custom_attr_"+JTHEF_AllOptionTypes[k]).style.display='none';

	document.getElementById("custom_attr_"+tid).style.display='';
	
	for(var k=0; k < JTHEF_AllOptionTypes.length; k++)
		document.getElementById("custom_params_"+JTHEF_AllOptionTypes[k]).style.display='none';

	document.getElementById("custom_params_"+tid).style.display='';
	
}
