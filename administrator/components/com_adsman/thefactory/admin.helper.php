<?php
/**------------------------------------------------------------------------
com_adsman -  Ads Factory 3.4.4
------------------------------------------------------------------------
 * @author TheFactory
 * @copyright Copyright (C) 2013 SKEPSIS Consult SRL. All Rights Reserved.
 * @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * Websites: http://www.thefactory.ro
 * Technical Support: Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/


class JTheFactoryAdminHelper extends JObject
{
    function beginAdminForm($task,$hiddenfields=null,$method='post',$use_list=false,$heading=null)
    {
	    ?>
        <fieldset class="adminform">
            <legend><span style="float:left;"><?php echo $heading; ?></span></legend>
         <form action="index.php" method="<?php echo $method;?>" name="adminForm" id="adminForm" enctype="multipart/form-data">
         <input type="hidden" name="option" value="<?php echo APP_EXTENSION;?>" />
         <input type="hidden" name="task" value="<?php echo $task;?>" />
	    <?php
        if ($use_list) {
            ?>
                 <input type="hidden" name="boxchecked" value="0" />
             <?php
        }
        if (count($hiddenfields))
            foreach ($hiddenfields as $name=>$value)
            {
            ?>
                 <input type="hidden" name="<?php echo $name;?>" value="<?php echo $value; ?>" />
             <?php
            }

    }
    
    function endAdminForm()
    {
        ?></form>
    </fieldset>
        <?php
    }
    
    function showAdminTableList($header_row,$rows,$pageNav=null,$heading=null,$filters=null)
    {
     ?>

     <table width="100%">
   <?php //if ($heading != 'Payment Items Configuration' && $heading != ''){ ?>

<!--      <tr>
          <th align="left" colspan="2">
              <h2 style="text-align:left;">
                  <img src="<?php echo F_ROOT_URI;?>administrator/components/com_adsman/img/toolbar/adscategories.png" alt="Categories" border="0" style="vertical-align:middle;" />&nbsp;&nbsp;<?php echo $heading; ?>
              </h2>
          </th>
-->

       <!--<td colspan="2">
    	<table class="adminheading">
    	 <tr>
    	  <th width="40%"><?php //echo $heading; ?></th>
    	  </tr>
    	 </table>
    	</td>-->
<!--       </tr>-->
       </table>
   <?php //} ?>
   <?php if ($filters){ ?>
       <div>
       		<span class='tooltip_left' style="margin: 4px 3px; padding:2px;"><?php echo JText::_('ADS_FILTER'); ?></span>
      		<span><?php echo $filters; ?></span> 
      	</div>
   <?php } ?>
   <table class="adminlist" width="100%">
   <?php if (count($header_row)){
    	  echo "<thead>";
    	  echo "<tr>";
    	  foreach ($header_row as $header)
    	  {

    	      if (is_array($header)){
    	          $attribs='';
    	          
    	          if (isset($header["width"]))
					$attribs.=' width="'.$header["width"].'"';
    	          
				  if(isset($header["align"]))
				  	$attribs.=' align="'.$header["align"].'"';
    	          
				  if (isset($header["class"]))
				  	$attribs.=' class="'.$header["class"].'"';

                  if (isset($header["nowrap"]))
                  	$attribs.=' nowrap="'.$header["nowrap"].'"';
    	          
				  if(isset($header["attributes"]))
				  	$attribs.=' '.$header["attributes"].' ';

    	          echo "<th $attribs>".JText::_($header["th"])."</th>";
    	      } else {
    	          echo "<th>".JText::_($header)."</th>";
    	      }
    	  }
    	  echo "</tr>";
    	  echo "</thead>";
    }

    $k=0;

    for ($i=0;$i<count($rows);$i++) {
       $k=1-$k;
       $row=$rows[$i];
       ?>
         <tr class="<?php echo 'row',$k; ?>">
        <?php
            for($j=0;$j<count($row);$j++){

                $r=$row[$j];

                if (is_array($r)) {
                  $attribs='';

                  if (isset($r["width"]))
                    $attribs.=' width="'.$r["width"].'"';

                  //if ($r["align"])
                    //$attribs.=' align="'.$r["align"].'"';

                  if ($r["class"])
                    $attribs.=' class="'.$r["class"].'"';

                 // $attribs.=' '.$r["attributes"]." ";

                    echo "<td $attribs>".$r["td"]."</td>";
                }
                else
                {
                    echo "<td>$r</td>";
                }
            }
         echo "</tr>";
       }
    echo "</table>";
    echo "<table class='adminlist'>";
      echo "<tfoot>";
         echo "<tr>";
	         echo "<td>";
		    if($pageNav) echo $pageNav->getListFooter();
	         echo "</td>";
         echo "</tr>";
      echo "</tfoot>";
    echo "</table>";

  }

    function quickiconButton( $link, $image, $text ) {
    	?>
    	<div style="float:left;">
    		<div class="icon">
    			<a href="<?php echo $link; ?>">
    				<?php
    					echo JHTML::_('image.administrator', $image,'../components/'.APP_EXTENSION.'/images/', NULL, NULL, $text );
    				?>
    				<span><?php echo $text; ?></span>
    			</a>
    		</div>
    	</div>
    	<?php
    }

	function writableCell( $folder, $relative=1, $text='', $visible=1, $make_writable_link="" )
	{
		$writeable		= '<b><font color="green">'. JText::_( 'ADS_WRITABLE' ) .'</font></b>';
		$unwriteable	= '<b><font color="red">'. JText::_( 'ADS_UNWRITABLE' ). " <img src='".JURI::root()."administrator/images/cpanel.png' border=0 style='width:20px !important; vertical-align:middle;' /><a href='$make_writable_link'>Try Chmode 755</a>  " .'  </font></b>';
	
		echo '<tr>';
		echo '<td class="item">';
		echo $text;
		if ( $visible ) {
			echo $folder;
		}
		echo '</td>';
		echo '<td >';
		if ( $relative ) {
			echo is_writable( JPATH_ROOT.DS."$folder" )	? $writeable : $unwriteable;
		} else {
			echo is_writable( "$folder" )		? $writeable : $unwriteable;
		}
		echo '</td>';
		echo '</tr>';
	}
	
	function get_php_setting($val)
	{
		$r =  (ini_get($val) == '1' ? 1 : 0);
		return $r ? JText::_( 'ON' ) : JText::_( 'OFF' ) ;
	}

}


?>