CREATE TABLE IF NOT EXISTS `#__home_media` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`url` VARCHAR(255)  NOT NULL ,
`image_url` VARCHAR(255)  NOT NULL ,
`type` VARCHAR(255)  NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`created_at` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8_unicode_ci;

