<?php
$dfcontact = array (
  'destinationMail' => 'allan@acmediadesigns.com',
  'htmlMails' => '1',
  'addServerData' => '1',
  'displayUserInput' => '1',
  'links' => '1',
  'onlineStatus' => '0',
  'captcha' => '0',
  'recaptcha_theme' => 'red',
  'recaptcha_public_key' => '',
  'recaptcha_private_key' => '',
  'pageTitle' => 
  array (
    'en-GB' => 'Contact',
  ),
  'infoText' => 
  array (
    'en-GB' => 'If you have any ideas or questions, please feel free to contact us using our contact data or the form below.',
  ),
  'formText' => 
  array (
    'en-GB' => 'Just a test blabla.',
  ),
  'buttonText' => 
  array (
    'en-GB' => 'Send',
  ),
  'field' => 
  array (
    'company' => 
    array (
      'value' => 'Dirty Rotten RIdes',
      'display' => '0',
    ),
    'title' => 
    array (
      'value' => '',
      'display' => '0',
    ),
    'name' => 
    array (
      'value' => '',
      'display' => '1',
      'duty' => '1',
    ),
    'position' => 
    array (
      'value' => '',
      'display' => '0',
    ),
    'addition' => 
    array (
      'value' => '',
      'display' => '0',
    ),
    'street' => 
    array (
      'value' => '',
      'display' => '0',
    ),
    'streetno' => 
    array (
      'value' => '',
      'display' => '0',
    ),
    'postbox' => 
    array (
      'value' => '',
      'display' => '0',
    ),
    'zip' => 
    array (
      'value' => '',
      'display' => '0',
    ),
    'city' => 
    array (
      'value' => '',
      'display' => '0',
    ),
    'state' => 
    array (
      'value' => '',
      'display' => '0',
    ),
    'country' => 
    array (
      'value' => '',
      'display' => '0',
    ),
    'geocoordinates' => 
    array (
      'value' => '',
      'display' => '0',
    ),
    'phone' => 
    array (
      'value' => '',
      'display' => '1',
      'duty' => '0',
    ),
    'ipphone' => 
    array (
      'value' => '',
      'display' => '0',
    ),
    'mobile' => 
    array (
      'value' => '',
      'display' => '1',
      'duty' => '0',
    ),
    'fax' => 
    array (
      'value' => '',
      'display' => '1',
      'duty' => '0',
    ),
    'email' => 
    array (
      'value' => 'webmaster@dirtyrottenrides.com',
      'display' => '1',
      'duty' => '1',
    ),
    'website' => 
    array (
      'value' => '',
      'display' => '0',
    ),
    'skype' => 
    array (
      'value' => '',
      'display' => '0',
    ),
    'aim' => 
    array (
      'value' => '',
      'display' => '0',
    ),
    'icq' => 
    array (
      'value' => '',
      'display' => '0',
    ),
    'yahoo' => 
    array (
      'value' => '',
      'display' => '0',
    ),
    'msn' => 
    array (
      'value' => '',
      'display' => '0',
    ),
    'linkedin' => 
    array (
      'value' => '',
      'display' => '0',
    ),
    'xing' => 
    array (
      'value' => '',
      'display' => '0',
    ),
    'facebook' => 
    array (
      'value' => '',
      'display' => '0',
    ),
    'googleplus' => 
    array (
      'value' => '',
      'display' => '0',
    ),
    'twitter' => 
    array (
      'value' => '',
      'display' => '0',
    ),
    'message' => 
    array (
      'display' => '1',
      'duty' => '1',
    ),
    'checkbox' => 
    array (
      'display' => '0',
      'text' => 
      array (
        'en-GB' => 'Dummytext.',
      ),
    ),
  ),
  'addressFormat' => 
  array (
    'selected' => 'us',
    'templates' => 
    array (
      'self' => '[COMPANY]
[TITLE]
[NAME]
[POSITION]
[ADDITION]
[STREETNO]{3} [STREET]
[POSTBOX]
[CITY], [STATE]{2} [ZIP]{4}
[COUNTRY]

[GEOCOORDINATES]

[PHONE]
[IPPHONE]
[MOBILE]
[FAX]

[EMAIL]
[WEBSITE]

[SKYPE]
[ICQ]
[AIM]
[MSN]
[YAHOO]
[LINKEDIN]
[XING]
[FACEBOOK]
[GOOGLEPLUS]
[TWITTER]

[OTHER]

[MESSAGE]

[CHECKBOX]

[CAPTCHA]

[MANDATORY]',
      'de' => '[COMPANY]
[TITLE]
[NAME]
[POSITION]
[ADDITION]
[STREET] [STREETNO]{3}
[POSTBOX]
[ZIP]{5} [CITY]
[STATE]
[COUNTRY]

[GEOCOORDINATES]

[PHONE]
[IPPHONE]
[MOBILE]
[FAX]

[EMAIL]
[WEBSITE]

[SKYPE]
[ICQ]
[AIM]
[MSN]
[YAHOO]
[LINKEDIN]
[XING]
[FACEBOOK]
[GOOGLEPLUS]
[TWITTER]

[OTHER]

[MESSAGE]

[CHECKBOX]

[CAPTCHA]

[MANDATORY]',
      'fr' => '[COMPANY]
[TITLE]
[NAME]
[POSITION]
[ADDITION]
[STREETNO]{3} [STREET]
[POSTBOX]
[ZIP]{5} [CITY]
[STATE], [COUNTRY]

[GEOCOORDINATES]

[PHONE]
[IPPHONE]
[MOBILE]
[FAX]

[EMAIL]
[WEBSITE]

[SKYPE]
[ICQ]
[AIM]
[MSN]
[YAHOO]
[LINKEDIN]
[XING]
[FACEBOOK]
[GOOGLEPLUS]
[TWITTER]

[OTHER]

[MESSAGE]

[CHECKBOX]

[CAPTCHA]

[MANDATORY]',
      'uk' => '[COMPANY]
[TITLE]
[NAME]
[POSITION]
[ADDITION]
[STREETNO]{3} [STREET]
[POSTBOX]
[CITY]
[STATE]
[ZIP]
[COUNTRY]

[GEOCOORDINATES]

[PHONE]
[IPPHONE]
[MOBILE]
[FAX]

[EMAIL]
[WEBSITE]

[SKYPE]
[ICQ]
[AIM]
[MSN]
[YAHOO]
[LINKEDIN]
[XING]
[FACEBOOK]
[GOOGLEPLUS]
[TWITTER]

[OTHER]

[MESSAGE]

[CHECKBOX]

[CAPTCHA]

[MANDATORY]',
      'us' => '[COMPANY]
[TITLE]
[NAME]
[POSITION]
[ADDITION]
[STREETNO]{3} [STREET]
[POSTBOX]
[CITY], [STATE]{2} [ZIP]{4}
[COUNTRY]

[GEOCOORDINATES]

[PHONE]
[IPPHONE]
[MOBILE]
[FAX]

[EMAIL]
[WEBSITE]

[SKYPE]
[ICQ]
[AIM]
[MSN]
[YAHOO]
[LINKEDIN]
[XING]
[FACEBOOK]
[GOOGLEPLUS]
[TWITTER]

[OTHER]

[MESSAGE]

[CHECKBOX]

[CAPTCHA]

[MANDATORY]',
      'jp' => '[COMPANY]
[TITLE]
[NAME]
[POSITION]
[ADDITION]
[ZIP] [STATE]
[CITY]
[STREETNO] [STREET]
[POSTBOX]
[COUNTRY]

[GEOCOORDINATES]

[PHONE]
[IPPHONE]
[MOBILE]
[FAX]

[EMAIL]
[WEBSITE]

[SKYPE]
[ICQ]
[AIM]
[MSN]
[YAHOO]
[LINKEDIN]
[XING]
[FACEBOOK]
[GOOGLEPLUS]
[TWITTER]

[OTHER]

[MESSAGE]

[CHECKBOX]

[CAPTCHA]

[MANDATORY]',
    ),
  ),
);
?>