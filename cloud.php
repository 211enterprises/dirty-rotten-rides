<?php
// We are a valid Joomla entry point.
define('_JEXEC', 1);

// Setup the base path related constant.
define('JPATH_BASE', dirname(__FILE__));

// Maximise error reporting.
error_reporting(E_ALL);
ini_set('display_errors', 1);

// Bootstrap the application.
require dirname(__FILE__).'/import.php';

class GoogleCloudStorageApp extends JApplicationCli
{
  /**
   * Display the application.
   */
  function doExecute(){
    $options = new JRegistry();

    $options->set('project.id', '262345695620');
    $options->set('client.id', '262345695620.apps.googleusercontent.com');
    $options->set('client.email', '262345695620@developer.gserviceaccount.com');
    $options->set('client.keyFile', 'AIzaSyC7bfl_zHGcxeOSfNzhzRLzj3YPFE2Q-wo');

    $googlecloudstorage = new JGooglecloudstorage($options);
    try
    {
      $response = $googlecloudstorage->objects->put->putObjectAcl(
        "test-put-bucket-2",
        "testFile.txt",
        array(
          "Owner" => "00b4903a97138b52f86bbff6ae0f21489cf1428e79641bd6e18c9684f034bf13",
          "Entries" => array(
            array(
              "Permission" => "FULL_CONTROL",
              "Scope" => array(
                "type" => "GroupById",
                "ID" => "00b4903a976ccfcd626423a59ea76477f98e19bfdbaf9ecd9da5dc091ea39eff",
              )
            ),
            array(
              "Permission" => "FULL_CONTROL",
              "Scope" => array(
                "type" => "UserByEmail",
                "EmailAddress" => "alex.ukf@gmail.com",
                "Name" => "Alex Marin",
              ),
            ),
            array(
              "Permission" => "FULL_CONTROL",
              "Scope" => array(
                "type" => "GroupById",
                "ID" => "00b4903a971c9d0699ba584e218b6419b0327c60567599c5a3c12d845a371de9",
              ),
            ),
          ),
        ),
        null,
        array(
          "x-goog-if-generation" => "1379366784366000",
          "x-goog-if-metageneration" => 6,
        )
      );

      print_r($response);
    }
    catch (Exception $e)
    {
      echo 'Caught exception: ',  $e->getMessage(), "\n";
    }
  }
}

$cli = JApplicationCli::getInstance('GoogleCloudStorageApp');
JFactory::$application = $cli;

$session = JFactory::getSession();
if ($session->isActive() == false){
  $session->initialise(JFactory::getApplication()->input);
  $session->start();
}

// Run the application
$cli->execute();